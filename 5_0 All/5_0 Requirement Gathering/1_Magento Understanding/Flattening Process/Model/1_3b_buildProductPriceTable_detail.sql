
-- dw_product_prices
	-- Drop and Create
	
-- dw_core_config_data
	-- Insert
	SELECT cs.store_id, cd.path, cd.value
	FROM 
			{$this->dbname}.core_config_data cd
		CROSS JOIN 
			{$this->dbname}.core_store cs
	WHERE cd.scope_id = 0 AND cs.store_id != 0;

	SELECT cd.scope_id, cd.path, cd.value
	FROM {$this->dbname}.core_config_data cd
	WHERE cd.scope = 'websites';

	SELECT cd.scope_id, cd.path, cd.value
	FROM {$this->dbname}.core_config_data cd
	WHERE cd.scope = 'stores';

-- dw_conf_presc_required
	-- Insert
	SELECT *
    FROM dw_core_config_data
    WHERE path = 'prescription/settings/enable';



-- dw_current_vat
	-- Insert 
	SELECT store_id
	FROM {$this->dbname}.core_store 
	WHERE store_id != 0;

	UPDATE dw_current_vat cv, dw_core_config_data cd
	SET cv.base_currency = cd.value
	WHERE cd.path = 'currency/options/base' AND cv.store_id = cd.store_id;

	UPDATE dw_current_vat cv, {$this->dbname}.directory_currency_rate cr 
	SET cv.base_to_global_rate = cr.rate
	WHERE cv.base_currency = cr.currency_from AND currency_to = 'GBP';
	
	UPDATE dw_current_vat
	set prof_fee_rate = 0.18;
	UPDATE dw_current_vat
	set prof_fee_rate = 0
	WHERE store_id IN (11, 19);

	UPDATE dw_current_vat
	set vat_rate = 1.20;
	UPDATE dw_current_vat
	set vat_rate = 1.21
	WHERE store_id IN (5);
	UPDATE dw_current_vat
	set vat_rate = 1.23
	WHERE store_id IN (3);
	UPDATE dw_current_vat
	set prof_fee_rate = 0.5
	WHERE store_id IN (3);
	UPDATE dw_current_vat
	set vat_rate = 1.19
	WHERE store_id IN (6);
	UPDATE dw_current_vat
	set shipping_cost = 0;



-- dw_product_prices
	-- Insert
	SELECT cs.name, tp.entity_id,
		tp.qty, GREATEST(1, CAST(p.packsize AS UNSIGNED)),
		tp.value, tp.qty * tp.value, GREATEST(1, CAST(p.packsize AS UNSIGNED)) * tp.value,
		p.cost, p.cost * tp.qty, 
		cs.store_id
	FROM 
			{$this->dbname}.catalog_product_entity_tier_price tp
		INNER JOIN 
			{$this->dbname}.core_store cs1 ON cs1.website_id = tp.website_id
		CROSS JOIN 
			{$this->dbname}.core_store cs
		INNER JOIN 
			dw_products_all_stores p ON p.product_id = tp.entity_id AND cs.store_id = p.store_id
		INNER JOIN 
			{$this->dbname}.catalog_product_website cpw ON cpw.product_id = p.product_id AND cpw.website_id = p.store_id
	WHERE tp.website_id = 0 AND cs.store_id != 0;

	SELECT cs.name, tp.entity_id,
		tp.qty, GREATEST(1, CAST(p.packsize AS UNSIGNED)),
		tp.value, tp.qty * tp.value, GREATEST(1, CAST(p.packsize AS UNSIGNED)) * tp.value,
		p.cost, p.cost * tp.qty,
		p.cost, p.cost * tp.qty,
		cs.store_id
	FROM 
			{$this->dbname}.catalog_product_entity_tier_price tp
		INNER JOIN 
			{$this->dbname}.core_store cs ON cs.website_id = tp.website_id
		INNER JOIN 
			dw_products_all_stores p ON p.product_id = tp.entity_id AND cs.store_id = p.store_id
		INNER JOIN 
			{$this->dbname}.catalog_product_website cpw ON cpw.product_id = p.product_id AND cpw.website_id = p.store_id
	WHERE tp.website_id != 0;

	-- Update 
	UPDATE dw_product_prices p, dw_current_vat cv
	SET
		p.local_prof_fee = local_qty_price_inc_vat * cv.prof_fee_rate,
		p.prof_fee_percent = cv.prof_fee_rate,
		p.store_currency_code = cv.base_currency,
		p.local_to_global_rate = cv.base_to_global_rate

		p.local_price_vat = (p.local_qty_price_inc_vat) - ((p.local_qty_price_inc_vat - p.local_prof_fee) / cv.vat_rate),
		p.local_qty_price_exc_vat = ((p.local_qty_price_inc_vat - p.local_prof_fee) / cv.vat_rate),
		p.local_product_cost = local_product_cost * (1 / local_to_global_rate)

		p.local_shipping_cost = cv.shipping_cost
	WHERE cv.store_id = p.store_id;

	UPDATE dw_product_prices pp, dw_products p, dw_current_vat cv
	SET local_shipping_cost = (local_shipping_cost / 2) * (1 / local_to_global_rate)
	WHERE pp.product_id = p.product_id AND p.is_lens = 1 AND cv.store_id = pp.store_id;

	UPDATE dw_product_prices p
	SET p.local_total_cost = local_total_cost + local_shipping_cost;

	UPDATE dw_product_prices p, dw_current_vat cv
	SET p.local_margin_amount = (p.local_qty_price_exc_vat) - (local_total_cost)
	WHERE cv.store_id = p.store_id;

	UPDATE dw_product_prices p, dw_current_vat cv
	SET
		p.vat_percent_before_prof_fee = cv.vat_rate - 1,
		p.local_margin_percent = p.local_margin_amount / p.local_qty_price_inc_vat,
		p.vat_percent = local_price_vat / local_qty_price_exc_vat
	WHERE cv.store_id = p.store_id;


	UPDATE dw_product_prices p
	SET
	  p.global_unit_price_inc_vat = p.local_unit_price_inc_vat * p.local_to_global_rate,
	  p.global_qty_price_inc_vat  = p.local_qty_price_inc_vat * p.local_to_global_rate,
	  p.global_pack_price_inc_vat = p.local_pack_price_inc_vat * p.local_to_global_rate,
	  p.global_prof_fee = p.local_prof_fee * p.local_to_global_rate,
	  p.global_shipping_cost = p.local_shipping_cost * p.local_to_global_rate,
	  p.global_margin_amount = p.local_margin_amount * p.local_to_global_rate,
	  p.global_price_vat = p.local_price_vat * p.local_to_global_rate;

	UPDATE dw_product_prices p
	SET
	  p.global_margin_percent = p.global_margin_amount / p.global_qty_price_inc_vat;