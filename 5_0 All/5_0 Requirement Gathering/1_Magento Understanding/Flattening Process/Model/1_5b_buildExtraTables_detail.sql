
-- max_reorder_quote_payment
SELECT quote_id, MAX(payment_id) payment_id
FROM {$this->dbname}.po_reorder_quote_payment
GROUP BY quote_id;

-- dw_products
UPDATE dw_products p, dw_glasses_products g
SET p.product_type = 'glasses'
WHERE p.product_id = g.product_id;

-- order_status_history_agg
SELECT eh.order_id as order_id, eh.status, MIN(eh.updated_at) status_time
FROM
		{$this->dbname}.sales_order_log eh
	INNER JOIN 
		dw_order_headers doh ON eh.order_id = doh.order_id
	INNER JOIN 
		dw_updated_customers uc ON uc.customer_id = doh.customer_id
WHERE eh.status IN ('canceled', 'complete', 'shipped')
GROUP BY order_id, status;

-- edi_stock_item_agg
SELECT MAX(product_id) product_id, product_code,
	MAX(BC) BC, MAX(PO) PO, MAX(CY) CY, MAX(DO) DO, MAX(CO) CO, MAX(DI) DI, MAX(AD) AD, MAX(AX) AX
FROM {$this->dbname}.edi_stock_item
WHERE product_code IS NOT NULL
GROUP BY product_code;

-- first_inv
-- first_inv_staging
SELECT ih.order_id, MIN(ih.invoice_id) as invoice_id, MIN(ih.created_at) AS invoiced_at
FROM
		dw_invoice_headers ih
	INNER JOIN 
		dw_order_headers doh ON ih.order_id = doh.order_id
    INNER JOIN 
		dw_updated_customers uc ON uc.customer_id = doh.customer_id
GROUP BY ih.order_id;

SELECT * 
FROM first_inv_staging;

-- first_ship
-- first_ship_staging
SELECT sh.order_id, MIN(sh.created_at) AS shipped_at
FROM
		dw_shipment_headers sh
	INNER JOIN 
		dw_order_headers doh ON sh.order_id = doh.order_id
    INNER JOIN 
		dw_updated_customers uc ON uc.customer_id = doh.customer_id
GROUP BY sh.order_id;

SELECT * 
FROM first_ship_staging;

-- last_full_ship
-- last_full_ship_staging
SELECT sh.order_id, MAX(sh.created_at) AS shipped_at
FROM
		dw_shipment_headers sh
	INNER JOIN 
		dw_order_headers doh ON sh.order_id = doh.order_id
    INNER JOIN 
		dw_updated_customers uc ON uc.customer_id = doh.customer_id
WHERE sh.order_id IN 
	(select sub_osh.order_id
    FROM 
			order_status_history_agg sub_osh
		INNER JOIN 
			dw_order_headers sub_doh ON sub_osh.order_id = sub_doh.order_id
      	INNER JOIN 
			dw_updated_customers sub_uc ON sub_uc.customer_id = sub_doh.customer_id
	WHERE sub_osh.status = 'shipped')
GROUP BY sh.order_id;

SELECT * 
FROM last_full_ship_staging;

-- last_full_payment
-- last_full_payment_staging
SELECT ih.order_id, MAX(ih.created_at) AS payment_time
FROM
		dw_invoice_headers ih 
	INNER JOIN 
		dw_order_headers oh ON ih.order_id = oh.order_id
    INNER JOIN 
		dw_updated_customers uc ON uc.customer_id = oh.customer_id
    WHERE oh.base_total_paid = oh.base_grand_total
    GROUP BY oh.order_id;

SELECT * FROM last_full_payment_staging;

	-- dw_category_path
		-- category path to a max depth of 7 parent categories
	SELECT d1.category_id,
		d1.name name1, d2.name name2, d3.name name3, d4.name name4,
		d5.name name5, d6.name name6, d7.name name7, d8.name name8
    FROM 
			dw_categories d1 
		LEFT JOIN 
			dw_categories d2 ON d1.parent_category_id = d2.category_id
		LEFT JOIN 
			dw_categories d3 ON d2.parent_category_id = d3.category_id
		LEFT JOIN 
			dw_categories d4 ON d3.parent_category_id = d4.category_id
		LEFT JOIN 
			dw_categories d5 ON d4.parent_category_id = d5.category_id
		LEFT JOIN 
			dw_categories d6 ON d5.parent_category_id = d6.category_id
		LEFT JOIN 
			dw_categories d7 ON d6.parent_category_id = d7.category_id
		LEFT JOIN 
			dw_categories d8 ON d7.parent_category_id = d8.category_id;

	-- dw_product_category_map
		-- match magento category structure to known category hierarchy for magento found in dw_flat_category_map
	SELECT p.product_id,
		MAX(p.name) name,
		IFNULL(MAX(fc.modality_name), 'NONE') modality_name, IFNULL(MAX(fc1.feature_name), 'NONE') feature_name, IFNULL(MAX(fc2.type_name), 'NONE')    type_name,
		IFNULL(MAX(p.product_type), 'Other') product_type_name,
		IFNULL(MAX(p.telesales_only), 0)      telesales_only
    FROM 
			dw_products p 
		LEFT JOIN 
			{$this->dbname}.catalog_category_product cpc ON p.product_id = cpc.product_id
		LEFT JOIN 
			dw_categories c ON c.category_id = cpc.category_id
		LEFT JOIN 
			dw_category_path cp ON cp.category_id = c.category_id
		LEFT JOIN 
			dw_flat_category fc ON fc.modality_name = cp.name1 OR fc.modality_name = cp.name2 OR fc.modality_name = cp.name3 OR fc.modality_name = cp.name4 OR 
				fc.modality_name = cp.name5 OR fc.modality_name = cp.name6 OR fc.modality_name = cp.name7 OR fc.modality_name = cp.name8
		LEFT JOIN 
			dw_flat_category fc1 ON fc1.feature_name = cp.name1 OR fc1.feature_name = cp.name2 OR fc1.feature_name = cp.name3 OR fc1.feature_name = cp.name4 OR 
				fc1.feature_name = cp.name5 OR fc1.feature_name = cp.name6 OR fc1.feature_name = cp.name7 OR fc1.feature_name = cp.name8
		LEFT JOIN 
			dw_flat_category fc2 ON fc2.type_name = cp.name1 OR fc2.type_name = cp.name2 OR fc2.type_name = cp.name3 OR fc2.type_name = cp.name4 OR 
				fc2.type_name = cp.name5 OR fc2.type_name = cp.name6 OR fc2.type_name = cp.name7 OR fc2.type_name = cp.name8
	GROUP BY p.product_id;

	-- dw_product_category_flat
		-- build a product category relation based on this note this is dependant on categor names on default magetno store
 	SELECT product_id, MAX(category_id) category_id
	FROM 
		(SELECT product_id, IFNULL(category_id,(SELECT category_id FROM dw_flat_category WHERE product_type = 'Other')) category_id
		FROM    
				dw_product_category_map mp     
			LEFT JOIN  
				dw_flat_category fc ON fc.modality_name = mp.modality_name AND fc.feature_name = mp.feature_name AND fc.type_name = mp.type_name AND 
					fc.product_type_name = mp.product_type_name AND fc.telesales_only = mp.telesales_only) a
	GROUP BY product_id;

	-- dw_product_price_qtys
    SELECT product_id, store_id, MIN(qty) AS first_tier_qty, MAX(qty) AS multibuy_qty
    FROM dw_product_prices
    GROUP BY product_id, store_id;

-- dw_stores
SELECT *
FROM dw_stores_map;

SELECT
    website_id, name, '', '', 1
FROM {$this->dbname}.core_website
WHERE website_id NOT IN (SELECT store_id FROM dw_stores);

-- customer_first_order
-- customer_first_order_staging
SELECT oh.customer_id, MIN(oh.order_id) order_id
FROM 
		dw_order_headers oh
    INNER JOIN 
		dw_updated_customers uc ON uc.customer_id = oh.customer_id
WHERE oh.customer_id IS NOT NULL
GROUP BY customer_id;

SELECT * FROM customer_first_order_staging;