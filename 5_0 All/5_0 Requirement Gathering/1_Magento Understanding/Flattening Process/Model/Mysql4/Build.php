<?php

class Pointernal_Datawarehouse_Model_Mysql4_Build extends Mage_Core_Model_Mysql4_Abstract
{
	private $mysqlTypes = array('static' => '', 'varchar' => 'varchar(255)', 'int' => 'int(11)', 'text' => 'text', 'datetime' => 'datetime', 'decimal' => 'decimal(12,4)');
	private $dbname;
    private $newLine = "CHAR(10 using utf8)";
	private $itemQtyCol = array(
		'order_lines' => 'qty_ordered',
		'invoice_lines' => 'qty',
		'shipment_lines' => 'qty',
		'creditmemo_lines' => 'qty',
	);

	private $itemIdCol = array(
		'order_lines' => 'item_id',
		'invoice_lines' => 'order_line_id',
		'shipment_lines' => 'order_line_id',
		'creditmemo_lines' => 'order_line_id',
	);

	private $localStoreCreditIncVat = array(
		'creditmemo_headers' => 'ABS((IFNULL(oh.base_customer_balance_amount, 0)) + (GREATEST(IFNULL(oh.bs_customer_bal_total_refunded, 0) - IFNULL(oh.base_customer_balance_amount, 0), 0)))',
		'order_headers' => 'ABS(IFNULL(oh.base_customer_balance_amount, 0))',
		'invoice_headers' => 'ABS(IFNULL(oh.base_customer_balance_amount, 0))',
		'shipment_headers' => 'ABS(IFNULL(oh.base_customer_balance_amount, 0))',
	);

	private $localAdjustmentIncVat = array(
		'creditmemo_headers' => ' IFNULL(IFNULL(oh.base_adjustment_positive, 0) - IFNULL(ABS(oh.base_adjustment_negative), 0),0) ',
		'order_headers' => ' 0 ',
		'invoice_headers' => ' 0 ',
		'shipment_headers' => ' 0 ',
	);

	private $localTotalIncVat = array(
		'creditmemo_headers' => '((oh.base_subtotal) + (oh.base_shipping_amount) - (ABS(oh.base_discount_amount)) - (IFNULL(oh.base_customer_balance_amount, 0)) - (GREATEST(IFNULL(oh.bs_customer_bal_total_refunded, 0) - IFNULL(oh.base_customer_balance_amount, 0), 0))  +  (IFNULL(oh.base_adjustment_positive, 0) - IFNULL(ABS(oh.base_adjustment_negative), 0)) )',
		'order_headers' => '((oh.base_subtotal) + (oh.base_shipping_amount) - (ABS(oh.base_discount_amount)) - (IFNULL(oh.base_customer_balance_amount, 0)))',
		'invoice_headers' => '((oh.base_subtotal) + (oh.base_shipping_amount) - (ABS(oh.base_discount_amount)) - (IFNULL(oh.base_customer_balance_amount, 0)))',
		'shipment_headers' => '((oh.base_subtotal) + (oh.base_shipping_amount) - (ABS(oh.base_discount_amount)) - (IFNULL(oh.base_customer_balance_amount, 0)))',
	);

	private $localToGlobalCols = array(
		'creditmemo_headers' => 'olv.local_to_global_rate = oh.base_to_global_rate,olv.order_currency_code =  oh.order_currency_code',
		'order_headers' => 'olv.local_to_global_rate = oh.base_to_global_rate,olv.order_currency_code =  oh.order_currency_code',
		'invoice_headers' => 'olv.local_to_global_rate = oh.base_to_global_rate,olv.order_currency_code =  oh.order_currency_code',
		'shipment_headers' => 'lv.local_to_global_rate = soh.base_to_global_rate,olv.order_currency_code =  soh.order_currency_code',
	);

	private $localPriceIncVat = array(
		'order_lines' => 'IFNULL(base_price_incl_tax,price)',
		'invoice_lines' => 'IFNULL(base_price_incl_tax,price)',
		'shipment_lines' => 'oh.price',
		'creditmemo_lines' => 'IFNULL(base_price_incl_tax,price)',
	);

	private $rowTotalIncVat = array(
		'order_lines' => 'base_row_total',
		'invoice_lines' => 'base_row_total',
		'shipment_lines' => 'oh.price*oh.qty',
		'creditmemo_lines' => 'base_row_total',
	);

	private $actualCost = array(
		'order_lines' => 'base_cost * oh.qty_ordered ',
		'invoice_lines' => 'base_cost * oh.qty',
		'shipment_lines' => 'base_cost * oh.qty',
		'creditmemo_lines' => 'base_cost * oh.qty',
	);

	private $orderItemJoin = array(
		'order_lines' => ' ',
		'invoice_lines' => ' ',
		'shipment_lines' => ' INNER JOIN dw_order_lines ol ON ol.item_id = order_line_id ',
		'creditmemo_lines' => ' ',
	);

	private $customerBalanceCalc = array(
		'creditmemo_headers' => '(IFNULL(oh.base_customer_balance_amount,0)) - (GREATEST(IFNULL(oh.bs_customer_bal_total_refunded,0) - IFNULL(oh.base_customer_balance_amount,0),0))',
		'order_headers' => '(IFNULL(oh.base_customer_balance_amount,0))',
		'invoice_headers' => '(IFNULL(oh.base_customer_balance_amount,0))',
		'shipment_headers' => '(IFNULL(oh.base_customer_balance_amount,0))',
	);

	private $orderAdjustmentCalc = array(
		'creditmemo_headers' => ' 0 ',
		'order_headers' => ' 0 ',
		'invoice_headers' => 'IFNULL(oh.base_adjustment_positive,0) - IFNULL(ABS(oh.base_adjustment_negative),0)',
		'shipment_headers' => ' 0 ',
	);

	/**
	 * Specify how to flatten a magneto entity each array key is a magento entity
	 * @var array
	 */
	private $entites = array(

		'invoice' => array(
			'table_name' => 'dw_invoice_headers',
			'type' => 'flat',
			'column_rename' => array(
				'entity_id' => 'invoice_id',
				'increment_id' => 'invoice_no'
			),
			'indexes' => array(
				'index1' => array('created_at'),
				'index2' => array('order_id'),
			),
		),

		'invoice_item' => array(
			'table_name' => 'dw_invoice_lines',
			'parent' => array(                              // structure for how to join to parent object to hild items
				'parent_table' => 'dw_invoice_headers',
				'parent_id' => 'invoice_id',
				'child_id' => 'parent_id'
			),
			'type' => 'items',
			'column_rename' => array(
				'entity_id' => 'item_id',
				'increment_id' => 'invoice_line_no',
				'parent_id' => 'invoice_id',
				'order_item_id' => 'order_line_id'
			),
			'indexes' => array(
				'index1' => array('invoice_id'),
				'index2' => array('product_id'),
				'index3' => array('order_line_id'),
			),
		),

		'creditmemo' => array(
			'table_name' => 'dw_creditmemo_headers',
			'type' => 'flat',
			'column_rename' => array(
				'entity_id' => 'creditmemo_id',
				'increment_id' => 'creditmemo_no'
			),
			'indexes' => array(
				'index1' => array('created_at'),
				'index2' => array('order_id'),
			),
		),
		'creditmemo_item' => array(
			'table_name' => 'dw_creditmemo_lines',
			'parent' => array(
				'parent_table' => 'dw_creditmemo_headers',
				'parent_id' => 'creditmemo_id',
				'child_id' => 'parent_id'
			),
			'type' => 'items',
			'column_rename' => array(
				'entity_id' => 'item_id',
				'increment_id' => 'creditmemo_line_no',
				'parent_id' => 'creditmemo_id',
				'order_item_id' => 'order_line_id'
			),
			'indexes' => array(
				'index1' => array('creditmemo_id'),
				'index2' => array('product_id'),
				'index3' => array('order_line_id'),
			),
		),


		'shipment' => array(
			'table_name' => 'dw_shipment_headers',
			'type' => 'flat',
			'column_rename' => array(
				'entity_id' => 'shipment_id',
				'increment_id' => 'shipment_no'
			),
			'indexes' => array(
				'index1' => array('order_id'),
			),
		),

		'shipment_item' => array(
			'table_name' => 'dw_shipment_lines',
			'parent' => array(
				'parent_table' => 'dw_shipment_headers',
				'parent_id' => 'shipment_id',
				'child_id' => 'parent_id'
			),
			'type' => 'items',
			'column_rename' => array(
				'entity_id' => 'item_id',
				'increment_id' => 'shipment_line_no',
				'parent_id' => 'shipment_id',
				'order_item_id' => 'order_line_id'
			),
			'indexes' => array(
				'index1' => array('shipment_id'),
				'index2' => array('product_id'),
				'index3' => array('order_line_id'),
			),
		),


		'order' => array(
			'table_name' => 'dw_order_headers',
			'type' => 'flat',
			'column_rename' => array(
				'entity_id' => 'order_id',
				'increment_id' => 'order_no'
			),
			'indexes' => array(
				'index1' => array('created_at'),
				'index2' => array('updated_at'),
				'index3' => array('status'),
				'index4' => array('coupon_code'),
				'index5' => array('affilCode'),
			),
		),

		'order_address' => array(
			'table_name' => 'dw_order_address',
			'parent'=>array(
				'parent_table'=>'dw_order_headers',
				'parent_id'=>'order_id',
				'child_id'=>'parent_id'
				),
			'type' => 'items',
			'column_rename' => array(
				'parent_id' => 'order_id',
				'entity_id' => 'address_id',
			)
		),

		'order_item' => array(
			'table_name' => 'dw_order_lines',
			'parent' => array(
				'parent_table' => 'dw_order_headers',
				'parent_id' => 'order_id',
				'child_id' => 'order_id'
			),
			'type' => 'items',
			'column_rename' => array(
				'entity_id' => 'item_id',
				'increment_id' => 'order_line_no',
				'parent_id' => 'order_id',
				'order_item_id' => 'order_line_id'
			),
			'indexes' => array(
				'index1' => array('order_id'),
				'index2' => array('product_id'),
			),
		),


		/**
		 * 'quote'=>array(
		 * 'table_name'=>'dw_quote_headers',
		 * 'type'=>'flat',
		 * 'column_rename'=>array(
		 * 'entity_id'=>'quote_id',
		 * 'increment_id'=>'quote_no'
		 * )
		 * ),
		 * 'quote_item'=>array(
		 * 'table_name'=>'dw_quote_lines',
		 * 'parent'=>array(
		 * 'parent_table'=>'dw_quote_headers',
		 * 'parent_id'=>'quote_id',
		 * 'child_id'=>'item_id'
		 * ),
		 * 'type'=>'items',
		 * 'column_rename'=>array(
		 * 'entity_id'=>'item_id',
		 * 'increment_id'=>'quote_line_no',
		 * 'quote_item_id'=>'quote_line_id'
		 * )
		 * ),
		 **/
		'catalog_category' => array(
			'table_name' => 'dw_categories',
			'type' => 'eav',
			'all_stores' => 1,
			'column_rename' => array(
				'entity_id' => 'category_id',
				'parent_id' => 'parent_category_id',
			)
		),

		'catalog_product' => array(
			'table_name' => 'dw_products',
			'type' => 'eav',
			'all_stores' => 1,
			'column_rename' => array(
				'entity_id' => 'product_id',
			)
		),

		'customer' => array(
			'table_name' => 'dw_customers',
			'type' => 'eav',
			'column_rename' => array(
				'entity_id' => 'customer_id'
			)
		),

		'customer_address' => array(
			'table_name' => 'dw_customer_addresses',
			'type' => 'eav',
			'column_rename' => array(
				'entity_id' => 'address_id',
				'parent_id' => 'customer_id',
				'increment_id' => 'address_no'
			)
		),
	);

	public function flagStdTables()
	{
		$currentSql = "";
		foreach ($this->entites as $entityCode => $entity) {
			$currentSql .= "UPDATE {$entity['table_name']} SET dw_proc = 1 where dw_proc = 0;";
		}
		$currentSql .= "TRUNCATE dw_updated_customers";
		$this->runQuery($currentSql);
	}

	/*
     * Create a table from a standerd magento entity such as eav entity
     *
     * name: buildStdTables
     * @param
     * @return
     *
     */

	function runQuery($sql, $delim = ';')
	{
		$write = Mage::getSingleton('core/resource')->getConnection('dwflatreplica_write');
		$sql = preg_replace('/\s+/', ' ', $sql);
		if (!empty($delim)) {
			$queries = explode($delim, $sql);
		} else {
			$queries = (array)$sql;
		}

		//make a table use for tracking how long each query takes
		$write->query("CREATE TABLE IF NOT EXISTS dw_log_query_time (
			query_text TEXT,
			run_at DATETIME,
			duration DECIMAL(12,4),
			affected INT)");

		foreach ($queries as $query) {
			$query = trim($query);
			if (empty($query)) {
				continue;
			}
			$this->log($query);
			$time_start = $this->microtime_float();
			$result = $write->raw_query($query);
			$time_end = $this->microtime_float();
			$this->log("Query time " . (string)($time_end - $time_start) . "     Rows Affected " . $result->rowCount() . "<br/><br/>");
			$write->raw_query("INSERT INTO dw_log_query_time VALUES (
			'" . addslashes($query) . "',
			NOW(),
			" . (string)($time_end - $time_start) . ",{$result->rowCount()})");
		}
	}

	/**
	 * Given a list of magento tables that represent data for separate magento entities this step  will create or update a set of table in
	 * dw_flat database which will be used for further data processing.  Data is updated incrementally based on which records have been updated
	 * since the last run
	 *
	 * Entities are build based on the entity array fields below
	 *
	 *	entity type key
	 *	Key of entity list - entity code used to look up magento table to get information on tables and structure
	 *	table_name
	 *	Name of the flat table that will be created in dw_flat
	 *	type
	 *	Describes if table is eav format or a single flat table
	 *	column_rename
	 *	An array of columns to rename in the resulting flat table
	 *	indexes
	 *	An array of indexes to add to resulting flat table
	 *	parent
	 *	An array for describing the relationship of the child table that has a parent table.  Parent table is a table that will be be join with parent_id and child_id being the join kes
	 *	all_stores
	 *	Boolean value describes ifan all store table which will hold attribute values for each store should be created

	 * @throws Exception
	 */
	public function buildStdTables()
	{
		$write = Mage::getSingleton('core/resource')->getConnection('dwflatreplica_write');
		//remove tables that are ok to be generated each time
		$sql = <<< SQL
DROP TABLE IF EXISTS `dw_categories`;
DROP TABLE IF EXISTS `dw_categories_all_stores`;
DROP TABLE IF EXISTS `dw_products`;
DROP TABLE IF EXISTS `dw_products_all_stores`;
SQL;
		$this->runQuery($sql);

		foreach ($this->entites as $entityCode => $entity) {
			//find the backend table for specific entity
			try {
				$currentSql = $this->getEntityTypeQuery($entityCode);
				$entityTable = $write->fetchAll($currentSql);
				$allStores = false;
				if (isset($entity['all_stores'])) {
					$allStores = true;
				}

				$entityTable = str_replace('/', '_', $entityTable[0] ['entity_table']);

				//if flat then table name should be sales_flat
				if ($entity['type'] == 'flat') {
					$entityTable = "sales_flat_$entityCode";
				}
				if ($entity['type'] == 'items') {
					$entityTable = "sales_flat_$entityCode";
				}
				if ($entity['type'] == 'eav') {
					$entityTable = $entityCode . '_entity';
				}

				//get the field set and type from entity table
				$currentSql = "desc {$this->dbname}.$entityTable";
				$entityFieldList = $write->fetchAll($currentSql);
				//get all fields from base table and set types
				$entityField = array();
				//entity updates store update statement for each attriubte
				$attUpdates = array();
				//store all the fields from the base table and primary keys
				$baseField = array();
				$baseOrigField = array(); // without renames
				$origPrimKey = array();
				$primKey = array();


				foreach ($entityFieldList as $columns) {
					$baseField[] = $this->getFieldName($columns['Field'], $entity['column_rename']);
					$baseOrigField[] = $columns['Field'];

					if ($columns['Key'] == 'PRI') {
						$primKey[] = $this->getFieldName($columns['Field'], $entity['column_rename']);
						$origPrimKey[] = $columns['Field'];
					}
					$entityField[] = '`' . $this->getFieldName($columns['Field'], $entity['column_rename']) . '` ' . $columns['Type'];
				}
				//eav table add extra fields on from the eav format
				if ($entity['type'] == 'eav') {
					$currentSql = $this->getEavQuery($entityCode);
					$eavFieldList = $write->fetchAll($currentSql);
					foreach ($eavFieldList as $columns) {
						if ($columns['backend_type'] != 'static') {
							$entityField[] = '`' . $this->getFieldName($columns['attribute_code'], $entity['column_rename']) . '` ' . $this->getMysqlType($columns['backend_type']);
							$attUpdates[$columns['attribute_code']] = $this->generateAttUpdate(
								$entity['table_name'],
								$this->getFieldName($columns['attribute_code'], $entity['column_rename']),
								$this->getFieldName('entity_id', $entity['column_rename']),
								$columns['backend_type'],
								$columns['attribute_id'],
								$entityTable,
								$allStores,
                                false,
                                $columns['backend_table']
							);
						}
					}
				}

				//add dw flags these flags will be used to flag what has changed and if it needs to be updated or not

				$entityField[] = 'dw_att int(1)';                       //eav attributes have been added
				$entityField[] = 'dw_type int(1)';
				$entityField[] = 'dw_synced int(1)';
				$entityField[] = 'dw_synced_at datetime';
				$entityField[] = 'dw_proc int(1)';
				//$entityField[] = 'entity_hash varchar(100) NOT NULL';   //hash for checking if value has changed


				$currentSql = $this->createTableQuery($entity['table_name'], $entityField, $primKey, $entity['indexes']);
				$this->runQuery($currentSql);
				//$currentSql = $this->markRecordsQuery($entity['table_name']);
				//$this->runQuery($currentSql);
				$currentSql = $this->baseTableInsert($entityTable, $entity['table_name'], $baseOrigField, $baseField, $origPrimKey, $primKey, $entity['parent'],$entity['updated_at']);
				$this->runQuery($currentSql);
				foreach ($attUpdates as $query) {
					$currentSql = $query;
					$this->runQuery($currentSql);
				}


				//if all store flaggged add rows to mimic store inheritance and create flat table with info for all stores
				if ($allStores) {
					$this->runQuery("DROP TABLE IF EXISTS {$entity['table_name']}_all_stores;");
					$this->runQuery("CREATE TABLE IF NOT EXISTS {$entity['table_name']}_all_stores SELECT a.* FROM {$entity['table_name']} a;");
					$this->runQuery("ALTER TABLE {$entity['table_name']}_all_stores ADD COLUMN store_id INT DEFAULT 0;");
					$this->runQuery("ALTER TABLE {$entity['table_name']}_all_stores ADD PRIMARY KEY(" . implode($primKey, ',') . ",store_id);");
					$storeCollection = Mage::getModel('core/store')->getCollection();
					foreach ($storeCollection as $store) {
						$storeId = $store->getId();
						if ($storeId == 0) {
							continue;
						}
						$this->runQuery("INSERT INTO  {$entity['table_name']}_all_stores SELECT a.*, {$storeId} FROM {$entity['table_name']} a;");
					}
					//rebuild updates to point at new table
					$attUpdates = array();
					foreach ($eavFieldList as $columns) {
						if ($columns['backend_type'] != 'static') {
							$entityField[] = '`' . $this->getFieldName($columns['attribute_code'], $entity['column_rename']) . '` ' . $this->getMysqlType($columns['backend_type']);
							$attUpdates[$columns['attribute_code']] = $this->generateAttUpdate(
								$entity['table_name'] . "_all_stores",
								$this->getFieldName($columns['attribute_code'], $entity['column_rename']),
								$this->getFieldName('entity_id', $entity['column_rename']),
								$columns['backend_type'],
								$columns['attribute_id'],
								$entityTable,
								$allStores,
								true,
                                $columns['backend_table']
							);
						}
					}

					foreach ($attUpdates as $query) {
						$currentSql = $query;
						$this->runQuery($currentSql);
					}

					$this->runQuery("UPDATE " . $entity['table_name'] . "_all_stores SET dw_att = 1 WHERE dw_att =0");
				}

				//flag updates as done
				$this->runQuery("UPDATE " . $entity['table_name'] . " SET dw_att = 1 WHERE dw_att =0");


			} catch (Exception $e) {
				//TODO: possible if missing column just drop and try again because of structure change
				throw $e;
			}
		}

		//move this to after all fields are synced in case something crashed

		foreach ($this->entites as $entityCode => $entity) {
			//find the backend table for specific entity
			$currentSql = $this->markRecordsQuery($entity['table_name']);
			$this->runQuery($currentSql);
		}

		//get rid of shipments that may have been deleted
		$this->removeDeletedShipments();

		//hack to fix everclear orders producing empty invoices for no reason
		$this->fixEverclearOrders();
	}

	public function log($data)
	{
		Mage::log($data, 0, 'datawarehouse.log');
		echo($data);
		echo("\n");

	}

	/*
     * Create table for each document type that will hold values for vat calculations
     *
     * name: buildVatTables
     * @param
     * @return
     *
     */

	function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	/*
     * Create tables to calculate the length of time bwtweeen specific order events
     * name: createCalEntities
     * @param
     * @return
     *
     */

	function getEntityTypeQuery($entity_code)
	{
		return "SELECT
					entity_table
				FROM  {$this->dbname}.eav_entity_type where entity_type_code = '$entity_code';";
	}


	/*
     * Handle renaming of columns in table creation
     *
     * name: getFieldName
     * @param
     * @return
     *
     */

	function getFieldName($field, $renames)
	{

		if ($renames[$field]) {
			return $renames[$field];
		}
		return $field;
	}

	/*
     * Return string query for creating a standard table
     * name: createTableQuery
     * @param
     * @return
     *
     */

	function getEavQuery($entity_code)
	{
		return "SELECT
					et.entity_type_id,
					attribute_id,
					attribute_code,
					entity_table,
					backend_type,
					backend_table
				FROM {$this->dbname}.eav_entity_type et INNER JOIN  {$this->dbname}.eav_attribute ea ON ea.entity_type_id = et.entity_type_id
				WHERE et.entity_type_code = '$entity_code'";
	}

	/*
     * Return the correct entity type table for each attribute item in an eav format
     *
     * name: getEntityTypeQuery
     * @param
     * @return
     *
     */

	function getMysqlType($field)
	{
		if ($this->mysqlTypes[$field]) {
			return $this->mysqlTypes[$field];
		}
		return $field;
	}

	/*
     * Return a query that will update an eav attribute
     *
     * name: getEavQuery
     * @param
     * @return
     *
     */

    function generateAttUpdate($tableName,$fields,$entityField,$backendType,$attId,$backendTable,$allStores,
                               $store_id = false,$backend_table =false)
    {
		$table = str_replace('/', '_', $backendTable);
		$table .= "_$backendType";
        if($backend_table) {
            $table = $backend_table;
        }
		$sql = "UPDATE $tableName a, {$this->dbname}.$table b SET a.$fields = b.value WHERE a.$entityField = b.entity_id AND b.attribute_id = $attId AND dw_att=0";
		if ($allStores) {
			$sql .= " AND b.store_id =" . ($store_id ? "a.store_id" : "0");
		}
		return $sql;
	}

	/*
     * Return a query to insert static columns from a magento  table
     *
     * name: baseTableInsert
     * @param
     * @return
     *
     */

	function createTableQuery($tableName, $fields, $primKeys, $indexes)
	{
		$indexes[] = array('dw_att');
		$indexes[] = array('dw_type');
		$indexes[] = array('dw_synced');
		$indexes[] = array('dw_synced_at');
		$indexes[] = array('dw_proc');
		//$indexes[] = array('entity_hash');
		$sql = "CREATE TABLE IF NOT EXISTS $tableName (" .
			implode(',', $fields);
		$sql .= count($primKeys) ? ',PRIMARY KEY(' . implode(',', $primKeys) . ')' : '';
		if (count($indexes)) {
			foreach ($indexes as $index) {
				$sql .= ',INDEX(' . implode(',', $index) . ')';
			}
		}
		$sql .= ");";
		return $sql;
	}

	function baseTableInsert($entityTable,$tableName,$origFields,$fields,$origKeys,$primKeys,$parent = null) {

		if(!is_array($parent)) {
			$sql = "REPLACE INTO $tableName (".implode(',',$fields).",dw_att,dw_type,dw_synced,dw_synced_at,dw_proc)"
				." SELECT ".implode(',',  $this->addAlias('a', $origFields)).",0,0,0,null,0".
				" FROM {$this->dbname}.$entityTable a LEFT JOIN $tableName b ON ".$this->makeJoinStatement($origKeys,$primKeys).
				' WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL)';
			// ' WHERE (a.updated_at > b.updated_at) OR (b.updated_at IS NULL) LIMIT 0,1000';
		}
		else {
			$parentTable =  $parent['parent_table'];
			$childId =  $parent['child_id'];
			$parentId =  $parent['parent_id'];

			$sql = "REPLACE INTO $tableName (".implode(',',$fields).",dw_att,dw_synced,dw_synced_at,dw_proc)"
				." SELECT ".implode(',',  $this->addAlias('a', $origFields)).",0,0,null,0".
				" FROM {$this->dbname}.$entityTable a LEFT JOIN $parentTable b ON a.$childId = b.$parentId".
				' WHERE (b.dw_type = 0 ) OR (b.dw_type IS NULL)';
			// ' WHERE (b.dw_type = 0 ) OR (b.dw_type IS NULL) LIMIT 0,1000';
		}

		return $sql;
	}

	function addAlias($alias, $fields)
	{
		$aliased = array();
		foreach ($fields as $field) {
			$aliased[] = "$alias.$field";
		}
		return $aliased;
	}

	/** Create join from the primary keys odf te table **/
	function makeJoinStatement($origKeys, $keys)
	{
		$joins = array();
		foreach ($origKeys as $i => $val) {
			$joins[] = "a.$val=b." . $keys[$i];
		}
		return implode(" AND ", $joins);
	}


	/*
     * Return query to update eav  attribute in cases where sotre structure exists default value wil be selected
     *
     * name: generateAttUpdate
     * @param
     * @return
     *
     */

	function markRecordsQuery($tableName)
	{
		return "UPDATE $tableName SET dw_type =1 WHERE dw_type = 0";
	}

	public function removeDeletedShipments()
	{
		$sql = "DELETE sh FROM dw_shipment_headers sh, {$this->dbname}.dw_deleted_shipment ds  WHERE sh.shipment_id = ds.shipment_id;
        DROP TABLE IF EXISTS missing_shipment_items;
        CREATE TABLE IF NOT EXISTS missing_shipment_items (
            shipment_id INT,
            PRIMARY KEY (shipment_id)
        ) AS
        SELECT DISTINCT shipment_id FROM dw_shipment_lines
        WHERE shipment_id NOT IN (SELECT shipment_id FROM dw_shipment_headers);
        DELETE sl FROM dw_shipment_lines sl , missing_shipment_items mi WHERE sl.shipment_id = mi.shipment_id;";
		$this->runQuery($sql);
	}

	/***
	 * Everclear is giveaway giving a spurious invoices so this hack is used to remove orders and allocate items to the
	 * correct invocie
	 * **/
	public function fixEverclearOrders()
	{
		$sql = "/** find invoices that have just eveclear in them **/
		DROP TABLE IF EXISTS dw_invoice_lines_everclear_empty;
		CREATE TABLE dw_invoice_lines_everclear_empty
		SELECT	ih.invoice_id,
			ih.order_id,
			ih.store_id,
			il.item_id,
			il.order_line_id,
			il.product_id,
			0 AS first_inv
		FROM `dw_invoice_headers` ih INNER JOIN `dw_invoice_lines` il ON il.invoice_id = ih.invoice_id
		WHERE ih.`grand_total` = 0 AND il.product_id = 2305
		ORDER BY ih.invoice_id;

		ALTER TABLE dw_invoice_lines_everclear_empty ADD INDEX idx_invoice_id (invoice_id);
		ALTER TABLE dw_invoice_lines_everclear_empty ADD INDEX idx_order_id (order_id);

		DROP TABLE IF EXISTS dw_invoice_confirm_empty_everclear;
		CREATE TABLE dw_invoice_confirm_empty_everclear
		SELECT invoice_id,MAX(product_id) max_pid,MIN(product_id) min_pid FROM dw_flat.dw_invoice_lines
		WHERE invoice_id IN ( SELECT DISTINCT invoice_id FROM dw_invoice_lines_everclear_empty)
		GROUP BY invoice_id;

		DELETE FROM dw_invoice_confirm_empty_everclear WHERE max_pid = 2305 AND max_pid = 2305;

		ALTER TABLE dw_invoice_confirm_empty_everclear ADD INDEX idx_invoice_id (invoice_id);
		DELETE d FROM dw_invoice_lines_everclear_empty d , dw_invoice_confirm_empty_everclear c WHERE d.invoice_id = c.invoice_id;;

		/** find first invoice for each of these **/
		DROP TABLE IF EXISTS dw_invoice_lines_everclear_first_invoice;
		CREATE TABLE dw_invoice_lines_everclear_first_invoice
		SELECT order_id, MIN(invoice_id) first_invoice_id
		FROM dw_flat.dw_invoice_headers WHERE order_id IN (SELECT order_id FROM dw_invoice_lines_everclear_empty)
		GROUP BY invoice_id;

		UPDATE dw_invoice_lines_everclear_empty e , dw_invoice_lines_everclear_first_invoice f SET first_inv = first_invoice_id
		WHERE f.order_id = e.order_id;

		DELETE FROM dw_invoice_lines_everclear_empty WHERE invoice_id = first_inv;

		/** in invoice items change the invoice id to the  first invoice id and then remove these invoices***/

		UPDATE  dw_invoice_lines il  , dw_invoice_lines_everclear_empty e SET il .invoice_id = first_inv
		WHERE il.item_id = e.item_id AND il.invoice_id != first_inv ;

		DELETE ih FROM dw_invoice_headers ih,  dw_invoice_lines_everclear_empty e where ih.invoice_id = e.invoice_id
												And ih.invoice_Id != first_inv;
		CREATE TABLE IF NOT EXISTS `dw_invoice_lines_everclear_empty_fixed` (
		  `invoice_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
		  `order_id` INT(10) UNSIGNED DEFAULT NULL,
		  `store_id` SMALLINT(5) UNSIGNED DEFAULT NULL,
		  `item_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
		  `order_line_id` INT(10) DEFAULT NULL,
		  `product_id` INT(10) DEFAULT NULL,
		  `first_inv` INT(1) NOT NULL DEFAULT '0',
		  PRIMARY KEY(invoice_id,order_id,item_id,first_inv)
		);
		REPLACE INTO dw_invoice_lines_everclear_empty_fixed SELECT * FROM dw_invoice_lines_everclear_empty";
		$this->runQuery($sql);
	}

	/**
	 * Create tables that flags customers that have a record that has changed since last build
	 */
	public function createSyncListTables()
	{
		$sql = <<< SQL
CREATE TABLE IF NOT EXISTS dw_updated_customers (
  customer_id   INT NOT NULL,
  update_record VARCHAR(255),
  PRIMARY KEY (customer_id)
);
REPLACE INTO dw_updated_customers
  SELECT
    customer_id,
    'customer'
  FROM dw_customers
  WHERE dw_proc = 0;
REPLACE INTO dw_updated_customers
  SELECT
    customer_id,
    'orders'
  FROM dw_order_headers
  WHERE dw_proc = 0;
REPLACE INTO dw_updated_customers
  SELECT
    oh.customer_id,
    'creditmemo'
  FROM dw_creditmemo_headers ch INNER JOIN dw_order_headers oh ON oh.order_id = ch.order_id
  WHERE ch.dw_proc = 0;
REPLACE INTO dw_updated_customers
  SELECT
    oh.customer_id,
    'invoice'
  FROM dw_invoice_headers ch INNER JOIN dw_order_headers oh ON oh.order_id = ch.order_id
  WHERE ch.dw_proc = 0;
REPLACE INTO dw_updated_customers
SELECT
  ch.customer_id,
  'shipment'
FROM dw_shipment_headers ch INNER JOIN dw_order_headers oh ON oh.order_id = ch.order_id
WHERE ch.dw_proc = 0;
SQL;
		$this->runQuery($sql);
		//TODO: on completion we need to state that this no longer needs to be flagged as updated
	}

	public function buildVatTables()
	{
		//if needed add item level versions of missing items
		$this->addMissingLineItems('creditmemo_lines', 'creditmemo_headers', 'creditmemo_id');
		$this->buildVatHeaderTable('order_headers', 'order_lines', 'order_id');
		$this->updateVatValues('order_headers', 'order_lines', 'order_id');
		$this->buildVatHeaderTable('invoice_headers', 'invoice_lines', 'invoice_id');
		$this->updateVatValues('invoice_headers', 'invoice_lines', 'invoice_id');
		$this->buildVatHeaderTable('creditmemo_headers', 'creditmemo_lines', 'creditmemo_id');
		$this->updateVatValues('creditmemo_headers', 'creditmemo_lines', 'creditmemo_id');
		$this->buildVatHeaderTable('shipment_headers', 'shipment_lines', 'shipment_id');
		$this->updateVatValues('shipment_headers', 'shipment_lines', 'shipment_id');
	}

	function addMissingLineItems($item_entity, $entity, $entityId)
	{
		$sql = <<< SQL
REPLACE INTO dw_{$item_entity} (
            item_id ,
            {$entityId} ,
            base_row_total ,
            discount_amount ,
            row_total ,
            base_discount_amount ,
            qty ,
            base_cost ,
            price ,
            base_row_total_incl_tax ,
            row_total_incl_tax ,
            product_id ,
            order_line_id ,
            sku ,
            name ,
            dw_att ,
            dw_type ,
            dw_synced ,
            dw_synced_at,
            dw_proc
        )
        SELECT
            {$entityId} + 1000000 AS item_id  ,
             {$entityId} ,
            (oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS base_row_total,
            discount_amount,
            (oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS row_total,
            base_discount_amount ,
            0 AS qty ,
            0 AS base_cost ,
            (oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS price ,
            (oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS base_row_total_incl_tax ,
            (oh.base_subtotal + oh.base_shipping_amount - ABS(oh.base_discount_amount) - {$this->customerBalanceCalc[$entity]} + (IFNULL({$this->orderAdjustmentCalc[$entity]} ,0))) AS row_total_incl_tax ,
            0 AS product_id ,
            0 AS order_line_id ,
            'ADJUSTMENT' AS sku ,
            'ADJUSTMENT' AS name ,
            dw_att ,
            dw_type ,
            dw_synced ,
            dw_synced_at,
            dw_proc
        FROM dw_{$entity} oh
        WHERE {$entityId} NOT IN (SELECT  {$entityId} FROM dw_{$item_entity} WHERE  {$entityId} IS NOT NULL)
SQL;
		$this->runQuery($sql);
	}

	function buildVatHeaderTable($entity, $item_entity, $entity_id)
	{
		$sql = <<< SQL
		CREATE TABLE IF NOT EXISTS {$entity}_vat  (
			   {$entity_id}  int(11) NOT NULL DEFAULT '0',
			   local_subtotal_inc_vat  decimal(12,4) DEFAULT NULL,
			   local_subtotal_vat  decimal(12,4) DEFAULT NULL,
			   local_subtotal_exc_vat  decimal(12,4) DEFAULT NULL,
			   local_shipping_inc_vat  decimal(12,4) DEFAULT NULL,
			   local_shipping_vat  decimal(12,4) DEFAULT NULL,
			   local_shipping_exc_vat  decimal(12,4) DEFAULT NULL,
			   local_discount_inc_vat  decimal(12,4) DEFAULT NULL,
			   local_discount_vat  decimal(12,4) DEFAULT NULL,
			   local_discount_exc_vat  decimal(12,4) DEFAULT NULL,
			   local_store_credit_inc_vat  decimal(12,4) DEFAULT NULL,
			   local_store_credit_vat  decimal(12,4) DEFAULT NULL,
			   local_store_credit_exc_vat  decimal(12,4) DEFAULT NULL,
			   local_adjustment_inc_vat  decimal(12,4) DEFAULT NULL,
			   local_adjustment_vat  decimal(12,4) DEFAULT NULL,
			   local_adjustment_exc_vat  decimal(12,4) DEFAULT NULL,
			   local_total_inc_vat  decimal(12,2) DEFAULT NULL,
			   local_total_vat  decimal(12,4) DEFAULT NULL,
			   local_total_exc_vat  decimal(12,2) DEFAULT NULL,
			  `local_subtotal_cost` decimal(12,4) DEFAULT NULL,
			  `local_shipping_cost` decimal(12,4) DEFAULT NULL,
			  `local_total_cost` decimal(12,4) DEFAULT NULL,
			  `local_margin_amount` decimal(12,4) DEFAULT NULL,
			  `local_margin_percent` decimal(12,4) DEFAULT NULL,
			   global_subtotal_inc_vat  decimal(12,4) DEFAULT NULL,
			   global_subtotal_vat  decimal(12,4) DEFAULT NULL,
			   global_subtotal_exc_vat  decimal(12,4) DEFAULT NULL,
			   global_shipping_inc_vat  decimal(12,4) DEFAULT NULL,
			   global_shipping_vat  decimal(12,4) DEFAULT NULL,
			   global_shipping_exc_vat  decimal(12,4) DEFAULT NULL,
			   global_discount_inc_vat  decimal(12,4) DEFAULT NULL,
			   global_discount_vat  decimal(12,4) DEFAULT NULL,
			   global_discount_exc_vat  decimal(12,4) DEFAULT NULL,
			   global_store_credit_inc_vat  decimal(12,4) DEFAULT NULL,
			   global_store_credit_vat  decimal(12,4) DEFAULT NULL,
			   global_store_credit_exc_vat  decimal(12,4) DEFAULT NULL,
			   global_adjustment_inc_vat  decimal(12,4) DEFAULT NULL,
			   global_adjustment_vat  decimal(12,4) DEFAULT NULL,
			   global_adjustment_exc_vat  decimal(12,4) DEFAULT NULL,
			   global_total_inc_vat  decimal(12,2) DEFAULT NULL,
			   global_total_vat  decimal(12,4) DEFAULT NULL,
			   global_total_exc_vat  decimal(12,2) DEFAULT NULL,
			  `global_subtotal_cost` decimal(12,4) DEFAULT NULL,
			  `global_shipping_cost` decimal(12,4) DEFAULT NULL,
			  `global_total_cost` decimal(12,4) DEFAULT NULL,
			  `global_margin_amount` decimal(12,4) DEFAULT NULL,
			  `global_margin_percent` decimal(12,4) DEFAULT NULL,
			   prof_fee_percent  decimal(12,4) DEFAULT NULL,
			   prof_fee  decimal(12,4) DEFAULT NULL,
			   global_prof_fee  decimal(12,4) DEFAULT NULL,
			   total_cost  decimal(12,4) DEFAULT NULL,
			   vat_percent  decimal(12,4) DEFAULT NULL,
			   vat_rate  decimal(12,4) DEFAULT NULL,
			  `vat_percent_before_prof_fee` decimal(12,4) DEFAULT NULL,
			  `discount_percent` decimal(12,4) DEFAULT NULL,
			   country_type  varchar(10) DEFAULT NULL,
			   country_id varchar(10) DEFAULT NULL,
			   vat_logic varchar(255) DEFAULT NULL,
			   entity_order_id INT DEFAULT NULL,
			   has_lens INT DEFAULT NULL,
			   vat_done INT NOT NULL DEFAULT 0,
			   rollup_total  decimal(12,2) DEFAULT NULL,
			   rollup_has_lens  INT DEFAULT NULL,
			   total_qty decimal(12,4) DEFAULT NULL,
               max_row_total decimal(12,4),
               min_row_total decimal(12,4),
               calculated_vat decimal(12,4),
               calculated_prof_fee decimal(12,4),
               total_weight decimal(12,4),
               magento_grand_total decimal(12,4),
               order_date DATETIME,
			   INDEX(order_date),
			   INDEX(vat_done),
			   INDEX(entity_order_id),
			  PRIMARY KEY ( {$entity_id} )
			);
			CREATE TABLE IF NOT EXISTS {$item_entity}_vat (
			item_id INT,
			  `local_prof_fee` decimal(12,4) DEFAULT NULL,
			  `local_price_inc_vat` decimal(12,4) DEFAULT NULL,
			  `local_price_vat` decimal(12,4) DEFAULT NULL,
			  `local_price_exc_vat` decimal(12,4) DEFAULT NULL,
			  `local_line_subtotal_inc_vat` decimal(12,4) DEFAULT NULL,
			  `local_line_subtotal_vat` decimal(12,4) DEFAULT NULL,
			  `local_line_subtotal_exc_vat` decimal(12,4) DEFAULT NULL,
			  `local_shipping_inc_vat` decimal(12,4) DEFAULT NULL,
			  `local_shipping_vat` decimal(12,4) DEFAULT NULL,
			  `local_shipping_exc_vat` decimal(12,4) DEFAULT NULL,
			  `local_discount_inc_vat` decimal(12,4) DEFAULT NULL,
			  `local_discount_vat` decimal(12,4) DEFAULT NULL,
			  `local_discount_exc_vat` decimal(12,4) DEFAULT NULL,
			  `local_store_credit_inc_vat` decimal(12,4) DEFAULT NULL,
			  `local_store_credit_vat` decimal(12,4) DEFAULT NULL,
			  `local_store_credit_exc_vat` decimal(12,4) DEFAULT NULL,
			  `local_adjustment_inc_vat` decimal(12,4) DEFAULT NULL,
			  `local_adjustment_vat` decimal(12,4) DEFAULT NULL,
			  `local_adjustment_exc_vat` decimal(12,4) DEFAULT NULL,
			  `local_line_total_inc_vat` decimal(12,4) DEFAULT NULL,
			  `local_line_total_vat` decimal(12,4) DEFAULT NULL,
			  `local_line_total_exc_vat` decimal(12,4) DEFAULT NULL,
			  `local_subtotal_cost` decimal(12,4) DEFAULT NULL,
			  `local_shipping_cost` decimal(12,4) DEFAULT NULL,
			  `local_total_cost` decimal(12,4) DEFAULT NULL,
			  `local_margin_amount` decimal(12,4) DEFAULT NULL,
			  `local_margin_percent` decimal(12,4) DEFAULT NULL,
			  `local_to_global_rate` decimal(12,4) DEFAULT NULL,
			  `order_currency_code` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
			  `global_prof_fee` decimal(12,4) DEFAULT NULL,
			  `global_price_inc_vat` decimal(12,4) DEFAULT NULL,
			  `global_price_vat` decimal(12,4) DEFAULT NULL,
			  `global_price_exc_vat` decimal(12,4) DEFAULT NULL,
			  `global_line_subtotal_inc_vat` decimal(12,4) DEFAULT NULL,
			  `global_line_subtotal_vat` decimal(12,4) DEFAULT NULL,
			  `global_line_subtotal_exc_vat` decimal(12,4) DEFAULT NULL,
			  `global_shipping_inc_vat` decimal(12,4) DEFAULT NULL,
			  `global_shipping_vat` decimal(12,4) DEFAULT NULL,
			  `global_shipping_exc_vat` decimal(12,4) DEFAULT NULL,
			  `global_discount_inc_vat` decimal(12,4) DEFAULT NULL,
			  `global_discount_vat` decimal(12,4) DEFAULT NULL,
			  `global_discount_exc_vat` decimal(12,4) DEFAULT NULL,
			  `global_store_credit_inc_vat` decimal(12,4) DEFAULT NULL,
			  `global_store_credit_vat` decimal(12,4) DEFAULT NULL,
			  `global_store_credit_exc_vat` decimal(12,4) DEFAULT NULL,
			  `global_adjustment_inc_vat` decimal(12,4) DEFAULT NULL,
			  `global_adjustment_vat` decimal(12,4) DEFAULT NULL,
			  `global_adjustment_exc_vat` decimal(12,4) DEFAULT NULL,
			  `global_line_total_inc_vat` decimal(12,4) DEFAULT NULL,
			  `global_line_total_vat` decimal(12,4) DEFAULT NULL,
			  `global_line_total_exc_vat` decimal(12,4) DEFAULT NULL,
			  `global_subtotal_cost` decimal(12,4) DEFAULT NULL,
			  `global_shipping_cost` decimal(12,4) DEFAULT NULL,
			  `global_total_cost` decimal(12,4) DEFAULT NULL,
			  `global_margin_amount` decimal(12,4) DEFAULT NULL,
			  `global_margin_percent` decimal(12,4) DEFAULT NULL,
			  `prof_fee_percent` decimal(12,4) DEFAULT NULL,
			  `vat_percent_before_prof_fee` decimal(12,4) DEFAULT NULL,
			  `vat_percent` decimal(12,4) DEFAULT NULL,
			  `discount_percent` decimal(12,4) DEFAULT NULL,
			   vat_done INT NOT NULL DEFAULT 0,
			  `qty` decimal(12,4) DEFAULT NULL,
              `row_total_inc_vat` decimal(12,4) DEFAULT NULL,
              `actual_cost` decimal(12,4) DEFAULT NULL,
              `row_weight` decimal(12,4) DEFAULT NULL,
			   INDEX(vat_done),
			  PRIMARY KEY(item_id)
			);
SQL;
		$this->runQuery($sql);
	}

	function updateVatValues($entity, $item_entity, $entity_id)
	{

		/**
		 * create table that will rank  entities by the number in relation to the order
		 * i.e. first invoice etc
		 *
		 **/

		if ($entity != 'order_headers') {
			$sql = <<< SQL
DROP TABLE IF EXISTS {$entity}_rank;
CREATE TABLE IF NOT EXISTS {$entity}_rank (
{$entity_id} INT NOT NULL,
order_id INT,
rank INT,
PRIMARY KEY ({$entity_id})
, INDEX(order_id, rank)
) AS
SELECT t.*
FROM (
       SELECT
         {$entity_id},
         eh.order_id,
         (
           CASE eh.order_id
           WHEN @curEventId
           THEN @curRow := @curRow + 1
           ELSE @curRow := 1 AND @curEventId := eh.order_id END
         ) AS rank
       FROM dw_{$entity} eh INNER JOIN (SELECT @curRow := 0, @curEventId := '') r
       INNER JOIN dw_order_headers oh ON eh.order_id = oh.order_id
       INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id
       ORDER BY order_id, {$entity_id}
     ) t;
SQL;

		}
		/**shipments are treated differently **/
		if ($entity != 'shipment_headers') {
			$sql .= <<< SQL
/***
VAT is calculated backwards from gross sales amount and then allocated to items by he amount they account for on order
VAT rate is decided based on store or the country goods are dispatched to
Note that store credit is not included in VAT calculations, shipping is inculuded and taken at the same rate as the rest of the order

Basic step are
* Set country information for entity
* Calc all inc_vat totals for entity
* Flag Rows by VAT logic based on business rules
* Set VAT rate professional fee (VAT exempt amount of the order)
* Calculate VAT with formula: calculated_vat = (total-calculated_prof_fee) - ((total-calculated_prof_fee) / vat_rate)
* Calculate Exc VAT and global values
* Allocate calculated values to entity items based on formula to spread values across items
* Note as shipments don't have any monetary values attached in magento they are calculated differently this is done by using matching values from (order,invoice,creditmemo)
*/

REPLACE INTO {$entity}_vat ({$entity_id}, country_type, country_id, entity_order_id)
SELECT
  oh.{$entity_id}, MAX( c.country_type )  country_type, MAX(c.country_code) country_code, MAX(oh.order_id) entity_order_id
FROM
dw_{$entity}   oh
INNER JOIN
dw_order_headers doh ON doh.order_id = oh.order_id
INNER JOIN
dw_updated_customers duc ON duc.customer_id = doh.customer_id
LEFT JOIN
dw_order_address   oa ON oa.address_id  =  oh.shipping_address_id
INNER JOIN
dw_countries   c ON c.country_code  =  oa.country_id
GROUP BY oh.{$entity_id};

/** add aggregated values from lines that will be need for other calcs**/

UPDATE
{$entity}_vat ov, (
SELECT
ol.{$entity_id},
IFNULL(MAX(p.is_lens), 0) is_lens,
ROUND(MAX(ol.base_row_total), 2) max_row_total,
ROUND(MIN(ol.base_row_total), 2) min_row_total,
IFNULL(SUM(ol2.weight * ol.{$this->itemQtyCol[$item_entity]}), 0)  total_weight,
IFNULL(SUM(ol.base_cost * ol.{$this->itemQtyCol[$item_entity]}), 0) total_cost,
IFNULL(SUM(ol.{$this->itemQtyCol[$item_entity]}), 0) total_qty
FROM
dw_{$item_entity} ol
INNER JOIN dw_{$entity} oh ON oh.{$entity_id} = ol.{$entity_id}
INNER JOIN dw_order_headers doh ON oh.order_id = doh.order_id
INNER JOIN dw_updated_customers uc ON uc.customer_id = doh.customer_id
LEFT JOIN
dw_products p ON p.product_id = ol.product_id
LEFT JOIN
dw_order_lines ol2 ON ol2.item_id = ol.{$this->itemIdCol[$item_entity]}
GROUP BY {$entity_id}
) sub
SET
ov.has_lens = sub.is_lens,
ov.total_weight = sub.total_weight,
ov.total_cost = sub.total_cost,
ov.total_qty = sub.total_qty,
ov.max_row_total = sub.max_row_total,
ov.min_row_total = sub.min_row_total
WHERE ov.{$entity_id} = sub.{$entity_id} AND
ov.vat_done = 0;

/**update incvat fields**/
/**adjustment n/a for invoice**/
UPDATE
{$entity}_vat ov, dw_{$entity} oh, dw_order_headers  ord
SET
local_subtotal_inc_vat = oh.base_subtotal,
local_shipping_inc_vat = oh.base_shipping_amount,
local_discount_inc_vat = oh.base_discount_amount,
local_store_credit_inc_vat = {$this->localStoreCreditIncVat[$entity]},
local_adjustment_inc_vat = {$this->localAdjustmentIncVat[$entity]},
local_total_inc_vat = {$this->localTotalIncVat[$entity]},
magento_grand_total = oh.base_grand_total,
local_subtotal_cost = ov.total_cost * (1/oh.base_to_global_rate),
local_shipping_cost = IFNULL(ord.partbill_shippingcost_cost * (1/oh.base_to_global_rate), 0),
local_total_cost =  (ov.total_cost * (1/oh.base_to_global_rate)) + IFNULL(ord.partbill_shippingcost_cost * (1/oh.base_to_global_rate), 0),
ov.order_date = ord.created_at
WHERE
oh.{$entity_id} = ov.{$entity_id} AND ord.order_id =  oh.order_id AND vat_done = 0;

/** Use rank to deal with any part entities and collect rollup value
* This only applied to orders that use a sliding scale professional fee where this a percentage this has no effect
**/

DROP TABLE IF EXISTS {$entity}_vat_rollup;
CREATE TABLE IF NOT EXISTS {$entity}_vat_rollup (
{$entity_id} int not null,
total_inc_vat decimal(34, 2),
has_lens int,
PRIMARY KEY({$entity_id})
)
SELECT ohv1.{$entity_id},
SUM(ohv2.local_total_inc_vat) total_inc_vat,
MAX(ohv2.has_lens) has_lens
FROM
{$entity}_vat ohv1
INNER JOIN
{$entity}_vat ohv2 ON ohv2.entity_order_id = ohv1.entity_order_id AND
ohv2.{$entity_id} <= ohv1.{$entity_id}
WHERE ohv1.vat_done = 0
GROUP BY ohv1.{$entity_id};
UPDATE
{$entity}_vat ov,
{$entity}_vat_rollup ovr
SET
ov.rollup_total = ovr.total_inc_vat,
ov.rollup_has_lens = ovr.has_lens
WHERE
ov.{$entity_id} = ovr.{$entity_id};

/** set vat logic string for each order **/
UPDATE
{$entity}_vat
SET
vat_logic = 'NON_EU'
WHERE
country_type NOT IN('EU', 'UK') AND
country_type IS NOT NULL AND
vat_logic IS NULL;

/** old logic **/
/** postoptics ***/
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'PO-2011.01.04-2011-08-31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2011.01.04' AND '2100.01.01' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id = 1;

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'PO-2010.01.01-2011-01-03'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2011.01.03' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id = 1;
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'PO-2008.12.01-2009.12.31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2008.12.01' AND '2009.12.31' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id = 1;
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'PO-1900.01.01-2008.11.30'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2008.11.30' AND
vat_logic IS NULL AND
oh.store_id = 1;

/** gl uk **/

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLUK-2011.01.04-2011.08.31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2011.01.04' AND '2100.01.01' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
(oh.store_id IN (2, 4, 8, 3) OR  (oh.store_id IN (3, 7) AND ov.country_id != 'IE'));
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLUK-2010.01.01-2011.01.03'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2011.01.03' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
(oh.store_id IN (2, 4, 8, 3) OR  (oh.store_id IN (3, 7) AND ov.country_id != 'IE'));
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLUK-2008.12.01-2009.12.31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2008.12.01' AND '2009.12.31' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
(oh.store_id IN (2, 4, 8, 3) OR (oh.store_id IN (3, 7) AND ov.country_id != 'IE'));
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLUK-1900.01.01-2008.11.30'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2008.11.30' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
(oh.store_id IN (2, 4, 8, 3) OR (oh.store_id IN (3, 7) AND ov.country_id != 'IE'));

/* * ie **/

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLIE-2010.01.01-2100.01.01'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2100.01.01' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id  IN (3, 7);

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLIE-2009.01.01-2009.12.31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2009.01.01' AND '2009.12.31' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id  IN (3, 7);

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLIE-1900.01.01-2008.12.31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2008.12.31' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id  IN (3, 7);

/** gl it **/

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLIT-2011.01.04-2100.01.01'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2011.01.04' AND '2100.01.01' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id = 10;

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLIT-2010.01.01-2011.01.03'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2011.01.03' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id = 10;

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLIT-2008.12.01-2009.12.31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2008.12.01' AND '2009.12.31' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id = 10;

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GLIT-1900.01.01-2008.11.30'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2008.11.30' AND
cast(ov.order_date as date) <= '2011.08.31' AND
vat_logic IS NULL AND
oh.store_id = 10;

/** getoptics logic set prof fee and percent**/

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-NL-2012.09.30'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2012.09.30' AND
vat_logic IS NULL AND
country_id = 'NL'
AND oh.store_id NOT IN (11, 19);

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-NL-2012.10.01'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2012.10.01' AND '2100.01.01' AND
country_id = 'NL' AND
vat_logic IS NULL AND
oh.store_id NOT IN (11, 19);

/** be ***/
UPDATE
    {$entity}_vat ov,
    dw_{$entity} oh
SET
     vat_logic = 'GO-BE-2015.10.31'
WHERE
    ov.{$entity_id} = oh.{$entity_id} AND
    country_id = 'BE' AND
    cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2015.10.31'AND
    vat_logic IS NULL
    AND oh.store_id NOT IN (11,19);
UPDATE
    {$entity}_vat ov,
    dw_{$entity} oh
SET
     vat_logic = 'GO-BE-CURENT'
WHERE
    ov.{$entity_id} = oh.{$entity_id} AND
    country_id = 'BE' AND
    cast(ov.order_date as date) BETWEEN '2015.11.01' AND '2100.01.01'AND
    vat_logic IS NULL
    AND oh.store_id NOT IN (11,19);

/** es **/
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-ES-CURENT'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2013.01.01' AND '2100.01.01' AND
country_id = 'ES' AND
vat_logic IS NULL
AND oh.store_id NOT IN (11, 19);

/** it **/
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-IT-CURENT'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2013.01.01' AND '2100.01.01' AND
country_id = 'IT' AND
vat_logic IS NULL
AND oh.store_id NOT IN (11, 19);


/** ie **/

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-IE-2011.10.31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2011.10.31' AND
country_id = 'IE' AND
vat_logic IS NULL
AND oh.store_id NOT IN (11, 19);

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-IE-2011.11.01'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2011.11.01' AND '2011.12.31' AND
country_id = 'IE' AND
vat_logic IS NULL
AND oh.store_id NOT IN (11, 19);

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-IE-2012.01.01-2012.10.31'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2012.01.01' and '2012.10.31' AND
country_id = 'IE' AND
vat_logic IS NULL
AND oh.store_id NOT IN (11, 19);
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-IE-2012.11.01-CURRENT'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
cast(ov.order_date as date) BETWEEN '2012.11.01' and '2100.01.01' AND
country_id = 'IE' AND
vat_logic IS NULL
AND oh.store_id NOT IN (11, 19);
/** every one else **/
UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'GO-GROUP'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
vat_logic IS NULL
AND oh.store_id NOT IN (11, 19);

/** asda **/

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'ASDA'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
vat_rate IS NULL
AND oh.store_id =11;

/** eyeplan **/

UPDATE
{$entity}_vat ov,
dw_{$entity} oh
SET
vat_logic = 'EYEPLAN'
WHERE
ov.{$entity_id} = oh.{$entity_id} AND
vat_rate IS NULL
AND oh.store_id =19;

/** getoptics logic work out vat rate based on date**/
/** non eu should be  always be 0 vat **/

UPDATE
{$entity}_vat
SET
prof_fee_percent = 0,
prof_fee = 0,
calculated_prof_fee = 0,
vat_percent = 0,
vat_rate = 0,
local_total_vat = 0,
local_subtotal_vat  = 0,
local_shipping_vat = 0,
local_discount_vat = 0,
local_store_credit_vat = 0,
local_adjustment_vat = 0,
local_total_vat = 0,
calculated_vat = 0,
calculated_prof_fee = 0
WHERE
vat_logic = 'NON_EU' AND vat_done =0;

/** postoptics **/
/** postoptics 1**/
UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total = 0
THEN 0
WHEN rollup_has_lens = 1
THEN 7.5
ELSE 0
END,
vat_rate = 1.20
WHERE
vat_logic = 'PO-2011.01.04-2011-08-31' AND
vat_done = 0;

/** postoptics 2 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total = 0
THEN 0
WHEN rollup_has_lens = 1
THEN 7.5
ELSE 0
END,
vat_rate = 1.175
WHERE
vat_logic = 'PO-2010.01.01-2011-01-03' AND
vat_done = 0;

/** postoptics 3 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total = 0
THEN 0
WHEN rollup_has_lens = 1
THEN 7.5
ELSE 0
END,
vat_rate = 1.15
WHERE
vat_logic = 'PO-2008.12.01-2009.12.31' AND
vat_done = 0;

/** postoptics 4 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total = 0
THEN 0
WHEN rollup_has_lens = 1
THEN 7.5
ELSE 0
END,
vat_rate = 1.175
WHERE
vat_logic = 'PO-1900.01.01-2008.11.30' AND
vat_done = 0;

/** gl uk **/
/**  gl uk 1 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total > 149.99
THEN 65
WHEN rollup_total > 69.99
THEN 40
WHEN rollup_total > 30
THEN 20
WHEN rollup_total = 0
THEN 0
ELSE 0
END,
vat_rate = 1.20
WHERE
vat_logic = 'GLUK-2011.01.04-2011.08.31' AND
vat_done = 0;

/**  gl uk 2 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total > 149.99
THEN 65
WHEN rollup_total > 69.99
THEN 40
WHEN rollup_total > 30
THEN 20
WHEN rollup_total = 0
THEN 0
ELSE 0
END,
vat_rate = 1.175
WHERE
vat_logic = 'GLUK-2010.01.01-2011.01.03' AND
vat_done = 0;

/**  gl uk 3 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total > 149.99
THEN 65
WHEN rollup_total > 69.99
THEN 40
WHEN rollup_total > 30
THEN 20
WHEN rollup_total = 0
THEN 0
ELSE 0
END,
vat_rate = 1.15
WHERE
vat_logic = 'GLUK-2008.12.01-2009.12.31' AND
vat_done = 0;

/**  gl uk 4 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total > 149.99
THEN 65
WHEN rollup_total > 69.99
THEN 40
WHEN rollup_total > 30
THEN 20
WHEN rollup_total = 0
THEN 0
ELSE 0
END,
vat_rate = 1.175
WHERE
vat_logic = 'GLUK-1900.01.01-2008.11.30' AND
vat_done = 0;

/** gl ie **/
/** gl ie 1 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = rollup_total / 2,
vat_rate = 1.21
WHERE
vat_logic = 'GLIE-2010.01.01-2100.01.01' AND
vat_done = 0;

/** gl ie 2 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = rollup_total / 2,
vat_rate = 1.215
WHERE
vat_logic = 'GLIE-2009.01.01-2009.12.31' AND
vat_done = 0;
/** 3 **/
UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = rollup_total / 2,
vat_rate = 1.21
WHERE
vat_logic = 'GLIE-1900.01.01-2008.12.31' AND
vat_done = 0;

/** gl it **/
/** gl it 1 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total > 149.99
THEN 65
WHEN rollup_total > 69.99
THEN 40
WHEN rollup_total > 30
THEN 20
WHEN rollup_total = 0
THEN 0
ELSE 0
END,
vat_rate = 1.20
WHERE
vat_logic = 'GLIT-2011.01.04-2100.01.01' AND
vat_done = 0;

/** gl it 2 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total > 149.99
THEN 65
WHEN rollup_total > 69.99
THEN 40
WHEN rollup_total > 30
THEN 20
WHEN rollup_total = 0
THEN 0
ELSE 0
END,
vat_rate = 1.175
WHERE
vat_logic = 'GLIT-2010.01.01-2011.01.03' AND
vat_done = 0;

/** gl it 3 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total > 149.99
THEN 65
WHEN rollup_total > 69.99
THEN 40
WHEN rollup_total > 30
THEN 20
WHEN rollup_total = 0
THEN 0
ELSE 0
END,
vat_rate = 1.15
WHERE
vat_logic = 'GLIT-2008.12.01-2009.12.31' AND
vat_done = 0;

/** gl it 4 **/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0,
calculated_prof_fee = CASE
WHEN rollup_total > 149.99
THEN 65
WHEN rollup_total > 69.99
THEN 40
WHEN rollup_total > 30
THEN 20
WHEN rollup_total = 0
THEN 0
ELSE 0
END,
vat_rate = 1.175
WHERE
vat_logic = 'GLIT-1900.01.01-2008.11.30' AND
vat_done = 0;

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0.18,
calculated_prof_fee = rollup_total * 0.18,
vat_rate = 1.19
WHERE
vat_logic = 'GO-NL-2012.09.30'
AND vat_done =0;

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0.18,
calculated_prof_fee = rollup_total * 0.18,
vat_rate = 1.21
WHERE
vat_logic = 'GO-NL-2012.10.01'
AND vat_done =0;

/** getoptics be ***/
UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0.18,
calculated_prof_fee = rollup_total * 0.18,
vat_rate = 1.20
WHERE
vat_logic = 'GO-BE-2015.10.31'
AND vat_done =0;


UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0.18,
calculated_prof_fee = rollup_total * 0.18,
vat_rate = 1.21
WHERE
vat_logic = 'GO-BE-CURENT'
AND vat_done =0;

/** getoptics es ***/

UPDATE
{$entity}_vat ov
SET
prof_fee_percent = 0.18,
calculated_prof_fee = rollup_total * 0.18,
vat_rate = 1.10
WHERE
vat_logic = 'GO-ES-CURENT'
AND vat_done =0;

/** getoptics it ***/
UPDATE
{$entity}_vat ov
            SET
                prof_fee_percent = 0.18 ,
                calculated_prof_fee = rollup_total * 0.18,
                vat_rate = 1.04
            WHERE
                vat_logic = 'GO-IT-CURENT' AND vat_done =0;

/** getoptics ie **/
UPDATE
    {$entity}_vat ov
SET
    prof_fee_percent = 0.18 ,
    calculated_prof_fee = rollup_total * 0.18,
    vat_rate = 1.21
WHERE
   vat_logic = 'GO-IE-2011.10.31' AND vat_done =0;

UPDATE
    {$entity}_vat ov
SET
    prof_fee_percent = 0.18 ,
    calculated_prof_fee = rollup_total * 0.18,
    vat_rate = 1.21 / 0.5263
WHERE
    vat_logic = 'GO-IE-2011.11.01' AND vat_done =0;

UPDATE
    {$entity}_vat ov
SET
    prof_fee_percent = 0.18 ,
    calculated_prof_fee = rollup_total * 0.18,
    vat_rate = 1.23 / 0.5263
WHERE
    vat_logic = 'GO-IE-2012.01.01-2012.10.31' AND vat_done =0;
    UPDATE
{$entity}_vat ov
SET
    prof_fee_percent = 0.5 ,
    calculated_prof_fee = rollup_total * 0.5,
    vat_rate = 1.23
WHERE
    vat_logic = 'GO-IE-2012.11.01-CURRENT' AND vat_done =0;

/** getoptics non specific - every one else **/

UPDATE
    {$entity}_vat ov
SET
    prof_fee_percent = 0.18 ,
    calculated_prof_fee = rollup_total * 0.18,
    vat_rate = 1.20
WHERE
    vat_logic = 'GO-GROUP' AND vat_done =0;

/** asda **/

UPDATE
    {$entity}_vat
SET
    prof_fee_percent = 0,
    calculated_prof_fee = 0
WHERE
    vat_logic = 'ASDA' AND vat_done =0;

/** eyeplan **/

UPDATE
    {$entity}_vat
SET
    prof_fee_percent = 0,
    calculated_prof_fee = 0
WHERE
    vat_logic = 'EYEPLAN' AND vat_done =0;

/**  vat for asda **/
UPDATE
    {$entity}_vat ov
SET
    vat_rate = 1.072
WHERE
    vat_logic = 'ASDA' AND vat_done =0;

/**  vat for eyeplan **/
UPDATE
    {$entity}_vat ov
SET
    vat_rate = 1.21
WHERE
    vat_logic = 'EYEPLAN' AND vat_done =0;


/** fill in vat only different for ie with CL / SOL blended split**/
UPDATE
    {$entity}_vat ov,
    dw_{$entity} oh
SET
    calculated_vat = ((rollup_total-calculated_prof_fee) * 0.5263) - ((rollup_total-calculated_prof_fee) / vat_rate)
WHERE
    ov.{$entity_id} = oh.{$entity_id} AND  vat_done =0 AND
    vat_logic IN ('GO-IE-2011.11.01','GO-IE-2012.01.01-2012.10.31');

UPDATE
    {$entity}_vat ov
SET
    calculated_vat = (rollup_total-calculated_prof_fee) - ((rollup_total-calculated_prof_fee) / vat_rate)
WHERE
   vat_logic NOT IN ('GO-IE-2011.11.01','GO-IE-2012.01.01-2012.10.31')
   AND vat_logic != 'NON_EU' AND vat_done =0;

UPDATE  {$entity}_vat SET local_total_vat = calculated_vat WHERE vat_done = 0;
UPDATE  {$entity}_vat SET prof_fee = calculated_prof_fee WHERE vat_done = 0;
SQL;

			if ($entity != 'order_headers') {
				$sql .= <<< SQL
/** handle entities with part documents ie credit / invoice **/
UPDATE
{$entity}_rank ch1,
{$entity}_vat chv,
{$entity}_rank ch2,
{$entity}_vat chv2
SET
chv2.local_total_vat = IFNULL(chv2.calculated_vat - IFNULL(chv.calculated_vat,0),0),
chv2.prof_fee = IFNULL(chv2.calculated_prof_fee - IFNULL(chv.calculated_prof_fee,0),0)
WHERE
chv.{$entity_id} = ch1.{$entity_id}  AND
ch2.order_id = ch1.order_id  AND
ch1.rank + 1 = ch2.rank AND
ch2.{$entity_id} = chv2.{$entity_id} AND
chv2.vat_done = 0;
SQL;
			}
			$sql .= <<< SQL
/** error if no vat logic has been set **/

UPDATE
    {$entity}_vat ov
SET
    vat_rate = 0 ,
    prof_fee_percent = 0,
    calculated_prof_fee = 0,
    local_total_vat = local_total_inc_vat,
    prof_fee = 0
WHERE
    vat_logic IS NULL;
/** fill in vat related columns if total is 0 then set to 0 **/
/** fill in ex vat cols **/
UPDATE
    {$entity}_vat ov
SET
    local_subtotal_vat = IFNULL(CASE WHEN local_total_inc_vat = 0 OR local_subtotal_inc_vat = 0 THEN 0 ELSE ((local_subtotal_inc_vat / local_total_inc_vat ) * local_total_vat) END ,0) ,
    local_shipping_vat = IFNULL(CASE WHEN local_total_inc_vat = 0 OR local_shipping_inc_vat = 0 THEN 0 ELSE  ((local_shipping_inc_vat / local_total_inc_vat ) * local_total_vat) END ,0),
    local_discount_vat = IFNULL(CASE WHEN local_discount_inc_vat = 0 OR local_discount_inc_vat = 0 THEN 0 ELSE  (-ABS((ABS(local_discount_inc_vat) / local_total_inc_vat ) * local_total_vat)) END ,0),
    local_store_credit_vat = IFNULL(CASE WHEN local_store_credit_vat = 0 OR local_store_credit_inc_vat = 0 THEN 0 ELSE ((local_store_credit_inc_vat / local_total_inc_vat ) * local_total_vat) END ,0),
    local_adjustment_vat = IFNULL(CASE WHEN local_total_inc_vat = 0  OR local_adjustment_inc_vat = 0 THEN 0 ELSE  ((local_adjustment_inc_vat / local_total_inc_vat ) * local_total_vat) END ,0)
WHERE
    vat_done = 0;
/** update date ex vat cols **/
UPDATE
    {$entity}_vat ov
SET
    local_subtotal_exc_vat = local_subtotal_inc_vat - local_subtotal_vat,
    local_total_exc_vat = local_total_inc_vat - local_total_vat,
    local_shipping_exc_vat = local_shipping_inc_vat - local_shipping_vat,
    local_discount_exc_vat = -ABS(ABS(local_discount_inc_vat) - ABS(local_discount_vat)),
    local_store_credit_exc_vat = local_store_credit_inc_vat - local_store_credit_vat,
    local_adjustment_exc_vat = local_adjustment_inc_vat - local_adjustment_vat
WHERE
    vat_done = 0;
/** margin and costs**/
UPDATE
    {$entity}_vat ov
SET
    vat_percent = (local_total_vat) / (local_total_exc_vat),
    vat_percent_before_prof_fee = vat_rate - 1 ,
    discount_percent = -1 * (ABS(local_discount_exc_vat) / (local_total_exc_vat  + ABS(local_discount_exc_vat))),
    local_margin_amount = local_total_exc_vat - local_total_cost  ,
    local_margin_percent = (local_total_exc_vat - local_total_cost) / local_total_exc_vat
WHERE
    vat_done = 0;


/** update global values **/
UPDATE
    {$entity}_vat ov,
    dw_{$entity} oh
SET
    global_subtotal_inc_vat = local_subtotal_inc_vat * oh.base_to_global_rate,
    global_subtotal_vat  = local_subtotal_vat * oh.base_to_global_rate,
    global_subtotal_exc_vat  = local_subtotal_exc_vat * oh.base_to_global_rate,
    global_shipping_inc_vat  = local_shipping_inc_vat * oh.base_to_global_rate,
    global_shipping_vat  = local_shipping_vat * oh.base_to_global_rate,
    global_shipping_exc_vat  = local_shipping_exc_vat * oh.base_to_global_rate,
    global_discount_inc_vat  = local_discount_inc_vat * oh.base_to_global_rate,
    global_discount_vat  = local_discount_vat * oh.base_to_global_rate,
    global_discount_exc_vat  = local_discount_exc_vat * oh.base_to_global_rate,
    global_store_credit_inc_vat  = local_store_credit_inc_vat * oh.base_to_global_rate,
    global_store_credit_vat  = local_store_credit_vat * oh.base_to_global_rate,
    global_store_credit_exc_vat  = local_store_credit_exc_vat * oh.base_to_global_rate,
    global_adjustment_inc_vat  = local_adjustment_inc_vat * oh.base_to_global_rate,
    global_adjustment_vat  = local_adjustment_vat * oh.base_to_global_rate,
    global_adjustment_exc_vat  = local_adjustment_exc_vat * oh.base_to_global_rate,
    global_total_inc_vat  = local_total_inc_vat * oh.base_to_global_rate,
    global_total_vat  = local_total_vat * oh.base_to_global_rate,
    global_total_exc_vat = local_total_exc_vat * oh.base_to_global_rate,
    global_prof_fee = prof_fee * oh.base_to_global_rate,
    global_subtotal_cost = local_subtotal_cost * oh.base_to_global_rate,
    global_shipping_cost = local_shipping_cost * oh.base_to_global_rate,
    global_total_cost = local_total_cost * oh.base_to_global_rate,
    global_margin_amount = local_margin_amount * oh.base_to_global_rate
WHERE
    ov.{$entity_id} = oh.{$entity_id}
    AND vat_done = 0;
UPDATE
    {$entity}_vat ov,
    dw_{$entity} oh
SET
    global_margin_percent =  global_margin_amount / global_total_inc_vat
WHERE
    ov.{$entity_id} = oh.{$entity_id}
    AND vat_done = 0;

UPDATE {$entity}_vat SET vat_done = 1 WHERE vat_done = 0;
/* insert  items that we need **/
/** only order has the qty ordered column  others have qty ***/
		REPLACE INTO {$item_entity}_vat
			(item_id,local_price_inc_vat,row_total_inc_vat,actual_cost,qty)
		SELECT
			oh.item_id,
			{$this->localPriceIncVat[$item_entity]},
			{$this->rowTotalIncVat[$item_entity]},
			{$this->actualCost[$item_entity]},
			oh.{$this->itemQtyCol[$item_entity]}
		FROM
			dw_{$item_entity} oh
		INNER JOIN dw_{$entity} eh ON oh.{$entity_id} = eh.{$entity_id}
		INNER JOIN dw_order_headers doh ON eh.order_id = doh.order_id
		INNER JOIN dw_updated_customers uc ON uc.customer_id = doh.customer_id
		{$this->orderItemJoin[$item_entity]};

		/** fill in allocate percent values create table to define allocation rates **/
		/** allocate by subtotal when available and qty if not **/

        CREATE TABLE IF NOT EXISTS {$item_entity}_allocation_rate (
            item_id INT NOT NULL,
            {$entity_id} INT,
            rate DECIMAL(12,8),
            PRIMARY KEY(item_id)
        );
        REPLACE INTO {$item_entity}_allocation_rate
        SELECT
            ol.item_id,
            ov.{$entity_id},
            CASE
                WHEN max_row_total = 0 AND min_row_total = 0 AND ROUND(local_subtotal_inc_vat,2) = 0
                THEN ol.{$this->itemQtyCol[$item_entity]}/ ov.total_qty
                WHEN ol.sku = 'ADJUSTMENT'
                THEN 1
                ELSE ol.base_row_total / ov.local_subtotal_inc_vat
            END AS rate
        FROM	{$entity}_vat ov
                        INNER JOIN dw_{$item_entity} ol ON ol.{$entity_id} = ov.{$entity_id}
                        INNER JOIN {$item_entity}_vat lv ON lv.item_id = ol.item_id
        WHERE	lv.vat_done = 0;

		UPDATE
			{$item_entity}_vat olv,
			dw_{$item_entity} ol,
			{$entity}_vat ov,
            {$item_entity}_allocation_rate ar
		SET
        /**
			olv.local_prof_fee = (ov.prof_fee * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_line_subtotal_vat = (ov.local_subtotal_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_line_subtotal_inc_vat = (ov.local_subtotal_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_line_subtotal_exc_vat = (ov.local_subtotal_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_shipping_inc_vat = (ov.local_shipping_inc_vat  * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_shipping_exc_vat = (ov.local_shipping_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_shipping_vat = (ov.local_shipping_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_shipping_cost = (ov.local_shipping_cost * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_subtotal_cost = (ov.local_subtotal_cost * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_total_cost = (ov.local_total_cost * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_discount_inc_vat = (ov.local_discount_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_discount_vat = (ov.local_discount_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_discount_exc_vat = (ov.local_discount_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_store_credit_inc_vat = (ov.local_store_credit_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_store_credit_vat = (ov.local_store_credit_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_store_credit_exc_vat = (ov.local_store_credit_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_adjustment_inc_vat = (ov.local_adjustment_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_adjustment_vat = (ov.local_adjustment_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_adjustment_exc_vat = (ov.local_adjustment_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_line_total_inc_vat = (ov.local_total_inc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_line_total_vat = (ov.local_total_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat)),
			olv.local_line_total_exc_vat = (ov.local_total_exc_vat * (olv.row_total_inc_vat / ov.local_subtotal_inc_vat))
        **/
            olv.local_prof_fee = (ov.prof_fee *  ar.rate),
			olv.local_line_subtotal_vat = (ov.local_subtotal_vat *  ar.rate),
			olv.local_line_subtotal_inc_vat = (ov.local_subtotal_inc_vat *  ar.rate),
			olv.local_line_subtotal_exc_vat = (ov.local_subtotal_exc_vat *  ar.rate),
			olv.local_shipping_inc_vat = (ov.local_shipping_inc_vat  *  ar.rate),
			olv.local_shipping_exc_vat = (ov.local_shipping_exc_vat *  ar.rate),
			olv.local_shipping_vat = (ov.local_shipping_vat *  ar.rate),
			olv.local_shipping_cost = (ov.local_shipping_cost *  ar.rate),
			olv.local_subtotal_cost = (ov.local_subtotal_cost *  ar.rate),
			olv.local_total_cost = (ov.local_total_cost *  ar.rate),
			olv.local_discount_inc_vat = (ov.local_discount_inc_vat *  ar.rate),
			olv.local_discount_vat = (ov.local_discount_vat *  ar.rate),
			olv.local_discount_exc_vat = (ov.local_discount_exc_vat *  ar.rate),
			olv.local_store_credit_inc_vat = (ov.local_store_credit_inc_vat *  ar.rate),
			olv.local_store_credit_vat = (ov.local_store_credit_vat *  ar.rate),
			olv.local_store_credit_exc_vat = (ov.local_store_credit_exc_vat *  ar.rate),
			olv.local_adjustment_inc_vat = (ov.local_adjustment_inc_vat *  ar.rate),
			olv.local_adjustment_vat = (ov.local_adjustment_vat *  ar.rate),
			olv.local_adjustment_exc_vat = (ov.local_adjustment_exc_vat *  ar.rate),
			olv.local_line_total_inc_vat = (ov.local_total_inc_vat *  ar.rate),
			olv.local_line_total_vat = (ov.local_total_vat *  ar.rate),
			olv.local_line_total_exc_vat = (ov.local_total_exc_vat *  ar.rate)
		WHERE
			olv.item_id = ol.item_id AND
			ol.{$entity_id} = ov.{$entity_id} AND
            ol.item_id = ar.item_id AND
			olv.vat_done = 0;
		/**assign price values & percent vals **/
		UPDATE
			{$item_entity}_vat olv,
			dw_{$item_entity} ol,
			{$entity}_vat ov
		SET
			/**olv.local_line_total_exc_vat = local_line_total_inc_vat - local_line_total_vat,**/
			olv.local_price_exc_vat = local_line_subtotal_exc_vat / olv.qty,
			olv.vat_percent =  olv.local_line_total_vat / (olv.local_line_total_exc_vat) ,
			olv.vat_percent_before_prof_fee =  ov.vat_rate - 1 ,
			olv.discount_percent = -1 * (ABS(olv.local_discount_exc_vat) /  (olv.local_line_total_exc_vat + ABS(olv.local_discount_exc_vat ))),
			olv.prof_fee_percent =  ov.prof_fee_percent
		WHERE
			olv.item_id = ol.item_id AND
			ol.{$entity_id} = ov.{$entity_id} AND
			olv.vat_done = 0;;

		UPDATE
			{$item_entity}_vat olv,
			dw_{$item_entity} ol,
			{$entity}_vat ov
		SET
			olv.local_price_vat = local_price_inc_vat - local_price_exc_vat
		WHERE
			olv.item_id = ol.item_id AND
			ol.{$entity_id} = ov.{$entity_id} AND
			olv.vat_done = 0;
		/** convert to global **/
		UPDATE
			{$item_entity}_vat olv,
			dw_{$item_entity} ol,
			dw_{$entity} oh,
			dw_order_headers soh
		SET
			{$this->localToGlobalCols[$entity]}
		WHERE
			olv.item_id = ol.item_id AND
			ol.{$entity_id} = oh.{$entity_id} AND
			oh.order_id = soh.order_id AND
			olv.vat_done = 0;

		/** update cost and margins**/
		UPDATE
			{$item_entity}_vat olv
		SET
			local_margin_amount  = local_line_total_exc_vat - local_total_cost,
			local_margin_percent  = (local_line_total_exc_vat - local_total_cost) / local_line_total_exc_vat
		WHERE
			olv.vat_done = 0;

		UPDATE
			{$item_entity}_vat
		SET
			global_prof_fee  = local_to_global_rate * local_prof_fee,
			global_price_inc_vat  = local_to_global_rate * local_price_inc_vat,
			global_price_vat  = local_to_global_rate * local_price_vat,
			global_price_exc_vat  = local_to_global_rate * local_price_exc_vat,
			global_line_subtotal_inc_vat  = local_to_global_rate * local_line_subtotal_inc_vat,
			global_line_subtotal_vat  = local_to_global_rate * local_line_subtotal_vat,
			global_line_subtotal_exc_vat  = local_to_global_rate * local_line_subtotal_exc_vat,
			global_shipping_inc_vat  = local_to_global_rate * local_shipping_inc_vat,
			global_shipping_vat  = local_to_global_rate * local_shipping_vat,
			global_shipping_exc_vat  = local_to_global_rate * local_shipping_exc_vat,
			global_discount_inc_vat  = local_to_global_rate * local_discount_inc_vat,
			global_discount_vat  = local_to_global_rate * local_discount_vat,
			global_discount_exc_vat  = local_to_global_rate * local_discount_exc_vat,
			global_store_credit_inc_vat  = local_to_global_rate * local_store_credit_inc_vat,
			global_store_credit_vat  = local_to_global_rate * local_store_credit_vat,
			global_store_credit_exc_vat  = local_to_global_rate * local_store_credit_exc_vat,
			global_adjustment_inc_vat  = local_to_global_rate * local_adjustment_inc_vat,
			global_adjustment_vat  = local_to_global_rate * local_adjustment_vat,
			global_adjustment_exc_vat  = local_to_global_rate * local_adjustment_exc_vat,
			global_line_total_inc_vat  = local_to_global_rate * local_line_total_inc_vat,
			global_line_total_vat  = local_to_global_rate * local_line_total_vat,
			global_line_total_exc_vat  = local_to_global_rate * local_line_total_exc_vat,
			global_subtotal_cost  = local_to_global_rate * local_subtotal_cost,
			global_shipping_cost  = local_to_global_rate * local_shipping_cost,
			global_total_cost  = local_to_global_rate * local_total_cost,
			global_margin_amount  = local_to_global_rate * local_margin_amount
		WHERE vat_done = 0;
        UPDATE
			{$item_entity}_vat
		SET
			global_margin_percent  = global_margin_amount / global_line_total_inc_vat
		WHERE vat_done = 0;
		UPDATE {$item_entity}_vat SET vat_done = 1 WHERE vat_done = 0;
SQL;
		} else {
			$sql .= <<< SQL
/**logic for shipments
built based each shipment item and the value of these to created a faux shipment header
dummy lines addded for creditmemo adjust prove to be difficult here  they will not match a shipment item **/
REPLACE INTO shipment_lines_vat (
  item_id,
  `local_prof_fee`,
  `local_price_inc_vat`,
  `local_price_vat`,
  `local_price_exc_vat`,
  `local_line_subtotal_inc_vat`,
  `local_line_subtotal_vat`,
  `local_line_subtotal_exc_vat`,
  `local_shipping_inc_vat`,
  `local_shipping_vat`,
  `local_shipping_exc_vat`,
  `local_discount_inc_vat`,
  `local_discount_vat`,
  `local_discount_exc_vat`,
  `local_store_credit_inc_vat`,
  `local_store_credit_vat`,
  `local_store_credit_exc_vat`,
  `local_adjustment_inc_vat`,
  `local_adjustment_vat`,
  `local_adjustment_exc_vat`,
  `local_line_total_inc_vat`,
  `local_line_total_vat`,
  `local_line_total_exc_vat`,
  `local_subtotal_cost`,
  `local_shipping_cost`,
  `local_to_global_rate`,
  `order_currency_code`,
  `global_prof_fee`,
  `global_price_inc_vat`,
  `global_price_vat`,
  `global_price_exc_vat`,
  `global_line_subtotal_inc_vat`,
  `global_line_subtotal_vat`,
  `global_line_subtotal_exc_vat`,
  `global_shipping_inc_vat`,
  `global_shipping_vat`,
  `global_shipping_exc_vat`,
  `global_discount_inc_vat`,
  `global_discount_vat`,
  `global_discount_exc_vat`,
  `global_store_credit_inc_vat`,
  `global_store_credit_vat`,
  `global_store_credit_exc_vat`,
  `global_adjustment_inc_vat`,
  `global_adjustment_vat`,
  `global_adjustment_exc_vat`,
  `global_line_total_inc_vat`,
  `global_line_total_vat`,
  `global_line_total_exc_vat`,
  `global_subtotal_cost`,
  `global_shipping_cost`,
  `prof_fee_percent`,
  vat_percent_before_prof_fee,
  vat_done,
  qty,
  row_weight
)
  SELECT
    sl.item_id,
    ov.`local_prof_fee` * (sl.qty / ov.qty),
    ov.`local_price_inc_vat` * (sl.qty / ov.qty),
    ov.`local_price_vat` * (sl.qty / ov.qty),
    ov.`local_price_exc_vat` * (sl.qty / ov.qty),
    ov.`local_line_subtotal_inc_vat` * (sl.qty / ov.qty),
    ov.`local_line_subtotal_vat` * (sl.qty / ov.qty),
    ov.`local_line_subtotal_exc_vat` * (sl.qty / ov.qty),
    ov.`local_shipping_inc_vat` * (sl.qty / ov.qty),
    ov.`local_shipping_vat` * (sl.qty / ov.qty),
    ov.`local_shipping_exc_vat` * (sl.qty / ov.qty),
    ov.`local_discount_inc_vat` * (sl.qty / ov.qty),
    ov.`local_discount_vat` * (sl.qty / ov.qty),
    ov.`local_discount_exc_vat` * (sl.qty / ov.qty),
    ov.`local_store_credit_inc_vat` * (sl.qty / ov.qty),
    ov.`local_store_credit_vat` * (sl.qty / ov.qty),
    ov.`local_store_credit_exc_vat` * (sl.qty / ov.qty),
    ov.`local_adjustment_inc_vat` * (sl.qty / ov.qty),
    ov.`local_adjustment_vat` * (sl.qty / ov.qty),
    ov.`local_adjustment_exc_vat` * (sl.qty / ov.qty),
    ov.`local_line_total_inc_vat` * (sl.qty / ov.qty),
    ov.`local_line_total_vat` * (sl.qty / ov.qty),
    ov.`local_line_total_exc_vat` * (sl.qty / ov.qty),
    ov.`local_subtotal_cost` * (sl.qty / ov.qty),
    ov.`local_shipping_cost`,
    ov.`local_to_global_rate`,
    ov.`order_currency_code`,
    ov.`global_prof_fee` * (sl.qty / ov.qty),
    ov.`global_price_inc_vat` * (sl.qty / ov.qty),
    ov.`global_price_vat` * (sl.qty / ov.qty),
    ov.`global_price_exc_vat` * (sl.qty / ov.qty),
    ov.`global_line_subtotal_inc_vat` * (sl.qty / ov.qty),
    ov.`global_line_subtotal_vat` * (sl.qty / ov.qty),
    ov.`global_line_subtotal_exc_vat` * (sl.qty / ov.qty),
    ov.`global_shipping_inc_vat` * (sl.qty / ov.qty),
    ov.`global_shipping_vat` * (sl.qty / ov.qty),
    ov.`global_shipping_exc_vat` * (sl.qty / ov.qty),
    ov.`global_discount_inc_vat` * (sl.qty / ov.qty),
    ov.`global_discount_vat` * (sl.qty / ov.qty),
    ov.`global_discount_exc_vat` * (sl.qty / ov.qty),
    ov.`global_store_credit_inc_vat` * (sl.qty / ov.qty),
    ov.`global_store_credit_vat` * (sl.qty / ov.qty),
    ov.`global_store_credit_exc_vat` * (sl.qty / ov.qty),
    ov.`global_adjustment_inc_vat` * (sl.qty / ov.qty),
    ov.`global_adjustment_vat` * (sl.qty / ov.qty),
    ov.`global_adjustment_exc_vat` * (sl.qty / ov.qty),
    ov.`global_line_total_inc_vat` * (sl.qty / ov.qty),
    ov.`global_line_total_vat` * (sl.qty / ov.qty),
    ov.`global_line_total_exc_vat` * (sl.qty / ov.qty),
    ov.`global_subtotal_cost` * (sl.qty / ov.qty),
    ov.`global_shipping_cost`,
    ov.`prof_fee_percent`,
    ov.`vat_percent_before_prof_fee`,
    0 AS vat_done,
    sl.qty,
    sl.weight
  FROM
    dw_shipment_lines sl LEFT JOIN order_lines_vat ov ON ov.item_id = sl.order_line_id
    LEFT JOIN shipment_lines_vat slv ON slv.item_id = sl.item_id
    INNER JOIN dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
    INNER JOIN dw_updated_customers uc ON uc.customer_id = sh.customer_id;

/** update cost and margins**/
UPDATE
  shipment_lines_vat
SET
  local_total_cost     = local_subtotal_cost + local_shipping_cost,
  local_margin_amount  = local_line_total_exc_vat - (local_subtotal_cost + local_shipping_cost),
  global_total_cost    = global_subtotal_cost + global_shipping_cost,
  global_margin_amount = global_line_total_exc_vat - (global_subtotal_cost + global_shipping_cost)
WHERE vat_done = 0;


/** update percentages **/
UPDATE
  shipment_lines_vat
SET
  local_margin_percent  = local_margin_amount / local_line_total_inc_vat,
  global_margin_percent = global_margin_amount / global_line_total_inc_vat,
  discount_percent      = -1 * (ABS(local_discount_exc_vat) / (local_line_total_exc_vat + ABS(local_discount_exc_vat))),
  vat_percent           = (local_line_total_vat) / (local_line_total_exc_vat)
WHERE vat_done = 0;


REPLACE INTO shipment_headers_vat (
  shipment_id,
  local_subtotal_inc_vat,
  local_subtotal_vat,
  local_subtotal_exc_vat,
  local_shipping_inc_vat,
  local_shipping_vat,
  local_shipping_exc_vat,
  local_discount_inc_vat,
  local_discount_vat,
  local_discount_exc_vat,
  local_store_credit_inc_vat,
  local_store_credit_vat,
  local_store_credit_exc_vat,
  local_adjustment_inc_vat,
  local_adjustment_vat,
  local_adjustment_exc_vat,
  local_total_inc_vat,
  local_total_vat,
  local_total_exc_vat,
  `local_subtotal_cost`,
  `local_shipping_cost`,
  `local_total_cost`,
  `local_margin_amount`,
  global_subtotal_inc_vat,
  global_subtotal_vat,
  global_subtotal_exc_vat,
  global_shipping_inc_vat,
  global_shipping_vat,
  global_shipping_exc_vat,
  global_discount_inc_vat,
  global_discount_vat,
  global_discount_exc_vat,
  global_store_credit_inc_vat,
  global_store_credit_vat,
  global_store_credit_exc_vat,
  global_adjustment_inc_vat,
  global_adjustment_vat,
  global_adjustment_exc_vat,
  global_total_inc_vat,
  global_total_vat,
  global_total_exc_vat,
  `global_subtotal_cost`,
  `global_shipping_cost`,
  `global_total_cost`,
  `global_margin_amount`,
  prof_fee_percent,
  prof_fee,
  global_prof_fee,
  total_cost,
  vat_rate,
  `vat_percent_before_prof_fee`,
  `discount_percent`,
  country_type,
  country_id,
  vat_logic,
  entity_order_id,
  has_lens,
  vat_done,
  rollup_total,
  rollup_has_lens,
  total_qty,
  total_weight

)
  SELECT
    sl.shipment_id,
    SUM(olv.local_line_subtotal_inc_vat) AS local_subtotal_inc_vat,
    SUM(olv.local_line_subtotal_vat),
    SUM(olv.local_line_subtotal_exc_vat),
    SUM(olv.local_shipping_inc_vat),
    SUM(olv.local_shipping_vat),
    SUM(olv.local_shipping_exc_vat),
    SUM(olv.local_discount_inc_vat),
    SUM(olv.local_discount_vat),
    SUM(olv.local_discount_exc_vat),
    SUM(olv.local_store_credit_inc_vat),
    SUM(olv.local_store_credit_vat),
    SUM(olv.local_store_credit_exc_vat),
    SUM(olv.local_adjustment_inc_vat),
    SUM(olv.local_adjustment_vat),
    SUM(olv.local_adjustment_exc_vat),
    SUM(olv.local_line_total_inc_vat)    AS local_total_inc_vat,
    SUM(olv.local_line_total_vat),
    SUM(olv.local_line_total_exc_vat)    AS local_total_exc_vat,
    SUM(olv.`local_subtotal_cost`),
    SUM(olv.`local_shipping_cost`),
    SUM(olv.`local_total_cost`),
    SUM(olv.`local_margin_amount`),
    SUM(olv.global_line_subtotal_inc_vat),
    SUM(olv.global_line_subtotal_vat),
    SUM(olv.global_line_subtotal_exc_vat),
    SUM(olv.global_shipping_inc_vat),
    SUM(olv.global_shipping_vat),
    SUM(olv.global_shipping_exc_vat),
    SUM(olv.global_discount_inc_vat),
    SUM(olv.global_discount_vat),
    SUM(olv.global_discount_exc_vat),
    SUM(olv.global_store_credit_inc_vat),
    SUM(olv.global_store_credit_vat),
    SUM(olv.global_store_credit_exc_vat),
    SUM(olv.global_adjustment_inc_vat),
    SUM(olv.global_adjustment_vat),
    SUM(olv.global_adjustment_exc_vat),
    SUM(olv.global_line_total_inc_vat)   AS global_total_inc_vat,
    SUM(olv.global_line_total_vat),
    SUM(olv.global_line_total_exc_vat)   AS global_total_exc_vat,
    SUM(olv.`global_subtotal_cost`),
    SUM(olv.`global_shipping_cost`),
    SUM(olv.`global_total_cost`),
    SUM(olv.`global_margin_amount`),
    MAX(olv.prof_fee_percent),
    SUM(olv.local_prof_fee),
    SUM(olv.global_prof_fee),
    SUM(olv.local_total_cost),
    0                                    AS vat_rate,
    MAX(olv.`vat_percent_before_prof_fee`),
    0                                    AS `discount_percent`,
    ''                                   AS country_type,
    ''                                   AS country_id,
    ''                                   AS vat_logic,
    0                                    AS entity_order_id,
    MAX(p.is_lens)                       AS has_lens,
    0                                    AS vat_done,
    0                                    AS rollup_total,
    0                                    AS rollup_has_lens,
    SUM(sl.qty),
    SUM(sl.weight)
  FROM
    shipment_lines_vat olv INNER JOIN dw_shipment_lines sl ON sl.item_id = olv.item_id
    LEFT JOIN dw_products p ON p.product_id = sl.product_id
    INNER JOIN dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
    INNER JOIN dw_updated_customers uc ON uc.customer_id = sh.customer_id
  GROUP BY
    sl.shipment_id;
/** update margins **/
UPDATE
  shipment_headers_vat
SET
  local_margin_percent  = local_margin_amount / local_total_inc_vat,
  global_margin_percent = global_margin_amount / global_total_inc_vat,
  vat_percent           = (local_total_vat) / (local_total_exc_vat),
  discount_percent      = (ABS(local_discount_exc_vat) / (local_total_exc_vat + ABS(local_discount_exc_vat)))
WHERE vat_done = 0;

CREATE TABLE IF NOT EXISTS creditmemo_lines_with_shipment (
  item_id          INT NOT NULL,
  shipment_item_id INT NOT NULL,
  shipment_id      INT NOT NULL,
  PRIMARY KEY (item_id),
  INDEX (shipment_item_id)
);

REPLACE INTO creditmemo_lines_with_shipment
  SELECT DISTINCT
    cl.item_id,
    MIN(sl.item_id)     AS shipment_item_id,
    MIN(sl.shipment_id) AS shipment_id
  FROM dw_creditmemo_lines cl INNER JOIN dw_shipment_lines sl ON sl.order_line_id = cl.order_line_id
    INNER JOIN dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
    INNER JOIN dw_updated_customers uc ON uc.customer_id = sh.customer_id
  GROUP BY cl.item_id;

REPLACE INTO creditmemo_shipment_lines_vat
  SELECT
    cl.item_id,
    `local_prof_fee`,
    `local_price_inc_vat`,
    `local_price_vat`,
    `local_price_exc_vat`,
    `local_line_subtotal_inc_vat`,
    `local_line_subtotal_vat`,
    `local_line_subtotal_exc_vat`,
    `local_shipping_inc_vat`,
    `local_shipping_vat`,
    `local_shipping_exc_vat`,
    `local_discount_inc_vat`,
    `local_discount_vat`,
    `local_discount_exc_vat`,
    `local_store_credit_inc_vat`,
    `local_store_credit_vat`,
    `local_store_credit_exc_vat`,
    `local_adjustment_inc_vat`,
    `local_adjustment_vat`,
    `local_adjustment_exc_vat`,
    `local_line_total_inc_vat`,
    `local_line_total_vat`,
    `local_line_total_exc_vat`,
    `local_subtotal_cost`,
    `local_shipping_cost`,
    `local_total_cost`,
    `local_margin_amount`,
    `local_margin_percent`,
    `local_to_global_rate`,
    ch.`order_currency_code`,
    `global_prof_fee`,
    `global_price_inc_vat`,
    `global_price_vat`,
    `global_price_exc_vat`,
    `global_line_subtotal_inc_vat`,
    `global_line_subtotal_vat`,
    `global_line_subtotal_exc_vat`,
    `global_shipping_inc_vat`,
    `global_shipping_vat`,
    `global_shipping_exc_vat`,
    `global_discount_inc_vat`,
    `global_discount_vat`,
    `global_discount_exc_vat`,
    `global_store_credit_inc_vat`,
    `global_store_credit_vat`,
    `global_store_credit_exc_vat`,
    `global_adjustment_inc_vat`,
    `global_adjustment_vat`,
    `global_adjustment_exc_vat`,
    `global_line_total_inc_vat`,
    `global_line_total_vat`,
    `global_line_total_exc_vat`,
    `global_subtotal_cost`,
    `global_shipping_cost`,
    `global_total_cost`,
    `global_margin_amount`,
    `global_margin_percent`,
    `prof_fee_percent`,
    `vat_percent_before_prof_fee`,
    `vat_percent`,
    olv.`discount_percent`,
    vat_done,
    olv.qty,
    ol.weight * olv.qty AS row_weight,
    cl.shipment_item_id,
    cl.shipment_id
  FROM
    creditmemo_lines_vat olv INNER JOIN creditmemo_lines_with_shipment cl ON cl.item_id = olv.item_id
    INNER JOIN dw_creditmemo_lines cl2 ON cl2.item_id = olv.item_id
    INNER JOIN dw_order_lines ol ON ol.item_id = cl2.order_line_id
    INNER JOIN dw_creditmemo_headers ch ON ch.creditmemo_id = cl2.creditmemo_id
    INNER JOIN dw_order_headers doh ON ch.order_id = doh.order_id
    INNER JOIN dw_updated_customers uc ON uc.customer_id = doh.customer_id;

CREATE TABLE IF NOT EXISTS creditmemo_shipment_headers_vat (
    creditmemo_id INT NOT NULL,
    local_subtotal_inc_vat DECIMAL(12,4),
    local_subtotal_vat DECIMAL(12,4),
    local_subtotal_exc_vat DECIMAL(12,4),
    local_shipping_inc_vat DECIMAL(12,4),
    local_shipping_vat DECIMAL(12,4),
    local_shipping_exc_vat DECIMAL(12,4),
    local_discount_inc_vat DECIMAL(12,4),
    local_discount_vat DECIMAL(12,4),
    local_discount_exc_vat DECIMAL(12,4),
    local_store_credit_inc_vat DECIMAL(12,4),
    local_store_credit_vat DECIMAL(12,4),
    local_store_credit_exc_vat DECIMAL(12,4),
    local_adjustment_inc_vat DECIMAL(12,4),
    local_adjustment_vat DECIMAL(12,4),
    local_adjustment_exc_vat DECIMAL(12,4),
    local_total_inc_vat DECIMAL(12,4),
    local_total_vat DECIMAL(12,4),
    local_total_exc_vat DECIMAL(12,4),
    `local_subtotal_cost` DECIMAL(12,4),
    `local_shipping_cost` DECIMAL(12,4),
    `local_total_cost` DECIMAL(12,4),
    `local_margin_amount` DECIMAL(12,4),
    `local_margin_percent` DECIMAL(12,4),
    global_subtotal_inc_vat DECIMAL(12,4),
    global_subtotal_vat DECIMAL(12,4),
    global_subtotal_exc_vat DECIMAL(12,4),
    global_shipping_inc_vat DECIMAL(12,4),
    global_shipping_vat DECIMAL(12,4),
    global_shipping_exc_vat DECIMAL(12,4),
    global_discount_inc_vat DECIMAL(12,4),
    global_discount_vat DECIMAL(12,4),
    global_discount_exc_vat DECIMAL(12,4),
    global_store_credit_inc_vat DECIMAL(12,4),
    global_store_credit_vat DECIMAL(12,4),
    global_store_credit_exc_vat DECIMAL(12,4),
    global_adjustment_inc_vat DECIMAL(12,4),
    global_adjustment_vat DECIMAL(12,4),
    global_adjustment_exc_vat DECIMAL(12,4),
    global_total_inc_vat DECIMAL(12,4),
    global_total_vat DECIMAL(12,4),
    global_total_exc_vat DECIMAL(12,4),
    `global_subtotal_cost` DECIMAL(12,4),
    `global_shipping_cost` DECIMAL(12,4),
    `global_total_cost` DECIMAL(12,4),
    `global_margin_amount` DECIMAL(12,4),
    `global_margin_percent` DECIMAL(12,4),
    prof_fee_percent DECIMAL(12,4),
    prof_fee DECIMAL(12,4),
    global_prof_fee DECIMAL(12,4),
    total_cost DECIMAL(12,4),
    vat_percent DECIMAL(12,4),
    vat_rate DECIMAL(12,4),
    `vat_percent_before_prof_fee` DECIMAL(12,4),
    `discount_percent` DECIMAL(12,4),
    country_type  VARCHAR(25),
    country_id  VARCHAR(25),
    vat_logic VARCHAR(25),
    entity_order_id INT,
    has_lens INT,
    vat_done INT,
    rollup_total DECIMAL(12,4),
    rollup_has_lens DECIMAL(12,4),
    total_qty INT,
    total_weight INT,
    shipment_id INT,
    PRIMARY KEY(creditmemo_id),
    INDEX(shipment_id)
);
REPLACE INTO creditmemo_shipment_headers_vat
SELECT
    cl.creditmemo_id,
    SUM(olv.local_line_subtotal_inc_vat) AS local_subtotal_inc_vat,
    SUM(olv.local_line_subtotal_vat) AS local_subtotal_vat,
    SUM(olv.local_line_subtotal_exc_vat) AS local_subtotal_exc_vat ,
    SUM(olv.local_shipping_inc_vat) AS local_shipping_inc_vat,
    SUM(olv.local_shipping_vat) AS local_shipping_vat ,
    SUM(olv.local_shipping_exc_vat) AS local_shipping_exc_vat,
    SUM(olv.local_discount_inc_vat) AS local_discount_inc_vat,
    SUM(olv.local_discount_vat) AS local_discount_vat,
    SUM(olv.local_discount_exc_vat) AS local_discount_exc_vat,
    SUM(olv.local_store_credit_inc_vat) AS local_store_credit_inc_vat,
    SUM(olv.local_store_credit_vat) AS local_store_credit_vat,
    SUM(olv.local_store_credit_exc_vat) AS local_store_credit_exc_vat,
    SUM(olv.local_adjustment_inc_vat) AS local_adjustment_inc_vat,
    SUM(olv.local_adjustment_vat) AS local_adjustment_vat,
    SUM(olv.local_adjustment_exc_vat) AS local_adjustment_exc_vat,
    SUM(olv.local_line_total_inc_vat) AS local_total_inc_vat ,
    SUM(olv.local_line_total_vat) AS local_total_vat,
    SUM(olv.local_line_total_exc_vat) AS local_total_exc_vat ,
    SUM(olv.`local_subtotal_cost`) AS local_subtotal_cost,
    SUM(olv.`local_shipping_cost`) AS local_shipping_cost,
    SUM(olv.`local_total_cost`) AS local_total_cost,
    SUM(olv.`local_margin_amount`) AS local_margin_amount,
    0 AS local_margin_percent,
    SUM(olv.global_line_subtotal_inc_vat) AS global_subtotal_inc_vat,
    SUM(olv.global_line_subtotal_vat) AS global_subtotal_vat,
    SUM(olv.global_line_subtotal_exc_vat) AS global_subtotal_exc_vat,
    SUM(olv.global_shipping_inc_vat) AS global_shipping_inc_vat,
    SUM(olv.global_shipping_vat) AS global_shipping_vat,
    SUM(olv.global_shipping_exc_vat) AS global_shipping_exc_vat ,
    SUM(olv.global_discount_inc_vat) AS global_discount_inc_vat,
    SUM(olv.global_discount_vat) AS global_discount_vat,
    SUM(olv.global_discount_exc_vat) AS global_discount_exc_vat,
    SUM(olv.global_store_credit_inc_vat) AS global_store_credit_inc_vat,
    SUM(olv.global_store_credit_vat) AS global_store_credit_vat,
    SUM(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat,
    SUM(olv.global_adjustment_inc_vat) AS global_adjustment_inc_vat,
    SUM(olv.global_adjustment_vat) AS global_adjustment_vat,
    SUM(olv.global_adjustment_exc_vat) AS global_adjustment_exc_vat,
    /**SUM(olv.global_line_total_inc_vat + IFNULL(olv.global_shipping_inc_vat,0) + IFNULL(-1 * ABS(olv.global_discount_inc_vat),0) + IFNULL(olv.global_adjustment_inc_vat,0)) AS global_total_inc_vat ,**/
    SUM(olv.global_line_total_inc_vat) AS global_total_inc_vat,
    SUM(olv.global_line_total_vat) AS global_total_vat,
    /**SUM(olv.global_line_total_exc_vat + IFNULL(olv.global_shipping_exc_vat,0) + IFNULL(-1 * ABS(olv.global_discount_exc_vat),0) + IFNULL(olv.global_adjustment_exc_vat,0)) AS global_total_exc_vat ,**/
    SUM(olv.`global_line_total_exc_vat`) AS global_total_exc_vat,
    SUM(olv.`global_subtotal_cost`) AS global_subtotal_cost,
    SUM(olv.`global_shipping_cost`) AS global_shipping_cost,
    SUM(olv.`global_total_cost`) AS global_total_cost,
    SUM(olv.`global_margin_amount`) AS global_margin_amount,
    0 AS global_margin_percent,
    SUM(olv.local_prof_fee) AS prof_fee_percent,
    SUM(olv.local_prof_fee) AS prof_fee,
    SUM(olv.global_prof_fee) AS global_prof_fee,
    SUM(olv.local_total_cost) AS total_cost,
    MAX(olv.vat_percent) AS vat_percent,
    0 AS vat_rate ,
    0 AS `vat_percent_before_prof_fee`,
    0 AS `discount_percent`,
    '' AS country_type,
    '' AS country_id ,
    '' AS vat_logic ,
    0 AS entity_order_id ,
    MAX(p.is_lens) AS has_lens ,
    0 AS vat_done ,
    0 AS rollup_total ,
    0 AS rollup_has_lens,
    SUM(cl.qty) AS total_qty,
    SUM(olv.row_weight) AS total_weight,
    MAX(olv.shipment_id) AS shipment_id
    FROM
      creditmemo_shipment_lines_vat olv INNER JOIN dw_creditmemo_lines cl ON cl.item_id = olv.item_id
      LEFT JOIN dw_products p ON p.product_id = cl.product_id
      INNER JOIN dw_creditmemo_headers ch ON ch.creditmemo_id = cl.creditmemo_id
      INNER JOIN dw_order_headers doh ON ch.order_id = doh.order_id
      INNER JOIN dw_updated_customers uc ON uc.customer_id = doh.customer_id
    GROUP BY
      cl.creditmemo_id;
/** update margins and percentages **/
UPDATE
  creditmemo_shipment_headers_vat
SET
  local_margin_percent  = local_margin_amount / local_total_inc_vat,
  global_margin_percent = global_margin_amount / global_total_inc_vat,
  global_margin_percent = global_margin_amount / global_total_inc_vat,
  vat_percent           = (local_total_vat) / (local_total_exc_vat),
  discount_percent      = -1 * (ABS(local_discount_exc_vat) / (local_total_exc_vat + ABS(local_discount_exc_vat)))
WHERE vat_done = 0;

UPDATE creditmemo_shipment_headers_vat
SET vat_done = 1 WHERE vat_done = 0 ;

UPDATE shipment_headers_vat
SET vat_done = 1 WHERE vat_done = 0;
SQL;


		}
		$this->runQuery($sql);
	}

	function createCalEntities()
	{
		$this->buildCalTables();
	}

	public function buildCalTables()
	{
		$sql = <<< SQL
        DROP TABLE IF EXISTS dw_invoice_calender_dates_staging;
        CREATE TABLE IF NOT EXISTS dw_invoice_calender_dates_staging (
            invoice_id INT NOT NULL,
            order_id INT NOT NULL,
            created_at DATETIME,
            order_created_at DATETIME,
            length_of_time_to_invoice INT,
            KEY(invoice_id),
            KEY(order_id),
            KEY(created_at),
            KEY(order_created_at),
            KEY(length_of_time_to_invoice),
            PRIMARY KEY (order_id)
        );

		CREATE TABLE IF NOT EXISTS dw_invoice_calender_dates (
            invoice_id INT NOT NULL,
            order_id INT NOT NULL,
            created_at DATETIME,
            order_created_at DATETIME,
            length_of_time_to_invoice INT,
            KEY(invoice_id),
            KEY(order_id),
            KEY(created_at),
            KEY(order_created_at),
            KEY(length_of_time_to_invoice),
            PRIMARY KEY (order_id)
        );

        REPLACE INTO dw_invoice_calender_dates_staging (
                invoice_id,
                order_id,
                created_at,
                order_created_at
            )
        SELECT
            ih.invoice_id,
            oh.order_id,
            ih.created_at AS created_at,
            oh.created_at AS order_created_at
        FROM
            dw_invoice_headers ih
                        INNER JOIN 	dw_order_headers oh ON oh.order_id = ih.order_id
                        INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id;

		UPDATE `dw_invoice_calender_dates_staging` SET `length_of_time_to_invoice` = DATEDIFF(created_at,order_created_at);
		REPLACE INTO dw_invoice_calender_dates SELECT * FROM dw_invoice_calender_dates_staging;

        DROP TABLE IF EXISTS dw_shipment_calender_dates_staging;

        CREATE TABLE IF NOT EXISTS dw_shipment_calender_dates_staging (
            shipment_id INT NOT NULL,
            order_id INT NOT NULL,
            created_at DATETIME,
            order_created_at DATETIME,
            invoiced_at DATETIME,
            length_of_time_to_first_invoice INT,
            length_of_time_from_first_invoice_to_this_shipment INT,
            KEY(shipment_id),
            KEY(order_id),
            KEY(created_at),
            KEY(order_created_at),
            KEY(invoiced_at),
            KEY(created_at,invoiced_at),
            KEY(created_at,order_created_at),
            PRIMARY KEY (shipment_id)
        );

		CREATE TABLE IF NOT EXISTS dw_shipment_calender_dates (
            shipment_id INT NOT NULL,
            order_id INT NOT NULL,
            created_at DATETIME,
            order_created_at DATETIME,
            invoiced_at DATETIME,
            length_of_time_to_first_invoice INT,
            length_of_time_from_first_invoice_to_this_shipment INT,
            KEY(shipment_id),
            KEY(order_id),
            KEY(created_at),
            KEY(order_created_at),
            KEY(invoiced_at),
            KEY(created_at,invoiced_at),
            KEY(created_at,order_created_at),
            PRIMARY KEY (shipment_id)
        );

        REPLACE INTO dw_shipment_calender_dates_staging (
                shipment_id,
                order_id,
                created_at,
                order_created_at,
                invoiced_at
            )
        SELECT
            sh.shipment_id,
            oh.order_id,
            sh.created_at AS created_at,
            oh.created_at AS order_created_at,
            inv.invoiced_at AS invoiced_at
        FROM
            dw_shipment_headers sh
                        INNER JOIN 	dw_order_headers oh ON oh.order_id = sh.order_id
                        LEFT JOIN	first_inv inv ON inv.order_id = oh.order_id
                        INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id;

        UPDATE `dw_shipment_calender_dates_staging` SET `length_of_time_to_first_invoice` = DATEDIFF(invoiced_at,order_created_at);
        UPDATE `dw_shipment_calender_dates_staging` SET `length_of_time_from_first_invoice_to_this_shipment` = DATEDIFF(created_at,invoiced_at);

        REPLACE INTO dw_shipment_calender_dates SELECT * FROM dw_shipment_calender_dates_staging;

        DROP TABLE IF EXISTS dw_qty_backordered;
        CREATE TABLE IF NOT EXISTS  dw_qty_backordered (
            order_line_id INT NOT NULL,
            shipment_item_id INT NOT NULL,
            qty_prior DECIMAL(12,4),
            PRIMARY KEY(shipment_item_id)
        )
        AS
        SELECT 	sl.order_line_id,sl.
            item_id shipment_item_id,
            SUM(sl2.qty) qty_prior
        FROM 	dw_shipment_lines sl	INNER JOIN dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
                        INNER JOIN dw_shipment_headers sh2 ON sh2.order_id = sh.order_id AND sh2.shipment_id < sh.shipment_id
                        INNER JOIN dw_shipment_lines sl2 ON sh2.shipment_id = sl2.shipment_id AND sl.order_line_id = sl2.order_line_id
        GROUP BY sl.order_line_id,sl.item_id;
SQL;
		$this->runQuery($sql);

	}


	/*
     *
     * Create table structure for vat values for each entity
     *
     * name: buildVatHeaderTable
     * @param
     * @return
     *
     */

	/**
	 * Add records to config table to show that build procedure has begun will be updated when finished to show that build ran
	 * succesffily
	 */
	function prepSync()
	{
		$this->clearLog();//use a table to log query times
		$this->runQuery("CREATE TABLE IF NOT EXISTS dw_sync_config(
            config_key VARCHAR(30),
            value VARCHAR(30),
            PRIMARY KEY(config_key)
        );

        REPLACE INTO dw_sync_config VALUES ('orders_sync_from','2012.11.01 00:00:00');
        REPLACE INTO dw_sync_config VALUES ('invoices_sync_from','2012.11.01 00:00:00');
        REPLACE INTO dw_sync_config VALUES ('shipments_sync_from','2012.11.01 00:00:00');
        REPLACE INTO dw_sync_config VALUES ('finished_sync',0);
        REPLACE INTO dw_sync_config VALUES ('finished_ga_sync',0);
        ");
	}


	/*
     * Update the VAT value for each entity as well as any releavent monetary columns
     *
     * name: updateVatValues
     * @param
     * @return
     *
     */

	function clearLog()
	{
		$write = Mage::getSingleton('core/resource')->getConnection('dwflatreplica_write');
		$write->query("DROP TABLE IF EXISTS dw_log_query_time;");
	}

	/*
     * Use tier price table to generate information on product prices including vat fields using a specified current vat rate
     *
     * name: buildProductPriceTable
     * @param
     * @return
     *
     */

	public function completeGASync()
	{
		$this->runQuery("REPLACE INTO dw_sync_config VALUES ('finished_ga_sync',1);");
	}

	/*
     * Create important supporting tables that are needed to populate other dw tables
     *
     * name: buildExtraTables
     * @param
     * @return
     *
     */

	function completeSync()
	{
		$this->runQuery("REPLACE INTO dw_sync_config VALUES ('finished_sync',1);");
	}

	/*
     * Some entities have no line items because adjustments are used leaving no values on the item level
     * this function runs query to add such ros in theses case
     *
     * name: addMissingLineItems
     * @param
     * @return
     *
     */

	function addBisChannel($data)
	{
		$write = Mage::getSingleton('core/resource')->getConnection('dwflatreplica_write');
		if ($write->showTableStatus('dw_bis_source_map') !== false) {
			$write->insertOnDuplicate('dw_bis_source_map', $data, array_keys($data));
		}
	}

	function addDeletedShipment($shipmentId)
	{
		if (!$shipmentId) {
			return;
		}
		$this->_getWriteAdapter()->insertOnDuplicate('dw_deleted_shipment', array('shipment_id' => $shipmentId, 'deleted_date' => date('Y-m-d H:i:s')));
	}

	public function buildProductPriceTable()
	{
		$sql = <<< SQL
DROP TABLE IF EXISTS dw_product_prices;
CREATE TABLE IF NOT EXISTS `dw_product_prices` (
  `price_id`                    int(11) AUTO_INCREMENT NOT NULL,
  `store_name`                  varchar(255)           NOT NULL,
  `product_id`                  int(11)                NOT NULL,
  `qty`                         decimal(12, 4)         NOT NULL,
  `pack_size`                   decimal(12, 4)         NOT NULL,
  `local_unit_price_inc_vat`    decimal(16, 8)         NOT NULL,
  `local_qty_price_inc_vat`     decimal(12, 4)         NOT NULL,
  `local_qty_price_exc_vat`     decimal(12, 4)     DEFAULT NULL,
  `local_pack_price_inc_vat`    decimal(12, 4)         NOT NULL,
  `local_prof_fee`              decimal(12, 4)     DEFAULT NULL,
  `local_product_cost`          decimal(12, 4)     DEFAULT NULL,
  `local_shipping_cost`         decimal(12, 4)     DEFAULT NULL,
  `local_total_cost`            decimal(12, 4)     DEFAULT NULL,
  `local_margin_amount`         decimal(12, 4)     DEFAULT NULL,
  `local_margin_percent`        decimal(12, 4)     DEFAULT NULL,
  `local_to_global_rate`        decimal(12, 4)     DEFAULT NULL,
  `store_currency_code`         varchar(4)
                                CHARACTER SET utf8 DEFAULT NULL,
  local_price_vat               decimal(12, 4)     DEFAULT NULL,
  `global_unit_price_inc_vat`   decimal(16, 8)         NOT NULL,
  `global_qty_price_inc_vat`    decimal(12, 4)         NOT NULL,
  `global_pack_price_inc_vat`   decimal(12, 4)         NOT NULL,
  `global_prof_fee`             decimal(12, 4)     DEFAULT NULL,
  `global_product_cost`         decimal(12, 4)     DEFAULT NULL,
  `global_shipping_cost`        decimal(12, 4)     DEFAULT NULL,
  `global_total_cost`           decimal(12, 4)     DEFAULT NULL,
  `global_margin_amount`        decimal(12, 4)     DEFAULT NULL,
  `global_margin_percent`       decimal(12, 4)     DEFAULT NULL,
  global_price_vat              decimal(12, 4)     DEFAULT NULL,
  `prof_fee_percent`            decimal(12, 4)     DEFAULT NULL,
  `vat_percent_before_prof_fee` decimal(12, 4)     DEFAULT NULL,
  `vat_percent`                 decimal(12, 4)     DEFAULT NULL,
  `store_id`                    INT                DEFAULT NULL,
  lcoal_vat_amount              decimal(12, 4)     DEFAULT NULL,
  PRIMARY KEY (`price_id`),
  UNIQUE (qty, product_id, store_id),
  KEY `product_id` (`product_id`),
  KEY `store_name` (`store_name`)
)
  ENGINE =MyISAM
  DEFAULT CHARSET =latin1;


/** create a flat version of config just like with prices**/
DROP TABLE IF EXISTS dw_core_config_data;
CREATE TABLE IF NOT EXISTS `dw_core_config_data` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `store_id`  int(11)          NOT NULL DEFAULT '0',
  `path`      varchar(255)     NOT NULL DEFAULT 'general',
  `value`     text             NOT NULL,
  PRIMARY KEY (`config_id`),
  INDEX(`path`),
  UNIQUE KEY `con   fig_scope` (`store_id`, `path`)
);
INSERT INTO dw_core_config_data
(
  store_id,
  path,
  value
)
  SELECT
    cs.store_id,
    cd.path,
    cd.value
  FROM {$this->dbname}.core_config_data cd
CROSS JOIN {$this->dbname}.core_store cs
WHERE cd.scope_id = 0 AND cs.store_id != 0;


REPLACE INTO dw_core_config_data
(
  store_id,
  path,
  value
)
  SELECT
    cd.scope_id,
    cd.path,
    cd.value
  FROM {$this->dbname}.core_config_data cd
WHERE cd.scope = 'websites';

REPLACE INTO dw_core_config_data
(
  store_id,
  path,
  value
)
  SELECT
    cd.scope_id,
    cd.path,
    cd.value
  FROM {$this->dbname}.core_config_data cd
WHERE cd.scope = 'stores';

DROP TABLE IF EXISTS dw_conf_presc_required;
CREATE TABLE IF NOT EXISTS `dw_conf_presc_required` (
  `config_id` INT(10) UNSIGNED NOT NULL,
  `store_id`  INT(11)          NOT NULL,
  `path`      VARCHAR(255)     NOT NULL,
  `value`     TEXT             NOT NULL,
  INDEX (store_id)
)
  AS SELECT *
     FROM dw_core_config_data
     WHERE path = 'prescription/settings/enable';

/** TODO: get this from conifg or someting **/
DROP TABLE IF EXISTS dw_current_vat;
CREATE TABLE IF NOT EXISTS dw_current_vat (
  store_id            INT NOT NULL,
  prof_fee_rate       decimal(12, 4),
  vat_rate            decimal(12, 4),
  shipping_cost       decimal(12, 4),
  base_currency       varchar(10),
  base_to_global_rate decimal(12, 4),
  PRIMARY KEY (store_id)
);

REPLACE INTO dw_current_vat (store_id) SELECT store_id
                                       FROM {$this->dbname}.core_store WHERE store_id != 0;
UPDATE dw_current_vat cv, dw_core_config_data cd
SET cv.base_currency = cd.value
WHERE cd.path = 'currency/options/base' AND cv.store_id = cd.store_id;
UPDATE dw_current_vat cv, {$this->dbname}.directory_currency_rate cr SET cv.base_to_global_rate = cr.rate
WHERE cv.base_currency = cr.currency_from AND currency_to = 'GBP';
/**will problably come from config too **/
UPDATE dw_current_vat
set prof_fee_rate = 0.18;
UPDATE dw_current_vat
set prof_fee_rate = 0
WHERE store_id IN (11, 19);
UPDATE dw_current_vat
set prof_fee_rate = 0
WHERE store_id IN (11, 19);
UPDATE dw_current_vat
set vat_rate = 1.20;
UPDATE dw_current_vat
set vat_rate = 1.21
WHERE store_id IN (5);
UPDATE dw_current_vat
set vat_rate = 1.23
WHERE store_id IN (3);
UPDATE dw_current_vat
set prof_fee_rate = 0.5
WHERE store_id IN (3);
UPDATE dw_current_vat
set vat_rate = 1.19
WHERE store_id IN (6);
UPDATE dw_current_vat
set shipping_cost = 0;


/** do default first and then add on ofter convert default to a list of all stores**/
/** default go in first then overide**/
INSERT INTO dw_product_prices
(
  store_name,
  product_id,
  qty,
  pack_size,
  local_unit_price_inc_vat,
  local_qty_price_inc_vat,
  local_pack_price_inc_vat,
  local_product_cost,
  local_total_cost,
  store_id
)
  SELECT
    cs.name,
    tp.entity_id,
    tp.qty,
    GREATEST(1, CAST(p.packsize AS UNSIGNED)),
    tp.value,
    tp.qty * tp.value,
    GREATEST(1, CAST(p.packsize AS UNSIGNED)) * tp.value,
    p.cost,
    p.cost * tp.qty,
    cs.store_id
  FROM {$this->dbname}.catalog_product_entity_tier_price tp
INNER JOIN {$this->dbname}.core_store cs1 ON cs1.website_id = tp.website_id
CROSS JOIN {$this->dbname}.core_store cs
INNER JOIN dw_products_all_stores p ON p.product_id = tp.entity_id AND cs.store_id = p.store_id
INNER JOIN {$this->dbname}.catalog_product_website cpw ON cpw.product_id = p.product_id AND cpw.website_id = p.store_id
WHERE tp.website_id = 0 AND cs.store_id != 0;

/** replace with these to fill gaps**/
REPLACE INTO dw_product_prices
(
  store_name,
  product_id,
  qty,
  pack_size,
  local_unit_price_inc_vat,
  local_qty_price_inc_vat,
  local_pack_price_inc_vat,
  local_product_cost,
  local_total_cost,
  global_product_cost,
  global_total_cost,
  store_id
)
  SELECT
    cs.name,
    tp.entity_id,
    tp.qty,
    GREATEST(1, CAST(p.packsize AS UNSIGNED)),
    tp.value,
    tp.qty * tp.value,
    GREATEST(1, CAST(p.packsize AS UNSIGNED)) * tp.value,
    p.cost,
    p.cost * tp.qty,
    p.cost,
    p.cost * tp.qty,
    cs.store_id
  FROM {$this->dbname}.catalog_product_entity_tier_price tp
INNER JOIN {$this->dbname}.core_store cs ON cs.website_id = tp.website_id
INNER JOIN dw_products_all_stores p ON p.product_id = tp.entity_id AND cs.store_id = p.store_id
INNER JOIN {$this->dbname}.catalog_product_website cpw ON cpw.product_id = p.product_id AND cpw.website_id = p.store_id
WHERE tp.website_id != 0;
/** fill in prices **/
UPDATE
    dw_product_prices p, dw_current_vat cv
SET
  p.local_prof_fee       = local_qty_price_inc_vat * cv.prof_fee_rate,
  p.prof_fee_percent     = cv.prof_fee_rate,
  p.store_currency_code  = cv.base_currency,
  p.local_to_global_rate = cv.base_to_global_rate
WHERE
  cv.store_id = p.store_id;

UPDATE
    dw_product_prices p, dw_current_vat cv
SET

  p.local_price_vat         = (p.local_qty_price_inc_vat) -
                              ((p.local_qty_price_inc_vat - p.local_prof_fee) / cv.vat_rate),
  p.local_qty_price_exc_vat = ((p.local_qty_price_inc_vat - p.local_prof_fee) / cv.vat_rate),
  p.local_product_cost      = local_product_cost * (1 / local_to_global_rate)
WHERE
  cv.store_id = p.store_id;

UPDATE
    dw_product_prices p, dw_current_vat cv
SET
  p.local_shipping_cost = cv.shipping_cost
WHERE
  cv.store_id = p.store_id;

/**half shipping cost if lens**/
UPDATE
    dw_product_prices pp, dw_products p, dw_current_vat cv
SET
  local_shipping_cost = (local_shipping_cost / 2) * (1 / local_to_global_rate)
WHERE
  pp.product_id = p.product_id AND p.is_lens = 1 AND cv.store_id = pp.store_id;

UPDATE
  dw_product_prices p
SET
  p.local_total_cost = local_total_cost + local_shipping_cost;

UPDATE
    dw_product_prices p, dw_current_vat cv
SET
  p.local_margin_amount = (p.local_qty_price_exc_vat) - (local_total_cost)
WHERE
  cv.store_id = p.store_id;

UPDATE
    dw_product_prices p, dw_current_vat cv
SET
  p.vat_percent_before_prof_fee = cv.vat_rate - 1,
  p.local_margin_percent        = p.local_margin_amount / p.local_qty_price_inc_vat,
  p.vat_percent                 = local_price_vat / local_qty_price_exc_vat
WHERE
  cv.store_id = p.store_id;

UPDATE
  dw_product_prices p
SET
  p.global_unit_price_inc_vat = p.local_unit_price_inc_vat * p.local_to_global_rate,
  p.global_qty_price_inc_vat  = p.local_qty_price_inc_vat * p.local_to_global_rate,
  p.global_pack_price_inc_vat = p.local_pack_price_inc_vat * p.local_to_global_rate,
  p.global_prof_fee           = p.local_prof_fee * p.local_to_global_rate,
  p.global_shipping_cost      = p.local_shipping_cost * p.local_to_global_rate,
  p.global_margin_amount      = p.local_margin_amount * p.local_to_global_rate,
  p.global_price_vat          = p.local_price_vat * p.local_to_global_rate;

UPDATE
  dw_product_prices p
SET
  p.global_margin_percent = p.global_margin_amount / p.global_qty_price_inc_vat;
SQL;
		$this->runQuery($sql);

	}

	public function buildExtraTables()
	{
		$sql = <<< SQL
/** sometimes reorder quote has mutliple payments attached take latest one**/
DROP TABLE IF EXISTS max_reorder_quote_payment;
CREATE TABLE IF NOT EXISTS max_reorder_quote_payment (
  quote_id   INT,
  payment_id INT,
  PRIMARY KEY (quote_id, payment_id)
)
  AS
    SELECT
      quote_id,
      MAX(payment_id) payment_id
    FROM {$this->dbname}.po_reorder_quote_payment
GROUP BY quote_id;
/** correct kids glasses flagged as solutions**/
UPDATE dw_products p, dw_glasses_products g
SET p.product_type = 'glasses'
WHERE p.product_id = g.product_id;

CREATE TABLE IF NOT EXISTS order_status_history_agg (
  order_id    INT NOT NULL,
  status      varchar(25),
  status_time datetime,
  KEY (status),
  PRIMARY KEY (order_id, status)
);

REPLACE INTO order_status_history_agg
SELECT
      eh.order_id as     order_id,
      eh.status,
      MIN(eh.updated_at) status_time
    FROM
  {$this->dbname}.sales_order_log eh
  INNER JOIN dw_order_headers doh ON eh.order_id = doh.order_id
  INNER JOIN dw_updated_customers uc ON uc.customer_id = doh.customer_id
WHERE
eh.status IN ('canceled', 'complete', 'shipped')
GROUP BY
  order_id,
status;
DROP TABLE IF EXISTS edi_stock_item_agg;
CREATE TABLE IF NOT EXISTS edi_stock_item_agg (
  product_id   INT NOT NULL,
  product_code VARCHAR(275),
  BC           VARCHAR(100),
  PO           VARCHAR(100),
  CY           VARCHAR(100),
  DO           VARCHAR(100),
  CO           VARCHAR(100),
  DI           VARCHAR(100),
  AD           VARCHAR(100),
  AX           VARCHAR(100),
  PRIMARY KEY (product_code)
)
  AS
    SELECT
      MAX(product_id) product_id,
      product_code,
      MAX(BC)         BC,
      MAX(PO)         PO,
      MAX(CY)         CY,
      MAX(DO)         DO,
      MAX(CO)         CO,
      MAX(DI)         DI,
      MAX(AD)         AD,
      MAX(AX)         AX
    FROM
  {$this->dbname}.edi_stock_item
WHERE
product_code IS NOT NULL
GROUP BY
  product_code;


CREATE TABLE IF NOT EXISTS first_inv (
  order_id    INT,
  invoice_id  int,
  invoiced_at DATETIME,
  PRIMARY KEY (order_id)
);

DROP TABLE IF EXISTS first_inv_staging;
CREATE TABLE IF NOT EXISTS first_inv_staging (
  order_id    INT,
  invoice_id  int,
  invoiced_at DATETIME,
  PRIMARY KEY (order_id)
)
AS
    SELECT
      ih.order_id,
      MIN(ih.invoice_id) as invoice_id,
      MIN(ih.created_at) AS invoiced_at
    FROM
      dw_invoice_headers ih
      INNER JOIN dw_order_headers doh ON ih.order_id = doh.order_id
      INNER JOIN dw_updated_customers uc ON uc.customer_id = doh.customer_id
    GROUP BY
      ih.order_id;

REPLACE INTO first_inv SELECT * FROM first_inv_staging;

CREATE TABLE IF NOT EXISTS first_ship (
  order_id   INT,
  shipped_at DATETIME,
  PRIMARY KEY (order_id)
);

DROP TABLE IF EXISTS first_ship_staging;
CREATE TABLE IF NOT EXISTS first_ship_staging (
  order_id   INT,
  shipped_at DATETIME,
  PRIMARY KEY (order_id)
)
  AS
    SELECT
      sh.order_id,
      MIN(sh.created_at) AS shipped_at
    FROM
      dw_shipment_headers sh
      INNER JOIN dw_order_headers doh ON sh.order_id = doh.order_id
      INNER JOIN dw_updated_customers uc ON uc.customer_id = doh.customer_id
    GROUP BY
      sh.order_id;

REPLACE INTO first_ship SELECT * FROM first_ship_staging;

CREATE TABLE IF NOT EXISTS last_full_ship (
  order_id   INT,
  shipped_at DATETIME,
  PRIMARY KEY (order_id)
);

DROP TABLE IF EXISTS last_full_ship_staging;
CREATE TABLE IF NOT EXISTS last_full_ship_staging (
  order_id   INT,
  shipped_at DATETIME,
  PRIMARY KEY (order_id)
)
  AS
    SELECT
      sh.order_id,
      MAX(sh.created_at) AS shipped_at
    FROM
      dw_shipment_headers sh
      INNER JOIN dw_order_headers doh ON sh.order_id = doh.order_id
      INNER JOIN dw_updated_customers uc ON uc.customer_id = doh.customer_id
    WHERE sh.order_id IN (select sub_osh.order_id
                       FROM order_status_history_agg sub_osh
                       INNER JOIN dw_order_headers sub_doh ON sub_osh.order_id = sub_doh.order_id
      					INNER JOIN dw_updated_customers sub_uc ON sub_uc.customer_id = sub_doh.customer_id
                       WHERE sub_osh.status = 'shipped')
	GROUP BY sh.order_id;

REPLACE INTO last_full_ship SELECT * FROM last_full_ship_staging;

CREATE TABLE IF NOT EXISTS last_full_payment (
  order_id     INT,
  payment_time DATETIME,
  PRIMARY KEY (order_id)
);

DROP TABLE IF EXISTS last_full_payment_staging;
CREATE TABLE IF NOT EXISTS last_full_payment_staging (
  order_id     INT,
  payment_time DATETIME,
  PRIMARY KEY (order_id)
)
  AS
    SELECT
      ih.order_id,
      MAX(ih.created_at) AS payment_time
    FROM
      dw_invoice_headers ih INNER JOIN dw_order_headers oh ON ih.order_id = oh.order_id
      INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id
    WHERE
      oh.base_total_paid = oh.base_grand_total
    GROUP BY
      oh.order_id;

REPLACE INTO last_full_payment SELECT * FROM last_full_payment_staging;

/**product suporting tables**/
/** flag products in free samples category as promotional**/
UPDATE `dw_products`
SET `promotional_product` = 1
WHERE product_id IN (SELECT cpc.product_id
                     FROM {$this->dbname}.`catalog_category_product` cpc WHERE category_id = 242);


/**
* get category path to a max depth of 7 parent categories
* match magento category structure to known category hierarchy for magento found in dw_flat_category_map
* build a product category relation based on this note this is dependant on categor names on default magetno store
**/
DROP TABLE IF EXISTS dw_category_path;
CREATE TABLE IF NOT EXISTS dw_category_path
    SELECT
      d1.category_id,
      d1.name name1,
      d2.name name2,
      d3.name name3,
      d4.name name4,
      d5.name name5,
      d6.name name6,
      d7.name name7,
      d8.name name8
    FROM dw_categories d1 LEFT JOIN dw_categories d2 ON d1.parent_category_id = d2.category_id
      LEFT JOIN dw_categories d3 ON d2.parent_category_id = d3.category_id
      LEFT JOIN dw_categories d4 ON d3.parent_category_id = d4.category_id
      LEFT JOIN dw_categories d5 ON d4.parent_category_id = d5.category_id
      LEFT JOIN dw_categories d6 ON d5.parent_category_id = d6.category_id
      LEFT JOIN dw_categories d7 ON d6.parent_category_id = d7.category_id
      LEFT JOIN dw_categories d8 ON d7.parent_category_id = d8.category_id;
DROP TABLE IF EXISTS dw_product_category_map;
CREATE TABLE IF NOT EXISTS dw_product_category_map
    SELECT
      p.product_id,
      MAX(p.name)                           name,
      IFNULL(MAX(fc.modality_name), 'NONE') modality_name,
      IFNULL(MAX(fc1.feature_name), 'NONE') feature_name,
      IFNULL(MAX(fc2.type_name), 'NONE')    type_name,
      IFNULL(MAX(p.product_type), 'Other')  product_type_name,
      IFNULL(MAX(p.telesales_only), 0)      telesales_only
    FROM dw_products p LEFT JOIN {$this->dbname}.catalog_category_product cpc ON p.product_id = cpc.product_id
LEFT JOIN dw_categories c ON c.category_id = cpc.category_id
LEFT JOIN dw_category_path cp ON cp.category_id = c.category_id
LEFT JOIN dw_flat_category fc
ON fc.modality_name = cp.name1 OR fc.modality_name = cp.name2
OR fc.modality_name = cp.name3 OR fc.modality_name = cp.name4
OR fc.modality_name = cp.name5 OR fc.modality_name = cp.name6
OR fc.modality_name = cp.name7 OR fc.modality_name = cp.name8
LEFT JOIN dw_flat_category fc1
ON fc1.feature_name = cp.name1 OR fc1.feature_name = cp.name2
OR fc1.feature_name = cp.name3 OR fc1.feature_name = cp.name4
OR fc1.feature_name = cp.name5 OR fc1.feature_name = cp.name6
OR fc1.feature_name = cp.name7 OR fc1.feature_name = cp.name8
LEFT JOIN dw_flat_category fc2
ON fc2.type_name = cp.name1 OR fc2.type_name = cp.name2
OR fc2.type_name = cp.name3 OR fc2.type_name = cp.name4
OR fc2.type_name = cp.name5 OR fc2.type_name = cp.name6
OR fc2.type_name = cp.name7 OR fc2.type_name = cp.name8
GROUP BY p.product_id;
DROP TABLE IF EXISTS dw_product_category_flat;
CREATE TABLE IF NOT EXISTS dw_product_category_flat (
  product_id  INT NOT NULL,
  category_id INT NOT NULL,
  PRIMARY KEY (product_id)
)
 	SELECT product_id,MAX(category_id) category_id
		FROM (SELECT  product_id,
						IFNULL(category_id,(SELECT category_id FROM dw_flat_category WHERE product_type = 'Other')) category_id
				FROM    dw_product_category_map mp     LEFT JOIN  dw_flat_category fc
												   ON fc.modality_name = mp.modality_name
												   AND fc.feature_name = mp.feature_name
												   AND fc.type_name = mp.type_name
												   AND fc.product_type_name = mp.product_type_name
												   AND fc.telesales_only = mp.telesales_only
												   ) a
												   GROUP BY product_id;

/**promo products have there own cateogries no add 500 to existing **/
UPDATE dw_product_category_flat
SET category_id = category_id + 500
WHERE product_id IN (SELECT product_id
                     from dw_products
                     where promotional_product = 1);

DROP TABLE IF EXISTS dw_product_price_qtys;
CREATE TABLE IF NOT EXISTS dw_product_price_qtys (
  product_id     INT,
  store_id       INT,
  first_tier_qty INT,
  multibuy_qty   INT,
  PRIMARY KEY (product_id, store_id)
)
  AS
    SELECT
      product_id,
      store_id,
      MIN(qty) AS first_tier_qty,
      MAX(qty) AS multibuy_qty
    FROM
      dw_product_prices
    GROUP BY
      product_id, store_id;

/** similar to to core table with custom fields and groupings **/
DROP TABLE IF EXISTS dw_stores;
CREATE TABLE IF NOT EXISTS dw_stores (
  store_id      INT NOT NULL,
  store_name    VARCHAR(100),
  website_group VARCHAR(100),
  store_type    VARCHAR(100),
  visible       INT,
  PRIMARY KEY (store_id)
)
    SELECT *
    FROM dw_stores_map;
INSERT INTO dw_stores
  SELECT
    website_id,
    name,
    '',
    '',
    1
  FROM {$this->dbname}.core_website
WHERE website_id NOT IN (SELECT store_id FROM dw_stores);

/** Customer supporting tables **/
CREATE TABLE IF NOT EXISTS customer_first_order (
  customer_id INT NOT NULL,
  order_id    INT NOT NULL,
  PRIMARY KEY (customer_id),
  INDEX(order_id)
);

DROP TABLE IF EXISTS customer_first_order_staging;
CREATE TABLE IF NOT EXISTS customer_first_order_staging (
  customer_id INT NOT NULL,
  order_id    INT NOT NULL,
  PRIMARY KEY (customer_id, order_id)
)
  AS
    SELECT
      oh.customer_id,
      MIN(oh.order_id) order_id
    FROM dw_order_headers oh
    INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id
    WHERE oh.customer_id IS NOT NULL
    GROUP BY customer_id;

REPLACE INTO customer_first_order SELECT * FROM customer_first_order_staging;
SQL;
		$this->runQuery($sql);
	}

	function buildOrderMarketingTable()
	{

		$sql = <<< SQL
        /***Some archived orders have been moved to other order states this can create problems so fix this This no longer is a problem but running his should be a fix if it occurs
        *DROP TABLE IF EXISTS dw_previously_archived
        *CREATE TABLE dw_previously_archived (
        *    order_id INT ,
        *    PRIMARY KEY (order_id)
        *) AS
        *SELECT oh.order_id FROM
        *dw_order_headers oh INNER JOIN (SELECT DISTINCT order_id FROM {$this->dbname}.sales_order_log WHERE STATUS = 'archived') sub1
        *ON oh.order_id = sub1.order_id
        *WHERE oh.status != 'archived'
        *
        *UPDATE dw_order_headers oh , dw_previously_archived pa
        *SET oh.status = 'archived' , oh.state = 'archived'
        *WHERE oh.order_id = pa.order_id
        ************************************************************/

        CREATE TABLE IF NOT EXISTS dw_order_headers_marketing (
            ORDER_ID INT,
            ORDER_NO CHAR(25),
            CREATED_AT DATETIME,
            CUSTOMER_ID INT,
            STORE_ID INT,
            STATUS VARCHAR(25),
            PREV_STATUS VARCHAR(25),
            ORDER_LIFECYCLE VARCHAR(30),
            ORDER_LIFECYCLE_CANCEL VARCHAR(30),
            ORDER_LIFECYCLE_ALL VARCHAR(30),
            ORDER_RANK_ALL INT,
            ORDER_RANK_CANCEL INT,
            ORDER_RANK_CANCEL_ONLY INT,
            ORDER_RANK INT,
            SOURCE VARCHAR(20),
            PRIMARY KEY (ORDER_ID,CREATED_AT,SOURCE),
            INDEX(ORDER_RANK_ALL),
            INDEX(ORDER_RANK_CANCEL),
            INDEX(CUSTOMER_ID),
            INDEX(SOURCE)
        );

        DROP TABLE IF EXISTS dw_order_headers_marketing_staging;
        CREATE TABLE dw_order_headers_marketing_staging (
            ORDER_ID INT,
            ORDER_NO CHAR(25),
            CREATED_AT DATETIME,
            CUSTOMER_ID INT,
            STORE_ID INT,
            STATUS VARCHAR(25),
            PREV_STATUS VARCHAR(25),
            ORDER_LIFECYCLE VARCHAR(30),
            ORDER_LIFECYCLE_CANCEL VARCHAR(30),
            ORDER_LIFECYCLE_ALL VARCHAR(30),
            ORDER_RANK_ALL INT,
            ORDER_RANK_CANCEL INT,
            ORDER_RANK_CANCEL_ONLY INT,
            ORDER_RANK INT,
            SOURCE VARCHAR(20),
            PRIMARY KEY (ORDER_ID,CREATED_AT,SOURCE),
            INDEX(ORDER_RANK_ALL),
            INDEX(ORDER_RANK_CANCEL),
            INDEX(CUSTOMER_ID),
            INDEX(SOURCE)
        );
        REPLACE INTO dw_order_headers_marketing_staging
        (
	    ORDER_ID,
            ORDER_NO,
            CUSTOMER_ID,
            STORE_ID,
            CREATED_AT,
            STATUS,
            SOURCE
        )
        SELECT	oh.order_id,
                oh.order_no,
                oh.customer_id,
                oh.store_id,
                oh.created_at,
                oh.STATUS,
                'current_db'
        FROM	dw_order_headers oh
        INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id
        WHERE STATUS != 'archived';

        REPLACE INTO dw_order_headers_marketing_staging
        (	ORDER_ID,
            ORDER_NO,
            CUSTOMER_ID,
            STORE_ID,
            CREATED_AT,
            STATUS,
            SOURCE
        )
        SELECT	oh.order_id,
                oh.order_id,
                oh.customer_id,
                oh.store_id,
                oh.created_at,
                'shipped',
                source
        FROM	dw_hist_order oh
        INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id;



        /** rank all orders including credit's canceled orders and then excluding **/
        DROP TABLE IF EXISTS order_rank_all;
        CREATE TABLE order_rank_all (
            ORDER_ID INT NOT NULL,
            ORDER_NO CHAR(25),
            CUSTOMER_ID INT,
            STORE_ID INT ,
            SOURCE VARCHAR(25),
            RANK INT,
            PRIMARY KEY(ORDER_ID,SOURCE)
        )
        AS
        SELECT * FROM (
                        SELECT
                ORDER_ID,
                ORDER_NO,
                CUSTOMER_ID,
                oh.STORE_ID,
                SOURCE,
                          (
                            CASE CUSTOMER_ID
                            WHEN @curEventId
                            THEN @curRow := @curRow + 1
                            ELSE @curRow := 1 AND @curEventId := CUSTOMER_ID END
                           )AS rank
                    FROM   dw_order_headers_marketing_staging oh   INNER JOIN (SELECT @curRow := 0, @curEventId := '') r
                INNER JOIN dw_core_config_data cd ON cd.store_id = oh.store_id
                WHERE path = 'surveysweb/surveys/active' AND VALUE = 0
                ORDER BY CUSTOMER_ID,CREATED_AT
                    ) t;
        /** rank  all apart from canceled and closed orders**/
        DROP TABLE IF EXISTS order_rank_cancel;
        CREATE TABLE order_rank_cancel (
            ORDER_ID INT NOT NULL,
            ORDER_NO CHAR(25),
            CUSTOMER_ID INT,
            STORE_ID INT ,
            SOURCE VARCHAR(25),
            RANK INT,
            PRIMARY KEY(ORDER_ID,SOURCE)
        )
        AS
        SELECT * FROM (
                SELECT
                ORDER_ID,
                ORDER_NO,
                CUSTOMER_ID,
                oh.STORE_ID,
                SOURCE,
                          (
                            CASE CUSTOMER_ID
                            WHEN @curEventId
                            THEN @curRow := @curRow + 1
                            ELSE @curRow := 1 AND @curEventId := CUSTOMER_ID END
                           )AS rank
                    FROM   dw_order_headers_marketing_staging oh   INNER JOIN (SELECT @curRow := 0, @curEventId := '') r
                INNER JOIN dw_core_config_data cd ON cd.store_id = oh.store_id
                WHERE path = 'surveysweb/surveys/active' AND VALUE = 0
                AND STATUS NOT IN ('canceled','closed')
                ORDER BY CUSTOMER_ID,CREATED_AT
                    ) t;
		/* rank  only canceled and closed orders**/
	    DROP TABLE IF EXISTS order_rank_cancel_only;
        CREATE TABLE order_rank_cancel_only (
            ORDER_ID INT NOT NULL,
            ORDER_NO CHAR(25),
            CUSTOMER_ID INT,
            STORE_ID INT ,
            SOURCE VARCHAR(25),
            RANK INT,
            PRIMARY KEY(ORDER_ID,SOURCE)
        )
        AS
        SELECT * FROM (
                SELECT
                ORDER_ID,
                ORDER_NO,
                CUSTOMER_ID,
                oh.STORE_ID,
                SOURCE,
                          (
                            CASE CUSTOMER_ID
                            WHEN @curEventId
                            THEN @curRow := @curRow + 1
                            ELSE @curRow := 1 AND @curEventId := CUSTOMER_ID END
                           )AS rank
                    FROM   dw_order_headers_marketing_staging oh   INNER JOIN (SELECT @curRow := 0, @curEventId := '') r
                INNER JOIN dw_core_config_data cd ON cd.store_id = oh.store_id
                WHERE path = 'surveysweb/surveys/active' AND VALUE = 0
                AND STATUS IN ('canceled', 'closed')
                ORDER BY CUSTOMER_ID,CREATED_AT

                    ) t;



        UPDATE dw_order_headers_marketing_staging om, order_rank_all orank
        SET om.ORDER_RANK_ALL = orank.rank
        WHERE orank.order_id =  om.order_id;

        UPDATE dw_order_headers_marketing_staging om, order_rank_cancel orank
        SET om.ORDER_RANK_CANCEL = orank.rank
        WHERE orank.order_id =  om.order_id;

        UPDATE dw_order_headers_marketing_staging om, order_rank_cancel_only orank
        SET om.ORDER_RANK_CANCEL_ONLY = orank.rank
        WHERE orank.order_id =  om.order_id;

		UPDATE dw_order_headers_marketing_staging om, dw_order_headers_marketing_staging om2
        SET om.PREV_STATUS = om2.status
        WHERE om.customer_id =  om2.customer_id AND om.order_rank_all  = om2.order_rank_all -1;


        UPDATE  dw_order_headers_marketing_staging h1, dw_order_headers_marketing h2
        SET		h1.ORDER_LIFECYCLE_ALL = CASE WHEN DATE_ADD(h1.created_at, INTERVAL -15 MONTH) >  h2.created_at
                THEN 'Reactivated'
                ELSE 'Regular' END
        WHERE	h1.order_rank_all > 1 AND h1.customer_id = h2.customer_id AND h2.order_rank_all  = h1.order_rank_all -1;

        UPDATE  dw_order_headers_marketing_staging h1, dw_order_headers_marketing h2
        SET		h1.ORDER_LIFECYCLE_CANCEL = CASE WHEN DATE_ADD(h1.created_at, INTERVAL -15 MONTH) >  h2.created_at
                THEN 'Reactivated'
                ELSE 'Regular' END
        WHERE	h1.order_rank_cancel > 1 AND h1.customer_id = h2.customer_id AND h2.order_rank_cancel = h1.order_rank_cancel-1;


        UPDATE  dw_order_headers_marketing_staging
        SET		ORDER_LIFECYCLE_ALL = 'New'
        WHERE	order_rank_all = 1;

        UPDATE  dw_order_headers_marketing_staging
        SET		ORDER_LIFECYCLE_CANCEL = 'New'
        WHERE	order_rank_cancel = 1;

        UPDATE  dw_order_headers_marketing_staging
        SET		ORDER_LIFECYCLE_ALL = 'Regular' ,ORDER_LIFECYCLE_CANCEL = 'Regular'
        WHERE	customer_id IS NULL;


        UPDATE  dw_order_headers_marketing_staging oh , dw_core_config_data cd
        SET		ORDER_LIFECYCLE_ALL = 'Survey' ,
                ORDER_LIFECYCLE_CANCEL = 'Survey' ,
                ORDER_RANK_ALL =1,
                ORDER_RANK_CANCEL =1
        WHERE	cd.store_id = oh.store_id AND path = 'surveysweb/surveys/active' AND `VALUE` = 1;

        UPDATE dw_order_headers_marketing_staging SET ORDER_LIFECYCLE = IFNULL(ORDER_LIFECYCLE_CANCEL,ORDER_LIFECYCLE_ALL);
        UPDATE dw_order_headers_marketing_staging SET ORDER_LIFECYCLE = 'Regular' WHERE ORDER_LIFECYCLE IS NULL;

		UPDATE dw_order_headers_marketing_staging SET ORDER_RANK = CASE WHEN ORDER_RANK_CANCEL_ONLY IS NULL THEN IFNULL(ORDER_RANK_CANCEL,0) ELSE GREATEST(IFNULL(ORDER_RANK_ALL-ORDER_RANK_CANCEL_ONLY,0),1) END ;
		/** store aggregate customers records**/
		CREATE TABLE IF NOT EXISTS dw_order_headers_marketing_agg (
			customer_id INT NOT NULL,
			first_order_date DATETIME,
			last_order_date DATETIME,
			no_of_orders INT,
			PRIMARY KEY(customer_id)
		);
		REPLACE INTO dw_order_headers_marketing_agg
  		SELECT `customer_id` as customer_id,MIN(`CREATED_AT`) first_order_date,MAX(`CREATED_AT`) last_order_date, MAX(`ORDER_RANK`) AS no_of_orders
		FROM dw_order_headers_marketing_staging
		WHERE customer_id IS NOT NULL
		GROUP BY `customer_id`;

        DELETE FROM dw_order_headers_marketing_staging WHERE source != 'current_db';
        REPLACE INTO dw_order_headers_marketing SELECT * FROM dw_order_headers_marketing_staging;
        /** this will allow us to workout channel on orders**/
        /** update coupon map as needed **/

		REPLACE INTO dw_coupon_code_map
		SELECT  src.code,channel ,
		CASE WHEN EXISTS(SELECT `value`	FROM `dw_core_config_data` WHERE path = 'referafriend/invite/voucher' AND CAST(`value` AS UNSIGNED) = src.coupon_id)
			THEN 1
			ELSE 0
			END  is_raf
		FROM {$this->dbname}.salesrule  sr INNER JOIN {$this->dbname}.salesrule_coupon src ON sr.rule_id = src.rule_id
		WHERE channel != '' AND  channel  IS NOT NULL;

		/**create a generic version of bis source**/

		DROP TABLE IF EXISTS dw_bis_map_generic;
		CREATE TABLE dw_bis_map_generic (
			bis_source_code VARCHAR(150) NOT NULL,
			channel VARCHAR(150),
			PRIMARY KEY(bis_source_code)
		) AS SELECT DISTINCT affilCode bis_source_code , NULL AS channel  FROM
		dw_order_headers
		WHERE affilCode IS NOT NULL AND affilCode != '';


		UPDATE dw_bis_map_generic mg INNER JOIN dw_bis_source_map mp ON mp.source_code = mg.bis_source_code
		SET mg.channel = mp.channel
		WHERE mp.channel != '' AND mp.channel IS NOT NULL;

		DROP TABLE IF EXISTS dw_bis_map_dynamic_types;
		CREATE TABLE dw_bis_map_dynamic_types(
			prefix VARCHAR(100) NOT NULL,
			channel VARCHAR(100) NOT NULL,
			PRIMARY KEY (prefix)
		);

		INSERT INTO  dw_bis_map_dynamic_types VALUES('adword','Adwords'),('affiliate','Affiliates');


		UPDATE dw_bis_map_generic mg , dw_bis_map_dynamic_types dt
		SET mg.channel = dt.channel
		WHERE bis_source_code LIKE CONCAT(dt.prefix,'%') AND (mg.channel IS NULL OR mg.channel = '');

		/**use all data to create table that will hold all channel**/
		CREATE TABLE IF NOT EXISTS dw_order_channel (
			order_id INT NOT NULL,
			order_no VARCHAR(20),
			bis_channel VARCHAR(100),
			raf_channel VARCHAR(100),
			ga_channel VARCHAR(100),
			coupon_channel VARCHAR(100),
			channel VARCHAR(100),
			PRIMARY KEY (order_id)
		);

		DROP TABLE IF EXISTS dw_order_channel_staging;
		CREATE TABLE IF NOT EXISTS dw_order_channel_staging (
			order_id INT NOT NULL,
			order_no VARCHAR(20),
			bis_channel VARCHAR(100),
			raf_channel VARCHAR(100),
			ga_channel VARCHAR(100),
			coupon_channel VARCHAR(100),
			channel VARCHAR(100),
			PRIMARY KEY (order_id)
		)

		SELECT oh.order_id,oh.order_no,mg.channel bis_channel,cm.channel raf_channel,ga.channel ga_channel,cm2.channel coupon_channel,
		COALESCE(mg.channel,cm.channel,ga.channel,cm2.channel,'Unknown') channel

		FROM dw_order_headers oh
		INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id
		LEFT JOIN `dw_bis_map_generic`mg ON `bis_source_code`= affilcode
		LEFT JOIN `dw_coupon_code_map`cm ON cm.`coupon_code`= oh.coupon_code AND cm.`is_raf` =1
		LEFT JOIN {$this->dbname}.`ga_entity_transaction_data` ga ON ga.`transactionId`= oh.order_no
		LEFT JOIN `dw_coupon_code_map`cm2 ON cm2.`coupon_code`= oh.coupon_code AND cm2.`is_raf` =0;

		REPLACE INTO dw_order_channel SELECT * FROM dw_order_channel_staging;

		/** build order sequence table **/

		DROP TABLE IF EXISTS dw_full_creditmemo;
CREATE TABLE IF NOT EXISTS dw_full_creditmemo (
	order_id INT,
	creditmemo_id INT,
	PRIMARY KEY(creditmemo_id)
) AS
SELECT 	oh.order_id,
	MAX(ch.creditmemo_id) creditmemo_id
FROM 	dw_creditmemo_headers ch INNER JOIN dw_order_headers oh ON oh.order_id = ch.order_id
WHERE 	oh.status = 'closed'
GROUP BY ch.order_id;


DROP TABLE IF EXISTS dw_entity_header_rank;
CREATE TABLE dw_entity_header_rank (
    ORDER_ID INT NOT NULL,
    DOCUMENT_ID INT NOT NULL,
    DOCUMENT_TYPE VARCHAR(10) NOT NULL,
    CREATED_AT DATETIME,
    CUSTOMER_ID INT,
    STORE_ID INT,
    SOURCE VARCHAR(20),
    DOCUMENT_SEQ INT,
    PRIMARY KEY (ORDER_ID,DOCUMENT_TYPE,DOCUMENT_ID,CREATED_AT,SOURCE),
    INDEX(CUSTOMER_ID),
    INDEX(CREATED_AT),
    INDEX(DOCUMENT_SEQ),
    INDEX(SOURCE)
);
INSERT INTO dw_entity_header_rank
(
    ORDER_ID,
    DOCUMENT_ID,
    DOCUMENT_TYPE,
    CREATED_AT,
    CUSTOMER_ID,
    STORE_ID,
    SOURCE,
    DOCUMENT_SEQ
)
SELECT	order_id,
	order_id,
	'ORDER',
	created_at,
	t1.customer_id,
	store_id,
	'current_db',
	1
FROM	dw_order_headers t1
INNER JOIN dw_updated_customers uc ON uc.customer_id = t1.customer_id
WHERE STATUS != 'archived'
;

INSERT INTO dw_entity_header_rank
(
    ORDER_ID,
    DOCUMENT_ID,
    DOCUMENT_TYPE,
    CREATED_AT,
    CUSTOMER_ID,
    STORE_ID,
    SOURCE,
    DOCUMENT_SEQ
)
SELECT	order_id,
	order_id,
	'CANCEL',
	created_at,
	t1.customer_id,
	store_id,
	'current_db',
	2
FROM	dw_order_headers t1
INNER JOIN dw_updated_customers uc ON uc.customer_id = t1.customer_id
WHERE STATUS = 'canceled';

INSERT INTO dw_entity_header_rank
(
    ORDER_ID,
    DOCUMENT_ID,
    DOCUMENT_TYPE,
    CREATED_AT,
    CUSTOMER_ID,
    STORE_ID,
    SOURCE,
    DOCUMENT_SEQ
)
SELECT	ch.order_id,
	ch.creditmemo_id,
	'CREDITMEMO',
	ch.created_at,
	oh.customer_id,
	oh.store_id,
	'current_db',
	4
FROM	dw_creditmemo_headers ch INNER JOIN dw_order_headers oh ON ch.order_id = oh.order_id
INNER JOIN dw_updated_customers uc ON uc.customer_id = oh.customer_id;

INSERT INTO dw_entity_header_rank
(
	ORDER_ID,
	DOCUMENT_ID,
	DOCUMENT_TYPE,
	CREATED_AT,
	CUSTOMER_ID,
	STORE_ID,
	SOURCE,
	DOCUMENT_SEQ
)
SELECT	order_id,
	order_id,
	'ORDER',
	created_at,
	t1.customer_id,
	store_id,
	source,
	1
FROM	dw_hist_order t1
INNER JOIN dw_updated_customers uc ON uc.customer_id = t1.customer_id;

DROP TABLE IF EXISTS dw_entity_header_rank_all;
CREATE TABLE IF NOT EXISTS dw_entity_header_rank_all (
	ORDER_ID INT NOT NULL,
	DOCUMENT_ID INT NOT NULL,
	DOCUMENT_TYPE  VARCHAR(10) NOT NULL,
	CREATED_AT DATETIME,
	CUSTOMER_ID INT,
	STORE_ID INT,
	SOURCE VARCHAR(20),
	DOCUMENT_SEQ INT,
	ORDER_RANK INT,
	NEXT_DOCUMENT_TYPE  VARCHAR(10),
	PREV_DOCUMENT_TYPE  VARCHAR(10),
	FULL_CREDIT INT DEFAULT 0,
	PREV_FULL_CREDIT INT DEFAULT 0,
	PRIMARY KEY (ORDER_ID,DOCUMENT_TYPE,DOCUMENT_ID,CREATED_AT,SOURCE),
	INDEX(DOCUMENT_ID,DOCUMENT_TYPE),
	INDEX(DOCUMENT_ID),
	INDEX(DOCUMENT_TYPE),
	INDEX(CUSTOMER_ID),
	INDEX(CREATED_AT),
	INDEX(ORDER_RANK),
	INDEX(SOURCE),
	INDEX(DOCUMENT_SEQ)
    ) AS
    SELECT t.* FROM (
	SELECT
	  a.*,
	  (
	    CASE customer_id
	    WHEN @curEventId
	    THEN  @curRow := @curRow + 1
	    ELSE @curRow := 1 AND @curEventId := customer_id END
	  ) AS ORDER_RANK,
	  '' NEXT_DOCUMENT_TYPE,
	  '' PREV_DOCUMENT_TYPE,
	  0 FULL_CREDIT
    FROM   dw_entity_header_rank a    INNER JOIN (SELECT @curRow := 0, @curEventId := '') r
    ORDER BY customer_id,created_at,DOCUMENT_SEQ
    ) t;

UPDATE dw_entity_header_rank_all t1 INNER JOIN dw_entity_header_rank_all t2 ON t1.CUSTOMER_ID = t2.CUSTOMER_ID AND t1.ORDER_RANK + 1 = t2.ORDER_RANK
SET t1.NEXT_DOCUMENT_TYPE = t2.DOCUMENT_TYPE;

UPDATE dw_entity_header_rank_all t1 INNER JOIN dw_full_creditmemo fc ON fc.creditmemo_id = t1.document_id AND t1.document_type = 'CREDITMEMO'
SET t1.FULL_CREDIT = 1;

UPDATE dw_entity_header_rank_all t1 SET t1.FULL_CREDIT = 1 WHERE DOCUMENT_TYPE = 'CANCEL';

UPDATE dw_entity_header_rank_all t1 INNER JOIN dw_entity_header_rank_all t2 ON t1.CUSTOMER_ID = t2.CUSTOMER_ID AND t1.ORDER_RANK - 1 = t2.ORDER_RANK
SET t1.PREV_DOCUMENT_TYPE = t2.DOCUMENT_TYPE , t1.PREV_FULL_CREDIT = t2.FULL_CREDIT;


CREATE TABLE IF NOT EXISTS dw_entity_header_rank_seq (
	ORDER_ID INT NOT NULL,
	DOCUMENT_ID INT NOT NULL,
	DOCUMENT_TYPE  VARCHAR(10) NOT NULL,
	CREATED_AT DATETIME,
	CUSTOMER_ID INT,
	STORE_ID INT,
	SOURCE VARCHAR(20),
	DOCUMENT_SEQ INT,
	ORDER_RANK INT,
	NEXT_DOCUMENT_TYPE  VARCHAR(10),
	PREV_DOCUMENT_TYPE  VARCHAR(10),
	FULL_CREDIT INT DEFAULT 0,
	PREV_FULL_CREDIT INT DEFAULT 0,
	ORDER_SEQ_PLUS INT,
	ORDER_SEQ_MINUS INT,
	ORDER_SEQ INT,
	PRIMARY KEY (ORDER_ID,DOCUMENT_TYPE,DOCUMENT_ID,CREATED_AT,SOURCE),
	INDEX(DOCUMENT_ID,DOCUMENT_TYPE),
	INDEX(DOCUMENT_ID),
	INDEX(DOCUMENT_TYPE),
	INDEX(CUSTOMER_ID),
	INDEX(CREATED_AT),
	INDEX(ORDER_RANK),
	INDEX(SOURCE),
	INDEX(DOCUMENT_SEQ)
	);

REPLACE INTO dw_entity_header_rank_seq
SELECT t.* FROM (
	SELECT
	  a.*,
	  (
	    CASE customer_id
	    WHEN @curEventId
	    THEN
		CASE
		WHEN (DOCUMENT_TYPE IN ('ORDER'))
		THEN @curRow := @curRow + 1

		ELSE @curRow := @curRow + 0
		END
	    ELSE @curRow := 1  END
	  ) AS ORDER_SEQ_PLUS,
	  (
	    CASE customer_id
	    WHEN @curEventId
	    THEN
		CASE
		WHEN (PREV_DOCUMENT_TYPE IN ('CANCEL','CREDITMEMO')) AND (PREV_FULL_CREDIT = 1)
		THEN @curRow2 := @curRow2 - 1

		ELSE @curRow2 := @curRow2 + 0
		END
	    ELSE @curRow2 := 1 AND @curEventId := customer_id END
	  ) - 1 AS ORDER_SEQ_MINUS,
	  0  AS ORDER_SEQ
    FROM   dw_entity_header_rank_all a    INNER JOIN (SELECT @curRow := 0, @curRow2 := 0, @curEventId := '') r
    ORDER BY customer_id,created_at,DOCUMENT_SEQ
    ) t;

UPDATE dw_entity_header_rank_seq sq SET ORDER_SEQ = IFNULL(GREATEST(1,(ORDER_SEQ_PLUS + ORDER_SEQ_MINUS)),1);

CREATE TABLE IF NOT EXISTS  dw_customer_total_orders(
	customer_id INT,
	max_order_seq_plus INT,
	min_order_seq_minus INT,
	no_of_orders INT,
	PRIMARY KEY (customer_id)
);


REPLACE INTO dw_customer_total_orders
SELECT
	seq.customer_id,
	MAX(seq.ORDER_SEQ_PLUS) max_order_seq_plus,
	MIN(seq.ORDER_SEQ_MINUS) min_order_seq_minus,
	IFNULL((MAX(seq.ORDER_SEQ_PLUS) + MIN(seq.ORDER_SEQ_MINUS)),0) no_of_orders
FROM 	dw_entity_header_rank_seq seq
	INNER JOIN dw_updated_customers uc ON uc.customer_id = seq.customer_id
WHERE 	seq.customer_id IS NOT NULL
GROUP BY seq.customer_id;

DELETE FROM dw_entity_header_rank_seq WHERE source != 'current_db';
SQL;
		$this->runQuery($sql);
	}

	function clearVatTables()
	{
		$sql = "DROP TABLE IF EXISTS order_headers_vat;
        DROP TABLE IF EXISTS invoice_headers_vat;
        DROP TABLE IF EXISTS creditmemo_headers_vat;
        DROP TABLE IF EXISTS shipment_headers_vat;
        DROP TABLE IF EXISTS order_lines_vat;
        DROP TABLE IF EXISTS invoice_lines_vat;
        DROP TABLE IF EXISTS creditmemo_lines_vat;
        DROP TABLE IF EXISTS shipment_lines_vat;";

		$this->runQuery($sql);
	}

	function rebuildDwTables()
	{
		$this->createStaticViews();
	}

	public function createStaticViews()
	{


		$sql = <<< SQL
CREATE OR REPLACE VIEW vw_catalog_product_website AS
			SELECT 	`catalog_product_website`.`product_id` AS `product_id`,
       				`catalog_product_website`.`website_id` AS `website_id`
			FROM   	`{$this->dbname}`.`catalog_product_website`
			UNION
			SELECT DISTINCT `dw_products_all_stores`.`product_id` AS `product_id`,
			                0                                     AS `0`
			FROM            `dw_products_all_stores`;


			CREATE OR REPLACE VIEW vw_products AS
			SELECT    `cw`.`store_name`                                                 AS `store_name`,
	          `pas`.`product_id`                                                AS `product_id`,
	          `pas`.`status`                                                    AS `status`,
	          `pcf`.`category_id`                                               AS `category_id`,
	          `pas`.`sku`                                                       AS `sku`,
	          `br`.`value`                                                      AS `brand`,
	          `pas`.`created_at`                                                AS `created_at`,
	          `pas`.`updated_at`                                                AS `updated_at`,
	          `cf`.`product_type`                                               AS `product_type`,
	          cast(`pas`.`product_lifecycle` AS      CHAR(255) charset latin1)  AS `product_lifecycle`,
	          cast(`pas`.`product_lifecycle_text` AS CHAR(8000) charset latin1) AS `product_lifecycle_text`,
	          `pas`.`telesales_only`                                            AS `telesales_only`,
	          `pas`.`stocked_lens`                                              AS `stocked_lens`,
	          `pas`.`tax_class_id`                                              AS `tax_class_id`,
	          `pas`.`name`                                                      AS `name`,
	          cast(`pas`.`description` AS       CHAR(8000) charset latin1)            AS `description`,
	          cast(`pas`.`short_description` AS CHAR(8000) charset latin1)            AS `short_description`,
	          cast(`pas`.`cat_description` AS   CHAR(8000) charset latin1)            AS `category_list_description`,
	          `pas`.`cost`                                                            AS `average_cost`,
	          `pas`.`weight`                                                          AS `weight`,
	          `man`.`value`                                                           AS `manufacturer`,
	          `pas`.`meta_title`                                                      AS `meta_title`,
	          cast(`pas`.`meta_keyword` AS CHAR(8000) charset latin1)                 AS `meta_keyword`,
	          `pas`.`meta_description`                                                AS `meta_description`,
	          `pas`.`image`                                                           AS `image`,
	          `pas`.`small_image`                                                     AS `small_image`,
	          `pas`.`thumbnail`                                                       AS `thumbnail`,
	          `pas`.`url_key`                                                         AS `url_key`,
	          `pas`.`url_path`                                                        AS `url_path`,
	          `pas`.`visibility`                                                      AS `visibility`,
	          `pas`.`image_label`                                                     AS `image_label`,
	          `pas`.`small_image_label`                                               AS `small_image_label`,
	          `pas`.`thumbnail_label`                                                 AS `thumbnail_label`,
	          `pas`.`equivalent_sku`                                                  AS `equivalent_sku`,
	          `pas`.`daysperlens`                                                     AS `daysperlens`,
	          `pas`.`altdaysperlens`                                                  AS `alt_daysperlens`,
	          `pas`.`equivalence`                                                     AS `equivalence`,
	          `pas`.`availability`                                                    AS `availability`,
	          `pas`.`condition`                                                       AS `condition`,
	          `pas`.`google_base_price`                                               AS `google_base_price`,
	          `pas`.`promotional_product`                                             AS `promotional_product`,
	          `pas`.`promotion_rule`                                                  AS `promotion_rule`,
	          `pas`.`promotion_rule2`                                                 AS `promotion_rule2`,
	          cast(`pas`.`upsell_text` AS CHAR(8000) charset latin1)                  AS `upsell_text`,
	          `pas`.`replacement_sku`                                                 AS `replacement_sku`,
	          `pas`.`price_comp_price`                                                AS `price_comp_price`,
	          `pas`.`google_feed_title_prefix`                                        AS `google_feed_title_prefix`,
	          `pas`.`google_feed_title_suffix`                                        AS `google_feed_title_suffix`,
	          `pas`.`autoreorder_alter_sku`                                           AS `autoreorder_alter_sku`,
	          `pas`.`rrp_price`                                                       AS `rrp_price`,
	          cast(`pas`.`promotional_text` AS CHAR(8000) charset latin1)             AS `promotional_text`,
	          `pas`.`glasses_colour`                                                  AS `glasses_colour`,
	          `pas`.`glasses_material`                                                AS `glasses_material`,
	          `pas`.`glasses_size`                                                    AS `glasses_size`,
	          `pas`.`glasses_sex`                                                     AS `glasses_sex`,
	          `pas`.`glasses_style`                                                   AS `glasses_style`,
	          `pas`.`glasses_shape`                                                   AS `glasses_shape`,
	          `pas`.`glasses_lens_width`                                              AS `glasses_lens_width`,
	          `pas`.`glasses_bridge_width`                                            AS `glasses_bridge_width`,
	          greatest(cast(`pas`.`packsize` AS UNSIGNED),1)                          AS `pack_qty`,
	          `pas`.`packtext`                                                        AS `packtext`,(
	          CASE
	                    WHEN (
	                                        cast(`pas`.`alt_packsize` AS UNSIGNED) > 0) THEN cast(`pas`.`alt_packsize` AS UNSIGNED)
	                    ELSE NULL
	          end)                                                                                                                                                                AS `alt_pack_qty`,
	          `pq`.`first_tier_qty`                                                                                                                                               AS `base_pack_qty`,
	          (`pq`.`first_tier_qty` / greatest(1,cast(`pas`.`packsize` AS UNSIGNED)))                                                                                            AS `base_no_packs`,
	          `dp1`.`local_unit_price_inc_vat`                                                                                                                                    AS `local_base_unit_price_inc_vat`,
	          `dp1`.`local_qty_price_inc_vat`                                                                                                                                     AS `local_base_value_inc_vat`,
	          `dp1`.`local_pack_price_inc_vat`                                                                                                                                    AS `local_base_pack_price_inc_vat`,
	          round((`dp1`.`local_qty_price_exc_vat` / `dp1`.`qty`),4)                                                                                                            AS `local_base_unit_price_exc_vat`,
	          `dp1`.`local_qty_price_exc_vat`                                                                                                                                     AS `local_base_value_exc_vat`,
	          cast(round(((`dp1`.`local_qty_price_exc_vat` / `dp1`.`qty`) * greatest(1,cast(`pas`.`packsize` AS UNSIGNED))),4) AS DECIMAL(12,4))                                  AS `local_base_pack_price_exc_vat`,
	          `dp1`.`local_prof_fee`                                                                                                                                              AS `local_base_prof_fee`,
	          `dp1`.`local_product_cost`                                                                                                                                          AS `local_base_product_cost`,
	          `dp1`.`local_shipping_cost`                                                                                                                                         AS `local_base_shipping_cost`,
	          `dp1`.`local_total_cost`                                                                                                                                            AS `local_base_total_cost`,
	          `dp1`.`local_margin_amount`                                                                                                                                         AS `local_base_margin_amount`,
	          `dp1`.`local_margin_percent`                                                                                                                                        AS `local_base_margin_percent`,
	          `dp1`.`global_unit_price_inc_vat`                                                                                                                                   AS `global_base_unit_price_inc_vat`,
	          `dp1`.`global_qty_price_inc_vat`                                                                                                                                    AS `global_base_value_inc_vat`,
	          `dp1`.`global_pack_price_inc_vat`                                                                                                                                   AS `global_base_pack_price_inc_vat`,
	          round(((`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`) / `dp1`.`qty`),4)                                                                           AS `global_base_unit_price_exc_vat`,
	          (`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`)                                                                                                    AS `global_base_value_exc_vat`,
	          cast(round((((`dp1`.`local_qty_price_exc_vat` * `dp1`.`local_to_global_rate`) / `dp1`.`qty`) * greatest(1,cast(`pas`.`packsize` AS UNSIGNED))),4) AS DECIMAL(12,4)) AS `global_base_pack_price_exc_vat`,
	          `dp1`.`global_prof_fee`                                                                                                                                             AS `global_base_prof_fee`,
	          `dp1`.`global_product_cost`                                                                                                                                         AS `global_base_product_cost`,
	          `dp1`.`global_shipping_cost`                                                                                                                                        AS `global_base_shipping_cost`,
	          `dp1`.`global_total_cost`                                                                                                                                           AS `global_base_total_cost`,
	          `dp1`.`global_margin_amount`                                                                                                                                        AS `global_base_margin_amount`,
	          `dp1`.`global_margin_percent`                                                                                                                                       AS `global_base_margin_percent`,
	          `pq`.`multibuy_qty`                                                                                                                                                 AS `multi_pack_qty`,
	          (`pq`.`multibuy_qty` / greatest(1,cast(`pas`.`packsize` AS UNSIGNED)))                                                                                              AS `multi_no_packs`,
	          `dp2`.`local_unit_price_inc_vat`                                                                                                                                    AS `local_multi_unit_price_inc_vat`,
	          `dp2`.`local_qty_price_inc_vat`                                                                                                                                     AS `local_multi_value_inc_vat`,
	          `dp2`.`local_pack_price_inc_vat`                                                                                                                                    AS `local_multi_pack_price_inc_vat`,
	          round((`dp2`.`local_qty_price_exc_vat` / `dp2`.`qty`),4)                                                                                                            AS `local_multi_unit_price_exc_vat`,
	          `dp2`.`local_qty_price_exc_vat`                                                                                                                                     AS `local_multi_value_exc_vat`,
	          cast(round(((`dp2`.`local_qty_price_exc_vat` / `dp2`.`qty`) * greatest(1,cast(`pas`.`packsize` AS UNSIGNED))),4) AS DECIMAL(12,4))                                  AS `local_multi_pack_price_exc_vat`,
	          `dp2`.`local_prof_fee`                                                                                                                                              AS `local_multi_prof_fee`,
	          `dp2`.`local_product_cost`                                                                                                                                          AS `local_multi_product_cost`,
	          `dp2`.`local_shipping_cost`                                                                                                                                         AS `local_multi_shipping_cost`,
	          `dp2`.`local_total_cost`                                                                                                                                            AS `local_multi_total_cost`,
	          `dp2`.`local_margin_amount`                                                                                                                                         AS `local_multi_margin_amount`,
	          `dp2`.`local_margin_percent`                                                                                                                                        AS `local_multi_margin_percent`,
	          `dp2`.`global_unit_price_inc_vat`                                                                                                                                   AS `global_multi_unit_price_inc_vat`,
	          `dp2`.`global_qty_price_inc_vat`                                                                                                                                    AS `global_multi_value_inc_vat`,
	          `dp2`.`global_pack_price_inc_vat`                                                                                                                                   AS `global_multi_pack_price_inc_vat`,
	          round(((`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`) / `dp2`.`qty`),4)                                                                           AS `global_multi_unit_price_exc_vat`,
	          (`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`)                                                                                                    AS `global_multi_value_exc_vat`,
	          cast(round((((`dp2`.`local_qty_price_exc_vat` * `dp2`.`local_to_global_rate`) / `dp2`.`qty`) * greatest(1,cast(`pas`.`packsize` AS UNSIGNED))),4) AS DECIMAL(12,4)) AS `global_multi_pack_price_exc_vat`,
	          `dp2`.`global_prof_fee`                                                                                                                                             AS `global_multi_prof_fee`,
	          `dp2`.`global_product_cost`                                                                                                                                         AS `global_multi_product_cost`,
	          `dp2`.`global_shipping_cost`                                                                                                                                        AS `global_multi_shipping_cost`,
	          `dp2`.`global_total_cost`                                                                                                                                           AS `global_multi_total_cost`,
	          `dp2`.`global_margin_amount`                                                                                                                                        AS `global_multi_margin_amount`,
	          `dp2`.`global_margin_percent`                                                                                                                                       AS `global_multi_margin_percent`,
	          `dp1`.`local_to_global_rate`                                                                                                                                        AS `local_to_global_rate`,
	          `dp1`.`store_currency_code`                                                                                                                                         AS `store_currency_code`,
	          `dp1`.`prof_fee_percent`                                                                                                                                            AS `prof_fee_percent`,
	          `dp1`.`vat_percent_before_prof_fee`                                                                                                                                 AS `vat_percent_before_prof_fee`,
	          `dp1`.`vat_percent`                                                                                                                                                 AS `vat_percent`
				FROM      (((((((((`dw_products_all_stores` `pas`
				JOIN      `vw_catalog_product_website` `cpw`
				ON       (((
				                                        `pas`.`product_id` = `cpw`.`product_id`)
				                    AND       (
				                                        `pas`.`store_id` = `cpw`.`website_id`))))
				LEFT JOIN `dw_stores` `cw`
				ON       ((
				                              `cw`.`store_id` = `pas`.`store_id`)))
				LEFT JOIN `dw_product_category_flat` `pcf`
				ON       ((
				                              `pcf`.`product_id` = `pas`.`product_id`)))
				LEFT JOIN `dw_flat_category` `cf`
				ON       ((
				                              `cf`.`category_id` = `pcf`.`category_id`)))
				LEFT JOIN `{$this->dbname}`.`eav_attribute_option_value` `man`
				ON       ((
				                              `man`.`option_id` = `pas`.`manufacturer`)))
				LEFT JOIN `{$this->dbname}`.`eav_attribute_option_value` `br`
				ON       ((
				                              `br`.`option_id` = `pas`.`brand`)))
				LEFT JOIN `dw_product_price_qtys` `pq`
				ON       (((
				                                        `pq`.`product_id` = `pas`.`product_id`)
				                    AND       (
				                                        `pq`.`store_id` = `pas`.`store_id`))))
				LEFT JOIN `dw_product_prices` `dp1`
				ON       (((
				                                        `dp1`.`product_id` = `pas`.`product_id`)
				                    AND       (
				                                        `dp1`.`store_id` = `pas`.`store_id`)
				                    AND       (
				                                        `pq`.`first_tier_qty` = `dp1`.`qty`))))
				LEFT JOIN `dw_product_prices` `dp2`
				ON       (((
				                                        `dp2`.`product_id` = `pas`.`product_id`)
				                    AND       (
				                                        `dp2`.`store_id` = `pas`.`store_id`)
				                    AND       (
				                                        `pq`.`multibuy_qty` = `dp2`.`qty`))));



		CREATE OR REPLACE VIEW vw_product_prices AS
		SELECT
		  `pas`.`price_id`                    AS `price_id`,
		  `pas`.`store_name`                  AS `store_name`,
		  `pas`.`product_id`                  AS `product_id`,
		  `pas`.`qty`                         AS `qty`,
		  `pas`.`pack_size`                   AS `pack_size`,
		  `pas`.`local_unit_price_inc_vat`    AS `local_unit_price_inc_vat`,
		  `pas`.`local_qty_price_inc_vat`     AS `local_qty_price_inc_vat`,
		  `pas`.`local_pack_price_inc_vat`    AS `local_pack_price_inc_vat`,
		  `pas`.`local_prof_fee`              AS `local_prof_fee`,
		  `pas`.`local_product_cost`          AS `local_product_cost`,
		  `pas`.`local_shipping_cost`         AS `local_shipping_cost`,
		  `pas`.`local_total_cost`            AS `local_total_cost`,
		  `pas`.`local_margin_amount`         AS `local_margin_amount`,
		  `pas`.`local_margin_percent`        AS `local_margin_percent`,
		  `pas`.`local_to_global_rate`        AS `local_to_global_rate`,
		  `pas`.`store_currency_code`         AS `store_currency_code`,
		  `pas`.`global_unit_price_inc_vat`   AS `global_unit_price_inc_vat`,
		  `pas`.`global_qty_price_inc_vat`    AS `global_qty_price_inc_vat`,
		  `pas`.`global_pack_price_inc_vat`   AS `global_pack_price_inc_vat`,
		  `pas`.`global_prof_fee`             AS `global_prof_fee`,
		  `pas`.`global_product_cost`         AS `global_product_cost`,
		  `pas`.`global_shipping_cost`        AS `global_shipping_cost`,
		  `pas`.`global_total_cost`           AS `global_total_cost`,
		  `pas`.`global_margin_amount`        AS `global_margin_amount`,
		  `pas`.`global_margin_percent`       AS `global_margin_percent`,
		  `pas`.`prof_fee_percent`            AS `prof_fee_percent`,
		  `pas`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
		  `pas`.`vat_percent`                 AS `vat_percent`
		FROM (`dw_product_prices` `pas`
		   JOIN `{$this->dbname}`.`catalog_product_website` `cpw`
		     ON (((`pas`.`product_id` = `cpw`.`product_id`)
		          AND (`pas`.`store_id` = `cpw`.`website_id`))));





    	CREATE OR REPLACE VIEW vw_invoice_headers_invoice AS
		SELECT `cw`.`store_name` AS `store_name`,
	       `inv`.`invoice_id` AS `invoice_id`,
	       `inv`.`invoice_no` AS `invoice_no`,
	       CAST('INVOICE' AS CHAR(25) CHARSET latin1) AS `document_type`,
	       `inv`.`created_at` AS `document_date`,
	       `inv`.`updated_at` AS `document_updated_at`,
	       `inv`.`invoice_id` AS `document_id`,
	       `inv`.`invoice_no` AS `document_no`,
	       `inv`.`created_at` AS `invoice_date`,
	       `inv`.`updated_at` AS `updated_at`,
	       `oh`.`order_id` AS `order_id`,
	       `oh`.`order_no` AS `order_no`,
	       `oh`.`created_at` AS `order_date`,
	       `oh`.`customer_id` AS `customer_id`,
	       `oh`.`customer_email` AS `customer_email`,
	       `oh`.`customer_firstname` AS `customer_firstname`,
	       `oh`.`customer_lastname` AS `customer_lastname`,
	       `oh`.`customer_middlename` AS `customer_middlename`,
	       `oh`.`customer_prefix` AS `customer_prefix`,
	       `oh`.`customer_suffix` AS `customer_suffix`,
	       `oh`.`customer_taxvat` AS `customer_taxvat`,
	       `oh`.`status` AS `status`,
	       `oh`.`coupon_code` AS `coupon_code`,
	       `ov`.`total_weight` AS `weight`,
	       `oh`.`customer_gender` AS `customer_gender`,
	       `oh`.`customer_dob` AS `customer_dob`,
	       LEFT(`oh`.`shipping_description`,LOCATE(' - ',`oh`.`shipping_description`)) AS `shipping_carrier`,
	       `oh`.`shipping_description` AS `shipping_method`,
	       (CASE `oh`.`warehouse_approved_time`
	            WHEN '0000-00-00 00:00:00' THEN NULL
	            ELSE `oh`.`warehouse_approved_time`
	        END) AS `approved_time`,
	       `ic`.`length_of_time_to_invoice` AS `length_of_time_to_invoice`,
	       (CASE
	            WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
	            ELSE IFNULL(CONVERT(`pm`.`payment_method_name` USING utf8),`op`.`method`)
	        END) AS `payment_method`,
	       `op`.`amount_authorized` AS `local_payment_authorized`,
	       `op`.`amount_canceled` AS `local_payment_canceled`,
	       `op`.`cc_exp_month` AS `cc_exp_month`,
	       `op`.`cc_exp_year` AS `cc_exp_year`,
	       `op`.`cc_ss_start_month` AS `cc_start_month`,
	       `op`.`cc_ss_start_year` AS `cc_start_year`,
	       `op`.`cc_last4` AS `cc_last4`,
	       `op`.`cc_status_description` AS `cc_status_description`,
	       `op`.`cc_owner` AS `cc_owner`,
	       IFNULL(CONVERT(`ctm`.`card_type_name` USING utf8),`op`.`cc_type`) AS `cc_type`,
	       `op`.`cc_status` AS `cc_status`,
	       `op`.`cc_ss_issue` AS `cc_issue_no`,
	       `op`.`cc_avs_status` AS `cc_avs_status`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info1`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info2`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info3`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info4`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info5`,
	       `bill`.`email` AS `billing_email`,
	       `bill`.`company` AS `billing_company`,
	       `bill`.`prefix` AS `billing_prefix`,
	       `bill`.`firstname` AS `billing_firstname`,
	       `bill`.`middlename` AS `billing_middlename`,
	       `bill`.`lastname` AS `billing_lastname`,
	       `bill`.`suffix` AS `billing_suffix`,
	       SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},1) AS `billing_street1`,
	       (CASE
	            WHEN (LOCATE({$this->newLine},`bill`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`bill`.`street`) + 1))
	            ELSE ''
	        END) AS `billing_street2`,
	       `bill`.`city` AS `billing_city`,
	       `bill`.`region` AS `billing_region`,
	       `bill`.`region_id` AS `billing_region_id`,
	       `bill`.`postcode` AS `billing_postcode`,
	       `bill`.`country_id` AS `billing_country_id`,
	       `bill`.`telephone` AS `billing_telephone`,
	       `bill`.`fax` AS `billing_fax`,
	       `ship`.`email` AS `shipping_email`,
	       `ship`.`company` AS `shipping_company`,
	       `ship`.`prefix` AS `shipping_prefix`,
	       `ship`.`firstname` AS `shipping_firstname`,
	       `ship`.`middlename` AS `shipping_middlename`,
	       `ship`.`lastname` AS `shipping_lastname`,
	       `ship`.`suffix` AS `shipping_suffix`,
	       SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},1) AS `shipping_street1`,
	       (CASE
	            WHEN (LOCATE({$this->newLine},`ship`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`ship`.`street`) + 1))
	            ELSE ''
	        END) AS `shipping_street2`,
	       `ship`.`city` AS `shipping_city`,
	       `ship`.`region` AS `shipping_region`,
	       `ship`.`region_id` AS `shipping_region_id`,
	       `ship`.`postcode` AS `shipping_postcode`,
	       `ship`.`country_id` AS `shipping_country_id`,
	       `ship`.`telephone` AS `shipping_telephone`,
	       `ship`.`fax` AS `shipping_fax`,
	       `ov`.`has_lens` AS `has_lens`,
	       `ov`.`total_qty` AS `total_qty`,
	       `ov`.`prof_fee` AS `local_prof_fee`,
	       `ov`.`local_subtotal_inc_vat` AS `local_subtotal_inc_vat`,
	       `ov`.`local_subtotal_vat` AS `local_subtotal_vat`,
	       `ov`.`local_subtotal_exc_vat` AS `local_subtotal_exc_vat`,
	       `ov`.`local_shipping_inc_vat` AS `local_shipping_inc_vat`,
	       `ov`.`local_shipping_vat` AS `local_shipping_vat`,
	       `ov`.`local_shipping_exc_vat` AS `local_shipping_exc_vat`,
	       (-(1) * ABS(`ov`.`local_discount_inc_vat`)) AS `local_discount_inc_vat`,
	       (-(1) * ABS(`ov`.`local_discount_vat`)) AS `local_discount_vat`,
	       (-(1) * ABS(`ov`.`local_discount_exc_vat`)) AS `local_discount_exc_vat`,
	       (-(1) * ABS(`ov`.`local_store_credit_inc_vat`)) AS `local_store_credit_inc_vat`,
	       (-(1) * ABS(`ov`.`local_store_credit_vat`)) AS `local_store_credit_vat`,
	       (-(1) * ABS(`ov`.`local_store_credit_exc_vat`)) AS `local_store_credit_exc_vat`,
	       `ov`.`local_adjustment_inc_vat` AS `local_adjustment_inc_vat`,
	       `ov`.`local_adjustment_vat` AS `local_adjustment_vat`,
	       `ov`.`local_adjustment_exc_vat` AS `local_adjustment_exc_vat`,
	       `ov`.`local_total_inc_vat` AS `local_total_inc_vat`,
	       `ov`.`local_total_vat` AS `local_total_vat`,
	       `ov`.`local_total_exc_vat` AS `local_total_exc_vat`,
	       `ov`.`local_subtotal_cost` AS `local_subtotal_cost`,
	       `ov`.`local_shipping_cost` AS `local_shipping_cost`,
	       `ov`.`local_total_cost` AS `local_total_cost`,
	       `ov`.`local_margin_amount` AS `local_margin_amount`,
	       `ov`.`local_margin_percent` AS `local_margin_percent`,
	       `oh`.`base_to_global_rate` AS `local_to_global_rate`,
	       `oh`.`order_currency_code` AS `order_currency_code`,
	       `ov`.`global_prof_fee` AS `global_prof_fee`,
	       `ov`.`global_subtotal_inc_vat` AS `global_subtotal_inc_vat`,
	       `ov`.`global_subtotal_vat` AS `global_subtotal_vat`,
	       `ov`.`global_subtotal_exc_vat` AS `global_subtotal_exc_vat`,
	       `ov`.`global_shipping_inc_vat` AS `global_shipping_inc_vat`,
	       `ov`.`global_shipping_vat` AS `global_shipping_vat`,
	       `ov`.`global_shipping_exc_vat` AS `global_shipping_exc_vat`,
	       (-(1) * ABS(`ov`.`global_discount_inc_vat`)) AS `global_discount_inc_vat`,
	       (-(1) * ABS(`ov`.`global_discount_vat`)) AS `global_discount_vat`,
	       (-(1) * ABS(`ov`.`global_discount_exc_vat`)) AS `global_discount_exc_vat`,
	       (-(1) * ABS(`ov`.`global_store_credit_inc_vat`)) AS `global_store_credit_inc_vat`,
	       (-(1) * ABS(`ov`.`global_store_credit_vat`)) AS `global_store_credit_vat`,
	       (-(1) * ABS(`ov`.`global_store_credit_exc_vat`)) AS `global_store_credit_exc_vat`,
	       `ov`.`global_adjustment_inc_vat` AS `global_adjustment_inc_vat`,
	       `ov`.`global_adjustment_vat` AS `global_adjustment_vat`,
	       `ov`.`global_adjustment_exc_vat` AS `global_adjustment_exc_vat`,
	       `ov`.`global_total_inc_vat` AS `global_total_inc_vat`,
	       `ov`.`global_total_vat` AS `global_total_vat`,
	       `ov`.`global_total_exc_vat` AS `global_total_exc_vat`,
	       `ov`.`global_subtotal_cost` AS `global_subtotal_cost`,
	       `ov`.`global_shipping_cost` AS `global_shipping_cost`,
	       `ov`.`global_total_cost` AS `global_total_cost`,
	       `ov`.`global_margin_amount` AS `global_margin_amount`,
	       `ov`.`global_margin_percent` AS `global_margin_percent`,
	       `ov`.`prof_fee_percent` AS `prof_fee_percent`,
	       `ov`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `ov`.`vat_percent` AS `vat_percent`,
	       ABS(`ov`.`discount_percent`) AS `discount_percent`,
	       CAST(`oh`.`customer_note` AS CHAR(8000) CHARSET latin1) AS `customer_note`,
	       CAST(`oh`.`customer_note_notify` AS CHAR(8000) CHARSET latin1) AS `customer_note_notify`,
	       `oh`.`remote_ip` AS `remote_ip`,
	       `oh`.`affilCode` AS `business_source`,
	       `ochan`.`channel` AS `business_channel`,
	       `oh`.`affilBatch` AS `affilBatch`,
	       `oh`.`affilCode` AS `affilCode`,
	       `oh`.`affilUserRef` AS `affilUserRef`,
	       `oh`.`postoptics_auto_verification` AS `auto_verification`,
	       (CASE
	            WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
	            WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
	            WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
	            WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
	            ELSE `oh`.`postoptics_source`
	        END) AS `order_type`,
	       `ohm`.`ORDER_LIFECYCLE` AS `order_lifecycle`,
	       `seq`.`ORDER_SEQ` AS `customer_order_seq_no`,
	       (CASE
	            WHEN (TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
	            ELSE NULL
	        END) AS `reminder_date`,
	       `oh`.`reminder_mobile` AS `reminder_mobile`,
	       `oh`.`reminder_period` AS `reminder_period`,
	       `oh`.`reminder_presc` AS `reminder_presc`,
	       `oh`.`reminder_type` AS `reminder_type`,
	       `oh`.`reminder_sent` AS `reminder_sent`,
	       `oh`.`reminder_follow_sent` AS `reminder_follow_sent`,
	       `oh`.`telesales_method_code` AS `telesales_method_code`,
	       `oh`.`telesales_admin_username` AS `telesales_admin_username`,
	       `oh`.`reorder_on_flag` AS `reorder_on_flag`,
	       `oh`.`reorder_profile_id` AS `reorder_profile_id`,
	       (CASE
	            WHEN ((CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
	                  OR (`rp`.`completed_profile` <> 1)) THEN NULL
	            ELSE `rp`.`next_order_date`
	        END) AS `reorder_date`,
	       `rp`.`interval_days` AS `reorder_interval`,
	       `rqp`.`cc_exp_month` AS `reorder_cc_exp_month`,
	       `rqp`.`cc_exp_year` AS `reorder_cc_exp_year`,
	       `oh`.`automatic_reorder` AS `automatic_reorder`,
	       (CASE
	            WHEN (((`oh`.`presc_verification_method` = 'call')
	                   OR ISNULL(`oh`.`presc_verification_method`))
	                  AND (`conf`.`value` = 1)
	                  AND (`ov2`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Call'
	            WHEN ((`oh`.`presc_verification_method` = 'upload')
	                  AND (`conf`.`value` = 1)
	                  AND (`ov2`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Upload / Fax / Scan'
	            WHEN ((`conf`.`value` = 1)
	                  AND (`ov2`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` = 'Yes')) THEN 'Automatic'
	            ELSE 'Not Required'
	        END) AS `presc_verification_method`,
	       `oh`.`referafriend_code` AS `referafriend_code`,
	       `oh`.`referafriend_referer` AS `referafriend_referer`,
	       GREATEST(`oh`.`dw_synced_at`,`inv`.`dw_synced_at`) AS `dw_synced_at`,
	       CAST(CONCAT('INVOICE',`inv`.`invoice_id`) AS CHAR(25) CHARSET latin1) as unique_id
	FROM ((((((((((((((((((`dw_invoice_headers` `inv`
	                       JOIN `dw_order_headers` `oh` ON((`oh`.`order_id` = `inv`.`order_id`)))
	                      LEFT JOIN `dw_stores` `cw` ON((`cw`.`store_id` = `oh`.`store_id`)))
	                     LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op` ON((`op`.`parent_id` = `oh`.`order_id`)))
	                    LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill` ON((`bill`.`entity_id` = `oh`.`billing_address_id`)))
	                   LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship` ON((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
	                  JOIN `invoice_headers_vat` `ov` ON((`ov`.`invoice_id` = `inv`.`invoice_id`)))
	                 JOIN `order_headers_vat` `ov2` ON((`ov2`.`order_id` = `oh`.`order_id`)))
	                LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp` ON((`oh`.`reorder_profile_id` = `rp`.`id`)))
	               LEFT JOIN `dw_invoice_calender_dates` `ic` ON((`ic`.`invoice_id` = `inv`.`invoice_id`)))
	              LEFT JOIN `dw_payment_method_map` `pm` ON((CONVERT(`pm`.`payment_method` USING utf8) = `op`.`method`)))
	             LEFT JOIN `dw_card_type_map` `ctm` ON((CONVERT(`ctm`.`card_type` USING utf8) = `op`.`cc_type`)))
	            LEFT JOIN `{$this->dbname}`.`po_reorder_quote` `rq` ON((`rq`.`entity_id` = `rp`.`reorder_quote_id`)))
	           LEFT JOIN `max_reorder_quote_payment` `mp` ON((`rq`.`entity_id` = `mp`.`quote_id`)))
	          LEFT JOIN `{$this->dbname}`.`po_reorder_quote_payment` `rqp` ON((`rqp`.`payment_id` = `mp`.`payment_id`)))
	         LEFT JOIN `dw_order_channel` `ochan` ON((`ochan`.`order_id` = `oh`.`order_id`)))
	        LEFT JOIN `dw_order_headers_marketing` `ohm` ON((`ohm`.`ORDER_ID` = `oh`.`order_id`)))
	       LEFT JOIN `dw_entity_header_rank_seq` `seq` ON(((`seq`.`DOCUMENT_ID` = `oh`.`order_id`)
	                                                                 AND (`seq`.`DOCUMENT_TYPE` = 'ORDER'))))
	      LEFT JOIN `dw_conf_presc_required` `conf` ON((`conf`.`store_id` = `oh`.`store_id`)));





		CREATE OR REPLACE VIEW vw_invoice_headers_creditmemo AS
		SELECT `cw`.`store_name` AS `store_name`,
	       IFNULL(`cred`.`invoice_id`,-(1)) AS `invoice_id`,
	       `inv`.`invoice_no` AS `invoice_no`,
	       CAST('CREDITMEMO' AS CHAR(25) CHARSET latin1) AS `document_type`,
	       `cred`.`created_at` AS `document_date`,
	       `cred`.`updated_at` AS `document_updated_at`,
	       `cred`.`creditmemo_id` AS `document_id`,
	       `cred`.`creditmemo_no` AS `document_no`,
	       `cred`.`created_at` AS `invoice_date`,
	       `cred`.`updated_at` AS `updated_at`,
	       `oh`.`order_id` AS `order_id`,
	       `oh`.`order_no` AS `order_no`,
	       `oh`.`created_at` AS `order_date`,
	       `oh`.`customer_id` AS `customer_id`,
	       `oh`.`customer_email` AS `customer_email`,
	       `oh`.`customer_firstname` AS `customer_firstname`,
	       `oh`.`customer_lastname` AS `customer_lastname`,
	       `oh`.`customer_middlename` AS `customer_middlename`,
	       `oh`.`customer_prefix` AS `customer_prefix`,
	       `oh`.`customer_suffix` AS `customer_suffix`,
	       `oh`.`customer_taxvat` AS `customer_taxvat`,
	       `oh`.`status` AS `status`,
	       `oh`.`coupon_code` AS `coupon_code`,
	       (-(1) * `ov`.`total_weight`) AS `weight`,
	       `oh`.`customer_gender` AS `customer_gender`,
	       `oh`.`customer_dob` AS `customer_dob`,
	       LEFT(`oh`.`shipping_description`,LOCATE(' - ',`oh`.`shipping_description`)) AS `shipping_carrier`,
	       `oh`.`shipping_description` AS `shipping_method`,
	       (CASE `oh`.`warehouse_approved_time`
	            WHEN '0000-00-00 00:00:00' THEN NULL
	            ELSE `oh`.`warehouse_approved_time`
	        END) AS `approved_time`,
	       `oc`.`length_of_time_to_invoice` AS `length_of_time_to_invoice`,
	       (CASE
	            WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
	            ELSE IFNULL(CONVERT(`pm`.`payment_method_name` USING utf8),`op`.`method`)
	        END) AS `payment_method`,
	       `op`.`amount_authorized` AS `local_payment_authorized`,
	       `op`.`amount_canceled` AS `local_payment_canceled`,
	       `op`.`cc_exp_month` AS `cc_exp_month`,
	       `op`.`cc_exp_year` AS `cc_exp_year`,
	       `op`.`cc_ss_start_month` AS `cc_start_month`,
	       `op`.`cc_ss_start_year` AS `cc_start_year`,
	       `op`.`cc_last4` AS `cc_last4`,
	       `op`.`cc_status_description` AS `cc_status_description`,
	       `op`.`cc_owner` AS `cc_owner`,
	       IFNULL(CONVERT(`ctm`.`card_type_name` USING utf8),`op`.`cc_type`) AS `cc_type`,
	       `op`.`cc_status` AS `cc_status`,
	       `op`.`cc_ss_issue` AS `cc_issue_no`,
	       `op`.`cc_avs_status` AS `cc_avs_status`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info1`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info2`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info3`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info4`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info5`,
	       `bill`.`email` AS `billing_email`,
	       `bill`.`company` AS `billing_company`,
	       `bill`.`prefix` AS `billing_prefix`,
	       `bill`.`firstname` AS `billing_firstname`,
	       `bill`.`middlename` AS `billing_middlename`,
	       `bill`.`lastname` AS `billing_lastname`,
	       `bill`.`suffix` AS `billing_suffix`,
	       SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},1) AS `billing_street1`,
	       (CASE
	            WHEN (LOCATE({$this->newLine},`bill`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`bill`.`street`) + 1))
	            ELSE ''
	        END) AS `billing_street2`,
	       `bill`.`city` AS `billing_city`,
	       `bill`.`region` AS `billing_region`,
	       `bill`.`region_id` AS `billing_region_id`,
	       `bill`.`postcode` AS `billing_postcode`,
	       `bill`.`country_id` AS `billing_country_id`,
	       `bill`.`telephone` AS `billing_telephone`,
	       `bill`.`fax` AS `billing_fax`,
	       `ship`.`email` AS `shipping_email`,
	       `ship`.`company` AS `shipping_company`,
	       `ship`.`prefix` AS `shipping_prefix`,
	       `ship`.`firstname` AS `shipping_firstname`,
	       `ship`.`middlename` AS `shipping_middlename`,
	       `ship`.`lastname` AS `shipping_lastname`,
	       `ship`.`suffix` AS `shipping_suffix`,
	       SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},1) AS `shiping_street1`,
	       (CASE
	            WHEN (LOCATE({$this->newLine},`ship`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`ship`.`street`) + 1))
	            ELSE ''
	        END) AS `shipping_street2`,
	       `ship`.`city` AS `shipping_city`,
	       `ship`.`region` AS `shipping_region`,
	       `ship`.`region_id` AS `shipping_region_id`,
	       `ship`.`postcode` AS `shipping_postcode`,
	       `ship`.`country_id` AS `shipping_country_id`,
	       `ship`.`telephone` AS `shipping_telephone`,
	       `ship`.`fax` AS `shipping_fax`,
	       `ov`.`has_lens` AS `has_lens`,
	       (-(1) * `ov`.`total_qty`) AS `total_qty`,
	       (-(1) * ABS(`ov`.`prof_fee`)) AS `local_prof_fee`,
	       (-(1) * ABS(`ov`.`local_subtotal_inc_vat`)) AS `local_subtotal_inc_vat`,
	       (-(1) * ABS(`ov`.`local_subtotal_vat`)) AS `local_subtotal_vat`,
	       (-(1) * ABS(`ov`.`local_subtotal_exc_vat`)) AS `local_subtotal_exc_vat`,
	       (-(1) * ABS(`ov`.`local_shipping_inc_vat`)) AS `local_shipping_inc_vat`,
	       (-(1) * ABS(`ov`.`local_shipping_vat`)) AS `local_shipping_vat`,
	       (-(1) * ABS(`ov`.`local_shipping_exc_vat`)) AS `local_shipping_exc_vat`,
	       ABS(`ov`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
	       ABS(`ov`.`local_discount_vat`) AS `local_discount_vat`,
	       ABS(`ov`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
	       ABS(`ov`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
	       ABS(`ov`.`local_store_credit_vat`) AS `local_store_credit_vat`,
	       ABS(`ov`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
	       (-(1) * `ov`.`local_adjustment_inc_vat`) AS `local_adjustment_inc_vat`,
	       (-(1) * `ov`.`local_adjustment_vat`) AS `local_adjustment_vat`,
	       (-(1) * `ov`.`local_adjustment_exc_vat`) AS `local_adjustment_exc_vat`,
	       (-(1) * `ov`.`local_total_inc_vat`) AS `local_total_inc_vat`,
	       (-(1) * `ov`.`local_total_vat`) AS `local_total_vat`,
	       (-(1) * `ov`.`local_total_exc_vat`) AS `local_total_exc_vat`,
	       (-(1) * ABS(`ov`.`local_subtotal_cost`)) AS `local_subtotal_cost`,
	       (-(1) * ABS(`ov`.`local_shipping_cost`)) AS `local_shipping_cost`,
	       (-(1) * ABS(`ov`.`local_total_cost`)) AS `local_total_cost`,
	       (-(1) * ABS(`ov`.`local_margin_amount`)) AS `local_margin_amount`,
	       `ov`.`local_margin_percent` AS `local_margin_percent`,
	       `oh`.`base_to_global_rate` AS `local_to_global_rate`,
	       `oh`.`order_currency_code` AS `order_currency_code`,
	       (-(1) * ABS(`ov`.`global_prof_fee`)) AS `global_prof_fee`,
	       (-(1) * ABS(`ov`.`global_subtotal_inc_vat`)) AS `global_subtotal_inc_vat`,
	       (-(1) * ABS(`ov`.`global_subtotal_vat`)) AS `global_subtotal_vat`,
	       (-(1) * ABS(`ov`.`global_subtotal_exc_vat`)) AS `global_subtotal_exc_vat`,
	       (-(1) * ABS(`ov`.`global_shipping_inc_vat`)) AS `global_shipping_inc_vat`,
	       (-(1) * ABS(`ov`.`global_shipping_vat`)) AS `global_shipping_vat`,
	       (-(1) * ABS(`ov`.`global_shipping_exc_vat`)) AS `global_shipping_exc_vat`,
	       ABS(`ov`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
	       ABS(`ov`.`global_discount_vat`) AS `global_discount_vat`,
	       ABS(`ov`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
	       ABS(`ov`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
	       ABS(`ov`.`global_store_credit_vat`) AS `global_store_credit_vat`,
	       ABS(`ov`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
	       (-(1) * `ov`.`global_adjustment_inc_vat`) AS `global_adjustment_inc_vat`,
	       (-(1) * `ov`.`global_adjustment_vat`) AS `global_adjustment_vat`,
	       (-(1) * `ov`.`global_adjustment_exc_vat`) AS `global_adjustment_exc_vat`,
	       (-(1) * `ov`.`global_total_inc_vat`) AS `global_total_inc_vat`,
	       (-(1) * `ov`.`global_total_vat`) AS `global_total_vat`,
	       (-(1) * `ov`.`global_total_exc_vat`) AS `global_total_exc_vat`,
	       (-(1) * ABS(`ov`.`global_subtotal_cost`)) AS `global_subtotal_cost`,
	       (-(1) * ABS(`ov`.`global_shipping_cost`)) AS `global_shipping_cost`,
	       (-(1) * ABS(`ov`.`global_total_cost`)) AS `global_total_cost`,
	       (-(1) * ABS(`ov`.`global_margin_amount`)) AS `global_margin_amount`,
	       `ov`.`global_margin_percent` AS `global_margin_percent`,
	       `ov`.`prof_fee_percent` AS `prof_fee_percent`,
	       `ov`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `ov`.`vat_percent` AS `vat_percent`,
	       ABS(`ov`.`discount_percent`) AS `discount_percent`,
	       CAST(`oh`.`customer_note` AS CHAR(8000) CHARSET latin1) AS `customer_note`,
	       CAST(`oh`.`customer_note_notify` AS CHAR(8000) CHARSET latin1) AS `customer_note_notify`,
	       `oh`.`remote_ip` AS `remote_ip`,
	       `oh`.`affilCode` AS `business_source`,
	       `ochan`.`channel` AS `business_channel`,
	       `oh`.`affilBatch` AS `affilBatch`,
	       `oh`.`affilCode` AS `affilCode`,
	       `oh`.`affilUserRef` AS `affilUserRef`,
	       `oh`.`postoptics_auto_verification` AS `auto_verification`,
	       (CASE
	            WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
	            WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
	            WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
	            WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
	            ELSE `oh`.`postoptics_source`
	        END) AS `order_type`,
	       `ohm`.`ORDER_LIFECYCLE` AS `order_lifecycle`,
	       `seq`.`ORDER_SEQ` AS `customer_order_seq_no`,
	       (CASE
	            WHEN (TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
	            ELSE NULL
	        END) AS `reminder_date`,
	       `oh`.`reminder_mobile` AS `reminder_mobile`,
	       `oh`.`reminder_period` AS `reminder_period`,
	       `oh`.`reminder_presc` AS `reminder_presc`,
	       `oh`.`reminder_type` AS `reminder_type`,
	       `oh`.`reminder_sent` AS `reminder_sent`,
	       `oh`.`reminder_follow_sent` AS `reminder_follow_sent`,
	       `oh`.`telesales_method_code` AS `telesales_method_code`,
	       `oh`.`telesales_admin_username` AS `telesales_admin_username`,
	       `oh`.`reorder_on_flag` AS `reorder_on_flag`,
	       `oh`.`reorder_profile_id` AS `reorder_profile_id`,
	       (CASE
	            WHEN ((CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
	                  OR (`rp`.`completed_profile` <> 1)) THEN NULL
	            ELSE `rp`.`next_order_date`
	        END) AS `reorder_date`,
	       `rp`.`interval_days` AS `reorder_interval`,
	       `rqp`.`cc_exp_month` AS `reorder_cc_exp_month`,
	       `rqp`.`cc_exp_year` AS `reorder_cc_exp_year`,
	       `oh`.`automatic_reorder` AS `automatic_reorder`,
	       (CASE
	            WHEN (((`oh`.`presc_verification_method` = 'call')
	                   OR ISNULL(`oh`.`presc_verification_method`))
	                  AND (`conf`.`value` = 1)
	                  AND (`ov2`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Call'
	            WHEN ((`oh`.`presc_verification_method` = 'upload')
	                  AND (`conf`.`value` = 1)
	                  AND (`ov2`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Upload / Fax / Scan'
	            WHEN ((`conf`.`value` = 1)
	                  AND (`ov2`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` = 'Yes')) THEN 'Automatic'
	            ELSE 'Not Required'
	        END) AS `presc_verification_method`,
	       `oh`.`referafriend_code` AS `referafriend_code`,
	       `oh`.`referafriend_referer` AS `referafriend_referer`,
	       GREATEST(`oh`.`dw_synced_at`,`cred`.`dw_synced_at`) AS `dw_synced_at`,
	       CAST(CONCAT('CREDITMEMO',`cred`.`creditmemo_id`) AS CHAR(25) CHARSET latin1) as unique_id

	FROM ((((((((((((((((((((`dw_creditmemo_headers` `cred`
	                         JOIN `dw_order_headers` `oh` ON((`oh`.`order_id` = `cred`.`order_id`)))
	                        LEFT JOIN `dw_stores` `cw` ON((`cw`.`store_id` = `oh`.`store_id`)))
	                       LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op` ON((`op`.`parent_id` = `oh`.`order_id`)))
	                      LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill` ON((`bill`.`entity_id` = `oh`.`billing_address_id`)))
	                     LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship` ON((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
	                    JOIN `creditmemo_shipment_headers_vat` `ov` ON((`ov`.`creditmemo_id` = `cred`.`creditmemo_id`)))
	                   JOIN `order_headers_vat` `ov2` ON((`ov2`.`order_id` = `oh`.`order_id`)))
	                  LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp` ON((`oh`.`reorder_profile_id` = `rp`.`id`)))
	                 LEFT JOIN `dw_invoice_calender_dates` `ic` ON((`ic`.`invoice_id` = `cred`.`invoice_id`)))
	                LEFT JOIN `dw_invoice_headers` `inv` ON((`inv`.`invoice_id` = `cred`.`invoice_id`)))
	               LEFT JOIN `dw_invoice_calender_dates` `oc` ON((`oc`.`invoice_id` = `inv`.`invoice_id`)))
	              LEFT JOIN `dw_payment_method_map` `pm` ON((CONVERT(`pm`.`payment_method` USING utf8) = `op`.`method`)))
	             LEFT JOIN `dw_card_type_map` `ctm` ON((CONVERT(`ctm`.`card_type` USING utf8) = `op`.`cc_type`)))
	            LEFT JOIN `{$this->dbname}`.`po_reorder_quote` `rq` ON((`rq`.`entity_id` = `rp`.`reorder_quote_id`)))
	           LEFT JOIN `max_reorder_quote_payment` `mp` ON((`rq`.`entity_id` = `mp`.`quote_id`)))
	          LEFT JOIN `{$this->dbname}`.`po_reorder_quote_payment` `rqp` ON((`rqp`.`payment_id` = `mp`.`payment_id`)))
	         LEFT JOIN `dw_order_channel` `ochan` ON((`ochan`.`order_id` = `oh`.`order_id`)))
	        LEFT JOIN `dw_order_headers_marketing` `ohm` ON((`ohm`.`ORDER_ID` = `oh`.`order_id`)))
	       LEFT JOIN `dw_entity_header_rank_seq` `seq` ON(((`seq`.`DOCUMENT_ID` = `cred`.`creditmemo_id`)
	                                                                 AND (`seq`.`DOCUMENT_TYPE` = 'CREDITMEMO'))))
	      LEFT JOIN `dw_conf_presc_required` `conf` ON((`conf`.`store_id` = `oh`.`store_id`)));




		CREATE OR REPLACE VIEW vw_invoice_lines_invoice AS
		SELECT `ol`.`item_id` AS `line_id`,
	       `ol`.`invoice_id` AS `invoice_id`,
	       `cw`.`store_name` AS `store_name`,
	       `inv`.`invoice_no` AS `invoice_no`,
	       CAST('INVOICE' AS CHAR(25) CHARSET latin1) AS `document_type`,
	       `inv`.`created_at` AS `document_date`,
	       `inv`.`updated_at` AS `document_updated_at`,
	       `inv`.`invoice_id` AS `document_id`,
	       `inv`.`created_at` AS `invoice_date`,
	       `inv`.`updated_at` AS `updated_at`,
	       `ol`.`product_id` AS `product_id`,
	       `cf`.`product_type` AS `product_type`,
	       CAST(`ol2`.`product_options` AS CHAR(8000) CHARSET latin1) AS `product_options`,
	       `p`.`is_lens` AS `is_lens`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `lens_eye`,
	       `esi`.`BC` AS `lens_base_curve`,
	       `esi`.`DI` AS `lens_diameter`,
	       `esi`.`PO` AS `lens_power`,
	       `esi`.`CY` AS `lens_cylinder`,
	       `esi`.`AX` AS `lens_axis`,
	       `esi`.`AD` AS `lens_addition`,
	       `esi`.`DO` AS `lens_dominance`,
	       `esi`.`CO` AS `lens_colour`,
	       `p`.`daysperlens` AS `lens_days`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_size`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_colour`,
	       `ol2`.`weight` AS `unit_weight`,
	       (`ol2`.`weight` * `ol`.`qty`) AS `line_weight`,
	       `ol2`.`is_virtual` AS `is_virtual`,
	       `ol`.`sku` AS `sku`,
	       `ol`.`name` AS `name`,
	       `pcf`.`category_id` AS `category_id`,
	       (CASE
	            WHEN (`oh`.`base_shipping_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `free_shipping`,
	       (CASE
	            WHEN (`oh`.`base_discount_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `no_discount`,
	       `ol`.`qty` AS `qty`,
	       `olv`.`local_prof_fee` AS `local_prof_fee`,
	       `olv`.`local_price_inc_vat` AS `local_price_inc_vat`,
	       `olv`.`local_price_vat` AS `local_price_vat`,
	       `olv`.`local_price_exc_vat` AS `local_price_exc_vat`,
	       `olv`.`local_line_subtotal_inc_vat` AS `local_line_subtotal_inc_vat`,
	       `olv`.`local_line_subtotal_vat` AS `local_line_subtotal_vat`,
	       `olv`.`local_line_subtotal_exc_vat` AS `local_line_subtotal_exc_vat`,
	       `olv`.`local_shipping_inc_vat` AS `local_shipping_inc_vat`,
	       `olv`.`local_shipping_vat` AS `local_shipping_vat`,
	       `olv`.`local_shipping_exc_vat` AS `local_shipping_exc_vat`,
	       (-(1) * ABS(`olv`.`local_discount_inc_vat`)) AS `local_discount_inc_vat`,
	       (-(1) * ABS(`olv`.`local_discount_vat`)) AS `local_discount_vat`,
	       (-(1) * ABS(`olv`.`local_discount_exc_vat`)) AS `local_discount_exc_vat`,
	       (-(1) * ABS(`olv`.`local_store_credit_inc_vat`)) AS `local_store_credit_inc_vat`,
	       (-(1) * ABS(`olv`.`local_store_credit_vat`)) AS `local_store_credit_vat`,
	       (-(1) * ABS(`olv`.`local_store_credit_exc_vat`)) AS `local_store_credit_exc_vat`,
	       `olv`.`local_adjustment_inc_vat` AS `local_adjustment_inc_vat`,
	       `olv`.`local_adjustment_vat` AS `local_adjustment_vat`,
	       `olv`.`local_adjustment_exc_vat` AS `local_adjustment_exc_vat`,
	       `olv`.`local_line_total_inc_vat` AS `local_line_total_inc_vat`,
	       `olv`.`local_line_total_vat` AS `local_line_total_vat`,
	       `olv`.`local_line_total_exc_vat` AS `local_line_total_exc_vat`,
	       `olv`.`local_subtotal_cost` AS `local_subtotal_cost`,
	       `olv`.`local_shipping_cost` AS `local_shipping_cost`,
	       `olv`.`local_total_cost` AS `local_total_cost`,
	       `olv`.`local_margin_amount` AS `local_margin_amount`,
	       `olv`.`local_margin_percent` AS `local_margin_percent`,
	       `olv`.`local_to_global_rate` AS `local_to_global_rate`,
	       `olv`.`order_currency_code` AS `order_currency_code`,
	       `olv`.`global_prof_fee` AS `global_prof_fee`,
	       `olv`.`global_price_inc_vat` AS `global_price_inc_vat`,
	       `olv`.`global_price_vat` AS `global_price_vat`,
	       `olv`.`global_price_exc_vat` AS `global_price_exc_vat`,
	       `olv`.`global_line_subtotal_inc_vat` AS `global_line_subtotal_inc_vat`,
	       `olv`.`global_line_subtotal_vat` AS `global_line_subtotal_vat`,
	       `olv`.`global_line_subtotal_exc_vat` AS `global_line_subtotal_exc_vat`,
	       `olv`.`global_shipping_inc_vat` AS `global_shipping_inc_vat`,
	       `olv`.`global_shipping_vat` AS `global_shipping_vat`,
	       `olv`.`global_shipping_exc_vat` AS `global_shipping_exc_vat`,
	       (-(1) * ABS(`olv`.`global_discount_inc_vat`)) AS `global_discount_inc_vat`,
	       (-(1) * ABS(`olv`.`global_discount_vat`)) AS `global_discount_vat`,
	       (-(1) * ABS(`olv`.`global_discount_exc_vat`)) AS `global_discount_exc_vat`,
	       (-(1) * ABS(`olv`.`global_store_credit_inc_vat`)) AS `global_store_credit_inc_vat`,
	       (-(1) * ABS(`olv`.`global_store_credit_vat`)) AS `global_store_credit_vat`,
	       (-(1) * ABS(`olv`.`global_store_credit_exc_vat`)) AS `global_store_credit_exc_vat`,
	       `olv`.`global_adjustment_inc_vat` AS `global_adjustment_inc_vat`,
	       `olv`.`global_adjustment_vat` AS `global_adjustment_vat`,
	       `olv`.`global_adjustment_exc_vat` AS `global_adjustment_exc_vat`,
	       `olv`.`global_line_total_inc_vat` AS `global_line_total_inc_vat`,
	       `olv`.`global_line_total_vat` AS `global_line_total_vat`,
	       `olv`.`global_line_total_exc_vat` AS `global_line_total_exc_vat`,
	       `olv`.`global_subtotal_cost` AS `global_subtotal_cost`,
	       `olv`.`global_shipping_cost` AS `global_shipping_cost`,
	       `olv`.`global_total_cost` AS `global_total_cost`,
	       `olv`.`global_margin_amount` AS `global_margin_amount`,
	       `olv`.`global_margin_percent` AS `global_margin_percent`,
	       `olv`.`prof_fee_percent` AS `prof_fee_percent`,
	       `olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `olv`.`vat_percent` AS `vat_percent`,
	       ABS(`olv`.`discount_percent`) AS `discount_percent`,
	       GREATEST(`ol`.`dw_synced_at`,`inv`.`dw_synced_at`,`oh`.`dw_synced_at`,`ol2`.`dw_synced_at`) AS `dw_synced_at` ,
	       CAST(CONCAT('INVOICE',`ol`.`item_id`) AS CHAR(25) CHARSET latin1) as unique_id
	FROM (((((((((((((((`dw_invoice_lines` `ol`
	                    JOIN `dw_invoice_headers` `inv` ON ((`inv`.`invoice_id` = `ol`.`invoice_id`)))
	                   JOIN `dw_order_headers` `oh` ON ((`oh`.`order_id` = `inv`.`order_id`)))
	                  JOIN `invoice_lines_vat` `olv` ON ((`olv`.`item_id` = `ol`.`item_id`)))
	                 LEFT JOIN `dw_order_lines` `ol2` ON ((`ol2`.`item_id` = `ol`.`order_line_id`)))
	                LEFT JOIN `dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
	               LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op` ON ((`op`.`parent_id` = `oh`.`order_id`)))
	              LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill` ON ((`bill`.`entity_id` = `oh`.`billing_address_id`)))
	             LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship` ON ((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
	            JOIN `invoice_headers_vat` `ov` ON ((`ov`.`invoice_id` = `inv`.`invoice_id`)))
	           LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp` ON ((`oh`.`reorder_profile_id` = `rp`.`id`)))
	          LEFT JOIN `dw_invoice_calender_dates` `ic` ON ((`ic`.`invoice_id` = `inv`.`invoice_id`)))
	         LEFT JOIN `dw_products` `p` ON ((`p`.`product_id` = `ol`.`product_id`)))
	        LEFT JOIN `edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `ol`.`product_id`)
	                                                            AND (`esi`.`product_code` = `ol`.`sku`))))
	       LEFT JOIN `dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
	      LEFT JOIN `dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)));





		CREATE OR REPLACE VIEW vw_invoice_lines_creditmemo AS
		SELECT `ol`.`item_id` AS `line_id`,
	       `oh`.`order_id` AS `order_id`,
	       `cw`.`store_name` AS `store_name`,
	       `oh`.`order_no` AS `order_no`,
	       'CREDITMEMO' AS `document_type`,
	       `oh2`.`created_at` AS `document_date`,
	       `oh2`.`updated_at` AS `document_updated_at`,
	       `oh2`.`creditmemo_id` AS `document_id`,
	       `oh`.`created_at` AS `order_date`,
	       `oh`.`updated_at` AS `updated_at`,
	       `ol`.`product_id` AS `product_id`,
	       `cf`.`product_type` AS `product_type`,
	       CAST(`ol2`.`product_options` AS CHAR(8000) CHARSET latin1) AS `product_options`,
	       `p`.`is_lens` AS `is_lens`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `lens_eye`,
	       `esi`.`BC` AS `lens_base_curve`,
	       `esi`.`DI` AS `lens_diameter`,
	       `esi`.`PO` AS `lens_power`,
	       `esi`.`CY` AS `lens_cylinder`,
	       `esi`.`AX` AS `lens_axis`,
	       `esi`.`AD` AS `lens_addition`,
	       `esi`.`DO` AS `lens_dominance`,
	       `esi`.`CO` AS `lens_colour`,
	       `p`.`daysperlens` AS `lens_days`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_size`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_colour`,
	       (-(1) * `ol2`.`weight`) AS `unit_weight`,
	       ((-(1) * `ol2`.`weight`) * `ol`.`qty`) AS `line_weight`,
	       `ol2`.`is_virtual` AS `is_virtual`,
	       `ol`.`sku` AS `sku`,
	       `ol`.`name` AS `name`,
	       `pcf`.`category_id` AS `category_id`,
	       (CASE
	            WHEN (`oh`.`base_shipping_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `free_shipping`,
	       (CASE
	            WHEN (`oh`.`base_discount_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `no_discount`,
	       (-(1) * `ol`.`qty`) AS `qty_ordered`,
	       (-(1) * `olv`.`local_prof_fee`) AS `local_prof_fee`,
	       `olv`.`local_price_inc_vat` AS `local_price_inc_vat`,
	       `olv`.`local_price_vat` AS `local_price_vat`,
	       `olv`.`local_price_exc_vat` AS `local_price_exc_vat`,
	       (-(1) * `olv`.`local_line_subtotal_inc_vat`) AS `local_line_subtotal_inc_vat`,
	       (-(1) * `olv`.`local_line_subtotal_vat`) AS `local_line_subtotal_vat`,
	       (-(1) * `olv`.`local_line_subtotal_exc_vat`) AS `local_line_subtotal_exc_vat`,
	       (-(1) * `olv`.`local_shipping_inc_vat`) AS `local_shipping_inc_vat`,
	       (-(1) * `olv`.`local_shipping_vat`) AS `local_shipping_vat`,
	       (-(1) * `olv`.`local_shipping_exc_vat`) AS `local_shipping_exc_vat`,
	       ABS(`olv`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
	       ABS(`olv`.`local_discount_vat`) AS `local_discount_vat`,
	       ABS(`olv`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
	       ABS(`olv`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
	       ABS(`olv`.`local_store_credit_vat`) AS `local_store_credit_vat`,
	       ABS(`olv`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
	       (-(1) * `olv`.`local_adjustment_inc_vat`) AS `local_adjustment_inc_vat`,
	       (-(1) * `olv`.`local_adjustment_vat`) AS `local_adjustment_vat`,
	       (-(1) * `olv`.`local_adjustment_exc_vat`) AS `local_adjustment_exc_vat`,
	       (-(1) * `olv`.`local_line_total_inc_vat`) AS `local_line_total_inc_vat`,
	       (-(1) * `olv`.`local_line_total_vat`) AS `local_line_total_vat`,
	       (-(1) * `olv`.`local_line_total_exc_vat`) AS `local_line_total_exc_vat`,
	       (-(1) * `olv`.`local_subtotal_cost`) AS `local_subtotal_cost`,
	       (-(1) * `olv`.`local_shipping_cost`) AS `local_shipping_cost`,
	       (-(1) * `olv`.`local_total_cost`) AS `local_total_cost`,
	       (-(1) * `olv`.`local_margin_amount`) AS `local_margin_amount`,
	       `olv`.`local_margin_percent` AS `local_margin_percent`,
	       `olv`.`local_to_global_rate` AS `local_to_global_rate`,
	       `olv`.`order_currency_code` AS `order_currency_code`,
	       (-(1) * `olv`.`global_prof_fee`) AS `global_prof_fee`,
	       `olv`.`global_price_inc_vat` AS `global_price_inc_vat`,
	       `olv`.`global_price_vat` AS `global_price_vat`,
	       `olv`.`global_price_exc_vat` AS `global_price_exc_vat`,
	       (-(1) * `olv`.`global_line_subtotal_inc_vat`) AS `global_line_subtotal_inc_vat`,
	       (-(1) * `olv`.`global_line_subtotal_vat`) AS `global_line_subtotal_vat`,
	       (-(1) * `olv`.`global_line_subtotal_exc_vat`) AS `global_line_subtotal_exc_vat`,
	       (-(1) * `olv`.`global_shipping_inc_vat`) AS `global_shipping_inc_vat`,
	       (-(1) * `olv`.`global_shipping_vat`) AS `global_shipping_vat`,
	       (-(1) * `olv`.`global_shipping_exc_vat`) AS `global_shipping_exc_vat`,
	       ABS(`olv`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
	       ABS(`olv`.`global_discount_vat`) AS `global_discount_vat`,
	       ABS(`olv`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
	       ABS(`olv`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
	       ABS(`olv`.`global_store_credit_vat`) AS `global_store_credit_vat`,
	       ABS(`olv`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
	       (-(1) * `olv`.`global_adjustment_inc_vat`) AS `global_adjustment_inc_vat`,
	       (-(1) * `olv`.`global_adjustment_vat`) AS `global_adjustment_vat`,
	       (-(1) * `olv`.`global_adjustment_exc_vat`) AS `global_adjustment_exc_vat`,
	       (-(1) * `olv`.`global_line_total_inc_vat`) AS `global_line_total_inc_vat`,
	       (-(1) * `olv`.`global_line_total_vat`) AS `global_line_total_vat`,
	       (-(1) * `olv`.`global_line_total_exc_vat`) AS `global_line_total_exc_vat`,
	       (-(1) * `olv`.`global_subtotal_cost`) AS `global_subtotal_cost`,
	       (-(1) * `olv`.`global_shipping_cost`) AS `global_shipping_cost`,
	       (-(1) * `olv`.`global_total_cost`) AS `global_total_cost`,
	       (-(1) * `olv`.`global_margin_amount`) AS `global_margin_amount`,
	       `olv`.`global_margin_percent` AS `global_margin_percent`,
	       `olv`.`prof_fee_percent` AS `prof_fee_percent`,
	       `olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `olv`.`vat_percent` AS `vat_percent`,
	       ABS(`olv`.`discount_percent`) AS `discount_percent`,
	       GREATEST(`ol`.`dw_synced_at`,`ol2`.`dw_synced_at`,`oh2`.`dw_synced_at`,`oh`.`dw_synced_at`) AS `dw_synced_at` ,
	       CAST(CONCAT('CREDITMEMO',`ol`.`item_id`) AS CHAR(25) CHARSET latin1) as unique_id
	FROM (((((((((`dw_creditmemo_lines` `ol`
	              LEFT JOIN `dw_order_lines` `ol2` ON ((`ol`.`order_line_id` = `ol2`.`item_id`)))
	             JOIN `dw_creditmemo_headers` `oh2` ON ((`oh2`.`creditmemo_id` = `ol`.`creditmemo_id`)))
	            JOIN `dw_order_headers` `oh` ON ((`oh`.`order_id` = `oh2`.`order_id`)))
	           LEFT JOIN `dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
	          LEFT JOIN `dw_products` `p` ON ((`p`.`product_id` = `ol`.`product_id`)))
	         LEFT JOIN `edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `ol`.`product_id`)
	                                                   AND (`esi`.`product_code` = `ol`.`sku`))))
	        JOIN `creditmemo_lines_vat` `olv` ON ((`olv`.`item_id` = `ol`.`item_id`)))
	       LEFT JOIN `dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
	      LEFT JOIN `dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)));





		CREATE OR REPLACE VIEW vw_order_headers_order AS
			SELECT `cw`.`store_name` AS `store_name`,
	       `oh`.`order_id` AS `order_id`,
	       `oh`.`order_no` AS `order_no`,
	       CAST('ORDER' AS CHAR(25) CHARSET latin1) AS `document_type`,
	       `oh`.`created_at` AS `document_date`,
	       `oh`.`updated_at` AS `document_updated_at`,
	       `oh`.`order_id` AS `document_id`,
	       `oh`.`created_at` AS `order_date`,
	       `oh`.`updated_at` AS `updated_at`,
	       `oh`.`customer_id` AS `customer_id`,
	       `oh`.`customer_email` AS `customer_email`,
	       `oh`.`customer_firstname` AS `customer_firstname`,
	       `oh`.`customer_lastname` AS `customer_lastname`,
	       `oh`.`customer_middlename` AS `customer_middlename`,
	       `oh`.`customer_prefix` AS `customer_prefix`,
	       `oh`.`customer_suffix` AS `customer_suffix`,
	       `oh`.`customer_taxvat` AS `customer_taxvat`,
	       `oh`.`status` AS `status`,
	       `oh`.`coupon_code` AS `coupon_code`,
	       `oh`.`weight` AS `weight`,
	       `oh`.`customer_gender` AS `customer_gender`,
	       `oh`.`customer_dob` AS `customer_dob`,
	       LEFT(`oh`.`shipping_description`,LOCATE(' - ',`oh`.`shipping_description`)) AS `shipping_carrier`,
	       `oh`.`shipping_description` AS `shipping_method`,
	       (CASE `oh`.`warehouse_approved_time`
	            WHEN '0000-00-00 00:00:00' THEN NULL
	            ELSE `oh`.`warehouse_approved_time`
	        END) AS `approved_time`,
	       (CASE
	            WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
	            ELSE IFNULL(CONVERT(`pm`.`payment_method_name` USING utf8),`op`.`method`)
	        END) AS `payment_method`,
	       `op`.`amount_authorized` AS `local_payment_authorized`,
	       `op`.`amount_canceled` AS `local_payment_canceled`,
	       `op`.`cc_exp_month` AS `cc_exp_month`,
	       `op`.`cc_exp_year` AS `cc_exp_year`,
	       `op`.`cc_ss_start_month` AS `cc_start_month`,
	       `op`.`cc_ss_start_year` AS `cc_start_year`,
	       `op`.`cc_last4` AS `cc_last4`,
	       `op`.`cc_status_description` AS `cc_status_description`,
	       `op`.`cc_owner` AS `cc_owner`,
	       IFNULL(CONVERT(`ctm`.`card_type_name` USING utf8),`op`.`cc_type`) AS `cc_type`,
	       `op`.`cc_status` AS `cc_status`,
	       `op`.`cc_ss_issue` AS `cc_issue_no`,
	       `op`.`cc_avs_status` AS `cc_avs_status`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info1`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info2`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info3`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info4`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info5`,
	       `bill`.`email` AS `billing_email`,
	       `bill`.`company` AS `billing_company`,
	       `bill`.`prefix` AS `billing_prefix`,
	       `bill`.`firstname` AS `billing_firstname`,
	       `bill`.`middlename` AS `billing_middlename`,
	       `bill`.`lastname` AS `billing_lastname`,
	       `bill`.`suffix` AS `billing_suffix`,
	       SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},1) AS `billing_street1`,
	       (CASE
	            WHEN (LOCATE({$this->newLine},`bill`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`bill`.`street`) + 1))
	            ELSE ''
	        END) AS `billing_street2`,
	       `bill`.`city` AS `billing_city`,
	       `bill`.`region` AS `billing_region`,
	       `bill`.`region_id` AS `billing_region_id`,
	       `bill`.`postcode` AS `billing_postcode`,
	       `bill`.`country_id` AS `billing_country_id`,
	       `bill`.`telephone` AS `billing_telephone`,
	       `bill`.`fax` AS `billing_fax`,
	       `ship`.`email` AS `shipping_email`,
	       `ship`.`company` AS `shipping_company`,
	       `ship`.`prefix` AS `shipping_prefix`,
	       `ship`.`firstname` AS `shipping_firstname`,
	       `ship`.`middlename` AS `shipping_middlename`,
	       `ship`.`lastname` AS `shipping_lastname`,
	       `ship`.`suffix` AS `shipping_suffix`,
	       SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},1) AS `shiping_street1`,
	       (CASE
	            WHEN (LOCATE({$this->newLine},`ship`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`ship`.`street`) + 1))
	            ELSE ''
	        END) AS `shipping_street2`,
	       `ship`.`city` AS `shipping_city`,
	       `ship`.`region` AS `shipping_region`,
	       `ship`.`region_id` AS `shipping_region_id`,
	       `ship`.`postcode` AS `shipping_postcode`,
	       `ship`.`country_id` AS `shipping_country_id`,
	       `ship`.`telephone` AS `shipping_telephone`,
	       `ship`.`fax` AS `shipping_fax`,
	       `ov`.`has_lens` AS `has_lens`,
	       `oh`.`total_qty_ordered` AS `total_qty`,
	       `ov`.`prof_fee` AS `local_prof_fee`,
	       `ov`.`local_subtotal_inc_vat` AS `local_subtotal_inc_vat`,
	       `ov`.`local_subtotal_vat` AS `local_subtotal_vat`,
	       `ov`.`local_subtotal_exc_vat` AS `local_subtotal_exc_vat`,
	       `ov`.`local_shipping_inc_vat` AS `local_shipping_inc_vat`,
	       `ov`.`local_shipping_vat` AS `local_shipping_vat`,
	       `ov`.`local_shipping_exc_vat` AS `local_shipping_exc_vat`,
	       (-(1) * ABS(`ov`.`local_discount_inc_vat`)) AS `local_discount_inc_vat`,
	       (-(1) * ABS(`ov`.`local_discount_vat`)) AS `local_discount_vat`,
	       (-(1) * ABS(`ov`.`local_discount_exc_vat`)) AS `local_discount_exc_vat`,
	       (-(1) * ABS(`ov`.`local_store_credit_inc_vat`)) AS `local_store_credit_inc_vat`,
	       (-(1) * ABS(`ov`.`local_store_credit_vat`)) AS `local_store_credit_vat`,
	       (-(1) * ABS(`ov`.`local_store_credit_exc_vat`)) AS `local_store_credit_exc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_inc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_exc_vat`,
	       `ov`.`local_total_inc_vat` AS `local_total_inc_vat`,
	       `ov`.`local_total_vat` AS `local_total_vat`,
	       `ov`.`local_total_exc_vat` AS `local_total_exc_vat`,
	       `ov`.`local_subtotal_cost` AS `local_subtotal_cost`,
	       `ov`.`local_shipping_cost` AS `local_shipping_cost`,
	       `ov`.`local_total_cost` AS `local_total_cost`,
	       `ov`.`local_margin_amount` AS `local_margin_amount`,
	       `ov`.`local_margin_percent` AS `local_margin_percent`,
	       `oh`.`base_to_global_rate` AS `local_to_global_rate`,
	       `oh`.`order_currency_code` AS `order_currency_code`,
	       `ov`.`global_prof_fee` AS `global_prof_fee`,
	       `ov`.`global_subtotal_inc_vat` AS `global_subtotal_inc_vat`,
	       `ov`.`global_subtotal_vat` AS `global_subtotal_vat`,
	       `ov`.`global_subtotal_exc_vat` AS `global_subtotal_exc_vat`,
	       `ov`.`global_shipping_inc_vat` AS `global_shipping_inc_vat`,
	       `ov`.`global_shipping_vat` AS `global_shipping_vat`,
	       `ov`.`global_shipping_exc_vat` AS `global_shipping_exc_vat`,
	       (-(1) * ABS(`ov`.`global_discount_inc_vat`)) AS `global_discount_inc_vat`,
	       (-(1) * ABS(`ov`.`global_discount_vat`)) AS `global_discount_vat`,
	       (-(1) * ABS(`ov`.`global_discount_exc_vat`)) AS `global_discount_exc_vat`,
	       (-(1) * ABS(`ov`.`global_store_credit_inc_vat`)) AS `global_store_credit_inc_vat`,
	       (-(1) * ABS(`ov`.`global_store_credit_vat`)) AS `global_store_credit_vat`,
	       (-(1) * ABS(`ov`.`global_store_credit_exc_vat`)) AS `global_store_credit_exc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_inc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_exc_vat`,
	       `ov`.`global_total_inc_vat` AS `global_total_inc_vat`,
	       `ov`.`global_total_vat` AS `global_total_vat`,
	       `ov`.`global_total_exc_vat` AS `global_total_exc_vat`,
	       `ov`.`global_subtotal_cost` AS `global_subtotal_cost`,
	       `ov`.`global_shipping_cost` AS `global_shipping_cost`,
	       `ov`.`global_total_cost` AS `global_total_cost`,
	       `ov`.`global_margin_amount` AS `global_margin_amount`,
	       `ov`.`global_margin_percent` AS `global_margin_percent`,
	       `ov`.`prof_fee_percent` AS `prof_fee_percent`,
	       `ov`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `ov`.`vat_percent` AS `vat_percent`,
	       ABS(`ov`.`discount_percent`) AS `discount_percent`,
	       CAST(`oh`.`customer_note` AS CHAR(8000) CHARSET latin1) AS `customer_note`,
	       CAST(`oh`.`customer_note_notify` AS CHAR(8000) CHARSET latin1) AS `customer_note_notify`,
	       `oh`.`remote_ip` AS `remote_ip`,
	       `oh`.`affilCode` AS `business_source`,
	       `ochan`.`channel` AS `business_channel`,
	       `oh`.`affilBatch` AS `affilBatch`,
	       `oh`.`affilCode` AS `affilCode`,
	       `oh`.`affilUserRef` AS `affilUserRef`,
	       `oh`.`postoptics_auto_verification` AS `auto_verification`,
	       (CASE
	            WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
	            WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
	            WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
	            WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
	            ELSE `oh`.`postoptics_source`
	        END) AS `order_type`,
	       `ohm`.`ORDER_LIFECYCLE` AS `order_lifecycle`,
	       `seq`.`ORDER_SEQ` AS `customer_order_seq_no`,
	       (CASE
	            WHEN (TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
	            ELSE NULL
	        END) AS `reminder_date`,
	       `oh`.`reminder_mobile` AS `reminder_mobile`,
	       `oh`.`reminder_period` AS `reminder_period`,
	       `oh`.`reminder_presc` AS `reminder_presc`,
	       `oh`.`reminder_type` AS `reminder_type`,
	       `oh`.`reminder_sent` AS `reminder_sent`,
	       `oh`.`reminder_follow_sent` AS `reminder_follow_sent`,
	       `oh`.`telesales_method_code` AS `telesales_method_code`,
	       `oh`.`telesales_admin_username` AS `telesales_admin_username`,
	       `oh`.`reorder_on_flag` AS `reorder_on_flag`,
	       `oh`.`reorder_profile_id` AS `reorder_profile_id`,
	       (CASE
	            WHEN ((CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
	                  OR (`rp`.`completed_profile` <> 1)) THEN NULL
	            ELSE `rp`.`next_order_date`
	        END) AS `reorder_date`,
	       `rp`.`interval_days` AS `reorder_interval`,
	       `rqp`.`cc_exp_month` AS `reorder_cc_exp_month`,
	       `rqp`.`cc_exp_year` AS `reorder_cc_exp_year`,
	       `oh`.`automatic_reorder` AS `automatic_reorder`,
	       (CASE
	            WHEN (((`oh`.`presc_verification_method` = 'call')
	                   OR ISNULL(`oh`.`presc_verification_method`))
	                  AND (`conf`.`value` = 1)
	                  AND (`ov`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Call'
	            WHEN ((`oh`.`presc_verification_method` = 'upload')
	                  AND (`conf`.`value` = 1)
	                  AND (`ov`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Upload / Fax / Scan'
	            WHEN ((`conf`.`value` = 1)
	                  AND (`ov`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` = 'Yes')) THEN 'Automatic'
	            ELSE 'Not Required'
	        END) AS `presc_verification_method`,
	       `oh`.`referafriend_code` AS `referafriend_code`,
	       `oh`.`referafriend_referer` AS `referafriend_referer`,
	       `oh`.`dw_synced_at` AS `dw_synced_at` ,
	       CAST(CONCAT('ORDER',`oh`.`order_id`) AS CHAR(25) CHARSET latin1) as unique_id
	FROM (((((((((((((((`dw_order_headers` `oh`
	                    LEFT JOIN `dw_stores` `cw` ON((`cw`.`store_id` = `oh`.`store_id`)))
	                   LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op` ON((`op`.`parent_id` = `oh`.`order_id`)))
	                  LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill` ON((`bill`.`entity_id` = `oh`.`billing_address_id`)))
	                 LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship` ON((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
	                JOIN `order_headers_vat` `ov` ON((`ov`.`order_id` = `oh`.`order_id`)))
	               LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp` ON((`oh`.`reorder_profile_id` = `rp`.`id`)))
	              LEFT JOIN `dw_payment_method_map` `pm` ON((CONVERT(`pm`.`payment_method` USING utf8) = `op`.`method`)))
	             LEFT JOIN `dw_card_type_map` `ctm` ON((CONVERT(`ctm`.`card_type` USING utf8) = `op`.`cc_type`)))
	            LEFT JOIN `{$this->dbname}`.`po_reorder_quote` `rq` ON((`rq`.`entity_id` = `rp`.`reorder_quote_id`)))
	           LEFT JOIN `max_reorder_quote_payment` `mp` ON((`rq`.`entity_id` = `mp`.`quote_id`)))
	          LEFT JOIN `{$this->dbname}`.`po_reorder_quote_payment` `rqp` ON((`rqp`.`payment_id` = `mp`.`payment_id`)))
	         LEFT JOIN `dw_order_channel` `ochan` ON((`ochan`.`order_id` = `oh`.`order_id`)))
	        LEFT JOIN `dw_order_headers_marketing` `ohm` ON((`ohm`.`ORDER_ID` = `oh`.`order_id`)))
	       LEFT JOIN `dw_entity_header_rank_seq` `seq` ON(((`seq`.`DOCUMENT_ID` = `oh`.`order_id`)
	                                                                 AND (`seq`.`DOCUMENT_TYPE` = 'ORDER'))))
	      LEFT JOIN `dw_conf_presc_required` `conf` ON((`conf`.`store_id` = `oh`.`store_id`)))
	WHERE (`oh`.`status` <> 'archived');




		CREATE OR REPLACE VIEW vw_order_headers_cancel AS
			SELECT `cw`.`store_name` AS `store_name`,
	       `oh`.`order_id` AS `order_id`,
	       `oh`.`order_no` AS `order_no`,
	       'CANCEL' AS `document_type`,
	       `os`.`status_time` AS `document_date`,
	       `oh`.`updated_at` AS `document_updated_at`,
	       `oh`.`order_id` AS `document_id`,
	       `oh`.`created_at` AS `order_date`,
	       `oh`.`updated_at` AS `updated_at`,
	       `oh`.`customer_id` AS `customer_id`,
	       `oh`.`customer_email` AS `customer_email`,
	       `oh`.`customer_firstname` AS `customer_firstname`,
	       `oh`.`customer_lastname` AS `customer_lastname`,
	       `oh`.`customer_middlename` AS `customer_middlename`,
	       `oh`.`customer_prefix` AS `customer_prefix`,
	       `oh`.`customer_suffix` AS `customer_suffix`,
	       `oh`.`customer_taxvat` AS `customer_taxvat`,
	       `oh`.`status` AS `status`,
	       `oh`.`coupon_code` AS `coupon_code`,
	       (-(1) * `oh`.`weight`) AS `weight`,
	       `oh`.`customer_gender` AS `customer_gender`,
	       `oh`.`customer_dob` AS `customer_dob`,
	       LEFT(`oh`.`shipping_description`,LOCATE(' - ',`oh`.`shipping_description`)) AS `shipping_carrier`,
	       `oh`.`shipping_description` AS `shipping_method`,
	       (CASE `oh`.`warehouse_approved_time`
	            WHEN '0000-00-00 00:00:00' THEN NULL
	            ELSE `oh`.`warehouse_approved_time`
	        END) AS `approved_time`,
	       (CASE
	            WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
	            ELSE IFNULL(CONVERT(`pm`.`payment_method_name` USING utf8),`op`.`method`)
	        END) AS `payment_method`,
	       `op`.`amount_authorized` AS `local_payment_authorized`,
	       `op`.`amount_canceled` AS `local_payment_canceled`,
	       `op`.`cc_exp_month` AS `cc_exp_month`,
	       `op`.`cc_exp_year` AS `cc_exp_year`,
	       `op`.`cc_ss_start_month` AS `cc_start_month`,
	       `op`.`cc_ss_start_year` AS `cc_start_year`,
	       `op`.`cc_last4` AS `cc_last4`,
	       `op`.`cc_status_description` AS `cc_status_description`,
	       `op`.`cc_owner` AS `cc_owner`,
	       IFNULL(CONVERT(`ctm`.`card_type_name` USING utf8),`op`.`cc_type`) AS `cc_type`,
	       `op`.`cc_status` AS `cc_status`,
	       `op`.`cc_ss_issue` AS `cc_issue_no`,
	       `op`.`cc_avs_status` AS `cc_avs_status`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info1`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info2`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info3`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info4`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info5`,
	       `bill`.`email` AS `billing_email`,
	       `bill`.`company` AS `billing_company`,
	       `bill`.`prefix` AS `billing_prefix`,
	       `bill`.`firstname` AS `billing_firstname`,
	       `bill`.`middlename` AS `billing_middlename`,
	       `bill`.`lastname` AS `billing_lastname`,
	       `bill`.`suffix` AS `billing_suffix`,
	       SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},1) AS `billing_street1`,
	       (CASE
	            WHEN (LOCATE({$this->newLine},`bill`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`bill`.`street`) + 1))
	            ELSE ''
	        END) AS `billing_street2`,
	       `bill`.`city` AS `billing_city`,
	       `bill`.`region` AS `billing_region`,
	       `bill`.`region_id` AS `billing_region_id`,
	       `bill`.`postcode` AS `billing_postcode`,
	       `bill`.`country_id` AS `billing_country_id`,
	       `bill`.`telephone` AS `billing_telephone`,
	       `bill`.`fax` AS `billing_fax`,
	       `ship`.`email` AS `shipping_email`,
	       `ship`.`company` AS `shipping_company`,
	       `ship`.`prefix` AS `shipping_prefix`,
	       `ship`.`firstname` AS `shipping_firstname`,
	       `ship`.`middlename` AS `shipping_middlename`,
	       `ship`.`lastname` AS `shipping_lastname`,
	       `ship`.`suffix` AS `shipping_suffix`,
	       SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},1) AS `shiping_street1`,
	       (CASE
	            WHEN (LOCATE({$this->newLine},`ship`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`ship`.`street`) + 1))
	            ELSE ''
	        END) AS `shipping_street2`,
	       `ship`.`city` AS `shipping_city`,
	       `ship`.`region` AS `shipping_region`,
	       `ship`.`region_id` AS `shipping_region_id`,
	       `ship`.`postcode` AS `shipping_postcode`,
	       `ship`.`country_id` AS `shipping_country_id`,
	       `ship`.`telephone` AS `shipping_telephone`,
	       `ship`.`fax` AS `shipping_fax`,
	       `ov`.`has_lens` AS `has_lens`,
	       (-(1) * `oh`.`total_qty_ordered`) AS `total_qty`,
	       (-(1) * `ov`.`prof_fee`) AS `local_prof_fee`,
	       (-(1) * `ov`.`local_subtotal_inc_vat`) AS `local_subtotal_inc_vat`,
	       (-(1) * `ov`.`local_subtotal_vat`) AS `local_subtotal_vat`,
	       (-(1) * `ov`.`local_subtotal_exc_vat`) AS `local_subtotal_exc_vat`,
	       (-(1) * `ov`.`local_shipping_inc_vat`) AS `local_shipping_inc_vat`,
	       (-(1) * `ov`.`local_shipping_vat`) AS `local_shipping_vat`,
	       (-(1) * `ov`.`local_shipping_exc_vat`) AS `local_shipping_exc_vat`,
	       ABS(`ov`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
	       ABS(`ov`.`local_discount_vat`) AS `local_discount_vat`,
	       ABS(`ov`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
	       ABS(`ov`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
	       ABS(`ov`.`local_store_credit_vat`) AS `local_store_credit_vat`,
	       ABS(`ov`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_inc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_exc_vat`,
	       (-(1) * `ov`.`local_total_inc_vat`) AS `local_total_inc_vat`,
	       (-(1) * `ov`.`local_total_vat`) AS `local_total_vat`,
	       (-(1) * `ov`.`local_total_exc_vat`) AS `local_total_exc_vat`,
	       (-(1) * `ov`.`local_subtotal_cost`) AS `local_subtotal_cost`,
	       (-(1) * `ov`.`local_shipping_cost`) AS `local_shipping_cost`,
	       (-(1) * `ov`.`local_total_cost`) AS `local_total_cost`,
	       (-(1) * `ov`.`local_margin_amount`) AS `local_margin_amount`,
	       `ov`.`local_margin_percent` AS `local_margin_percent`,
	       `oh`.`base_to_global_rate` AS `local_to_global_rate`,
	       `oh`.`order_currency_code` AS `order_currency_code`,
	       (-(1) * `ov`.`global_prof_fee`) AS `global_prof_fee`,
	       (-(1) * `ov`.`global_subtotal_inc_vat`) AS `global_subtotal_inc_vat`,
	       (-(1) * `ov`.`global_subtotal_vat`) AS `global_subtotal_vat`,
	       (-(1) * `ov`.`global_subtotal_exc_vat`) AS `global_subtotal_exc_vat`,
	       (-(1) * `ov`.`global_shipping_inc_vat`) AS `global_shipping_inc_vat`,
	       (-(1) * `ov`.`global_shipping_vat`) AS `global_shipping_vat`,
	       (-(1) * `ov`.`global_shipping_exc_vat`) AS `global_shipping_exc_vat`,
	       ABS(`ov`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
	       ABS(`ov`.`global_discount_vat`) AS `global_discount_vat`,
	       ABS(`ov`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
	       ABS(`ov`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
	       ABS(`ov`.`global_store_credit_vat`) AS `global_store_credit_vat`,
	       ABS(`ov`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_inc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_exc_vat`,
	       (-(1) * `ov`.`global_total_inc_vat`) AS `global_total_inc_vat`,
	       (-(1) * `ov`.`global_total_vat`) AS `global_total_vat`,
	       (-(1) * `ov`.`global_total_exc_vat`) AS `global_total_exc_vat`,
	       (-(1) * `ov`.`global_subtotal_cost`) AS `global_subtotal_cost`,
	       (-(1) * `ov`.`global_shipping_cost`) AS `global_shipping_cost`,
	       (-(1) * `ov`.`global_total_cost`) AS `global_total_cost`,
	       (-(1) * `ov`.`global_margin_amount`) AS `global_margin_amount`,
	       `ov`.`global_margin_percent` AS `global_margin_percent`,
	       `ov`.`prof_fee_percent` AS `prof_fee_percent`,
	       `ov`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `ov`.`vat_percent` AS `vat_percent`,
	       ABS(`ov`.`discount_percent`) AS `discount_percent`,
	       CAST(`oh`.`customer_note` AS CHAR(8000) CHARSET latin1) AS `customer_note`,
	       CAST(`oh`.`customer_note_notify` AS CHAR(8000) CHARSET latin1) AS `customer_note_notify`,
	       `oh`.`remote_ip` AS `remote_ip`,
	       `oh`.`affilCode` AS `business_source`,
	       `ochan`.`channel` AS `business_channel`,
	       `oh`.`affilBatch` AS `affilBatch`,
	       `oh`.`affilCode` AS `affilCode`,
	       `oh`.`affilUserRef` AS `affilUserRef`,
	       `oh`.`postoptics_auto_verification` AS `auto_verification`,
	       (CASE
	            WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
	            WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
	            WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
	            WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
	            ELSE `oh`.`postoptics_source`
	        END) AS `order_type`,
	       `ohm`.`ORDER_LIFECYCLE` AS `order_lifecycle`,
	       `seq`.`ORDER_SEQ` AS `customer_order_seq_no`,
	       (CASE
	            WHEN (TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
	            ELSE NULL
	        END) AS `reminder_date`,
	       `oh`.`reminder_mobile` AS `reminder_mobile`,
	       `oh`.`reminder_period` AS `reminder_period`,
	       `oh`.`reminder_presc` AS `reminder_presc`,
	       `oh`.`reminder_type` AS `reminder_type`,
	       `oh`.`reminder_sent` AS `reminder_sent`,
	       `oh`.`reminder_follow_sent` AS `reminder_follow_sent`,
	       `oh`.`telesales_method_code` AS `telesales_method_code`,
	       `oh`.`telesales_admin_username` AS `telesales_admin_username`,
	       `oh`.`reorder_on_flag` AS `reorder_on_flag`,
	       `oh`.`reorder_profile_id` AS `reorder_profile_id`,
	       (CASE
	            WHEN ((CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
	                  OR (`rp`.`completed_profile` <> 1)) THEN NULL
	            ELSE `rp`.`next_order_date`
	        END) AS `reorder_date`,
	       `rp`.`interval_days` AS `reorder_interval`,
	       `rqp`.`cc_exp_month` AS `reorder_cc_exp_month`,
	       `rqp`.`cc_exp_year` AS `reorder_cc_exp_year`,
	       `oh`.`automatic_reorder` AS `automatic_reorder`,
	       (CASE
	            WHEN (((`oh`.`presc_verification_method` = 'call')
	                   OR ISNULL(`oh`.`presc_verification_method`))
	                  AND (`conf`.`value` = 1)
	                  AND (`ov`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Call'
	            WHEN ((`oh`.`presc_verification_method` = 'upload')
	                  AND (`conf`.`value` = 1)
	                  AND (`ov`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Upload / Fax / Scan'
	            WHEN ((`conf`.`value` = 1)
	                  AND (`ov`.`has_lens` = 1)
	                  AND (`oh`.`postoptics_auto_verification` = 'Yes')) THEN 'Automatic'
	            ELSE 'Not Required'
	        END) AS `presc_verification_method`,
	       `oh`.`referafriend_code` AS `referafriend_code`,
	       `oh`.`referafriend_referer` AS `referafriend_referer`,
	       `oh`.`dw_synced_at` AS `dw_synced_at` ,
	       CAST(CONCAT('CANCEL',`oh`.`order_id`) AS CHAR(25) CHARSET latin1) as unique_id
	FROM ((((((((((((((((`dw_order_headers` `oh`
	                     LEFT JOIN `dw_stores` `cw` ON((`cw`.`store_id` = `oh`.`store_id`)))
	                    LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op` ON((`op`.`parent_id` = `oh`.`order_id`)))
	                   LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill` ON((`bill`.`entity_id` = `oh`.`billing_address_id`)))
	                  LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship` ON((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
	                 JOIN `order_headers_vat` `ov` ON((`ov`.`order_id` = `oh`.`order_id`)))
	                LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp` ON((`oh`.`reorder_profile_id` = `rp`.`id`)))
	               LEFT JOIN `order_status_history_agg` `os` ON((`os`.`order_id` = `oh`.`order_id`)))
	              LEFT JOIN `dw_payment_method_map` `pm` ON((CONVERT(`pm`.`payment_method` USING utf8) = `op`.`method`)))
	             LEFT JOIN `dw_card_type_map` `ctm` ON((CONVERT(`ctm`.`card_type` USING utf8) = `op`.`cc_type`)))
	            LEFT JOIN `{$this->dbname}`.`po_reorder_quote` `rq` ON((`rq`.`entity_id` = `rp`.`reorder_quote_id`)))
	           LEFT JOIN `max_reorder_quote_payment` `mp` ON((`rq`.`entity_id` = `mp`.`quote_id`)))
	          LEFT JOIN `{$this->dbname}`.`po_reorder_quote_payment` `rqp` ON((`rqp`.`payment_id` = `mp`.`payment_id`)))
	         LEFT JOIN `dw_order_channel` `ochan` ON((`ochan`.`order_id` = `oh`.`order_id`)))
	        LEFT JOIN `dw_order_headers_marketing` `ohm` ON((`ohm`.`ORDER_ID` = `oh`.`order_id`)))
	       LEFT JOIN `dw_entity_header_rank_seq` `seq` ON(((`seq`.`DOCUMENT_ID` = `oh`.`order_id`)
	                                                                 AND (`seq`.`DOCUMENT_TYPE` = 'CANCEL'))))
	      LEFT JOIN `dw_conf_presc_required` `conf` ON((`conf`.`store_id` = `oh`.`store_id`)))
	WHERE ((`oh`.`status` = 'canceled')
	       AND (`os`.`status` = 'canceled'));







		CREATE OR REPLACE VIEW vw_order_headers_creditmemo AS
			SELECT `cw`.`store_name` AS `store_name`,
       `oh2`.`order_id` AS `order_id`,
       `oh`.`order_no` AS `order_no`,
       'CREDITMEMO' AS `document_type`,
       `oh2`.`created_at` AS `document_date`,
       `oh2`.`updated_at` AS `document_updated_at`,
       `oh2`.`creditmemo_id` AS `document_id`,
       `oh`.`created_at` AS `order_date`,
       `oh`.`updated_at` AS `updated_at`,
       `oh`.`customer_id` AS `customer_id`,
       `oh`.`customer_email` AS `customer_email`,
       `oh`.`customer_firstname` AS `customer_firstname`,
       `oh`.`customer_lastname` AS `customer_lastname`,
       `oh`.`customer_middlename` AS `customer_middlename`,
       `oh`.`customer_prefix` AS `customer_prefix`,
       `oh`.`customer_suffix` AS `customer_suffix`,
       `oh`.`customer_taxvat` AS `customer_taxvat`,
       `oh`.`status` AS `status`,
       `oh`.`coupon_code` AS `coupon_code`,
       (-(1) * `ov`.`total_weight`) AS `weight`,
       `oh`.`customer_gender` AS `customer_gender`,
       `oh`.`customer_dob` AS `customer_dob`,
       LEFT(`oh`.`shipping_description`,LOCATE(' - ',`oh`.`shipping_description`)) AS `shipping_carrier`,
       `oh`.`shipping_description` AS `shipping_method`,
       (CASE `oh`.`warehouse_approved_time`
            WHEN '0000-00-00 00:00:00' THEN NULL
            ELSE `oh`.`warehouse_approved_time`
        END) AS `approved_time`,
       (CASE
            WHEN (`ov`.`local_total_exc_vat` = 0) THEN 'Free'
            ELSE IFNULL(CONVERT(`pm`.`payment_method_name` USING utf8),`op`.`method`)
        END) AS `payment_method`,
       `op`.`amount_authorized` AS `local_payment_authorized`,
       `op`.`amount_canceled` AS `local_payment_canceled`,
       `op`.`cc_exp_month` AS `cc_exp_month`,
       `op`.`cc_exp_year` AS `cc_exp_year`,
       `op`.`cc_ss_start_month` AS `cc_start_month`,
       `op`.`cc_ss_start_year` AS `cc_start_year`,
       `op`.`cc_last4` AS `cc_last4`,
       `op`.`cc_status_description` AS `cc_status_description`,
       `op`.`cc_owner` AS `cc_owner`,
       IFNULL(CONVERT(`ctm`.`card_type_name` USING utf8),`op`.`cc_type`) AS `cc_type`,
       `op`.`cc_status` AS `cc_status`,
       `op`.`cc_ss_issue` AS `cc_issue_no`,
       `op`.`cc_avs_status` AS `cc_avs_status`,
       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info1`,
       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info2`,
       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info3`,
       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info4`,
       CAST(NULL AS CHAR(255) CHARSET latin1) AS `payment_info5`,
       `bill`.`email` AS `billing_email`,
       `bill`.`company` AS `billing_company`,
       `bill`.`prefix` AS `billing_prefix`,
       `bill`.`firstname` AS `billing_firstname`,
       `bill`.`middlename` AS `billing_middlename`,
       `bill`.`lastname` AS `billing_lastname`,
       `bill`.`suffix` AS `billing_suffix`,
       SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},1) AS `billing_street1`,
       (CASE
            WHEN (LOCATE({$this->newLine},`bill`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`bill`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`bill`.`street`) + 1))
            ELSE ''
        END) AS `billing_street2`,
       `bill`.`city` AS `billing_city`,
       `bill`.`region` AS `billing_region`,
       `bill`.`region_id` AS `billing_region_id`,
       `bill`.`postcode` AS `billing_postcode`,
       `bill`.`country_id` AS `billing_country_id`,
       `bill`.`telephone` AS `billing_telephone`,
       `bill`.`fax` AS `billing_fax`,
       `ship`.`email` AS `shipping_email`,
       `ship`.`company` AS `shipping_company`,
       `ship`.`prefix` AS `shipping_prefix`,
       `ship`.`firstname` AS `shipping_firstname`,
       `ship`.`middlename` AS `shipping_middlename`,
       `ship`.`lastname` AS `shipping_lastname`,
       `ship`.`suffix` AS `shipping_suffix`,
       SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},1) AS `shiping_street1`,
       (CASE
            WHEN (LOCATE({$this->newLine},`ship`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`ship`.`street`,{$this->newLine},2),(LOCATE({$this->newLine},`ship`.`street`) + 1))
            ELSE ''
        END) AS `shipping_street2`,
       `ship`.`city` AS `shipping_city`,
       `ship`.`region` AS `shipping_region`,
       `ship`.`region_id` AS `shipping_region_id`,
       `ship`.`postcode` AS `shipping_postcode`,
       `ship`.`country_id` AS `shipping_country_id`,
       `ship`.`telephone` AS `shipping_telephone`,
       `ship`.`fax` AS `shipping_fax`,
       `ov`.`has_lens` AS `has_lens`,
       (-(1) * `ov`.`total_qty`) AS `total_qty`,
       (-(1) * `ov`.`prof_fee`) AS `local_prof_fee`,
       (-(1) * `ov`.`local_subtotal_inc_vat`) AS `local_subtotal_inc_vat`,
       (-(1) * `ov`.`local_subtotal_vat`) AS `local_subtotal_vat`,
       (-(1) * `ov`.`local_subtotal_exc_vat`) AS `local_subtotal_exc_vat`,
       (-(1) * `ov`.`local_shipping_inc_vat`) AS `local_shipping_inc_vat`,
       (-(1) * `ov`.`local_shipping_vat`) AS `local_shipping_vat`,
       (-(1) * `ov`.`local_shipping_exc_vat`) AS `local_shipping_exc_vat`,
       ABS(`ov`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
       ABS(`ov`.`local_discount_vat`) AS `local_discount_vat`,
       ABS(`ov`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
       ABS(`ov`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
       ABS(`ov`.`local_store_credit_vat`) AS `local_store_credit_vat`,
       ABS(`ov`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
       (-(1) * `ov`.`local_adjustment_inc_vat`) AS `local_adjustment_inc_vat`,
       (-(1) * `ov`.`local_adjustment_vat`) AS `local_adjustment_vat`,
       (-(1) * `ov`.`local_adjustment_exc_vat`) AS `local_adjustment_exc_vat`,
       (-(1) * `ov`.`local_total_inc_vat`) AS `local_total_inc_vat`,
       (-(1) * `ov`.`local_total_vat`) AS `local_total_vat`,
       (-(1) * `ov`.`local_total_exc_vat`) AS `local_total_exc_vat`,
       (-(1) * `ov`.`local_subtotal_cost`) AS `local_subtotal_cost`,
       (-(1) * `ov`.`local_shipping_cost`) AS `local_shipping_cost`,
       (-(1) * `ov`.`local_total_cost`) AS `local_total_cost`,
       (-(1) * `ov`.`local_margin_amount`) AS `local_margin_amount`,
       `ov`.`local_margin_percent` AS `local_margin_percent`,
       `oh`.`base_to_global_rate` AS `local_to_global_rate`,
       `oh`.`order_currency_code` AS `order_currency_code`,
       (-(1) * `ov`.`global_prof_fee`) AS `global_prof_fee`,
       (-(1) * `ov`.`global_subtotal_inc_vat`) AS `global_subtotal_inc_vat`,
       (-(1) * `ov`.`global_subtotal_vat`) AS `global_subtotal_vat`,
       (-(1) * `ov`.`global_subtotal_exc_vat`) AS `global_subtotal_exc_vat`,
       (-(1) * `ov`.`global_shipping_inc_vat`) AS `global_shipping_inc_vat`,
       (-(1) * `ov`.`global_shipping_vat`) AS `global_shipping_vat`,
       (-(1) * `ov`.`global_shipping_exc_vat`) AS `global_shipping_exc_vat`,
       ABS(`ov`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
       ABS(`ov`.`global_discount_vat`) AS `global_discount_vat`,
       ABS(`ov`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
       ABS(`ov`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
       ABS(`ov`.`global_store_credit_vat`) AS `global_store_credit_vat`,
       ABS(`ov`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
       (-(1) * `ov`.`global_adjustment_inc_vat`) AS `global_adjustment_inc_vat`,
       (-(1) * `ov`.`global_adjustment_vat`) AS `global_adjustment_vat`,
       (-(1) * `ov`.`global_adjustment_exc_vat`) AS `global_adjustment_exc_vat`,
       (-(1) * `ov`.`global_total_inc_vat`) AS `global_total_inc_vat`,
       (-(1) * `ov`.`global_total_vat`) AS `global_total_vat`,
       (-(1) * `ov`.`global_total_exc_vat`) AS `global_total_exc_vat`,
       (-(1) * `ov`.`global_subtotal_cost`) AS `global_subtotal_cost`,
       (-(1) * `ov`.`global_shipping_cost`) AS `global_shipping_cost`,
       (-(1) * `ov`.`global_total_cost`) AS `global_total_cost`,
       (-(1) * `ov`.`global_margin_amount`) AS `global_margin_amount`,
       `ov`.`global_margin_percent` AS `global_margin_percent`,
       `ov`.`prof_fee_percent` AS `prof_fee_percent`,
       `ov`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
       `ov`.`vat_percent` AS `vat_percent`,
       ABS(`ov`.`discount_percent`) AS `discount_percent`,
       CAST(`oh`.`customer_note` AS CHAR(8000) CHARSET latin1) AS `customer_note`,
       CAST(`oh`.`customer_note_notify` AS CHAR(8000) CHARSET latin1) AS `customer_note_notify`,
       `oh`.`remote_ip` AS `remote_ip`,
       `oh`.`affilCode` AS `business_source`,
       `ochan`.`channel` AS `business_channel`,
       `oh`.`affilBatch` AS `affilBatch`,
       `oh`.`affilCode` AS `affilCode`,
       `oh`.`affilUserRef` AS `affilUserRef`,
       `oh`.`postoptics_auto_verification` AS `auto_verification`,
       (CASE
            WHEN (`oh`.`automatic_reorder` = 1) THEN 'Automatic'
            WHEN (`oh`.`postoptics_source` = 'user') THEN 'Web'
            WHEN (`oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
            WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
            ELSE `oh`.`postoptics_source`
        END) AS `order_type`,
       `ohm`.`ORDER_LIFECYCLE` AS `order_lifecycle`,
       `seq`.`ORDER_SEQ` AS `customer_order_seq_no`,
       (CASE
            WHEN (TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
            ELSE NULL
        END) AS `reminder_date`,
       `oh`.`reminder_mobile` AS `reminder_mobile`,
       `oh`.`reminder_period` AS `reminder_period`,
       `oh`.`reminder_presc` AS `reminder_presc`,
       `oh`.`reminder_type` AS `reminder_type`,
       `oh`.`reminder_sent` AS `reminder_sent`,
       `oh`.`reminder_follow_sent` AS `reminder_follow_sent`,
       `oh`.`telesales_method_code` AS `telesales_method_code`,
       `oh`.`telesales_admin_username` AS `telesales_admin_username`,
       `oh`.`reorder_on_flag` AS `reorder_on_flag`,
       `oh`.`reorder_profile_id` AS `reorder_profile_id`,
       (CASE
            WHEN ((CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
                  OR (`rp`.`completed_profile` <> 1)) THEN NULL
            ELSE `rp`.`next_order_date`
        END) AS `reorder_date`,
       `rp`.`interval_days` AS `reorder_interval`,
       `rqp`.`cc_exp_month` AS `reorder_cc_exp_month`,
       `rqp`.`cc_exp_year` AS `reorder_cc_exp_year`,
       `oh`.`automatic_reorder` AS `automatic_reorder`,
       (CASE
            WHEN (((`oh`.`presc_verification_method` = 'call')
                   OR ISNULL(`oh`.`presc_verification_method`))
                  AND (`conf`.`value` = 1)
                  AND (`ov2`.`has_lens` = 1)
                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Call'
            WHEN ((`oh`.`presc_verification_method` = 'upload')
                  AND (`conf`.`value` = 1)
                  AND (`ov2`.`has_lens` = 1)
                  AND (`oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Upload / Fax / Scan'
            WHEN ((`conf`.`value` = 1)
                  AND (`ov2`.`has_lens` = 1)
                  AND (`oh`.`postoptics_auto_verification` = 'Yes')) THEN 'Automatic'
            ELSE 'Not Required'
        END) AS `presc_verification_method`,
       `oh`.`referafriend_code` AS `referafriend_code`,
       `oh`.`referafriend_referer` AS `referafriend_referer`,
        GREATEST(`oh`.`dw_synced_at`,`oh2`.`dw_synced_at`) AS `dw_synced_at` ,
		CAST(CONCAT('CREDITMEMO',`oh2`.`creditmemo_id`) AS CHAR(25) CHARSET latin1) as unique_id
FROM (((((((((((((((((`dw_creditmemo_headers` `oh2`
                      JOIN `dw_order_headers` `oh` ON((`oh2`.`order_id` = `oh`.`order_id`)))
                     LEFT JOIN `dw_stores` `cw` ON((`cw`.`store_id` = `oh`.`store_id`)))
                    LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op` ON((`op`.`parent_id` = `oh`.`order_id`)))
                   LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill` ON((`bill`.`entity_id` = `oh`.`billing_address_id`)))
                  LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship` ON((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
                 JOIN `creditmemo_headers_vat` `ov` ON((`ov`.`creditmemo_id` = `oh2`.`creditmemo_id`)))
                JOIN `order_headers_vat` `ov2` ON((`ov2`.`order_id` = `oh`.`order_id`)))
               LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp` ON((`oh`.`reorder_profile_id` = `rp`.`id`)))
              LEFT JOIN `dw_payment_method_map` `pm` ON((CONVERT(`pm`.`payment_method` USING utf8) = `op`.`method`)))
             LEFT JOIN `dw_card_type_map` `ctm` ON((CONVERT(`ctm`.`card_type` USING utf8) = `op`.`cc_type`)))
            LEFT JOIN `{$this->dbname}`.`po_reorder_quote` `rq` ON((`rq`.`entity_id` = `rp`.`reorder_quote_id`)))
           LEFT JOIN `max_reorder_quote_payment` `mp` ON((`rq`.`entity_id` = `mp`.`quote_id`)))
          LEFT JOIN `{$this->dbname}`.`po_reorder_quote_payment` `rqp` ON((`rqp`.`payment_id` = `mp`.`payment_id`)))
         LEFT JOIN `dw_order_channel` `ochan` ON((`ochan`.`order_id` = `oh`.`order_id`)))
        LEFT JOIN `dw_order_headers_marketing` `ohm` ON((`ohm`.`ORDER_ID` = `oh`.`order_id`)))
       LEFT JOIN `dw_entity_header_rank_seq` `seq` ON(((`seq`.`DOCUMENT_ID` = `oh2`.`creditmemo_id`)
                                                                 AND (`seq`.`DOCUMENT_TYPE` = 'CREDITMEMO'))))
      LEFT JOIN `dw_conf_presc_required` `conf` ON((`conf`.`store_id` = `oh`.`store_id`)));








		CREATE OR REPLACE VIEW vw_order_lines_order AS
		SELECT `ol`.`item_id` AS `line_id`,
		       `ol`.`order_id` AS `order_id`,
		       `cw`.`store_name` AS `store_name`,
		       `oh`.`order_no` AS `order_no`,
		       CAST('ORDER' AS CHAR(25) CHARSET latin1) AS `document_type`,
		       `oh`.`created_at` AS `document_date`,
		       `oh`.`updated_at` AS `document_updated_at`,
		       `oh`.`order_id` AS `document_id`,
		       `oh`.`created_at` AS `order_date`,
		       `oh`.`updated_at` AS `updated_at`,
		       `ol`.`product_id` AS `product_id`,
		       `cf`.`product_type` AS `product_type`,
		       CAST(`ol`.`product_options` AS CHAR(8000) CHARSET latin1) AS `product_options`,
		       `p`.`is_lens` AS `is_lens`,
		       CAST('' AS CHAR(1) CHARSET latin1) AS `lens_eye`,
		       `esi`.`BC` AS `lens_base_curve`,
		       `esi`.`DI` AS `lens_diameter`,
		       `esi`.`PO` AS `lens_power`,
		       `esi`.`CY` AS `lens_cylinder`,
		       `esi`.`AX` AS `lens_axis`,
		       `esi`.`AD` AS `lens_addition`,
		       `esi`.`DO` AS `lens_dominance`,
		       `esi`.`CO` AS `lens_colour`,
		       `p`.`daysperlens` AS `lens_days`,
		       CAST('' AS CHAR(1) CHARSET latin1) AS `product_size`,
		       CAST('' AS CHAR(1) CHARSET latin1) AS `product_colour`,
		       `ol`.`weight` AS `unit_weight`,
		       `ol`.`row_weight` AS `line_weight`,
		       `ol`.`is_virtual` AS `is_virtual`,
		       `ol`.`sku` AS `sku`,
		       `ol`.`name` AS `name`,
		       `pcf`.`category_id` AS `category_id`,
		       (CASE
		            WHEN (`oh`.`base_shipping_amount` > 0) THEN 1
		            ELSE 0
		        END) AS `free_shipping`,
		       (CASE
		            WHEN (`oh`.`base_discount_amount` > 0) THEN 1
		            ELSE 0
		        END) AS `no_discount`,
		       `ol`.`qty_ordered` AS `qty`,
		       `olv`.`local_prof_fee` AS `local_prof_fee`,
		       `olv`.`local_price_inc_vat` AS `local_price_inc_vat`,
		       `olv`.`local_price_vat` AS `local_price_vat`,
		       `olv`.`local_price_exc_vat` AS `local_price_exc_vat`,
		       `olv`.`local_line_subtotal_inc_vat` AS `local_line_subtotal_inc_vat`,
		       `olv`.`local_line_subtotal_vat` AS `local_line_subtotal_vat`,
		       `olv`.`local_line_subtotal_exc_vat` AS `local_line_subtotal_exc_vat`,
		       `olv`.`local_shipping_inc_vat` AS `local_shipping_inc_vat`,
		       `olv`.`local_shipping_vat` AS `local_shipping_vat`,
		       `olv`.`local_shipping_exc_vat` AS `local_shipping_exc_vat`,
		       (-(1) * ABS(`olv`.`local_discount_inc_vat`)) AS `local_discount_inc_vat`,
		       (-(1) * ABS(`olv`.`local_discount_vat`)) AS `local_discount_vat`,
		       (-(1) * ABS(`olv`.`local_discount_exc_vat`)) AS `local_discount_exc_vat`,
		       (-(1) * ABS(`olv`.`local_store_credit_inc_vat`)) AS `local_store_credit_inc_vat`,
		       (-(1) * ABS(`olv`.`local_store_credit_vat`)) AS `local_store_credit_vat`,
		       (-(1) * ABS(`olv`.`local_store_credit_exc_vat`)) AS `local_store_credit_exc_vat`,
		       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_inc_vat`,
		       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_vat`,
		       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_exc_vat`,
		       `olv`.`local_line_total_inc_vat` AS `local_line_total_inc_vat`,
		       `olv`.`local_line_total_vat` AS `local_line_total_vat`,
		       `olv`.`local_line_total_exc_vat` AS `local_line_total_exc_vat`,
		       `olv`.`local_subtotal_cost` AS `local_subtotal_cost`,
		       `olv`.`local_shipping_cost` AS `local_shipping_cost`,
		       `olv`.`local_total_cost` AS `local_total_cost`,
		       `olv`.`local_margin_amount` AS `local_margin_amount`,
		       `olv`.`local_margin_percent` AS `local_margin_percent`,
		       `olv`.`local_to_global_rate` AS `local_to_global_rate`,
		       `olv`.`order_currency_code` AS `order_currency_code`,
		       `olv`.`global_prof_fee` AS `global_prof_fee`,
		       `olv`.`global_price_inc_vat` AS `global_price_inc_vat`,
		       `olv`.`global_price_vat` AS `global_price_vat`,
		       `olv`.`global_price_exc_vat` AS `global_price_exc_vat`,
		       `olv`.`global_line_subtotal_inc_vat` AS `global_line_subtotal_inc_vat`,
		       `olv`.`global_line_subtotal_vat` AS `global_line_subtotal_vat`,
		       `olv`.`global_line_subtotal_exc_vat` AS `global_line_subtotal_exc_vat`,
		       `olv`.`global_shipping_inc_vat` AS `global_shipping_inc_vat`,
		       `olv`.`global_shipping_vat` AS `global_shipping_vat`,
		       `olv`.`global_shipping_exc_vat` AS `global_shipping_exc_vat`,
		       (-(1) * ABS(`olv`.`global_discount_inc_vat`)) AS `global_discount_inc_vat`,
		       (-(1) * ABS(`olv`.`global_discount_vat`)) AS `global_discount_vat`,
		       (-(1) * ABS(`olv`.`global_discount_exc_vat`)) AS `global_discount_exc_vat`,
		       (-(1) * ABS(`olv`.`global_store_credit_inc_vat`)) AS `global_store_credit_inc_vat`,
		       (-(1) * ABS(`olv`.`global_store_credit_vat`)) AS `global_store_credit_vat`,
		       (-(1) * ABS(`olv`.`global_store_credit_exc_vat`)) AS `global_store_credit_exc_vat`,
		       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_inc_vat`,
		       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_vat`,
		       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_exc_vat`,
		       `olv`.`global_line_total_inc_vat` AS `global_line_total_inc_vat`,
		       `olv`.`global_line_total_vat` AS `global_line_total_vat`,
		       `olv`.`global_line_total_exc_vat` AS `global_line_total_exc_vat`,
		       `olv`.`global_subtotal_cost` AS `global_subtotal_cost`,
		       `olv`.`global_shipping_cost` AS `global_shipping_cost`,
		       `olv`.`global_total_cost` AS `global_total_cost`,
		       `olv`.`global_margin_amount` AS `global_margin_amount`,
		       `olv`.`global_margin_percent` AS `global_margin_percent`,
		       `olv`.`prof_fee_percent` AS `prof_fee_percent`,
		       `olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
		       `olv`.`vat_percent` AS `vat_percent`,
		       ABS(`olv`.`discount_percent`) AS `discount_percent`,
		       	GREATEST(`ol`.`dw_synced_at`,`oh`.`dw_synced_at`) AS `dw_synced_at` ,
	       		CAST(CONCAT('ORDER',`ol`.`item_id`) AS CHAR(25) CHARSET latin1) as unique_id
		FROM (((((((`dw_order_lines` `ol`
		            JOIN `dw_order_headers` `oh` ON ((`oh`.`order_id` = `ol`.`order_id`)))
		           LEFT JOIN `dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
		          LEFT JOIN `dw_products` `p` ON ((`p`.`product_id` = `ol`.`product_id`)))
		         LEFT JOIN `edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `ol`.`product_id`)
		                                                   AND (`esi`.`product_code` = `ol`.`sku`))))
		        JOIN `order_lines_vat` `olv` ON ((`olv`.`item_id` = `ol`.`item_id`)))
		       LEFT JOIN `dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
		      LEFT JOIN `dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)))
		WHERE (`oh`.`status` <> 'archived');






		CREATE OR REPLACE VIEW vw_order_lines_cancel AS
		SELECT `ol`.`item_id` AS `line_id`,
	       `ol`.`order_id` AS `order_id`,
	       `cw`.`store_name` AS `store_name`,
	       `oh`.`order_no` AS `order_no`,
	       'CANCEL' AS `document_type`,
	       `os`.`status_time` AS `document_date`,
	       `oh`.`updated_at` AS `document_updated_at`,
	       `oh`.`order_id` AS `document_id`,
	       `oh`.`created_at` AS `order_date`,
	       `oh`.`updated_at` AS `updated_at`,
	       `ol`.`product_id` AS `product_id`,
	       `cf`.`product_type` AS `product_type`,
	       CAST(`ol`.`product_options` AS CHAR(8000) CHARSET latin1) AS `product_options`,
	       `p`.`is_lens` AS `is_lens`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `lens_eye`,
	       `esi`.`BC` AS `lens_base_curve`,
	       `esi`.`DI` AS `lens_diameter`,
	       `esi`.`PO` AS `lens_power`,
	       `esi`.`CY` AS `lens_cylinder`,
	       `esi`.`AX` AS `lens_axis`,
	       `esi`.`AD` AS `lens_addition`,
	       `esi`.`DO` AS `lens_dominance`,
	       `esi`.`CO` AS `lens_colour`,
	       `p`.`daysperlens` AS `lens_days`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_size`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_colour`,
	       (-(1) * `ol`.`weight`) AS `unit_weight`,
	       (-(1) * `ol`.`row_weight`) AS `line_weight`,
	       `ol`.`is_virtual` AS `is_virtual`,
	       `ol`.`sku` AS `sku`,
	       `ol`.`name` AS `name`,
	       `pcf`.`category_id` AS `category_id`,
	       (CASE
	            WHEN (`oh`.`base_shipping_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `free_shipping`,
	       (CASE
	            WHEN (`oh`.`base_discount_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `no_discount`,
	       (-(1) * `ol`.`qty_ordered`) AS `qty`,
	       (-(1) * `olv`.`local_prof_fee`) AS `local_prof_fee`,
	       `olv`.`local_price_inc_vat` AS `local_price_inc_vat`,
	       `olv`.`local_price_vat` AS `local_price_vat`,
	       `olv`.`local_price_exc_vat` AS `local_price_exc_vat`,
	       (-(1) * `olv`.`local_line_subtotal_inc_vat`) AS `local_line_subtotal_inc_vat`,
	       (-(1) * `olv`.`local_line_subtotal_vat`) AS `local_line_subtotal_vat`,
	       (-(1) * `olv`.`local_line_subtotal_exc_vat`) AS `local_line_subtotal_exc_vat`,
	       (-(1) * `olv`.`local_shipping_inc_vat`) AS `local_shipping_inc_vat`,
	       (-(1) * `olv`.`local_shipping_vat`) AS `local_shipping_vat`,
	       (-(1) * `olv`.`local_shipping_exc_vat`) AS `local_shipping_exc_vat`,
	       ABS(`olv`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
	       ABS(`olv`.`local_discount_vat`) AS `local_discount_vat`,
	       ABS(`olv`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
	       ABS(`olv`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
	       ABS(`olv`.`local_store_credit_vat`) AS `local_store_credit_vat`,
	       ABS(`olv`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_inc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `local_adjustment_exc_vat`,
	       (-(1) * `olv`.`local_line_total_inc_vat`) AS `local_line_total_inc_vat`,
	       (-(1) * `olv`.`local_line_total_vat`) AS `local_line_total_vat`,
	       (-(1) * `olv`.`local_line_total_exc_vat`) AS `local_line_total_exc_vat`,
	       (-(1) * `olv`.`local_subtotal_cost`) AS `local_subtotal_cost`,
	       (-(1) * `olv`.`local_shipping_cost`) AS `local_shipping_cost`,
	       (-(1) * `olv`.`local_total_cost`) AS `local_total_cost`,
	       (-(1) * `olv`.`local_margin_amount`) AS `local_margin_amount`,
	       `olv`.`local_margin_percent` AS `local_margin_percent`,
	       `olv`.`local_to_global_rate` AS `local_to_global_rate`,
	       `olv`.`order_currency_code` AS `order_currency_code`,
	       (-(1) * `olv`.`global_prof_fee`) AS `global_prof_fee`,
	       `olv`.`global_price_inc_vat` AS `global_price_inc_vat`,
	       `olv`.`global_price_vat` AS `global_price_vat`,
	       `olv`.`global_price_exc_vat` AS `global_price_exc_vat`,
	       (-(1) * `olv`.`global_line_subtotal_inc_vat`) AS `global_line_subtotal_inc_vat`,
	       (-(1) * `olv`.`global_line_subtotal_vat`) AS `global_line_subtotal_vat`,
	       (-(1) * `olv`.`global_line_subtotal_exc_vat`) AS `global_line_subtotal_exc_vat`,
	       (-(1) * `olv`.`global_shipping_inc_vat`) AS `global_shipping_inc_vat`,
	       (-(1) * `olv`.`global_shipping_vat`) AS `global_shipping_vat`,
	       (-(1) * `olv`.`global_shipping_exc_vat`) AS `global_shipping_exc_vat`,
	       ABS(`olv`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
	       ABS(`olv`.`global_discount_vat`) AS `global_discount_vat`,
	       ABS(`olv`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
	       ABS(`olv`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
	       ABS(`olv`.`global_store_credit_vat`) AS `global_store_credit_vat`,
	       ABS(`olv`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_inc_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_vat`,
	       CAST(NULL AS DECIMAL(12,4)) AS `global_adjustment_exc_vat`,
	       (-(1) * `olv`.`global_line_total_inc_vat`) AS `global_line_total_inc_vat`,
	       (-(1) * `olv`.`global_line_total_vat`) AS `global_line_total_vat`,
	       (-(1) * `olv`.`global_line_total_exc_vat`) AS `global_line_total_exc_vat`,
	       (-(1) * `olv`.`global_subtotal_cost`) AS `global_subtotal_cost`,
	       (-(1) * `olv`.`global_shipping_cost`) AS `global_shipping_cost`,
	       (-(1) * `olv`.`global_total_cost`) AS `global_total_cost`,
	       (-(1) * `olv`.`global_margin_amount`) AS `global_margin_amount`,
	       `olv`.`global_margin_percent` AS `global_margin_percent`,
	       `olv`.`prof_fee_percent` AS `prof_fee_percent`,
	       `olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `olv`.`vat_percent` AS `vat_percent`,
	       ABS(`olv`.`discount_percent`) AS `discount_percent`,
	       GREATEST(`ol`.`dw_synced_at`,`oh`.`dw_synced_at`) AS `dw_synced_at` ,
	       CAST(CONCAT('CANCEL',`ol`.`item_id`) AS CHAR(25) CHARSET latin1) as unique_id
	FROM ((((((((`dw_order_lines` `ol`
	             JOIN `dw_order_headers` `oh` ON ((`oh`.`order_id` = `ol`.`order_id`)))
	            LEFT JOIN `dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
	           LEFT JOIN `dw_products` `p` ON ((`p`.`product_id` = `ol`.`product_id`)))
	          LEFT JOIN `edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `ol`.`product_id`)
	                                                    AND (`esi`.`product_code` = `ol`.`sku`))))
	         JOIN `order_lines_vat` `olv` ON ((`olv`.`item_id` = `ol`.`item_id`)))
	        LEFT JOIN `order_status_history_agg` `os` ON ((`os`.`order_id` = `oh`.`order_id`)))
	       LEFT JOIN `dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
	      LEFT JOIN `dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)))
	WHERE ((`oh`.`status` = 'canceled')
	       AND (`os`.`status` = 'canceled'));
		CREATE OR REPLACE VIEW vw_order_lines_creditmemo AS
		SELECT `ol`.`item_id` AS `line_id`,
	       `oh`.`order_id` AS `order_id`,
	       `cw`.`store_name` AS `store_name`,
	       `oh`.`order_no` AS `order_no`,
	       'CREDITMEMO' AS `document_type`,
	       `oh2`.`created_at` AS `document_date`,
	       `oh2`.`updated_at` AS `document_updated_at`,
	       `oh2`.`creditmemo_id` AS `document_id`,
	       `oh`.`created_at` AS `order_date`,
	       `oh`.`updated_at` AS `updated_at`,
	       `ol`.`product_id` AS `product_id`,
	       `cf`.`product_type` AS `product_type`,
	       CAST(`ol2`.`product_options` AS CHAR(8000) CHARSET latin1) AS `product_options`,
	       `p`.`is_lens` AS `is_lens`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `lens_eye`,
	       `esi`.`BC` AS `lens_base_curve`,
	       `esi`.`DI` AS `lens_diameter`,
	       `esi`.`PO` AS `lens_power`,
	       `esi`.`CY` AS `lens_cylinder`,
	       `esi`.`AX` AS `lens_axis`,
	       `esi`.`AD` AS `lens_addition`,
	       `esi`.`DO` AS `lens_dominance`,
	       `esi`.`CO` AS `lens_colour`,
	       `p`.`daysperlens` AS `lens_days`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_size`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_colour`,
	       (-(1) * `ol2`.`weight`) AS `unit_weight`,
	       ((-(1) * `ol2`.`weight`) * `ol`.`qty`) AS `line_weight`,
	       `ol2`.`is_virtual` AS `is_virtual`,
	       `ol`.`sku` AS `sku`,
	       `ol`.`name` AS `name`,
	       `pcf`.`category_id` AS `category_id`,
	       (CASE
	            WHEN (`oh`.`base_shipping_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `free_shipping`,
	       (CASE
	            WHEN (`oh`.`base_discount_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `no_discount`,
	       (-(1) * `ol`.`qty`) AS `qty_ordered`,
	       (-(1) * `olv`.`local_prof_fee`) AS `local_prof_fee`,
	       `olv`.`local_price_inc_vat` AS `local_price_inc_vat`,
	       `olv`.`local_price_vat` AS `local_price_vat`,
	       `olv`.`local_price_exc_vat` AS `local_price_exc_vat`,
	       (-(1) * `olv`.`local_line_subtotal_inc_vat`) AS `local_line_subtotal_inc_vat`,
	       (-(1) * `olv`.`local_line_subtotal_vat`) AS `local_line_subtotal_vat`,
	       (-(1) * `olv`.`local_line_subtotal_exc_vat`) AS `local_line_subtotal_exc_vat`,
	       (-(1) * `olv`.`local_shipping_inc_vat`) AS `local_shipping_inc_vat`,
	       (-(1) * `olv`.`local_shipping_vat`) AS `local_shipping_vat`,
	       (-(1) * `olv`.`local_shipping_exc_vat`) AS `local_shipping_exc_vat`,
	       ABS(`olv`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
	       ABS(`olv`.`local_discount_vat`) AS `local_discount_vat`,
	       ABS(`olv`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
	       ABS(`olv`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
	       ABS(`olv`.`local_store_credit_vat`) AS `local_store_credit_vat`,
	       ABS(`olv`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
	       (-(1) * `olv`.`local_adjustment_inc_vat`) AS `local_adjustment_inc_vat`,
	       (-(1) * `olv`.`local_adjustment_vat`) AS `local_adjustment_vat`,
	       (-(1) * `olv`.`local_adjustment_exc_vat`) AS `local_adjustment_exc_vat`,
	       (-(1) * `olv`.`local_line_total_inc_vat`) AS `local_line_total_inc_vat`,
	       (-(1) * `olv`.`local_line_total_vat`) AS `local_line_total_vat`,
	       (-(1) * `olv`.`local_line_total_exc_vat`) AS `local_line_total_exc_vat`,
	       (-(1) * `olv`.`local_subtotal_cost`) AS `local_subtotal_cost`,
	       (-(1) * `olv`.`local_shipping_cost`) AS `local_shipping_cost`,
	       (-(1) * `olv`.`local_total_cost`) AS `local_total_cost`,
	       (-(1) * `olv`.`local_margin_amount`) AS `local_margin_amount`,
	       `olv`.`local_margin_percent` AS `local_margin_percent`,
	       `olv`.`local_to_global_rate` AS `local_to_global_rate`,
	       `olv`.`order_currency_code` AS `order_currency_code`,
	       (-(1) * `olv`.`global_prof_fee`) AS `global_prof_fee`,
	       `olv`.`global_price_inc_vat` AS `global_price_inc_vat`,
	       `olv`.`global_price_vat` AS `global_price_vat`,
	       `olv`.`global_price_exc_vat` AS `global_price_exc_vat`,
	       (-(1) * `olv`.`global_line_subtotal_inc_vat`) AS `global_line_subtotal_inc_vat`,
	       (-(1) * `olv`.`global_line_subtotal_vat`) AS `global_line_subtotal_vat`,
	       (-(1) * `olv`.`global_line_subtotal_exc_vat`) AS `global_line_subtotal_exc_vat`,
	       (-(1) * `olv`.`global_shipping_inc_vat`) AS `global_shipping_inc_vat`,
	       (-(1) * `olv`.`global_shipping_vat`) AS `global_shipping_vat`,
	       (-(1) * `olv`.`global_shipping_exc_vat`) AS `global_shipping_exc_vat`,
	       ABS(`olv`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
	       ABS(`olv`.`global_discount_vat`) AS `global_discount_vat`,
	       ABS(`olv`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
	       ABS(`olv`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
	       ABS(`olv`.`global_store_credit_vat`) AS `global_store_credit_vat`,
	       ABS(`olv`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
	       (-(1) * `olv`.`global_adjustment_inc_vat`) AS `global_adjustment_inc_vat`,
	       (-(1) * `olv`.`global_adjustment_vat`) AS `global_adjustment_vat`,
	       (-(1) * `olv`.`global_adjustment_exc_vat`) AS `global_adjustment_exc_vat`,
	       (-(1) * `olv`.`global_line_total_inc_vat`) AS `global_line_total_inc_vat`,
	       (-(1) * `olv`.`global_line_total_vat`) AS `global_line_total_vat`,
	       (-(1) * `olv`.`global_line_total_exc_vat`) AS `global_line_total_exc_vat`,
	       (-(1) * `olv`.`global_subtotal_cost`) AS `global_subtotal_cost`,
	       (-(1) * `olv`.`global_shipping_cost`) AS `global_shipping_cost`,
	       (-(1) * `olv`.`global_total_cost`) AS `global_total_cost`,
	       (-(1) * `olv`.`global_margin_amount`) AS `global_margin_amount`,
	       `olv`.`global_margin_percent` AS `global_margin_percent`,
	       `olv`.`prof_fee_percent` AS `prof_fee_percent`,
	       `olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `olv`.`vat_percent` AS `vat_percent`,
	       ABS(`olv`.`discount_percent`) AS `discount_percent`,
	       GREATEST(`ol`.`dw_synced_at`,`oh2`.`dw_synced_at`,`oh`.`dw_synced_at`,`ol2`.`dw_synced_at`) AS `dw_synced_at` ,
	       CAST(CONCAT('CREDITMEMO',`ol`.`item_id`) AS CHAR(25) CHARSET latin1) as unique_id
	FROM (((((((((`dw_creditmemo_lines` `ol`
	              LEFT JOIN `dw_order_lines` `ol2` ON ((`ol`.`order_line_id` = `ol2`.`item_id`)))
	             JOIN `dw_creditmemo_headers` `oh2` ON ((`oh2`.`creditmemo_id` = `ol`.`creditmemo_id`)))
	            JOIN `dw_order_headers` `oh` ON ((`oh`.`order_id` = `oh2`.`order_id`)))
	           LEFT JOIN `dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
	          LEFT JOIN `dw_products` `p` ON ((`p`.`product_id` = `ol`.`product_id`)))
	         LEFT JOIN `edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `ol`.`product_id`)
	                                                   AND (`esi`.`product_code` = `ol`.`sku`))))
	        JOIN `creditmemo_lines_vat` `olv` ON ((`olv`.`item_id` = `ol`.`item_id`)))
	       LEFT JOIN `dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
	      LEFT JOIN `dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)));
SQL;

		$this->runQuery($sql);

		$sql = <<< SQL
CREATE OR REPLACE VIEW `vw_shipment_headers_shipment` AS
SELECT
  `cw`.`store_name`                                                  AS `store_name`,
  IFNULL(`sh`.`shipment_id`, -(1))                                   AS `shipment_id`,
  `sh`.`shipment_no`                                                 AS `shipment_no`,
  CAST('SHIPMENT' AS CHAR(25)
  CHARSET latin1)                                                    AS `document_type`,
  `sh`.`created_at`                                                  AS `document_date`,
  `sh`.`created_at`                                                  AS `document_updated_at`,
  `sh`.`shipment_id`                                                 AS `document_id`,
  `sh`.`created_at`                                                  AS `shipment_date`,
  `sh`.`updated_at`                                                  AS `updated_at`,
  `ih`.`invoice_id`                                                  AS `invoice_id`,
  `ih`.`invoice_no`                                                  AS `invoice_no`,
  `ih`.`created_at`                                                  AS `invoice_date`,
  `oh`.`order_id`                                                    AS `order_id`,
  `oh`.`order_no`                                                    AS `order_no`,
  `oh`.`created_at`                                                  AS `order_date`,
  `shr`.`rank`                                                       AS `shipment_no_of_order`,
  `oh`.`customer_id`                                                 AS `customer_id`,
  `oh`.`customer_email`                                              AS `customer_email`,
  `oh`.`customer_firstname`                                          AS `customer_firstname`,
  `oh`.`customer_lastname`                                           AS `customer_lastname`,
  `oh`.`customer_middlename`                                         AS `customer_middlename`,
  `oh`.`customer_prefix`                                             AS `customer_prefix`,
  `oh`.`customer_suffix`                                             AS `customer_suffix`,
  `oh`.`customer_taxvat`                                             AS `customer_taxvat`,
  `oh`.`status`                                                      AS `status`,
  `oh`.`coupon_code`                                                 AS `coupon_code`,
  `sh`.`total_weight`                                                AS `weight`,
  `oh`.`customer_gender`                                             AS `customer_gender`,
  `oh`.`customer_dob`                                                AS `customer_dob`,
  (
    CASE
    WHEN (
      CHAR_LENGTH(`sh`.`shipping_description`) > 1) THEN LEFT(`sh`.`shipping_description`,
                                                              LOCATE(' - ', `sh`.`shipping_description`))
    ELSE LEFT(`oh`.`shipping_description`, LOCATE(' - ', `oh`.`shipping_description`))
    END)                                                             AS `shipping_carrier`,
  (
    CASE
    WHEN (
      CHAR_LENGTH(`sh`.`shipping_description`) > 1) THEN `sh`.`shipping_description`
    ELSE `oh`.`shipping_description`
    END)                                                             AS `shipping_method`,
  (
    CASE `oh`.`warehouse_approved_time`
    WHEN '0000-00-00 00:00:00' THEN NULL
    ELSE `oh`.`warehouse_approved_time`
    END)                                                             AS `approved_time`,
  `sc`.`length_of_time_to_first_invoice`                             AS `length_of_time_to_invoice`,
  `sc`.`length_of_time_from_first_invoice_to_this_shipment`          AS `length_of_time_invoice_to_this_shipment`,
  (
    CASE
    WHEN (
      `ov`.`local_total_exc_vat` = 0) THEN 'Free'
    ELSE IFNULL(CONVERT(`pm`.`payment_method_name` USING utf8), `op`.`method`)
    END)                                                             AS `payment_method`,
  `op`.`amount_authorized`                                           AS `local_payment_authorized`,
  `op`.`amount_canceled`                                             AS `local_payment_canceled`,
  `op`.`cc_exp_month`                                                AS `cc_exp_month`,
  `op`.`cc_exp_year`                                                 AS `cc_exp_year`,
  `op`.`cc_ss_start_month`                                           AS `cc_start_month`,
  `op`.`cc_ss_start_year`                                            AS `cc_start_year`,
  `op`.`cc_last4`                                                    AS `cc_last4`,
  `op`.`cc_status_description`                                       AS `cc_status_description`,
  `op`.`cc_owner`                                                    AS `cc_owner`,
  IFNULL(CONVERT(`ctm`.`card_type_name` USING utf8), `op`.`cc_type`) AS `cc_type`,
  `op`.`cc_status`                                                   AS `cc_status`,
  `op`.`cc_ss_issue`                                                 AS `cc_issue_no`,
  `op`.`cc_avs_status`                                               AS `cc_avs_status`,
  CAST(NULL AS CHAR(255)
  CHARSET latin1)                                                    AS `payment_info1`,
  CAST(NULL AS CHAR(255)
  CHARSET latin1)                                                    AS `payment_info2`,
  CAST(NULL AS CHAR(255)
  CHARSET latin1)                                                    AS `payment_info3`,
  CAST(NULL AS CHAR(255)
  CHARSET latin1)                                                    AS `payment_info4`,
  CAST(NULL AS CHAR(255)
  CHARSET latin1)                                                    AS `payment_info5`,
  `bill`.`email`                                                     AS `billing_email`,
  `bill`.`company`                                                   AS `billing_company`,
  `bill`.`prefix`                                                    AS `billing_prefix`,
  `bill`.`firstname`                                                 AS `billing_firstname`,
  `bill`.`middlename`                                                AS `billing_middlename`,
  `bill`.`lastname`                                                  AS `billing_lastname`,
  `bill`.`suffix`                                                    AS `billing_suffix`,
  SUBSTRING_INDEX(`bill`.`street`, {$this->newLine}, 1)                            AS `billing_street1`,
  (
    CASE
    WHEN (
      LOCATE(
          {$this->newLine},
          `bill`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`bill`.`street`,
                                                             {$this->newLine},
                                                             2), (LOCATE(
                                                                      {$this->newLine},
                                                                      `bill`.`street`) + 1))
    ELSE ''
    END)                                                             AS `billing_street2`,
  `bill`.`city`                                                      AS `billing_city`,
  `bill`.`region`                                                    AS `billing_region`,
  `bill`.`region_id`                                                 AS `billing_region_id`,
  `bill`.`postcode`                                                  AS `billing_postcode`,
  `bill`.`country_id`                                                AS `billing_country_id`,
  `bill`.`telephone`                                                 AS `billing_telephone`,
  `bill`.`fax`                                                       AS `billing_fax`,
  `ship`.`email`                                                     AS `shipping_email`,
  `ship`.`company`                                                   AS `shipping_company`,
  `ship`.`prefix`                                                    AS `shipping_prefix`,
  `ship`.`firstname`                                                 AS `shipping_firstname`,
  `ship`.`middlename`                                                AS `shipping_middlename`,
  `ship`.`lastname`                                                  AS `shipping_lastname`,
  `ship`.`suffix`                                                    AS `shipping_suffix`,
  SUBSTRING_INDEX(`ship`.`street`, {$this->newLine}, 1)                            AS `shiping_street1`,
  (
    CASE
    WHEN (
      LOCATE(
          {$this->newLine},
          `ship`.`street`) <> 0) THEN SUBSTR(SUBSTRING_INDEX(`ship`.`street`,
                                                             {$this->newLine},
                                                             2), (LOCATE(
                                                                      {$this->newLine},
                                                                      `ship`.`street`) + 1))
    ELSE ''
    END)                                                             AS `shipping_street2`,
  `ship`.`city`                                                      AS `shipping_city`,
  `ship`.`region`                                                    AS `shipping_region`,
  `ship`.`region_id`                                                 AS `shipping_region_id`,
  `ship`.`postcode`                                                  AS `shipping_postcode`,
  `ship`.`country_id`                                                AS `shipping_country_id`,
  `ship`.`telephone`                                                 AS `shipping_telephone`,
  `ship`.`fax`                                                       AS `shipping_fax`,
  `ov`.`has_lens`                                                    AS `has_lens`,
  `ov`.`total_qty`                                                   AS `total_qty`,
  `ov`.`prof_fee`                                                    AS `local_prof_fee`,
  `ov`.`local_subtotal_inc_vat`                                      AS `local_subtotal_inc_vat`,
  `ov`.`local_subtotal_vat`                                          AS `local_subtotal_vat`,
  `ov`.`local_subtotal_exc_vat`                                      AS `local_subtotal_exc_vat`,
  `ov`.`local_shipping_inc_vat`                                      AS `local_shipping_inc_vat`,
  `ov`.`local_shipping_vat`                                          AS `local_shipping_vat`,
  `ov`.`local_shipping_exc_vat`                                      AS `local_shipping_exc_vat`,
  (-(1) * ABS(`ov`.`local_discount_inc_vat`))                        AS `local_discount_inc_vat`,
  (-(1) * ABS(`ov`.`local_discount_vat`))                            AS `local_discount_vat`,
  (-(1) * ABS(`ov`.`local_discount_exc_vat`))                        AS `local_discount_exc_vat`,
  (-(1) * ABS(`ov`.`local_store_credit_inc_vat`))                    AS `local_store_credit_inc_vat`,
  (-(1) * ABS(`ov`.`local_store_credit_vat`))                        AS `local_store_credit_vat`,
  (-(1) * ABS(`ov`.`local_store_credit_exc_vat`))                    AS `local_store_credit_exc_vat`,
  `ov`.`local_adjustment_inc_vat`                                    AS `local_adjustment_inc_vat`,
  `ov`.`local_adjustment_vat`                                        AS `local_adjustment_vat`,
  `ov`.`local_adjustment_exc_vat`                                    AS `local_adjustment_exc_vat`,
  `ov`.`local_total_inc_vat`                                         AS `local_total_inc_vat`,
  `ov`.`local_total_vat`                                             AS `local_total_vat`,
  `ov`.`local_total_exc_vat`                                         AS `local_total_exc_vat`,
  `ov`.`local_subtotal_cost`                                         AS `local_subtotal_cost`,
  `ov`.`local_shipping_cost`                                         AS `local_shipping_cost`,
  `ov`.`local_total_cost`                                            AS `local_total_cost`,
  `ov`.`local_margin_amount`                                         AS `local_margin_amount`,
  `ov`.`local_margin_percent`                                        AS `local_margin_percent`,
  `oh`.`base_to_global_rate`                                         AS `local_to_global_rate`,
  `oh`.`order_currency_code`                                         AS `order_currency_code`,
  `ov`.`global_prof_fee`                                             AS `global_prof_fee`,
  `ov`.`global_subtotal_inc_vat`                                     AS `global_subtotal_inc_vat`,
  `ov`.`global_subtotal_vat`                                         AS `global_subtotal_vat`,
  `ov`.`global_subtotal_exc_vat`                                     AS `global_subtotal_exc_vat`,
  `ov`.`global_shipping_inc_vat`                                     AS `global_shipping_inc_vat`,
  `ov`.`global_shipping_vat`                                         AS `global_shipping_vat`,
  `ov`.`global_shipping_exc_vat`                                     AS `global_shipping_exc_vat`,
  (-(1) * ABS(`ov`.`global_discount_inc_vat`))                       AS `global_discount_inc_vat`,
  (-(1) * ABS(`ov`.`global_discount_vat`))                           AS `global_discount_vat`,
  (-(1) * ABS(`ov`.`global_discount_exc_vat`))                       AS `global_discount_exc_vat`,
  (-(1) * ABS(`ov`.`global_store_credit_inc_vat`))                   AS `global_store_credit_inc_vat`,
  (-(1) * ABS(`ov`.`global_store_credit_vat`))                       AS `global_store_credit_vat`,
  (-(1) * ABS(`ov`.`global_store_credit_exc_vat`))                   AS `global_store_credit_exc_vat`,
  `ov`.`global_adjustment_inc_vat`                                   AS `global_adjustment_inc_vat`,
  `ov`.`global_adjustment_vat`                                       AS `global_adjustment_vat`,
  `ov`.`global_adjustment_exc_vat`                                   AS `global_adjustment_exc_vat`,
  `ov`.`global_total_inc_vat`                                        AS `global_total_inc_vat`,
  `ov`.`global_total_vat`                                            AS `global_total_vat`,
  `ov`.`global_total_exc_vat`                                        AS `global_total_exc_vat`,
  `ov`.`global_subtotal_cost`                                        AS `global_subtotal_cost`,
  `ov`.`global_shipping_cost`                                        AS `global_shipping_cost`,
  `ov`.`global_total_cost`                                           AS `global_total_cost`,
  `ov`.`global_margin_amount`                                        AS `global_margin_amount`,
  `ov`.`global_margin_percent`                                       AS `global_margin_percent`,
  `ov`.`prof_fee_percent`                                            AS `prof_fee_percent`,
  `ov`.`vat_percent_before_prof_fee`                                 AS `vat_percent_before_prof_fee`,
  `ov`.`vat_percent`                                                 AS `vat_percent`,
  ABS(`ov`.`discount_percent`)                                       AS `discount_percent`,
  CAST(`oh`.`customer_note` AS CHAR(8000)
  CHARSET latin1)                                                    AS `customer_note`,
  CAST(`oh`.`customer_note_notify` AS CHAR(8000)
  CHARSET latin1)                                                    AS `customer_note_notify`,
  `oh`.`remote_ip`                                                   AS `remote_ip`,
  `oh`.`affilcode`                                                   AS `business_source`,
  `ochan`.`channel`                                                  AS `business_channel`,
  `oh`.`affilbatch`                                                  AS `affilbatch`,
  `oh`.`affilcode`                                                   AS `affilcode`,
  `oh`.`affiluserref`                                                AS `affiluserref`,
  `oh`.`postoptics_auto_verification`                                AS `auto_verification`,
  (
    CASE
    WHEN (
      `oh`.`automatic_reorder` = 1) THEN 'Automatic'
    WHEN (
      `oh`.`postoptics_source` = 'user') THEN 'Web'
    WHEN (
      `oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
    WHEN ISNULL(`oh`.`postoptics_source`) THEN 'Web'
    ELSE `oh`.`postoptics_source`
    END)                                                             AS `order_type`,
  `ohm`.`order_lifecycle`                                            AS `order_lifecycle`,
  `seq`.`order_seq`                                                  AS `customer_order_seq_no`,
  (
    CASE
    WHEN (
      TO_DAYS(`oh`.`reminder_date`) > 693961) THEN CAST(`oh`.`reminder_date` AS DATE)
    ELSE NULL
    END)                                                             AS `reminder_date`,
  `oh`.`reminder_mobile`                                             AS `reminder_mobile`,
  `oh`.`reminder_period`                                             AS `reminder_period`,
  `oh`.`reminder_presc`                                              AS `reminder_presc`,
  `oh`.`reminder_type`                                               AS `reminder_type`,
  `oh`.`reminder_sent`                                               AS `reminder_sent`,
  `oh`.`reminder_follow_sent`                                        AS `reminder_follow_sent`,
  `oh`.`telesales_method_code`                                       AS `telesales_method_code`,
  `oh`.`telesales_admin_username`                                    AS `telesales_admin_username`,
  `oh`.`reorder_on_flag`                                             AS `reorder_on_flag`,
  `oh`.`reorder_profile_id`                                          AS `reorder_profile_id`,
  (
    CASE
    WHEN (
      (
        CAST(`rp`.`enddate` AS DATE) <= CAST(NOW() AS DATE))
      OR (
        `rp`.`completed_profile` <> 1)) THEN NULL
    ELSE `rp`.`next_order_date`
    END)                                                             AS `reorder_date`,
  `rp`.`interval_days`                                               AS `reorder_interval`,
  `rqp`.`cc_exp_month`                                               AS `reorder_cc_exp_month`,
  `rqp`.`cc_exp_year`                                                AS `reorder_cc_exp_year`,
  `oh`.`automatic_reorder`                                           AS `automatic_reorder`,
  (
    CASE
    WHEN (
      (
        (
          `oh`.`presc_verification_method` = 'call')
        OR ISNULL(`oh`.`presc_verification_method`))
      AND (
        `conf`.`value` = 1)
      AND (
        `ov2`.`has_lens` = 1)
      AND (
        `oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Call'
    WHEN (
      (
        `oh`.`presc_verification_method` = 'upload')
      AND (
        `conf`.`value` = 1)
      AND (
        `ov2`.`has_lens` = 1)
      AND (
        `oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Upload / Fax / Scan'
    WHEN (
      (
        `conf`.`value` = 1)
      AND (
        `ov2`.`has_lens` = 1)
      AND (
        `oh`.`postoptics_auto_verification` = 'Yes')) THEN 'Automatic'
    ELSE 'Not Required'
    END)                                                             AS `presc_verification_method`,
  `oh`.`referafriend_code`                                           AS `referafriend_code`,
  `oh`.`referafriend_referer`                                        AS `referafriend_referer`
FROM (((((((((((((((((((((`dw_shipment_headers` `sh`
  JOIN `dw_order_headers` `oh`
    ON ((
    `oh`.`order_id` = `sh`.`order_id`)))
  LEFT JOIN `dw_stores` `cw`
    ON ((
    `cw`.`store_id` = `oh`.`store_id`)))
  LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op`
    ON ((
    `op`.`parent_id` = `oh`.`order_id`)))
  LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill`
    ON ((
    `bill`.`entity_id` = `oh`.`billing_address_id`)))
  LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship`
    ON ((
    `ship`.`entity_id` = `oh`.`shipping_address_id`)))
  JOIN `shipment_headers_vat` `ov`
    ON ((
    `ov`.`shipment_id` = `sh`.`shipment_id`)))
  JOIN `order_headers_vat` `ov2`
    ON ((
    `ov2`.`order_id` = `oh`.`order_id`)))
  LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp`
    ON ((
    `oh`.`reorder_profile_id` = `rp`.`id`)))
  LEFT JOIN `dw_shipment_calender_dates` `sc`
    ON ((
    `sc`.`shipment_id` = `sh`.`shipment_id`)))
  LEFT JOIN `first_inv` `fi`
    ON ((
    `sh`.`order_id` = `fi`.`order_id`)))
  LEFT JOIN `dw_invoice_headers` `ih`
    ON ((
    `ih`.`invoice_id` = `fi`.`invoice_id`)))
  LEFT JOIN `dw_payment_method_map` `pm`
    ON ((
    CONVERT(`pm`.`payment_method` USING utf8) = `op`.`method`)))
  LEFT JOIN `dw_card_type_map` `ctm`
    ON ((
    CONVERT(`ctm`.`card_type` USING utf8) = `op`.`cc_type`)))
  LEFT JOIN `{$this->dbname}`.`po_reorder_quote` `rq`
    ON ((
    `rq`.`entity_id` = `rp`.`reorder_quote_id`)))
  LEFT JOIN `max_reorder_quote_payment` `mp`
    ON ((
    `rq`.`entity_id` = `mp`.`quote_id`)))
  LEFT JOIN `{$this->dbname}`.`po_reorder_quote_payment` `rqp`
    ON ((
    `rqp`.`payment_id` = `mp`.`payment_id`)))
  LEFT JOIN `dw_order_channel` `ochan`
    ON ((
    `ochan`.`order_id` = `oh`.`order_id`)))
  LEFT JOIN `dw_order_headers_marketing` `ohm`
    ON ((
    `ohm`.`order_id` = `oh`.`order_id`)))
  LEFT JOIN `dw_entity_header_rank_seq` `seq`
    ON (((
           `seq`.`document_id` = `oh`.`order_id`)
         AND (
    `seq`.`document_type` = 'ORDER'))))
  LEFT JOIN `shipment_headers_rank` `shr`
    ON ((
    `shr`.`shipment_id` = `sh`.`shipment_id`)))
  LEFT JOIN `dw_conf_presc_required` `conf`
    ON ((
    `conf`.`store_id` = `oh`.`store_id`)));


CREATE OR REPLACE VIEW `vw_shipment_headers_creditmemo`
AS
  SELECT    `cw`.`store_name`                             AS `store_name`,
            ifnull(`sh`.`shipment_id`,-(1))               AS `shipment_id`,
            `sh`.`shipment_no`                            AS `shipment_no`,
            cast('CREDITMEMO' AS CHAR(25) charset latin1) AS `document_type`,
            `oh2`.`created_at`                            AS `document_date`,
            `oh2`.`updated_at`                            AS `document_updated_at`,
            `oh2`.`creditmemo_id`                         AS `document_id`,
            `oh2`.`created_at`                            AS `shipment_date`,
            `oh2`.`updated_at`                            AS `updated_at`,
            `ih`.`invoice_id`                             AS `invoice_id`,
            `ih`.`invoice_no`                             AS `invoice_no`,
            `ih`.`created_at`                             AS `invoice_date`,
            `oh`.`order_id`                               AS `order_id`,
            `oh`.`order_no`                               AS `order_no`,
            `oh`.`created_at`                             AS `order_date`,
            `shr`.`rank`                                  AS `shipment_no_of_order`,
            `oh`.`customer_id`                            AS `customer_id`,
            `oh`.`customer_email`                         AS `customer_email`,
            `oh`.`customer_firstname`                     AS `customer_firstname`,
            `oh`.`customer_lastname`                      AS `customer_lastname`,
            `oh`.`customer_middlename`                    AS `customer_middlename`,
            `oh`.`customer_prefix`                        AS `customer_prefix`,
            `oh`.`customer_suffix`                        AS `customer_suffix`,
            `oh`.`customer_taxvat`                        AS `customer_taxvat`,
            `oh`.`status`                                 AS `status`,
            `oh`.`coupon_code`                            AS `coupon_code`,
            (-(1) * `ov`.`total_weight`)                  AS `weight`,
            `oh`.`customer_gender`                        AS `customer_gender`,
            `oh`.`customer_dob`                           AS `customer_dob`,(
            CASE
                      WHEN (
                                          char_length(`sh`.`shipping_description`) > 1) THEN LEFT(`sh`.`shipping_description`,locate(' - ',`sh`.`shipping_description`))
                      ELSE LEFT(`oh`.`shipping_description`,locate(' - ',`oh`.`shipping_description`))
            end) AS `shipping_carrier`,(
            CASE
                      WHEN (
                                          char_length(`sh`.`shipping_description`) > 1) THEN `sh`.`shipping_description`
                      ELSE `oh`.`shipping_description`
            end) AS `shipping_method`,(
            CASE `oh`.`warehouse_approved_time`
                      WHEN '0000-00-00 00:00:00' THEN NULL
                      ELSE `oh`.`warehouse_approved_time`
            end)                                                      AS `approved_time`,
            `sc`.`length_of_time_to_first_invoice`                    AS `length_of_time_to_invoice`,
            `sc`.`length_of_time_from_first_invoice_to_this_shipment` AS `length_of_time_invoice_to_this_shipment`,(
            CASE
                      WHEN (
                                          `ov`.`local_total_exc_vat` = 0) THEN 'Free'
                      ELSE ifnull(CONVERT(`pm`.`payment_method_name` USING utf8),`op`.`method`)
            end)                                                              AS `payment_method`,
            `op`.`amount_authorized`                                          AS `local_payment_authorized`,
            `op`.`amount_canceled`                                            AS `local_payment_canceled`,
            `op`.`cc_exp_month`                                               AS `cc_exp_month`,
            `op`.`cc_exp_year`                                                AS `cc_exp_year`,
            `op`.`cc_ss_start_month`                                          AS `cc_start_month`,
            `op`.`cc_ss_start_year`                                           AS `cc_start_year`,
            `op`.`cc_last4`                                                   AS `cc_last4`,
            `op`.`cc_status_description`                                      AS `cc_status_description`,
            `op`.`cc_owner`                                                   AS `cc_owner`,
            ifnull(CONVERT(`ctm`.`card_type_name` USING utf8),`op`.`cc_type`) AS `cc_type`,
            `op`.`cc_status`                                                  AS `cc_status`,
            `op`.`cc_ss_issue`                                                AS `cc_issue_no`,
            `op`.`cc_avs_status`                                              AS `cc_avs_status`,
            cast(NULL AS CHAR(255) charset latin1)                            AS `payment_info1`,
            cast(NULL AS CHAR(255) charset latin1)                            AS `payment_info2`,
            cast(NULL AS CHAR(255) charset latin1)                            AS `payment_info3`,
            cast(NULL AS CHAR(255) charset latin1)                            AS `payment_info4`,
            cast(NULL AS CHAR(255) charset latin1)                            AS `payment_info5`,
            `bill`.`email`                                                    AS `billing_email`,
            `bill`.`company`                                                  AS `billing_company`,
            `bill`.`prefix`                                                   AS `billing_prefix`,
            `bill`.`firstname`                                                AS `billing_firstname`,
            `bill`.`middlename`                                               AS `billing_middlename`,
            `bill`.`lastname`                                                 AS `billing_lastname`,
            `bill`.`suffix`                                                   AS `billing_suffix`,
            substring_index(`bill`.`street`,{$this->newLine},1)                           AS `billing_street1`,(
            CASE
                      WHEN (
                                          locate({$this->newLine},`bill`.`street`) <> 0) THEN substr(substring_index(`bill`.`street`,{$this->newLine},2),(locate({$this->newLine},`bill`.`street`) + 1))
                      ELSE ''
            end)                                    AS `billing_street2`,
            `bill`.`city`                           AS `billing_city`,
            `bill`.`region`                         AS `billing_region`,
            `bill`.`region_id`                      AS `billing_region_id`,
            `bill`.`postcode`                       AS `billing_postcode`,
            `bill`.`country_id`                     AS `billing_country_id`,
            `bill`.`telephone`                      AS `billing_telephone`,
            `bill`.`fax`                            AS `billing_fax`,
            `ship`.`email`                          AS `shipping_email`,
            `ship`.`company`                        AS `shipping_company`,
            `ship`.`prefix`                         AS `shipping_prefix`,
            `ship`.`firstname`                      AS `shipping_firstname`,
            `ship`.`middlename`                     AS `shipping_middlename`,
            `ship`.`lastname`                       AS `shipping_lastname`,
            `ship`.`suffix`                         AS `shipping_suffix`,
            substring_index(`ship`.`street`,{$this->newLine},1) AS `shiping_street1`,(
            CASE
                      WHEN (
                                          locate({$this->newLine},`ship`.`street`) <> 0) THEN substr(substring_index(`ship`.`street`,{$this->newLine},2),(locate({$this->newLine},`ship`.`street`) + 1))
                      ELSE ''
            end)                                                    AS `shipping_street2`,
            `ship`.`city`                                           AS `shipping_city`,
            `ship`.`region`                                         AS `shipping_region`,
            `ship`.`region_id`                                      AS `shipping_region_id`,
            `ship`.`postcode`                                       AS `shipping_postcode`,
            `ship`.`country_id`                                     AS `shipping_country_id`,
            `ship`.`telephone`                                      AS `shipping_telephone`,
            `ship`.`fax`                                            AS `shipping_fax`,
            `ov`.`has_lens`                                         AS `has_lens`,
            (-(1) * `ov`.`total_qty`)                               AS `total_qty`,
            (-(1) * abs(`ov`.`prof_fee`))                           AS `local_prof_fee`,
            (-(1) * abs(`ov`.`local_subtotal_inc_vat`))             AS `local_subtotal_inc_vat`,
            (-(1) * abs(`ov`.`local_subtotal_vat`))                 AS `local_subtotal_vat`,
            (-(1) * abs(`ov`.`local_subtotal_exc_vat`))             AS `local_subtotal_exc_vat`,
            (-(1) * abs(`ov`.`local_shipping_inc_vat`))             AS `local_shipping_inc_vat`,
            (-(1) * abs(`ov`.`local_shipping_vat`))                 AS `local_shipping_vat`,
            (-(1) * abs(`ov`.`local_shipping_exc_vat`))             AS `local_shipping_exc_vat`,
            abs(`ov`.`local_discount_inc_vat`)                      AS `local_discount_inc_vat`,
            abs(`ov`.`local_discount_vat`)                          AS `local_discount_vat`,
            abs(`ov`.`local_discount_exc_vat`)                      AS `local_discount_exc_vat`,
            abs(`ov`.`local_store_credit_inc_vat`)                  AS `local_store_credit_inc_vat`,
            abs(`ov`.`local_store_credit_vat`)                      AS `local_store_credit_vat`,
            abs(`ov`.`local_store_credit_exc_vat`)                  AS `local_store_credit_exc_vat`,
            (-(1) * `ov`.`local_adjustment_inc_vat`)                AS `local_adjustment_inc_vat`,
            (-(1) * `ov`.`local_adjustment_vat`)                    AS `local_adjustment_vat`,
            (-(1) * `ov`.`local_adjustment_exc_vat`)                AS `local_adjustment_exc_vat`,
            (-(1) * `ov`.`local_total_inc_vat`)                     AS `local_total_inc_vat`,
            (-(1) * `ov`.`local_total_vat`)                         AS `local_total_vat`,
            (-(1) * `ov`.`local_total_exc_vat`)                     AS `local_total_exc_vat`,
            (-(1) * abs(`ov`.`local_subtotal_cost`))                AS `local_subtotal_cost`,
            (-(1) * abs(`ov`.`local_shipping_cost`))                AS `local_shipping_cost`,
            (-(1) * abs(`ov`.`local_total_cost`))                   AS `local_total_cost`,
            (-(1) * abs(`ov`.`local_margin_amount`))                AS `local_margin_amount`,
            `ov`.`local_margin_percent`                             AS `local_margin_percent`,
            `oh`.`base_to_global_rate`                              AS `local_to_global_rate`,
            `oh`.`order_currency_code`                              AS `order_currency_code`,
            (-(1) * abs(`ov`.`global_prof_fee`))                    AS `global_prof_fee`,
            (-(1) * abs(`ov`.`global_subtotal_inc_vat`))            AS `global_subtotal_inc_vat`,
            (-(1) * abs(`ov`.`global_subtotal_vat`))                AS `global_subtotal_vat`,
            (-(1) * abs(`ov`.`global_subtotal_exc_vat`))            AS `global_subtotal_exc_vat`,
            (-(1) * abs(`ov`.`global_shipping_inc_vat`))            AS `global_shipping_inc_vat`,
            (-(1) * abs(`ov`.`global_shipping_vat`))                AS `global_shipping_vat`,
            (-(1) * abs(`ov`.`global_shipping_exc_vat`))            AS `global_shipping_exc_vat`,
            abs(`ov`.`global_discount_inc_vat`)                     AS `global_discount_inc_vat`,
            abs(`ov`.`global_discount_vat`)                         AS `global_discount_vat`,
            abs(`ov`.`global_discount_exc_vat`)                     AS `global_discount_exc_vat`,
            abs(`ov`.`global_store_credit_inc_vat`)                 AS `global_store_credit_inc_vat`,
            abs(`ov`.`global_store_credit_vat`)                     AS `global_store_credit_vat`,
            abs(`ov`.`global_store_credit_exc_vat`)                 AS `global_store_credit_exc_vat`,
            (-(1) * `ov`.`global_adjustment_inc_vat`)               AS `global_adjustment_inc_vat`,
            (-(1) * `ov`.`global_adjustment_vat`)                   AS `global_adjustment_vat`,
            (-(1) * `ov`.`global_adjustment_exc_vat`)               AS `global_adjustment_exc_vat`,
            (-(1) * `ov`.`global_total_inc_vat`)                    AS `global_total_inc_vat`,
            (-(1) * `ov`.`global_total_vat`)                        AS `global_total_vat`,
            (-(1) * abs(`ov`.`global_total_exc_vat`))               AS `global_total_exc_vat`,
            (-(1) * abs(`ov`.`global_subtotal_cost`))               AS `global_subtotal_cost`,
            (-(1) * abs(`ov`.`global_shipping_cost`))               AS `global_shipping_cost`,
            (-(1) * abs(`ov`.`global_total_cost`))                  AS `global_total_cost`,
            (-(1) * abs(`ov`.`global_margin_amount`))               AS `global_margin_amount`,
            `ov`.`global_margin_percent`                            AS `global_margin_percent`,
            `ov`.`prof_fee_percent`                                 AS `prof_fee_percent`,
            `ov`.`vat_percent_before_prof_fee`                      AS `vat_percent_before_prof_fee`,
            `ov`.`vat_percent`                                      AS `vat_percent`,
            abs(`ov`.`discount_percent`)                            AS `discount_percent`,
            cast(`oh`.`customer_note` AS        CHAR(8000) charset latin1) AS `customer_note`,
            cast(`oh`.`customer_note_notify` AS CHAR(8000) charset latin1) AS `customer_note_notify`,
            `oh`.`remote_ip`                                               AS `remote_ip`,
            `oh`.`affilcode`                                               AS `business_source`,
            `ochan`.`channel`                                              AS `business_channel`,
            `oh`.`affilbatch`                                              AS `affilbatch`,
            `oh`.`affilcode`                                               AS `affilcode`,
            `oh`.`affiluserref`                                            AS `affiluserref`,
            `oh`.`postoptics_auto_verification`                            AS `auto_verification`,(
            CASE
                      WHEN (
                                          `oh`.`automatic_reorder` = 1) THEN 'Automatic'
                      WHEN (
                                          `oh`.`postoptics_source` = 'user') THEN 'Web'
                      WHEN (
                                          `oh`.`postoptics_source` = 'telesales') THEN 'Telesales'
                      WHEN isnull(`oh`.`postoptics_source`) THEN 'Web'
                      ELSE `oh`.`postoptics_source`
            end)                    AS `order_type`,
            `ohm`.`order_lifecycle` AS `order_lifecycle`,
            `seq`.`order_seq`       AS `customer_order_seq_no`,(
            CASE
                      WHEN (
                                          to_days(`oh`.`reminder_date`) > 693961) THEN cast(`oh`.`reminder_date` AS date)
                      ELSE NULL
            end)                            AS `reminder_date`,
            `oh`.`reminder_mobile`          AS `reminder_mobile`,
            `oh`.`reminder_period`          AS `reminder_period`,
            `oh`.`reminder_presc`           AS `reminder_presc`,
            `oh`.`reminder_type`            AS `reminder_type`,
            `oh`.`reminder_sent`            AS `reminder_sent`,
            `oh`.`reminder_follow_sent`     AS `reminder_follow_sent`,
            `oh`.`telesales_method_code`    AS `telesales_method_code`,
            `oh`.`telesales_admin_username` AS `telesales_admin_username`,
            `oh`.`reorder_on_flag`          AS `reorder_on_flag`,
            `oh`.`reorder_profile_id`       AS `reorder_profile_id`,(
            CASE
                      WHEN (
                                          (
                                                    cast(`rp`.`enddate` AS date) <= cast(now() AS date))
                                OR        (
                                                    `rp`.`completed_profile` <> 1)) THEN NULL
                      ELSE `rp`.`next_order_date`
            end)                     AS `reorder_date`,
            `rp`.`interval_days`     AS `reorder_interval`,
            `rqp`.`cc_exp_month`     AS `reorder_cc_exp_month`,
            `rqp`.`cc_exp_year`      AS `reorder_cc_exp_year`,
            `oh`.`automatic_reorder` AS `automatic_reorder`,(
            CASE
                      WHEN (
                                          (
                                                    (
                                                              `oh`.`presc_verification_method` = 'call')
                                          OR        isnull(`oh`.`presc_verification_method`))
                                AND       (
                                                    `conf`.`value` = 1)
                                AND       (
                                                    `ov2`.`has_lens` = 1)
                                AND       (
                                                    `oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Call'
                      WHEN (
                                          (
                                                    `oh`.`presc_verification_method` = 'upload')
                                AND       (
                                                    `conf`.`value` = 1)
                                AND       (
                                                    `ov2`.`has_lens` = 1)
                                AND       (
                                                    `oh`.`postoptics_auto_verification` <> 'Yes')) THEN 'Upload / Fax / Scan'
                      WHEN (
                                          (
                                                    `conf`.`value` = 1)
                                AND       (
                                                    `ov2`.`has_lens` = 1)
                                AND       (
                                                    `oh`.`postoptics_auto_verification` = 'Yes')) THEN 'Automatic'
                      ELSE 'Not Required'
            end)                        AS `presc_verification_method`,
            `oh`.`referafriend_code`    AS `referafriend_code`,
            `oh`.`referafriend_referer` AS `referafriend_referer`
  FROM      ((((((((((((((((((((((`dw_creditmemo_headers` `oh2`
  JOIN      `dw_order_headers` `oh`
  ON       ((
                                `oh2`.`order_id` = `oh`.`order_id`)))
  LEFT JOIN `dw_stores` `cw`
  ON       ((
                                `cw`.`store_id` = `oh`.`store_id`)))
  LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op`
  ON       ((
                                `op`.`parent_id` = `oh`.`order_id`)))
  LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill`
  ON       ((
                                `bill`.`entity_id` = `oh`.`billing_address_id`)))
  LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship`
  ON       ((
                                `ship`.`entity_id` = `oh`.`shipping_address_id`)))
  JOIN      `creditmemo_shipment_headers_vat` `ov`
  ON       ((
                                `ov`.`creditmemo_id` = `oh2`.`creditmemo_id`)))
  JOIN      `order_headers_vat` `ov2`
  ON       ((
                                `ov2`.`order_id` = `oh`.`order_id`)))
  LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp`
  ON       ((
                                `oh`.`reorder_profile_id` = `rp`.`id`)))
  LEFT JOIN `first_inv` `fi`
  ON       ((
                                `oh`.`order_id` = `fi`.`order_id`)))
  LEFT JOIN `dw_invoice_headers` `ih`
  ON       ((
                                `ih`.`invoice_id` = `fi`.`invoice_id`)))
  LEFT JOIN `dw_payment_method_map` `pm`
  ON       ((
                                CONVERT(`pm`.`payment_method` USING utf8) = `op`.`method`)))
  LEFT JOIN `dw_card_type_map` `ctm`
  ON       ((
                                CONVERT(`ctm`.`card_type` USING utf8) = `op`.`cc_type`)))
  LEFT JOIN `{$this->dbname}`.`po_reorder_quote` `rq`
  ON       ((
                                `rq`.`entity_id` = `rp`.`reorder_quote_id`)))
  LEFT JOIN `max_reorder_quote_payment` `mp`
  ON       ((
                                `rq`.`entity_id` = `mp`.`quote_id`)))
  LEFT JOIN `{$this->dbname}`.`po_reorder_quote_payment` `rqp`
  ON       ((
                                `rqp`.`payment_id` = `mp`.`payment_id`)))
  LEFT JOIN `dw_order_channel` `ochan`
  ON       ((
                                `ochan`.`order_id` = `oh`.`order_id`)))
  LEFT JOIN `dw_order_headers_marketing` `ohm`
  ON       ((
                                `ohm`.`order_id` = `oh`.`order_id`)))
  LEFT JOIN `dw_entity_header_rank_seq` `seq`
  ON       (((
                                          `seq`.`document_id` = `oh2`.`creditmemo_id`)
                      AND       (
                                          `seq`.`document_type` = 'CREDITMEMO'))))
  LEFT JOIN `dw_shipment_headers` `sh`
  ON       ((
                                `ov`.`shipment_id` = `sh`.`shipment_id`)))
  LEFT JOIN `dw_shipment_calender_dates` `sc`
  ON       ((
                                `sc`.`shipment_id` = `sh`.`shipment_id`)))
  LEFT JOIN `shipment_headers_rank` `shr`
  ON       ((
                                `shr`.`shipment_id` = `sh`.`shipment_id`)))
  LEFT JOIN `dw_conf_presc_required` `conf`
  ON       ((
                                `conf`.`store_id` = `oh`.`store_id`)));


CREATE OR REPLACE VIEW vw_shipment_lines_shipment AS
  SELECT
    `sl`.`item_id`                                     AS `line_id`,
    `sl`.`shipment_id`                                 AS `shipment_id`,
    `cw`.`store_name`                                  AS `store_name`,
    `sh`.`shipment_no`                                 AS `shipment_no`,
    CAST('SHIPMENT' AS CHAR(25)
    CHARSET latin1)                                    AS `document_type`,
    `sh`.`created_at`                                  AS `document_date`,
    `sh`.`updated_at`                                  AS `document_updated_at`,
    `sh`.`shipment_id`                                 AS `document_id`,
    `sh`.`created_at`                                  AS `shipment_date`,
    `sh`.`updated_at`                                  AS `updated_at`,
    `sl`.`product_id`                                  AS `product_id`,
    `cf`.`product_type`                                AS `product_type`,
    CAST(`ol`.`product_options` AS CHAR(8000)
    CHARSET latin1)                                    AS `product_options`,
    `p`.`is_lens`                                      AS `is_lens`,
    CAST('' AS CHAR(1)
    CHARSET latin1)                                    AS `lens_eye`,
    `esi`.`BC`                                         AS `lens_base_curve`,
    `esi`.`DI`                                         AS `lens_diameter`,
    `esi`.`PO`                                         AS `lens_power`,
    `esi`.`CY`                                         AS `lens_cylinder`,
    `esi`.`AX`                                         AS `lens_axis`,
    `esi`.`AD`                                         AS `lens_addition`,
    `esi`.`DO`                                         AS `lens_dominance`,
    `esi`.`CO`                                         AS `lens_colour`,
    `p`.`daysperlens`                                  AS `lens_days`,
    CAST('' AS CHAR(1)
    CHARSET latin1)                                    AS `product_size`,
    CAST('' AS CHAR(1)
    CHARSET latin1)                                    AS `product_colour`,
    `sl`.`weight`                                      AS `unit_weight`,
    (`sl`.`weight` * `sl`.`qty`)                       AS `line_weight`,
    `ol`.`is_virtual`                                  AS `is_virtual`,
    `sl`.`sku`                                         AS `sku`,
    `sl`.`name`                                        AS `name`,
    `pcf`.`category_id`                                AS `category_id`,
    (CASE
     WHEN (`oh`.`base_shipping_amount` > 0) THEN 1
     ELSE 0
     END)                                              AS `free_shipping`,
    (CASE
     WHEN (`oh`.`base_discount_amount` > 0) THEN 1
     ELSE 0
     END)                                              AS `no_discount`,
    `sl`.`qty`                                         AS `qty`,
    `bo`.`qty_prior`                                   AS `qty_backordered`,
    `olv`.`local_prof_fee`                             AS `local_prof_fee`,
    `olv`.`local_price_inc_vat`                        AS `local_price_inc_vat`,
    `olv`.`local_price_vat`                            AS `local_price_vat`,
    `olv`.`local_price_exc_vat`                        AS `local_price_exc_vat`,
    `olv`.`local_line_subtotal_inc_vat`                AS `local_line_subtotal_inc_vat`,
    `olv`.`local_line_subtotal_vat`                    AS `local_line_subtotal_vat`,
    `olv`.`local_line_subtotal_exc_vat`                AS `local_line_subtotal_exc_vat`,
    `olv`.`local_shipping_inc_vat`                     AS `local_shipping_inc_vat`,
    `olv`.`local_shipping_vat`                         AS `local_shipping_vat`,
    `olv`.`local_shipping_exc_vat`                     AS `local_shipping_exc_vat`,
    (-(1) * ABS(`olv`.`local_discount_inc_vat`))       AS `local_discount_inc_vat`,
    (-(1) * ABS(`olv`.`local_discount_vat`))           AS `local_discount_vat`,
    (-(1) * ABS(`olv`.`local_discount_exc_vat`))       AS `local_discount_exc_vat`,
    (-(1) * ABS(`olv`.`local_store_credit_inc_vat`))   AS `local_store_credit_inc_vat`,
    (-(1) * ABS(`olv`.`local_store_credit_vat`))       AS `local_store_credit_vat`,
    (-(1) * ABS(`olv`.`local_store_credit_exc_vat`))   AS `local_store_credit_exc_vat`,
    `olv`.`local_adjustment_inc_vat`                   AS `local_adjustment_inc_vat`,
    `olv`.`local_adjustment_vat`                       AS `local_adjustment_vat`,
    `olv`.`local_adjustment_exc_vat`                   AS `local_adjustment_exc_vat`,
    `olv`.`local_line_total_inc_vat`                   AS `local_line_total_inc_vat`,
    `olv`.`local_line_total_vat`                       AS `local_line_total_vat`,
    `olv`.`local_line_total_exc_vat`                   AS `local_line_total_exc_vat`,
    `olv`.`local_subtotal_cost`                        AS `local_subtotal_cost`,
    `olv`.`local_shipping_cost`                        AS `local_shipping_cost`,
    `olv`.`local_total_cost`                           AS `local_total_cost`,
    `olv`.`local_margin_amount`                        AS `local_margin_amount`,
    `olv`.`local_margin_percent`                       AS `local_margin_percent`,
    `olv`.`local_to_global_rate`                       AS `local_to_global_rate`,
    `olv`.`order_currency_code`                        AS `order_currency_code`,
    `olv`.`global_prof_fee`                            AS `global_prof_fee`,
    `olv`.`global_price_inc_vat`                       AS `global_price_inc_vat`,
    `olv`.`global_price_vat`                           AS `global_price_vat`,
    `olv`.`global_price_exc_vat`                       AS `global_price_exc_vat`,
    `olv`.`global_line_subtotal_inc_vat`               AS `global_line_subtotal_inc_vat`,
    `olv`.`global_line_subtotal_vat`                   AS `global_line_subtotal_vat`,
    `olv`.`global_line_subtotal_exc_vat`               AS `global_line_subtotal_exc_vat`,
    `olv`.`global_shipping_inc_vat`                    AS `global_shipping_inc_vat`,
    `olv`.`global_shipping_vat`                        AS `global_shipping_vat`,
    `olv`.`global_shipping_exc_vat`                    AS `global_shipping_exc_vat`,
    (-(1) * ABS(`olv`.`global_discount_inc_vat`))      AS `global_discount_inc_vat`,
    (-(1) * ABS(`olv`.`global_discount_vat`))          AS `global_discount_vat`,
    (-(1) * ABS(`olv`.`global_discount_exc_vat`))      AS `global_discount_exc_vat`,
    (-(1) * ABS(`olv`.`global_store_credit_inc_vat`))  AS `global_store_credit_inc_vat`,
    (-(1) * ABS(`olv`.`global_store_credit_vat`))      AS `global_store_credit_vat`,
    (-(1) * ABS(`olv`.`global_store_credit_exc_vat`))  AS `global_store_credit_exc_vat`,
    `olv`.`global_adjustment_inc_vat`                  AS `global_adjustment_inc_vat`,
    `olv`.`global_adjustment_vat`                      AS `global_adjustment_vat`,
    `olv`.`global_adjustment_exc_vat`                  AS `global_adjustment_exc_vat`,
    `olv`.`global_line_total_inc_vat`                  AS `global_line_total_inc_vat`,
    `olv`.`global_line_total_vat`                      AS `global_line_total_vat`,
    `olv`.`global_line_total_exc_vat`                  AS `global_line_total_exc_vat`,
    `olv`.`global_subtotal_cost`                       AS `global_subtotal_cost`,
    `olv`.`global_shipping_cost`                       AS `global_shipping_cost`,
    `olv`.`global_total_cost`                          AS `global_total_cost`,
    `olv`.`global_margin_amount`                       AS `global_margin_amount`,
    `olv`.`global_margin_percent`                      AS `global_margin_percent`,
    `olv`.`prof_fee_percent`                           AS `prof_fee_percent`,
    `olv`.`vat_percent_before_prof_fee`                AS `vat_percent_before_prof_fee`,
    `olv`.`vat_percent`                                AS `vat_percent`,
    ABS(`olv`.`discount_percent`)                      AS `discount_percent`,
    GREATEST(`oh`.`dw_synced_at`, `sh`.`dw_synced_at`) AS `dw_synced_at`,
    CAST(CONCAT('SHIPMENT', `sl`.`item_id`) AS CHAR(25)
    CHARSET latin1)                                    as unique_id
  FROM (((((((((((((((((`dw_shipment_lines` `sl`
    JOIN `dw_shipment_headers` `sh` ON ((`sh`.`shipment_id` = `sl`.`shipment_id`)))
	                     JOIN `dw_order_headers` `oh` ON ((`oh`.`order_id` = `sh`.`order_id`)))
	                    JOIN `dw_order_lines` `ol` ON ((`ol`.`item_id` = `sl`.`order_line_id`)))
	                   LEFT JOIN `dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
	                  LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op` ON ((`op`.`parent_id` = `oh`.`order_id`)))
	                 LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill` ON ((`bill`.`entity_id` = `oh`.`billing_address_id`)))
	                LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship` ON ((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
	               JOIN `shipment_lines_vat` `olv` ON ((`olv`.`item_id` = `sl`.`item_id`)))
	              LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp` ON ((`oh`.`reorder_profile_id` = `rp`.`id`)))
	             LEFT JOIN `dw_shipment_calender_dates` `sc` ON ((`sc`.`shipment_id` = `sh`.`shipment_id`)))
	            LEFT JOIN `first_inv` `fi` ON ((`sh`.`order_id` = `fi`.`order_id`)))
	           LEFT JOIN `dw_invoice_headers` `ih` ON ((`ih`.`invoice_id` = `fi`.`invoice_id`)))
	          LEFT JOIN `dw_products` `p` ON ((`p`.`product_id` = `sl`.`product_id`)))
	         LEFT JOIN `edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `sl`.`product_id`)
	                                                             AND (`esi`.`product_code` = `sl`.`sku`))))
	        LEFT JOIN `dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
	       LEFT JOIN `dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)))
	      LEFT JOIN `dw_qty_backordered` `bo` ON ((`bo`.`shipment_item_id` = `sl`.`item_id`)))
SQL;

		$this->runQuery($sql);

		$sql = <<< SQL
CREATE OR REPLACE VIEW vw_shipment_lines_creditmemo AS
			SELECT `cl`.`item_id` AS `line_id`,
	       `sh`.`shipment_id` AS `shipment_id`,
	       `cw`.`store_name` AS `store_name`,
	       `sh`.`shipment_no` AS `shipment_no`,
	       CAST('CREDITMEMO' AS CHAR(25) CHARSET latin1) AS `document_type`,
	       `oh2`.`created_at` AS `document_date`,
	       `oh2`.`updated_at` AS `document_updated_at`,
	       `oh2`.`creditmemo_id` AS `document_id`,
	       `oh2`.`created_at` AS `shipment_date`,
	       `oh2`.`updated_at` AS `updated_at`,
	       `cl`.`product_id` AS `product_id`,
	       `cf`.`product_type` AS `product_type`,
	       CAST(`ol`.`product_options` AS CHAR(8000) CHARSET latin1) AS `product_options`,
	       `p`.`is_lens` AS `is_lens`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `lens_eye`,
	       `esi`.`BC` AS `lens_base_curve`,
	       `esi`.`DI` AS `lens_diameter`,
	       `esi`.`PO` AS `lens_power`,
	       `esi`.`CY` AS `lens_cylinder`,
	       `esi`.`AX` AS `lens_axis`,
	       `esi`.`AD` AS `lens_addition`,
	       `esi`.`DO` AS `lens_dominance`,
	       `esi`.`CO` AS `lens_colour`,
	       `p`.`daysperlens` AS `lens_days`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_size`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `product_colour`,
	       (-(1) * `ol`.`weight`) AS `unit_weight`,
	       ((-(1) * `ol`.`weight`) * `cl`.`qty`) AS `line_weight`,
	       `ol`.`is_virtual` AS `is_virtual`,
	       `cl`.`sku` AS `sku`,
	       `cl`.`name` AS `name`,
	       `pcf`.`category_id` AS `category_id`,
	       (CASE
	            WHEN (`oh`.`base_shipping_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `free_shipping`,
	       (CASE
	            WHEN (`oh`.`base_discount_amount` > 0) THEN 1
	            ELSE 0
	        END) AS `no_discount`,
	       (-(1) * `cl`.`qty`) AS `qty`,
	       (-(1) * `bo`.`qty_prior`) AS `qty_backordered`,
	       (-(1) * `olv`.`local_prof_fee`) AS `local_prof_fee`,
	       `olv`.`local_price_inc_vat` AS `local_price_inc_vat`,
	       `olv`.`local_price_vat` AS `local_price_vat`,
	       `olv`.`local_price_exc_vat` AS `local_price_exc_vat`,
	       (-(1) * `olv`.`local_line_subtotal_inc_vat`) AS `local_line_subtotal_inc_vat`,
	       (-(1) * `olv`.`local_line_subtotal_vat`) AS `local_line_subtotal_vat`,
	       (-(1) * `olv`.`local_line_subtotal_exc_vat`) AS `local_line_subtotal_exc_vat`,
	       (-(1) * `olv`.`local_shipping_inc_vat`) AS `local_shipping_inc_vat`,
	       (-(1) * `olv`.`local_shipping_vat`) AS `local_shipping_vat`,
	       (-(1) * `olv`.`local_shipping_exc_vat`) AS `local_shipping_exc_vat`,
	       ABS(`olv`.`local_discount_inc_vat`) AS `local_discount_inc_vat`,
	       ABS(`olv`.`local_discount_vat`) AS `local_discount_vat`,
	       ABS(`olv`.`local_discount_exc_vat`) AS `local_discount_exc_vat`,
	       ABS(`olv`.`local_store_credit_inc_vat`) AS `local_store_credit_inc_vat`,
	       ABS(`olv`.`local_store_credit_vat`) AS `local_store_credit_vat`,
	       ABS(`olv`.`local_store_credit_exc_vat`) AS `local_store_credit_exc_vat`,
	       (-(1) * `olv`.`local_adjustment_inc_vat`) AS `local_adjustment_inc_vat`,
	       (-(1) * `olv`.`local_adjustment_vat`) AS `local_adjustment_vat`,
	       (-(1) * `olv`.`local_adjustment_exc_vat`) AS `local_adjustment_exc_vat`,
	       (-(1) * `olv`.`local_line_total_inc_vat`) AS `local_line_total_inc_vat`,
	       (-(1) * `olv`.`local_line_total_vat`) AS `local_line_total_vat`,
	       (-(1) * `olv`.`local_line_total_exc_vat`) AS `local_line_total_exc_vat`,
	       (-(1) * `olv`.`local_subtotal_cost`) AS `local_subtotal_cost`,
	       (-(1) * `olv`.`local_shipping_cost`) AS `local_shipping_cost`,
	       (-(1) * `olv`.`local_total_cost`) AS `local_total_cost`,
	       (-(1) * `olv`.`local_margin_amount`) AS `local_margin_amount`,
	       `olv`.`local_margin_percent` AS `local_margin_percent`,
	       `olv`.`local_to_global_rate` AS `local_to_global_rate`,
	       `olv`.`order_currency_code` AS `order_currency_code`,
	       (-(1) * `olv`.`global_prof_fee`) AS `global_prof_fee`,
	       `olv`.`global_price_inc_vat` AS `global_price_inc_vat`,
	       `olv`.`global_price_vat` AS `global_price_vat`,
	       `olv`.`global_price_exc_vat` AS `global_price_exc_vat`,
	       (-(1) * `olv`.`global_line_subtotal_inc_vat`) AS `global_line_subtotal_inc_vat`,
	       (-(1) * `olv`.`global_line_subtotal_vat`) AS `global_line_subtotal_vat`,
	       (-(1) * `olv`.`global_line_subtotal_exc_vat`) AS `global_line_subtotal_exc_vat`,
	       (-(1) * `olv`.`global_shipping_inc_vat`) AS `global_shipping_inc_vat`,
	       (-(1) * `olv`.`global_shipping_vat`) AS `global_shipping_vat`,
	       (-(1) * `olv`.`global_shipping_exc_vat`) AS `global_shipping_exc_vat`,
	       ABS(`olv`.`global_discount_inc_vat`) AS `global_discount_inc_vat`,
	       ABS(`olv`.`global_discount_vat`) AS `global_discount_vat`,
	       ABS(`olv`.`global_discount_exc_vat`) AS `global_discount_exc_vat`,
	       ABS(`olv`.`global_store_credit_inc_vat`) AS `global_store_credit_inc_vat`,
	       ABS(`olv`.`global_store_credit_vat`) AS `global_store_credit_vat`,
	       ABS(`olv`.`global_store_credit_exc_vat`) AS `global_store_credit_exc_vat`,
	       (-(1) * `olv`.`global_adjustment_inc_vat`) AS `global_adjustment_inc_vat`,
	       (-(1) * `olv`.`global_adjustment_vat`) AS `global_adjustment_vat`,
	       (-(1) * `olv`.`global_adjustment_exc_vat`) AS `global_adjustment_exc_vat`,
	       (-(1) * `olv`.`global_line_total_inc_vat`) AS `global_line_total_inc_vat`,
	       (-(1) * `olv`.`global_line_total_vat`) AS `global_line_total_vat`,
	       (-(1) * `olv`.`global_line_total_exc_vat`) AS `global_line_total_exc_vat`,
	       (-(1) * `olv`.`global_subtotal_cost`) AS `global_subtotal_cost`,
	       (-(1) * `olv`.`global_shipping_cost`) AS `global_shipping_cost`,
	       (-(1) * `olv`.`global_total_cost`) AS `global_total_cost`,
	       (-(1) * `olv`.`global_margin_amount`) AS `global_margin_amount`,
	       `olv`.`global_margin_percent` AS `global_margin_percent`,
	       `olv`.`prof_fee_percent` AS `prof_fee_percent`,
	       `olv`.`vat_percent_before_prof_fee` AS `vat_percent_before_prof_fee`,
	       `olv`.`vat_percent` AS `vat_percent`,
	       ABS(`olv`.`discount_percent`) AS `discount_percent`,
	       GREATEST(`ol`.`dw_synced_at`,`cl`.`dw_synced_at`,`oh`.`dw_synced_at`,`oh2`.`dw_synced_at`) AS `dw_synced_at` ,
	       CAST(CONCAT('CREDITMEMO',`cl`.`item_id`) AS CHAR(25) CHARSET latin1) as unique_id
	FROM ((((((((((((((((((`dw_creditmemo_lines` `cl`
	                       JOIN `dw_creditmemo_headers` `oh2` ON ((`oh2`.`creditmemo_id` = `cl`.`creditmemo_id`)))
	                      JOIN `dw_order_headers` `oh` ON ((`oh2`.`order_id` = `oh`.`order_id`)))
	                     JOIN `dw_order_lines` `ol` ON ((`ol`.`item_id` = `cl`.`order_line_id`)))
	                    LEFT JOIN `dw_stores` `cw` ON ((`cw`.`store_id` = `oh`.`store_id`)))
	                   LEFT JOIN `{$this->dbname}`.`sales_flat_order_payment` `op` ON ((`op`.`parent_id` = `oh`.`order_id`)))
	                  LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `bill` ON ((`bill`.`entity_id` = `oh`.`billing_address_id`)))
	                 LEFT JOIN `{$this->dbname}`.`sales_flat_order_address` `ship` ON ((`ship`.`entity_id` = `oh`.`shipping_address_id`)))
	                JOIN `creditmemo_shipment_lines_vat` `olv` ON ((`olv`.`item_id` = `cl`.`item_id`)))
	               LEFT JOIN `{$this->dbname}`.`po_reorder_profile` `rp` ON ((`oh`.`reorder_profile_id` = `rp`.`id`)))
	              LEFT JOIN `first_inv` `fi` ON ((`oh`.`order_id` = `fi`.`order_id`)))
	             LEFT JOIN `dw_invoice_headers` `ih` ON ((`ih`.`invoice_id` = `fi`.`invoice_id`)))
	            LEFT JOIN `dw_products` `p` ON ((`p`.`product_id` = `cl`.`product_id`)))
	           LEFT JOIN `edi_stock_item_agg` `esi` ON (((`esi`.`product_id` = `cl`.`product_id`)
	                                                               AND (`esi`.`product_code` = `cl`.`sku`))))
	          LEFT JOIN `dw_product_category_flat` `pcf` ON ((`pcf`.`product_id` = `ol`.`product_id`)))
	         LEFT JOIN `dw_flat_category` `cf` ON ((`cf`.`category_id` = `pcf`.`category_id`)))
	        LEFT JOIN `dw_shipment_lines` `sl` ON ((`sl`.`item_id` = `olv`.`shipment_item_id`)))
	       LEFT JOIN `dw_shipment_headers` `sh` ON ((`sl`.`shipment_id` = `sh`.`shipment_id`)))
	      LEFT JOIN `dw_qty_backordered` `bo` ON ((`bo`.`shipment_item_id` = `sl`.`item_id`)));

		CREATE OR REPLACE VIEW vw_customers AS
		SELECT `c`.`customer_id` AS `customer_id`,
	       `c`.`email` AS `email`,
	       `ds`.`store_name` AS `store_name`,
	       `c`.`created_at` AS `created_at`,
	       `c`.`updated_at` AS `updated_at`,
	       `ds`.`website_group` AS `website_group`,
	       COALESCE(`cim`.`new_customer_created_in`,`ds2`.`store_name`,`ds`.`store_name`) AS `created_in`,
	       `c`.`prefix` AS `prefix`,
	       `c`.`firstname` AS `firstname`,
	       `c`.`middlename` AS `middlename`,
	       `c`.`lastname` AS `lastname`,
	       `c`.`suffix` AS `suffix`,
	       `c`.`taxvat` AS `taxvat`,
	       `c`.`postoptics_send_post` AS `postoptics_send_post`,
	       `c`.`facebook_id` AS `facebook_id`,
	       CAST(`c`.`facebook_permissions` AS CHAR(255) CHARSET latin1) AS `facebook_permissions`,
	       CAST('' AS CHAR(1) CHARSET latin1) AS `gender`,
	       `c`.`dob` AS `dob`,
	       `c`.`unsubscribe_all` AS `unsubscribe_all`,
	       `days_worn`.`value` AS `days_worn`,
	       `c`.`parent_customer_id` AS `parent_customer_id`,
	       `c`.`is_parent_customer` AS `is_parent_customer`,
	       `c`.`eyeplan_credit_limit` AS `eyeplan_credit_limit`,
	       `c`.`eyeplan_approved_flag` AS `eyeplan_approved_flag`,
	       `c`.`eyeplan_can_ref_new_customer` AS `eyeplan_can_ref_new_customer`,
	       `c`.`cus_phone` AS `cus_phone`,
	       `c`.`found_us_info` AS `found_us_info`,
	       `c`.`password_hash` AS `password_hash`,
	       `c`.`referafriend_code` AS `referafriend_code`,
	       `c`.`alternate_email` AS `alternate_email`,
	       `c`.`old_access_cust_no` AS `old_access_cust_no`,
	       `c`.`old_web_cust_no` AS `old_web_cust_no`,
	       `c`.`old_customer_id` AS `old_customer_id`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin1`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin2`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin3`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin4`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `emvadmin5`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `language`,
	       `ohma`.`first_order_date` AS `first_order_date`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `last_logged_in_date`,
	       `ohma`.`last_order_date` AS `last_order_date`,
	       `tord`.`no_of_orders` AS `num_of_orders`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `card_expiry_date`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_lifecycle`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_usage`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_geog`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_purch_behaviour`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_eysight`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_sport`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_professional`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_lifestage`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_vanity`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_2`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_3`,
	       CAST(NULL AS CHAR(255) CHARSET latin1) AS `segment_4`,
	       `c`.`default_billing` AS `default_billing`,
	       `c`.`default_shipping` AS `default_shipping`,
	       `ochan`.`channel` AS `business_channel`,
	       `c`.`website_id` AS `website_id`,
	       `c`.`dw_synced_at` ,
	       `c`.customer_id  as unique_id
	FROM (((((((((`dw_customers` `c`
	              LEFT JOIN `dw_stores` `ds` ON ((`ds`.`store_id` = `c`.`store_id`)))
	             LEFT JOIN `{$this->dbname}`.`eav_attribute_option_value` `days_worn` ON (((`days_worn`.`option_id` = `c`.`days_worn_info`)
	                                                                                 AND (`days_worn`.`store_id` = 0))))
	            LEFT JOIN `customer_created_in_map` `cim` ON ((`cim`.`customer_created_in` = `c`.`created_in`)))
	           LEFT JOIN `customer_first_order` `fo` ON ((`fo`.`customer_id` = `c`.`customer_id`)))
	          LEFT JOIN `dw_order_headers` `oh` ON ((`oh`.`order_id` = `fo`.`order_id`)))
	         LEFT JOIN `dw_stores` `ds2` ON ((`oh`.`store_id` = `ds2`.`store_id`)))
	        LEFT JOIN `dw_order_channel` `ochan` ON ((`ochan`.`order_id` = `oh`.`order_id`)))
	       LEFT JOIN `dw_order_headers_marketing_agg` `ohma` ON ((`ohma`.`customer_id` = `c`.`customer_id`)))
	      LEFT JOIN `dw_customer_total_orders` `tord` ON ((`tord`.`customer_id` = `c`.`customer_id`)))
	WHERE (`c`.`website_group_id` <> 100);



		CREATE OR REPLACE VIEW vw_order_headers AS
        SELECT * FROM `vw_order_headers_order` UNION ALL
        SELECT * FROM `vw_order_headers_creditmemo` UNION ALL
        SELECT * FROM `vw_order_headers_cancel`;

        CREATE OR REPLACE VIEW vw_order_lines AS
        SELECT * FROM `vw_order_lines_creditmemo`UNION ALL
        SELECT * FROM `vw_order_lines_cancel`;


        CREATE OR REPLACE VIEW vw_invoice_headers AS
        SELECT * FROM `vw_invoice_headers_invoice` UNION ALL
        SELECT * FROM `vw_invoice_headers_creditmemo`;

        CREATE OR REPLACE VIEW vw_invoice_lines AS
        SELECT * FROM `vw_invoice_lines_invoice` UNION ALL
        SELECT * FROM `vw_invoice_lines_creditmemo` ;

        CREATE OR REPLACE VIEW vw_shipment_headers AS
        SELECT * FROM `vw_shipment_headers_shipment` UNION ALL
        SELECT * FROM `vw_shipment_headers_creditmemo`;

        CREATE OR REPLACE VIEW vw_shipment_lines AS
        SELECT * FROM `vw_shipment_lines_shipment` UNION ALL
        SELECT * FROM `vw_shipment_lines_creditmemo` ;


CREATE OR REPLACE  VIEW {$this->dbname}.`vw_sales_flat_order_payment` AS
SELECT
  `sales_flat_order_payment`.`entity_id`                     AS `entity_id`,
  `sales_flat_order_payment`.`parent_id`                     AS `parent_id`,
  `sales_flat_order_payment`.`shipping_captured`             AS `shipping_captured`,
  `sales_flat_order_payment`.`amount_refunded`               AS `amount_refunded`,
  `sales_flat_order_payment`.`amount_canceled`               AS `amount_canceled`,
  `sales_flat_order_payment`.`base_amount_authorized`        AS `base_amount_authorized`,
  `sales_flat_order_payment`.`base_amount_paid_online`       AS `base_amount_paid_online`,
  `sales_flat_order_payment`.`base_amount_refunded_online`   AS `base_amount_refunded_online`,
  `sales_flat_order_payment`.`base_shipping_amount`          AS `base_shipping_amount`,
  `sales_flat_order_payment`.`shipping_amount`               AS `shipping_amount`,
  `sales_flat_order_payment`.`amount_paid`                   AS `amount_paid`,
  `sales_flat_order_payment`.`amount_authorized`             AS `amount_authorized`,
  `sales_flat_order_payment`.`base_amount_ordered`           AS `base_amount_ordered`,
  `sales_flat_order_payment`.`base_shipping_refunded`        AS `base_shipping_refunded`,
  `sales_flat_order_payment`.`shipping_refunded`             AS `shipping_refunded`,
  `sales_flat_order_payment`.`base_amount_refunded`          AS `base_amount_refunded`,
  `sales_flat_order_payment`.`amount_ordered`                AS `amount_ordered`,
  `sales_flat_order_payment`.`base_amount_canceled`          AS `base_amount_canceled`,
  `sales_flat_order_payment`.`ideal_transaction_checked`     AS `ideal_transaction_checked`,
  `sales_flat_order_payment`.`quote_payment_id`              AS `quote_payment_id`,
  `sales_flat_order_payment`.`cc_exp_month`                  AS `cc_exp_month`,
  `sales_flat_order_payment`.`cc_ss_start_year`              AS `cc_ss_start_year`,
  `sales_flat_order_payment`.`echeck_bank_name`              AS `echeck_bank_name`,
  `sales_flat_order_payment`.`method`                        AS `method`,
  `sales_flat_order_payment`.`cc_debug_request_body`         AS `cc_debug_request_body`,
  `sales_flat_order_payment`.`cc_secure_verify`              AS `cc_secure_verify`,
  `sales_flat_order_payment`.`cybersource_token`             AS `cybersource_token`,
  `sales_flat_order_payment`.`ideal_issuer_title`            AS `ideal_issuer_title`,
  `sales_flat_order_payment`.`protection_eligibility`        AS `protection_eligibility`,
  `sales_flat_order_payment`.`cc_approval`                   AS `cc_approval`,
  `sales_flat_order_payment`.`cc_last4`                      AS `cc_last4`,
  `sales_flat_order_payment`.`cc_status_description`         AS `cc_status_description`,
  `sales_flat_order_payment`.`echeck_type`                   AS `echeck_type`,
  `sales_flat_order_payment`.`paybox_question_number`        AS `paybox_question_number`,
  `sales_flat_order_payment`.`cc_debug_response_serialized`  AS `cc_debug_response_serialized`,
  `sales_flat_order_payment`.`cc_ss_start_month`             AS `cc_ss_start_month`,
  `sales_flat_order_payment`.`echeck_account_type`           AS `echeck_account_type`,
  `sales_flat_order_payment`.`last_trans_id`                 AS `last_trans_id`,
  `sales_flat_order_payment`.`cc_cid_status`                 AS `cc_cid_status`,
  `sales_flat_order_payment`.`cc_type`                       AS `cc_type`,
  `sales_flat_order_payment`.`ideal_issuer_id`               AS `ideal_issuer_id`,
  `sales_flat_order_payment`.`po_number`                     AS `po_number`,
  `sales_flat_order_payment`.`cc_exp_year`                   AS `cc_exp_year`,
  `sales_flat_order_payment`.`cc_status`                     AS `cc_status`,
  `sales_flat_order_payment`.`echeck_routing_number`         AS `echeck_routing_number`,
  `sales_flat_order_payment`.`account_status`                AS `account_status`,
  `sales_flat_order_payment`.`anet_trans_method`             AS `anet_trans_method`,
  `sales_flat_order_payment`.`cc_debug_response_body`        AS `cc_debug_response_body`,
  `sales_flat_order_payment`.`cc_ss_issue`                   AS `cc_ss_issue`,
  `sales_flat_order_payment`.`echeck_account_name`           AS `echeck_account_name`,
  `sales_flat_order_payment`.`cc_avs_status`                 AS `cc_avs_status`,
  `sales_flat_order_payment`.`cc_number_enc`                 AS `cc_number_enc`,
  `sales_flat_order_payment`.`cc_trans_id`                   AS `cc_trans_id`,
  `sales_flat_order_payment`.`flo2cash_account_id`           AS `flo2cash_account_id`,
  `sales_flat_order_payment`.`paybox_request_number`         AS `paybox_request_number`,
  `sales_flat_order_payment`.`address_status`                AS `address_status`,
  `sales_flat_order_payment`.`cybersource_cc_stored`         AS `cybersource_cc_stored`,
  `sales_flat_order_payment`.`cybersource_last_authorize`    AS `cybersource_last_authorize`,
  `sales_flat_order_payment`.`cybersource_last_authorize_id` AS `cybersource_last_authorize_id`,
  `sales_flat_order_payment`.`cybersource_last_capture`      AS `cybersource_last_capture`,
  `sales_flat_order_payment`.`cybersource_last_capture_id`   AS `cybersource_last_capture_id`,
  `sales_flat_order_payment`.`cybersource_last_refund`       AS `cybersource_last_refund`,
  `sales_flat_order_payment`.`cybersource_last_refund_id`    AS `cybersource_last_refund_id`,
  `sales_flat_order_payment`.`cybersource_last_void`         AS `cybersource_last_void`,
  `sales_flat_order_payment`.`cybersource_last_void_id`      AS `cybersource_last_void_id`,
  `sales_flat_order_payment`.`cybersource_stored_id`         AS `cybersource_stored_id`,
  `sales_flat_order_payment`.`cybersource_stored_use`        AS `cybersource_stored_use`,
  `sales_flat_order_payment`.`internetkassa_issuer_pm`       AS `internetkassa_issuer_pm`,
  `sales_flat_order_payment`.`internetkassa_issuer_brand`    AS `internetkassa_issuer_brand`,
  `sales_flat_order_payment`.`internetkassa_issuer_ideal`    AS `internetkassa_issuer_ideal`
FROM {$this->dbname}.`sales_flat_order_payment`;

CREATE OR REPLACE  VIEW {$this->dbname}.`vw_sales_flat_order_payment` AS
SELECT
  `sales_flat_order_payment`.`entity_id`                     AS `entity_id`,
  `sales_flat_order_payment`.`parent_id`                     AS `parent_id`,
  `sales_flat_order_payment`.`shipping_captured`             AS `shipping_captured`,
  `sales_flat_order_payment`.`amount_refunded`               AS `amount_refunded`,
  `sales_flat_order_payment`.`amount_canceled`               AS `amount_canceled`,
  `sales_flat_order_payment`.`base_amount_authorized`        AS `base_amount_authorized`,
  `sales_flat_order_payment`.`base_amount_paid_online`       AS `base_amount_paid_online`,
  `sales_flat_order_payment`.`base_amount_refunded_online`   AS `base_amount_refunded_online`,
  `sales_flat_order_payment`.`base_shipping_amount`          AS `base_shipping_amount`,
  `sales_flat_order_payment`.`shipping_amount`               AS `shipping_amount`,
  `sales_flat_order_payment`.`amount_paid`                   AS `amount_paid`,
  `sales_flat_order_payment`.`amount_authorized`             AS `amount_authorized`,
  `sales_flat_order_payment`.`base_amount_ordered`           AS `base_amount_ordered`,
  `sales_flat_order_payment`.`base_shipping_refunded`        AS `base_shipping_refunded`,
  `sales_flat_order_payment`.`shipping_refunded`             AS `shipping_refunded`,
  `sales_flat_order_payment`.`base_amount_refunded`          AS `base_amount_refunded`,
  `sales_flat_order_payment`.`amount_ordered`                AS `amount_ordered`,
  `sales_flat_order_payment`.`base_amount_canceled`          AS `base_amount_canceled`,
  `sales_flat_order_payment`.`ideal_transaction_checked`     AS `ideal_transaction_checked`,
  `sales_flat_order_payment`.`quote_payment_id`              AS `quote_payment_id`,
  `sales_flat_order_payment`.`cc_exp_month`                  AS `cc_exp_month`,
  `sales_flat_order_payment`.`cc_ss_start_year`              AS `cc_ss_start_year`,
  `sales_flat_order_payment`.`echeck_bank_name`              AS `echeck_bank_name`,
  `sales_flat_order_payment`.`method`                        AS `method`,
  `sales_flat_order_payment`.`cc_debug_request_body`         AS `cc_debug_request_body`,
  `sales_flat_order_payment`.`cc_secure_verify`              AS `cc_secure_verify`,
  `sales_flat_order_payment`.`cybersource_token`             AS `cybersource_token`,
  `sales_flat_order_payment`.`ideal_issuer_title`            AS `ideal_issuer_title`,
  `sales_flat_order_payment`.`protection_eligibility`        AS `protection_eligibility`,
  `sales_flat_order_payment`.`cc_approval`                   AS `cc_approval`,
  `sales_flat_order_payment`.`cc_last4`                      AS `cc_last4`,
  `sales_flat_order_payment`.`cc_status_description`         AS `cc_status_description`,
  `sales_flat_order_payment`.`echeck_type`                   AS `echeck_type`,
  `sales_flat_order_payment`.`paybox_question_number`        AS `paybox_question_number`,
  `sales_flat_order_payment`.`cc_debug_response_serialized`  AS `cc_debug_response_serialized`,
  `sales_flat_order_payment`.`cc_ss_start_month`             AS `cc_ss_start_month`,
  `sales_flat_order_payment`.`echeck_account_type`           AS `echeck_account_type`,
  `sales_flat_order_payment`.`last_trans_id`                 AS `last_trans_id`,
  `sales_flat_order_payment`.`cc_cid_status`                 AS `cc_cid_status`,
  `sales_flat_order_payment`.`cc_type`                       AS `cc_type`,
  `sales_flat_order_payment`.`ideal_issuer_id`               AS `ideal_issuer_id`,
  `sales_flat_order_payment`.`po_number`                     AS `po_number`,
  `sales_flat_order_payment`.`cc_exp_year`                   AS `cc_exp_year`,
  `sales_flat_order_payment`.`cc_status`                     AS `cc_status`,
  `sales_flat_order_payment`.`echeck_routing_number`         AS `echeck_routing_number`,
  `sales_flat_order_payment`.`account_status`                AS `account_status`,
  `sales_flat_order_payment`.`anet_trans_method`             AS `anet_trans_method`,
  `sales_flat_order_payment`.`cc_debug_response_body`        AS `cc_debug_response_body`,
  `sales_flat_order_payment`.`cc_ss_issue`                   AS `cc_ss_issue`,
  `sales_flat_order_payment`.`echeck_account_name`           AS `echeck_account_name`,
  `sales_flat_order_payment`.`cc_avs_status`                 AS `cc_avs_status`,
  `sales_flat_order_payment`.`cc_number_enc`                 AS `cc_number_enc`,
  `sales_flat_order_payment`.`cc_trans_id`                   AS `cc_trans_id`,
  `sales_flat_order_payment`.`flo2cash_account_id`           AS `flo2cash_account_id`,
  `sales_flat_order_payment`.`paybox_request_number`         AS `paybox_request_number`,
  `sales_flat_order_payment`.`address_status`                AS `address_status`,
  `sales_flat_order_payment`.`cybersource_cc_stored`         AS `cybersource_cc_stored`,
  `sales_flat_order_payment`.`cybersource_last_authorize`    AS `cybersource_last_authorize`,
  `sales_flat_order_payment`.`cybersource_last_authorize_id` AS `cybersource_last_authorize_id`,
  `sales_flat_order_payment`.`cybersource_last_capture`      AS `cybersource_last_capture`,
  `sales_flat_order_payment`.`cybersource_last_capture_id`   AS `cybersource_last_capture_id`,
  `sales_flat_order_payment`.`cybersource_last_refund`       AS `cybersource_last_refund`,
  `sales_flat_order_payment`.`cybersource_last_refund_id`    AS `cybersource_last_refund_id`,
  `sales_flat_order_payment`.`cybersource_last_void`         AS `cybersource_last_void`,
  `sales_flat_order_payment`.`cybersource_last_void_id`      AS `cybersource_last_void_id`,
  `sales_flat_order_payment`.`cybersource_stored_id`         AS `cybersource_stored_id`,
  `sales_flat_order_payment`.`cybersource_stored_use`        AS `cybersource_stored_use`,
  `sales_flat_order_payment`.`internetkassa_issuer_pm`       AS `internetkassa_issuer_pm`,
  `sales_flat_order_payment`.`internetkassa_issuer_brand`    AS `internetkassa_issuer_brand`,
  `sales_flat_order_payment`.`internetkassa_issuer_ideal`    AS `internetkassa_issuer_ideal`
FROM {$this->dbname}.`sales_flat_order_payment`;

SQL;
		$this->runQuery($sql);

	}

	public function getWriteDb()
	{
		//TODO: this should come from config or something
		return "`phocas`.";
	}

	public function createDebugSummary()
	{
		$this->fname = dirname(__FILE__) . "/../../queries/summary_" . date('Y-m-d-h-i-s') . ".txt";
		/** $this->buildSummaryQuery('vw_catalog_product_website',array(
		 * 'group'=>array('product_id'),
		 * 'dates'=>array(),
		 * 'order'=>array('product_id'),
		 * ));**/

		/** $this->buildSummaryQuery('vw_customers',array(
		 * 'group'=>array('created_at'),
		 * 'dates'=>array('created_at'),
		 * 'order'=>array('created_at'),
		 * ));**/
		$this->buildSummaryQuery('vw_invoice_headers_invoice', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_total_inc_vat', 'global_total_exc_vat'),
		));

		$this->buildSummaryQuery('vw_invoice_headers_creditmemo', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_total_inc_vat', 'global_total_exc_vat'),
		));

		$this->buildSummaryQuery('vw_invoice_lines_invoice', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_line_total_inc_vat', 'global_line_total_exc_vat'),
		));

		$this->buildSummaryQuery('vw_invoice_lines_creditmemo', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_line_total_inc_vat', 'global_line_total_exc_vat'),
		));

		$this->buildSummaryQuery('vw_order_headers_order', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_total_inc_vat', 'global_total_exc_vat'),
		));


		$this->buildSummaryQuery('vw_order_headers_cancel', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_total_inc_vat', 'global_total_exc_vat'),
		));


		$this->buildSummaryQuery('vw_order_headers_creditmemo', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_total_inc_vat', 'global_total_exc_vat'),
		));


		$this->buildSummaryQuery('vw_order_lines_order', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_line_total_inc_vat', 'global_line_total_exc_vat'),
		));


		$this->buildSummaryQuery('vw_order_lines_cancel', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_line_total_inc_vat', 'global_line_total_exc_vat'),
		));


		$this->buildSummaryQuery('vw_order_lines_creditmemo', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_line_total_inc_vat', 'global_line_total_exc_vat'),
		));

		$this->buildSummaryQuery('vw_shipment_headers_shipment', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_total_inc_vat', 'global_total_exc_vat'),
		));


		$this->buildSummaryQuery('vw_shipment_headers_creditmemo', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_total_inc_vat', 'global_total_exc_vat'),
		));

		$this->buildSummaryQuery('vw_shipment_lines_shipment', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_line_total_inc_vat', 'global_line_total_exc_vat'),
		));


		$this->buildSummaryQuery('vw_shipment_lines_creditmemo', array(
			'group' => array('document_date', 'document_type', 'store_name'),
			'dates' => array('document_date'),
			'order' => array('document_date', 'document_type', 'store_name'),
			'cols' => array('global_line_total_inc_vat', 'global_line_total_exc_vat'),
		));

		/**  $this->buildSummaryQuery('vw_product_prices',array(
		 * 'group'=>array('product_id','store_name'),
		 * 'dates'=>array(),
		 * 'order'=>array('product_id','store_name'),
		 * ));**/


	}

	public function buildSummaryQuery($table, $conf)
	{

		//get list of columns
		$write = Mage::getSingleton('core/resource')->getConnection('dwflatreplica_write');
		$currentSql = "desc $table";
		$entityFieldList = $write->fetchAll($currentSql);

		$grouping = $conf['group'];
		$dates = $conf['dates'];
		$order = $conf['order'];
		$retain = (array)$conf['cols'];
		$sql = array(
			'group' => array(),
			'fields' => array(),
			'order' => array(),
		);
		$prorityFields = array();
		foreach ($entityFieldList as $fieldInfo) {
			$field = $fieldInfo['Field'];
			if (array_search($field, $grouping) !== FALSE) {
				if (array_search($field, $dates) !== FALSE) {
					$sql['group'][] = "YEAR($field),MONTH({$field})";
					$prorityFields[] = "YEAR($field) as year ,MONTH({$field}) as month";
				} else {
					$sql['group'][] = $field;
					$prorityFields[] = $field;
				}
			} else {
				//if(strpos($fieldInfo['Type'],'int')=== false && strpos($fieldInfo['Type'],'decimal')=== false) {
				if (array_search($field, $retain) !== FALSE) {
					$sql['fields'][] = "'$field' as $field,SUM($field) as SUM_{$field}";
				} else {
					continue;
				}
				//s}
				// } else {
				//     $sql['fields'][] =  "'$field' as $field,MAX($field) as MAX_{$field},MIN($field) as MIN_{$field},AVG($field) as AVG_{$field},SUM($field) as SUM_{$field}";
				// }
			}
			if (array_search($field, $order) !== FALSE) {
				if (array_search($field, $dates) !== FALSE) {
					$sql['order'][] = "YEAR($field),MONTH({$field})";
				} else {
					$sql['order'][] = $field;
				}
			}
		}
		$sql['fields'] = array_merge($prorityFields, $sql['fields']);
		$tablename = "dw_summ_{$table}";
		$query = "DROP TABLE IF EXISTS dw_summ_{$table};\nCREATE TABLE IF NOT EXISTS $tablename \n ";
		$query .= "SELECT " . implode(",\n", $sql['fields']) . " FROM $table\n";
		if (count($sql['group'])) {
			$query .= " \n GROUP BY " . implode(",", $sql['group']);
		}
		if (count($sql['order'])) {
			$query .= " \n ORDER BY " . implode(",", $sql['order']);
		}
		$query .= ";";

		$accessInfo = Mage::getSingleton('core/resource')->getConnection('dwflatreplica_write')->getConfig();
		$this->runQuery($query);
		$file = $this->fname;
		$fp = fopen($file, 'a+');
		fwrite($fp, $table . "\r\n\r\n");
		$outfile = "mysql -h {$accessInfo['host']} -u {$accessInfo['username']} --password={$accessInfo['password']} -e \"SELECT * FROM {$accessInfo['dbname']}.{$tablename}\" >> $file";
		echo($outfile);
		exec($outfile);
		fclose($fp);

	}

	protected function _construct()
	{
		$this->_init('datawarehouse/build');
		$this->dbname = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');

	}
}
