
-- buildExtraTables

	-- max_reorder_quote_payment

	-- order_status_history_agg

	-- edi_stock_item_agg

	-- first_inv
	-- first_inv_staging
	-- first_ship
	-- first_ship_staging

	-- last_full_ship
	-- last_full_ship_staging
	-- last_full_payment
	-- last_full_payment_staging

	-- dw_category_path
	-- dw_product_category_map
	-- dw_product_category_flat
	-- dw_product_price_qtys

	-- dw_stores

	-- customer_first_order
	-- customer_first_order_staging

	-- Description: 
		-- Write a PHP variable with SQL Code + Run Query (PHP Variable)
		-- SQL Code: DROP + CREATE + REPLACE INTO


	-- dw_category_path
	SELECT d1.category_id,
		d1.name name1, d2.name name2, d3.name name3, d4.name name4,
		d5.name name5, d6.name name6, d7.name name7, d8.name name8
    FROM 
			dw_categories d1 
		LEFT JOIN 
			dw_categories d2 ON d1.parent_category_id = d2.category_id
		LEFT JOIN 
			dw_categories d3 ON d2.parent_category_id = d3.category_id
		LEFT JOIN 
			dw_categories d4 ON d3.parent_category_id = d4.category_id
		LEFT JOIN 
			dw_categories d5 ON d4.parent_category_id = d5.category_id
		LEFT JOIN 
			dw_categories d6 ON d5.parent_category_id = d6.category_id
		LEFT JOIN 
			dw_categories d7 ON d6.parent_category_id = d7.category_id
		LEFT JOIN 
			dw_categories d8 ON d7.parent_category_id = d8.category_id;

	-- dw_product_category_map
	SELECT p.product_id,
		MAX(p.name) name,
		IFNULL(MAX(fc.modality_name), 'NONE') modality_name, IFNULL(MAX(fc1.feature_name), 'NONE') feature_name, IFNULL(MAX(fc2.type_name), 'NONE')    type_name,
		IFNULL(MAX(p.product_type), 'Other') product_type_name,
		IFNULL(MAX(p.telesales_only), 0)      telesales_only
    FROM 
			dw_products p 
		LEFT JOIN 
			{$this->dbname}.catalog_category_product cpc ON p.product_id = cpc.product_id
		LEFT JOIN 
			dw_categories c ON c.category_id = cpc.category_id
		LEFT JOIN 
			dw_category_path cp ON cp.category_id = c.category_id
		LEFT JOIN 
			dw_flat_category fc ON fc.modality_name = cp.name1 OR fc.modality_name = cp.name2 OR fc.modality_name = cp.name3 OR fc.modality_name = cp.name4 OR 
				fc.modality_name = cp.name5 OR fc.modality_name = cp.name6 OR fc.modality_name = cp.name7 OR fc.modality_name = cp.name8
		LEFT JOIN 
			dw_flat_category fc1 ON fc1.feature_name = cp.name1 OR fc1.feature_name = cp.name2 OR fc1.feature_name = cp.name3 OR fc1.feature_name = cp.name4 OR 
				fc1.feature_name = cp.name5 OR fc1.feature_name = cp.name6 OR fc1.feature_name = cp.name7 OR fc1.feature_name = cp.name8
		LEFT JOIN 
			dw_flat_category fc2 ON fc2.type_name = cp.name1 OR fc2.type_name = cp.name2 OR fc2.type_name = cp.name3 OR fc2.type_name = cp.name4 OR 
				fc2.type_name = cp.name5 OR fc2.type_name = cp.name6 OR fc2.type_name = cp.name7 OR fc2.type_name = cp.name8
	GROUP BY p.product_id;

	-- dw_product_category_flat
 	SELECT product_id, MAX(category_id) category_id
	FROM 
		(SELECT product_id, IFNULL(category_id,(SELECT category_id FROM dw_flat_category WHERE product_type = 'Other')) category_id
		FROM    
				dw_product_category_map mp     
			LEFT JOIN  
				dw_flat_category fc ON fc.modality_name = mp.modality_name AND fc.feature_name = mp.feature_name AND fc.type_name = mp.type_name AND 
					fc.product_type_name = mp.product_type_name AND fc.telesales_only = mp.telesales_only) a
	GROUP BY product_id;