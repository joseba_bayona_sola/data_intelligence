
	-- CREATE TABLES statements
		-- {$entity}_vat
		-- {$item_entity}_vat


-- {$entity}_rank
SELECT t.*
FROM 
	(SELECT {$entity_id}, eh.order_id,
		(CASE eh.order_id WHEN @curEventId THEN @curRow := @curRow + 1 ELSE @curRow := 1 AND @curEventId := eh.order_id END) AS rank
	FROM 
			dw_{$entity} eh 
		INNER JOIN 
			(SELECT @curRow := 0, @curEventId := '') r
		INNER JOIN 
			dw_order_headers oh ON eh.order_id = oh.order_id
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = oh.customer_id
	ORDER BY order_id, {$entity_id}) t;


-- {$entity}_vat
	-- Insert (Set country information)
	SELECT oh.{$entity_id}, 
		MAX(c.country_type) country_type, MAX(c.country_code) country_code, 
		MAX(oh.order_id) entity_order_id
	FROM
			dw_{$entity} oh
		INNER JOIN
			dw_order_headers doh ON doh.order_id = oh.order_id
		INNER JOIN
			dw_updated_customers duc ON duc.customer_id = doh.customer_id
		LEFT JOIN
			dw_order_address oa ON oa.address_id  =  oh.shipping_address_id
		INNER JOIN
			dw_countries c ON c.country_code  =  oa.country_id
	GROUP BY oh.{$entity_id};

	-- Update (add aggregated values from lines that will be need for other calcs)
	UPDATE {$entity}_vat ov, 
		(SELECT ol.{$entity_id}, IFNULL(MAX(p.is_lens), 0) is_lens,
			ROUND(MAX(ol.base_row_total), 2) max_row_total, ROUND(MIN(ol.base_row_total), 2) min_row_total,
			IFNULL(SUM(ol2.weight * ol.{$this->itemQtyCol[$item_entity]}), 0)  total_weight,
			IFNULL(SUM(ol.base_cost * ol.{$this->itemQtyCol[$item_entity]}), 0) total_cost,
			IFNULL(SUM(ol.{$this->itemQtyCol[$item_entity]}), 0) total_qty
		FROM
				dw_{$item_entity} ol
			INNER JOIN 
				dw_{$entity} oh ON oh.{$entity_id} = ol.{$entity_id}
			INNER JOIN 
				dw_order_headers doh ON oh.order_id = doh.order_id
			INNER JOIN 
				dw_updated_customers uc ON uc.customer_id = doh.customer_id
			LEFT JOIN
				dw_products p ON p.product_id = ol.product_id
			LEFT JOIN
				dw_order_lines ol2 ON ol2.item_id = ol.{$this->itemIdCol[$item_entity]}
		GROUP BY {$entity_id}) sub
	SET
		ov.has_lens = sub.is_lens,
		ov.max_row_total = sub.max_row_total, ov.min_row_total = sub.min_row_total,
		ov.total_weight = sub.total_weight, 
		ov.total_cost = sub.total_cost,
		ov.total_qty = sub.total_qty
	WHERE ov.{$entity_id} = sub.{$entity_id} AND ov.vat_done = 0;

	-- Update (Calc all inc_vat totals for entity)
	UPDATE {$entity}_vat ov, dw_{$entity} oh, dw_order_headers ord
	SET
		local_subtotal_inc_vat = oh.base_subtotal, 
		local_shipping_inc_vat = oh.base_shipping_amount, local_discount_inc_vat = oh.base_discount_amount,
		local_store_credit_inc_vat = {$this->localStoreCreditIncVat[$entity]}, local_adjustment_inc_vat = {$this->localAdjustmentIncVat[$entity]},
		local_total_inc_vat = {$this->localTotalIncVat[$entity]},
		magento_grand_total = oh.base_grand_total,
		local_subtotal_cost = ov.total_cost * (1/oh.base_to_global_rate), local_shipping_cost = IFNULL(ord.partbill_shippingcost_cost * (1/oh.base_to_global_rate), 0),
		local_total_cost =  (ov.total_cost * (1/oh.base_to_global_rate)) + IFNULL(ord.partbill_shippingcost_cost * (1/oh.base_to_global_rate), 0),
		ov.order_date = ord.created_at
	WHERE oh.{$entity_id} = ov.{$entity_id} AND ord.order_id =  oh.order_id AND vat_done = 0;

-- {$entity}_vat_rollup
	-- Insert (collect rollup value - deal with any part entities - only applied to orders that use a sliding scale professional fee)
	-- WHY
	SELECT ohv1.{$entity_id},
		SUM(ohv2.local_total_inc_vat) total_inc_vat,
		MAX(ohv2.has_lens) has_lens
	FROM
			{$entity}_vat ohv1
		INNER JOIN
			{$entity}_vat ohv2 ON ohv2.entity_order_id = ohv1.entity_order_id AND ohv2.{$entity_id} <= ohv1.{$entity_id}
	WHERE ohv1.vat_done = 0
	GROUP BY ohv1.{$entity_id};

	-- Update 
	UPDATE {$entity}_vat ov, {$entity}_vat_rollup ovr
	SET
		ov.rollup_total = ovr.total_inc_vat,
		ov.rollup_has_lens = ovr.has_lens
	WHERE ov.{$entity_id} = ovr.{$entity_id};

-- {$entity}_vat
	-- Update VAT LOGIC (Flag Rows by VAT logic based on business rules)
	UPDATE{$entity}_vat
	SET vat_logic = 'NON_EU'
	WHERE country_type NOT IN('EU', 'UK') AND country_type IS NOT NULL 
		AND vat_logic IS NULL;

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'PO-2011.01.04-2011-08-31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2011.01.04' AND '2100.01.01' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND oh.store_id = 1;
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'PO-2010.01.01-2011-01-03'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2011.01.03' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND oh.store_id = 1;
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'PO-2008.12.01-2009.12.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2008.12.01' AND '2009.12.31' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND oh.store_id = 1;
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'PO-1900.01.01-2008.11.30'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2008.11.30' 
		AND vat_logic IS NULL AND oh.store_id = 1;

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLUK-2011.01.04-2011.08.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2011.01.04' AND '2100.01.01' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND (oh.store_id IN (2, 4, 8, 3) OR (oh.store_id IN (3, 7) AND ov.country_id != 'IE'));
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLUK-2010.01.01-2011.01.03'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2011.01.03' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND (oh.store_id IN (2, 4, 8, 3) OR (oh.store_id IN (3, 7) AND ov.country_id != 'IE'));
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLUK-2008.12.01-2009.12.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2008.12.01' AND '2009.12.31' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND (oh.store_id IN (2, 4, 8, 3) OR (oh.store_id IN (3, 7) AND ov.country_id != 'IE'));
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLUK-1900.01.01-2008.11.30'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2008.11.30' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND (oh.store_id IN (2, 4, 8, 3) OR (oh.store_id IN (3, 7) AND ov.country_id != 'IE'));

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLIE-2010.01.01-2100.01.01'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2100.01.01' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND oh.store_id  IN (3, 7);
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLIE-2009.01.01-2009.12.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2009.01.01' AND '2009.12.31' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL AND oh.store_id  IN (3, 7);
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLIE-1900.01.01-2008.12.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2008.12.31' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL AND oh.store_id  IN (3, 7);

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLIT-2011.01.04-2100.01.01'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2011.01.04' AND '2100.01.01' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND oh.store_id = 10;
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLIT-2010.01.01-2011.01.03'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2010.01.01' AND '2011.01.03' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND oh.store_id = 10;
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLIT-2008.12.01-2009.12.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2008.12.01' AND '2009.12.31' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND oh.store_id = 10;
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GLIT-1900.01.01-2008.11.30'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2008.11.30' AND cast(ov.order_date as date) <= '2011.08.31' 
		AND vat_logic IS NULL 
		AND oh.store_id = 10;


	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-NL-2012.09.30'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2012.09.30' 
		AND vat_logic IS NULL 
		AND country_id = 'NL' AND oh.store_id NOT IN (11, 19);
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-NL-2012.10.01'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2012.10.01' AND '2100.01.01' 
		AND vat_logic IS NULL 
		AND country_id = 'NL' AND oh.store_id NOT IN (11, 19);

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-BE-2015.10.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2015.10.31' 
		AND vat_logic IS NULL 
		AND country_id = 'BE' AND oh.store_id NOT IN (11,19);
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-BE-CURENT'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2015.11.01' AND '2100.01.01'
		AND vat_logic IS NULL
		AND country_id = 'BE' AND oh.store_id NOT IN (11,19);

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-ES-CURENT'
	WHERE ov.{$entity_id} = oh.{$entity_id}	
		AND cast(ov.order_date as date) BETWEEN '2013.01.01' AND '2100.01.01' 
		AND vat_logic IS NULL
		AND country_id = 'ES' AND oh.store_id NOT IN (11, 19);

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-IT-CURENT'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2013.01.01' AND '2100.01.01' 
		AND vat_logic IS NULL
		AND country_id = 'IT' AND oh.store_id NOT IN (11, 19);

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-IE-2011.10.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '1900.01.01' AND '2011.10.31' 
		AND vat_logic IS NULL
		AND country_id = 'IE' AND oh.store_id NOT IN (11, 19);
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-IE-2011.11.01'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2011.11.01' AND '2011.12.31' 
		AND vat_logic IS NULL
		AND country_id = 'IE' AND oh.store_id NOT IN (11, 19);
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-IE-2012.01.01-2012.10.31'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2012.01.01' and '2012.10.31' 
		AND vat_logic IS NULL
		AND country_id = 'IE' AND oh.store_id NOT IN (11, 19);
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-IE-2012.11.01-CURRENT'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND cast(ov.order_date as date) BETWEEN '2012.11.01' and '2100.01.01' 
		AND vat_logic IS NULL
		AND country_id = 'IE' AND oh.store_id NOT IN (11, 19);

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'GO-GROUP'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND vat_logic IS NULL
		AND oh.store_id NOT IN (11, 19);

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'ASDA'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND vat_rate IS NULL
		AND oh.store_id = 11;

	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET vat_logic = 'EYEPLAN'
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND vat_rate IS NULL
		AND oh.store_id = 19;

-- {$entity}_vat
	-- Update {$entity}_vat (prof_fee, vat_rate values - based on logic, rollup_total) (Set VAT rate professional fee)
	UPDATE {$entity}_vat
	SET 
		prof_fee_percent = 0, prof_fee = 0, calculated_prof_fee = 0,
		vat_percent = 0, vat_rate = 0,
		local_total_vat = 0, 
		local_subtotal_vat  = 0, local_shipping_vat = 0, local_discount_vat = 0, local_store_credit_vat = 0, local_adjustment_vat = 0,
		local_total_vat = 0,
		calculated_vat = 0, calculated_prof_fee = 0
	WHERE vat_logic = 'NON_EU' AND vat_done = 0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = CASE WHEN rollup_total = 0 THEN 0 WHEN rollup_has_lens = 1 THEN 7.5 ELSE 0 END,
		vat_rate = 1.20
	WHERE vat_logic = 'PO-2011.01.04-2011-08-31' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET 
		prof_fee_percent = 0, 
		calculated_prof_fee = CASE WHEN rollup_total = 0 THEN 0 WHEN rollup_has_lens = 1 THEN 7.5 ELSE 0 END,
		vat_rate = 1.175
	WHERE vat_logic = 'PO-2010.01.01-2011-01-03' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET 
		prof_fee_percent = 0,
		calculated_prof_fee = CASE WHEN rollup_total = 0 THEN 0 WHEN rollup_has_lens = 1 THEN 7.5 ELSE 0 END,
		vat_rate = 1.15
	WHERE vat_logic = 'PO-2008.12.01-2009.12.31' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET 
		prof_fee_percent = 0,
		calculated_prof_fee = CASE WHEN rollup_total = 0 THEN 0 WHEN rollup_has_lens = 1 THEN 7.5 ELSE 0 END,
		vat_rate = 1.175
	WHERE vat_logic = 'PO-1900.01.01-2008.11.30' AND vat_done = 0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 
			CASE  
				WHEN rollup_total > 149.99 THEN 65 
				WHEN rollup_total > 69.99 THEN 40 
				WHEN rollup_total > 30 THEN 20
				WHEN rollup_total = 0 THEN 0
				ELSE 0
			END,
		vat_rate = 1.20
	WHERE vat_logic = 'GLUK-2011.01.04-2011.08.31' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 
			CASE
				WHEN rollup_total > 149.99 THEN 65
				WHEN rollup_total > 69.99 THEN 40
				WHEN rollup_total > 30 THEN 20
				WHEN rollup_total = 0 THEN 0
				ELSE 0
			END,
	vat_rate = 1.175
	WHERE vat_logic = 'GLUK-2010.01.01-2011.01.03' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 
			CASE
				WHEN rollup_total > 149.99 THEN 65
				WHEN rollup_total > 69.99 THEN 40
				WHEN rollup_total > 30 THEN 20
				WHEN rollup_total = 0 THEN 0
				ELSE 0
			END,
	vat_rate = 1.15
	WHERE vat_logic = 'GLUK-2008.12.01-2009.12.31' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET 
		prof_fee_percent = 0,
		calculated_prof_fee = 
			CASE
				WHEN rollup_total > 149.99 THEN 65
				WHEN rollup_total > 69.99 THEN 40
				WHEN rollup_total > 30 THEN 20
				WHEN rollup_total = 0 THEN 0
				ELSE 0
			END,
		vat_rate = 1.175
	WHERE vat_logic = 'GLUK-1900.01.01-2008.11.30' AND vat_done = 0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = rollup_total / 2,
		vat_rate = 1.21
	WHERE vat_logic = 'GLIE-2010.01.01-2100.01.01' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = rollup_total / 2,
		vat_rate = 1.215
	WHERE vat_logic = 'GLIE-2009.01.01-2009.12.31' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = rollup_total / 2,
		vat_rate = 1.21
	WHERE vat_logic = 'GLIE-1900.01.01-2008.12.31' AND vat_done = 0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 
			CASE
				WHEN rollup_total > 149.99 THEN 65
				WHEN rollup_total > 69.99 THEN 40
				WHEN rollup_total > 30 THEN 20
				WHEN rollup_total = 0 THEN 0
				ELSE 0
			END,
		vat_rate = 1.20
	WHERE
	vat_logic = 'GLIT-2011.01.04-2100.01.01' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 
			CASE
				WHEN rollup_total > 149.99 THEN 65
				WHEN rollup_total > 69.99 THEN 40
				WHEN rollup_total > 30 THEN 20
				WHEN rollup_total = 0 THEN 0
				ELSE 0
			END,
		vat_rate = 1.175
	WHERE vat_logic = 'GLIT-2010.01.01-2011.01.03' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 
			CASE
				WHEN rollup_total > 149.99 THEN 65
				WHEN rollup_total > 69.99 THEN 40
				WHEN rollup_total > 30 THEN 20
				WHEN rollup_total = 0 THEN 0
				ELSE 0
			END,
		vat_rate = 1.15
	WHERE vat_logic = 'GLIT-2008.12.01-2009.12.31' AND vat_done = 0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 
			CASE
				WHEN rollup_total > 149.99 THEN 65
				WHEN rollup_total > 69.99 THEN 40
				WHEN rollup_total > 30 THEN 20
				WHEN rollup_total = 0 THEN 0
				ELSE 0
			END,
		vat_rate = 1.175
	WHERE vat_logic = 'GLIT-1900.01.01-2008.11.30' AND vat_done = 0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.19
	WHERE vat_logic = 'GO-NL-2012.09.30' AND vat_done =0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.21
	WHERE vat_logic = 'GO-NL-2012.10.01' AND vat_done =0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.20
	WHERE vat_logic = 'GO-BE-2015.10.31' AND vat_done =0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.21
	WHERE vat_logic = 'GO-BE-CURENT' AND vat_done =0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.10
	WHERE vat_logic = 'GO-ES-CURENT' AND vat_done =0;

	UPDATE {$entity}_vat ov
    SET
		prof_fee_percent = 0.18 ,
        calculated_prof_fee = rollup_total * 0.18,
        vat_rate = 1.04
	WHERE vat_logic = 'GO-IT-CURENT' AND vat_done =0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18 ,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.21
	WHERE vat_logic = 'GO-IE-2011.10.31' AND vat_done =0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18 ,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.21 / 0.5263
	WHERE vat_logic = 'GO-IE-2011.11.01' AND vat_done =0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18 ,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.23 / 0.5263
	WHERE vat_logic = 'GO-IE-2012.01.01-2012.10.31' AND vat_done =0;
	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.5 ,
		calculated_prof_fee = rollup_total * 0.5,
		vat_rate = 1.23
	WHERE vat_logic = 'GO-IE-2012.11.01-CURRENT' AND vat_done =0;

	UPDATE {$entity}_vat ov
	SET
		prof_fee_percent = 0.18 ,
		calculated_prof_fee = rollup_total * 0.18,
		vat_rate = 1.20
	WHERE vat_logic = 'GO-GROUP' AND vat_done =0;

	UPDATE {$entity}_vat
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 0
	WHERE vat_logic = 'ASDA' AND vat_done =0;
	UPDATE {$entity}_vat ov
	SET
		vat_rate = 1.072
	WHERE vat_logic = 'ASDA' AND vat_done =0;

	UPDATE {$entity}_vat
	SET
		prof_fee_percent = 0,
		calculated_prof_fee = 0
	WHERE vat_logic = 'EYEPLAN' AND vat_done =0;
	UPDATE {$entity}_vat ov
	SET 
		vat_rate = 1.21
	WHERE vat_logic = 'EYEPLAN' AND vat_done =0;

	-- Update {$entity}_vat (calculated_vat)
		-- Calculate VAT with formula: calculated_vat = (total-calculated_prof_fee) - ((total-calculated_prof_fee) / vat_rate)
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET
		calculated_vat = ((rollup_total-calculated_prof_fee) * 0.5263) - ((rollup_total-calculated_prof_fee) / vat_rate)
	WHERE ov.{$entity_id} = oh.{$entity_id} 
		AND vat_logic IN ('GO-IE-2011.11.01','GO-IE-2012.01.01-2012.10.31') AND vat_done =0;

	UPDATE {$entity}_vat ov
	SET
		calculated_vat = (rollup_total-calculated_prof_fee) - ((rollup_total-calculated_prof_fee) / vat_rate)
	WHERE vat_logic NOT IN ('GO-IE-2011.11.01','GO-IE-2012.01.01-2012.10.31') AND vat_logic != 'NON_EU' AND vat_done =0;

	UPDATE {$entity}_vat 
	SET local_total_vat = calculated_vat 
	WHERE vat_done = 0;
	UPDATE {$entity}_vat 
	SET prof_fee = calculated_prof_fee 
	WHERE vat_done = 0;

		-----> Invoices (handle entities with part documents ie credit / invoice)
		UPDATE
			{$entity}_rank ch1, {$entity}_vat chv,
			{$entity}_rank ch2, {$entity}_vat chv2
		SET
			chv2.local_total_vat = IFNULL(chv2.calculated_vat - IFNULL(chv.calculated_vat,0),0),
			chv2.prof_fee = IFNULL(chv2.calculated_prof_fee - IFNULL(chv.calculated_prof_fee,0),0)
		WHERE chv.{$entity_id} = ch1.{$entity_id} AND ch2.order_id = ch1.order_id 
			AND ch1.rank + 1 = ch2.rank 
			AND  ch2.{$entity_id} = chv2.{$entity_id} 
			AND chv2.vat_done = 0;

	UPDATE {$entity}_vat ov
	SET
		vat_rate = 0 ,
		prof_fee_percent = 0,
		calculated_prof_fee = 0,
		local_total_vat = local_total_inc_vat,
		prof_fee = 0
	WHERE vat_logic IS NULL;	

	-- Update {$entity}_vat (vat values) - Calculate VAT
	UPDATE {$entity}_vat ov
	SET
		local_subtotal_vat = IFNULL(CASE WHEN local_total_inc_vat = 0 OR local_subtotal_inc_vat = 0 THEN 0 ELSE ((local_subtotal_inc_vat / local_total_inc_vat ) * local_total_vat) END, 0),
		local_shipping_vat = IFNULL(CASE WHEN local_total_inc_vat = 0 OR local_shipping_inc_vat = 0 THEN 0 ELSE  ((local_shipping_inc_vat / local_total_inc_vat ) * local_total_vat) END, 0),
		local_discount_vat = IFNULL(CASE WHEN local_discount_inc_vat = 0 OR local_discount_inc_vat = 0 THEN 0 ELSE  (-ABS((ABS(local_discount_inc_vat) / local_total_inc_vat ) * local_total_vat)) END, 0),
		local_store_credit_vat = IFNULL(CASE WHEN local_store_credit_vat = 0 OR local_store_credit_inc_vat = 0 THEN 0 ELSE ((local_store_credit_inc_vat / local_total_inc_vat ) * local_total_vat) END, 0),
		local_adjustment_vat = IFNULL(CASE WHEN local_total_inc_vat = 0  OR local_adjustment_inc_vat = 0 THEN 0 ELSE  ((local_adjustment_inc_vat / local_total_inc_vat ) * local_total_vat) END, 0)
	WHERE vat_done = 0;

	-- Update {$entity}_vat (excvat values) -- Calculate Exc VAT
	UPDATE {$entity}_vat ov
	SET
		local_subtotal_exc_vat = local_subtotal_inc_vat - local_subtotal_vat,
		local_total_exc_vat = local_total_inc_vat - local_total_vat,
		local_shipping_exc_vat = local_shipping_inc_vat - local_shipping_vat,
		local_discount_exc_vat = -ABS(ABS(local_discount_inc_vat) - ABS(local_discount_vat)),
		local_store_credit_exc_vat = local_store_credit_inc_vat - local_store_credit_vat,
		local_adjustment_exc_vat = local_adjustment_inc_vat - local_adjustment_vat
	WHERE vat_done = 0;

	-- Update {$entity}_vat (margin, cost values)
	UPDATE {$entity}_vat ov
	SET
		vat_percent = (local_total_vat) / (local_total_exc_vat),
		vat_percent_before_prof_fee = vat_rate - 1 ,
		discount_percent = -1 * (ABS(local_discount_exc_vat) / (local_total_exc_vat  + ABS(local_discount_exc_vat))),
		local_margin_amount = local_total_exc_vat - local_total_cost  ,
		local_margin_percent = (local_total_exc_vat - local_total_cost) / local_total_exc_vat
	WHERE vat_done = 0;

	-- Update {$entity}_vat (global values) -- Calculate global values
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET
		global_subtotal_inc_vat = local_subtotal_inc_vat * oh.base_to_global_rate, global_subtotal_vat  = local_subtotal_vat * oh.base_to_global_rate, global_subtotal_exc_vat  = local_subtotal_exc_vat * oh.base_to_global_rate,
		global_shipping_inc_vat  = local_shipping_inc_vat * oh.base_to_global_rate, global_shipping_vat  = local_shipping_vat * oh.base_to_global_rate, global_shipping_exc_vat  = local_shipping_exc_vat * oh.base_to_global_rate,
		global_discount_inc_vat  = local_discount_inc_vat * oh.base_to_global_rate, global_discount_vat  = local_discount_vat * oh.base_to_global_rate, global_discount_exc_vat  = local_discount_exc_vat * oh.base_to_global_rate,
		global_store_credit_inc_vat  = local_store_credit_inc_vat * oh.base_to_global_rate, global_store_credit_vat  = local_store_credit_vat * oh.base_to_global_rate, global_store_credit_exc_vat  = local_store_credit_exc_vat * oh.base_to_global_rate,
		global_adjustment_inc_vat  = local_adjustment_inc_vat * oh.base_to_global_rate, global_adjustment_vat  = local_adjustment_vat * oh.base_to_global_rate, global_adjustment_exc_vat  = local_adjustment_exc_vat * oh.base_to_global_rate,
		global_total_inc_vat  = local_total_inc_vat * oh.base_to_global_rate, global_total_vat  = local_total_vat * oh.base_to_global_rate, global_total_exc_vat = local_total_exc_vat * oh.base_to_global_rate,
		global_prof_fee = prof_fee * oh.base_to_global_rate,
		global_subtotal_cost = local_subtotal_cost * oh.base_to_global_rate, global_shipping_cost = local_shipping_cost * oh.base_to_global_rate,
		global_total_cost = local_total_cost * oh.base_to_global_rate,
		global_margin_amount = local_margin_amount * oh.base_to_global_rate
	WHERE ov.{$entity_id} = oh.{$entity_id}
		AND vat_done = 0;
	
	UPDATE {$entity}_vat ov, dw_{$entity} oh
	SET
		global_margin_percent =  global_margin_amount / global_total_inc_vat
	WHERE ov.{$entity_id} = oh.{$entity_id}
		AND vat_done = 0;

	-- Flat Updated Records
	UPDATE {$entity}_vat SET vat_done = 1 WHERE vat_done = 0;

-- {$item_entity} -- Allocate calculated values to entity items based on formula to spread values across items
	-- Insert 
	SELECT oh.item_id,
		{$this->localPriceIncVat[$item_entity]},
		{$this->rowTotalIncVat[$item_entity]},
		{$this->actualCost[$item_entity]},
		oh.{$this->itemQtyCol[$item_entity]}
	FROM 
			dw_{$item_entity} oh
		INNER JOIN 
			dw_{$entity} eh ON oh.{$entity_id} = eh.{$entity_id}
		INNER JOIN 
			dw_order_headers doh ON eh.order_id = doh.order_id
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = doh.customer_id {$this->orderItemJoin[$item_entity]};

-- {$item_entity}_allocation_rate - Set Allocation Rate per Order
	-- Insert 
	SELECT ol.item_id, ov.{$entity_id},
		CASE 
			WHEN max_row_total = 0 AND min_row_total = 0 AND ROUND(local_subtotal_inc_vat,2) = 0 THEN ol.{$this->itemQtyCol[$item_entity]}/ ov.total_qty
            WHEN ol.sku = 'ADJUSTMENT' THEN 1
            ELSE ol.base_row_total / ov.local_subtotal_inc_vat
		END AS rate
	FROM	
			{$entity}_vat ov
        INNER JOIN 
			dw_{$item_entity} ol ON ol.{$entity_id} = ov.{$entity_id}
		INNER JOIN 
			{$item_entity}_vat lv ON lv.item_id = ol.item_id
	WHERE lv.vat_done = 0;

	-- Update Lines VAT values (Inc, Vat, Exc) based on Allocation Rate and Order Header Value
	UPDATE 
		{$item_entity}_vat olv, dw_{$item_entity} ol, {$entity}_vat ov, {$item_entity}_allocation_rate ar
	SET
		olv.local_prof_fee = (ov.prof_fee *  ar.rate),
		olv.local_line_subtotal_vat = (ov.local_subtotal_vat *  ar.rate), olv.local_line_subtotal_inc_vat = (ov.local_subtotal_inc_vat *  ar.rate), olv.local_line_subtotal_exc_vat = (ov.local_subtotal_exc_vat *  ar.rate),
		olv.local_shipping_inc_vat = (ov.local_shipping_inc_vat  *  ar.rate), olv.local_shipping_exc_vat = (ov.local_shipping_exc_vat *  ar.rate), olv.local_shipping_vat = (ov.local_shipping_vat *  ar.rate),
		olv.local_shipping_cost = (ov.local_shipping_cost *  ar.rate), olv.local_subtotal_cost = (ov.local_subtotal_cost *  ar.rate), olv.local_total_cost = (ov.local_total_cost *  ar.rate),
		olv.local_discount_inc_vat = (ov.local_discount_inc_vat *  ar.rate), olv.local_discount_vat = (ov.local_discount_vat *  ar.rate), olv.local_discount_exc_vat = (ov.local_discount_exc_vat *  ar.rate),
		olv.local_store_credit_inc_vat = (ov.local_store_credit_inc_vat *  ar.rate), olv.local_store_credit_vat = (ov.local_store_credit_vat *  ar.rate), olv.local_store_credit_exc_vat = (ov.local_store_credit_exc_vat *  ar.rate),
		olv.local_adjustment_inc_vat = (ov.local_adjustment_inc_vat *  ar.rate), olv.local_adjustment_vat = (ov.local_adjustment_vat *  ar.rate), olv.local_adjustment_exc_vat = (ov.local_adjustment_exc_vat *  ar.rate),
		olv.local_line_total_inc_vat = (ov.local_total_inc_vat *  ar.rate), olv.local_line_total_vat = (ov.local_total_vat *  ar.rate), olv.local_line_total_exc_vat = (ov.local_total_exc_vat *  ar.rate)
	WHERE olv.item_id = ol.item_id AND ol.{$entity_id} = ov.{$entity_id} AND ol.item_id = ar.item_id 
		AND olv.vat_done = 0;


	-- Update Lines OTHER values (percentages)	
	UPDATE
		{$item_entity}_vat olv, dw_{$item_entity} ol, {$entity}_vat ov
	SET
		olv.local_price_exc_vat = local_line_subtotal_exc_vat / olv.qty,
		olv.vat_percent =  olv.local_line_total_vat / (olv.local_line_total_exc_vat) ,
		olv.vat_percent_before_prof_fee =  ov.vat_rate - 1 ,
		olv.discount_percent = -1 * (ABS(olv.local_discount_exc_vat) /  (olv.local_line_total_exc_vat + ABS(olv.local_discount_exc_vat ))),
		olv.prof_fee_percent =  ov.prof_fee_percent
	WHERE olv.item_id = ol.item_id AND ol.{$entity_id} = ov.{$entity_id} 
		AND olv.vat_done = 0;;

	UPDATE 
		{$item_entity}_vat olv, dw_{$item_entity} ol, {$entity}_vat ov
	SET
		olv.local_price_vat = local_price_inc_vat - local_price_exc_vat
	WHERE olv.item_id = ol.item_id AND ol.{$entity_id} = ov.{$entity_id} 
		AND olv.vat_done = 0;
	

	UPDATE
		{$item_entity}_vat olv, dw_{$item_entity} ol, dw_{$entity} oh, dw_order_headers soh
	SET
		{$this->localToGlobalCols[$entity]}
	WHERE olv.item_id = ol.item_id AND ol.{$entity_id} = oh.{$entity_id} AND oh.order_id = soh.order_id 
		AND olv.vat_done = 0;

	-- Update OTHER FINAL values for Lines
	UPDATE {$item_entity}_vat olv
	SET
		local_margin_amount  = local_line_total_exc_vat - local_total_cost,
		local_margin_percent  = (local_line_total_exc_vat - local_total_cost) / local_line_total_exc_vat
	WHERE olv.vat_done = 0;

	-- Update Global Values for Lines	
	UPDATE {$item_entity}_vat
	SET
		global_prof_fee  = local_to_global_rate * local_prof_fee, 
		global_price_inc_vat  = local_to_global_rate * local_price_inc_vat, global_price_vat  = local_to_global_rate * local_price_vat, global_price_exc_vat  = local_to_global_rate * local_price_exc_vat,
		global_line_subtotal_inc_vat  = local_to_global_rate * local_line_subtotal_inc_vat, global_line_subtotal_vat  = local_to_global_rate * local_line_subtotal_vat, global_line_subtotal_exc_vat  = local_to_global_rate * local_line_subtotal_exc_vat,
		global_shipping_inc_vat  = local_to_global_rate * local_shipping_inc_vat, global_shipping_vat  = local_to_global_rate * local_shipping_vat, global_shipping_exc_vat  = local_to_global_rate * local_shipping_exc_vat,
		global_discount_inc_vat  = local_to_global_rate * local_discount_inc_vat, global_discount_vat  = local_to_global_rate * local_discount_vat, global_discount_exc_vat  = local_to_global_rate * local_discount_exc_vat,
		global_store_credit_inc_vat  = local_to_global_rate * local_store_credit_inc_vat, global_store_credit_vat  = local_to_global_rate * local_store_credit_vat, global_store_credit_exc_vat  = local_to_global_rate * local_store_credit_exc_vat,
		global_adjustment_inc_vat  = local_to_global_rate * local_adjustment_inc_vat, global_adjustment_vat  = local_to_global_rate * local_adjustment_vat, global_adjustment_exc_vat  = local_to_global_rate * local_adjustment_exc_vat,
		global_line_total_inc_vat  = local_to_global_rate * local_line_total_inc_vat, global_line_total_vat  = local_to_global_rate * local_line_total_vat, global_line_total_exc_vat  = local_to_global_rate * local_line_total_exc_vat,
		global_subtotal_cost  = local_to_global_rate * local_subtotal_cost, global_shipping_cost  = local_to_global_rate * local_shipping_cost,
		global_total_cost  = local_to_global_rate * local_total_cost,
		global_margin_amount  = local_to_global_rate * local_margin_amount
	WHERE vat_done = 0;
    
	UPDATE {$item_entity}_vat
	SET global_margin_percent  = global_margin_amount / global_line_total_inc_vat
	WHERE vat_done = 0;

	UPDATE {$item_entity}_vat SET vat_done = 1 WHERE vat_done = 0;

---------------------------------------------  Shipments

-- shipment_lines_vat
	-- Create FAKE Shipment Values (Inc, Vat, Exc and Local, Global) based on Order values related with shipments -- WHAT WITH QTY
	SELECT sl.item_id,
		ov.`local_prof_fee` * (sl.qty / ov.qty),
		ov.`local_price_inc_vat` * (sl.qty / ov.qty), ov.`local_price_vat` * (sl.qty / ov.qty), ov.`local_price_exc_vat` * (sl.qty / ov.qty),
		ov.`local_line_subtotal_inc_vat` * (sl.qty / ov.qty), ov.`local_line_subtotal_vat` * (sl.qty / ov.qty), ov.`local_line_subtotal_exc_vat` * (sl.qty / ov.qty),
		ov.`local_shipping_inc_vat` * (sl.qty / ov.qty), ov.`local_shipping_vat` * (sl.qty / ov.qty), ov.`local_shipping_exc_vat` * (sl.qty / ov.qty),
		ov.`local_discount_inc_vat` * (sl.qty / ov.qty), ov.`local_discount_vat` * (sl.qty / ov.qty), ov.`local_discount_exc_vat` * (sl.qty / ov.qty),
		ov.`local_store_credit_inc_vat` * (sl.qty / ov.qty), ov.`local_store_credit_vat` * (sl.qty / ov.qty), ov.`local_store_credit_exc_vat` * (sl.qty / ov.qty),
		ov.`local_adjustment_inc_vat` * (sl.qty / ov.qty), ov.`local_adjustment_vat` * (sl.qty / ov.qty), ov.`local_adjustment_exc_vat` * (sl.qty / ov.qty),
		ov.`local_line_total_inc_vat` * (sl.qty / ov.qty), ov.`local_line_total_vat` * (sl.qty / ov.qty), ov.`local_line_total_exc_vat` * (sl.qty / ov.qty),
		ov.`local_subtotal_cost` * (sl.qty / ov.qty), ov.`local_shipping_cost`,
		ov.`local_to_global_rate`, ov.`order_currency_code`,
		ov.`global_prof_fee` * (sl.qty / ov.qty),
		ov.`global_price_inc_vat` * (sl.qty / ov.qty), ov.`global_price_vat` * (sl.qty / ov.qty), ov.`global_price_exc_vat` * (sl.qty / ov.qty),
		ov.`global_line_subtotal_inc_vat` * (sl.qty / ov.qty), ov.`global_line_subtotal_vat` * (sl.qty / ov.qty), ov.`global_line_subtotal_exc_vat` * (sl.qty / ov.qty),
		ov.`global_shipping_inc_vat` * (sl.qty / ov.qty), ov.`global_shipping_vat` * (sl.qty / ov.qty), ov.`global_shipping_exc_vat` * (sl.qty / ov.qty),
		ov.`global_discount_inc_vat` * (sl.qty / ov.qty), ov.`global_discount_vat` * (sl.qty / ov.qty), ov.`global_discount_exc_vat` * (sl.qty / ov.qty),
		ov.`global_store_credit_inc_vat` * (sl.qty / ov.qty), ov.`global_store_credit_vat` * (sl.qty / ov.qty), ov.`global_store_credit_exc_vat` * (sl.qty / ov.qty),
		ov.`global_adjustment_inc_vat` * (sl.qty / ov.qty), ov.`global_adjustment_vat` * (sl.qty / ov.qty), ov.`global_adjustment_exc_vat` * (sl.qty / ov.qty),
		ov.`global_line_total_inc_vat` * (sl.qty / ov.qty), ov.`global_line_total_vat` * (sl.qty / ov.qty), ov.`global_line_total_exc_vat` * (sl.qty / ov.qty),
		ov.`global_subtotal_cost` * (sl.qty / ov.qty), ov.`global_shipping_cost`,
		ov.`prof_fee_percent`,
		ov.`vat_percent_before_prof_fee`,
		0 AS vat_done,
		sl.qty, sl.weight
  FROM
			dw_shipment_lines sl 
		LEFT JOIN 
			order_lines_vat ov ON ov.item_id = sl.order_line_id
		LEFT JOIN 
			shipment_lines_vat slv ON slv.item_id = sl.item_id
		INNER JOIN 
			dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = sh.customer_id;

	-- Update
	UPDATE shipment_lines_vat
	SET
		local_total_cost = local_subtotal_cost + local_shipping_cost,
		local_margin_amount = local_line_total_exc_vat - (local_subtotal_cost + local_shipping_cost),
		global_total_cost = global_subtotal_cost + global_shipping_cost,
		global_margin_amount = global_line_total_exc_vat - (global_subtotal_cost + global_shipping_cost)
	WHERE vat_done = 0;

	-- Update
	UPDATE shipment_lines_vat
	SET
		local_margin_percent = local_margin_amount / local_line_total_inc_vat,
		global_margin_percent = global_margin_amount / global_line_total_inc_vat,
		discount_percent = -1 * (ABS(local_discount_exc_vat) / (local_line_total_exc_vat + ABS(local_discount_exc_vat))),
		vat_percent = (local_line_total_vat) / (local_line_total_exc_vat)
	WHERE vat_done = 0;

-- shipment_headers_vat
	-- Create FAKE Shipment Values (Inc, Vat, Exc and Local, Global) with SUM of Shipment Lines created on the step before
	-- Insert
	SELECT sl.shipment_id,
		SUM(olv.local_line_subtotal_inc_vat) AS local_subtotal_inc_vat, SUM(olv.local_line_subtotal_vat), SUM(olv.local_line_subtotal_exc_vat),
		SUM(olv.local_shipping_inc_vat), SUM(olv.local_shipping_vat), SUM(olv.local_shipping_exc_vat),
		SUM(olv.local_discount_inc_vat), SUM(olv.local_discount_vat), SUM(olv.local_discount_exc_vat),
		SUM(olv.local_store_credit_inc_vat), SUM(olv.local_store_credit_vat), SUM(olv.local_store_credit_exc_vat),
		SUM(olv.local_adjustment_inc_vat), SUM(olv.local_adjustment_vat), SUM(olv.local_adjustment_exc_vat),
		SUM(olv.local_line_total_inc_vat) AS local_total_inc_vat, SUM(olv.local_line_total_vat), SUM(olv.local_line_total_exc_vat) AS local_total_exc_vat,
		SUM(olv.`local_subtotal_cost`), SUM(olv.`local_shipping_cost`),
		SUM(olv.`local_total_cost`),
		SUM(olv.`local_margin_amount`),
		SUM(olv.global_line_subtotal_inc_vat), SUM(olv.global_line_subtotal_vat), SUM(olv.global_line_subtotal_exc_vat),
		SUM(olv.global_shipping_inc_vat), SUM(olv.global_shipping_vat), SUM(olv.global_shipping_exc_vat),
		SUM(olv.global_discount_inc_vat), SUM(olv.global_discount_vat), SUM(olv.global_discount_exc_vat),
		SUM(olv.global_store_credit_inc_vat), SUM(olv.global_store_credit_vat), SUM(olv.global_store_credit_exc_vat),
		SUM(olv.global_adjustment_inc_vat), SUM(olv.global_adjustment_vat), SUM(olv.global_adjustment_exc_vat),
		SUM(olv.global_line_total_inc_vat) AS global_total_inc_vat, SUM(olv.global_line_total_vat), SUM(olv.global_line_total_exc_vat) AS global_total_exc_vat,
		SUM(olv.`global_subtotal_cost`), SUM(olv.`global_shipping_cost`),
		SUM(olv.`global_total_cost`),
		SUM(olv.`global_margin_amount`),
		MAX(olv.prof_fee_percent),
		SUM(olv.local_prof_fee),
		SUM(olv.global_prof_fee),
		SUM(olv.local_total_cost),
		0 AS vat_rate,
		MAX(olv.`vat_percent_before_prof_fee`),
		0 AS `discount_percent`,
		'' AS country_type, '' AS country_id,
		'' AS vat_logic,
		0 AS entity_order_id,
		MAX(p.is_lens) AS has_lens,
		0 AS vat_done,
		0 AS rollup_total, 0 AS rollup_has_lens,
		SUM(sl.qty), SUM(sl.weight)
	FROM
			shipment_lines_vat olv 
		INNER JOIN 
			dw_shipment_lines sl ON sl.item_id = olv.item_id
		LEFT JOIN 
			dw_products p ON p.product_id = sl.product_id
		INNER JOIN 
			dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = sh.customer_id
	GROUP BY sl.shipment_id;

	UPDATE shipment_headers_vat
	SET
		local_margin_percent  = local_margin_amount / local_total_inc_vat,
		global_margin_percent = global_margin_amount / global_total_inc_vat,
		vat_percent = (local_total_vat) / (local_total_exc_vat),
		discount_percent = (ABS(local_discount_exc_vat) / (local_total_exc_vat + ABS(local_discount_exc_vat)))
	WHERE vat_done = 0;

	
	-- creditmemo_lines_with_shipment
	-- Create Fake Shipment Credit Memo Lines (FIRST TABLE WITH SHIPMENT - CREDITMEMOS AND AFTER WITH LINES VALUES)
		-- Insert 
		SELECT DISTINCT cl.item_id, 
			MIN(sl.item_id) AS shipment_item_id, MIN(sl.shipment_id) AS shipment_id
		FROM 
				dw_creditmemo_lines cl 
			INNER JOIN 
				dw_shipment_lines sl ON sl.order_line_id = cl.order_line_id
			INNER JOIN 
				dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
			INNER JOIN 
				dw_updated_customers uc ON uc.customer_id = sh.customer_id
		GROUP BY cl.item_id;

	-- creditmemo_shipment_lines_vat
		-- Insert 
		SELECT cl.item_id,
			`local_prof_fee`,
			`local_price_inc_vat`, `local_price_vat`, `local_price_exc_vat`,
			`local_line_subtotal_inc_vat`, `local_line_subtotal_vat`, `local_line_subtotal_exc_vat`,
			`local_shipping_inc_vat`, `local_shipping_vat`, `local_shipping_exc_vat`,
			`local_discount_inc_vat`, `local_discount_vat`, `local_discount_exc_vat`,
			`local_store_credit_inc_vat`, `local_store_credit_vat`, `local_store_credit_exc_vat`,
			`local_adjustment_inc_vat`, `local_adjustment_vat`, `local_adjustment_exc_vat`,
			`local_line_total_inc_vat`, `local_line_total_vat`, `local_line_total_exc_vat`,
			`local_subtotal_cost`, `local_shipping_cost`,
			`local_total_cost`,
			`local_margin_amount`,
			`local_margin_percent`,
			`local_to_global_rate`, ch.`order_currency_code`,
			`global_prof_fee`,
			`global_price_inc_vat`, `global_price_vat`, `global_price_exc_vat`,
			`global_line_subtotal_inc_vat`, `global_line_subtotal_vat`, `global_line_subtotal_exc_vat`,
			`global_shipping_inc_vat`, `global_shipping_vat`, `global_shipping_exc_vat`,
			`global_discount_inc_vat`, `global_discount_vat`, `global_discount_exc_vat`,
			`global_store_credit_inc_vat`, `global_store_credit_vat`, `global_store_credit_exc_vat`,
			`global_adjustment_inc_vat`, `global_adjustment_vat`, `global_adjustment_exc_vat`,
			`global_line_total_inc_vat`, `global_line_total_vat`, `global_line_total_exc_vat`,
			`global_subtotal_cost`, `global_shipping_cost`,
			`global_total_cost`,
			`global_margin_amount`,
			`global_margin_percent`,
			`prof_fee_percent`,
			`vat_percent_before_prof_fee`,
			`vat_percent`,
			olv.`discount_percent`,
			vat_done,
			olv.qty,
			ol.weight * olv.qty AS row_weight,
			cl.shipment_item_id,
			cl.shipment_id
	  FROM
				creditmemo_lines_vat olv 
			INNER JOIN 
				creditmemo_lines_with_shipment cl ON cl.item_id = olv.item_id
			INNER JOIN 
				dw_creditmemo_lines cl2 ON cl2.item_id = olv.item_id
			INNER JOIN 
				dw_order_lines ol ON ol.item_id = cl2.order_line_id
			INNER JOIN 
				dw_creditmemo_headers ch ON ch.creditmemo_id = cl2.creditmemo_id
			INNER JOIN 
				dw_order_headers doh ON ch.order_id = doh.order_id
			INNER JOIN 
				dw_updated_customers uc ON uc.customer_id = doh.customer_id;

	-- creditmemo_shipment_headers_vat: SUM of SHIPMENT CREDITMEMO LINE values created before
		-- Insert 
		SELECT cl.creditmemo_id,
			SUM(olv.local_line_subtotal_inc_vat) AS local_subtotal_inc_vat, SUM(olv.local_line_subtotal_vat) AS local_subtotal_vat, SUM(olv.local_line_subtotal_exc_vat) AS local_subtotal_exc_vat ,
			SUM(olv.local_shipping_inc_vat) AS local_shipping_inc_vat, SUM(olv.local_shipping_vat) AS local_shipping_vat, SUM(olv.local_shipping_exc_vat) AS local_shipping_exc_vat,
			SUM(olv.local_discount_inc_vat) AS local_discount_inc_vat, SUM(olv.local_discount_vat) AS local_discount_vat, SUM(olv.local_discount_exc_vat) AS local_discount_exc_vat,
			SUM(olv.local_store_credit_inc_vat) AS local_store_credit_inc_vat, SUM(olv.local_store_credit_vat) AS local_store_credit_vat, SUM(olv.local_store_credit_exc_vat) AS local_store_credit_exc_vat,
			SUM(olv.local_adjustment_inc_vat) AS local_adjustment_inc_vat, SUM(olv.local_adjustment_vat) AS local_adjustment_vat, SUM(olv.local_adjustment_exc_vat) AS local_adjustment_exc_vat,
			SUM(olv.local_line_total_inc_vat) AS local_total_inc_vat, SUM(olv.local_line_total_vat) AS local_total_vat, SUM(olv.local_line_total_exc_vat) AS local_total_exc_vat ,
			SUM(olv.`local_subtotal_cost`) AS local_subtotal_cost, SUM(olv.`local_shipping_cost`) AS local_shipping_cost,
			SUM(olv.`local_total_cost`) AS local_total_cost,
			SUM(olv.`local_margin_amount`) AS local_margin_amount,
			0 AS local_margin_percent,
			SUM(olv.global_line_subtotal_inc_vat) AS global_subtotal_inc_vat, SUM(olv.global_line_subtotal_vat) AS global_subtotal_vat, SUM(olv.global_line_subtotal_exc_vat) AS global_subtotal_exc_vat,
			SUM(olv.global_shipping_inc_vat) AS global_shipping_inc_vat, SUM(olv.global_shipping_vat) AS global_shipping_vat, SUM(olv.global_shipping_exc_vat) AS global_shipping_exc_vat ,
			SUM(olv.global_discount_inc_vat) AS global_discount_inc_vat, SUM(olv.global_discount_vat) AS global_discount_vat, SUM(olv.global_discount_exc_vat) AS global_discount_exc_vat,
			SUM(olv.global_store_credit_inc_vat) AS global_store_credit_inc_vat, SUM(olv.global_store_credit_vat) AS global_store_credit_vat, SUM(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat,
			SUM(olv.global_adjustment_inc_vat) AS global_adjustment_inc_vat, SUM(olv.global_adjustment_vat) AS global_adjustment_vat, SUM(olv.global_adjustment_exc_vat) AS global_adjustment_exc_vat,
			/**SUM(olv.global_line_total_inc_vat + IFNULL(olv.global_shipping_inc_vat,0) + IFNULL(-1 * ABS(olv.global_discount_inc_vat),0) + IFNULL(olv.global_adjustment_inc_vat,0)) AS global_total_inc_vat ,**/
			SUM(olv.global_line_total_inc_vat) AS global_total_inc_vat, SUM(olv.global_line_total_vat) AS global_total_vat, SUM(olv.`global_line_total_exc_vat`) AS global_total_exc_vat,
			/**SUM(olv.global_line_total_exc_vat + IFNULL(olv.global_shipping_exc_vat,0) + IFNULL(-1 * ABS(olv.global_discount_exc_vat),0) + IFNULL(olv.global_adjustment_exc_vat,0)) AS global_total_exc_vat ,**/
			SUM(olv.`global_subtotal_cost`) AS global_subtotal_cost, SUM(olv.`global_shipping_cost`) AS global_shipping_cost,
			SUM(olv.`global_total_cost`) AS global_total_cost,
			SUM(olv.`global_margin_amount`) AS global_margin_amount,
			0 AS global_margin_percent,
			SUM(olv.local_prof_fee) AS prof_fee_percent,
			SUM(olv.local_prof_fee) AS prof_fee,
			SUM(olv.global_prof_fee) AS global_prof_fee,
			SUM(olv.local_total_cost) AS total_cost,
			MAX(olv.vat_percent) AS vat_percent,
			0 AS vat_rate ,
			0 AS `vat_percent_before_prof_fee`,
			0 AS `discount_percent`,
			'' AS country_type,
			'' AS country_id ,
			'' AS vat_logic ,
			0 AS entity_order_id ,
			MAX(p.is_lens) AS has_lens ,
			0 AS vat_done ,
			0 AS rollup_total ,
			0 AS rollup_has_lens,
			SUM(cl.qty) AS total_qty,
			SUM(olv.row_weight) AS total_weight,
			MAX(olv.shipment_id) AS shipment_id
    FROM
			creditmemo_shipment_lines_vat olv 
		INNER JOIN 
			dw_creditmemo_lines cl ON cl.item_id = olv.item_id
		LEFT JOIN 
			dw_products p ON p.product_id = cl.product_id
		INNER JOIN 
			dw_creditmemo_headers ch ON ch.creditmemo_id = cl.creditmemo_id
		INNER JOIN 
			dw_order_headers doh ON ch.order_id = doh.order_id
		INNER JOIN 
			dw_updated_customers uc ON uc.customer_id = doh.customer_id
    GROUP BY cl.creditmemo_id;

	UPDATE creditmemo_shipment_headers_vat
	SET
		local_margin_percent  = local_margin_amount / local_total_inc_vat,
		global_margin_percent = global_margin_amount / global_total_inc_vat,
		global_margin_percent = global_margin_amount / global_total_inc_vat,
		vat_percent           = (local_total_vat) / (local_total_exc_vat),
		discount_percent      = -1 * (ABS(local_discount_exc_vat) / (local_total_exc_vat + ABS(local_discount_exc_vat)))
	WHERE vat_done = 0;

	UPDATE creditmemo_shipment_headers_vat SET vat_done = 1 WHERE vat_done = 0 ; 

	UPDATE shipment_headers_vat SET vat_done = 1 WHERE vat_done = 0;