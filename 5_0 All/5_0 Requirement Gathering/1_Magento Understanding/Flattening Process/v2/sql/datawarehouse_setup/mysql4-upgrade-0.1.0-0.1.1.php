<?php
//proper table syntax
$installer = $this;
$installer->startSetup();


// products 
            
//$installer->run("CREATE OR REPLACE VIEW vw_catalog_product_website AS 
    //SELECT * 
    //FROM catalog_product_website 
    //UNION SELECT DISTINCT product_id,0 
    //FROM dw_products_all_stores;");
                
            
//$installer->run("CREATE OR REPLACE VIEW vw_products AS
//SELECT
	//cw.`store_name` AS store_name,
	//pas.product_id AS product_id,
	//`status` AS `status`,
	//pcf.category_id AS category_id,
	//sku AS sku,
	//br.value AS brand,
	//created_at AS created_at,
	//updated_at AS updated_at,
	//cf.product_type AS product_type,
	//product_lifecycle AS product_lifecycle,
	//product_lifecycle_text AS product_lifecycle_text,
	//pas.telesales_only AS telesales_only,
	//stocked_lens AS stocked_lens,
	//tax_class_id AS tax_class_id,

	//pas.`name` AS `name`,
	//description AS description,
	//short_description AS short_description,
	//cat_description AS  category_list_description,
	//0 AS average_cost ,
	//weight AS weight,
	//man.value AS manufacturer,

	//meta_title AS meta_title,
	//meta_keyword AS meta_keyword,
	//meta_description AS meta_description,
	//image AS image,
	//small_image AS small_image,
	//thumbnail AS thumbnail,
	//url_key AS url_key,
	//url_path AS url_path,
	//pas.visibility AS visibility,
	//image_label AS image_label,
	//small_image_label AS small_image_label,
	//thumbnail_label AS thumbnail_label,

	//equivalent_sku AS equivalent_sku,
	//daysperlens AS daysperlens,
	//altdaysperlens AS alt_daysperlens,
	//equivalence AS equivalence,
	//availability AS availability,
	//`condition` AS `condition` ,
	//google_base_price  AS google_base_price,
	//promotional_product AS promotional_product,
	//promotion_rule AS promotion_rule,
	//promotion_rule2 AS promotion_rule2,
	//upsell_text AS upsell_text,
	//replacement_sku AS replacement_sku,
	//price_comp_price AS price_comp_price,
	//google_feed_title_prefix AS google_feed_title_prefix,
	//google_feed_title_suffix AS google_feed_title_suffix,
	//autoreorder_alter_sku AS autoreorder_alter_sku,
	//rrp_price  AS rrp_price,
	//promotional_text AS promotional_text,
	//glasses_colour AS glasses_colour,
	//glasses_material AS glasses_material,
	//glasses_size AS glasses_size,
	//glasses_sex AS glasses_sex,
	//glasses_style AS glasses_style,
	//glasses_shape AS glasses_shape,
	//glasses_lens_width AS glasses_lens_width,
	//glasses_bridge_width AS glasses_bridge_width,

	//packsize AS pack_qty,
	//packtext AS packtext,
	//0 AS alt_pack_qty,

	//pq.first_tier_qty AS base_pack_qty,
	//pq.first_tier_qty / GREATEST(1,CAST(pas.packsize AS UNSIGNED))AS base_no_packs,

	//dp1.local_unit_price_inc_vat AS local_base_unit_price_inc_vat,
	//dp1.local_qty_price_inc_vat  AS local_base_value_inc_vat ,
	//dp1.local_pack_price_inc_vat  AS local_base_pack_price_inc_vat ,
	//ROUND((dp1.local_qty_price_exc_vat / dp1.qty),4) AS local_base_unit_price_exc_vat,
	//dp1.local_qty_price_exc_vat  AS local_base_value_exc_vat ,
	//CAST(ROUND(((dp1.local_qty_price_exc_vat / dp1.qty) * GREATEST(1,CAST(pas.packsize AS UNSIGNED))) ,4) AS DECIMAL(12,4)) AS local_base_pack_price_exc_vat ,
	//dp1.local_prof_fee  AS local_base_prof_fee ,
	//dp1.local_product_cost  AS local_base_product_cost ,
	//dp1.local_shipping_cost  AS local_base_shipping_cost ,
	//dp1.local_total_cost  AS local_base_total_cost ,
	//dp1.local_margin_amount  AS local_base_margin_amount ,
	//dp1.local_margin_percent  AS local_base_margin_percent ,

	//dp1.global_unit_price_inc_vat AS global_base_unit_price_inc_vat,
	//dp1.global_qty_price_inc_vat  AS global_base_value_inc_vat ,
	//dp1.global_pack_price_inc_vat  AS global_base_pack_price_inc_vat ,
	//ROUND(((dp1.local_qty_price_exc_vat * dp1.local_to_global_rate) / dp1.qty),4)AS global_base_unit_price_exc_vat,
	//(dp1.local_qty_price_exc_vat * dp1.local_to_global_rate) AS global_base_value_exc_vat ,
	//CAST(ROUND((((dp1.local_qty_price_exc_vat * dp1.local_to_global_rate) / dp1.qty) * GREATEST(1,CAST(pas.packsize AS UNSIGNED))),4) AS DECIMAL(12,4)) AS global_base_pack_price_exc_vat ,
	//dp1.global_prof_fee  AS global_base_prof_fee ,
	//dp1.global_product_cost  AS global_base_product_cost ,
	//dp1.global_shipping_cost  AS global_base_shipping_cost ,
	//dp1.global_total_cost  AS global_base_total_cost ,
	//dp1.global_margin_amount  AS global_base_margin_amount ,
	//dp1.global_margin_percent  AS global_base_margin_percent ,


	//pq.multibuy_qty AS multi_pack_qty,
	//pq.multibuy_qty / GREATEST(1,CAST(pas.packsize AS UNSIGNED)) AS multi_no_packs,

	//dp2.local_unit_price_inc_vat AS local_multi_unit_price_inc_vat,
	//dp2.local_qty_price_inc_vat  AS local_multi_value_inc_vat ,
	//dp2.local_pack_price_inc_vat  AS local_multi_pack_price_inc_vat ,
	//ROUND((dp2.local_qty_price_exc_vat / dp2.qty),4) AS local_multi_unit_price_exc_vat,
	//dp2.local_qty_price_exc_vat  AS local_multi_value_exc_vat ,
	//CAST(ROUND(((dp2.local_qty_price_exc_vat / dp2.qty) * GREATEST(1,CAST(pas.packsize AS UNSIGNED))),4) AS DECIMAL(12,4)) AS local_multi_pack_price_exc_vat ,
	//dp2.local_prof_fee  AS local_multi_prof_fee ,
	//dp2.local_product_cost  AS local_multi_product_cost ,
	//dp2.local_shipping_cost  AS local_multi_shipping_cost ,
	//dp2.local_total_cost  AS local_multi_total_cost ,
	//dp2.local_margin_amount  AS local_multi_margin_amount ,
	//dp2.local_margin_percent  AS local_multi_margin_percent ,

	//dp2.global_unit_price_inc_vat AS global_multi_unit_price_inc_vat,
	//dp2.global_qty_price_inc_vat  AS global_multi_value_inc_vat ,
	//dp2.global_pack_price_inc_vat  AS global_multi_pack_price_inc_vat ,
	//ROUND(((dp2.local_qty_price_exc_vat * dp2.local_to_global_rate) / dp2.qty),4) AS global_multi_unit_price_exc_vat,
	//(dp2.local_qty_price_exc_vat * dp2.local_to_global_rate) AS global_multi_value_exc_vat ,
	//CAST(ROUND((((dp2.local_qty_price_exc_vat * dp2.local_to_global_rate)/ dp2.qty) * GREATEST(1,CAST(pas.packsize AS UNSIGNED))),4) AS DECIMAL(12,4)) AS global_multi_pack_price_exc_vat ,
	//dp2.global_prof_fee  AS global_multi_prof_fee ,
	//dp2.global_product_cost  AS global_multi_product_cost ,
	//dp2.global_shipping_cost  AS global_multi_shipping_cost ,
	//dp2.global_total_cost  AS global_multi_total_cost ,
	//dp2.global_margin_amount  AS global_multi_margin_amount ,
	//dp2.global_margin_percent  AS global_multi_margin_percent ,

	//dp1.local_to_global_rate ,
	//dp1.store_currency_code,
	//dp1.prof_fee_percent ,
	//dp1.vat_percent_before_prof_fee, 
	//dp1.vat_percent 

//FROM
	//dw_products_all_stores pas INNER JOIN vw_catalog_product_website cpw ON pas.product_id = cpw.product_id AND pas.store_id = cpw.website_id
                    //LEFT JOIN dw_stores cw ON cw.store_id = pas.store_id
					//LEFT JOIN dw_product_category_flat pcf ON pcf.product_id = pas.product_id
                    //LEFT JOIN dw_flat_category cf ON cf.category_id = pcf.category_id
					//LEFT JOIN eav_attribute_option_value man ON man.option_id = pas.manufacturer
					//LEFT JOIN eav_attribute_option_value br ON br.option_id = pas.brand
					//LEFT JOIN dw_product_price_qtys pq ON pq.product_id = pas.product_id AND pq.store_id = pas.store_id
					//LEFT JOIN dw_product_prices dp1 ON dp1.product_id = pas.product_id AND dp1.store_id = pas.store_id AND pq.first_tier_qty = dp1.qty
					//LEFT JOIN dw_product_prices dp2 ON dp2.product_id = pas.product_id AND dp2.store_id = pas.store_id AND pq.multibuy_qty = dp2.qty;
    //");    
            
//$installer->run("CREATE OR REPLACE VIEW vw_product_prices AS
//SELECT 
	//price_id,
	//store_name,
	//pas.product_id,
	//qty,
	//pack_size,
	//local_unit_price_inc_vat,
	//local_qty_price_inc_vat ,
	//local_pack_price_inc_vat ,
	//local_prof_fee ,
	//local_product_cost ,
	//local_shipping_cost ,
	//local_total_cost ,
	//local_margin_amount ,
	//local_margin_percent, 
	//local_to_global_rate, 
	//store_currency_code,

	//global_unit_price_inc_vat,
	//global_qty_price_inc_vat ,
	//global_pack_price_inc_vat ,
	//global_prof_fee ,
	//global_product_cost ,
	//global_shipping_cost ,
	//global_total_cost ,
	//global_margin_amount ,
	//global_margin_percent, 

	//prof_fee_percent ,
	//vat_percent_before_prof_fee ,
	//vat_percent 
//FROM
	//dw_product_prices pas   INNER JOIN catalog_product_website cpw ON pas.product_id = cpw.product_id AND pas.store_id = cpw.website_id;");

//$installer->run("CREATE OR REPLACE VIEW vw_invoice_headers_invoice AS
//SELECT 
	//cw.store_name AS store_name,
	//inv.invoice_id AS invoice_id,
	//inv.invoice_no AS invoice_no,
	//CAST('INVOICE' AS CHAR(25)) AS document_type,
    //inv.created_at AS document_date,
    //inv.updated_at AS document_updated_at,
    //inv.invoice_id AS document_id,
	//inv.created_at AS invoice_date, 
	//inv.updated_at AS updated_at,
	//oh.order_id AS order_id,
	//oh.order_no AS order_no,
	//oh.created_at AS order_date,

	//oh.customer_id AS customer_id,
	//oh.customer_email AS customer_email,
	//oh.customer_firstname AS customer_firstname,
	//oh.customer_lastname AS customer_lastname,
	//oh.customer_middlename AS customer_middlename,
	//oh.customer_prefix AS customer_prefix,
	//oh.customer_suffix AS customer_suffix,
	//oh.customer_taxvat AS customer_taxvat,
	//oh.status AS `status`,	
	//oh.coupon_code AS coupon_code,
	//oh.weight AS weight,
	//oh.customer_gender AS customer_gender,
	//oh.customer_dob AS customer_dob,

	//LEFT(oh.shipping_description,LOCATE(' - ',oh.shipping_description)) shipping_carrier,
	//oh.shipping_description AS shipping_method,
	//oh.warehouse_approved_time AS approved_time,
	
	//oc.length_of_time_to_verify AS length_of_time_to_verify, 
	//oc.length_of_time_for_payment AS length_of_time_for_payment,
	//oc.length_of_time_to_invoice AS length_of_time_to_invoice,
	//ic.length_of_time_invoice_to_first_shipment AS length_of_time_invoice_to_first_shipment,
	//ic.length_of_time_invoice_to_last_shipment AS length_of_time_invoice_to_last_shipment,
	//ic.length_of_time_to_first_shipment AS length_of_time_to_first_shipment,
	//ic.length_of_time_to_last_shipment AS length_of_time_to_last_shipment,

	//IFNULL(pm.payment_method_name,op.method) AS payment_method,
	//op.amount_authorized  AS local_payment_authorized,
	//op.amount_canceled AS local_payment_canceled,

	//op.cc_exp_month AS cc_exp_month,
	//op.cc_exp_year AS cc_exp_year,
	//op.cc_ss_start_month AS cc_start_month,
	//op.cc_ss_start_year AS cc_start_year,
	//op.cc_last4 AS cc_last4,
	//op.cc_status_description AS cc_status_description,
	//op.cc_owner AS cc_owner,
	//IFNULL(ctm.card_type_name,op.cc_type) AS cc_type,
	//op.cc_status AS cc_status,
	//op.cc_ss_issue AS cc_issue_no,
	//op.cc_avs_status AS cc_avs_status,

	//'' AS payment_info1,
	//'' AS payment_info2,
	//'' AS payment_info3,
	//'' AS payment_info4,
	//'' AS payment_info5,

	//bill.email AS billing_email,
	//bill.company AS billing_company,
	//bill.prefix AS billing_prefix,
	//bill.firstname AS billing_firstname,
	//bill.middlename AS billing_middlename,
	//bill.lastname AS billing_lastname,
	//bill.suffix AS billing_suffix,
	//bill.street AS billing_street,
	//bill.city AS billing_city,
	//bill.region AS billing_region,
	//bill.region_id AS billing_region_id,
	//bill.postcode AS billing_postcode,
	//bill.country_id AS billing_country_id,
	//bill.telephone AS billing_telephone,
	//bill.fax AS billing_fax,

	//ship.email AS shipping_email,
	//ship.company AS shipping_company,
	//ship.prefix AS shipping_prefix,
	//ship.firstname AS shipping_firstname,
	//ship.middlename AS shipping_middlename,
	//ship.lastname AS shipping_lastname,
	//ship.suffix AS shipping_suffix,
	//ship.street AS shipping_street,
	//ship.city AS shipping_city,
	//ship.region AS shipping_region,
	//ship.region_id AS shipping_region_id,
	//ship.postcode AS shipping_postcode,
	//ship.country_id AS shipping_country_id,
	//ship.telephone AS shipping_telephone,
	//ship.fax AS shipping_fax,

	//ov.has_lens AS has_lens,
	//ov.total_qty AS total_qty,

	//ov.prof_fee  AS local_prof_fee ,
	//ov.local_subtotal_inc_vat  AS local_subtotal_inc_vat ,
	//ov.local_subtotal_vat  AS local_subtotal_vat ,
	//ov.local_subtotal_exc_vat  AS local_subtotal_exc_vat ,
	//ov.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//ov.local_shipping_vat  AS local_shipping_vat ,
	//ov.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//-1 * ABS(ov.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//-1 * ABS(ov.local_discount_vat)  AS local_discount_vat ,
	//-1 * ABS(ov.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//-1 * ABS(ov.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//-1 * ABS(ov.local_store_credit_vat)  AS local_store_credit_vat ,
	//-1 * ABS(ov.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//ov.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//ov.local_adjustment_vat  AS local_adjustment_vat ,
	//ov.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//ov.local_total_inc_vat  AS local_total_inc_vat ,
	//ov.local_total_vat  AS local_total_vat ,
	//ov.local_total_exc_vat  AS local_total_exc_vat ,
	//ov.local_subtotal_cost  AS local_subtotal_cost ,
	//ov.local_shipping_cost  AS local_shipping_cost ,
	//ov.local_total_cost  AS local_total_cost ,
	//ov.local_margin_amount  AS local_margin_amount ,
	//ov.local_margin_percent  AS local_margin_percent ,
	//oh.base_to_global_rate  AS local_to_global_rate ,
	//oh.order_currency_code AS order_currency_code,

	//ov.global_prof_fee  AS global_prof_fee ,
	//ov.global_subtotal_inc_vat  AS global_subtotal_inc_vat ,
	//ov.global_subtotal_vat  AS global_subtotal_vat ,
	//ov.global_subtotal_exc_vat  AS global_subtotal_exc_vat ,
	//ov.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//ov.global_shipping_vat  AS global_shipping_vat ,
	//ov.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//-1 * ABS(ov.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//-1 * ABS(ov.global_discount_vat)  AS global_discount_vat ,
	//-1 * ABS(ov.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//-1 * ABS(ov.global_store_credit_inc_vat) AS global_store_credit_inc_vat ,
	//-1 * ABS(ov.global_store_credit_vat)  AS global_store_credit_vat ,
	//-1 * ABS(ov.global_store_credit_exc_vat)  AS global_store_credit_exc_vat ,
	//ov.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//ov.global_adjustment_vat  AS global_adjustment_vat ,
	//ov.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//ov.global_total_inc_vat  AS global_total_inc_vat ,
	//ov.global_total_vat  AS global_total_vat ,
	//ov.global_total_exc_vat  AS global_total_exc_vat ,
	//ov.global_subtotal_cost  AS global_subtotal_cost ,
	//ov.global_shipping_cost  AS global_shipping_cost ,
	//ov.global_total_cost  AS global_total_cost ,
	//ov.global_margin_amount  AS global_margin_amount ,
	//ov.global_margin_percent  AS global_margin_percent ,

	//ov.prof_fee_percent  AS prof_fee_percent ,
	//ov.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//ov.vat_percent  AS vat_percent ,
	//ABS(ov.discount_percent) AS discount_percent ,
	
	//oh.customer_note AS customer_note,
	//oh.customer_note_notify AS customer_note_notify, 
	//oh.remote_ip AS remote_ip,
	//oh.affilcode AS business_source,
	//bc.channel AS business_channel,
	//oh.affilBatch AS affilBatch,
	//oh.affilCode AS affilCode,
	//oh.affilUserRef AS affilUserRef,
	//oh.postoptics_auto_verification AS auto_verification,
    //CASE WHEN oh.postoptics_source = 'user' THEN 'web' ELSE  oh.postoptics_source  END As order_type,
    //ohm.ORDER_LIFECYCle AS order_lifecycle,
	//oh.reminder_date AS reminder_date,
	//oh.reminder_mobile AS reminder_mobile,
	//oh.reminder_period AS reminder_period,
	//oh.reminder_presc AS reminder_presc,
	//oh.reminder_type AS reminder_type,
	//oh.reminder_sent   AS reminder_sent  ,
	//oh.reminder_follow_sent   AS reminder_follow_sent  ,
	//oh.telesales_method_code AS telesales_method_code,
	//oh.telesales_admin_username AS telesales_admin_username,
	//oh.reorder_on_flag   AS reorder_on_flag  ,
	//oh.reorder_profile_id AS reorder_profile_id,
	//rp.next_order_date  AS reorder_date ,
	//rp.interval_days AS reorder_interval,
	//rqp.cc_exp_month AS reorder_cc_exp_month,
	//rqp.cc_exp_year AS reorder_cc_exp_year,
	//oh.automatic_reorder   AS automatic_reorder  ,
	//oh.presc_verification_method AS presc_verification_method,
	//oh.referafriend_code AS referafriend_code,
	//oh.referafriend_referer AS referafriend_referer

//FROM	
	//dw_invoice_headers inv
				//INNER JOIN dw_order_headers oh 	ON oh.order_id = inv.order_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
				//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
				//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
				//INNER JOIN invoice_headers_vat ov ON ov.invoice_id = inv.invoice_id
				//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
				//LEFT JOIN dw_invoice_calender_dates ic ON ic.invoice_id = inv.invoice_id
				//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
                //LEFT JOIN dw_payment_method_map pm ON pm.payment_method = op.method
                //LEFT JOIN dw_card_type_map ctm ON ctm.card_type = op.cc_type
                //LEFT JOIN po_reorder_quote rq ON rq.entity_id = rp.reorder_quote_id  
                //LEFT JOIN po_reorder_quote_payment rqp ON rqp.quote_id =  rq.entity_id
                //LEFT JOIN dw_bis_source_map bc ON bc.source_code = oh.affilcode
                //LEFT JOIN dw_order_headers_marketing ohm ON ohm.order_id = oh.order_id;");
                
                
//$installer->run("CREATE OR REPLACE VIEW vw_invoice_headers_creditmemo AS
//SELECT 
	//cw.store_name AS store_name,
	//cred.creditmemo_id AS invoice_id,
	//cred.creditmemo_no AS invoice_no,
	//CAST('CREDITMEMO' AS CHAR(25)) AS document_type,
    //cred.created_at AS document_date, 
    //cred.updated_at AS document_updated_at, 
    //cred.creditmemo_id AS document_id, 
	//cred.created_at AS invoice_date, 
	//cred.updated_at AS updated_at,
	//oh.order_id AS order_id,
	//oh.order_no AS order_no,
	//oh.created_at AS order_date,
	
	//oh.customer_id AS customer_id,
	//oh.customer_email AS customer_email,
	//oh.customer_firstname AS customer_firstname,
	//oh.customer_lastname AS customer_lastname,
	//oh.customer_middlename AS customer_middlename,
	//oh.customer_prefix AS customer_prefix,
	//oh.customer_suffix AS customer_suffix,
	//oh.customer_taxvat AS customer_taxvat,
	//oh.status AS `status`,	
	//oh.coupon_code AS coupon_code,
	//-1* oh.weight AS weight,
	//oh.customer_gender AS customer_gender,
	//oh.customer_dob AS customer_dob,

	//LEFT(oh.shipping_description,LOCATE(' - ',oh.shipping_description)) shipping_carrier,
	//oh.shipping_description AS shipping_method,
	//oh.warehouse_approved_time AS approved_time,
	
	//oc.length_of_time_to_verify AS length_of_time_to_verify, 
	//oc.length_of_time_for_payment AS length_of_time_for_payment,
	//oc.length_of_time_to_invoice AS length_of_time_to_invoice,
	//ic.length_of_time_invoice_to_first_shipment AS length_of_time_invoice_to_first_shipment,
	//ic.length_of_time_invoice_to_last_shipment AS length_of_time_invoice_to_last_shipment,
	//ic.length_of_time_to_first_shipment AS length_of_time_to_first_shipment,
	//ic.length_of_time_to_last_shipment AS length_of_time_to_last_shipment,

	//IFNULL(pm.payment_method_name,op.method) AS payment_method,
	//op.amount_authorized  AS local_payment_authorized,
	//op.amount_canceled AS local_payment_canceled,

	//op.cc_exp_month AS cc_exp_month,
	//op.cc_exp_year AS cc_exp_year,
	//op.cc_ss_start_month AS cc_start_month,
	//op.cc_ss_start_year AS cc_start_year,
	//op.cc_last4 AS cc_last4,
	//op.cc_status_description AS cc_status_description,
	//op.cc_owner AS cc_owner,
	//IFNULL(ctm.card_type_name,op.cc_type) AS cc_type,
	//op.cc_status AS cc_status,
	//op.cc_ss_issue AS cc_issue_no,
	//op.cc_avs_status AS cc_avs_status,

	//'' AS payment_info1,
	//'' AS payment_info2,
	//'' AS payment_info3,
	//'' AS payment_info4,
	//'' AS payment_info5,

	//bill.email AS billing_email,
	//bill.company AS billing_company,
	//bill.prefix AS billing_prefix,
	//bill.firstname AS billing_firstname,
	//bill.middlename AS billing_middlename,
	//bill.lastname AS billing_lastname,
	//bill.suffix AS billing_suffix,
	//bill.street AS billing_street,
	//bill.city AS billing_city,
	//bill.region AS billing_region,
	//bill.region_id AS billing_region_id,
	//bill.postcode AS billing_postcode,
	//bill.country_id AS billing_country_id,
	//bill.telephone AS billing_telephone,
	//bill.fax AS billing_fax,

	//ship.email AS shipping_email,
	//ship.company AS shipping_company,
	//ship.prefix AS shipping_prefix,
	//ship.firstname AS shipping_firstname,
	//ship.middlename AS shipping_middlename,
	//ship.lastname AS shipping_lastname,
	//ship.suffix AS shipping_suffix,
	//ship.street AS shipping_street,
	//ship.city AS shipping_city,
	//ship.region AS shipping_region,
	//ship.region_id AS shipping_region_id,
	//ship.postcode AS shipping_postcode,
	//ship.country_id AS shipping_country_id,
	//ship.telephone AS shipping_telephone,
	//ship.fax AS shipping_fax,

	//ov.has_lens AS has_lens,
	//-1 * ov.total_qty AS total_qty,

    //-1 * ABS(ov.prof_fee)  AS local_prof_fee ,
	//-1 * ABS(ov.local_subtotal_inc_vat)  AS local_subtotal_inc_vat ,
	//-1 * ABS(ov.local_subtotal_vat)  AS local_subtotal_vat ,
	//-1 * ABS(ov.local_subtotal_exc_vat)  AS local_subtotal_exc_vat ,
	//-1 * ABS(ov.local_shipping_inc_vat)  AS local_shipping_inc_vat ,
	//-1 * ABS(ov.local_shipping_vat)  AS local_shipping_vat ,
	//-1 * ABS(ov.local_shipping_exc_vat)  AS local_shipping_exc_vat ,
	//ABS(ov.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//ABS(ov.local_discount_vat)  AS local_discount_vat ,
	//ABS(ov.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//ABS(ov.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//ABS(ov.local_store_credit_vat)  AS local_store_credit_vat ,
	//ABS(ov.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//-1 * ABS(ov.local_adjustment_inc_vat)  AS local_adjustment_inc_vat ,
	//-1 * ABS(ov.local_adjustment_vat)  AS local_adjustment_vat ,
	//-1 * ABS(ov.local_adjustment_exc_vat)  AS local_adjustment_exc_vat ,
	//-1 * ABS(ov.local_total_inc_vat)  AS local_total_inc_vat ,
	//-1 * ABS(ov.local_total_vat)  AS local_total_vat ,
	//-1 * ABS(ov.local_total_exc_vat)  AS local_total_exc_vat ,
	//-1 * ABS(ov.local_subtotal_cost)  AS local_subtotal_cost ,
	//-1 * ABS(ov.local_shipping_cost)  AS local_shipping_cost ,
	//-1 * ABS(ov.local_total_cost)  AS local_total_cost ,
	//-1 * ABS(ov.local_margin_amount)  AS local_margin_amount ,
	//ov.local_margin_percent  AS local_margin_percent ,
	//oh.base_to_global_rate  AS local_to_global_rate ,
	//oh.order_currency_code AS order_currency_code,

	//-1 * ABS(ov.global_prof_fee)  AS global_prof_fee ,
	//-1 * ABS(ov.global_subtotal_inc_vat)  AS global_subtotal_inc_vat ,
	//-1 * ABS(ov.global_subtotal_vat)  AS global_subtotal_vat ,
	//-1 * ABS(ov.global_subtotal_exc_vat)  AS global_subtotal_exc_vat ,
	//-1 * ABS(ov.global_shipping_inc_vat)  AS global_shipping_inc_vat ,
	//-1 * ABS(ov.global_shipping_vat)  AS global_shipping_vat ,
	//-1 * ABS(ov.global_shipping_exc_vat)  AS global_shipping_exc_vat ,
	//ABS(ov.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//ABS(ov.global_discount_vat)  AS global_discount_vat ,
	//ABS(ov.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//ABS(ov.global_store_credit_inc_vat) AS global_store_credit_inc_vat ,
	//ABS(ov.global_store_credit_vat)  AS global_store_credit_vat ,
	//ABS(ov.global_store_credit_exc_vat)  AS global_store_credit_exc_vat ,
	//-1 * ABS(ov.global_adjustment_inc_vat)  AS global_adjustment_inc_vat ,
	//-1 * ABS(ov.global_adjustment_vat)  AS global_adjustment_vat ,
	//-1 * ABS(ov.global_adjustment_exc_vat)  AS global_adjustment_exc_vat ,
	//-1 * ABS(ov.global_total_inc_vat)  AS global_total_inc_vat ,
	//-1 * ABS(ov.global_total_vat)  AS global_total_vat ,
	//-1 * ABS(ov.global_total_exc_vat)  AS global_total_exc_vat ,
	//-1 * ABS(ov.global_subtotal_cost)  AS global_subtotal_cost ,
	//-1 * ABS(ov.global_shipping_cost)  AS global_shipping_cost ,
	//-1 * ABS(ov.global_total_cost)  AS global_total_cost ,
	//-1 * ABS(ov.global_margin_amount) AS global_margin_amount ,
	//ov.global_margin_percent  AS global_margin_percent ,

	//ov.prof_fee_percent  AS prof_fee_percent ,
	//ov.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//ov.vat_percent  AS vat_percent ,
	//ABS(ov.discount_percent)   AS discount_percent ,
	
	//oh.customer_note AS customer_note,
	//oh.customer_note_notify AS customer_note_notify, 
	//oh.remote_ip AS remote_ip,
	//oh.affilcode AS business_source,
	//bc.channel AS business_channel,
	//oh.affilBatch AS affilBatch,
	//oh.affilCode AS affilCode,
	//oh.affilUserRef AS affilUserRef,
	//oh.postoptics_auto_verification AS auto_verification,
    //CASE WHEN oh.postoptics_source = 'user' THEN 'web' ELSE  oh.postoptics_source  END As order_type,
    //ohm.ORDER_LIFECYCle AS order_lifecycle,
	//oh.reminder_date AS reminder_date,
	//oh.reminder_mobile AS reminder_mobile,
	//oh.reminder_period AS reminder_period,
	//oh.reminder_presc AS reminder_presc,
	//oh.reminder_type AS reminder_type,
	//oh.reminder_sent   AS reminder_sent  ,
	//oh.reminder_follow_sent   AS reminder_follow_sent  ,
	//oh.telesales_method_code AS telesales_method_code,
	//oh.telesales_admin_username AS telesales_admin_username,
	//oh.reorder_on_flag   AS reorder_on_flag  ,
	//oh.reorder_profile_id AS reorder_profile_id,
	//rp.next_order_date  AS reorder_date ,
	//rp.interval_days AS reorder_interval,
	//rqp.cc_exp_month AS reorder_cc_exp_month,
	//rqp.cc_exp_year AS reorder_cc_exp_year,
	//oh.automatic_reorder   AS automatic_reorder  ,
	//oh.presc_verification_method AS presc_verification_method,
	//oh.referafriend_code AS referafriend_code,
	//oh.referafriend_referer AS referafriend_referer

//FROM	
	//dw_creditmemo_headers cred
				//INNER JOIN dw_order_headers oh 	ON oh.order_id = cred.order_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
				//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
				//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
				//INNER JOIN creditmemo_headers_vat ov ON ov.creditmemo_id = cred.creditmemo_id
				//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
				//LEFT JOIN dw_invoice_calender_dates ic ON ic.invoice_id = cred.invoice_id
				//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
                //LEFT JOIN dw_invoice_headers inv ON inv.invoice_id = cred.invoice_id
                //LEFT JOIN dw_payment_method_map pm ON pm.payment_method = op.method
                //LEFT JOIN dw_card_type_map ctm ON ctm.card_type = op.cc_type
                //LEFT JOIN po_reorder_quote rq ON rq.entity_id = rp.reorder_quote_id  
                //LEFT JOIN po_reorder_quote_payment rqp ON rqp.quote_id =  rq.entity_id
                //LEFT JOIN dw_bis_source_map bc ON bc.source_code = oh.affilcode
                //LEFT JOIN dw_order_headers_marketing ohm ON ohm.order_id = oh.order_id;");
                
                
//$installer->run("CREATE OR REPLACE VIEW vw_invoice_lines_invoice AS 
//SELECT 
	//ol.item_id AS line_id,
	//ol.invoice_id AS invoice_id,
	//cw.store_name AS store_name,
	//inv.invoice_no AS invoice_no,
	//CAST('INVOICE' AS CHAR(25)) AS document_type,
    //inv.created_at AS document_date,
    //inv.updated_at AS document_updated_at,
    //inv.invoice_id AS document_id,
	//inv.created_at AS invoice_date,
	//inv.updated_at AS updated_at,

	//ol.product_id AS product_id,
	//cf.product_type AS product_type,
	//ol2.product_options AS product_options,

	//p.is_lens  AS is_lens ,
	//'' AS lens_eye,
	//esi.BC AS lens_base_curve,
	//esi.DI  AS lens_diameter ,
	//esi.PO  AS lens_power ,
	//esi.CY  AS lens_cylinder ,
	//esi.AX  AS lens_axis ,
	//esi.AD  AS lens_addition ,
	//esi.DO AS lens_dominance,
	//esi.CO AS lens_colour,
	//p.daysperlens AS lens_days,

	//'' AS product_size,
	//'' AS product_colour,

	//ol2.weight  AS unit_weight ,
	//ol2.weight * ol.qty AS line_weight ,
	//ol2.is_virtual AS is_virtual,
	//ol.sku AS sku,
	//ol.name AS `name`,
	//pcf.category_id AS category_id,
    //CASE WHEN oh.base_shipping_amount > 0 THEN 1 ELSE 0 END AS free_shipping ,
	//CASE WHEN oh.base_discount_amount > 0 THEN 1 ELSE 0 END AS no_discount ,
	
	//ol.qty AS qty,
	
	//olv.local_prof_fee  AS local_prof_fee ,
	//olv.local_price_inc_vat  AS local_price_inc_vat ,
	//olv.local_price_vat  AS local_price_vat ,
	//olv.local_price_exc_vat  AS local_price_exc_vat ,
	//olv.local_line_subtotal_inc_vat  AS local_line_subtotal_inc_vat ,
	//olv.local_line_subtotal_vat  AS local_line_subtotal_vat ,
	//olv.local_line_subtotal_exc_vat  AS local_line_subtotal_exc_vat ,
	//olv.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//olv.local_shipping_vat  AS local_shipping_vat ,
	//olv.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//-1 * ABS(olv.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//-1 * ABS(olv.local_discount_vat)  AS local_discount_vat ,
	//-1 * ABS(olv.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//-1 * ABS(olv.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//-1 * ABS(olv.local_store_credit_vat)  AS local_store_credit_vat ,
	//-1 * ABS(olv.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//olv.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//olv.local_adjustment_vat  AS local_adjustment_vat ,
	//olv.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//olv.local_line_total_inc_vat  AS local_line_total_inc_vat ,
	//olv.local_line_total_vat  AS local_line_total_vat ,
	//olv.local_line_total_exc_vat  AS local_line_total_exc_vat ,
	//olv.local_subtotal_cost  AS local_subtotal_cost ,
	//olv.local_shipping_cost  AS local_shipping_cost ,
	//olv.local_total_cost  AS local_total_cost ,
	//olv.local_margin_amount  AS local_margin_amount ,
	//olv.local_margin_percent  AS local_margin_percent ,
	//olv.local_to_global_rate  AS local_to_global_rate ,
	//olv.order_currency_code AS order_currency_code,

	//olv.global_prof_fee  AS global_prof_fee ,
	//olv.global_price_inc_vat  AS global_price_inc_vat ,
	//olv.global_price_vat  AS global_price_vat ,
	//olv.global_price_exc_vat  AS global_price_exc_vat ,
	//olv.global_line_subtotal_inc_vat  AS global_line_subtotal_inc_vat ,
	//olv.global_line_subtotal_vat  AS global_line_subtotal_vat ,
	//olv.global_line_subtotal_exc_vat  AS global_line_subtotal_exc_vat ,
	//olv.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//olv.global_shipping_vat  AS global_shipping_vat ,
	//olv.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//-1 * ABS(olv.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//-1 * ABS(olv.global_discount_vat)  AS global_discount_vat ,
	//-1 * ABS(olv.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//-1 * ABS(olv.global_store_credit_inc_vat)  AS global_store_credit_inc_vat ,
	//-1 * ABS(olv.global_store_credit_vat)  AS global_store_credit_vat ,
	//-1 * ABS(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat ,
	//olv.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//olv.global_adjustment_vat  AS global_adjustment_vat ,
	//olv.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//olv.global_line_total_inc_vat  AS global_line_total_inc_vat ,
	//olv.global_line_total_vat  AS global_line_total_vat ,
	//olv.global_line_total_exc_vat  AS global_line_total_exc_vat ,
	//olv.global_subtotal_cost  AS global_subtotal_cost ,
	//olv.global_shipping_cost  AS global_shipping_cost ,
	//olv.global_total_cost  AS global_total_cost ,
	//olv.global_margin_amount  AS global_margin_amount ,
	//olv.global_margin_percent  AS global_margin_percent ,

	//olv.prof_fee_percent  AS prof_fee_percent ,
	//olv.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//olv.vat_percent  AS vat_percent ,
	//ABS(olv.discount_percent)  AS discount_percent 
//FROM	
	//dw_invoice_lines ol	INNER JOIN dw_invoice_headers inv ON inv.invoice_id = ol.invoice_id
				//INNER JOIN dw_order_headers oh 	ON oh.order_id = inv.order_id
				//INNER JOIN invoice_lines_vat olv ON olv.item_id = ol.item_id
				//LEFT JOIN dw_order_lines ol2 ON ol2.item_id = ol.order_line_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
				//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
				//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
				//INNER JOIN invoice_headers_vat ov ON ov.invoice_id = inv.invoice_id
				//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
				//LEFT JOIN dw_invoice_calender_dates ic ON ic.invoice_id = inv.invoice_id
				//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
				//LEFT JOIN dw_products p ON p.product_id = ol.product_id
				//LEFT JOIN edi_stock_item_agg esi ON esi.product_id = ol.product_id AND esi.product_code = ol.sku
                //LEFT JOIN dw_product_category_flat pcf ON pcf.product_id = ol.product_id
                //LEFT JOIN dw_flat_category cf ON cf.category_id = pcf.category_id;");
				
	
//$installer->run("CREATE OR REPLACE VIEW vw_invoice_lines_creditmemo AS
//SELECT 
	//ol.item_id AS line_id,
	//cred.creditmemo_id AS invoice_id,
	//cw.store_name AS store_name,
	//cred.creditmemo_no AS invoice_no,
	//CAST('CREDITMEMO' AS CHAR(25)) AS document_type,
	//cred.created_at AS document_date,
	//cred.updated_at AS document_updated_at,
	//cred.creditmemo_id AS document_id,
	//cred.created_at AS invoice_date,
	//cred.updated_at AS updated_at,

	//ol.product_id AS product_id,
	//cf.product_type AS product_type,
	//ol2.product_options AS product_options,

	//p.is_lens  AS is_lens ,
	//'' AS lens_eye,
	//esi.BC AS lens_base_curve,
	//esi.DI  AS lens_diameter ,
	//esi.PO  AS lens_power ,
	//esi.CY  AS lens_cylinder ,
	//esi.AX  AS lens_axis ,
	//esi.AD  AS lens_addition ,
	//esi.DO AS lens_dominance,
	//esi.CO AS lens_colour,
	//p.daysperlens AS lens_days,

	//'' AS product_size,
	//'' AS product_colour,

	//-1 * ol2.weight  AS unit_weight ,
    //-1 * ol2.weight * ol.qty AS line_weight ,
	//ol2.is_virtual AS is_virtual,
	//ol.sku AS sku,
	//ol.name AS `name`,
	//pcf.category_id AS category_id,
    //CASE WHEN oh.base_shipping_amount > 0 THEN 1 ELSE 0 END AS free_shipping ,
	//CASE WHEN oh.base_discount_amount > 0 THEN 1 ELSE 0 END AS no_discount ,
	
	//-1 * ol.qty AS qty,
	
	//-1 * olv.local_prof_fee  AS local_prof_fee ,
	//olv.local_price_inc_vat  AS local_price_inc_vat ,
	//olv.local_price_vat  AS local_price_vat ,
	//olv.local_price_exc_vat  AS local_price_exc_vat ,
	//-1 * olv.local_line_subtotal_inc_vat  AS local_line_subtotal_inc_vat ,
	//-1 * olv.local_line_subtotal_vat  AS local_line_subtotal_vat ,
	//-1 * olv.local_line_subtotal_exc_vat  AS local_line_subtotal_exc_vat ,
	//-1 * olv.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//-1 * olv.local_shipping_vat  AS local_shipping_vat ,
	//-1 * olv.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//ABS(olv.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//ABS(olv.local_discount_vat)  AS local_discount_vat ,
	//ABS(olv.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//ABS(olv.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//ABS(olv.local_store_credit_vat)  AS local_store_credit_vat ,
	//ABS(olv.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//-1 * olv.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//-1 * olv.local_adjustment_vat  AS local_adjustment_vat ,
	//-1 * olv.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//-1 * olv.local_line_total_inc_vat  AS local_line_total_inc_vat ,
	//-1 * olv.local_line_total_vat  AS local_line_total_vat ,
	//-1 * olv.local_line_total_exc_vat  AS local_line_total_exc_vat ,
	//-1 * olv.local_subtotal_cost  AS local_subtotal_cost ,
	//-1 * olv.local_shipping_cost  AS local_shipping_cost ,
	//-1 * olv.local_total_cost  AS local_total_cost ,
	//-1 * olv.local_margin_amount  AS local_margin_amount ,
	//olv.local_margin_percent  AS local_margin_percent ,
	//olv.local_to_global_rate  AS local_to_global_rate ,
	//olv.order_currency_code AS order_currency_code,

	//-1 * olv.global_prof_fee  AS global_prof_fee ,
	//olv.global_price_inc_vat  AS global_price_inc_vat ,
	//olv.global_price_vat  AS global_price_vat ,
	//olv.global_price_exc_vat  AS global_price_exc_vat ,
	//-1 * olv.global_line_subtotal_inc_vat  AS global_line_subtotal_inc_vat ,
	//-1 * olv.global_line_subtotal_vat  AS global_line_subtotal_vat ,
	//-1 * olv.global_line_subtotal_exc_vat  AS global_line_subtotal_exc_vat ,
	//-1 * olv.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//-1 * olv.global_shipping_vat  AS global_shipping_vat ,
	//-1 * olv.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//ABS(olv.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//ABS(olv.global_discount_vat)  AS global_discount_vat ,
	//ABS(olv.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//ABS(olv.global_store_credit_inc_vat)  AS global_store_credit_inc_vat ,
	//ABS(olv.global_store_credit_vat)  AS global_store_credit_vat ,
	//ABS(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat ,
	//-1 * olv.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//-1 * olv.global_adjustment_vat  AS global_adjustment_vat ,
	//-1 * olv.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//-1 * olv.global_line_total_inc_vat  AS global_line_total_inc_vat ,
	//-1 * olv.global_line_total_vat  AS global_line_total_vat ,
	//-1 * olv.global_line_total_exc_vat  AS global_line_total_exc_vat ,
	//-1 * olv.global_subtotal_cost  AS global_subtotal_cost ,
	//-1 * olv.global_shipping_cost  AS global_shipping_cost ,
	//-1 * olv.global_total_cost  AS global_total_cost ,
	//-1 * olv.global_margin_amount  AS global_margin_amount ,
	//olv.global_margin_percent  AS global_margin_percent ,

	//olv.prof_fee_percent  AS prof_fee_percent ,
	//olv.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//olv.vat_percent  AS vat_percent ,
	//ABS(olv.discount_percent)  AS discount_percent 
	
//FROM 	dw_creditmemo_lines ol 		INNER JOIN dw_creditmemo_headers cred ON cred.creditmemo_id = ol.creditmemo_id			
					//INNER JOIN dw_order_headers oh 	ON oh.order_id = cred.order_id
					//LEFT JOIN dw_order_lines ol2 ON ol2.item_id = ol.order_line_id
					//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
					//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
					//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
					//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
					//INNER JOIN creditmemo_lines_vat olv ON olv.item_id = ol.item_id
					//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
					//LEFT JOIN dw_invoice_calender_dates ic ON ic.invoice_id = cred.invoice_id
					//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
					//LEFT JOIN dw_products p ON p.product_id = ol.product_id
					//LEFT JOIN edi_stock_item_agg esi ON esi.product_id = ol.product_id AND esi.product_code = ol.sku
                    //LEFT JOIN dw_invoice_headers inv ON inv.invoice_id = cred.invoice_id
                    //LEFT JOIN dw_product_category_flat pcf ON pcf.product_id = ol.product_id
                    //LEFT JOIN dw_flat_category cf ON cf.category_id = pcf.category_id;");


//$installer->run("CREATE OR REPLACE VIEW vw_order_headers_order AS
//SELECT 
	//cw.store_name AS store_name,
	//oh.order_id AS order_id,
	//oh.order_no AS order_no,
	//CAST('ORDER' AS CHAR(25)) AS document_type,
	//oh.created_at AS document_date, 
	//oh.updated_at AS document_updated_at,
	//oh.order_id AS document_id,
	//oh.created_at AS order_date, 
	//oh.updated_at AS updated_at,
	//oh.customer_id AS customer_id,
	//oh.customer_email AS customer_email,
	//oh.customer_firstname AS customer_firstname,
	//oh.customer_lastname AS customer_lastname,
	//oh.customer_middlename AS customer_middlename,
	//oh.customer_prefix AS customer_prefix,
	//oh.customer_suffix AS customer_suffix,
	//oh.customer_taxvat AS customer_taxvat,
	//oh.status AS `status`,	
	//oh.coupon_code AS coupon_code,
	//oh.weight AS weight,
	//oh.customer_gender AS customer_gender,
	//oh.customer_dob AS customer_dob,

	//LEFT(oh.shipping_description,LOCATE(' - ',oh.shipping_description)) shipping_carrier,
	//oh.shipping_description AS shipping_method,
	//oh.warehouse_approved_time AS approved_time,
	
	//oc.length_of_time_to_verify AS length_of_time_to_verify, 
	//oc.length_of_time_for_payment AS length_of_time_for_payment,
	//oc.length_of_time_to_invoice AS length_of_time_to_invoice,
	//oc.length_of_time_to_cancel AS length_of_time_to_cancel,
	

	//IFNULL(pm.payment_method_name,op.method) AS payment_method,
	//op.amount_authorized  AS local_payment_authorized,
	//op.amount_canceled AS local_payment_canceled,

	//op.cc_exp_month AS cc_exp_month,
	//op.cc_exp_year AS cc_exp_year,
	//op.cc_ss_start_month AS cc_start_month,
	//op.cc_ss_start_year AS cc_start_year,
	//op.cc_last4 AS cc_last4,
	//op.cc_status_description AS cc_status_description,
	//op.cc_owner AS cc_owner,
	//IFNULL(ctm.card_type_name,op.cc_type) AS cc_type,
	//op.cc_status AS cc_status,
	//op.cc_ss_issue AS cc_issue_no,
	//op.cc_avs_status AS cc_avs_status,

	//'' AS payment_info1,
	//'' AS payment_info2,
	//'' AS payment_info3,
	//'' AS payment_info4,
	//'' AS payment_info5,

	//bill.email AS billing_email,
	//bill.company AS billing_company,
	//bill.prefix AS billing_prefix,
	//bill.firstname AS billing_firstname,
	//bill.middlename AS billing_middlename,
	//bill.lastname AS billing_lastname,
	//bill.suffix AS billing_suffix,
	//bill.street AS billing_street,
	//bill.city AS billing_city,
	//bill.region AS billing_region,
	//bill.region_id AS billing_region_id,
	//bill.postcode AS billing_postcode,
	//bill.country_id AS billing_country_id,
	//bill.telephone AS billing_telephone,
	//bill.fax AS billing_fax,

	//ship.email AS shipping_email,
	//ship.company AS shipping_company,
	//ship.prefix AS shipping_prefix,
	//ship.firstname AS shipping_firstname,
	//ship.middlename AS shipping_middlename,
	//ship.lastname AS shipping_lastname,
	//ship.suffix AS shipping_suffix,
	//ship.street AS shipping_street,
	//ship.city AS shipping_city,
	//ship.region AS shipping_region,
	//ship.region_id AS shipping_region_id,
	//ship.postcode AS shipping_postcode,
	//ship.country_id AS shipping_country_id,
	//ship.telephone AS shipping_telephone,
	//ship.fax AS shipping_fax,

	//ov.has_lens AS has_lens,
	//oh.total_qty_ordered AS total_qty,

	//ov.prof_fee  AS local_prof_fee ,
	//ov.local_subtotal_inc_vat  AS local_subtotal_inc_vat ,
	//ov.local_subtotal_vat  AS local_subtotal_vat ,
	//ov.local_subtotal_exc_vat  AS local_subtotal_exc_vat ,
	//ov.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//ov.local_shipping_vat  AS local_shipping_vat ,
	//ov.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//-1 * ABS(ov.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//-1 * ABS(ov.local_discount_vat)  AS local_discount_vat ,
	//-1 * ABS(ov.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//-1 * ABS(ov.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//-1 * ABS(ov.local_store_credit_vat)  AS local_store_credit_vat ,
	//-1 * ABS(ov.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//ov.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//ov.local_adjustment_vat  AS local_adjustment_vat ,
	//ov.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//ov.local_total_inc_vat  AS local_total_inc_vat ,
	//ov.local_total_vat  AS local_total_vat ,
	//ov.local_total_exc_vat  AS local_total_exc_vat ,
	//ov.local_subtotal_cost  AS local_subtotal_cost ,
	//ov.local_shipping_cost  AS local_shipping_cost ,
	//ov.local_total_cost  AS local_total_cost ,
	//ov.local_margin_amount  AS local_margin_amount ,
	//ov.local_margin_percent  AS local_margin_percent ,
	//oh.base_to_global_rate  AS local_to_global_rate ,
	//oh.order_currency_code AS order_currency_code,

	//ov.global_prof_fee  AS global_prof_fee ,
	//ov.global_subtotal_inc_vat  AS global_subtotal_inc_vat ,
	//ov.global_subtotal_vat  AS global_subtotal_vat ,
	//ov.global_subtotal_exc_vat  AS global_subtotal_exc_vat ,
	//ov.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//ov.global_shipping_vat  AS global_shipping_vat ,
	//ov.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//-1 * ABS(ov.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//-1 * ABS(ov.global_discount_vat)  AS global_discount_vat ,
	//-1 * ABS(ov.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//-1 * ABS(ov.global_store_credit_inc_vat) AS global_store_credit_inc_vat ,
	//-1 * ABS(ov.global_store_credit_vat)  AS global_store_credit_vat ,
	//-1 * ABS(ov.global_store_credit_exc_vat)  AS global_store_credit_exc_vat ,
	//ov.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//ov.global_adjustment_vat  AS global_adjustment_vat ,
	//ov.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//ov.global_total_inc_vat  AS global_total_inc_vat ,
	//ov.global_total_vat  AS global_total_vat ,
	//ov.global_total_exc_vat  AS global_total_exc_vat ,
	//ov.global_subtotal_cost  AS global_subtotal_cost ,
	//ov.global_shipping_cost  AS global_shipping_cost ,
	//ov.global_total_cost  AS global_total_cost ,
	//ov.global_margin_amount  AS global_margin_amount ,
	//ov.global_margin_percent  AS global_margin_percent ,

	//ov.prof_fee_percent  AS prof_fee_percent ,
	//ov.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//ov.vat_percent  AS vat_percent ,
	//ABS(ov.discount_percent) AS discount_percent ,
	
	//oh.customer_note AS customer_note,
	//oh.customer_note_notify AS customer_note_notify, 
	//oh.remote_ip AS remote_ip,
	//oh.affilcode AS business_source,
	//bc.channel AS business_channel,
	//oh.affilBatch AS affilBatch,
	//oh.affilCode AS affilCode,
	//oh.affilUserRef AS affilUserRef,
	//oh.postoptics_auto_verification AS auto_verification,
    //CASE WHEN oh.postoptics_source = 'user' THEN 'web' ELSE  oh.postoptics_source  END As order_type,
    //ohm.ORDER_LIFECYCle AS order_lifecycle,
	//oh.reminder_date AS reminder_date,
	//oh.reminder_mobile AS reminder_mobile,
	//oh.reminder_period AS reminder_period,
	//oh.reminder_presc AS reminder_presc,
	//oh.reminder_type AS reminder_type,
	//oh.reminder_sent   AS reminder_sent  ,
	//oh.reminder_follow_sent   AS reminder_follow_sent  ,
	//oh.telesales_method_code AS telesales_method_code,
	//oh.telesales_admin_username AS telesales_admin_username,
	//oh.reorder_on_flag   AS reorder_on_flag  ,
	//oh.reorder_profile_id AS reorder_profile_id,
	//rp.next_order_date  AS reorder_date ,
	//rp.interval_days AS reorder_interval,
	//rqp.cc_exp_month AS reorder_cc_exp_month,
    //rqp.cc_exp_year AS reorder_cc_exp_year,
	//oh.automatic_reorder   AS automatic_reorder  ,
	//oh.presc_verification_method AS presc_verification_method,
	//oh.referafriend_code AS referafriend_code,
	//oh.referafriend_referer AS referafriend_referer

//FROM	
	//dw_order_headers oh 	LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
				//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
				//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
				//INNER JOIN order_headers_vat ov ON ov.order_id = oh.order_id
				//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
				//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
                //LEFT JOIN dw_payment_method_map pm ON pm.payment_method = op.method
                //LEFT JOIN dw_card_type_map ctm ON ctm.card_type = op.cc_type
                //LEFT JOIN po_reorder_quote rq ON rq.entity_id = rp.reorder_quote_id  
                //LEFT JOIN po_reorder_quote_payment rqp ON rqp.quote_id =  rq.entity_id
                //LEFT JOIN dw_bis_source_map bc ON bc.source_code = oh.affilcode
                //LEFT JOIN dw_order_headers_marketing ohm ON ohm.order_id = oh.order_id
//WHERE oh.status != 'archived';");

//$installer->run("CREATE OR REPLACE VIEW vw_order_headers_cancel AS
//SELECT 
	//cw.store_name AS store_name,
	//oh.order_id AS order_id,
	//oh.order_no AS order_no,
	//'CANCEL' AS document_type,
	//os.status_time AS document_date, 
	//oh.updated_at AS document_updated_at, 
	//oh.order_id AS document_id, 
	//oh.created_at AS order_date, 
	//oh.updated_at AS updated_at,
	//oh.customer_id AS customer_id,
	//oh.customer_email AS customer_email,
	//oh.customer_firstname AS customer_firstname,
	//oh.customer_lastname AS customer_lastname,
	//oh.customer_middlename AS customer_middlename,
	//oh.customer_prefix AS customer_prefix,
	//oh.customer_suffix AS customer_suffix,
	//oh.customer_taxvat AS customer_taxvat,
	//oh.status AS `status`,	
	//oh.coupon_code AS coupon_code,
	//-1 * oh.weight AS weight,
	//oh.customer_gender AS customer_gender,
	//oh.customer_dob AS customer_dob,

	//LEFT(oh.shipping_description,LOCATE(' - ',oh.shipping_description)) shipping_carrier,
	//oh.shipping_description AS shipping_method,
	//oh.warehouse_approved_time AS approved_time,
	
	//oc.length_of_time_to_verify AS length_of_time_to_verify, 
	//oc.length_of_time_for_payment AS length_of_time_for_payment,
	//oc.length_of_time_to_invoice AS length_of_time_to_invoice,
	//oc.length_of_time_to_cancel AS length_of_time_to_cancel,
	

	//IFNULL(pm.payment_method_name,op.method) AS payment_method,
	//op.amount_authorized  AS local_payment_authorized,
	//op.amount_canceled AS local_payment_canceled,

	//op.cc_exp_month AS cc_exp_month,
	//op.cc_exp_year AS cc_exp_year,
	//op.cc_ss_start_month AS cc_start_month,
	//op.cc_ss_start_year AS cc_start_year,
	//op.cc_last4 AS cc_last4,
	//op.cc_status_description AS cc_status_description,
	//op.cc_owner AS cc_owner,
	//IFNULL(ctm.card_type_name,op.cc_type) AS cc_type,
	//op.cc_status AS cc_status,
	//op.cc_ss_issue AS cc_issue_no,
	//op.cc_avs_status AS cc_avs_status,

	//'' AS payment_info1,
	//'' AS payment_info2,
	//'' AS payment_info3,
	//'' AS payment_info4,
	//'' AS payment_info5,

	//bill.email AS billing_email,
	//bill.company AS billing_company,
	//bill.prefix AS billing_prefix,
	//bill.firstname AS billing_firstname,
	//bill.middlename AS billing_middlename,
	//bill.lastname AS billing_lastname,
	//bill.suffix AS billing_suffix,
	//bill.street AS billing_street,
	//bill.city AS billing_city,
	//bill.region AS billing_region,
	//bill.region_id AS billing_region_id,
	//bill.postcode AS billing_postcode,
	//bill.country_id AS billing_country_id,
	//bill.telephone AS billing_telephone,
	//bill.fax AS billing_fax,

	//ship.email AS shipping_email,
	//ship.company AS shipping_company,
	//ship.prefix AS shipping_prefix,
	//ship.firstname AS shipping_firstname,
	//ship.middlename AS shipping_middlename,
	//ship.lastname AS shipping_lastname,
	//ship.suffix AS shipping_suffix,
	//ship.street AS shipping_street,
	//ship.city AS shipping_city,
	//ship.region AS shipping_region,
	//ship.region_id AS shipping_region_id,
	//ship.postcode AS shipping_postcode,
	//ship.country_id AS shipping_country_id,
	//ship.telephone AS shipping_telephone,
	//ship.fax AS shipping_fax,

	//ov.has_lens AS has_lens,
	//-1 * oh.total_qty_ordered AS total_qty,

	//-1 * ov.prof_fee  AS local_prof_fee ,
	//-1 * ov.local_subtotal_inc_vat  AS local_subtotal_inc_vat ,
	//-1 * ov.local_subtotal_vat  AS local_subtotal_vat ,
	//-1 * ov.local_subtotal_exc_vat  AS local_subtotal_exc_vat ,
	//-1 * ov.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//-1 * ov.local_shipping_vat  AS local_shipping_vat ,
	//-1 * ov.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//ABS(ov.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//ABS(ov.local_discount_vat)  AS local_discount_vat ,
	//ABS(ov.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//ABS(ov.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//ABS(ov.local_store_credit_vat)  AS local_store_credit_vat ,
	//ABS(ov.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//-1 * ov.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//-1 * ov.local_adjustment_vat  AS local_adjustment_vat ,
	//-1 * ov.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//-1 * ov.local_total_inc_vat  AS local_total_inc_vat ,
	//-1 * ov.local_total_vat  AS local_total_vat ,
	//-1 * ov.local_total_exc_vat  AS local_total_exc_vat ,
	//-1 * ov.local_subtotal_cost  AS local_subtotal_cost ,
	//-1 * ov.local_shipping_cost  AS local_shipping_cost ,
	//-1 * ov.local_total_cost  AS local_total_cost ,
	//-1 * ov.local_margin_amount  AS local_margin_amount ,
	//ov.local_margin_percent  AS local_margin_percent ,
	//oh.base_to_global_rate  AS local_to_global_rate ,
	//oh.order_currency_code AS order_currency_code,

	//-1 * ov.global_prof_fee  AS global_prof_fee ,
	//-1 * ov.global_subtotal_inc_vat  AS global_subtotal_inc_vat ,
	//-1 * ov.global_subtotal_vat  AS global_subtotal_vat ,
	//-1 * ov.global_subtotal_exc_vat  AS global_subtotal_exc_vat ,
	//-1 * ov.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//-1 * ov.global_shipping_vat  AS global_shipping_vat ,
	//-1 * ov.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//ABS(ov.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//ABS(ov.global_discount_vat)  AS global_discount_vat ,
	//ABS(ov.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//ABS(ov.global_store_credit_inc_vat) AS global_store_credit_inc_vat ,
	//ABS(ov.global_store_credit_vat)  AS global_store_credit_vat ,
	//ABS(ov.global_store_credit_exc_vat)  AS global_store_credit_exc_vat ,
	//-1 * ov.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//-1 * ov.global_adjustment_vat  AS global_adjustment_vat ,
	//-1 * ov.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//-1 * ov.global_total_inc_vat  AS global_total_inc_vat ,
	//-1 * ov.global_total_vat  AS global_total_vat ,
	//-1 * ov.global_total_exc_vat  AS global_total_exc_vat ,
	//-1 * ov.global_subtotal_cost  AS global_subtotal_cost ,
	//-1 * ov.global_shipping_cost  AS global_shipping_cost ,
	//-1 * ov.global_total_cost  AS global_total_cost ,
	//-1 * ov.global_margin_amount  AS global_margin_amount ,
	//ov.global_margin_percent  AS global_margin_percent ,

	//ov.prof_fee_percent  AS prof_fee_percent ,
	//ov.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//ov.vat_percent  AS vat_percent ,
	//ABS(ov.discount_percent)  AS discount_percent ,
	
	//oh.customer_note AS customer_note,
	//oh.customer_note_notify AS customer_note_notify, 
	//oh.remote_ip AS remote_ip,
	//oh.affilcode AS business_source,
	//bc.channel AS business_channel,
	//oh.affilBatch AS affilBatch,
	//oh.affilCode AS affilCode,
	//oh.affilUserRef AS affilUserRef,
	//oh.postoptics_auto_verification AS auto_verification,
    //CASE WHEN oh.postoptics_source = 'user' THEN 'web' ELSE  oh.postoptics_source  END As order_type,
    //ohm.ORDER_LIFECYCle AS order_lifecycle,
	//oh.reminder_date AS reminder_date,
	//oh.reminder_mobile AS reminder_mobile,
	//oh.reminder_period AS reminder_period,
	//oh.reminder_presc AS reminder_presc,
	//oh.reminder_type AS reminder_type,
	//oh.reminder_sent   AS reminder_sent  ,
	//oh.reminder_follow_sent   AS reminder_follow_sent  ,
	//oh.telesales_method_code AS telesales_method_code,
	//oh.telesales_admin_username AS telesales_admin_username,
	//oh.reorder_on_flag   AS reorder_on_flag  ,
	//oh.reorder_profile_id AS reorder_profile_id,
	//rp.next_order_date  AS reorder_date ,
	//rp.interval_days AS reorder_interval,
	//rqp.cc_exp_month AS reorder_cc_exp_month,
	//rqp.cc_exp_year AS reorder_cc_exp_year,
	//oh.automatic_reorder   AS automatic_reorder  ,
	//oh.presc_verification_method AS presc_verification_method,
	//oh.referafriend_code AS referafriend_code,
	//oh.referafriend_referer AS referafriend_referer
//FROM	
	//dw_order_headers oh 	LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
				//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
				//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
				//INNER JOIN order_headers_vat ov ON ov.order_id = oh.order_id
				//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
				//LEFT JOIN order_status_history_agg os ON os.order_id = oh.order_id
				//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
                //LEFT JOIN dw_payment_method_map pm ON pm.payment_method = op.method
                //LEFT JOIN dw_card_type_map ctm ON ctm.card_type = op.cc_type
                //LEFT JOIN po_reorder_quote rq ON rq.entity_id = rp.reorder_quote_id  
                //LEFT JOIN po_reorder_quote_payment rqp ON rqp.quote_id =  rq.entity_id
                //LEFT JOIN dw_bis_source_map bc ON bc.source_code = oh.affilcode
                //LEFT JOIN dw_order_headers_marketing ohm ON ohm.order_id = oh.order_id
//WHERE 
	//oh.status = 'canceled' AND os.status = 'canceled';");
    
//$installer->run("CREATE OR REPLACE VIEW vw_order_headers_creditmemo AS
//SELECT 
	//cw.store_name AS store_name,
	//oh2.order_id AS order_id,
	//oh.order_no AS order_no,
	//'CREDITMEMO' AS document_type,
	//oh2.created_at AS document_date, 
	//oh2.updated_at AS document_updated_at, 
	//oh2.creditmemo_id AS document_id, 
	//oh.created_at AS order_date,
	//oh.updated_at AS updated_at,
	//oh.customer_id AS customer_id,
	//oh.customer_email AS customer_email,
	//oh.customer_firstname AS customer_firstname,
	//oh.customer_lastname AS customer_lastname,
	//oh.customer_middlename AS customer_middlename,
	//oh.customer_prefix AS customer_prefix,
	//oh.customer_suffix AS customer_suffix,
	//oh.customer_taxvat AS customer_taxvat,
	//oh.status AS `status`,	
	//oh.coupon_code AS coupon_code,
	//-1 * oh.weight AS weight,
	//oh.customer_gender AS customer_gender,
	//oh.customer_dob AS customer_dob,

	//LEFT(oh.shipping_description,LOCATE(' - ',oh.shipping_description)) shipping_carrier,
	//oh.shipping_description AS shipping_method,
	//oh.warehouse_approved_time AS approved_time,
	
	//oc.length_of_time_to_verify AS length_of_time_to_verify, 
	//oc.length_of_time_for_payment AS length_of_time_for_payment,
	//oc.length_of_time_to_invoice AS length_of_time_to_invoice,
	//oc.length_of_time_to_cancel AS length_of_time_to_cancel,
	

	//IFNULL(pm.payment_method_name,op.method) AS payment_method,
	//op.amount_authorized  AS local_payment_authorized,
	//op.amount_canceled AS local_payment_canceled,

	//op.cc_exp_month AS cc_exp_month,
	//op.cc_exp_year AS cc_exp_year,
	//op.cc_ss_start_month AS cc_start_month,
	//op.cc_ss_start_year AS cc_start_year,
	//op.cc_last4 AS cc_last4,
	//op.cc_status_description AS cc_status_description,
	//op.cc_owner AS cc_owner,
	//IFNULL(ctm.card_type_name,op.cc_type) AS cc_type,
	//op.cc_status AS cc_status,
	//op.cc_ss_issue AS cc_issue_no,
	//op.cc_avs_status AS cc_avs_status,

	//'' AS payment_info1,
	//'' AS payment_info2,
	//'' AS payment_info3,
	//'' AS payment_info4,
	//'' AS payment_info5,

	//bill.email AS billing_email,
	//bill.company AS billing_company,
	//bill.prefix AS billing_prefix,
	//bill.firstname AS billing_firstname,
	//bill.middlename AS billing_middlename,
	//bill.lastname AS billing_lastname,
	//bill.suffix AS billing_suffix,
	//bill.street AS billing_street,
	//bill.city AS billing_city,
	//bill.region AS billing_region,
	//bill.region_id AS billing_region_id,
	//bill.postcode AS billing_postcode,
	//bill.country_id AS billing_country_id,
	//bill.telephone AS billing_telephone,
	//bill.fax AS billing_fax,

	//ship.email AS shipping_email,
	//ship.company AS shipping_company,
	//ship.prefix AS shipping_prefix,
	//ship.firstname AS shipping_firstname,
	//ship.middlename AS shipping_middlename,
	//ship.lastname AS shipping_lastname,
	//ship.suffix AS shipping_suffix,
	//ship.street AS shipping_street,
	//ship.city AS shipping_city,
	//ship.region AS shipping_region,
	//ship.region_id AS shipping_region_id,
	//ship.postcode AS shipping_postcode,
	//ship.country_id AS shipping_country_id,
	//ship.telephone AS shipping_telephone,
	//ship.fax AS shipping_fax,

	//ov.has_lens AS has_lens,
	//-1 * ov.total_qty AS total_qty,

	//-1 * ov.prof_fee  AS local_prof_fee ,
	//-1 * ov.local_subtotal_inc_vat  AS local_subtotal_inc_vat ,
	//-1 * ov.local_subtotal_vat  AS local_subtotal_vat ,
	//-1 * ov.local_subtotal_exc_vat  AS local_subtotal_exc_vat ,
	//-1 * ov.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//-1 * ov.local_shipping_vat  AS local_shipping_vat ,
	//-1 * ov.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//ABS(ov.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//ABS(ov.local_discount_vat)  AS local_discount_vat ,
	//ABS(ov.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//ABS(ov.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//ABS(ov.local_store_credit_vat)  AS local_store_credit_vat ,
	//ABS(ov.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//-1 * ov.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//-1 * ov.local_adjustment_vat  AS local_adjustment_vat ,
	//-1 * ov.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//-1 * ov.local_total_inc_vat  AS local_total_inc_vat ,
	//-1 * ov.local_total_vat  AS local_total_vat ,
	//-1 * ov.local_total_exc_vat  AS local_total_exc_vat ,
	//-1 * ov.local_subtotal_cost  AS local_subtotal_cost ,
	//-1 * ov.local_shipping_cost  AS local_shipping_cost ,
	//-1 * ov.local_total_cost  AS local_total_cost ,
	//-1 * ov.local_margin_amount  AS local_margin_amount ,
	//ov.local_margin_percent  AS local_margin_percent ,
	//oh.base_to_global_rate  AS local_to_global_rate ,
	//oh.order_currency_code AS order_currency_code,

	//-1 * ov.global_prof_fee  AS global_prof_fee ,
	//-1 * ov.global_subtotal_inc_vat  AS global_subtotal_inc_vat ,
	//-1 * ov.global_subtotal_vat  AS global_subtotal_vat ,
	//-1 * ov.global_subtotal_exc_vat  AS global_subtotal_exc_vat ,
	//-1 * ov.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//-1 * ov.global_shipping_vat  AS global_shipping_vat ,
	//-1 * ov.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//ABS(ov.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//ABS(ov.global_discount_vat)  AS global_discount_vat ,
	//ABS(ov.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//ABS(ov.global_store_credit_inc_vat) AS global_store_credit_inc_vat ,
	//ABS(ov.global_store_credit_vat)  AS global_store_credit_vat ,
	//ABS(ov.global_store_credit_exc_vat)  AS global_store_credit_exc_vat ,
	//-1 * ov.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//-1 * ov.global_adjustment_vat  AS global_adjustment_vat ,
	//-1 * ov.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//-1 * ov.global_total_inc_vat  AS global_total_inc_vat ,
	//-1 * ov.global_total_vat  AS global_total_vat ,
	//-1 * ov.global_total_exc_vat  AS global_total_exc_vat ,
	//-1 * ov.global_subtotal_cost  AS global_subtotal_cost ,
	//-1 * ov.global_shipping_cost  AS global_shipping_cost ,
	//-1 * ov.global_total_cost  AS global_total_cost ,
	//-1 * ov.global_margin_amount  AS global_margin_amount ,
	//ov.global_margin_percent  AS global_margin_percent ,

	//ov.prof_fee_percent  AS prof_fee_percent ,
	//ov.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//ov.vat_percent  AS vat_percent ,
    //ABS(ov.discount_percent)  AS discount_percent ,
	
	//oh.customer_note AS customer_note,
	//oh.customer_note_notify AS customer_note_notify, 
	//oh.remote_ip AS remote_ip,
	//oh.affilcode AS business_source,
	//bc.channel AS business_channel,
	//oh.affilBatch AS affilBatch,
	//oh.affilCode AS affilCode,
	//oh.affilUserRef AS affilUserRef,
	//oh.postoptics_auto_verification AS auto_verification,
    //CASE WHEN oh.postoptics_source = 'user' THEN 'web' ELSE  oh.postoptics_source  END As order_type,
    //ohm.ORDER_LIFECYCle AS order_lifecycle,
	//oh.reminder_date AS reminder_date,
	//oh.reminder_mobile AS reminder_mobile,
	//oh.reminder_period AS reminder_period,
	//oh.reminder_presc AS reminder_presc,
	//oh.reminder_type AS reminder_type,
	//oh.reminder_sent   AS reminder_sent  ,
	//oh.reminder_follow_sent   AS reminder_follow_sent  ,
	//oh.telesales_method_code AS telesales_method_code,
	//oh.telesales_admin_username AS telesales_admin_username,
	//oh.reorder_on_flag   AS reorder_on_flag  ,
	//oh.reorder_profile_id AS reorder_profile_id,
	//rp.next_order_date  AS reorder_date ,
	//rp.interval_days AS reorder_interval,
	//rqp.cc_exp_month AS reorder_cc_exp_month,
	//rqp.cc_exp_year AS reorder_cc_exp_year,
	//oh.automatic_reorder   AS automatic_reorder  ,
	//oh.presc_verification_method AS presc_verification_method,
	//oh.referafriend_code AS referafriend_code,
	//oh.referafriend_referer AS referafriend_referer
//FROM	
	//dw_creditmemo_headers oh2	INNER JOIN dw_order_headers oh ON oh2.order_id = oh.order_id
					//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
					//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
					//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
					//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
					//INNER JOIN creditmemo_headers_vat ov ON ov.creditmemo_id = oh2.creditmemo_id
					//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
					//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
                    //LEFT JOIN dw_payment_method_map pm ON pm.payment_method = op.method
                    //LEFT JOIN dw_card_type_map ctm ON ctm.card_type = op.cc_type
                    //LEFT JOIN po_reorder_quote rq ON rq.entity_id = rp.reorder_quote_id  
                    //LEFT JOIN po_reorder_quote_payment rqp ON rqp.quote_id =  rq.entity_id
                    //LEFT JOIN dw_bis_source_map bc ON bc.source_code = oh.affilcode 
                    //LEFT JOIN dw_order_headers_marketing ohm ON ohm.order_id = oh.order_id;");


//$installer->run("CREATE OR REPLACE VIEW vw_order_lines_order AS  
//SELECT
	//ol.item_id AS line_id,
	//ol.order_id AS order_id,
	//cw.store_name AS store_name,
	//oh.order_no AS order_no,
	//CAST('ORDER' AS CHAR(25)) AS document_type,
	//oh.created_at AS document_date,
	//oh.updated_at AS document_updated_at,
	//oh.order_id AS document_id,
	//oh.created_at AS order_date,
	//oh.updated_at AS updated_at,

	//ol.product_id AS product_id,
	//cf.product_type AS product_type,
	//ol.product_options AS product_options,

	//p.is_lens  AS is_lens ,
	//'' AS lens_eye,
	//esi.BC AS lens_base_curve,
	//esi.DI  AS lens_diameter ,
	//esi.PO  AS lens_power ,
	//esi.CY  AS lens_cylinder ,
	//esi.AX  AS lens_axis ,
	//esi.AD  AS lens_addition ,
	//esi.DO AS lens_dominance,
	//esi.CO AS lens_colour,
	//p.daysperlens AS lens_days,

	//'' AS product_size,
	//'' AS product_colour,

	//ol.weight  AS unit_weight ,
	//ol.row_weight AS line_weight ,
	//ol.is_virtual AS is_virtual,
	//ol.sku AS sku,
	//ol.name AS `name`,
	//pcf.category_id AS category_id,
    //CASE WHEN oh.base_shipping_amount > 0 THEN 1 ELSE 0 END AS free_shipping ,
	//CASE WHEN oh.base_discount_amount > 0 THEN 1 ELSE 0 END AS no_discount ,

	//ol.qty_ordered AS qty,
	
	//olv.local_prof_fee  AS local_prof_fee ,
	//olv.local_price_inc_vat  AS local_price_inc_vat ,
	//olv.local_price_vat  AS local_price_vat ,
	//olv.local_price_exc_vat  AS local_price_exc_vat ,
	//olv.local_line_subtotal_inc_vat  AS local_line_subtotal_inc_vat ,
	//olv.local_line_subtotal_vat  AS local_line_subtotal_vat ,
	//olv.local_line_subtotal_exc_vat  AS local_line_subtotal_exc_vat ,
	//olv.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//olv.local_shipping_vat  AS local_shipping_vat ,
	//olv.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//-1 * ABS(olv.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//-1 * ABS(olv.local_discount_vat)  AS local_discount_vat ,
	//-1 * ABS(olv.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//-1 * ABS(olv.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//-1 * ABS(olv.local_store_credit_vat)  AS local_store_credit_vat ,
	//-1 * ABS(olv.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//olv.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//olv.local_adjustment_vat  AS local_adjustment_vat ,
	//olv.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//olv.local_line_total_inc_vat  AS local_line_total_inc_vat ,
	//olv.local_line_total_vat  AS local_line_total_vat ,
	//olv.local_line_total_exc_vat  AS local_line_total_exc_vat ,
	//olv.local_subtotal_cost  AS local_subtotal_cost ,
	//olv.local_shipping_cost  AS local_shipping_cost ,
	//olv.local_total_cost  AS local_total_cost ,
	//olv.local_margin_amount  AS local_margin_amount ,
	//olv.local_margin_percent  AS local_margin_percent ,
	//olv.local_to_global_rate  AS local_to_global_rate ,
	//olv.order_currency_code AS order_currency_code,

	//olv.global_prof_fee  AS global_prof_fee ,
	//olv.global_price_inc_vat  AS global_price_inc_vat ,
	//olv.global_price_vat  AS global_price_vat ,
	//olv.global_price_exc_vat  AS global_price_exc_vat ,
	//olv.global_line_subtotal_inc_vat  AS global_line_subtotal_inc_vat ,
	//olv.global_line_subtotal_vat  AS global_line_subtotal_vat ,
	//olv.global_line_subtotal_exc_vat  AS global_line_subtotal_exc_vat ,
	//olv.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//olv.global_shipping_vat  AS global_shipping_vat ,
	//olv.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//-1 * ABS(olv.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//-1 * ABS(olv.global_discount_vat)  AS global_discount_vat ,
	//-1 * ABS(olv.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//-1 * ABS(olv.global_store_credit_inc_vat)  AS global_store_credit_inc_vat ,
	//-1 * ABS(olv.global_store_credit_vat)  AS global_store_credit_vat ,
	//-1 * ABS(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat ,
	//olv.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//olv.global_adjustment_vat  AS global_adjustment_vat ,
	//olv.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//olv.global_line_total_inc_vat  AS global_line_total_inc_vat ,
	//olv.global_line_total_vat  AS global_line_total_vat ,
	//olv.global_line_total_exc_vat  AS global_line_total_exc_vat ,
	//olv.global_subtotal_cost  AS global_subtotal_cost ,
	//olv.global_shipping_cost  AS global_shipping_cost ,
	//olv.global_total_cost  AS global_total_cost ,
	//olv.global_margin_amount  AS global_margin_amount ,
	//olv.global_margin_percent  AS global_margin_percent ,

	//olv.prof_fee_percent  AS prof_fee_percent ,
	//olv.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//olv.vat_percent  AS vat_percent ,
	//ABS(olv.discount_percent)  AS discount_percent 
//FROM	
	//dw_order_lines ol 	INNER JOIN dw_order_headers oh ON oh.order_id = ol.order_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN dw_products p ON p.product_id = ol.product_id
				//LEFT JOIN edi_stock_item_agg esi ON esi.product_id = ol.product_id AND esi.product_code = ol.sku
				//INNER JOIN order_lines_vat olv ON olv.item_id = ol.item_id
                //LEFT JOIN dw_product_category_flat pcf ON pcf.product_id = ol.product_id
                //LEFT JOIN dw_flat_category cf ON cf.category_id = pcf.category_id
//WHERE oh.status != 'archived';");
				
//$installer->run("CREATE OR REPLACE VIEW vw_order_lines_cancel AS  
//SELECT
	//ol.item_id AS line_id,
	//ol.order_id AS order_id,
	//cw.store_name AS store_name,
	//oh.order_no AS order_no,
	//'CANCEL' AS document_type,
	//os.status_time AS document_date,
	//oh.updated_at AS document_updated_at,
	//oh.order_id AS document_id,
	//oh.created_at AS order_date,
	//oh.updated_at AS updated_at,

	//ol.product_id AS product_id,
	//cf.product_type AS product_type,
	//ol.product_options AS product_options,

	//p.is_lens  AS is_lens ,
	//'' AS lens_eye,
	//esi.BC AS lens_base_curve,
	//esi.DI  AS lens_diameter ,
	//esi.PO  AS lens_power ,
	//esi.CY  AS lens_cylinder ,
	//esi.AX  AS lens_axis ,
	//esi.AD  AS lens_addition ,
	//esi.DO AS lens_dominance,
	//esi.CO AS lens_colour,
	//p.daysperlens AS lens_days,

	//'' AS product_size,
	//'' AS product_colour,

	//-1 * ol.weight  AS unit_weight ,
	//-1 * ol.row_weight AS line_weight ,
	//ol.is_virtual AS is_virtual,
	//ol.sku AS sku,
	//ol.name AS `name`,
	//pcf.category_id AS category_id,
    //CASE WHEN oh.base_shipping_amount > 0 THEN 1 ELSE 0 END AS free_shipping ,
	//CASE WHEN oh.base_discount_amount > 0 THEN 1 ELSE 0 END AS no_discount ,

	//-1 * ol.qty_ordered  AS qty,
	
	//-1 * olv.local_prof_fee  AS local_prof_fee ,
	//olv.local_price_inc_vat  AS local_price_inc_vat ,
	//olv.local_price_vat  AS local_price_vat ,
	//olv.local_price_exc_vat  AS local_price_exc_vat ,
	//-1 * olv.local_line_subtotal_inc_vat  AS local_line_subtotal_inc_vat ,
	//-1 * olv.local_line_subtotal_vat  AS local_line_subtotal_vat ,
	//-1 * olv.local_line_subtotal_exc_vat  AS local_line_subtotal_exc_vat ,
	//-1 * olv.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//-1 * olv.local_shipping_vat  AS local_shipping_vat ,
	//-1 * olv.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//ABS(olv.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//ABS(olv.local_discount_vat)  AS local_discount_vat ,
	//ABS(olv.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//ABS(olv.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//ABS(olv.local_store_credit_vat)  AS local_store_credit_vat ,
	//ABS(olv.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//-1 * olv.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//-1 * olv.local_adjustment_vat  AS local_adjustment_vat ,
	//-1 * olv.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//-1 * olv.local_line_total_inc_vat  AS local_line_total_inc_vat ,
	//-1 * olv.local_line_total_vat  AS local_line_total_vat ,
	//-1 * olv.local_line_total_exc_vat  AS local_line_total_exc_vat ,
	//-1 * olv.local_subtotal_cost  AS local_subtotal_cost ,
	//-1 * olv.local_shipping_cost  AS local_shipping_cost ,
	//-1 * olv.local_total_cost  AS local_total_cost ,
	//-1 * olv.local_margin_amount  AS local_margin_amount ,
	//olv.local_margin_percent  AS local_margin_percent ,
	//olv.local_to_global_rate  AS local_to_global_rate ,
	//olv.order_currency_code AS order_currency_code,

	//-1 * olv.global_prof_fee  AS global_prof_fee ,
	//olv.global_price_inc_vat  AS global_price_inc_vat ,
	//olv.global_price_vat  AS global_price_vat ,
	//olv.global_price_exc_vat  AS global_price_exc_vat ,
	//-1 * olv.global_line_subtotal_inc_vat  AS global_line_subtotal_inc_vat ,
	//-1 * olv.global_line_subtotal_vat  AS global_line_subtotal_vat ,
	//-1 * olv.global_line_subtotal_exc_vat  AS global_line_subtotal_exc_vat ,
	//-1 * olv.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//-1 * olv.global_shipping_vat  AS global_shipping_vat ,
	//-1 * olv.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//ABS(olv.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//ABS(olv.global_discount_vat)  AS global_discount_vat ,
	//ABS(olv.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//ABS(olv.global_store_credit_inc_vat)  AS global_store_credit_inc_vat ,
	//ABS(olv.global_store_credit_vat)  AS global_store_credit_vat ,
	//ABS(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat ,
	//-1 * olv.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//-1 * olv.global_adjustment_vat  AS global_adjustment_vat ,
	//-1 * olv.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//-1 * olv.global_line_total_inc_vat  AS global_line_total_inc_vat ,
	//-1 * olv.global_line_total_vat  AS global_line_total_vat ,
	//-1 * olv.global_line_total_exc_vat  AS global_line_total_exc_vat ,
	//-1 * olv.global_subtotal_cost  AS global_subtotal_cost ,
	//-1 * olv.global_shipping_cost  AS global_shipping_cost ,
	//-1 * olv.global_total_cost  AS global_total_cost ,
	//-1 * olv.global_margin_amount  AS global_margin_amount ,
	//olv.global_margin_percent  AS global_margin_percent ,

	//olv.prof_fee_percent  AS prof_fee_percent ,
	//olv.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//olv.vat_percent  AS vat_percent ,
	//ABS(olv.discount_percent)  AS discount_percent 		
//FROM	
	//dw_order_lines ol 	INNER JOIN dw_order_headers oh ON oh.order_id = ol.order_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN dw_products p ON p.product_id = ol.product_id
				//LEFT JOIN edi_stock_item_agg esi ON esi.product_id = ol.product_id AND esi.product_code = ol.sku
				//INNER JOIN order_lines_vat olv ON olv.item_id = ol.item_id
				//LEFT JOIN order_status_history_agg os ON os.order_id = oh.order_id
                //LEFT JOIN dw_product_category_flat pcf ON pcf.product_id = ol.product_id
                //LEFT JOIN dw_flat_category cf ON cf.category_id = pcf.category_id
//WHERE 
	//oh.status = 'canceled' AND os.status = 'canceled';");
    
//$installer->run("CREATE OR REPLACE VIEW vw_order_lines_creditmemo AS  
//SELECT
	//ol.item_id AS line_id,
	//ol2.order_id AS order_id,
	//cw.store_name AS store_name,
	//oh.order_no AS order_no,
	//'CREDITMEMO' AS document_type,
	//oh2.created_at AS document_date,
	//oh2.updated_at AS document_updated_at,
	//oh2.creditmemo_id AS document_id,
	//oh.created_at AS order_date,
	//oh.updated_at AS updated_at,

	//ol.product_id AS product_id,
	//cf.product_type AS product_type,
	//ol2.product_options AS product_options,

	//p.is_lens  AS is_lens ,
	//'' AS lens_eye,
	//esi.BC AS lens_base_curve,
	//esi.DI  AS lens_diameter ,
	//esi.PO  AS lens_power ,
	//esi.CY  AS lens_cylinder ,
	//esi.AX  AS lens_axis ,
	//esi.AD  AS lens_addition ,
	//esi.DO AS lens_dominance,
	//esi.CO AS lens_colour,
	//p.daysperlens AS lens_days,

	//'' AS product_size,
	//'' AS product_colour,

	//-1 * ol2.weight  AS unit_weight ,
	//-1 * ol2.weight * ol.qty AS line_weight ,
	//ol2.is_virtual AS is_virtual,
	//ol.sku AS sku,
	//ol.name AS `name`,
	//pcf.category_id AS category_id,
    //CASE WHEN oh.base_shipping_amount > 0 THEN 1 ELSE 0 END AS free_shipping ,
	//CASE WHEN oh.base_discount_amount > 0 THEN 1 ELSE 0 END AS no_discount ,

	//-1 * ol.qty  AS qty_ordered ,
    
    
	//-1 * olv.local_prof_fee  AS local_prof_fee ,
	//olv.local_price_inc_vat  AS local_price_inc_vat ,
	//olv.local_price_vat  AS local_price_vat ,
	//olv.local_price_exc_vat  AS local_price_exc_vat ,
	//-1 * olv.local_line_subtotal_inc_vat  AS local_line_subtotal_inc_vat ,
	//-1 * olv.local_line_subtotal_vat  AS local_line_subtotal_vat ,
	//-1 * olv.local_line_subtotal_exc_vat  AS local_line_subtotal_exc_vat ,
	//-1 * olv.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//-1 * olv.local_shipping_vat  AS local_shipping_vat ,
	//-1 * olv.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//ABS(olv.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//ABS(olv.local_discount_vat)  AS local_discount_vat ,
	//ABS(olv.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//ABS(olv.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//ABS(olv.local_store_credit_vat)  AS local_store_credit_vat ,
	//ABS(olv.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//-1 * olv.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//-1 * olv.local_adjustment_vat  AS local_adjustment_vat ,
	//-1 * olv.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//-1 * olv.local_line_total_inc_vat  AS local_line_total_inc_vat ,
	//-1 * olv.local_line_total_vat  AS local_line_total_vat ,
	//-1 * olv.local_line_total_exc_vat  AS local_line_total_exc_vat ,
	//-1 * olv.local_subtotal_cost  AS local_subtotal_cost ,
	//-1 * olv.local_shipping_cost  AS local_shipping_cost ,
	//-1 * olv.local_total_cost  AS local_total_cost ,
	//-1 * olv.local_margin_amount  AS local_margin_amount ,
	//olv.local_margin_percent  AS local_margin_percent ,
	//olv.local_to_global_rate  AS local_to_global_rate ,
	//olv.order_currency_code AS order_currency_code,

	//-1 * olv.global_prof_fee  AS global_prof_fee ,
	//olv.global_price_inc_vat  AS global_price_inc_vat ,
	//olv.global_price_vat  AS global_price_vat ,
	//olv.global_price_exc_vat  AS global_price_exc_vat ,
	//-1 * olv.global_line_subtotal_inc_vat  AS global_line_subtotal_inc_vat ,
	//-1 * olv.global_line_subtotal_vat  AS global_line_subtotal_vat ,
	//-1 * olv.global_line_subtotal_exc_vat  AS global_line_subtotal_exc_vat ,
	//-1 * olv.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//-1 * olv.global_shipping_vat  AS global_shipping_vat ,
	//-1 * olv.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//ABS(olv.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//ABS(olv.global_discount_vat)  AS global_discount_vat ,
	//ABS(olv.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//ABS(olv.global_store_credit_inc_vat)  AS global_store_credit_inc_vat ,
	//ABS(olv.global_store_credit_vat)  AS global_store_credit_vat ,
	//ABS(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat ,
	//-1 * olv.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//-1 * olv.global_adjustment_vat  AS global_adjustment_vat ,
	//-1 * olv.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//-1 * olv.global_line_total_inc_vat  AS global_line_total_inc_vat ,
	//-1 * olv.global_line_total_vat  AS global_line_total_vat ,
	//-1 * olv.global_line_total_exc_vat  AS global_line_total_exc_vat ,
	//-1 * olv.global_subtotal_cost  AS global_subtotal_cost ,
	//-1 * olv.global_shipping_cost  AS global_shipping_cost ,
	//-1 * olv.global_total_cost  AS global_total_cost ,
	//-1 * olv.global_margin_amount  AS global_margin_amount ,
	//olv.global_margin_percent  AS global_margin_percent ,

	//olv.prof_fee_percent  AS prof_fee_percent ,
	//olv.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//olv.vat_percent  AS vat_percent ,
	//ABS(olv.discount_percent)  AS discount_percent 		
//FROM	
	//dw_creditmemo_lines ol 	LEFT JOIN dw_order_lines ol2 ON ol.order_line_id = ol2.item_id
				//INNER JOIN dw_creditmemo_headers oh2 ON oh2.creditmemo_id = ol.creditmemo_id
				//INNER JOIN dw_order_headers oh ON oh.order_id = oh2.order_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN dw_products p ON p.product_id = ol.product_id
				//LEFT JOIN edi_stock_item_agg esi ON esi.product_id = ol.product_id AND esi.product_code = ol.sku
				//INNER JOIN creditmemo_lines_vat olv ON olv.item_id = ol.item_id
                //LEFT JOIN dw_product_category_flat pcf ON pcf.product_id = ol.product_id
                //LEFT JOIN dw_flat_category cf ON cf.category_id = pcf.category_id;");
                

//$installer->run("CREATE OR REPLACE VIEW vw_shipment_headers_shipment AS
//SELECT 
	//cw.store_name AS store_name,
	//sh.shipment_id AS shipment_id,
	//sh.shipment_no AS shipment_no,
	//CAST('SHIPMENT' AS CHAR(25)) AS document_type,
	//sh.created_at AS document_date, 
	//sh.created_at AS document_updated_at, 
	//sh.shipment_id AS document_id,
    //sh.created_at AS shipment_date, 
	//sh.updated_at AS updated_at,
	//ih.invoice_id AS invoice_id,
	//ih.invoice_no AS invoice_no,
	//ih.created_at AS invoice_date,
	//oh.order_id AS order_id,
	//oh.order_no AS order_no,
	//oh.created_at AS order_date,
	//CAST(0 AS SIGNED) AS shipment_no_of_order,
	 

	//oh.customer_id AS customer_id,
	//oh.customer_email AS customer_email,
	//oh.customer_firstname AS customer_firstname,
	//oh.customer_lastname AS customer_lastname,
	//oh.customer_middlename AS customer_middlename,
	//oh.customer_prefix AS customer_prefix,
	//oh.customer_suffix AS customer_suffix,
	//oh.customer_taxvat AS customer_taxvat,
	//oh.status AS `status`,	
	//oh.coupon_code AS coupon_code,
	//oh.weight AS weight,
	//oh.customer_gender AS customer_gender,
	//oh.customer_dob AS customer_dob,

	//LEFT(oh.shipping_description,LOCATE(' - ',oh.shipping_description)) shipping_carrier,
	//oh.shipping_description AS shipping_method,
	//oh.warehouse_approved_time AS approved_time,
	
	//oc.length_of_time_to_verify AS length_of_time_to_verify, 
	//oc.length_of_time_for_payment AS length_of_time_for_payment,
	//oc.length_of_time_to_invoice AS length_of_time_to_invoice,
	//sc.length_of_time_invoice_to_this_shipment AS length_of_time_invoice_to_this_shipment,
	//sc.length_of_time_to_this_shipment AS length_of_time_to_this_shipment,
	
	
	//IFNULL(pm.payment_method_name,op.method) AS payment_method,
	//op.amount_authorized  AS local_payment_authorized,
	//op.amount_canceled AS local_payment_canceled,

	//op.cc_exp_month AS cc_exp_month,
	//op.cc_exp_year AS cc_exp_year,
	//op.cc_ss_start_month AS cc_start_month,
	//op.cc_ss_start_year AS cc_start_year,
	//op.cc_last4 AS cc_last4,
	//op.cc_status_description AS cc_status_description,
	//op.cc_owner AS cc_owner,
	//IFNULL(ctm.card_type_name,op.cc_type) AS cc_type,
	//op.cc_status AS cc_status,
	//op.cc_ss_issue AS cc_issue_no,
	//op.cc_avs_status AS cc_avs_status,

	//'' AS payment_info1,
	//'' AS payment_info2,
	//'' AS payment_info3,
	//'' AS payment_info4,
	//'' AS payment_info5,

	//bill.email AS billing_email,
	//bill.company AS billing_company,
	//bill.prefix AS billing_prefix,
	//bill.firstname AS billing_firstname,
	//bill.middlename AS billing_middlename,
	//bill.lastname AS billing_lastname,
	//bill.suffix AS billing_suffix,
	//bill.street AS billing_street,
	//bill.city AS billing_city,
	//bill.region AS billing_region,
	//bill.region_id AS billing_region_id,
	//bill.postcode AS billing_postcode,
	//bill.country_id AS billing_country_id,
	//bill.telephone AS billing_telephone,
	//bill.fax AS billing_fax,

	//ship.email AS shipping_email,
	//ship.company AS shipping_company,
	//ship.prefix AS shipping_prefix,
	//ship.firstname AS shipping_firstname,
	//ship.middlename AS shipping_middlename,
	//ship.lastname AS shipping_lastname,
	//ship.suffix AS shipping_suffix,
	//ship.street AS shipping_street,
	//ship.city AS shipping_city,
	//ship.region AS shipping_region,
	//ship.region_id AS shipping_region_id,
	//ship.postcode AS shipping_postcode,
	//ship.country_id AS shipping_country_id,
	//ship.telephone AS shipping_telephone,
	//ship.fax AS shipping_fax,

	//ov.has_lens AS has_lens,
	//ov.total_qty AS total_qty,

	//ov.prof_fee  AS local_prof_fee ,
	//ov.local_subtotal_inc_vat  AS local_subtotal_inc_vat ,
	//ov.local_subtotal_vat  AS local_subtotal_vat ,
	//ov.local_subtotal_exc_vat  AS local_subtotal_exc_vat ,
	//ov.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//ov.local_shipping_vat  AS local_shipping_vat ,
	//ov.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//-1 * ABS(ov.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//-1 * ABS(ov.local_discount_vat)  AS local_discount_vat ,
	//-1 * ABS(ov.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//-1 * ABS(ov.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//-1 * ABS(ov.local_store_credit_vat)  AS local_store_credit_vat ,
	//-1 * ABS(ov.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//ov.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//ov.local_adjustment_vat  AS local_adjustment_vat ,
	//ov.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//ov.local_total_inc_vat  AS local_total_inc_vat ,
	//ov.local_total_vat  AS local_total_vat ,
	//ov.local_total_exc_vat  AS local_total_exc_vat ,
	//ov.local_subtotal_cost  AS local_subtotal_cost ,
	//ov.local_shipping_cost  AS local_shipping_cost ,
	//ov.local_total_cost  AS local_total_cost ,
	//ov.local_margin_amount  AS local_margin_amount ,
	//ov.local_margin_percent  AS local_margin_percent ,
	//oh.base_to_global_rate  AS local_to_global_rate ,
	//oh.order_currency_code AS order_currency_code,

	//ov.global_prof_fee  AS global_prof_fee ,
	//ov.global_subtotal_inc_vat  AS global_subtotal_inc_vat ,
	//ov.global_subtotal_vat  AS global_subtotal_vat ,
	//ov.global_subtotal_exc_vat  AS global_subtotal_exc_vat ,
	//ov.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//ov.global_shipping_vat  AS global_shipping_vat ,
	//ov.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//-1 * ABS(ov.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//-1 * ABS(ov.global_discount_vat)  AS global_discount_vat ,
	//-1 * ABS(ov.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//-1 * ABS(ov.global_store_credit_inc_vat) AS global_store_credit_inc_vat ,
	//-1 * ABS(ov.global_store_credit_vat)  AS global_store_credit_vat ,
	//-1 * ABS(ov.global_store_credit_exc_vat)  AS global_store_credit_exc_vat ,
	//ov.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//ov.global_adjustment_vat  AS global_adjustment_vat ,
	//ov.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//ov.global_total_inc_vat  AS global_total_inc_vat ,
	//ov.global_total_vat  AS global_total_vat ,
	//ov.global_total_exc_vat  AS global_total_exc_vat ,
	//ov.global_subtotal_cost  AS global_subtotal_cost ,
	//ov.global_shipping_cost  AS global_shipping_cost ,
	//ov.global_total_cost  AS global_total_cost ,
	//ov.global_margin_amount  AS global_margin_amount ,
	//ov.global_margin_percent  AS global_margin_percent ,

	//ov.prof_fee_percent  AS prof_fee_percent ,
	//ov.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//ov.vat_percent  AS vat_percent ,
	//ABS(ov.discount_percent)  AS discount_percent ,
	
	//oh.customer_note AS customer_note,
	//oh.customer_note_notify AS customer_note_notify, 
	//oh.remote_ip AS remote_ip,
	//oh.affilcode AS business_source,
	//bc.channel AS business_channel,
	//oh.affilBatch AS affilBatch,
	//oh.affilCode AS affilCode,
	//oh.affilUserRef AS affilUserRef,
	//oh.postoptics_auto_verification AS auto_verification,
    //CASE WHEN oh.postoptics_source = 'user' THEN 'web' ELSE  oh.postoptics_source  END As order_type,
    //ohm.ORDER_LIFECYCle AS order_lifecycle,
	//oh.reminder_date AS reminder_date,
	//oh.reminder_mobile AS reminder_mobile,
	//oh.reminder_period AS reminder_period,
	//oh.reminder_presc AS reminder_presc,
	//oh.reminder_type AS reminder_type,
	//oh.reminder_sent   AS reminder_sent  ,
	//oh.reminder_follow_sent   AS reminder_follow_sent  ,
	//oh.telesales_method_code AS telesales_method_code,
	//oh.telesales_admin_username AS telesales_admin_username,
	//oh.reorder_on_flag   AS reorder_on_flag  ,
	//oh.reorder_profile_id AS reorder_profile_id,
	//rp.next_order_date  AS reorder_date ,
	//rp.interval_days AS reorder_interval,
	//rqp.cc_exp_month AS reorder_cc_exp_month,
	//rqp.cc_exp_year AS reorder_cc_exp_year,
	//oh.automatic_reorder   AS automatic_reorder  ,
	//oh.presc_verification_method AS presc_verification_method,
	//oh.referafriend_code AS referafriend_code,
	//oh.referafriend_referer AS referafriend_referer

//FROM	
	//dw_shipment_headers sh
				//INNER JOIN dw_order_headers oh 	ON oh.order_id = sh.order_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
				//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
				//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
				//INNER JOIN shipment_headers_vat ov ON ov.shipment_id = sh.shipment_id
				//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
				//LEFT JOIN dw_shipment_calender_dates sc ON sc.shipment_id = sh.shipment_id
				//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
				//LEFT JOIN first_inv_id fi ON sh.order_id = fi.order_id 
				//LEFT JOIN dw_invoice_headers ih ON ih.invoice_id = fi.invoice_id 
                //LEFT JOIN dw_payment_method_map pm ON pm.payment_method = op.method
                //LEFT JOIN dw_card_type_map ctm ON ctm.card_type = op.cc_type
                //LEFT JOIN po_reorder_quote rq ON rq.entity_id = rp.reorder_quote_id  
                //LEFT JOIN po_reorder_quote_payment rqp ON rqp.quote_id =  rq.entity_id
                //LEFT JOIN dw_bis_source_map bc ON bc.source_code = oh.affilcode
                //LEFT JOIN dw_order_headers_marketing ohm ON ohm.order_id = oh.order_id;");
				
		
//$installer->run("CREATE OR REPLACE VIEW vw_shipment_headers_creditmemo AS 
//SELECT
	//cw.store_name AS store_name,
	//oh2.creditmemo_id AS shipment_id,
	//oh2.creditmemo_no AS shipment_no,
	//CAST('CREDITMEMO' AS CHAR(25)) AS document_type,
	//oh2.created_at AS document_date, 
	//oh2.updated_at AS document_updated_at, 
	//oh2.creditmemo_id AS document_id, 
	//oh2.created_at AS shipment_date, 
	//oh2.updated_at AS updated_at,
	//ih.invoice_id AS invoice_id,
	//ih.invoice_no AS invoice_no,
	//ih.created_at AS invoice_date,
	//oh.order_id AS order_id,
	//oh.order_no AS order_no,
	//oh.created_at AS order_date,
	//CAST(0 AS SIGNED) AS shipment_no_of_order,
	 

	//oh.customer_id AS customer_id,
	//oh.customer_email AS customer_email,
	//oh.customer_firstname AS customer_firstname,
	//oh.customer_lastname AS customer_lastname,
	//oh.customer_middlename AS customer_middlename,
	//oh.customer_prefix AS customer_prefix,
	//oh.customer_suffix AS customer_suffix,
	//oh.customer_taxvat AS customer_taxvat,
	//oh.status AS `status`,	
	//oh.coupon_code AS coupon_code,
	//-1 * oh.weight AS weight,
	//oh.customer_gender AS customer_gender,
	//oh.customer_dob AS customer_dob,

	//LEFT(oh.shipping_description,LOCATE(' - ',oh.shipping_description)) shipping_carrier,
	//oh.shipping_description AS shipping_method,
	//oh.warehouse_approved_time AS approved_time,
	
	//oc.length_of_time_to_verify AS length_of_time_to_verify, 
	//oc.length_of_time_for_payment AS length_of_time_for_payment,
	//oc.length_of_time_to_invoice AS length_of_time_to_invoice,
	//NULL AS length_of_time_invoice_to_this_shipment,
	//NULL AS length_of_time_to_this_shipment,
	
	
	//IFNULL(pm.payment_method_name,op.method) AS payment_method,
	//op.amount_authorized  AS local_payment_authorized,
	//op.amount_canceled AS local_payment_canceled,

	//op.cc_exp_month AS cc_exp_month,
	//op.cc_exp_year AS cc_exp_year,
	//op.cc_ss_start_month AS cc_start_month,
	//op.cc_ss_start_year AS cc_start_year,
	//op.cc_last4 AS cc_last4,
	//op.cc_status_description AS cc_status_description,
	//op.cc_owner AS cc_owner,
	//IFNULL(ctm.card_type_name,op.cc_type) AS cc_type,
	//op.cc_status AS cc_status,
	//op.cc_ss_issue AS cc_issue_no,
	//op.cc_avs_status AS cc_avs_status,

	//'' AS payment_info1,
	//'' AS payment_info2,
	//'' AS payment_info3,
	//'' AS payment_info4,
	//'' AS payment_info5,

	//bill.email AS billing_email,
	//bill.company AS billing_company,
	//bill.prefix AS billing_prefix,
	//bill.firstname AS billing_firstname,
	//bill.middlename AS billing_middlename,
	//bill.lastname AS billing_lastname,
	//bill.suffix AS billing_suffix,
	//bill.street AS billing_street,
	//bill.city AS billing_city,
	//bill.region AS billing_region,
	//bill.region_id AS billing_region_id,
	//bill.postcode AS billing_postcode,
	//bill.country_id AS billing_country_id,
	//bill.telephone AS billing_telephone,
	//bill.fax AS billing_fax,

	//ship.email AS shipping_email,
	//ship.company AS shipping_company,
	//ship.prefix AS shipping_prefix,
	//ship.firstname AS shipping_firstname,
	//ship.middlename AS shipping_middlename,
	//ship.lastname AS shipping_lastname,
	//ship.suffix AS shipping_suffix,
	//ship.street AS shipping_street,
	//ship.city AS shipping_city,
	//ship.region AS shipping_region,
	//ship.region_id AS shipping_region_id,
	//ship.postcode AS shipping_postcode,
	//ship.country_id AS shipping_country_id,
	//ship.telephone AS shipping_telephone,
	//ship.fax AS shipping_fax,

	//ov.has_lens AS has_lens,
	//-1 * ov.total_qty AS total_qty,

	//-1 * ABS(ov.prof_fee)  AS local_prof_fee ,
	//-1 * ABS(ov.local_subtotal_inc_vat)  AS local_subtotal_inc_vat ,
	//-1 * ABS(ov.local_subtotal_vat)  AS local_subtotal_vat ,
	//-1 * ABS(ov.local_subtotal_exc_vat)  AS local_subtotal_exc_vat ,
	//-1 * ABS(ov.local_shipping_inc_vat)  AS local_shipping_inc_vat ,
	//-1 * ABS(ov.local_shipping_vat)  AS local_shipping_vat ,
	//-1 * ABS(ov.local_shipping_exc_vat)  AS local_shipping_exc_vat ,
	//ABS(ov.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//ABS(ov.local_discount_vat)  AS local_discount_vat ,
	//ABS(ov.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//ABS(ov.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//ABS(ov.local_store_credit_vat)  AS local_store_credit_vat ,
	//ABS(ov.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//-1 * ABS(ov.local_adjustment_inc_vat)  AS local_adjustment_inc_vat ,
	//-1 * ABS(ov.local_adjustment_vat)  AS local_adjustment_vat ,
	//-1 * ABS(ov.local_adjustment_exc_vat)  AS local_adjustment_exc_vat ,
	//-1 * ABS(ov.local_total_inc_vat)  AS local_total_inc_vat ,
	//-1 * ABS(ov.local_total_vat)  AS local_total_vat ,
	//-1 * ABS(ov.local_total_exc_vat)  AS local_total_exc_vat ,
	//-1 * ABS(ov.local_subtotal_cost)  AS local_subtotal_cost ,
	//-1 * ABS(ov.local_shipping_cost)  AS local_shipping_cost ,
	//-1 * ABS(ov.local_total_cost)  AS local_total_cost ,
	//-1 * ABS(ov.local_margin_amount)  AS local_margin_amount ,
	//ov.local_margin_percent  AS local_margin_percent ,
	//oh.base_to_global_rate  AS local_to_global_rate ,
	//oh.order_currency_code AS order_currency_code,

	//-1 * ABS(ov.global_prof_fee)  AS global_prof_fee ,
	//-1 * ABS(ov.global_subtotal_inc_vat)  AS global_subtotal_inc_vat ,
	//-1 * ABS(ov.global_subtotal_vat)  AS global_subtotal_vat ,
	//-1 * ABS(ov.global_subtotal_exc_vat)  AS global_subtotal_exc_vat ,
	//-1 * ABS(ov.global_shipping_inc_vat)  AS global_shipping_inc_vat ,
	//-1 * ABS(ov.global_shipping_vat)  AS global_shipping_vat ,
	//-1 * ABS(ov.global_shipping_exc_vat)  AS global_shipping_exc_vat ,
	//ABS(ov.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//ABS(ov.global_discount_vat)  AS global_discount_vat ,
	//ABS(ov.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//ABS(ov.global_store_credit_inc_vat) AS global_store_credit_inc_vat ,
	//ABS(ov.global_store_credit_vat)  AS global_store_credit_vat ,
	//ABS(ov.global_store_credit_exc_vat)  AS global_store_credit_exc_vat ,
	//-1 * ABS(ov.global_adjustment_inc_vat)  AS global_adjustment_inc_vat ,
	//-1 * ABS(ov.global_adjustment_vat)  AS global_adjustment_vat ,
	//-1 * ABS(ov.global_adjustment_exc_vat)  AS global_adjustment_exc_vat ,
	//-1 * ABS(ov.global_total_inc_vat)  AS global_total_inc_vat ,
	//-1 * ABS(ov.global_total_vat)  AS global_total_vat ,
	//-1 * ABS(ov.global_total_exc_vat)  AS global_total_exc_vat ,
	//-1 * ABS(ov.global_subtotal_cost)  AS global_subtotal_cost ,
	//-1 * ABS(ov.global_shipping_cost)  AS global_shipping_cost ,
	//-1 * ABS(ov.global_total_cost)  AS global_total_cost ,
	//-1 * ABS(ov.global_margin_amount) AS global_margin_amount ,
	//ov.global_margin_percent  AS global_margin_percent ,

	//ov.prof_fee_percent  AS prof_fee_percent ,
	//ov.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//ov.vat_percent  AS vat_percent ,
	//ABS(ov.discount_percent)  AS discount_percent ,
	
	//oh.customer_note AS customer_note,
	//oh.customer_note_notify AS customer_note_notify, 
	//oh.remote_ip AS remote_ip,
	//oh.affilcode AS business_source,
	//bc.channel AS business_channel,
	//oh.affilBatch AS affilBatch,
	//oh.affilCode AS affilCode,
	//oh.affilUserRef AS affilUserRef,
	//oh.postoptics_auto_verification AS auto_verification,
    //CASE WHEN oh.postoptics_source = 'user' THEN 'web' ELSE  oh.postoptics_source  END As order_type,
    //ohm.ORDER_LIFECYCle AS order_lifecycle,
	//oh.reminder_date AS reminder_date,
	//oh.reminder_mobile AS reminder_mobile,
	//oh.reminder_period AS reminder_period,
	//oh.reminder_presc AS reminder_presc,
	//oh.reminder_type AS reminder_type,
	//oh.reminder_sent   AS reminder_sent  ,
	//oh.reminder_follow_sent   AS reminder_follow_sent  ,
	//oh.telesales_method_code AS telesales_method_code,
	//oh.telesales_admin_username AS telesales_admin_username,
	//oh.reorder_on_flag   AS reorder_on_flag  ,
	//oh.reorder_profile_id AS reorder_profile_id,
	//rp.next_order_date  AS reorder_date ,
	//rp.interval_days AS reorder_interval,
	//rqp.cc_exp_month AS reorder_cc_exp_month,
	//rqp.cc_exp_year AS reorder_cc_exp_year,
	//oh.automatic_reorder   AS automatic_reorder  ,
	//oh.presc_verification_method AS presc_verification_method,
	//oh.referafriend_code AS referafriend_code,
	//oh.referafriend_referer AS referafriend_referer
//FROM	
	//dw_creditmemo_headers oh2	INNER JOIN dw_order_headers oh ON oh2.order_id = oh.order_id
					//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
					//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
					//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
					//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
					//INNER JOIN creditmemo_shipment_headers_vat ov ON ov.creditmemo_id = oh2.creditmemo_id
					//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
					//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
					//LEFT JOIN first_inv_id fi ON oh.order_id = fi.order_id 
					//LEFT JOIN dw_invoice_headers ih ON ih.invoice_id = fi.invoice_id
                    //LEFT JOIN dw_payment_method_map pm ON pm.payment_method = op.method
                    //LEFT JOIN dw_card_type_map ctm ON ctm.card_type = op.cc_type
                    //LEFT JOIN po_reorder_quote rq ON rq.entity_id = rp.reorder_quote_id  
                    //LEFT JOIN po_reorder_quote_payment rqp ON rqp.quote_id =  rq.entity_id
                    //LEFT JOIN dw_bis_source_map bc ON bc.source_code = oh.affilcode
                    //LEFT JOIN dw_order_headers_marketing ohm ON ohm.order_id = oh.order_id;");


//$installer->run("CREATE OR REPLACE VIEW vw_shipment_lines_shipment AS
//SELECT 
	//sl.item_id AS line_id,
	//sl.shipment_id AS shipment_id,
	//cw.store_name AS store_name,
	//sh.shipment_no AS shipment_no,
	//CAST('SHIPMENT' AS CHAR(25)) AS document_type,
	//sh.created_at AS document_date,
	//sh.updated_at AS document_updated_at,
	//sh.shipment_id AS document_id,
	//sh.created_at AS shipment_date,
	//sh.updated_at AS updated_at,

	//sl.product_id AS product_id,
	//cf.product_type AS product_type,
	//ol.product_options AS product_options,

	//p.is_lens  AS is_lens ,
	//'' AS lens_eye,
	//esi.BC AS lens_base_curve,
	//esi.DI  AS lens_diameter ,
	//esi.PO  AS lens_power ,
	//esi.CY  AS lens_cylinder ,
	//esi.AX  AS lens_axis ,
	//esi.AD  AS lens_addition ,
	//esi.DO AS lens_dominance,
	//esi.CO AS lens_colour,
	//p.daysperlens AS lens_days,

	//'' AS product_size,
	//'' AS product_colour,

	//sl.weight  AS unit_weight ,
	//sl.weight * sl.qty AS line_weight ,
	//ol.is_virtual AS is_virtual,
	//sl.sku AS sku,
	//sl.name AS `name`,
	//pcf.category_id AS category_id,
    //CASE WHEN oh.base_shipping_amount > 0 THEN 1 ELSE 0 END AS free_shipping ,
	//CASE WHEN oh.base_discount_amount > 0 THEN 1 ELSE 0 END AS no_discount ,

	//sl.qty AS qty,
	
	//olv.local_prof_fee  AS local_prof_fee ,
	//olv.local_price_inc_vat  AS local_price_inc_vat ,
	//olv.local_price_vat  AS local_price_vat ,
	//olv.local_price_exc_vat  AS local_price_exc_vat ,
	//olv.local_line_subtotal_inc_vat  AS local_line_subtotal_inc_vat ,
	//olv.local_line_subtotal_vat  AS local_line_subtotal_vat ,
	//olv.local_line_subtotal_exc_vat  AS local_line_subtotal_exc_vat ,
	//olv.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//olv.local_shipping_vat  AS local_shipping_vat ,
	//olv.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//-1 * ABS(olv.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//-1 * ABS(olv.local_discount_vat)  AS local_discount_vat ,
	//-1 * ABS(olv.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//-1 * ABS(olv.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//-1 * ABS(olv.local_store_credit_vat)  AS local_store_credit_vat ,
	//-1 * ABS(olv.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//olv.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//olv.local_adjustment_vat  AS local_adjustment_vat ,
	//olv.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//olv.local_line_total_inc_vat  AS local_line_total_inc_vat ,
	//olv.local_line_total_vat  AS local_line_total_vat ,
	//olv.local_line_total_exc_vat  AS local_line_total_exc_vat ,
	//olv.local_subtotal_cost  AS local_subtotal_cost ,
	//olv.local_shipping_cost  AS local_shipping_cost ,
	//olv.local_total_cost  AS local_total_cost ,
	//olv.local_margin_amount  AS local_margin_amount ,
	//olv.local_margin_percent  AS local_margin_percent ,
	//olv.local_to_global_rate  AS local_to_global_rate ,
	//olv.order_currency_code AS order_currency_code,

	//olv.global_prof_fee  AS global_prof_fee ,
	//olv.global_price_inc_vat  AS global_price_inc_vat ,
	//olv.global_price_vat  AS global_price_vat ,
	//olv.global_price_exc_vat  AS global_price_exc_vat ,
	//olv.global_line_subtotal_inc_vat  AS global_line_subtotal_inc_vat ,
	//olv.global_line_subtotal_vat  AS global_line_subtotal_vat ,
	//olv.global_line_subtotal_exc_vat  AS global_line_subtotal_exc_vat ,
	//olv.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//olv.global_shipping_vat  AS global_shipping_vat ,
	//olv.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//-1 * ABS(olv.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//-1 * ABS(olv.global_discount_vat)  AS global_discount_vat ,
	//-1 * ABS(olv.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//-1 * ABS(olv.global_store_credit_inc_vat)  AS global_store_credit_inc_vat ,
	//-1 * ABS(olv.global_store_credit_vat)  AS global_store_credit_vat ,
	//-1 * ABS(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat ,
	//olv.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//olv.global_adjustment_vat  AS global_adjustment_vat ,
	//olv.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//olv.global_line_total_inc_vat  AS global_line_total_inc_vat ,
	//olv.global_line_total_vat  AS global_line_total_vat ,
	//olv.global_line_total_exc_vat  AS global_line_total_exc_vat ,
	//olv.global_subtotal_cost  AS global_subtotal_cost ,
	//olv.global_shipping_cost  AS global_shipping_cost ,
	//olv.global_total_cost  AS global_total_cost ,
	//olv.global_margin_amount  AS global_margin_amount ,
	//olv.global_margin_percent  AS global_margin_percent ,

	//olv.prof_fee_percent  AS prof_fee_percent ,
	//olv.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//olv.vat_percent  AS vat_percent ,
	//ABS(olv.discount_percent)  AS discount_percent 
//FROM	
	//dw_shipment_lines sl	INNER JOIN dw_shipment_headers sh ON sh.shipment_id = sl.shipment_id
				//INNER JOIN dw_order_headers oh 	ON oh.order_id = sh.order_id
				//INNER JOIN dw_order_lines ol 	ON ol.item_id = sl.order_line_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
				//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
				//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
				//INNER JOIN shipment_lines_vat olv ON olv.item_id = sl.item_id
				//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
				//LEFT JOIN dw_shipment_calender_dates sc ON sc.shipment_id = sh.shipment_id
				//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
				//LEFT JOIN first_inv_id fi ON sh.order_id = fi.order_id 
				//LEFT JOIN dw_invoice_headers ih ON ih.invoice_id = fi.invoice_id 
				//LEFT JOIN dw_products p ON p.product_id =sl.product_id
				//LEFT JOIN edi_stock_item_agg esi ON esi.product_id = sl.product_id AND esi.product_code = sl.sku
                //LEFT JOIN dw_product_category_flat pcf ON pcf.product_id = ol.product_id
                //LEFT JOIN dw_flat_category cf ON cf.category_id = pcf.category_id;");

//$installer->run("CREATE OR REPLACE VIEW vw_shipment_lines_creditmemo AS
//SELECT			
	//cl.item_id AS line_id,
	//cl.creditmemo_id AS shipment_id,
	//cw.store_name AS store_name,
	//oh2.creditmemo_no AS shipment_no,
	//CAST('CREDITMEMO' AS CHAR(25)) AS document_type,
	//oh2.created_at AS document_date,
	//oh2.updated_at AS document_updated_at,
	//oh2.creditmemo_id AS document_id,
	//oh2.created_at AS shipment_date,
	//oh2.updated_at AS updated_at,

	//cl.product_id AS product_id,
	//cf.product_type AS product_type,
	//ol.product_options AS product_options,

	//p.is_lens AS is_lens ,
	//'' AS lens_eye,
	//esi.BC AS lens_base_curve,
	//esi.DI AS lens_diameter ,
	//esi.PO AS lens_power ,
	//esi.CY AS lens_cylinder ,
	//esi.AX AS lens_axis ,
	//esi.AD AS lens_addition ,
	//esi.DO AS lens_dominance,
	//esi.CO AS lens_colour,
	//p.daysperlens AS lens_days,

	//'' AS product_size,
	//'' AS product_colour,

	//-1 * ol.weight  AS unit_weight ,
	//-1 * ol.weight * cl.qty AS line_weight ,
	//ol.is_virtual AS is_virtual,
	//cl.sku AS sku,
	//cl.name AS `name`,
	//pcf.category_id AS category_id,
    //CASE WHEN oh.base_shipping_amount > 0 THEN 1 ELSE 0 END AS free_shipping ,
	//CASE WHEN oh.base_discount_amount > 0 THEN 1 ELSE 0 END AS no_discount ,

	//-1 * cl.qty  AS qty,
	
	//-1 * olv.local_prof_fee  AS local_prof_fee ,
	//olv.local_price_inc_vat  AS local_price_inc_vat ,
	//olv.local_price_vat  AS local_price_vat ,
	//olv.local_price_exc_vat  AS local_price_exc_vat ,
	//-1 * olv.local_line_subtotal_inc_vat  AS local_line_subtotal_inc_vat ,
	//-1 * olv.local_line_subtotal_vat  AS local_line_subtotal_vat ,
	//-1 * olv.local_line_subtotal_exc_vat  AS local_line_subtotal_exc_vat ,
	//-1 * olv.local_shipping_inc_vat  AS local_shipping_inc_vat ,
	//-1 * olv.local_shipping_vat  AS local_shipping_vat ,
	//-1 * olv.local_shipping_exc_vat  AS local_shipping_exc_vat ,
	//ABS(olv.local_discount_inc_vat)  AS local_discount_inc_vat ,
	//ABS(olv.local_discount_vat)  AS local_discount_vat ,
	//ABS(olv.local_discount_exc_vat)  AS local_discount_exc_vat ,
	//ABS(olv.local_store_credit_inc_vat)  AS local_store_credit_inc_vat ,
	//ABS(olv.local_store_credit_vat)  AS local_store_credit_vat ,
	//ABS(olv.local_store_credit_exc_vat)  AS local_store_credit_exc_vat ,
	//-1 * olv.local_adjustment_inc_vat  AS local_adjustment_inc_vat ,
	//-1 * olv.local_adjustment_vat  AS local_adjustment_vat ,
	//-1 * olv.local_adjustment_exc_vat  AS local_adjustment_exc_vat ,
	//-1 * olv.local_line_total_inc_vat  AS local_line_total_inc_vat ,
	//-1 * olv.local_line_total_vat  AS local_line_total_vat ,
	//-1 * olv.local_line_total_exc_vat  AS local_line_total_exc_vat ,
	//-1 * olv.local_subtotal_cost  AS local_subtotal_cost ,
	//-1 * olv.local_shipping_cost  AS local_shipping_cost ,
	//-1 * olv.local_total_cost  AS local_total_cost ,
	//-1 * olv.local_margin_amount  AS local_margin_amount ,
	//olv.local_margin_percent  AS local_margin_percent ,
	//olv.local_to_global_rate  AS local_to_global_rate ,
	//olv.order_currency_code AS order_currency_code,

	//-1 * olv.global_prof_fee  AS global_prof_fee ,
	//olv.global_price_inc_vat  AS global_price_inc_vat ,
	//olv.global_price_vat  AS global_price_vat ,
	//olv.global_price_exc_vat  AS global_price_exc_vat ,
	//-1 * olv.global_line_subtotal_inc_vat  AS global_line_subtotal_inc_vat ,
	//-1 * olv.global_line_subtotal_vat  AS global_line_subtotal_vat ,
	//-1 * olv.global_line_subtotal_exc_vat  AS global_line_subtotal_exc_vat ,
	//-1 * olv.global_shipping_inc_vat  AS global_shipping_inc_vat ,
	//-1 * olv.global_shipping_vat  AS global_shipping_vat ,
	//-1 * olv.global_shipping_exc_vat  AS global_shipping_exc_vat ,
	//ABS(olv.global_discount_inc_vat)  AS global_discount_inc_vat ,
	//ABS(olv.global_discount_vat)  AS global_discount_vat ,
	//ABS(olv.global_discount_exc_vat)  AS global_discount_exc_vat ,
	//ABS(olv.global_store_credit_inc_vat)  AS global_store_credit_inc_vat ,
	//ABS(olv.global_store_credit_vat)  AS global_store_credit_vat ,
	//ABS(olv.global_store_credit_exc_vat) AS global_store_credit_exc_vat ,
	//-1 * olv.global_adjustment_inc_vat  AS global_adjustment_inc_vat ,
	//-1 * olv.global_adjustment_vat  AS global_adjustment_vat ,
	//-1 * olv.global_adjustment_exc_vat  AS global_adjustment_exc_vat ,
	//-1 * olv.global_line_total_inc_vat  AS global_line_total_inc_vat ,
	//-1 * olv.global_line_total_vat  AS global_line_total_vat ,
	//-1 * olv.global_line_total_exc_vat  AS global_line_total_exc_vat ,
	//-1 * olv.global_subtotal_cost  AS global_subtotal_cost ,
	//-1 * olv.global_shipping_cost  AS global_shipping_cost ,
	//-1 * olv.global_total_cost  AS global_total_cost ,
	//-1 * olv.global_margin_amount  AS global_margin_amount ,
	//olv.global_margin_percent  AS global_margin_percent ,

	//olv.prof_fee_percent  AS prof_fee_percent ,
	//olv.vat_percent_before_prof_fee  AS vat_percent_before_prof_fee ,
	//olv.vat_percent  AS vat_percent ,
	//ABS(olv.discount_percent)  AS discount_percent 				
//FROM
	//dw_creditmemo_lines cl 	INNER JOIN dw_creditmemo_headers oh2 ON oh2.creditmemo_id = cl.creditmemo_id
				//INNER JOIN dw_order_headers oh ON oh2.order_id = oh.order_id
				//INNER JOIN dw_order_lines ol 	ON ol.item_id = cl.order_line_id
				//LEFT JOIN dw_stores cw ON cw.store_id = oh.store_id
				//LEFT JOIN sales_flat_order_payment op ON op.parent_id = oh.order_id 
				//LEFT JOIN sales_flat_order_address bill ON bill.entity_id = oh.billing_address_id
				//LEFT JOIN sales_flat_order_address ship ON ship.entity_id = oh.shipping_address_id
				//INNER JOIN creditmemo_shipment_lines_vat olv ON olv.item_id = cl.item_id
				//LEFT JOIN po_reorder_profile rp ON oh.reorder_profile_id = rp.id
				//LEFT JOIN dw_order_calender_dates oc ON oc.order_id = oh.order_id
				//LEFT JOIN first_inv_id fi ON oh.order_id = fi.order_id 
				//LEFT JOIN dw_invoice_headers ih ON ih.invoice_id = fi.invoice_id 
				//LEFT JOIN dw_products p ON p.product_id =cl.product_id
				//LEFT JOIN edi_stock_item_agg esi ON esi.product_id = cl.product_id AND esi.product_code = cl.sku
                //LEFT JOIN dw_product_category_flat pcf ON pcf.product_id = ol.product_id
                //LEFT JOIN dw_flat_category cf ON cf.category_id = pcf.category_id;");
 
//$installer->run("UPDATE dw_calendar SET opening_time = '08:30:00' , closing_time = '14:30:00' WHERE ISUKHoliday = 0 AND Day_NumberInWeek = 6;");  


   
//$installer->run("CREATE OR REPLACE VIEW vw_customers AS
//SELECT 
	//c.customer_id AS customer_id,
	//email AS email,
	//ds.store_name AS store_name,
	//c.created_at AS created_at,
	//c.updated_at AS updated_at,
	//ds.website_group AS website_group,
    //COALESCE(new_customer_created_in,ds2.store_name,ds.store_name) AS created_in,
	//prefix AS prefix,
	//firstname AS firstname,
	//middlename AS middlename,
	//lastname AS lastname,
	//suffix AS suffix,
	//taxvat AS taxvat,
	//postoptics_send_post AS postoptics_send_post,
	//facebook_id AS facebook_id,
	//facebook_permissions AS facebook_permissions,
	//gender AS gender,
	//dob AS dob,
	//unsubscribe_all AS unsubscribe_all,
	//days_worn.value AS days_worn,
	//parent_customer_id AS parent_customer_id,
	//is_parent_customer AS is_parent_customer,
	//eyeplan_credit_limit AS eyeplan_credit_limit,
	//eyeplan_approved_flag AS eyeplan_approved_flag,
	//eyeplan_can_ref_new_customer AS eyeplan_can_ref_new_customer,
	//cus_phone AS cus_phone,
	//found_us_info AS found_us_info,
	//cast('' as char) AS referafriend_code,
	//cast('' as char) AS alternate_email,
	//cast('' as char) AS emvadmin1,
	//cast('' as char) AS emvadmin2,
	//cast('' as char) AS emvadmin3,
	//cast('' as char) AS emvadmin4,
	//cast('' as char) AS emvadmin5 ,
	//cast('' as char) AS  `language`,
	//cast('' as char) AS first_order_date,
	//cast('' as char) AS last_logged_in_date,
	//cast('' as char) AS last_order_date ,
	//cast('' as char) AS num_of_orders,
	//cast('' as char) AS card_expiry_date,
	//cast('' as char) AS segment_lifecycle,
	//cast('' as char) AS segment_usage,
	//cast('' as char) AS segment_geog,
	//cast('' as char) AS segment_purch_behaviour,
	//cast('' as char) AS segment_eysight,
	//cast('' as char) AS segment_sport,
	//cast('' as char) AS segment_professional,
	//cast('' as char) AS segment_lifestage,
	//cast('' as char) AS segment_vanity,
	//cast('' as char) AS segment,
	//cast('' as char) AS segment_2,
	//cast('' as char) AS segment_3,
	//cast('' as char) AS segment_4
//FROM
	//dw_customers c	LEFT JOIN dw_stores ds ON ds.store_id = c.store_id
                    //LEFT JOIN eav_attribute_option_value days_worn ON option_id = days_worn_info AND days_worn.store_id = 0
                    //LEFT JOIN customer_created_in_map cim ON cim.customer_created_in = c.created_in
                    //LEFT JOIN customer_first_order fo ON fo.customer_id = c.customer_id
                    //LEFT JOIN dw_order_headers oh ON  oh.order_id = fo.order_id
                    //LEFT JOIN dw_stores ds2 ON oh.store_id = ds2.store_id;");
    
$installer->endSetup();
	 
