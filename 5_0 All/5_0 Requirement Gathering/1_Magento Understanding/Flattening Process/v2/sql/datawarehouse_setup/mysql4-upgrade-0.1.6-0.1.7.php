<?php
//proper table syntax
$installer = $this;
$installer->startSetup();

$dbname = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');

$installer->run("DROP TABLE IF EXISTS {$dbname}.dw_flat_category;");


$installer->run("CREATE TABLE IF NOT EXISTS {$dbname}.`dw_flat_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `promotional_product` tinyint(4) DEFAULT NULL,
  `modality` varchar(100) DEFAULT NULL,
  `modality_name` varchar(100) DEFAULT NULL,
  `modality_path` varchar(60) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `type_name` varchar(100) DEFAULT NULL,
  `type_path` varchar(60) NOT NULL,
  `feature` varchar(100) DEFAULT NULL,
  `feature_name` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `product_type` varchar(100) DEFAULT NULL,
  `product_type_name` varchar(100) DEFAULT NULL,
  `telesales_only` int(11) DEFAULT NULL,
  `feature_path` varchar(60) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=538 ;
");

$installer->run("INSERT INTO {$dbname}.`dw_flat_category` (`category_id`, `promotional_product`, `modality`, `modality_name`, `modality_path`, `type`, `type_name`, `type_path`, `feature`, `feature_name`, `category`, `product_type`, `product_type_name`, `telesales_only`, `feature_path`) VALUES
(1, 0, 'Daily', 'Dailies', '1/2/4/10', 'Spherical', 'NONE', '', 'Standard', 'NONE', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, ''),
(2, 0, 'Daily', 'Dailies', '1/2/4/10', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'Standard', 'NONE', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, ''),
(3, 0, 'Daily', 'Dailies', '1/2/4/10', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'Standard', 'NONE', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, ''),
(4, 0, 'Daily', 'Dailies', '1/2/4/10', 'Spherical', 'NONE', '', 'SiHy', 'Silicone Hydrogel', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, '1/2/4/211'),
(5, 0, 'Daily', 'Dailies', '1/2/4/10', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'SiHy', 'Silicone Hydrogel', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, '1/2/4/211'),
(6, 0, 'Daily', 'Dailies', '1/2/4/10', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'SiHy', 'Silicone Hydrogel', 'Dailies', 'Contact Lenses - Dailies', 'lens', 0, '1/2/4/211'),
(7, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Spherical', 'NONE', '', 'Standard', 'NONE', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
(8, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'Standard', 'NONE', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
(9, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'Standard', 'NONE', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
(10, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Spherical', 'NONE', '', 'SiHy', 'Silicone Hydrogel', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
(11, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'SiHy', 'Silicone Hydrogel', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
(12, 0, 'Two Weekly', 'Two-Weekly', '1/2/4/11', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'SiHy', 'Silicone Hydrogel', 'Two Weeklies', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
(13, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Spherical', 'NONE', '', 'Standard', 'NONE', 'Monthlies', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
(14, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'Standard', 'NONE', 'Torics for Astigmatism', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
(15, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'Standard', 'NONE', 'Multifocals', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
(16, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Multifocal Toric', 'Multifocal Toric Lenses', '1/2/4/14/177', 'Standard', 'NONE', 'Multifocals', 'Contact Lenses - Monthlies & Other', 'lens', 0, ''),
(17, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Spherical', 'NONE', '', 'SiHy', 'Silicone Hydrogel', 'Monthlies', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
(18, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Toric', 'Torics for Astigmatism', '1/2/4/13', 'SiHy', 'Silicone Hydrogel', 'Torics for Astigmatism', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
(19, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Multifocal', 'Multifocals', '1/2/4/14/233', 'SiHy', 'Silicone Hydrogel', 'Multifocals', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
(20, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Multifocal Toric', 'Multifocal Toric Lenses', '1/2/4/14/177', 'SiHy', 'Silicone Hydrogel', 'Multifocals', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/211'),
(21, 0, 'Daily', 'Dailies', '1/2/4/10', 'Spherical', 'Feshlook', '', 'Coloured', 'NONE', 'Colours', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/15'),
(22, 0, 'Monthly', 'Monthlies', '1/2/4/12', 'Spherical', 'Monthly Coloured', '', 'Coloured', 'NONE', 'Colours', 'Contact Lenses - Monthlies & Other', 'lens', 0, '1/2/4/15'),
(23, 0, '', 'NONE', '', '', 'NONE', '', '', 'NONE', 'Telesales Only', 'Contact Lenses - Monthlies & Other', 'lens', 1, ''),
(24, 0, '', 'NONE', '', '', 'Multi-purpose solutions', '1/2/17/19', '', 'NONE', 'Multi-purpose Solutions', 'Solutions & Eye Care', 'solution', 0, ''),
(25, 0, '', 'NONE', '', '', 'Hydrogen Peroxide', '1/2/17/20', '', 'NONE', 'Hydrogen Peroxide', 'Solutions & Eye Care', 'solution', 0, ''),
(26, 0, '', 'NONE', '', '', 'Gas Permeable Solutions', '1/2/17/21', '', 'NONE', 'Gas Permeable Solutions', 'Solutions & Eye Care', 'solution', 0, ''),
(27, 0, '', 'NONE', '', '', 'Travel Packs', '1/2/17/22', '', 'NONE', 'Travel Packs', 'Solutions & Eye Care', 'solution', 0, ''),
(28, 0, '', 'NONE', '', '', 'Cleaners & Salines', '1/2/17/29', '', 'NONE', 'Cleaners & Salines', 'Solutions & Eye Care', 'solution', 0, ''),
(29, 0, '', 'NONE', '', '', 'Eye Drops', '1/2/127/27', '', 'NONE', 'Eye Drops', 'Solutions & Eye Care', 'solution', 0, ''),
(30, 0, '', 'NONE', '', '', 'Eye Wash & Sprays', '1/2/127/101', '', 'NONE', 'Eye Wash & Sprays', 'Solutions & Eye Care', 'solution', 0, ''),
(31, 0, '', 'NONE', '', '', 'Dry Eyes', '1/2/127/26', '', 'NONE', 'Dry Eye Treatments', 'Solutions & Eye Care', 'solution', 0, ''),
(32, 0, '', 'NONE', '', '', 'Ocular Vitamins', '1/2/127/25', '', 'NONE', 'Ocular Vitamins', 'Solutions & Eye Care', 'solution', 0, ''),
(33, 0, '', 'NONE', '', '', 'Hygiene & Lid Care', '1/2/127/28', '', 'NONE', 'Hygiene & Lid Care', 'Solutions & Eye Care', 'solution', 0, ''),
(34, 0, '', 'NONE', '', '', 'Eye Care Cosmetics', '', '', 'NONE', 'Hypoallegenic Makeup', 'Solutions & Eye Care', 'solution', 0, ''),
(35, 0, '', 'NONE', '', '', 'Glasses', '', '', 'NONE', 'Glasses Frames', 'Glasses - Frame', 'glasses', 0, ''),
(36, 0, '', 'NONE', '', '', 'NONE', '', '', 'NONE', 'Glasses Lenses', 'Glasses - Lens', 'glasseslens', 0, ''),
(37, 0, '', 'NONE', '', '', 'NONE', '', '', 'NONE', 'Other', 'Other', 'other', 0, ''),
(501, 1, 'Daily', '', '1/2/4/10', 'Spherical', '', '', 'Standard', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, ''),
(502, 1, 'Daily', '', '1/2/4/10', 'Toric', '', '1/2/4/13', 'Standard', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, ''),
(503, 1, 'Daily', '', '1/2/4/10', 'Multifocal', '', '1/2/4/14/233', 'Standard', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, ''),
(504, 1, 'Daily', '', '1/2/4/10', 'Spherical', '', '', 'SiHy', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, '1/2/4/211'),
(505, 1, 'Daily', '', '1/2/4/10', 'Toric', '', '1/2/4/13', 'SiHy', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, '1/2/4/211'),
(506, 1, 'Daily', '', '1/2/4/10', 'Multifocal', '', '1/2/4/14/233', 'SiHy', '', '{Promo} Dailies', '{Promo} Contact Lenses - Dailies', '', 0, '1/2/4/211'),
(507, 1, 'Two Weekly', '', '1/2/4/11', 'Spherical', '', '', 'Standard', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
(508, 1, 'Two Weekly', '', '1/2/4/11', 'Toric', '', '1/2/4/13', 'Standard', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
(509, 1, 'Two Weekly', '', '1/2/4/11', 'Multifocal', '', '1/2/4/14/233', 'Standard', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
(510, 1, 'Two Weekly', '', '1/2/4/11', 'Spherical', '', '', 'SiHy', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
(511, 1, 'Two Weekly', '', '1/2/4/11', 'Toric', '', '1/2/4/13', 'SiHy', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
(512, 1, 'Two Weekly', '', '1/2/4/11', 'Multifocal', '', '1/2/4/14/233', 'SiHy', '', '{Promo} Two Weeklies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
(513, 1, 'Monthly', '', '1/2/4/12', 'Spherical', '', '', 'Standard', '', '{Promo} Monthlies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
(514, 1, 'Monthly', '', '1/2/4/12', 'Toric', '', '1/2/4/13', 'Standard', '', '{Promo} Torics for Astigmatism', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
(515, 1, 'Monthly', '', '1/2/4/12', 'Multifocal', '', '1/2/4/14/233', 'Standard', '', '{Promo} Multifocals', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
(516, 1, 'Monthly', '', '1/2/4/12', 'Multifocal Toric', '', '1/2/4/14/177', 'Standard', '', '{Promo} Multifocals', '{Promo} Contact Lenses - Monthlies & Other', '', 0, ''),
(517, 1, 'Monthly', '', '1/2/4/12', 'Spherical', '', '', 'SiHy', '', '{Promo} Monthlies', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
(518, 1, 'Monthly', '', '1/2/4/12', 'Toric', '', '1/2/4/13', 'SiHy', '', '{Promo} Torics for Astigmatism', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
(519, 1, 'Monthly', '', '1/2/4/12', 'Multifocal', '', '1/2/4/14/233', 'SiHy', '', '{Promo} Multifocals', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
(520, 1, 'Monthly', '', '1/2/4/12', 'Multifocal Toric', '', '1/2/4/14/177', 'SiHy', '', '{Promo} Multifocals', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/211'),
(521, 1, 'Daily', '', '1/2/4/10', 'Spherical', '', '', 'Coloured', '', '{Promo} Colours', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/15'),
(522, 1, 'Monthly', '', '1/2/4/12', 'Spherical', '', '', 'Coloured', '', '{Promo} Colours', '{Promo} Contact Lenses - Monthlies & Other', '', 0, '1/2/4/15'),
(523, 1, '', '', '', '', '', '', '', '', '{Promo} Telesales Only', '{Promo} Solutions & Eye Care', '', 0, ''),
(524, 1, '', '', '', '', '', '', '', '', '{Promo} Multi-purpose Solutions', '{Promo} Solutions & Eye Care', '', 0, ''),
(525, 1, '', '', '', '', '', '', '', '', '{Promo} Hydrogen Peroxide', '{Promo} Solutions & Eye Care', '', 0, ''),
(526, 1, '', '', '', '', '', '', '', '', '{Promo} Gas Permeable Solutions', '{Promo} Solutions & Eye Care', '', 0, ''),
(527, 1, '', '', '', '', '', '', '', '', '{Promo} Travel Packs', '{Promo} Solutions & Eye Care', '', 0, ''),
(528, 1, '', '', '', '', '', '', '', '', '{Promo} Cleaners & Salines', '{Promo} Solutions & Eye Care', '', 0, ''),
(529, 1, '', '', '', '', '', '', '', '', '{Promo} Eye Drops', '{Promo} Solutions & Eye Care', '', 0, ''),
(530, 1, '', '', '', '', '', '', '', '', '{Promo} Eye Wash & Sprays', '{Promo} Solutions & Eye Care', '', 0, ''),
(531, 1, '', '', '', '', '', '', '', '', '{Promo} Dry Eye Treatments', '{Promo} Solutions & Eye Care', '', 0, ''),
(532, 1, '', '', '', '', '', '', '', '', '{Promo} Ocular Vitamins', '{Promo} Solutions & Eye Care', '', 0, ''),
(533, 1, '', '', '', '', '', '', '', '', '{Promo} Hygiene & Lid Care', '{Promo} Solutions & Eye Care', '', 0, ''),
(534, 1, '', '', '', '', '', '', '', '', '{Promo} Hypoallegenic Makeup', '{Promo} Solutions & Eye Care', '', 0, ''),
(535, 1, '', '', '', '', '', '', '', '', '{Promo} Glasses Frames', '{Promo} Glasses - Frame', '', 0, ''),
(536, 1, '', '', '', '', '', '', '', '', '{Promo} Glasses Lenses', '{Promo} Glasses - Lens', '', 0, ''),
(537, 1, '', '', '', '', '', '', '', '', '{Promo} Other', '{Promo} Other', '', 0, '');");

    
$installer->endSetup();
	 
