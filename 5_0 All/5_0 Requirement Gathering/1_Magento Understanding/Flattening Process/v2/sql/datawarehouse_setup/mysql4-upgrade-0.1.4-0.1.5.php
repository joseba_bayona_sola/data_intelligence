<?php
//proper table syntax
$installer = $this;
$installer->startSetup();
$dbname = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');


$installer->run("UPDATE dw_stores SET store_type = 'Core UK' WHERE store_type = 'UK'");
$installer->run("UPDATE dw_stores_map SET store_type = 'Core UK' WHERE store_type = 'UK'");

$installer->run("CREATE OR REPLACE VIEW vw_categories AS
SELECT 	category_id, 
    promotional_product,
	modality, 
	`type`, 
	feature, 
	category, 
	product_type
FROM 
dw_flat_category;");

$installer->run("REPLACE INTO dw_payment_method_map VALUES ('imxpodpayment','Payment on Delivery');");



$query = "ALTER TABLE {$dbname}.{$this->getTable('salesrule/rule')}
          ADD COLUMN `channel` VARCHAR(255);
         ";

$installer->run($query);

$installer->run("REPLACE INTO dw_coupon_code_map VALUES 
('*REFER-A-FRIEND','','Refer A Friend'),
('*REFER-FRIEND','','Refer A Friend'),
('*REFER-FRIEND-IT','','Refer A Friend');");

$installer->run("UPDATE {$dbname}. `{$this->getTable('salesrule/rule')}` sr , dw_coupon_code_map dc , {$dbname}.`{$this->getTable('salesrule/coupon')}` src SET sr.channel =  dc.channel 
WHERE sr.`rule_id` = src.rule_id AND  dc.coupon_code = src.`code`;");


// coupon_/ bis channel
$installer->run("CREATE OR REPLACE VIEW vw_coupon_channel AS
SELECT `code` AS coupon_code,
	from_date AS start_date,
	to_date AS end_date,
	COALESCE(NULL,'') AS source_code,
	sr.channel AS channel,
	sr.postoptics_only_new_customer AS new_customer,
	cast(sr.description as char(8000)) description
	
FROM {$dbname}.salesrule_coupon sc    INNER JOIN {$dbname}.salesrule sr ON sc.rule_id = sr.rule_id;");
     
     
$installer->run("REPLACE INTO dw_bis_source_map VALUES
('adwords-br','Adwords'),
('adwords-branded-pages','Adwords'),
('adwords-nc','Adwords'),
('adwords-cl','Adwords'),
('adwords-np','Adwords'),
('TradeDoubler','Affiliates'),
('AffiliateWindow','Affiliates'),
('WebGains','Affiliates'),
('TradeDoublerKel','Affiliates'),
('adwords-sb','Adwords'),
('adwords-sp','Adwords'),
('emv-wc','Marketing'),
('adwords-s','Adwords'),
('adwords-cb','Adwords'),
('adwords-nc-ae','Adwords'),
('adwords-ncp','Adwords'),
('affiliates-af-za','Affiliates'),
('affiliates-af-td','Affiliates'),
('affnet','Affiliates'),
('googlebase','Google Shopping'),
('adwords-nc-sl','Adwords'),
('adwords-sp-sl','Adwords'),
('adwords-rm_np','Adwords'),
('adwords-c','Adwords'),
('adwords-ccl','Adwords'),
('adwords-cl/','Adwords'),
('adwords-nc/','Adwords'),
('adwords-br-tesco','Adwords'),
('googleshopping','Google Shopping'),
('adwords-br-daysoft','Adwords'),
('adwords-cl-219','Adwords'),
('adwords-sainsbury','Adwords'),
('adwords-manuf-cv','Adwords'),
('affiliates-za','Affiliates'),
('adwords-manuf-cp','Adwords'),
('adwords-cl-multi','Adwords'),
('adwords-brands-sols','Adwords'),
('adwords-manuf-bal','Adwords'),
('adwords-brands-alc','Adwords'),
('adwords-brands-lid','Adwords'),
('adwords-cl-toric','Adwords'),
('adwords-brands-acv','Adwords'),
('adwords-brands','Adwords'),
('adwords-manufacturers','Adwords'),
('adwords-br-002','Adwords'),
('adwords-branded-pages-sl','Adwords'),
('adwords-cl-day','Adwords'),
('adwords-tesco','Adwords'),
('adwords-asda','Adwords'),
('adwords-boots','Adwords'),
('adwords-cl-astig','Adwords'),
('adwords-br-dc','Adwords'),
('adwords-cl-color','Adwords'),
('ciao','Affiliates'),
('adwords-cllr','Adwords'),
('adwords-cl-lr-d','Adwords'),
('adwords-daysoft','Adwords'),
('adwords-cl-month','Adwords'),
('adwords-lensway','Adwords'),
('adwords-sl','Adwords'),
('pricerunner','Affiliates'),
('adwords-competitors','Adwords'),
('adwords-cl-ie','Adwords'),
('adwords-br-ie','Adwords'),
('adwords-cl-sl','Adwords'),
('adwords-sl-ie','Adwords'),
('adwords-nc-ie','Adwords');");


$installer->endSetup();
