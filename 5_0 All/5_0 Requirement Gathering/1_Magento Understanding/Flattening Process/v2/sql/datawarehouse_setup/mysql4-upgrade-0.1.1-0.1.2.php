<?php
//proper table syntax
$installer = $this;
$installer->startSetup();

$dbname = (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');

// coupon_/ bis channel
$installer->run("CREATE OR REPLACE VIEW vw_coupon_channel AS
SELECT `code` AS coupon_code,
	from_date AS start_date,
	to_date AS end_date,
	COALESCE(NULL,cm.business_source,'') AS source_code,
	COALESCE(NULL,cm.channel,'')  AS channel,
	sr.postoptics_only_new_customer AS new_customer,
	cast(sr.description as char(8000)) description
	
FROM {$dbname}.salesrule_coupon sc    INNER JOIN {$dbname}.salesrule sr ON sc.rule_id = sr.rule_id
                            LEFT JOIN dw_coupon_code_map cm ON cm.coupon_code = sc.code;");
     
$installer->endSetup();
	 
