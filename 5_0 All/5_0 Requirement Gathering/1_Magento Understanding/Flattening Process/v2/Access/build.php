<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once dirname(__FILE__).'/../../../../../../app/Mage.php';
Mage::app();

$model = Mage::getModel('datawarehouse/build');
$model->prepSync();
$model->syncGAData();
$model->buildStdtables();
$model->createSyncListTables();
$model->buildVatTables();
$model->buildProductPriceTable();
$model->buildExtraTables();
$model->buildCalTables();

$model->rebuildDwTables();
$model->completeSync();

if($argv[1] != 'skip') { 
	$status = $model->startPhocasBuild();
	echo("Starting Phocas Build Job Result: {$status}\n");
}

$model->flagStdTables();
$model->createDebugSummary();




?>
