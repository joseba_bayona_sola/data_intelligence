<?php
/* 
 * Observer for order shipment related tasks
 */

class Pointernal_Datawarehouse_Model_Observer
{
    public function addShipmentToDeleteList($observer) {
    	try{
	        $shipment = $observer->getEvent()->getShipment();
			$build = Mage::getModel('datawarehouse/build')->addDeletedShipment($shipment->getId());
		} catch(Exception $e) {
			Mage::log("Error deleting shipment {$e->getMessage()}",false,'datawarehouse.log');
			throw $e;	
		}
    }
}

?>
