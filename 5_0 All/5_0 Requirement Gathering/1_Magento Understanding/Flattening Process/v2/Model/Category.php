<?php
class Pointernal_Datawarehouse_Model_Category extends Mage_Core_Model_Abstract {
	
	protected function _construct() {
		$this->_init('datawarehouse/category');
		parent::_construct();
	}
	
	function _loadForProduct( $prod ) {
		
		return $this->getResource()->_loadForProduct( $prod );
	}
}