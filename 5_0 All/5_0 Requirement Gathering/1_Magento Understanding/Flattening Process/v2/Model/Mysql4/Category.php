<?php

class Pointernal_Datawarehouse_Model_Mysql4_Category extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct(){
        $this->_init('datawarehouse/category', 'category_id');
    }    
    
    
    function _loadForProduct( $prod ) {

    	
    	$product_type = $prod->getData('product_type');
    	$telesales_only = $prod->getData('telesales_only');
    	$promotional_product = $prod->getData('promotional_product');
    	
    	$cat_coll = $prod->getCategoryCollection();

    	foreach ($cat_coll as $k => $v) {
    		$cats[] = $v->getData('path');
    	}
    	
    	
    	$read = $this->_getReadAdapter();
     	$query =  $read->select()
    					->from('dw_flat_category')
    	
				    	->where("product_type_name = ?", $product_type )
				    	->where("telesales_only = ?", $telesales_only )
				    	->where("promotional_product = ?", $promotional_product )
     					;
     	
     	$q1 = null;
     	$q2 = null;
     	$q3 = null;
     	if (count($cats)>0) {
     	foreach ($cats as $k=>$v) {
     		$q1[] = ( "   \n'{$v}' like concat(modality_path, '%') ");
			$q2[] = ( "   \n'{$v}' like concat(type_path, '%')    ");
			$q3[] = ( "   \n'{$v}' like concat(feature_path, '%')  ");
     	}}	
     	
     	if (count($q1)) { 
     			$q1s = "\n ( modality_path='' OR ( modality_path!='' AND (". implode(' OR ', $q1) ."))) "; 
     			$query->where($q1s);
     	}
     	if (count($q2)) { 
     			$q2s = "\n ( type_path=''     OR ( type_path!=''     AND (". implode(' OR ', $q2) ."))) "; 
     			$query->where($q2s);
     	}
     	if (count($q3)) { 
     			$q3s = "\n ( feature_path=''  OR ( feature_path!=''  AND (". implode(' OR ', $q3) ."))) "; 
     			$query->where($q3s);
     	}
     	
     	
     	
     	
     	
     	$query->order(' IF(LENGTH(modality_path)>0,1,0) + IF(LENGTH(type_path)>0,1,0) + IF(LENGTH(feature_path)>0,1,0) DESC');
     	$query->limit(1);

    	$result = $read->fetchAll($query);
    	
    	foreach ($result as $k => $res) {
    		//$s = $res['category']. ' | '.$res['type_name']. ' | '.$res['product_type'];
    		$s = $res['product_type']. (($res['type_name']!='NONE')? ' - '.$res['type_name']:'');
    		//$s = $res['product_type']. ' | '.$res['type_name'];
    		return $s;
    	}
    	
    	//var_dump($result);
    	
    }
}
