<?php
class Pointernal_Datawarehouse_Model_Build extends Mage_Core_Model_Abstract {

  protected function _construct() {
        $this->_init('datawarehouse/build');
        parent::_construct();
    }
    
    public function buildStdtables() {
		$this->getResource()->buildStdTables();
	}

	public function flagStdTables() {
		$this->getResource()->flagStdTables();
	}

	public function createSyncListTables() {
		$this->getResource()->createSyncListTables();
	}
	
    public function buildVatTables() {
		$this->getResource()->buildVatTables();
	}
	
    public function buildExtraTables() {
		$this->getResource()->buildExtraTables();
		$this->getResource()->buildOrderMarketingTable();
	}
    
    public function buildCalTables() {
		$this->getResource()->buildCalTables();
	}
	
	 public function buildProductPriceTable() {
		$this->getResource()->buildProductPriceTable();
	}
    
    public function completeSync() {
		$this->getResource()->completeSync();
	}
    
    public function prepSync() {
		$this->getResource()->prepSync();
		$this->addConfigBisSource();
	}
    
    public function rebuildDwTables() {
		$this->getResource()->rebuildDwTables();
	}
    
    public function clearVatTables() {
       $this->getResource()->clearVatTables();
  	} 
    
    public function createDebugSummary() {
       $this->getResource()->createDebugSummary();
   	}
	
	public function addDeletedShipment($shipmentId) {
       $this->getResource()->addDeletedShipment($shipmentId);
   	}
	/**
	 * Call GA Sync module to copy analytics channel info errors ignore for this as monitoring will be handled elsewher
	 */
	public function syncGAData() {
		$exception = FALSE;
		$client = Mage::getModel('gasync/client');
		$stores = $client->getEnabledStores();
		foreach($stores as $storeId) {
			$client->setStoreId($storeId);
			try {
				$data = $client->syncData();
			} catch (Exception $e) {
				Mage::log($e->getMessage(), 0, 'datawarehouse.log');
				$exception = TRUE;
			}
			if (!$exception) {
				$this->getResource()->completeGASync();
			}

		}
	}
	
	public function addConfigBisSource() {
		$configBisChannels = Mage::getStoreConfig('dw/dw_creds/dw_bis_source',0);
		$configBisChannels = explode("\n", $configBisChannels);
		$invlaidEntries = array_keys($configBisChannels, false);
		foreach($invlaidEntries as $invalidKey => $invalid) {
			unset($configBisChannels[$invalidKey]);
		}

		if(is_array($configBisChannels) && count($configBisChannels)) {
			$configBisChannels = array_map(trim, $configBisChannels);
			foreach($configBisChannels as $configBisChannel) {
				$bisSourceChannel = explode('|', $configBisChannel);
				if(is_array($bisSourceChannel) && count($bisSourceChannel) == 2){
					$this->getResource()->addBisChannel(array('bis_source_code'=>$bisSourceChannel[0],'channel'=>$bisSourceChannel[1]));
				}
			}
		}
	}


	public function startPhocasBuild()
	{
		$DW_host = Mage::getStoreConfig('dw/dw_creds/in_ip',0);
		$DW_user = Mage::getStoreConfig('dw/dw_creds/dw_user',0);
		$DW_pass = Mage::helper('core')->decrypt(Mage::getStoreConfig('dw/dw_creds/dw_pass',0));

		$connection = mssql_connect($DW_host, $DW_user, $DW_pass);
		mssql_select_db(  'msdb',  $connection );

		$proc = mssql_init('sp_start_job', $connection);
		$jobname = 'Phocas Build';

		mssql_bind($proc,'@job_name',$jobname,SQLVARCHAR,false,false,30);
		mssql_execute($proc , 0);

		return mssql_get_last_message();
	}
}
?>
