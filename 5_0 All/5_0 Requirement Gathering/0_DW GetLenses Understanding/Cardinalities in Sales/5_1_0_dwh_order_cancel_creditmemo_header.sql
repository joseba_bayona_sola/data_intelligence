select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'

select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'CANCEL'

select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'CREDITMEMO'

select top 1000 oh.document_id document_id_o, oh.order_id, oh.order_no, ch.document_id document_id_c, cmh.document_id document_id_cm, 
	oh.document_type document_type_o, oh.document_date document_date_o, 
	ch.document_type document_type_c, ch.document_date document_date_c, 
	cmh.document_type document_type_cm, cmh.document_date document_date_cm, 
	count(ch.document_id) over (partition by oh.document_id) num_cancellations, 
	count(cmh.document_id) over (partition by oh.document_id) num_creditmemos
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	inner join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
	left join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CREDITMEMO') cmh on oh.order_id = cmh.order_id
order by oh.document_date, ch.document_date, cmh.document_date

-------------------------------------

select top 1000 count(*) over () num_tot,
	oh.document_id, count(ch.document_id) num_cancellations, count(cmh.document_id) num_creditmemos 
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	inner join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
	left join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CREDITMEMO') cmh on oh.order_id = cmh.order_id
group by oh.document_id
order by oh.document_id

select num_cancellations, num_creditmemos, count(*)
from
	(select count(*) over () num_tot,
		oh.document_id, count(distinct ch.document_id) num_cancellations, count(distinct cmh.document_id) num_creditmemos 
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
		left join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CREDITMEMO') cmh on oh.order_id = cmh.order_id
	group by oh.document_id) t
group by num_cancellations, num_creditmemos
order by num_cancellations, num_creditmemos;

-------------------------------------

select *
from
	(select oh.document_id document_id_o, oh.order_id, oh.order_no, ch.document_id document_id_c, cmh.document_id document_id_cm, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		ch.document_type document_type_c, ch.document_date document_date_c, 
		cmh.document_type document_type_cm, cmh.document_date document_date_cm, 
		count(ch.document_id) over (partition by oh.document_id) num_cancellations, 
		count(cmh.document_id) over (partition by oh.document_id) num_creditmemos
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
		left join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CREDITMEMO') cmh on oh.order_id = cmh.order_id) t
where num_creditmemos > 0
order by document_date_o, document_date_c, document_date_cm