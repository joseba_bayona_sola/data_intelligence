
select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'

select top 1000 document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
from DW_GetLenses.dbo.shipment_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'SHIPMENT'
	-- and shipment_id in (3977836, 3980246)

select top 1000 oh.document_id, oh.order_id, oh.order_no, sh.shipment_id, sh.shipment_no, 
	oh.document_type document_type_o, oh.document_date document_date_o, 
	sh.document_type document_type_s, sh.document_date document_date_s, 
	count(sh.shipment_id) over (partition by oh.document_id) num_shipments
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	left join
		(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
		from DW_GetLenses.dbo.shipment_headers 
		where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id
order by oh.document_date, sh.document_date

-------------------------------------

select top 1000 count(*) over () num_tot,
	oh.document_id, count(sh.shipment_id) num_shipments
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	left join
		(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
		from DW_GetLenses.dbo.shipment_headers 
		where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id
group by oh.document_id
order by oh.document_id

select num_tot, num_shipments, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		oh.document_id, count(sh.shipment_id) num_shipments
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		left join
			(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id
	group by oh.document_id) t
group by num_tot, num_shipments
order by num_tot, num_shipments;

-------------------------------------

select top 1000 *
from 
	(select oh.document_id, oh.order_id, oh.order_no, sh.shipment_id, sh.shipment_no, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		sh.document_type document_type_s, sh.document_date document_date_s, 
		count(sh.shipment_id) over (partition by oh.document_id) num_shipments
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		left join
			(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id) t
where num_shipments > 1
order by document_date_o, document_date_s

-------------------------------------

select top 1000 count(*) over () num_tot,
	sh.document_id, count(oh.order_id) num_orders
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'ORDER') oh
	right join
		(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
		from DW_GetLenses.dbo.shipment_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id
group by sh.document_id
order by sh.document_id

select num_tot, num_orders, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		sh.document_id, count(oh.order_id) num_orders
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'ORDER') oh
		right join
			(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.shipment_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id
	group by sh.document_id) t
group by num_tot, num_orders
order by num_tot, num_orders;
