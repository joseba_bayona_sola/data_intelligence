
select entity_id, increment_id, created_at
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at, order_id
from magento01.sales_flat_invoice
where created_at > CURDATE() - interval 365 day
limit 100;

select oh.entity_id entity_id_o, ih.entity_id entity_id_i, oh.increment_id increment_id_o, ih.increment_id increment_id_i, 
  oh.created_at created_at_o, ih.created_at created_at_i
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_invoice) ih on oh.entity_id = ih.order_id
where oh.entity_id = 3633888
limit 100;

-- ----------------------------------------------------------

select oh.entity_id entity_id_o, count(ih.entity_id) num_invoices
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_invoice) ih on oh.entity_id = ih.order_id
group by oh.entity_id
order by oh.entity_id
limit 100;

select num_invoices, count(*)
from
  (select oh.entity_id entity_id_o, count(ih.entity_id) num_invoices
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where created_at > CURDATE() - interval 365 day) oh
    left join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_invoice) ih on oh.entity_id = ih.order_id
  group by oh.entity_id) t
group by num_invoices
order by num_invoices;

-- ----------------------------------------------------------

select ih.entity_id entity_id_i, count(oh.entity_id) num_orders
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order) oh
  right join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_invoice
    where created_at > CURDATE() - interval 365 day) ih on oh.entity_id = ih.order_id
group by ih.entity_id
order by ih.entity_id
limit 100;

select num_orders, count(*)
from
  (select ih.entity_id entity_id_i, count(oh.entity_id) num_orders
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order) oh
    right join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_invoice
      where created_at > CURDATE() - interval 365 day) ih on oh.entity_id = ih.order_id
  group by ih.entity_id) t  
group by num_orders
order by num_orders;  