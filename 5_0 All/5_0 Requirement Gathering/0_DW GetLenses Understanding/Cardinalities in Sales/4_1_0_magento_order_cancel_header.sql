
select status, count(*)
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
group by status
order by status;

select state, count(*)
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
group by state
order by state;

-- ------------------------------------------------------------

select entity_id, increment_id, created_at
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at
from magento01.sales_flat_order
where status = 'canceled'
limit 100;

select oh.entity_id entity_id_o, ch.entity_id entity_id_c, oh.increment_id increment_id_o, ch.increment_id increment_id_c, 
  oh.created_at created_at_o, ch.created_at created_at_c
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  left join
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where status = 'canceled') ch on oh.entity_id = ch.entity_id
limit 100;

-- ----------------------------------------------------------

select oh.entity_id entity_id_o, count(ch.entity_id) num_cancellations
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  left join
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where status = 'canceled') ch on oh.entity_id = ch.entity_id 
group by oh.entity_id
order by oh.entity_id
limit 100;

select num_cancellations, count(*)
from
  (select oh.entity_id entity_id_o, count(ch.entity_id) num_cancellations
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where created_at > CURDATE() - interval 365 day) oh
    left join
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where status = 'canceled') ch on oh.entity_id = ch.entity_id 
  group by oh.entity_id) t
group by num_cancellations
order by num_cancellations;  
