select top 1000 count(*) over () num_tot,
	oh.document_id, count(ih.document_id) num_invoices, count(sh.document_id) num_shipments
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	inner join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
	left join
		(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
		from DW_GetLenses.dbo.invoice_headers 
		where document_type = 'INVOICE') ih on oh.order_id = ih.order_id
	left join
		(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
		from DW_GetLenses.dbo.shipment_headers 
		where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id
group by oh.document_id
order by oh.document_id

select num_tot, num_invoices, num_shipments, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		oh.document_id, count(ih.document_id) num_invoices, count(sh.document_id) num_shipments
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
		left join
			(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'INVOICE') ih on oh.order_id = ih.order_id
		left join
			(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id
	group by oh.document_id) t
group by num_tot, num_invoices, num_shipments
order by num_tot, num_invoices, num_shipments;

select *
from
	(select oh.document_id document_id_o, oh.order_id, oh.order_no, ch.document_id document_id_c, 
		ih.invoice_id, ih.invoice_no, sh.shipment_id, sh.shipment_no,
		oh.document_type document_type_o, oh.document_date document_date_o, 
		ch.document_type document_type_c, ch.document_date document_date_c, 
		ih.document_type document_type_i, ih.document_date document_date_i, 
		sh.document_type document_type_s, sh.document_date document_date_s, 
		count(ih.document_id) over (partition by oh.document_id) num_invoices, 
		count(sh.document_id) over (partition by oh.document_id) num_shipments
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
		left join
			(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'INVOICE') ih on oh.order_id = ih.order_id
		left join
			(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id) t
where num_invoices <> 0 or num_shipments <> 0
order by document_date_o, document_date_i, document_date_s;

--------------------------------------------------------------------------

select *
from DW_GetLenses.dbo.order_headers
where order_id in (4049602, 4265696);

select *
from DW_GetLenses.dbo.invoice_headers
where order_id in (4049602, 4265696);

select *
from DW_GetLenses.dbo.shipment_headers
where order_id in (4049602, 4265696);