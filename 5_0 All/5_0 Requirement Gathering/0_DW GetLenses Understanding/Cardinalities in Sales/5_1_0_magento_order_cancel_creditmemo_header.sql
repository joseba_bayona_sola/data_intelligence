select entity_id, increment_id, created_at
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at
from magento01.sales_flat_order
where status = 'canceled'
limit 100;

select entity_id, increment_id, created_at, order_id
from magento01.sales_flat_creditmemo
where created_at > CURDATE() - interval 365 day
limit 100;

select oh.entity_id entity_id_o, ch.entity_id entity_id_c, cmh.entity_id entity_id_cm, 
  oh.increment_id increment_id_o, ch.increment_id increment_id_c, cmh.increment_id increment_id_cm, 
  oh.created_at created_at_o, ch.created_at created_at_c, cmh.created_at created_at_cm
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  inner join
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where status = 'canceled') ch on oh.entity_id = ch.entity_id
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_creditmemo) cmh on oh.entity_id = cmh.order_id    
limit 100;

-- ----------------------------------------------------------

select oh.entity_id entity_id_o, 
  count(distinct ch.entity_id) num_cancellations, count(distinct cmh.entity_id) num_creditmemos
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  inner join
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where status = 'canceled') ch on oh.entity_id = ch.entity_id
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_creditmemo) cmh on oh.entity_id = cmh.order_id 
group by oh.entity_id
order by oh.entity_id
limit 100;   

select num_cancellations, num_creditmemos, count(*)
from
  (select oh.entity_id entity_id_o, 
    count(distinct ch.entity_id) num_cancellations, count(distinct cmh.entity_id) num_creditmemos
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where created_at > CURDATE() - interval 365 day) oh
    inner join
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where status = 'canceled') ch on oh.entity_id = ch.entity_id
    left join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_creditmemo) cmh on oh.entity_id = cmh.order_id 
  group by oh.entity_id) t
group by num_cancellations, num_creditmemos
order by num_cancellations, num_creditmemos;