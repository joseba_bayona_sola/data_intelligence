
select top 1000 oh.document_id, oh.order_id, oh.order_no, sh.shipment_id, sh.shipment_no, 
	oh.document_type document_type_o, oh.document_date document_date_o, 
	sh.document_type document_type_s, sh.document_date document_date_s, 
	count(sh.shipment_id) over (partition by oh.document_id) num_shipments, 

	oh.order_currency_code order_currency_code_o, sh.order_currency_code order_currency_code_s, 

	case when (oh.local_to_global_rate = sh.local_to_global_rate) then 1 else 0 end local_to_global_rate,
	oh.local_to_global_rate local_to_global_rate_o, sh.local_to_global_rate local_to_global_rate_s,
	
	case when (oh.local_subtotal_inc_vat = sh.local_subtotal_inc_vat) then 1 else 0 end local_subtotal_inc_vat,
	case when (abs(oh.local_subtotal_inc_vat - sh.local_subtotal_inc_vat) < 1) then 1 else 0 end local_subtotal_inc_vat_2,
	oh.local_subtotal_inc_vat local_subtotal_inc_vat_o, sh.local_subtotal_inc_vat local_subtotal_inc_vat_s,
	case when (oh.local_subtotal_exc_vat = sh.local_subtotal_exc_vat) then 1 else 0 end local_subtotal_exc_vat,	 
	case when (abs(oh.local_subtotal_exc_vat - sh.local_subtotal_exc_vat) < 1) then 1 else 0 end local_subtotal_exc_vat_2,
	oh.local_subtotal_exc_vat local_subtotal_exc_vat_o, sh.local_subtotal_exc_vat local_subtotal_exc_vat_s, 
	case when (oh.local_total_inc_vat = sh.local_total_inc_vat) then 1 else 0 end local_total_inc_vat,
	case when (abs(oh.local_total_inc_vat - sh.local_total_inc_vat) < 1) then 1 else 0 end local_total_inc_vat_2,
	oh.local_total_inc_vat local_total_inc_vat_o, sh.local_total_inc_vat local_total_inc_vat_s, 
	case when (oh.local_total_exc_vat = sh.local_total_exc_vat) then 1 else 0 end local_total_exc_vat,
	case when (abs(oh.local_total_exc_vat - sh.local_total_exc_vat) < 1) then 1 else 0 end local_total_exc_vat_2,
	oh.local_total_exc_vat local_total_exc_vat_o, sh.local_total_exc_vat local_total_exc_vat_s, 

	case when (oh.global_subtotal_inc_vat = sh.global_subtotal_inc_vat) then 1 else 0 end global_subtotal_inc_vat,
	case when (abs(oh.global_subtotal_inc_vat - sh.global_subtotal_inc_vat) < 1) then 1 else 0 end global_subtotal_inc_vat_2,
	oh.global_subtotal_inc_vat global_subtotal_inc_vat_o, sh.global_subtotal_inc_vat global_subtotal_inc_vat_s, 
	case when (oh.global_subtotal_exc_vat = sh.global_subtotal_exc_vat) then 1 else 0 end global_subtotal_exc_vat,
	case when (abs(oh.global_subtotal_exc_vat - sh.global_subtotal_exc_vat) < 1) then 1 else 0 end global_subtotal_exc_vat_2,
	oh.global_subtotal_exc_vat global_subtotal_exc_vat_o, sh.global_subtotal_exc_vat global_subtotal_exc_vat_s,
	case when (oh.global_total_inc_vat = sh.global_total_inc_vat) then 1 else 0 end global_total_inc_vat,	 
	case when (abs(oh.global_total_inc_vat - sh.global_total_inc_vat) < 1) then 1 else 0 end global_total_inc_vat_2,
	oh.global_total_inc_vat global_total_inc_vat_o, sh.global_total_inc_vat global_total_inc_vat_s, 
	case when (oh.global_total_exc_vat = sh.global_total_exc_vat) then 1 else 0 end global_total_exc_vat,
	case when (abs(oh.global_total_exc_vat - sh.global_total_exc_vat) < 1) then 1 else 0 end global_total_exc_vat_2,
	oh.global_total_exc_vat global_total_exc_vat_o, sh.global_total_exc_vat global_total_exc_vat_s
from
		(select document_id, order_id, order_no, document_type, document_date, 
			order_currency_code, local_to_global_rate,
			local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
			global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	inner join
		(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no, 
			order_currency_code, local_to_global_rate,
			local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
			global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
		from DW_GetLenses.dbo.shipment_headers 
		where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id
order by oh.document_date, sh.document_date

select num_shipments,
	local_to_global_rate,
	local_subtotal_inc_vat_2, local_subtotal_exc_vat_2, local_total_inc_vat_2, local_total_exc_vat_2, 
	global_subtotal_inc_vat_2, global_subtotal_exc_vat_2, global_total_inc_vat_2, global_total_exc_vat_2, count(*)
from
	(select oh.document_id, oh.order_id, oh.order_no, sh.shipment_id, sh.shipment_no, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		sh.document_type document_type_s, sh.document_date document_date_s, 
		count(sh.shipment_id) over (partition by oh.document_id) num_shipments, 

		oh.order_currency_code order_currency_code_o, sh.order_currency_code order_currency_code_s, 

		case when (oh.local_to_global_rate = sh.local_to_global_rate) then 1 else 0 end local_to_global_rate,
		oh.local_to_global_rate local_to_global_rate_o, sh.local_to_global_rate local_to_global_rate_s,
	
		case when (oh.local_subtotal_inc_vat = sh.local_subtotal_inc_vat) then 1 else 0 end local_subtotal_inc_vat,
		case when (abs(oh.local_subtotal_inc_vat - sh.local_subtotal_inc_vat) < 1) then 1 else 0 end local_subtotal_inc_vat_2,
		oh.local_subtotal_inc_vat local_subtotal_inc_vat_o, sh.local_subtotal_inc_vat local_subtotal_inc_vat_s,
		case when (oh.local_subtotal_exc_vat = sh.local_subtotal_exc_vat) then 1 else 0 end local_subtotal_exc_vat,	 
		case when (abs(oh.local_subtotal_exc_vat - sh.local_subtotal_exc_vat) < 1) then 1 else 0 end local_subtotal_exc_vat_2,
		oh.local_subtotal_exc_vat local_subtotal_exc_vat_o, sh.local_subtotal_exc_vat local_subtotal_exc_vat_s, 
		case when (oh.local_total_inc_vat = sh.local_total_inc_vat) then 1 else 0 end local_total_inc_vat,
		case when (abs(oh.local_total_inc_vat - sh.local_total_inc_vat) < 1) then 1 else 0 end local_total_inc_vat_2,
		oh.local_total_inc_vat local_total_inc_vat_o, sh.local_total_inc_vat local_total_inc_vat_s, 
		case when (oh.local_total_exc_vat = sh.local_total_exc_vat) then 1 else 0 end local_total_exc_vat,
		case when (abs(oh.local_total_exc_vat - sh.local_total_exc_vat) < 1) then 1 else 0 end local_total_exc_vat_2,
		oh.local_total_exc_vat local_total_exc_vat_o, sh.local_total_exc_vat local_total_exc_vat_s, 

		case when (oh.global_subtotal_inc_vat = sh.global_subtotal_inc_vat) then 1 else 0 end global_subtotal_inc_vat,
		case when (abs(oh.global_subtotal_inc_vat - sh.global_subtotal_inc_vat) < 1) then 1 else 0 end global_subtotal_inc_vat_2,
		oh.global_subtotal_inc_vat global_subtotal_inc_vat_o, sh.global_subtotal_inc_vat global_subtotal_inc_vat_s, 
		case when (oh.global_subtotal_exc_vat = sh.global_subtotal_exc_vat) then 1 else 0 end global_subtotal_exc_vat,
		case when (abs(oh.global_subtotal_exc_vat - sh.global_subtotal_exc_vat) < 1) then 1 else 0 end global_subtotal_exc_vat_2,
		oh.global_subtotal_exc_vat global_subtotal_exc_vat_o, sh.global_subtotal_exc_vat global_subtotal_exc_vat_s,
		case when (oh.global_total_inc_vat = sh.global_total_inc_vat) then 1 else 0 end global_total_inc_vat,	 
		case when (abs(oh.global_total_inc_vat - sh.global_total_inc_vat) < 1) then 1 else 0 end global_total_inc_vat_2,
		oh.global_total_inc_vat global_total_inc_vat_o, sh.global_total_inc_vat global_total_inc_vat_s, 
		case when (oh.global_total_exc_vat = sh.global_total_exc_vat) then 1 else 0 end global_total_exc_vat,
		case when (abs(oh.global_total_exc_vat - sh.global_total_exc_vat) < 1) then 1 else 0 end global_total_exc_vat_2,
		oh.global_total_exc_vat global_total_exc_vat_o, sh.global_total_exc_vat global_total_exc_vat_s
	from
			(select document_id, order_id, order_no, document_type, document_date, 
				order_currency_code, local_to_global_rate,
				local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
				global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no, 
				order_currency_code, local_to_global_rate,
				local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
				global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id) t
group by num_shipments,
	local_to_global_rate,
	local_subtotal_inc_vat_2, local_subtotal_exc_vat_2, local_total_inc_vat_2, local_total_exc_vat_2, 
	global_subtotal_inc_vat_2, global_subtotal_exc_vat_2, global_total_inc_vat_2, global_total_exc_vat_2
order by num_shipments,
	local_to_global_rate,
	local_subtotal_inc_vat_2, local_subtotal_exc_vat_2, local_total_inc_vat_2, local_total_exc_vat_2, 
	global_subtotal_inc_vat_2, global_subtotal_exc_vat_2, global_total_inc_vat_2, global_total_exc_vat_2


select *
from
	(select oh.document_id, oh.order_id, oh.order_no, sh.shipment_id, sh.shipment_no, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		sh.document_type document_type_s, sh.document_date document_date_s, 
		count(sh.shipment_id) over (partition by oh.document_id) num_shipments, 

		oh.order_currency_code order_currency_code_o, sh.order_currency_code order_currency_code_s, 

		case when (oh.local_to_global_rate = sh.local_to_global_rate) then 1 else 0 end local_to_global_rate,
		oh.local_to_global_rate local_to_global_rate_o, sh.local_to_global_rate local_to_global_rate_s,
	
		case when (oh.local_subtotal_inc_vat = sh.local_subtotal_inc_vat) then 1 else 0 end local_subtotal_inc_vat,
		case when (abs(oh.local_subtotal_inc_vat - sh.local_subtotal_inc_vat) < 1) then 1 else 0 end local_subtotal_inc_vat_2,
		oh.local_subtotal_inc_vat local_subtotal_inc_vat_o, sh.local_subtotal_inc_vat local_subtotal_inc_vat_s,
		case when (oh.local_subtotal_exc_vat = sh.local_subtotal_exc_vat) then 1 else 0 end local_subtotal_exc_vat,	 
		case when (abs(oh.local_subtotal_exc_vat - sh.local_subtotal_exc_vat) < 1) then 1 else 0 end local_subtotal_exc_vat_2,
		oh.local_subtotal_exc_vat local_subtotal_exc_vat_o, sh.local_subtotal_exc_vat local_subtotal_exc_vat_s, 
		case when (oh.local_total_inc_vat = sh.local_total_inc_vat) then 1 else 0 end local_total_inc_vat,
		case when (abs(oh.local_total_inc_vat - sh.local_total_inc_vat) < 1) then 1 else 0 end local_total_inc_vat_2,
		oh.local_total_inc_vat local_total_inc_vat_o, sh.local_total_inc_vat local_total_inc_vat_s, 
		case when (oh.local_total_exc_vat = sh.local_total_exc_vat) then 1 else 0 end local_total_exc_vat,
		case when (abs(oh.local_total_exc_vat - sh.local_total_exc_vat) < 1) then 1 else 0 end local_total_exc_vat_2,
		oh.local_total_exc_vat local_total_exc_vat_o, sh.local_total_exc_vat local_total_exc_vat_s, 

		case when (oh.global_subtotal_inc_vat = sh.global_subtotal_inc_vat) then 1 else 0 end global_subtotal_inc_vat,
		case when (abs(oh.global_subtotal_inc_vat - sh.global_subtotal_inc_vat) < 1) then 1 else 0 end global_subtotal_inc_vat_2,
		oh.global_subtotal_inc_vat global_subtotal_inc_vat_o, sh.global_subtotal_inc_vat global_subtotal_inc_vat_s, 
		case when (oh.global_subtotal_exc_vat = sh.global_subtotal_exc_vat) then 1 else 0 end global_subtotal_exc_vat,
		case when (abs(oh.global_subtotal_exc_vat - sh.global_subtotal_exc_vat) < 1) then 1 else 0 end global_subtotal_exc_vat_2,
		oh.global_subtotal_exc_vat global_subtotal_exc_vat_o, sh.global_subtotal_exc_vat global_subtotal_exc_vat_s,
		case when (oh.global_total_inc_vat = sh.global_total_inc_vat) then 1 else 0 end global_total_inc_vat,	 
		case when (abs(oh.global_total_inc_vat - sh.global_total_inc_vat) < 1) then 1 else 0 end global_total_inc_vat_2,
		oh.global_total_inc_vat global_total_inc_vat_o, sh.global_total_inc_vat global_total_inc_vat_s, 
		case when (oh.global_total_exc_vat = sh.global_total_exc_vat) then 1 else 0 end global_total_exc_vat,
		case when (abs(oh.global_total_exc_vat - sh.global_total_exc_vat) < 1) then 1 else 0 end global_total_exc_vat_2,
		oh.global_total_exc_vat global_total_exc_vat_o, sh.global_total_exc_vat global_total_exc_vat_s
	from
			(select document_id, order_id, order_no, document_type, document_date, 
				order_currency_code, local_to_global_rate,
				local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
				global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no, 
				order_currency_code, local_to_global_rate,
				local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
				global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'SHIPMENT') sh on oh.order_id = sh.order_id) t
where num_shipments = 1 and 
	(local_subtotal_inc_vat_2 = 0 or local_subtotal_exc_vat_2 = 0 or local_total_inc_vat_2 = 0 or local_total_exc_vat_2 = 0 or 
	global_subtotal_inc_vat_2 = 0 or global_subtotal_exc_vat_2 = 0 or global_total_inc_vat_2 = 0 or global_total_exc_vat_2 = 0) 
order by document_date_o, document_date_s 