
select top 1000 document_id, shipment_id, shipment_no, document_type, document_date
from DW_GetLenses.dbo.shipment_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'SHIPMENT'

select top 1000 document_id, shipment_id, shipment_no, document_type, document_date
from DW_GetLenses.dbo.shipment_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'CREDITMEMO'

select top 1000 sh.document_id document_id_s, sh.shipment_id, sh.shipment_no, ch.document_id document_id_c, 
	sh.document_type document_type_s, sh.document_date document_date_s, 
	ch.document_type document_type_c, ch.document_date document_date_c, 
	count(ch.document_id) over (partition by sh.document_id) num_creditmemos
from
		(select document_id, shipment_id, shipment_no, document_type, document_date
		from DW_GetLenses.dbo.shipment_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'SHIPMENT') sh
	left join
		(select document_id, shipment_id, shipment_no, document_type, document_date
		from DW_GetLenses.dbo.shipment_headers 
		where document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
order by sh.document_date, ch.document_date

-------------------------------------

select top 1000 count(*) over () num_tot,
	sh.document_id, count(ch.document_id) num_creditmemos
from
		(select document_id, shipment_id, shipment_no, document_type, document_date
		from DW_GetLenses.dbo.shipment_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'SHIPMENT') sh
	left join
		(select document_id, shipment_id, shipment_no, document_type, document_date
		from DW_GetLenses.dbo.shipment_headers 
		where document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
group by sh.document_id
order by sh.document_id

select num_tot, num_creditmemos, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		sh.document_id, count(ch.document_id) num_creditmemos
	from
			(select document_id, shipment_id, shipment_no, document_type, document_date
			from DW_GetLenses.dbo.shipment_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'SHIPMENT') sh
		left join
			(select document_id, shipment_id, shipment_no, document_type, document_date
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
	group by sh.document_id) t
group by num_tot, num_creditmemos
order by num_tot, num_creditmemos;

-------------------------------------

select *
from
	(select sh.document_id document_id_s, sh.shipment_id, sh.shipment_no, ch.document_id document_id_c, 
		sh.document_type document_type_s, sh.document_date document_date_s, 
		ch.document_type document_type_c, ch.document_date document_date_c, 
		count(ch.document_id) over (partition by sh.document_id) num_creditmemos
	from
			(select document_id, shipment_id, shipment_no, document_type, document_date
			from DW_GetLenses.dbo.shipment_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'SHIPMENT') sh
		left join
			(select document_id, shipment_id, shipment_no, document_type, document_date
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id) t
--where num_creditmemos = 1
where num_creditmemos > 1
order by t.document_date_s, t.document_date_c

-------------------------------------

select top 1000 count(*) over () num_tot,
	ch.document_id, count(sh.document_id) num_shipments
from
		(select document_id, shipment_id, shipment_no, document_type, document_date
		from DW_GetLenses.dbo.shipment_headers 
		where document_type = 'SHIPMENT') sh
	right join
		(select document_id, shipment_id, shipment_no, document_type, document_date
		from DW_GetLenses.dbo.shipment_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
group by ch.document_id
order by ch.document_id

select num_tot, num_shipments, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		ch.document_id, count(sh.document_id) num_shipments
	from
			(select document_id, shipment_id, shipment_no, document_type, document_date
			from DW_GetLenses.dbo.shipment_headers 
			where document_type = 'SHIPMENT') sh
		right join
			(select document_id, shipment_id, shipment_no, document_type, document_date
			from DW_GetLenses.dbo.shipment_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
	group by ch.document_id) t
group by num_tot, num_shipments
order by num_tot, num_shipments;