
select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'

select top 1000 oh.document_id, oh.order_id, oh.order_no, oh.document_type, oh.document_date, 
	ol.line_id, ol.product_id, ol.name, ol.sku, 
	count(ol.line_id) over (partition by oh.document_id) num_lines
from 
		DW_GetLenses.dbo.order_headers oh
	left join
		DW_GetLenses.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where oh.document_date > GETUTCDATE() - 365
	and oh.document_type = 'ORDER'
order by oh.document_id, ol.line_id

-------------------------------------

select top 1000  count(*) over () num_tot,
	oh.document_id, count(ol.line_id) num
from 
		DW_GetLenses.dbo.order_headers oh
	left join
		DW_GetLenses.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where oh.document_date > GETUTCDATE() - 365
	and oh.document_type = 'ORDER'
group by oh.document_id
order by oh.document_id;

select num_tot, num_lines, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot, oh.document_id, count(ol.line_id) num_lines
	from 
			DW_GetLenses.dbo.order_headers oh
		left join
			DW_GetLenses.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
	where oh.document_date > GETUTCDATE() - 365
		and oh.document_type = 'ORDER'
	group by oh.document_id) t
group by num_tot, num_lines
order by num_tot, num_lines

-------------------------------------

select top 1000 *
from 
	(select oh.document_id, oh.order_id, oh.order_no, oh.document_type, oh.document_date, 
		ol.line_id, ol.product_id, ol.name, ol.sku, 
		count(ol.line_id) over (partition by oh.document_id) num_lines
	from 
			DW_GetLenses.dbo.order_headers oh
		left join
			DW_GetLenses.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
	where oh.document_date > GETUTCDATE() - 365
		and oh.document_type = 'ORDER') t
where num_lines = 15
order by document_id, line_id
