
select entity_id, increment_id, created_at
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
limit 100;

select oh.entity_id, oh.increment_id, oh.created_at, 
  ol.item_id, ol.product_id, ol.name, ol.sku
from 
  magento01.sales_flat_order oh
left join 
  magento01.sales_flat_order_item ol on oh.entity_id = ol.order_id
where oh.created_at > CURDATE() - interval 365 day
  and oh.entity_id = 3632971
order by oh.entity_id, ol.item_id
limit 1000;

-- ----------------------------------------------------------

select oh.entity_id, count(ol.item_id) num
from 
  magento01.sales_flat_order oh
left join 
  magento01.sales_flat_order_item ol on oh.entity_id = ol.order_id
where oh.created_at > CURDATE() - interval 365 day
group by oh.entity_id
order by oh.entity_id
limit 1000;

select num_lines, count(*) num
from
  (select oh.entity_id, count(ol.item_id) num_lines
  from 
    magento01.sales_flat_order oh
  left join 
    magento01.sales_flat_order_item ol on oh.entity_id = ol.order_id
  where oh.created_at > CURDATE() - interval 365 day
  group by oh.entity_id) t
group by num_lines
order by num_lines;

