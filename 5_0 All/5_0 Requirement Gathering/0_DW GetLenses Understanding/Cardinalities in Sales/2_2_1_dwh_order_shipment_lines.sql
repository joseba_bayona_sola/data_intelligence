
select top 1000 oh.document_id, oh.order_id, oh.order_no, oh.document_type, oh.document_date, 
	ol.line_id, ol.product_id, ol.name, ol.sku, 
	count(ol.line_id) over (partition by oh.document_id) num_lines
from 
		DW_GetLenses.dbo.order_headers oh
	left join
		DW_GetLenses.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where oh.document_date > GETUTCDATE() - 365
	and oh.document_type = 'ORDER'
	and oh.order_id in (3704572, 4670235, 4169566, 3633888)
order by oh.document_id, ol.line_id

select top 1000 sh.document_id, sh.shipment_id, sh.shipment_no, sh.document_type, sh.document_date, sh.order_id, sh.order_no, 
	sl.line_id, sl.product_id, sl.name, sl.sku, 
	count(sl.line_id) over (partition by sh.document_id) num_lines
from 
		DW_GetLenses.dbo.shipment_headers sh
	left join
		DW_GetLenses.dbo.shipment_lines sl on sh.document_id = sl.document_id and sh.document_type = sl.document_type		
where sh.document_date > GETUTCDATE() - 365
	and sh.document_type = 'SHIPMENT'
	and sh.order_id in (3704572, 4670235, 4169566, 3633888)
order by sh.document_id, sl.line_id

	on oh.order_id = sh.order_id + ??? (ol.line_id <> sl.line_id) / ol.sku = sl.sku and RANK (in case Order with 2 lines for same product SKU)
		sh.document_id = sl.document_id and sh.document_type = sl.document_type