
select top 1000 document_id, order_id, order_no, document_type, document_date, 
	order_currency_code, local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'

select top 1000 document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
	order_currency_code, local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
from DW_GetLenses.dbo.invoice_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'INVOICE'

select top 1000 oh.document_id, oh.order_id, oh.order_no, ih.invoice_id, ih.invoice_no, 
	oh.document_type document_type_o, oh.document_date document_date_o, 
	ih.document_type document_type_i, ih.document_date document_date_i, 
	count(ih.invoice_id) over (partition by oh.document_id) num_invoices, 

	oh.order_currency_code order_currency_code_o, ih.order_currency_code order_currency_code_i, 

	case when (oh.local_to_global_rate = ih.local_to_global_rate) then 1 else 0 end local_to_global_rate,
	oh.local_to_global_rate local_to_global_rate_o, ih.local_to_global_rate local_to_global_rate_i,
	
	case when (oh.local_subtotal_inc_vat = ih.local_subtotal_inc_vat) then 1 else 0 end local_subtotal_inc_vat,
	oh.local_subtotal_inc_vat local_subtotal_inc_vat_o, ih.local_subtotal_inc_vat local_subtotal_inc_vat_i,
	case when (oh.local_subtotal_exc_vat = ih.local_subtotal_exc_vat) then 1 else 0 end local_subtotal_exc_vat,	 
	oh.local_subtotal_exc_vat local_subtotal_exc_vat_o, ih.local_subtotal_exc_vat local_subtotal_exc_vat_i, 
	case when (oh.local_total_inc_vat = ih.local_total_inc_vat) then 1 else 0 end local_total_inc_vat,
	oh.local_total_inc_vat local_total_inc_vat_o, ih.local_total_inc_vat local_total_inc_vat_i, 
	case when (oh.local_total_exc_vat = ih.local_total_exc_vat) then 1 else 0 end local_total_exc_vat,
	oh.local_total_exc_vat local_total_exc_vat_o, ih.local_total_exc_vat local_total_exc_vat_i, 

	case when (oh.global_subtotal_inc_vat = ih.global_subtotal_inc_vat) then 1 else 0 end global_subtotal_inc_vat,
	oh.global_subtotal_inc_vat global_subtotal_inc_vat_o, ih.global_subtotal_inc_vat global_subtotal_inc_vat_i, 
	case when (oh.global_subtotal_exc_vat = ih.global_subtotal_exc_vat) then 1 else 0 end global_subtotal_exc_vat,
	oh.global_subtotal_exc_vat global_subtotal_exc_vat_o, ih.global_subtotal_exc_vat global_subtotal_exc_vat_i,
	case when (oh.global_total_inc_vat = ih.global_total_inc_vat) then 1 else 0 end global_total_inc_vat,	 
	oh.global_total_inc_vat global_total_inc_vat_o, ih.global_total_inc_vat global_total_inc_vat_i, 
	case when (oh.global_total_exc_vat = ih.global_total_exc_vat) then 1 else 0 end global_total_exc_vat,
	oh.global_total_exc_vat global_total_exc_vat_o, ih.global_total_exc_vat global_total_exc_vat_i
from
		(select document_id, order_id, order_no, document_type, document_date, 
			order_currency_code, local_to_global_rate,
			local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
			global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	inner join
		(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no, 
			order_currency_code, local_to_global_rate,
			local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
			global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
		from DW_GetLenses.dbo.invoice_headers 
		where document_type = 'INVOICE') ih on oh.order_id = ih.order_id
order by oh.document_date, ih.document_date

---------------------------------------------------------------------------------------------------------------

select local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat, count(*)
from
	(select oh.document_id, oh.order_id, oh.order_no, ih.invoice_id, ih.invoice_no, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		ih.document_type document_type_i, ih.document_date document_date_i, 
		count(ih.invoice_id) over (partition by oh.document_id) num_invoices, 

		oh.order_currency_code order_currency_code_o, ih.order_currency_code order_currency_code_i, 

		case when (oh.local_to_global_rate = ih.local_to_global_rate) then 1 else 0 end local_to_global_rate,
		oh.local_to_global_rate local_to_global_rate_o, ih.local_to_global_rate local_to_global_rate_i,
	
		case when (oh.local_subtotal_inc_vat = ih.local_subtotal_inc_vat) then 1 else 0 end local_subtotal_inc_vat,
		oh.local_subtotal_inc_vat local_subtotal_inc_vat_o, ih.local_subtotal_inc_vat local_subtotal_inc_vat_i,
		case when (oh.local_subtotal_exc_vat = ih.local_subtotal_exc_vat) then 1 else 0 end local_subtotal_exc_vat,	 
		oh.local_subtotal_exc_vat local_subtotal_exc_vat_o, ih.local_subtotal_exc_vat local_subtotal_exc_vat_i, 
		case when (oh.local_total_inc_vat = ih.local_total_inc_vat) then 1 else 0 end local_total_inc_vat,
		oh.local_total_inc_vat local_total_inc_vat_o, ih.local_total_inc_vat local_total_inc_vat_i, 
		case when (oh.local_total_exc_vat = ih.local_total_exc_vat) then 1 else 0 end local_total_exc_vat,
		oh.local_total_exc_vat local_total_exc_vat_o, ih.local_total_exc_vat local_total_exc_vat_i, 

		case when (oh.global_subtotal_inc_vat = ih.global_subtotal_inc_vat) then 1 else 0 end global_subtotal_inc_vat,
		oh.global_subtotal_inc_vat global_subtotal_inc_vat_o, ih.global_subtotal_inc_vat global_subtotal_inc_vat_i, 
		case when (oh.global_subtotal_exc_vat = ih.global_subtotal_exc_vat) then 1 else 0 end global_subtotal_exc_vat,
		oh.global_subtotal_exc_vat global_subtotal_exc_vat_o, ih.global_subtotal_exc_vat global_subtotal_exc_vat_i,
		case when (oh.global_total_inc_vat = ih.global_total_inc_vat) then 1 else 0 end global_total_inc_vat,	 
		oh.global_total_inc_vat global_total_inc_vat_o, ih.global_total_inc_vat global_total_inc_vat_i, 
		case when (oh.global_total_exc_vat = ih.global_total_exc_vat) then 1 else 0 end global_total_exc_vat,
		oh.global_total_exc_vat global_total_exc_vat_o, ih.global_total_exc_vat global_total_exc_vat_i
	from
			(select document_id, order_id, order_no, document_type, document_date, 
				order_currency_code, local_to_global_rate,
				local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
				global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no, 
				order_currency_code, local_to_global_rate,
				local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
				global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'INVOICE') ih on oh.order_id = ih.order_id) t
group by local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
order by local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat;

---------------------------------------------------------------------------------------------------------------

select top 1000 *
from
	(select oh.document_id, oh.order_id, oh.order_no, ih.invoice_id, ih.invoice_no, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		ih.document_type document_type_i, ih.document_date document_date_i, 
		count(ih.invoice_id) over (partition by oh.document_id) num_invoices, 

		oh.order_currency_code order_currency_code_o, ih.order_currency_code order_currency_code_i, 

		case when (oh.local_to_global_rate = ih.local_to_global_rate) then 1 else 0 end local_to_global_rate,
		oh.local_to_global_rate local_to_global_rate_o, ih.local_to_global_rate local_to_global_rate_i,
	
		case when (oh.local_subtotal_inc_vat = ih.local_subtotal_inc_vat) then 1 else 0 end local_subtotal_inc_vat,
		oh.local_subtotal_inc_vat local_subtotal_inc_vat_o, ih.local_subtotal_inc_vat local_subtotal_inc_vat_i,
		case when (oh.local_subtotal_exc_vat = ih.local_subtotal_exc_vat) then 1 else 0 end local_subtotal_exc_vat,	 
		oh.local_subtotal_exc_vat local_subtotal_exc_vat_o, ih.local_subtotal_exc_vat local_subtotal_exc_vat_i, 
		case when (oh.local_total_inc_vat = ih.local_total_inc_vat) then 1 else 0 end local_total_inc_vat,
		oh.local_total_inc_vat local_total_inc_vat_o, ih.local_total_inc_vat local_total_inc_vat_i, 
		case when (oh.local_total_exc_vat = ih.local_total_exc_vat) then 1 else 0 end local_total_exc_vat,
		oh.local_total_exc_vat local_total_exc_vat_o, ih.local_total_exc_vat local_total_exc_vat_i, 

		case when (oh.global_subtotal_inc_vat = ih.global_subtotal_inc_vat) then 1 else 0 end global_subtotal_inc_vat,
		oh.global_subtotal_inc_vat global_subtotal_inc_vat_o, ih.global_subtotal_inc_vat global_subtotal_inc_vat_i, 
		case when (oh.global_subtotal_exc_vat = ih.global_subtotal_exc_vat) then 1 else 0 end global_subtotal_exc_vat,
		oh.global_subtotal_exc_vat global_subtotal_exc_vat_o, ih.global_subtotal_exc_vat global_subtotal_exc_vat_i,
		case when (oh.global_total_inc_vat = ih.global_total_inc_vat) then 1 else 0 end global_total_inc_vat,	 
		oh.global_total_inc_vat global_total_inc_vat_o, ih.global_total_inc_vat global_total_inc_vat_i, 
		case when (oh.global_total_exc_vat = ih.global_total_exc_vat) then 1 else 0 end global_total_exc_vat,
		oh.global_total_exc_vat global_total_exc_vat_o, ih.global_total_exc_vat global_total_exc_vat_i
	from
			(select document_id, order_id, order_no, document_type, document_date, 
				order_currency_code, local_to_global_rate,
				local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
				global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no, 
				order_currency_code, local_to_global_rate,
				local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
				global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'INVOICE') ih on oh.order_id = ih.order_id) t
where local_to_global_rate = 0 or 
	local_subtotal_inc_vat = 0 or local_subtotal_exc_vat = 0 or local_total_inc_vat = 0 or local_total_exc_vat = 0 or 
	global_subtotal_inc_vat = 0 or global_subtotal_exc_vat = 0 or global_total_inc_vat = 0 or global_total_exc_vat = 0 
order by local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat, 
	document_date_o, document_date_i