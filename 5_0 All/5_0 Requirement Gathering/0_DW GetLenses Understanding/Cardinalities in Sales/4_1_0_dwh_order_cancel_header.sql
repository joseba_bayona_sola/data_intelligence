
select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'

select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'CANCEL'

select top 1000 oh.document_id document_id_o, oh.order_id, oh.order_no, ch.document_id document_id_c, 
	oh.document_type document_type_o, oh.document_date document_date_o, 
	ch.document_type document_type_c, ch.document_date document_date_c, 
	count(ch.document_id) over (partition by oh.document_id) num_cancellations
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	left join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
order by oh.document_date, ch.document_date

-------------------------------------

select top 1000 count(*) over () num_tot,
	oh.document_id, count(ch.document_id) num_cancellations
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	left join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
group by oh.document_id
order by oh.document_id

select num_tot, num_cancellations, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		oh.document_id, count(ch.document_id) num_cancellations
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		left join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CANCEL') ch on oh.order_id = ch.order_id
	group by oh.document_id) t
group by num_tot, num_cancellations
order by num_tot, num_cancellations;

-------------------------------------

select top 1000 *
from
	(select oh.document_id document_id_o, oh.order_id, oh.order_no, ch.document_id document_id_c, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		ch.document_type document_type_c, ch.document_date document_date_c, 
		count(ch.document_id) over (partition by oh.document_id) num_cancellations
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		left join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CANCEL') ch on oh.order_id = ch.order_id) t
where num_cancellations = 1
order by t.document_date_o, t.document_date_c

-------------------------------------

select top 1000 count(*) over () num_tot,
	ch.document_id, count(oh.document_id) num_orders
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'ORDER') oh
	right join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'CANCEL') ch on oh.order_id = ch.order_id
group by ch.document_id
order by ch.document_id

select num_tot, num_orders, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		ch.document_id, count(oh.document_id) num_orders
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'ORDER') oh
		right join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'CANCEL') ch on oh.order_id = ch.order_id
	group by ch.document_id) t
group by num_tot, num_orders
order by num_tot, num_orders;



