
select entity_id, increment_id, created_at
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at, order_id
from magento01.sales_flat_shipment
where created_at > CURDATE() - interval 365 day
limit 100;

select oh.entity_id entity_id_o, sh.entity_id entity_id_s, oh.increment_id increment_id_o, sh.increment_id increment_id_s, 
  oh.created_at created_at_o, sh.created_at created_at_s
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id
-- where oh.entity_id = 3633888
limit 100;

-- ----------------------------------------------------------

select oh.entity_id entity_id_o, count(sh.entity_id) num_shipments
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id
group by oh.entity_id
order by oh.entity_id
limit 100;

select num_shipments, count(*)
from
  (select oh.entity_id entity_id_o, count(sh.entity_id) num_shipments
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where created_at > CURDATE() - interval 365 day) oh
    left join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id
  group by oh.entity_id) t
group by num_shipments
order by num_shipments;

-- ----------------------------------------------------------

select sh.entity_id entity_id_s, count(oh.entity_id) num_orders
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order) oh
  right join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment
    where created_at > CURDATE() - interval 365 day) sh on oh.entity_id = sh.order_id
group by sh.entity_id
order by sh.entity_id
limit 100;

select num_orders, count(*)
from
  (select sh.entity_id entity_id_s, count(oh.entity_id) num_orders
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order) oh
    right join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_shipment
      where created_at > CURDATE() - interval 365 day) sh on oh.entity_id = sh.order_id
  group by sh.entity_id) t  
group by num_orders
order by num_orders;  