
select top 1000 document_id, order_id, order_no, document_type, document_date, 
	order_currency_code, local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
from DW_GetLenses.dbo.order_headers 
where order_id in (3637211, 3662782)

select top 1000 document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
	order_currency_code, local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
from DW_GetLenses.dbo.invoice_headers 
where order_id in (3637211, 3662782)

select top 1000 document_id, shipment_id, shipment_no, document_type, document_date, order_id, order_no
	order_currency_code, local_to_global_rate,
	local_subtotal_inc_vat, local_subtotal_exc_vat, local_total_inc_vat, local_total_exc_vat, 
	global_subtotal_inc_vat, global_subtotal_exc_vat, global_total_inc_vat, global_total_exc_vat
from DW_GetLenses.dbo.shipment_headers 
where order_id in (3637211, 3662782)

