select entity_id, increment_id, created_at
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at, order_id
from magento01.sales_flat_shipment
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at, order_id
from magento01.sales_flat_creditmemo
where created_at > CURDATE() - interval 365 day
limit 100;

select oh.entity_id entity_id_o, ch.entity_id entity_id_c, ch.invoice_id, sh.entity_id entity_id_s, 
  oh.increment_id increment_id_o, sh.increment_id increment_id_s, ch.increment_id increment_id_c, 
  oh.created_at created_at_o, sh.created_at created_at_s, ch.created_at created_at_c
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  inner join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id 
  left join
    (select entity_id, increment_id, created_at, order_id, invoice_id
    from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id
-- where oh.entity_id = 3633888
limit 100;

select oh.entity_id entity_id_o, ch.entity_id entity_id_c, ch.invoice_id, sh.entity_id entity_id_s, 
  oh.increment_id increment_id_o, sh.increment_id increment_id_s, ch.increment_id increment_id_c, 
  oh.created_at created_at_o, sh.created_at created_at_s, ch.created_at created_at_c
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  inner join
    (select entity_id, increment_id, created_at, order_id, invoice_id
    from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id  
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id 
-- where oh.entity_id = 3633888
limit 100;

-- ----------------------------------------------------------

select oh.entity_id entity_id_o, count(ch.entity_id) num_creditmemos
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  inner join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id 
  left join
    (select entity_id, increment_id, created_at, order_id, invoice_id
    from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id
group by oh.entity_id
order by oh.entity_id
limit 100;    

select num_creditmemos, count(*)
from
  (select oh.entity_id entity_id_o, count(ch.entity_id) num_creditmemos
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where created_at > CURDATE() - interval 365 day) oh
    inner join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id 
    left join
      (select entity_id, increment_id, created_at, order_id, invoice_id
      from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id
  group by oh.entity_id) t    
group by num_creditmemos
order by num_creditmemos;    

-- --------------------------------------------------------------------------------------------------

select oh.entity_id entity_id_o, count(sh.entity_id) num_shipments
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  inner join
    (select entity_id, increment_id, created_at, order_id, invoice_id
    from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id  
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id 
group by oh.entity_id
order by oh.entity_id
limit 100;     

select num_shipments, count(*)
from
  (select oh.entity_id entity_id_o, count(sh.entity_id) num_shipments
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where created_at > CURDATE() - interval 365 day) oh
    inner join
      (select entity_id, increment_id, created_at, order_id, invoice_id
      from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id  
    left join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id 
  group by oh.entity_id) t
group by num_shipments
order by num_shipments;      
