
select top 1000 oh.document_id, oh.order_id, oh.order_no, oh.document_type, oh.document_date, 
	ol.line_id, ol.product_id, ol.name, ol.sku, 
	count(ol.line_id) over (partition by oh.document_id) num_lines
from 
		DW_GetLenses.dbo.order_headers oh
	left join
		DW_GetLenses.dbo.order_lines ol on oh.document_id = ol.document_id and oh.document_type = ol.document_type
where oh.document_date > GETUTCDATE() - 365
	and oh.document_type = 'ORDER'
	and oh.order_id in (3633416, 3633888)
order by oh.document_id, ol.line_id

select top 1000 ih.document_id, ih.invoice_id, ih.invoice_no, ih.document_type, ih.document_date, ih.order_id, ih.order_no,
	il.line_id, il.product_id, il.name, il.sku, 
	count(il.line_id) over (partition by ih.document_id) num_lines
from 
	DW_GetLenses.dbo.invoice_headers ih
left join
	DW_GetLenses.dbo.invoice_lines il on ih.document_id = il.document_id and ih.document_type = il.document_type
where ih.document_date > GETUTCDATE() - 365
	and ih.document_type = 'INVOICE'
	and ih.order_id in (3633416, 3633888)
order by ih.document_id, il.line_id



	on oh.order_id = ih.order_id + ??? (ol.line_id <> il.line_id) / ol.sku = il.sku and RANK (in case Order with 2 lines for same product SKU)
		ih.document_id = il.document_id and ih.document_type = il.document_type