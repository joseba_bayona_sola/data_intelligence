
select entity_id, increment_id, created_at
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at, order_id
from magento01.sales_flat_creditmemo
where created_at > CURDATE() - interval 365 day
limit 100;

select oh.entity_id entity_id_o, ch.entity_id entity_id_c, ch.invoice_id, oh.increment_id increment_id_o, ch.increment_id increment_id_c, 
  oh.created_at created_at_o, ch.created_at created_at_c
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id
-- where oh.entity_id = 3633888
limit 100;

-- ----------------------------------------------------------

select oh.entity_id entity_id_o, count(ch.entity_id) num_creditmemos
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id
group by oh.entity_id
order by oh.entity_id
limit 100;

select num_creditmemos, count(*)
from
  (select oh.entity_id entity_id_o, count(ch.entity_id) num_creditmemos
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where created_at > CURDATE() - interval 365 day) oh
    left join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_creditmemo) ch on oh.entity_id = ch.order_id
  group by oh.entity_id) t
group by num_creditmemos
order by num_creditmemos;  

-- ----------------------------------------------------------

select ch.entity_id entity_id_c, count(oh.entity_id) num_orders
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order) oh
  right join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_creditmemo
    where created_at > CURDATE() - interval 365 day) ch on oh.entity_id = ch.order_id
group by ch.entity_id
order by ch.entity_id
limit 100;

select num_orders, count(*)
from
  (select ch.entity_id entity_id_c, count(oh.entity_id) num_orders
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order) oh
    right join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_creditmemo
      where created_at > CURDATE() - interval 365 day) ch on oh.entity_id = ch.order_id
  group by ch.entity_id) t  
group by num_orders
order by num_orders;  