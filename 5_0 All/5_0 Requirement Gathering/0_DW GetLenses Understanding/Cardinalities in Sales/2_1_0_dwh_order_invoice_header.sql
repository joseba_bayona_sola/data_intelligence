

select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'

select top 1000 document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
from DW_GetLenses.dbo.invoice_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'INVOICE'

select top 1000 oh.document_id, oh.order_id, oh.order_no, ih.invoice_id, ih.invoice_no, 
	oh.document_type document_type_o, oh.document_date document_date_o, 
	ih.document_type document_type_i, ih.document_date document_date_i, 
	count(ih.invoice_id) over (partition by oh.document_id) num_invoices
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	left join
		(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
		from DW_GetLenses.dbo.invoice_headers 
		where document_type = 'INVOICE') ih on oh.order_id = ih.order_id
order by oh.document_date, ih.document_date

-------------------------------------

select top 1000 count(*) over () num_tot,
	oh.document_id, count(ih.invoice_id) num_invoices
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	left join
		(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
		from DW_GetLenses.dbo.invoice_headers 
		where document_type = 'INVOICE') ih on oh.order_id = ih.order_id
group by oh.document_id
order by oh.document_id

select num_tot, num_invoices, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		oh.document_id, count(ih.invoice_id) num_invoices
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		left join
			(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'INVOICE') ih on oh.order_id = ih.order_id
	group by oh.document_id) t
group by num_tot, num_invoices
order by num_tot, num_invoices;

-------------------------------------

select *
from DW_GetLenses.dbo.order_headers
where order_id = 3633888

select *
from DW_GetLenses.dbo.invoice_headers
where order_id = 3633888

select *
from DW_GetLenses.dbo.invoice_lines
where document_id in (3076567, 3076568)

-------------------------------------

select top 1000 *
from 
	(select oh.document_id, oh.order_id, oh.order_no, ih.invoice_id, ih.invoice_no, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		ih.document_type document_type_i, ih.document_date document_date_i, 
		count(ih.invoice_id) over (partition by oh.document_id) num_invoices
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		left join
			(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'INVOICE') ih on oh.order_id = ih.order_id) t
where num_invoices = 2
order by document_date_o, document_date_i


-------------------------------------

select top 1000 count(*) over () num_tot,
	ih.document_id, count(oh.order_id) num_orders
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'ORDER') oh
	right join
		(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
		from DW_GetLenses.dbo.invoice_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'INVOICE') ih on oh.order_id = ih.order_id
group by ih.document_id
order by ih.document_id

select num_tot, num_orders, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		ih.document_id, count(oh.order_id) num_orders
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'ORDER') oh
		right join
			(select document_id, invoice_id, invoice_no, document_type, document_date, order_id, order_no
			from DW_GetLenses.dbo.invoice_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'INVOICE') ih on oh.order_id = ih.order_id
	group by ih.document_id) t
group by num_tot, num_orders
order by num_tot, num_orders;