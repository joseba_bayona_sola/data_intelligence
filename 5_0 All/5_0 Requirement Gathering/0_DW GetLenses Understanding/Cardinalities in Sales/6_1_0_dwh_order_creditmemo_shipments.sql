
select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'ORDER'

select top 1000 document_id, order_id, order_no, document_type, document_date
from DW_GetLenses.dbo.order_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'CREDITMEMO'


select top 1000 document_id, shipment_id, shipment_no, document_type, document_date
from DW_GetLenses.dbo.shipment_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'SHIPMENT'

select top 1000 document_id, shipment_id, shipment_no, document_type, document_date
from DW_GetLenses.dbo.shipment_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'CREDITMEMO'

select top 1000 oh.document_id document_id_o, oh.order_id, oh.order_no, sh.shipment_id, sh.shipment_no, ch.document_id document_id_c, 
	oh.document_type document_type_o, oh.document_date document_date_o, 
	ch.document_type document_type_c, ch.document_date document_date_c, 
	sh.document_type document_type_s, sh.document_date document_date_s, 
	count(ch.document_id) over (partition by oh.document_id) num_creditmemos, 
	count(sh.document_id) over (partition by oh.document_id) num_shipments 
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	inner join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CREDITMEMO') ch on oh.order_id = ch.order_id
	left join
		(select sh.document_id, sh.shipment_id, sh.shipment_no, sh.document_type, sh.document_date, sh.order_id
		from
				(select document_id, shipment_id, shipment_no, document_type, document_date, order_id
				from DW_GetLenses.dbo.shipment_headers 
				where document_date > GETUTCDATE() - 365
					and document_type = 'SHIPMENT') sh
			left join
				(select document_id, shipment_id, shipment_no, document_type, document_date
				from DW_GetLenses.dbo.shipment_headers 
				where document_date > GETUTCDATE() - 365
					and document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
		where ch.shipment_id is null) sh on oh.order_id = sh.order_id
order by oh.document_date, ch.document_date, sh.document_date

-------------------------------------

select top 1000 count(*) over () num_tot,
	oh.document_id, count(ch.document_id) num_creditmemos, count(sh.shipment_id) num_shipments
from
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'ORDER') oh
	inner join
		(select document_id, order_id, order_no, document_type, document_date
		from DW_GetLenses.dbo.order_headers 
		where document_type = 'CREDITMEMO') ch on oh.order_id = ch.order_id
	left join
		(select sh.document_id, sh.shipment_id, sh.shipment_no, sh.document_type, sh.document_date, sh.order_id
		from
				(select document_id, shipment_id, shipment_no, document_type, document_date, order_id
				from DW_GetLenses.dbo.shipment_headers 
				where document_date > GETUTCDATE() - 365
					and document_type = 'SHIPMENT') sh
			left join
				(select document_id, shipment_id, shipment_no, document_type, document_date
				from DW_GetLenses.dbo.shipment_headers 
				where document_date > GETUTCDATE() - 365
					and document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
		where ch.shipment_id is null) sh on oh.order_id = sh.order_id
group by oh.document_id
order by oh.document_id;

select num_tot, num_creditmemos, num_shipments, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		oh.document_id, count(ch.document_id) num_creditmemos, count(sh.shipment_id) num_shipments
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CREDITMEMO') ch on oh.order_id = ch.order_id
		left join
			(select sh.document_id, sh.shipment_id, sh.shipment_no, sh.document_type, sh.document_date, sh.order_id
			from
					(select document_id, shipment_id, shipment_no, document_type, document_date, order_id
					from DW_GetLenses.dbo.shipment_headers 
					where document_date > GETUTCDATE() - 365
						and document_type = 'SHIPMENT') sh
				left join
					(select document_id, shipment_id, shipment_no, document_type, document_date
					from DW_GetLenses.dbo.shipment_headers 
					where document_date > GETUTCDATE() - 365
						and document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
			where ch.shipment_id is null) sh on oh.order_id = sh.order_id
	group by oh.document_id) t
group by num_tot, num_creditmemos, num_shipments
order by num_tot, num_creditmemos, num_shipments;

-------------------------------------

select *
from 
	(select oh.document_id document_id_o, oh.order_id, oh.order_no, sh.shipment_id, sh.shipment_no, ch.document_id document_id_c, 
		oh.document_type document_type_o, oh.document_date document_date_o, 
		ch.document_type document_type_c, ch.document_date document_date_c, 
		sh.document_type document_type_s, sh.document_date document_date_s, 
		count(ch.document_id) over (partition by oh.document_id) num_creditmemos, 
		count(sh.document_id) over (partition by oh.document_id) num_shipments 
	from
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'ORDER') oh
		inner join
			(select document_id, order_id, order_no, document_type, document_date
			from DW_GetLenses.dbo.order_headers 
			where document_type = 'CREDITMEMO') ch on oh.order_id = ch.order_id
		left join
			(select sh.document_id, sh.shipment_id, sh.shipment_no, sh.document_type, sh.document_date, sh.order_id
			from
					(select document_id, shipment_id, shipment_no, document_type, document_date, order_id
					from DW_GetLenses.dbo.shipment_headers 
					where document_date > GETUTCDATE() - 365
						and document_type = 'SHIPMENT') sh
				left join
					(select document_id, shipment_id, shipment_no, document_type, document_date
					from DW_GetLenses.dbo.shipment_headers 
					where document_date > GETUTCDATE() - 365
						and document_type = 'CREDITMEMO') ch on sh.shipment_id = ch.shipment_id
			where ch.shipment_id is null) sh on oh.order_id = sh.order_id) t
where num_shipments > 0 
order by document_date_o, document_date_c, document_date_s
