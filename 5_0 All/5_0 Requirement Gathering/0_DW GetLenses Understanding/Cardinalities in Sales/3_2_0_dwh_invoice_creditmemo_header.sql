
select top 1000 document_id, invoice_id, invoice_no, document_type, document_date
from DW_GetLenses.dbo.invoice_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'INVOICE'

select top 1000 document_id, invoice_id, invoice_no, document_type, document_date
from DW_GetLenses.dbo.invoice_headers 
where document_date > GETUTCDATE() - 365
	and document_type = 'CREDITMEMO'

select top 1000 ih.document_id document_id_i, ih.invoice_id, ih.invoice_no, ch.document_id document_id_c, 
	ih.document_type document_type_i, ih.document_date document_date_i, 
	ch.document_type document_type_c, ch.document_date document_date_c, 
	count(ch.document_id) over (partition by ih.document_id) num_creditmemos
from
		(select document_id, invoice_id, invoice_no, document_type, document_date
		from DW_GetLenses.dbo.invoice_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'INVOICE') ih
	left join
		(select document_id, invoice_id, invoice_no, document_type, document_date
		from DW_GetLenses.dbo.invoice_headers 
		where document_type = 'CREDITMEMO') ch on ih.invoice_id = ch.invoice_id
order by ih.document_date, ch.document_date

-------------------------------------

select top 1000 count(*) over () num_tot,
	ih.document_id, count(ch.document_id) num_creditmemos
from
		(select document_id, invoice_id, invoice_no, document_type, document_date
		from DW_GetLenses.dbo.invoice_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'INVOICE') ih
	left join
		(select document_id, invoice_id, invoice_no, document_type, document_date
		from DW_GetLenses.dbo.invoice_headers 
		where document_type = 'CREDITMEMO') ch on ih.invoice_id = ch.invoice_id
group by ih.document_id
order by ih.document_id

select num_tot, num_creditmemos, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		ih.document_id, count(ch.document_id) num_creditmemos
	from
			(select document_id, invoice_id, invoice_no, document_type, document_date
			from DW_GetLenses.dbo.invoice_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'INVOICE') ih
		left join
			(select document_id, invoice_id, invoice_no, document_type, document_date
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'CREDITMEMO') ch on ih.invoice_id = ch.invoice_id
	group by ih.document_id) t
group by num_tot, num_creditmemos
order by num_tot, num_creditmemos;

-------------------------------------

select *
from
	(select ih.document_id document_id_i, ih.invoice_id, ih.invoice_no, ch.document_id document_id_c, 
		ih.document_type document_type_i, ih.document_date document_date_i, 
		ch.document_type document_type_c, ch.document_date document_date_c, 
		count(ch.document_id) over (partition by ih.document_id) num_creditmemos
	from
			(select document_id, invoice_id, invoice_no, document_type, document_date
			from DW_GetLenses.dbo.invoice_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'INVOICE') ih
		left join
			(select document_id, invoice_id, invoice_no, document_type, document_date
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'CREDITMEMO') ch on ih.invoice_id = ch.invoice_id) t
--where num_creditmemos = 1
where num_creditmemos > 1
order by t.document_date_i, t.document_date_c

-------------------------------------

select top 1000 count(*) over () num_tot,
	ch.document_id, count(ih.document_id) num_invoices
from
		(select document_id, invoice_id, invoice_no, document_type, document_date
		from DW_GetLenses.dbo.invoice_headers 
		where document_type = 'INVOICE') ih
	right join
		(select document_id, invoice_id, invoice_no, document_type, document_date
		from DW_GetLenses.dbo.invoice_headers 
		where document_date > GETUTCDATE() - 365
			and document_type = 'CREDITMEMO') ch on ih.invoice_id = ch.invoice_id
group by ch.document_id
order by ch.document_id

select num_tot, num_invoices, count(*) num, 
	count(*) * 100 / convert(decimal(10,2), num_tot) perc
from
	(select count(*) over () num_tot,
		ch.document_id, count(ih.document_id) num_invoices
	from
			(select document_id, invoice_id, invoice_no, document_type, document_date
			from DW_GetLenses.dbo.invoice_headers 
			where document_type = 'INVOICE') ih
		right join
			(select document_id, invoice_id, invoice_no, document_type, document_date
			from DW_GetLenses.dbo.invoice_headers 
			where document_date > GETUTCDATE() - 365
				and document_type = 'CREDITMEMO') ch on ih.invoice_id = ch.invoice_id
	group by ch.document_id) t
group by num_tot, num_invoices
order by num_tot, num_invoices;