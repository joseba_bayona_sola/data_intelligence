
select entity_id, increment_id, created_at
from magento01.sales_flat_order
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at
from magento01.sales_flat_order
where status = 'canceled'
limit 100;

select entity_id, increment_id, created_at, order_id
from magento01.sales_flat_invoice
where created_at > CURDATE() - interval 365 day
limit 100;

select entity_id, increment_id, created_at, order_id
from magento01.sales_flat_shipment
where created_at > CURDATE() - interval 365 day
limit 100;

select oh.entity_id entity_id_o, ch.entity_id entity_id_c, ih.entity_id entity_id_i, sh.entity_id entity_id_s,
  oh.increment_id increment_id_o, ch.increment_id increment_id_c, ih.increment_id increment_id_i, sh.increment_id increment_id_s, 
  oh.created_at created_at_o, ch.created_at created_at_c, ih.created_at created_at_i, sh.created_at created_at_s
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  inner join
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where status = 'canceled') ch on oh.entity_id = ch.entity_id
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_invoice) ih on oh.entity_id = ih.order_id    
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id    
limit 100;

-- ----------------------------------------------------------

select oh.entity_id entity_id_o, 
  count(ih.entity_id) num_invoices, count(sh.entity_id) num_shipments
from
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where created_at > CURDATE() - interval 365 day) oh
  inner join
    (select entity_id, increment_id, created_at
    from magento01.sales_flat_order
    where status = 'canceled') ch on oh.entity_id = ch.entity_id
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_invoice) ih on oh.entity_id = ih.order_id    
  left join
    (select entity_id, increment_id, created_at, order_id
    from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id    
group by oh.entity_id
order by oh.entity_id
limit 100;    

select num_invoices, num_shipments, count(*)
from
  (select oh.entity_id entity_id_o, 
    count(ih.entity_id) num_invoices, count(sh.entity_id) num_shipments
  from
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where created_at > CURDATE() - interval 365 day) oh
    inner join
      (select entity_id, increment_id, created_at
      from magento01.sales_flat_order
      where status = 'canceled') ch on oh.entity_id = ch.entity_id
    left join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_invoice) ih on oh.entity_id = ih.order_id    
    left join
      (select entity_id, increment_id, created_at, order_id
      from magento01.sales_flat_shipment) sh on oh.entity_id = sh.order_id    
  group by oh.entity_id) t
group by num_invoices, num_shipments
order by num_invoices, num_shipments