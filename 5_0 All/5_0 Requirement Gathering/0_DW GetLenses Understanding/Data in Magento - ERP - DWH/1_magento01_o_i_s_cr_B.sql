
-- Order Header
select entity_id, increment_id, created_at, state, status,
  -- base_grand_total, base_subtotal, base_shipping_amount, base_discount_amount 
  -- base_subtotal, base_subtotal_canceled, base_subtotal_invoiced, base_subtotal_refunded, base_subtotal_incl_tax
  -- base_shipping_amount, base_shipping_canceled, base_shipping_invoiced, base_shipping_refunded, base_shipping_tax_amount, base_shipping_tax_refunded, base_shipping_discount_amount, base_shipping_incl_tax
  -- base_discount_amount, base_discount_canceled, base_discount_invoiced, base_discount_refunded
  -- base_grand_total
  -- base_currency_code, global_currency_code, order_currency_code, store_currency_code, base_to_global_rate, base_to_order_rate
  -- base_tax_amount, base_tax_canceled, base_tax_invoiced, base_tax_refunded
  -- base_total_canceled, base_total_invoiced, base_total_invoiced_cost, base_total_offline_refunded, base_total_online_refunded, base_total_paid, base_total_qty_ordered, base_total_refunded, base_total_due
  -- base_adjustment_negative, base_adjustment_positive
  -- base_custbalance_amount, base_customer_balance_amount, base_customer_balance_invoiced, base_customer_balance_refunded, bs_customer_bal_total_refunded
  -- base_gift_cards_amount, base_gift_cards_invoiced, base_gift_cards_refunded
  base_reward_currency_amount, base_rwrd_crrncy_amt_invoiced, base_rwrd_crrncy_amnt_refnded
  -- base_hidden_tax_amount, base_shipping_hidden_tax_amnt, base_hidden_tax_invoiced, base_hidden_tax_refunded
  -- gw_base_price, gw_items_base_price, gw_card_base_price, gw_base_tax_amount, gw_items_base_tax_amount, gw_card_base_tax_amount, 
  -- gw_base_price_invoiced, gw_items_base_price_invoiced, gw_card_base_price_invoiced, gw_base_tax_amount_invoiced, gw_items_base_tax_invoiced, gw_card_base_tax_invoiced, 
  -- gw_base_price_refunded, gw_items_base_price_refunded, gw_card_base_price_refunded, gw_base_tax_amount_refunded, gw_items_base_tax_refunded, gw_card_base_tax_refunded
from magento01.sales_flat_order
where increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
-- where increment_id in ('1100011236', '300110492')
order by increment_id
limit 100;


  select base_to_order_rate, count(*)
  from magento01.sales_flat_order
  group by base_to_order_rate
  order by base_to_order_rate
 
  select entity_id, increment_id, created_at, 
    base_subtotal, subtotal
  from magento01.sales_flat_order
  where base_subtotal <> subtotal
  order by increment_id desc
  limit 100;
  
  select base_adjustment_negative, base_adjustment_positive, count(*)
  from magento01.sales_flat_order
  group by base_adjustment_negative, base_adjustment_positive
  order by base_adjustment_negative, base_adjustment_positive
  

-- Order Lines
select o.entity_id, o.increment_id, o.created_at, o.state, o.status, oi.item_id, oi.product_id, oi.name, 
  -- oi.qty_ordered, oi.weight, oi.base_price, oi.base_row_total, oi.base_cost 
  -- oi.qty_ordered, oi.qty_canceled, oi.qty_invoiced, oi.qty_refunded, oi.qty_shipped, oi.qty_backordered, oi.qty_returned
  -- oi.weight, oi.row_weight
  -- oi.base_price, oi.base_original_price, oi.base_price_incl_tax
  -- oi.base_tax_amount, oi.base_tax_invoiced, oi.base_tax_refunded, oi.base_tax_before_discount
  -- oi.base_discount_amount, oi.base_discount_invoiced, oi.base_discount_refunded
  -- oi.base_amount_refunded
  -- oi.tax_percent, oi.discount_percent
  -- oi.base_cost
  oi.base_row_total, oi.base_row_invoiced, oi.base_row_total_incl_tax
  -- oi.base_tax_before_discount
  -- oi.base_weee_tax_applied_amount, oi.base_weee_tax_applied_row_amnt, oi.base_weee_tax_disposition, oi.base_weee_tax_row_disposition
  -- oi.base_hidden_tax_amount, oi.base_hidden_tax_invoiced, oi.base_hidden_tax_refunded
  -- oi.gw_base_price, oi.gw_base_tax_amount, oi.gw_base_price_invoiced, oi.gw_base_tax_amount_invoiced, oi.gw_base_price_refunded, oi.gw_base_tax_amount_refunded
from 
    magento01.sales_flat_order_item oi
  inner join 
    magento01.sales_flat_order o on oi.order_id = o.entity_id
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, oi.item_id
limit 100;


-- -----------------------------------------------

-- Invoice Header
select o.entity_id, o.increment_id, o.created_at, i.entity_id, i.increment_id, i.state,
  --   i.base_grand_total, i.base_subtotal, i.base_shipping_amount, i.base_discount_amount 
  -- i.base_subtotal, i.base_subtotal_incl_tax
  -- i.base_shipping_amount, i.base_shipping_tax_amount, i.base_shipping_incl_tax  
  -- i.base_discount_amount
  -- i.base_grand_total, 
  -- i.base_currency_code, i.global_currency_code, i.order_currency_code, i.store_currency_code, i.base_to_global_rate, i.base_to_order_rate
  -- i.base_tax_amount
  i.base_total_refunded 
  -- i.base_customer_balance_amount
  -- i.base_gift_cards_amount
  -- i.base_reward_currency_amount
  -- i.base_hidden_tax_amount, i.base_shipping_hidden_tax_amnt
  -- i.gw_base_price, i.gw_items_base_price, i.gw_card_base_price, i.gw_base_tax_amount, i.gw_items_base_tax_amount, i.gw_card_base_tax_amount  
from 
    magento01.sales_flat_invoice i
  inner join 
    magento01.sales_flat_order o on i.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id
limit 100;

-- Invoice Lines
select o.entity_id, o.increment_id, o.created_at, i.entity_id, i.increment_id, ii.entity_id, ii.product_id, ii.name, 
  ii.qty, ii.base_price, ii.base_row_total, ii.base_cost 
  -- ii.qty
  -- ii.base_price, ii.base_price_incl_tax
  -- ii.base_tax_amount
  -- ii.base_discount_amount
  -- ii.base_row_total, ii.base_row_total_incl_tax
  -- ii.base_weee_tax_applied_amount, ii.base_weee_tax_applied_row_amnt, ii.base_weee_tax_disposition, ii.base_weee_tax_row_disposition
  -- ii.base_hidden_tax_amount
from 
    magento01.sales_flat_invoice_item ii
  inner join   
    magento01.sales_flat_invoice i on ii.parent_id = i.entity_id
  inner join 
    magento01.sales_flat_order o on i.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, ii.parent_id, ii.entity_id
limit 100;

-- -----------------------------------------------

-- Shipment Header
select o.entity_id, o.increment_id, o.created_at, s.entity_id, s.increment_id, 
  s.shipment_status, s.shipping_description, 
  s.total_weight, s.total_qty
from 
    magento01.sales_flat_shipment s
  inner join 
    magento01.sales_flat_order o on s.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id
limit 100;

-- Shipment Lines
select o.entity_id, o.increment_id, o.created_at, s.entity_id, s.increment_id, si.entity_id, si.product_id, si.name, 
  si.qty, si.price, si.weight, si.row_total
from 
    magento01.sales_flat_shipment_item si
  inner join   
    magento01.sales_flat_shipment s on si.parent_id = s.entity_id
  inner join 
    magento01.sales_flat_order o on s.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, si.parent_id, si.entity_id
limit 100;

-- -----------------------------------------------

-- CreditMemo Header
select o.entity_id, o.increment_id, o.created_at, c.entity_id, c.increment_id, c.state,
  c.base_grand_total, c.base_subtotal, c.base_shipping_amount, c.base_discount_amount 
  -- c.base_subtotal, c.base_subtotal_incl_tax
  -- c.base_shipping_amount, c.base_shipping_tax_amount, c.base_shipping_incl_tax  
  -- d.base_discount_amount
  -- c.base_grand_total, 
  -- c.base_currency_code, c.global_currency_code, c.order_currency_code, c.store_currency_code, c.base_to_global_rate, c.base_to_order_rate
  -- c.base_tax_amount
  -- c.base_adjustment, c.base_adjustment_positive, c.base_adjustment_negative  
  -- i.base_customer_balance_amount, i.bs_customer_bal_total_refunded
  -- i.base_gift_cards_amount
  -- i.base_reward_currency_amount
  -- i.base_hidden_tax_amount, i.base_shipping_hidden_tax_amnt
  -- i.gw_base_price, i.gw_items_base_price, i.gw_card_base_price, i.gw_base_tax_amount, i.gw_items_base_tax_amount, i.gw_card_base_tax_amount  
from 
    magento01.sales_flat_creditmemo c
  inner join 
    magento01.sales_flat_order o on c.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id
limit 100;

-- CreditMemo Lines
select o.entity_id, o.increment_id, o.created_at, c.entity_id, c.increment_id, ci.entity_id, ci.product_id, ci.name, 
  ci.qty, ci.base_price, ci.base_row_total, ci.base_cost 
  -- ci.qty
  -- ci.base_price, ci.base_price_incl_tax
  -- ci.base_tax_amount
  -- ci.base_discount_amount
  -- ci.base_row_total, ci.base_row_total_incl_tax
  -- ci.base_weee_tax_applied_amount, ci.base_weee_tax_applied_row_amnt, ci.base_weee_tax_disposition, ci.base_weee_tax_row_disposition
  -- ci.base_hidden_tax_amount
from 
    magento01.sales_flat_creditmemo_item ci
  inner join  
    magento01.sales_flat_creditmemo c on ci.parent_id = c.entity_id
  inner join 
    magento01.sales_flat_order o on c.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, ci.parent_id, ci.entity_id
limit 100;

-- -----------------------------------------------

-- Order Payment

select o.entity_id, o.increment_id, o.created_at, op.entity_id, 
  op.method, 
  -- op.cc_type, op.cc_status, op.cc_status_description,
  -- op.echeck_type, op.echeck_account_type, op.echeck_routing_number, op.echeck_bank_name, op.echeck_routing_number
  -- op.ideal_transaction_checked, op.ideal_issuer_title, op.ideal_issuer_id
  op.cybersource_token, op.cybersource_cc_stored,
  -- op.internetkassa_issuer_pm, op.internetkassa_issuer_brand, op.internetkassa_issuer_ideal,
  -- op.globalcollect_payment_product_id, op.globalcollect_payment_status_code, op.globalcollect_token_id, op.globalcollect_issuer_id
  op.base_amount_ordered, op.base_amount_canceled, op.base_amount_refunded, op.base_amount_refunded_online, op.base_amount_paid, op.base_amount_paid_online, op.base_amount_authorized
  -- op.base_shipping_amount, op.base_shipping_captured, op.base_shipping_refunded

from 
    magento01.sales_flat_order_payment op
  inner join 
    magento01.sales_flat_order o on op.parent_id = o.entity_id
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, op.entity_id
limit 100;

select method, count(*)
from magento01.sales_flat_order_payment 
group by method
order by method

-- -----------------------------------------------

-- Order Address

select o.entity_id, o.increment_id, o.created_at, oa.* 

from 
    magento01.sales_flat_order_address oa
  inner join 
    magento01.sales_flat_order o on oa.entity_id = o.billing_address_id
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, oa.entity_id
limit 100;