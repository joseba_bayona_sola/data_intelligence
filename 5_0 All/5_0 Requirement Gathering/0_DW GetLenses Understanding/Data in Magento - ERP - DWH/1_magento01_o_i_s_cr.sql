
-- Orders
  
select entity_id, increment_id, created_at, 
  entity_id, created_at, updated_at, 
  state, status, automatic_reorder, postoptics_source, 
  store_id, customer_id, customer_email, 
  shipping_description, 
  reminder_type, reminder_date, reminder_period,
  reorder_on_flag, 
  0,
  base_subtotal, base_shipping_amount, base_discount_amount, base_customer_balance_amount, 0, 
    base_subtotal + base_shipping_amount - ABS(base_discount_amount) - IFNULL(base_customer_balance_amount, 0)
from magento01.sales_flat_order
where increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by increment_id
limit 100;


select o.increment_id, oi.*
from 
    magento01.sales_flat_order_item oi
  inner join 
    magento01.sales_flat_order o on oi.order_id = o.entity_id
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, oi.item_id
limit 100;

-- Invoices
select o.increment_id, i.*
from 
    magento01.sales_flat_invoice i
  inner join 
    magento01.sales_flat_order o on i.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id
limit 100;

select o.increment_id, ii.*
from 
    magento01.sales_flat_invoice_item ii
  inner join   
    magento01.sales_flat_invoice i on ii.parent_id = i.entity_id
  inner join 
    magento01.sales_flat_order o on i.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, ii.parent_id, ii.entity_id
limit 100;

-- Shipments
select o.increment_id, s.*
from 
    magento01.sales_flat_shipment s
  inner join 
    magento01.sales_flat_order o on s.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id
limit 100;

select o.increment_id, si.*
from 
    magento01.sales_flat_shipment_item si
  inner join   
    magento01.sales_flat_shipment s on si.parent_id = s.entity_id
  inner join 
    magento01.sales_flat_order o on s.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, si.parent_id, si.entity_id
limit 100;

-- Credit Memo
select o.increment_id, c.*
from 
    magento01.sales_flat_creditmemo c
  inner join 
    magento01.sales_flat_order o on c.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id
limit 100;

select o.increment_id, ci.*
from 
    magento01.sales_flat_creditmemo_item ci
  inner join  
    magento01.sales_flat_creditmemo c on ci.parent_id = c.entity_id
  inner join 
    magento01.sales_flat_order o on c.order_id = o.entity_id  
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, ci.parent_id, ci.entity_id
limit 100;




select o.increment_id, op.*
from 
    magento01.sales_flat_order_payment op
  inner join 
    magento01.sales_flat_order o on op.parent_id = o.entity_id
where o.increment_id in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by o.increment_id, op.entity_id
limit 100;

  select method, count(*) num
  from magento01.sales_flat_order_payment 
  group by method
  order by num desc