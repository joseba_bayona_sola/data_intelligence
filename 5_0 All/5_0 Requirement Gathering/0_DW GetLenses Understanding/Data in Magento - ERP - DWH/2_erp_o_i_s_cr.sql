﻿

	-- Customer - Store (5001326091 - CANCEL, 8001713505 - CANCEL, 8002395321 - CREDITMEMO, 8002416303 - CANCEL)
	select o.id, 
		st.storeid, st.storename, cu.customerid, cu.email, cu.firstName, cu.lastName,
		o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
		o._type, o.orderStatus, o.shipmentStatus, o.statusUpdated, 
		o.couponCode,
		o.shippingMethod, o.shippingDescription, o.paymentMethod, 
		o.automatic_reorder,
		o.allocationStatus, 
		o.labelRenderer,
		o.totalNetPrice, o.totalPacksOrdered,
		o.orderCurrencyCode, o.globalCurrencyCode,
		o.createdDate, o.allocatedDate, o.promisedShippingDate, o.promisedDeliveryDate
	from 
			orderprocessing$order o
		inner join
			(select cu.customerid, cu.email, cu.firstName, cu.lastName, ocu.orderprocessing$orderID
			from
					(select orderprocessing$orderID, customer$retailCustomerID
					from orderprocessing$order_customer) ocu
				inner join
					(select ce.id, ce.customerid, ce.subMetaObjectName,
						rc.email, rc.phoneNumber, rc.prefix, rc.suffix, rc.firstName, rc.lastName, rc.middleName, rc.taxvat
					from 
							(select id, customerid, subMetaObjectName
							from customer$customerentity) ce
						inner join
							(select id, 
								email, phoneNumber, 
								prefix, suffix, firstName, lastName, middleName, 
								taxvat
							from customer$retailcustomer) rc on ce.id = rc.id) cu on ocu.customer$retailCustomerID = cu.id) cu on o.id = cu.orderprocessing$orderID
		inner join
			(select st.id, st.storeid, st.storename, ost.orderprocessing$orderID
			from	
					(select orderprocessing$orderID, companyorganisation$magWebStoreID
					from orderprocessing$order_magwebstore) ost
				inner join
					(select id, storeid, storename
					from companyorganisation$magwebstore) st on ost.companyorganisation$magWebStoreID = st.id) st on o.id = st.orderprocessing$orderID
	where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
	order by incrementID
	limit 1000;

-- Base Values
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus,
	bv.*
from 
		orderprocessing$order o
	inner join
		(select bv.*, bvo.orderProcessing$orderID
		from 
				(select orderprocessing$baseValuesID, orderProcessing$orderID
				from orderprocessing$basevalues_order) bvo
			inner join	
				(select id, 
					subtotal, subtotalCanceled, subtotalInvoiced, subtotalRefunded, 
					discountAmount, discountCanceled, discountInvoiced, discountRefunded, 
					shippingAmount, shippingCanceled, shippingInvoiced, shippingRefunded, 
					totalCanceled, totalInvoiced, totalRefunded, 
					adjustmentNegative, adjustmentPositive, 
					subtotalInclTax,
					custBalanceAmount, customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
					grandTotal, 
					toOrderRate, toGlobalRate, currencyCode
				from orderprocessing$basevalues) bv on bvo.orderprocessing$baseValuesID = bv.id) bv on o.id = bv.orderProcessing$orderID
		
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by incrementID 
limit 1000;

-- Order Line Magento
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, 
	olm.*
from 
		orderprocessing$order o
	inner join
		(select olm.*, olmo.orderprocessing$orderID
		from
			(select orderprocessing$orderLineMagentoID, orderprocessing$orderID
			from orderprocessing$orderlinemagento_order) olmo
		inner join	
			(select olm.id, olm.lineAdded, olm.deduplicate, olm.fulfilled, 
				ola.magentoItemID, ola.orderLineType, ola.subMetaObjectName,
				ola.sku, ola.eye, ola.lensGroupID, 
				ola.basePrice, ola.baseRowPrice, 
				ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityShipped, ola.quantityAllocated, ola.quantityIssued,  
				ola.netPrice, ola.netRowPrice,
				ola.allocated
			from
					(select id, lineAdded, deduplicate, fulfilled
					from orderprocessing$orderlinemagento) olm
				inner join
					(select id, 
						magentoItemID, orderLineType, subMetaObjectName,
						sku, eye, lensGroupID, 
						basePrice, baseRowPrice, 
						quantityOrdered, quantityInvoiced, quantityRefunded, quantityShipped, quantityAllocated, quantityIssued,  
						netPrice, netRowPrice,
						allocated, 
						createdDate, changedDate, 
						system$owner, system$changedBy
					from orderprocessing$orderlineabstract) ola on olm.id = ola.id) olm on olmo.orderprocessing$orderLineMagentoID = olm.id) olm 
						on o.id = olm.orderProcessing$orderID
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by incrementID, olm.sku
limit 1000;
	
-- Order Line
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, 
	ol.*
from 
		orderprocessing$order o
	inner join
		(select ol.*, olo.orderprocessing$orderID
		from 
			(select orderprocessing$orderLineID, orderprocessing$orderID
			from orderprocessing$orderline_order) olo
		inner join
			(select ol.id, ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped, 
				ola.magentoItemID, ola.orderLineType, ola.subMetaObjectName,
				ola.sku, ola.eye, ola.lensGroupID, 
				ola.basePrice, ola.baseRowPrice, 
				ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityShipped, ola.quantityAllocated, ola.quantityIssued,  
				ola.netPrice, ola.netRowPrice,
				ola.allocated
			from
					(select id, status, packsOrdered, packsAllocated, packsShipped
					from orderprocessing$orderline) ol
				inner join
					(select id, 
						magentoItemID, orderLineType, subMetaObjectName,
						sku, eye, lensGroupID, 
						basePrice, baseRowPrice, 
						quantityOrdered, quantityInvoiced, quantityRefunded, quantityShipped, quantityAllocated, quantityIssued,  
						netPrice, netRowPrice,
						allocated, 
						createdDate, changedDate, 
						system$owner, system$changedBy
					from orderprocessing$orderlineabstract) ola on ol.id = ola.id) ol on olo.orderprocessing$orderLineID = ol.id) ol 
						on o.id = ol.orderProcessing$orderID		
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by incrementID, ol.sku
limit 1000;

-- Shipping Group
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, 
	sg.*
from 
		orderprocessing$order o
	inner join
		(select sg.id, sg.dispensingFee, sg.allocationStatus, sg.shipmentStatus, 
			sg.wholesale, sg.letterBoxAble, sg.onHold, sg.adHoc, 
			sgo.orderprocessing$orderID
		from
				(select orderprocessing$shippingGroupID, orderprocessing$orderID
				from orderprocessing$shippinggroup_order) sgo
			inner join	
				(select id, 
					dispensingFee, 
					allocationStatus, shipmentStatus, 
					wholesale, 
					letterBoxAble, onHold, adHoc
				from orderprocessing$shippinggroup) sg on sgo.orderprocessing$shippingGroupID = sg.id) sg on o.id = sg.orderProcessing$orderID
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by incrementID 
limit 1000;


-- Order Line Abstract - Product - Required Stock Item
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, 
	olo.orderprocessing$orderLineID,
	psi.*
from 
		orderprocessing$order o
	inner join
		(select orderprocessing$orderLineID, orderprocessing$orderID
		from orderprocessing$orderline_order) olo on o.id = olo.orderprocessing$orderID	
	inner join
		(select orderprocessing$orderLineAbstractID, product$productID
		from orderprocessing$orderline_product) olp on olo.orderprocessing$orderLineID = olp.orderprocessing$orderLineAbstractID
	left join		
		(select orderprocessing$orderLineAbstractID, product$stockItemID
		from orderprocessing$orderline_requiredstockitem) olsi on olo.orderprocessing$orderLineID = olsi.orderprocessing$orderLineAbstractID
	inner join
		(select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.id productID, p.productType, p.SKU, p.oldSKU, p.description, 
			-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
			-- si.id, 
			si.id stockItemID, si.SKU SKU_SI, si.oldSKU, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id) psi 
					on olp.product$productID = psi.productID -- and olsi.product$stockItemID = psi.stockItemID
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')					
order by incrementID, psi.sku	

---------------------------------------------------------------------------------------------------

-- Order Line - Stock Allocation Process
select sa.id, 
	w.id, w.code, w.shortName,
	o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, 
	sa.transactionID, sa.timestamp,
	sa.allocationType, sa.recordType, 
	sa.allocatedUnits, sa.allocatedStockQuantity, sa.issuedQuantity, sa.packSize, 
	sa.stockAllocated, sa.cancelled, 
	sa.issuedDateTime
from 
		stockallocation$orderlinestockallocationtransaction sa
	inner join
		(select w.id, w.code, w.shortName, saw.stockallocation$orderLineStockAllocationTransactionID
		from 
				(select stockallocation$orderLineStockAllocationTransactionID, inventory$warehouseID
				from stockallocation$at_warehouse) saw		
			inner join
				(select id, code, shortName
				from inventory$warehouse) w on saw.inventory$warehouseID = w.id) w on sa.id = w.stockallocation$orderLineStockAllocationTransactionID	
	inner join
		(select o.*, sao.stockallocation$orderLineStockAllocationTransactionID
		from
			(select stockallocation$orderLineStockAllocationTransactionID, orderprocessing$orderID
			from stockallocation$at_order) sao
		inner join
			(select id, 
				magentoOrderID, incrementID, orderLinesMagento, orderLinesDeduped,
				_type, orderStatus, shipmentStatus, 
				allocationStatus, 
				createdDate, allocatedDate, promisedShippingDate, promisedDeliveryDate
			from orderprocessing$order
			where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')) o on sao.orderprocessing$orderID = o.id) o
				on sa.id = o.stockallocation$orderLineStockAllocationTransactionID
order by o.incrementID
limit 1000;

	-- Order Line (With Info) - Stock Allocation Process
	select sa.id, 
		w.id, w.code, w.shortName,
		o.id, 
		o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
		o._type, o.orderStatus, o.shipmentStatus,
		ol.*,
		sa.transactionID, sa.timestamp,
		sa.allocationType, sa.recordType, 
		sa.allocatedUnits, sa.allocatedStockQuantity, sa.issuedQuantity, sa.packSize, 
		sa.stockAllocated, sa.cancelled, 
		sa.issuedDateTime
	from 
			stockallocation$orderlinestockallocationtransaction sa
		inner join
			(select w.id, w.code, w.shortName, saw.stockallocation$orderLineStockAllocationTransactionID
			from 
					(select stockallocation$orderLineStockAllocationTransactionID, inventory$warehouseID
					from stockallocation$at_warehouse) saw		
				inner join
					(select id, code, shortName
					from inventory$warehouse) w on saw.inventory$warehouseID = w.id) w on sa.id = w.stockallocation$orderLineStockAllocationTransactionID	
		inner join
			(select o.*, sao.stockallocation$orderLineStockAllocationTransactionID
			from
				(select stockallocation$orderLineStockAllocationTransactionID, orderprocessing$orderID
				from stockallocation$at_order) sao
			inner join
				(select id, 
					magentoOrderID, incrementID, orderLinesMagento, orderLinesDeduped,
					_type, orderStatus, shipmentStatus, 
					allocationStatus, 
					createdDate, allocatedDate, promisedShippingDate, promisedDeliveryDate
				from orderprocessing$order
				where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')) o on sao.orderprocessing$orderID = o.id) o
					on sa.id = o.stockallocation$orderLineStockAllocationTransactionID
		inner join
			(select ol.*, saol.stockallocation$orderLineStockAllocationTransactionID
			from
				(select stockallocation$orderLineStockAllocationTransactionID, orderprocessing$orderLineID
				from stockallocation$at_orderline) saol
			inner join
				(select ol.id, ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped, 
					ola.magentoItemID, ola.orderLineType, ola.subMetaObjectName,
					ola.sku, ola.eye, ola.lensGroupID, 
					ola.basePrice, ola.baseRowPrice, 
					ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityShipped, ola.quantityAllocated, ola.quantityIssued,  
					ola.netPrice, ola.netRowPrice,
					ola.allocated
				from
						(select id, status, packsOrdered, packsAllocated, packsShipped
						from orderprocessing$orderline) ol
					inner join
						(select *
						from orderprocessing$orderlineabstract) ola on ol.id = ola.id) ol on saol.orderprocessing$orderLineID = ol.id) ol 
							on sa.id = ol.stockallocation$orderLineStockAllocationTransactionID	
	order by o.incrementID
	limit 1000;
