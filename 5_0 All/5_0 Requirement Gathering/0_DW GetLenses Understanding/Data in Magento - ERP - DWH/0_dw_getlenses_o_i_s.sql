
-- Orders

select top 100 
	order_id, order_no, order_date, 
	document_type, document_id, document_date, document_updated_at, 
	status, order_type,
	store_name, customer_id, customer_email, order_lifecycle, customer_order_seq_no, 
	business_channel, coupon_code, 
	payment_method, cc_type, shipping_carrier, shipping_method, 
	reminder_type, reminder_date, reminder_period, 
	automatic_reorder, reorder_on_flag, reorder_interval,
	presc_verification_method,
	total_qty, local_prof_fee, prof_fee_percent, vat_percent_before_prof_fee, vat_percent,
	local_subtotal_inc_vat, local_shipping_inc_vat, local_discount_inc_vat, local_store_credit_inc_vat, local_adjustment_inc_vat, local_total_inc_vat
from DW_GetLenses.dbo.order_headers
--where order_no in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
where order_no in ('8002327128', '8002290953', '8002314962', '8002380227', '8002323825')
order by order_no, document_date;

select top 100 
	line_id, order_id, order_no, order_date, 
	document_type, document_id, document_date, document_updated_at,
	store_name, 
	product_type, product_id, sku, name, 
	unit_weight, line_weight, 
	price_type,
	qty, local_prof_fee, prof_fee_percent, vat_percent_before_prof_fee, vat_percent,
	local_price_inc_vat, local_line_subtotal_inc_vat, local_shipping_inc_vat, local_discount_inc_vat, local_store_credit_inc_vat, local_adjustment_inc_vat, local_line_total_inc_vat
from DW_GetLenses.dbo.order_lines
--where order_no in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
where order_no in ('8002327128', '8002290953', '8002314962', '8002380227', '8002323825')
order by order_no, document_date;

-- Invoices

select top 100 
	order_id, order_no, order_date, 
	invoice_id, invoice_no, invoice_date,
	document_type, document_id, document_date, document_updated_at, 
	status, order_type,
	store_name, customer_id, customer_email, order_lifecycle, customer_order_seq_no, 
	business_channel, coupon_code, 
	payment_method, cc_type, shipping_carrier, shipping_method, 
	reminder_type, reminder_date, reminder_period, 
	automatic_reorder, reorder_on_flag, reorder_interval,
	presc_verification_method,
	total_qty, local_prof_fee, prof_fee_percent, vat_percent_before_prof_fee, vat_percent,
	local_subtotal_inc_vat, local_shipping_inc_vat, local_discount_inc_vat, local_store_credit_inc_vat, local_adjustment_inc_vat, local_total_inc_vat
from DW_GetLenses.dbo.invoice_headers
where order_no in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by order_no, document_date;

select top 100 
	line_id, -- order_id, order_no, order_date, 
	il.invoice_id, il.invoice_no, il.invoice_date,
	il.document_type, il.document_id, il.document_date, il.document_updated_at,
	il.store_name, 
	product_type, product_id, sku, name, 
	unit_weight, line_weight, 
	price_type,
	il.qty, il.local_prof_fee, il.prof_fee_percent, il.vat_percent_before_prof_fee, il.vat_percent,
	il.local_price_inc_vat, il.local_line_subtotal_inc_vat, il.local_shipping_inc_vat, il.local_discount_inc_vat, il.local_store_credit_inc_vat, il.local_adjustment_inc_vat, il.local_line_total_inc_vat
from 
		DW_GetLenses.dbo.invoice_lines il
	inner join
--		DW_GetLenses.dbo.invoice_headers ih on il.invoice_id = ih.invoice_id and il.document_type = ih.document_type
	DW_GetLenses.dbo.invoice_headers ih on il.document_id = ih.document_id and il.document_type = ih.document_type and il.store_name = ih.store_name
where ih.order_no in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by ih.order_no, il.document_date;

-- Shipments

select top 100 
	order_id, order_no, order_date, 
	invoice_id, invoice_no, invoice_date,
	shipment_id, shipment_no, shipment_date,
	document_type, document_id, document_date, document_updated_at, 
	status, order_type,
	store_name, customer_id, customer_email, order_lifecycle, customer_order_seq_no, 
	business_channel, coupon_code, 
	payment_method, cc_type, shipping_carrier, shipping_method, 
	reminder_type, reminder_date, reminder_period, 
	automatic_reorder, reorder_on_flag, reorder_interval,
	presc_verification_method,
	total_qty, local_prof_fee, prof_fee_percent, vat_percent_before_prof_fee, vat_percent,
	local_subtotal_inc_vat, local_shipping_inc_vat, local_discount_inc_vat, local_store_credit_inc_vat, local_adjustment_inc_vat, local_total_inc_vat
from DW_GetLenses.dbo.shipment_headers
where order_no in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by order_no, document_date;

select top 100 
	line_id, -- order_id, order_no, order_date, 
	sl.shipment_id, sl.shipment_no, sl.shipment_date,
	sl.document_type, sl.document_id, sl.document_date, sl.document_updated_at,
	sl.store_name, 
	product_type, product_id, sku, name, 
	unit_weight, line_weight, 
	price_type,
	sl.qty, sl.local_prof_fee, sl.prof_fee_percent, sl.vat_percent_before_prof_fee, sl.vat_percent,
	sl.local_price_inc_vat, sl.local_line_subtotal_inc_vat, sl.local_shipping_inc_vat, sl.local_discount_inc_vat, sl.local_store_credit_inc_vat, sl.local_adjustment_inc_vat, sl.local_line_total_inc_vat
from 
		DW_GetLenses.dbo.shipment_lines sl
	inner join
--		DW_GetLenses.dbo.invoice_headers ih on il.invoice_id = ih.invoice_id and il.document_type = ih.document_type
	DW_GetLenses.dbo.shipment_headers sh on sl.document_id = sh.document_id and sl.document_type = sh.document_type and sl.store_name = sh.store_name
where sh.order_no in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by sh.order_no, sl.document_date;
