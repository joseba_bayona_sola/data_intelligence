﻿	-- Shipment - Order
	select s.id, w.*, o.*,
		s.shipmentID, s.shipmentNumber, s.orderIncrementID, s.partitionID0_120,
		s.status, s.statusChange, 
		s.shippingMethod, s.shippingDescription, s.paymentMethod, 
		s.warehouseMethod, 
		s.fullyShipped, 
		s.labelRenderer, 
		s.shipmentValue, s.shippingTotal, s.wholeSaleCarriage, s.toFollowValue, 
		s.notify, s.syncedToMagento, s.preHoldStage, s.preHoldStatus, s.hold, s.processingFailed, s.intersite, 
		s.year, s.month, 		
		s.createdDate, s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate, s.confirmedShippingDate
	from 
		customershipments$customershipment s
	inner join
		(select w.code, w.shortName, sw.customershipments$customerShipmentID
		from
				(select customershipments$customerShipmentID, inventory$warehouseID
				from customershipments$customershipment_warehouse) sw
			inner join	
				(select id, code, shortName
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.customershipments$customerShipmentID
	inner join
		(select o.*, so.customershipments$customerShipmentID
		from
			(select customershipments$customerShipmentID, orderprocessing$orderID
			from customershipments$customershipment_order) so
		inner join
			(select id, 
				magentoOrderID, incrementID, orderLinesMagento, orderLinesDeduped,
				_type, orderStatus, shipmentStatus, 
				allocationStatus, 
				createdDate, allocatedDate, promisedShippingDate, promisedDeliveryDate
			from orderprocessing$order
			where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')) o on so.orderprocessing$orderID = o.id) o
				on s.id = o.customershipments$customerShipmentID
	order by o.incrementID, s.shipmentNumber;


-- Consignment - Package Detail
select s.id, 
	s.shipmentID, s.shipmentNumber, s.orderIncrementID, s.partitionID0_120,
	s.status, s.fullyShipped, s.warehouseMethod, s.labelRenderer, 
	s.shipmentValue, 
	s.createdDate, s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate, s.confirmedShippingDate, 
	c.*
from 
	customershipments$customershipment s
left join
	(select c.*, sc.customershipments$customerShipmentID
	from
		(select customershipments$consignmentID, customershipments$customerShipmentID
		from customershipments$scurriconsignment_shipment) sc 
	inner join
		(select c.*, 
			pk.tracking_number, pk.description, pk.width, pk.length, pk.height
		from
				(select id, 
					identifier, order_number, 
					warehouse_id, 
					consignment_number, 
					shipping_method, service_id, carrier, service, 
					delivery_instructions, tracking_url, 
					order_value, currency,
					create_date
				from customershipments$consignment) c
			inner join
				(select customershipments$packageDetailID, customershipments$consignmentID
				from customershipments$consignmentpackages) cpk on c.id = cpk.customershipments$consignmentID
			inner join
				(select id, 
					tracking_number, description, 
					width, length, height
				from customershipments$packagedetail) pk on cpk.customershipments$packageDetailID = pk.id) c on sc.customershipments$consignmentID = c.id) c
						on s.id = c.customershipments$customerShipmentID
where orderIncrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by orderIncrementID, s.shipmentNumber
limit 1000;	

-- Lifecycle Update
select s.id, 
	s.shipmentID, s.shipmentNumber, s.orderIncrementID, s.partitionID0_120,
	s.status, s.fullyShipped, s.warehouseMethod, s.labelRenderer, 
	s.shipmentValue, 
	s.createdDate, s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate, s.confirmedShippingDate, 
	li.* 
from 
	customershipments$customershipment s
inner join
	(select li.id, li.status, li.timestamp, sli.customershipments$customerShipmentID
	from
			(select customershipments$lifecycle_updateID, customershipments$customerShipmentID
			from customershipments$customershipment_progress) sli
		inner join
			(select id, status, timestamp
			from customershipments$lifecycle_update) li on sli.customershipments$lifecycle_updateID = li.id) li on s.id = li.customershipments$customerShipmentID
	
where orderIncrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by orderIncrementID, s.shipmentNumber, li.timestamp
limit 1000;

-- API Updates
select s.id, 
	s.shipmentID, s.shipmentNumber, s.orderIncrementID, s.partitionID0_120,
	s.status, s.fullyShipped, s.warehouseMethod, s.labelRenderer, 
	s.shipmentValue, 
	s.createdDate, s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate, s.confirmedShippingDate, 
	api.*
from 
	customershipments$customershipment s
inner join
	(select api.*, sapi.customershipments$customerShipmentID
	from
			(select customershipments$api_updatesID, customershipments$customerShipmentID
			from customershipments$api_updates_customershipment) sapi
		inner join
			(select id, 
				service, status, operation, 
				timestamp, 
				detail, body
			from customershipments$api_updates) api on sapi.customershipments$api_updatesID = api.id) api on s.id = api.customershipments$customerShipmentID
where orderIncrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by orderIncrementID, s.shipmentNumber, api.service, api.timestamp
limit 1000;

-- Customer Shipment Line
select s.id, 
	s.shipmentID, s.shipmentNumber, s.orderIncrementID, s.partitionID0_120,
	s.status, s.fullyShipped, s.warehouseMethod, s.labelRenderer, 
	s.shipmentValue, 
	s.createdDate, s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate, s.confirmedShippingDate, 
	sl.*
from 
	customershipments$customershipment s
inner join
	(select sl.*, sls.customershipments$customerShipmentID
	from
		(select customershipments$customerShipmentLineID, customershipments$customerShipmentID
		from customershipments$customershipmentline_customershipment) sls
	inner join
		(select id, 
			shipmentLineID, lineNumber, status, variableBreakdown, 
			stockItemID, productName, productDescription, eye, 
			quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
			packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
			price, priceFormatted, 
			subTotal, subTotal_formatted, 
			VATRate, VAT, VAT_formatted,
			fullyShipped,  
			BOMLine, 
			system$changedBy
		from customershipments$customershipmentline) sl on sls.customershipments$customerShipmentLineID = sl.id) sl on s.id = sl.customershipments$customerShipmentID	
where orderIncrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by orderIncrementID, s.shipmentNumber, sl.shipmentLineID
limit 1000;

	-- Product - Stock Item 
	select sl.*, psi.*, 
		sp.product$productID, ssi.product$stockItemID
	from  
			(select s.id s_ID, 
				s.shipmentID, s.shipmentNumber, s.orderIncrementID, s.partitionID0_120,
				s.status, s.fullyShipped, s.warehouseMethod, s.labelRenderer, 
				s.shipmentValue, 
				s.createdDate, s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate, s.confirmedShippingDate, 
				sl.*
			from 
				customershipments$customershipment s
			inner join
				(select sl.*, sls.customershipments$customerShipmentID
				from
					(select customershipments$customerShipmentLineID, customershipments$customerShipmentID
					from customershipments$customershipmentline_customershipment) sls
				inner join
					(select id, 
						shipmentLineID, lineNumber, status, variableBreakdown, 
						stockItemID, productName, productDescription, eye, 
						quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
						packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
						price, priceFormatted, 
						subTotal, subTotal_formatted, 
						VATRate, VAT, VAT_formatted,
						fullyShipped,  
						BOMLine, 
						system$changedBy
					from customershipments$customershipmentline) sl on sls.customershipments$customerShipmentLineID = sl.id) sl on s.id = sl.customershipments$customerShipmentID	
			where orderIncrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')) sl
		inner join
			(select customershipments$customerShipmentLineID, product$productID
			from customershipments$customershipmentline_product) sp on sl.id = sp.customershipments$customerShipmentLineID
		left join
			(select customershipments$customerShipmentLineID, product$stockItemID
			from customershipments$customershipmentline_stockitemforwholesale) ssi on sl.id = ssi.customershipments$customerShipmentLineID
		inner join
			(select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
				p.id productID, p.productType, p.SKU, p.oldSKU, p.description, 
				-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
				-- si.id, 
				si.id stockItemID, si.SKU SKU_SI, si.oldSKU, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
			from 
					product$productfamily pf
				inner join
					(select product$productID, product$productFamilyID 
					from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
				inner join
					(select id, 
						productType, 
						SKU, oldSKU, description, 
						subMetaObjectName, price,
						isBOM, valid, displaySubProductOnPackingSlip,
						createdDate, changedDate, 
						system$owner, system$changedBy	
					from product$product) p on pfp.product$productID = p.id		
				inner join
					(select product$stockItemID, product$productID 
					from product$stockitem_product) sip on p.id = sip.product$productID 
				inner join
					(select id, 
						SKU, oldSKU, packSize, 
						manufacturerArticleID, manufacturerCodeNumber,
						SNAPDescription, SNAPUploadStatus, 
						changed
					from product$stockitem) si on sip.product$stockItemID = si.id) psi
						on sp.product$productID = psi.productID -- and ssi.product$stockItemID = psi.stockItemID
order by sl.orderIncrementID, sl.shipmentNumber, sl.shipmentLineID

-- Pickline - Pickline Update / Shipment
select s.id, 
	s.shipmentID, s.shipmentNumber, s.orderIncrementID, s.partitionID0_120,
	s.status, s.fullyShipped, s.warehouseMethod, s.labelRenderer, 
	s.shipmentValue, 
	s.createdDate, s.dispatchDate, s.expectedShippingDate, s.expectedDeliveryDate, s.confirmedShippingDate, 
	pl.*
from 
	customershipments$customershipment s
left join
	(select pl.*, spl.customershipments$customerShipmentID
	from
		(select customershipments$pickLineID, customershipments$customerShipmentID
		from customershipments$picklist_customershipment) spl
	inner join
		(select pl.id id_PL, pl.pickLineID, plu.*
		from
				(select id, pickLineID
				from customershipments$pickline) pl
			inner join
				(select customershipments$pickLineID, customershipments$pickLineUpdateID
				from customershipments$pickline_shipmentlineupdate) plplu on pl.id = plplu.customershipments$pickLineID 
			inner join	
				(select id, 
					status, timestamp,
					qtyOrdered, qtyAllocated, qtyTasked, qtyPicked, qtyShipped, qtyDelivered, qtyDueOut
				from customershipments$picklineupdate) plu on plplu.customershipments$pickLineUpdateID = plu.id) pl on spl.customershipments$pickLineID = pl.id_PL) pl
					on s.id = pl.customershipments$customerShipmentID
	
where orderIncrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825', '5001326091', '8002416303', '8001713505')
order by orderIncrementID, s.shipmentNumber
limit 1000;