
select *
from DW_GetLenses.dbo.invoice_headers
where invoice_id = 4087314

select *
from DW_GetLenses.dbo.invoice_lines
where document_id in (4039455, 1245801, 1245957)
order by document_date

select top 1000 *
from DW_GetLenses.dbo.invoice_lines
where document_date > '2016-12-01' and product_type is null

select top 1000 *
from DW_GetLenses.dbo.invoice_lines
where document_date > '2016-12-01' and local_adjustment_inc_vat is not null and local_adjustment_inc_vat <> 0

----

select *
from DW_GetLenses.dbo.invoice_headers
where order_no = '26000144170'

select top 1000 *
from DW_GetLenses.dbo.invoice_lines
where document_id in (4089628, 1246070)
order by document_date

----

select *
from DW_GetLenses.dbo.invoice_headers
where order_no = '26000143619'

select top 1000 *
from DW_GetLenses.dbo.invoice_lines
where document_id in (4034262, 1246060)
order by document_date
