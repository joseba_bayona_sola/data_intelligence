
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id, 
	count(*) over (partition by order_id) num_rep
from DW_GetLenses.dbo.dw_hist_order
order by num_rep desc

select top 1000 shipment_id, source, created_at, store_id, customer_id, 
	count(*) over (partition by shipment_id) num_rep
from DW_GetLenses.dbo.dw_hist_shipment
order by num_rep desc


--- 

select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where customer_id = 14132
order by created_at desc

select top 1000 shipment_id, source, created_at, store_id, customer_id
from DW_GetLenses.dbo.dw_hist_shipment
where customer_id = 14132
order by created_at desc

---

select top 1000 ho.order_id, hs.shipment_id, ho.source, hs.source, 
	ho.created_at cr_o, hs.created_at cr_s, 
	ho.store_id, ho.customer_id, hs.customer_id, 
	case when (ho.order_id is null) then '01' else case when (hs.shipment_id is null) then '10' else '11' end end type
from 
		DW_GetLenses.dbo.dw_hist_shipment hs
	full join
		DW_GetLenses.dbo.dw_hist_order ho on hs.shipment_id = ho.order_id and hs.customer_id = ho.customer_id
-- where hs.shipment_id is null
where ho.order_id is null
order by ho.customer_id, ho.order_id 

select type, count(*)
from
	(select ho.order_id, hs.shipment_id, ho.source, 
		ho.created_at cr_o, hs.created_at cr_s, 
		ho.store_id, ho.customer_id, 
		case when (ho.order_id is null) then '01' else case when (hs.shipment_id is null) then '10' else '11' end end type
	from 
			DW_GetLenses.dbo.dw_hist_shipment hs
		full join
			DW_GetLenses.dbo.dw_hist_order ho on hs.shipment_id = ho.order_id and hs.customer_id = ho.customer_id) t
group by type
order by type
