
select *
from
	(select order_id, source, created_at, store_id, grand_total, customer_id, 
		rank() over (partition by source order by order_id) r_source
	from DW_GetLenses.dbo.dw_hist_order) t
where r_source < 5

select *
from
	(select order_id, source, created_at, store_id, grand_total, customer_id, 
		rank() over (partition by store_id order by order_id) r_store
	from DW_GetLenses.dbo.dw_hist_order) t
where r_store < 5


select order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where customer_id = 895017
order by created_at

-- 

select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where customer_id = 744465

select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where customer_id = 744465
order by document_date

select top 1000 store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_Proforma.dbo.order_headers
where --left(store_name, charindex('.', store_name)-1) = 'lenshome' and 
	customer_id = 744465
order by document_date

7307924
7651939
7162185
7055328