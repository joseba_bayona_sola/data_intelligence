
-- VISIO OPTIK
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'visio_db'

select store_name, order_id, order_id - 600000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_Proforma.dbo.order_headers
where customer_id = 1355857
order by document_date

select top 1000 
	ho.order_id, pr.order_id order_id_pr, pr.order_id_n, pr.order_no, 
	ho.store_id, pr.store_name, 
	pr.document_type, 
	ho.created_at, pr.document_date, 
	case when (ho.order_id is null) then '01' else case when (pr.order_id is null) then '10' else '11' end end type
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'visio_db') ho
	full join
		(select store_name, order_id, order_id - 600000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_Proforma.dbo.order_headers
		where left(store_name, charindex('.', store_name)-1) = 'visiooptik') pr on ho.order_id = pr.order_id_n and ho.customer_id = pr.customer_id

select type, count(*)
from
	(select 
		ho.order_id, pr.order_id order_id_pr, pr.order_id_n, pr.order_no, 
		ho.store_id, pr.store_name, 
		pr.document_type, 
		ho.created_at, pr.document_date, 
		case when (ho.order_id is null) then '01' else case when (pr.order_id is null) then '10' else '11' end end type
	from
			(select order_id, source, created_at, store_id, grand_total, customer_id
			from DW_GetLenses.dbo.dw_hist_order
			where source = 'visio_db') ho
		full join
			(select store_name, order_id, order_id - 600000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
			from DW_Proforma.dbo.order_headers
			where left(store_name, charindex('.', store_name)-1) = 'visiooptik') pr on ho.order_id = pr.order_id_n and ho.customer_id = pr.customer_id) t
group by type
order by type

-- LENSBASE

select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'lensbase_db'

select store_name, order_id, order_id - 800000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_Proforma.dbo.order_headers
where customer_id = 892642
order by document_date

select top 1000 
	ho.order_id, pr.order_id order_id_pr, pr.order_id_n, pr.order_no, 
	ho.store_id, pr.store_name, 
	pr.document_type, 
	ho.created_at, pr.document_date, 
	ho.customer_id, pr.customer_id,
	case when (ho.order_id is null) then '01' else case when (pr.order_id is null) then '10' else '11' end end type
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'lensbase_db') ho
	full join
		(select store_name, order_id, order_id - 800000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_Proforma.dbo.order_headers
		where left(store_name, charindex('.', store_name)-1) = 'lensbase') pr on ho.order_id = pr.order_id_n and ho.customer_id = pr.customer_id
where pr.order_id is null

select type, count(*)
from
	(select 
		ho.order_id, pr.order_id order_id_pr, pr.order_id_n, pr.order_no, 
		ho.store_id, pr.store_name, 
		pr.document_type, 
		ho.created_at, pr.document_date, 
		case when (ho.order_id is null) then '01' else case when (pr.order_id is null) then '10' else '11' end end type
	from
			(select order_id, source, created_at, store_id, grand_total, customer_id
			from DW_GetLenses.dbo.dw_hist_order
			where source = 'lensbase_db') ho
		full join
			(select store_name, order_id, order_id - 800000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
			from DW_Proforma.dbo.order_headers
			where left(store_name, charindex('.', store_name)-1) = 'lensbase') pr on ho.order_id = pr.order_id_n and ho.customer_id = pr.customer_id) t
group by type
order by type

--

select top 1000 
	ho.order_id, pr.order_id order_id_pr, pr.order_id_n, pr.order_no, 
	ho.store_id, pr.store_name, 
	pr.document_type, 
	ho.created_at, pr.document_date, 
	case when (ho.order_id is null) then '01' else case when (pr.order_id is null) then '10' else '11' end end type
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'lensbase_db') ho
	full join
		(select store_name, order_id, order_id - 900000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_Proforma.dbo.order_headers
		where left(store_name, charindex('.', store_name)-1) = 'lenshome') pr on ho.order_id = pr.order_id_n and ho.customer_id = pr.customer_id

select type, count(*)
from
	(select 
		ho.order_id, pr.order_id order_id_pr, pr.order_id_n, pr.order_no, 
		ho.store_id, pr.store_name, 
		pr.document_type, 
		ho.created_at, pr.document_date, 
		case when (ho.order_id is null) then '01' else case when (pr.order_id is null) then '10' else '11' end end type
	from
			(select order_id, source, created_at, store_id, grand_total, customer_id
			from DW_GetLenses.dbo.dw_hist_order
			where source = 'lensbase_db') ho
		full join
			(select store_name, order_id, order_id - 900000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
			from DW_Proforma.dbo.order_headers
			where left(store_name, charindex('.', store_name)-1) = 'lenshome') pr on ho.order_id = pr.order_id_n and ho.customer_id = pr.customer_id) t
group by type
order by type


-- VISIO DIRECT
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'visiondirect' and customer_id = 580999

select store_name, order_id, order_id - 700000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_Proforma.dbo.order_headers
where customer_id = 580999
order by document_date

select top 1000 
	ho.order_id, pr.order_id order_id_pr, pr.order_id_n, pr.order_no, 
	ho.store_id, pr.store_name, 
	pr.document_type, 
	ho.created_at, pr.document_date, 
	ho.customer_id, pr.customer_id,
	case when (ho.order_id is null) then '01' else case when (pr.order_id is null) then '10' else '11' end end type
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'visiondirect') ho
	full join
		(select store_name, order_id, order_id - 700000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_Proforma.dbo.order_headers
		where left(store_name, charindex('.', store_name)-1) = 'visiondirect') pr on ho.order_id = pr.order_id_n and ho.customer_id = pr.customer_id
where pr.order_id is null

select type, count(*)
from
	(select 
		ho.order_id, pr.order_id order_id_pr, pr.order_id_n, pr.order_no, 
		ho.store_id, pr.store_name, 
		pr.document_type, 
		ho.created_at, pr.document_date, 
		ho.customer_id, 
		case when (ho.order_id is null) then '01' else case when (pr.order_id is null) then '10' else '11' end end type
	from
			(select order_id, source, created_at, store_id, grand_total, customer_id
			from DW_GetLenses.dbo.dw_hist_order
			where source = 'visiondirect') ho
		full join
			(select store_name, order_id, order_id - 700000000 order_id_n, order_no, document_type, document_date, customer_id, customer_order_seq_no
			from DW_Proforma.dbo.order_headers
			where left(store_name, charindex('.', store_name)-1) = 'visiondirect') pr on ho.order_id = pr.order_id_n and ho.customer_id = pr.customer_id) t
group by type
order by type
