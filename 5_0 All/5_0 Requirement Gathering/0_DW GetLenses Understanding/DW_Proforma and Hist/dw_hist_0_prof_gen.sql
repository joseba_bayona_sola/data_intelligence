
select top 1000 *
from DW_GetLenses.dbo.dw_hist_order

select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order

	select count(*)	-- 2.183.646
	from DW_GetLenses.dbo.dw_hist_order

	select source, count(*), min(created_at), max(created_at), min(order_id), max(order_id)
	from DW_GetLenses.dbo.dw_hist_order
	group by source
	order by source

	select ho.store_id, s.store_name, ho.num
	from
			(select store_id, count(*) num
			from DW_GetLenses.dbo.dw_hist_order
			group by store_id) ho
		left join
			DW_GetLenses.dbo.dw_stores_full s on ho.store_id = s.store_id
	order by ho.store_id

	select ho.source, ho.store_id, s.store_name, ho.num
	from
			(select source, store_id, count(*) num
			from DW_GetLenses.dbo.dw_hist_order
			group by source, store_id) ho
		left join
			DW_GetLenses.dbo.dw_stores_full s on ho.store_id = s.store_id
	--order by ho.source, ho.store_id
	order by ho.store_id, ho.source

------------------------------------------------------

select top 1000 *
from DW_GetLenses.dbo.dw_hist_shipment

select top 1000 shipment_id, source, created_at, store_id, customer_id
from DW_GetLenses.dbo.dw_hist_shipment

	select count(*)	-- 2.101.440
	from DW_GetLenses.dbo.dw_hist_shipment

	-- ?? getlenses_nl_l - masterlens_db - vh1_db
	select source, count(*), min(created_at), max(created_at)
	from DW_GetLenses.dbo.dw_hist_shipment
	group by source
	order by source

	select ho.store_id, s.store_name, ho.num
	from
			(select store_id, count(*) num
			from DW_GetLenses.dbo.dw_hist_order
			group by store_id) ho
		left join
			DW_GetLenses.dbo.dw_stores_full s on ho.store_id = s.store_id
	order by ho.store_id

	select ho.source, ho.store_id, s.store_name, ho.num
	from
			(select source, store_id, count(*) num
			from DW_GetLenses.dbo.dw_hist_order
			group by source, store_id) ho
		left join
			DW_GetLenses.dbo.dw_stores_full s on ho.store_id = s.store_id
	--order by ho.source, ho.store_id
	order by ho.store_id, ho.source