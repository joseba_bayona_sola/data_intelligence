-- ASDA
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'asda_db' and customer_id = 136471
order by order_id

select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where customer_id = 136471
order by document_date

select top 1000 
	ho.order_id, gl.order_id order_id_gl, gl.order_no, 
	ho.store_id, gl.store_name, 
	gl.document_type, 
	ho.created_at, gl.document_date
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'asda_db') ho
	inner join
		(select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers) gl 
			--on ho.customer_id = gl.customer_id and ho.order_id = gl.order_id
			on ho.customer_id = gl.customer_id and ho.created_at = gl.document_date

select top 1000 *
from
	(select 'asda_db' store_name, customer_id, order_id, ' ' order_no, created_at, rank() over (partition by customer_id order by created_at) customer_order_seq_no
	from DW_GetLenses.dbo.dw_hist_order
	where source = 'asda_db' 
	union 
	select store_name, customer_id, order_id, order_no, document_date, customer_order_seq_no
	from DW_GetLenses.dbo.order_headers
	where customer_id in 
		(select distinct customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'asda_db')) t 
order by customer_id, created_at


-- VH1
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'vhi_db' and customer_id = 101313
order by order_id

select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where customer_id = 101313
order by document_date

select top 1000 
	ho.order_id, gl.order_id order_id_gl, gl.order_no, 
	ho.store_id, gl.store_name, 
	gl.document_type, 
	ho.created_at, gl.document_date
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'vhi_db') ho
	inner join
		(select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers) gl 
			--on ho.customer_id = gl.customer_id and ho.order_id = gl.order_id
			on ho.customer_id = gl.customer_id and ho.created_at = gl.document_date


select top 1000 *
from
	(select 'vhi_db' store_name, customer_id, order_id, ' ' order_no, created_at, rank() over (partition by customer_id order by created_at) customer_order_seq_no
	from DW_GetLenses.dbo.dw_hist_order
	where source = 'vhi_db' 
	union 
	select store_name, customer_id, order_id, order_no, document_date, customer_order_seq_no
	from DW_GetLenses.dbo.order_headers
	where customer_id in 
		(select distinct customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'vhi_db')) t 
order by customer_id, created_at


-- POSTOPTICS
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'postoptics_db' and customer_id = 14132
order by order_id

select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where customer_id = 14132
order by document_date

select top 1000 
	ho.order_id, gl.order_id order_id_gl, gl.order_no, 
	ho.store_id, gl.store_name, 
	gl.document_type, 
	ho.created_at, gl.document_date
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'postoptics_db') ho
	inner join
		(select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers) gl 
			--on ho.customer_id = gl.customer_id and ho.order_id = gl.order_id
			on ho.customer_id = gl.customer_id and ho.created_at = gl.document_date

select top 1000 *
from
	(select 'postoptics_db' store_name, customer_id, order_id, ' ' order_no, created_at, rank() over (partition by customer_id order by created_at) customer_order_seq_no
	from DW_GetLenses.dbo.dw_hist_order
	where source = 'postoptics_db' 
	union 
	select store_name, customer_id, order_id, order_no, document_date, customer_order_seq_no
	from DW_GetLenses.dbo.order_headers
	where customer_id in 
		(select distinct customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'postoptics_db')) t 
order by customer_id, created_at

-- GETLENSES DB
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'getlenses_db' and customer_id = 68564
order by order_id

select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where customer_id = 68564
order by document_date

select top 1000 
	ho.order_id, gl.order_id order_id_gl, gl.order_no, 
	ho.store_id, gl.store_name, 
	gl.document_type, 
	ho.created_at, gl.document_date
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'getlenses_db') ho
	inner join
		(select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers) gl 
			on ho.customer_id = gl.customer_id and ho.order_id = gl.order_id
			--on ho.customer_id = gl.customer_id and ho.created_at = gl.document_date

select top 1000 *
from
	(select 'getlenses_db' store_name, customer_id, order_id, ' ' order_no, created_at, rank() over (partition by customer_id order by created_at) customer_order_seq_no
	from DW_GetLenses.dbo.dw_hist_order
	where source = 'getlenses_db' 
	union 
	select store_name, customer_id, order_id, order_no, document_date, customer_order_seq_no
	from DW_GetLenses.dbo.order_headers
	where customer_id in 
		(select distinct customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'getlenses_db')) t 
order by customer_id, created_at

-- GETLENSES NL DB
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'getlenses_nl_' and customer_id = 71619
order by order_id

select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where customer_id = 71619
order by document_date

select top 1000 
	ho.order_id, gl.order_id order_id_gl, gl.order_no, 
	ho.store_id, gl.store_name, 
	gl.document_type, 
	ho.created_at, gl.document_date
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'getlenses_nl_') ho
	inner join
		(select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers) gl 
			--on ho.customer_id = gl.customer_id and ho.order_id = gl.order_id
			on ho.customer_id = gl.customer_id and ho.created_at = gl.document_date

select top 1000 *
from
	(select 'getlenses_nl_' store_name, customer_id, order_id, ' ' order_no, created_at, rank() over (partition by customer_id order by created_at) customer_order_seq_no
	from DW_GetLenses.dbo.dw_hist_order
	where source = 'getlenses_nl_' 
	union 
	select store_name, customer_id, order_id, order_no, document_date, customer_order_seq_no
	from DW_GetLenses.dbo.order_headers
	where customer_id in 
		(select distinct customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'getlenses_nl_')) t 
order by customer_id, created_at

-- MASTERLENS
select top 1000 order_id, source, created_at, store_id, grand_total, customer_id
from DW_GetLenses.dbo.dw_hist_order
where source = 'masterlens_db' and customer_id = 324357
order by order_id

select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
from DW_GetLenses.dbo.order_headers
where customer_id = 324357
order by document_date

select top 1000 
	ho.order_id, gl.order_id order_id_gl, gl.order_no, 
	ho.store_id, gl.store_name, 
	gl.document_type, 
	ho.created_at, gl.document_date
from
		(select order_id, source, created_at, store_id, grand_total, customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'masterlens_db') ho
	inner join
		(select store_name, order_id, order_no, document_type, document_date, customer_id, customer_order_seq_no
		from DW_GetLenses.dbo.order_headers) gl 
			on ho.customer_id = gl.customer_id and ho.order_id = gl.order_id
			--on ho.customer_id = gl.customer_id and ho.created_at = gl.document_date

select top 1000 *
from
	(select 'masterlens_db' store_name, customer_id, order_id, ' ' order_no, created_at, rank() over (partition by customer_id order by created_at) customer_order_seq_no
	from DW_GetLenses.dbo.dw_hist_order
	where source = 'masterlens_db' 
	union 
	select store_name, customer_id, order_id, order_no, document_date, customer_order_seq_no
	from DW_GetLenses.dbo.order_headers
	where customer_id in 
		(select distinct customer_id
		from DW_GetLenses.dbo.dw_hist_order
		where source = 'masterlens_db')) t 
order by customer_id, created_at