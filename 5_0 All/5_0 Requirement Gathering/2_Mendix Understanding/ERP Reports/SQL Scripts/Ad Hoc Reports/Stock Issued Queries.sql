﻿--

select bsi_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsi.IssuedQuantity,0)) as QtyIssued
from inventory$batchstockissue_warehousestockitembatch bsi_whsib
LEFT OUTER JOIN inventory$batchstockissue bsi
on bsi_whsib.inventory$batchstockissueid = bsi.id
where bsi.cancelled = true
group by bsi_whsib.inventory$warehousestockitembatchid

--

select *
from 
inventory$batchstockissue bsi
where bsi.cancelled = true

	