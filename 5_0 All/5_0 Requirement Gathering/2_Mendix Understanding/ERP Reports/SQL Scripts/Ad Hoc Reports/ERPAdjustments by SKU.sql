﻿--
select 
	warehouse,
	batchid,
	sku,
	productfamily,
	packsize,
	QuantityReceived,
	QuantityAdded,
	QuantityERPAdded,
	QuantityDisposed,
	QuantityERPDisposed,
	QuantityIssued,
	CurrentStock
-- 	sum(QuantityReceived) QuantityReceived,
-- 	sum(QuantityAdded) QuantityAdded,
-- 	sum(QuantityERPAdded) QuantityERPAdded,
-- 	sum(QuantityDisposed) QuantityDisposed,
-- 	sum(QuantityERPDisposed ) QuantityERPDisposed,
-- 	sum(QuantityIssued) QuantityIssued,
-- 	sum(CurrentStock) CurrentStock
from
	a$reporting$monthlystockinwarehouse
where
	warehouse || sku in
(
	select distinct warehouse || sku
	from a$reporting$monthlystockinwarehouse
	where QuantityERPAdded !=0 or QuantityERPDisposed !=0
)
and
	sku = '023280000000000000000011'
-- group by
--  	warehouse,
-- 	sku,
-- 	productfamily,
-- 	packsize
order by
	warehouse,
	batchid desc,
	sku,
	productfamily,
	packsize