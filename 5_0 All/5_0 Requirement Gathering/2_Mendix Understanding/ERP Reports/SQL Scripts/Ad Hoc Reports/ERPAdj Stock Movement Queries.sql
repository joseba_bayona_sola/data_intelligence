﻿
-----------------------------------------------------------------------------------------------------------------------------
--
select
	wh.name,
	si.sku,
	whsib.batch_id,
	whsib.arriveddate,
	whsib.confirmeddate,
	whsib.stockregistereddate,
	whsib.receivedquantity,
	whsib.issuedquantity,
	whsib.disposedquantity,
	bsm.BatchStockMovement_ID,
	bsm.Quantity,
	bsm.comment,
	sm.movetype,
	sm.dateclosed,
	sm.datecreated,
	sm.movementtype,
	sm.fromstate,
	sm.tostate,
	sm.stockadjustment,
	sm.reasonid,
	sm.qtytasked,
	sm.qtyactioned
from
	inventory$batchstockmovement bsm
LEFT OUTER JOIN
	inventory$batchstockmovement_stockmovement bsm_sm
on
	bsm.id = bsm_sm.inventory$batchstockmovementid
LEFT OUTER JOIN
	inventory$stockmovement sm
on
	bsm_sm.inventory$stockmovementid = sm.id
LEFT OUTER JOIN
	inventory$batchstockmovement_warehousestockitembatch bsm_whsib
on
	bsm.id = bsm_whsib.inventory$batchstockmovementid
LEFT OUTER JOIN
	inventory$warehousestockitembatch whsib
on
	bsm_whsib.inventory$warehousestockitembatchid = whsib.id
LEFT OUTER JOIN
	inventory$warehousestockitembatch_warehousestockitem whsib_whsi
on
	whsib.id = whsib_whsi.inventory$warehousestockitembatchid
	
left outer join
	inventory$warehousestockitem whsi
on
	whsib_whsi.inventory$warehousestockitemid = whsi.id
	
left outer join
	inventory$located_at whsi_wh
on
	whsi.id = whsi_wh.inventory$warehousestockitemid
	
left outer join
	inventory$warehouse wh
on
	whsi_wh.inventory$warehouseid = wh.id
left outer join
	inventory$warehousestockitem_stockitem whsi_si
on
	whsi.id = whsi_si.inventory$warehousestockitemid
left outer join
	product$stockitem si
on
	whsi_si.product$stockitemid = si.id
where
 	sm.stockadjustment in ('Positive','Negative')
and
	bsm.comment like '%Patch028%'
-- and
-- 	whsib.batch_id in
-- (
-- 357355,
-- 400073,
-- 401408,
-- 401875,
-- 478427,
-- 526051,
-- 576669,
-- 601445,
-- 627148,
-- 679622
-- )
-- group by
-- 	sm.movetype,
-- 	sm.movementtype,
-- 	sm.fromstate,
-- 	sm.tostate,
-- 	sm.stockadjustment
-- and
-- 	whsib.arriveddate > sm.dateclosed
-- and
-- 	bsm.BatchStockMovement_ID = 32631
order by
	whsib.arriveddate,
--	bsm.quantity,
	wh.name,
	si.sku,
	whsib.batch_id,
	bsm.BatchStockMovement_ID
--	sm.stockadjustment,
--	sm.movetype,

-------------------------------------------------------------------------------------------------------------------------------
-- 
-- select 
-- 	whsib.batch_id,
-- 	whsib.arriveddate,
-- 	whsib.confirmeddate,
-- 	whsib.stockregistereddate,
-- 	whsib.receivedquantity,
-- 	whsib.issuedquantity,
-- 	whsib.disposedquantity
-- from inventory$warehousestockitembatch whsib
-- where 
-- --	whsib.arriveddate is null
-- --and
-- --	whsib.confirmeddate is null
-- --and
-- 	whsib.stockregistereddate is null
-- order by
-- 	whsib.arriveddate,
-- 	whsib.confirmeddate,
-- 	whsib.stockregistereddate
-- 

--select * from databasepatching$runtimelog where patchname like '%27'
