﻿select
	bsm_whsib.inventory$warehousestockitembatchid as whsib_id,
	bsm.Quantity,
	sm.movetype,
	sm.movementtype,
	sm.stockadjustment,
	sm.qtytasked,
	sm.qtyactioned
from
	inventory$batchstockmovement_warehousestockitembatch bsm_whsib
LEFT OUTER JOIN
	inventory$batchstockmovement bsm
on
	bsm_whsib.inventory$batchstockmovementid = bsm.id
LEFT OUTER JOIN
	inventory$batchstockmovement_stockmovement bsm_sm
on
	bsm.id = bsm_sm.inventory$batchstockmovementid
LEFT OUTER JOIN
	inventory$stockmovement sm
on
	bsm_sm.inventory$stockmovementid = sm.id
where
	sm.stockadjustment in ('Negative','Positive')
-- and
-- 	sm.DateClosed < (select max(monthend) from tmp_rundate)
-- group by
-- 	bsm_whsib.inventory$warehousestockitembatchid
order by
	bsm_whsib.inventory$warehousestockitembatchid
limit 1000