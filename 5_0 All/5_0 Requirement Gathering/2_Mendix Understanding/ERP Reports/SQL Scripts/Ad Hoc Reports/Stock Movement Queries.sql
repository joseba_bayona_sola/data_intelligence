﻿--
select
distinct
--	sm.moveid,
	sm.movetype,
	sm.movementtype,
	sm.stockadjustment,
	sm.fromstate,
	sm.tostate,
	sm.fromstatus,
	sm.tostatus
-- 	sm.unit,
-- 	sm.movelineid,
-- 	sm.jobsequence,
-- 	sm.dateclosed,
-- 	sm.fromfacility,
-- 	sm.qtyactioned,
-- 	sm.fromsite,
-- 	sm.tosite,
-- 	sm.stage,
-- 	sm.reasonid,
-- 	sm.status,
-- 	sm.fromzone,
-- 	sm.frombay,
-- 	sm.operator,
-- 	sm.fromslot,
-- 	sm.siteid,
-- 	sm.datecreated,
-- 	sm.fromsection,
-- 	sm.skuid,
-- 	sm._class_jsonkey,
-- 	sm.reference,
-- 	sm.fromowner,
-- 	sm.qtytasked,
from
	inventory$stockmovement sm
-- where
-- 	sm.stockadjustment in ('Positive','Negative')
-- and
-- 	sm.movementtype like 'ERP%'
-- group by
-- 	sm.movetype,
-- 	sm.movementtype,
-- 	sm.fromstate,
-- 	sm.tostate,
-- 	sm.stockadjustment
order by
	sm.stockadjustment,
	sm.movetype,
	sm.movementtype
--
-----------------------------------------------------------------------------------------------------------------------------
--
-- select
-- 	sm.movetype,
-- 	sm.movementtype,
-- 	sm.fromstate,
-- 	sm.tostate,
-- 	sm.stockadjustment,
-- 	sum(coalesce(bsm.Quantity,0)) as QtyAdded
-- from
-- 	inventory$batchstockmovement bsm
-- LEFT OUTER JOIN
-- 	inventory$batchstockmovement_stockmovement bsm_sm
-- on
-- 	bsm.id = bsm_sm.inventory$batchstockmovementid
-- LEFT OUTER JOIN
-- 	inventory$stockmovement sm
-- on
-- 	bsm_sm.inventory$stockmovementid = sm.id
-- where
-- 	sm.stockadjustment in ('Positive','Negative')
-- group by
-- 	sm.movetype,
-- 	sm.movementtype,
-- 	sm.fromstate,
-- 	sm.tostate,
-- 	sm.stockadjustment
-- order by
-- --	sm.stockadjustment,
-- --	sm.movetype,
-- 	sm.movementtype
-- 
-- 
-- 	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyDisposed
-- 	from inventory$batchstockmovement_warehousestockitembatch bsm_whsib
-- 	LEFT OUTER JOIN inventory$batchstockmovement bsm
-- 	on bsm_whsib.inventory$batchstockmovementid = bsm.id
-- 	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
-- 	on bsm.id = bsm_sm.inventory$batchstockmovementid
-- 	LEFT OUTER JOIN inventory$stockmovement sm
-- 	on bsm_sm.inventory$stockmovementid = sm.id
-- 	where sm.stockadjustment = 'Negative'
-- --	and sm.DateClosed < (select max(monthend) from tmp_rundate)
-- 	group by bsm_whsib.inventory$warehousestockitembatchid
-- 
-- 
-- select * from inventory$batchstockissue
-- where cancelled = true
-- limit 10000