﻿
select
	whsib.batch_id,
	whsib.createddate,
	sh.shipmentid,
	sh.receiptid,
	sh.status,
	sh.ShipmentType,
	whsib.createddate,
	whsib.groupfifodate,
	whsib.arriveddate,
	age(whsib.arriveddate,whsib.stockregistereddate),
	whsib.confirmeddate,
	age(whsib.confirmeddate,whsib.stockregistereddate),
	whsib.stockregistereddate,
	sh.createddate,
	sh.arriveddate,
	sh.confirmeddate,
	sh.stockregistereddate
from
	inventory$warehousestockitembatch whsib
LEFT OUTER JOIN
--inner join
	inventory$warehousestockitembatch_shipment whsib_sh
on
	whsib.id = whsib_sh.inventory$warehousestockitembatchid
left outer join
	warehouseshipments$shipment sh
on
	whsib_sh.warehouseshipments$shipmentid = sh.id
where
	whsib.confirmeddate > whsib.stockregistereddate
or
	whsib.arriveddate > whsib.stockregistereddate
or
	whsib.arriveddate > whsib.confirmeddate
-- [(ConfirmedDate > StockRegisteredDate) or (ArrivedDate > StockRegisteredDate)
-- or (ArrivedDate > ConfirmedDate)]
-- and not
-- 	whsib.confirmeddate > whsib.stockregistereddate
--and not
--	date_trunc('day',whsib.confirmeddate) = date_trunc('day',whsib.stockregistereddate)
-- or
-- 	whsib.arriveddate is null
-- or
-- 	whsib.arriveddate > whsib.stockregistereddate
-- or
-- 	whsib.confirmeddate > whsib.stockregistereddate
--  	whsib.groupfifodate is not null
-- and
-- 	whsib.stockregistereddate <  '2016-10-13' 
-- and
-- 	whsib.stockregistereddate >= '2016-10-12' 
--and
 --	whsib.createddate  >= '2016-07-27'
order by
	whsib.arriveddate desc,
	age(whsib.arriveddate,whsib.stockregistereddate) desc,
	whsib.batch_id desc,
	whsib.stockregistereddate desc,
	whsib.confirmeddate desc



