﻿--
select
 distinct
	left(sh.shipmentid,3),
	left(sh.receiptid,3),
	sh.shipmenttype
--	left(ltrim(sh.supplieradviceref),3)
-- 	shol.stockitem,
-- 	sh.arriveddate DateDelivered, 
-- 	sh.confirmeddate DateReceived, 
-- 	sh.stockregistereddate DateRegistered,
-- 	shol.unitcost as SupplierUnitCost,
-- 	shol.status Status,
-- 	shol.Quantityordered,
-- 	shol.Quantityreceived,
-- 	shol.Quantityrejected,
-- 	shol.Quantityaccepted
from
-- 	warehouseshipments$shipmentorderline shol
-- left outer join
-- 	warehouseshipments$shipmentorderline_shipment as shol_sh
-- on
-- 	shol.id = shol_sh.warehouseshipments$shipmentorderlineid
-- left outer join
	warehouseshipments$shipment as sh
-- on
-- 	shol_sh.warehouseshipments$shipmentid = sh.id
order by
	sh.shipmenttype

