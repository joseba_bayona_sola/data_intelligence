﻿
select
	whsib.Batch_ID,
	whsib.ArrivedDate,
	whsib.ConfirmedDate,
	whsib.StockRegisteredDate,
	whsib.fullyissued,
	whsib.fullyallocated,
	whsib.ReceivedQuantity,
	whsib.allocatedquantity,
	whsib.issuedquantity,
	whsib.onholdquantity,
	whsib.disposedquantity,
	whsib.ReceivedQuantity - whsib.issuedquantity -	whsib.disposedquantity as remainingquantity,
	whsib.allocatedquantity - whsib.issuedquantity as outstandingallocationquantity,
	whsib.ReceivedQuantity - whsib.allocatedquantity - whsib.onholdquantity as freetosellquantity,
	bsa.allocatedquantity,
	bsa.cancelled,
	bsi.createddate,
	bsi.issuedquantity,
	bsi.cancelled
from
	inventory$warehousestockitembatch as whsib
left outer join
	inventory$batchstockissue_warehousestockitembatch bsi_whsib
on
	whsib.id = bsi_whsib.inventory$warehousestockitembatchid
LEFT OUTER JOIN
	inventory$batchstockissue bsi
on
	bsi_whsib.inventory$batchstockissueid = bsi.id
left outer join
	inventory$batchstockissue_batchstockallocation bsi_bsa
on
	bsi.id = bsi_bsa.inventory$batchstockissueid
left outer join
	inventory$batchstockallocation bsa
on
	bsi_bsa.inventory$batchstockallocationid = bsa.id
where
--	whsib.Batch_ID = 634920
--	whsib.ReceivedQuantity = 0
	bsa.cancelled != bsi.cancelled
--and
--	bsi.cancelled = true
order by
	whsib.Batch_ID,
	whsib.ArrivedDate,
	bsi.createddate
limit 1000