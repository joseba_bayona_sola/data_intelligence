﻿--
select
	us.name,
	ur.name
from
--	administration$account
	system$user us
left outer join
	system$userroles roles
on
	us.id = roles.system$userid
left outer join
	system$userrole ur
on
	roles.system$userroleid = ur.id
where
	ur.name in ('SystemAdministrator') --,'Purchasing_Admin','Warehouse_Admin','UserAdministrator')
order by
	us.name,
	ur.name
