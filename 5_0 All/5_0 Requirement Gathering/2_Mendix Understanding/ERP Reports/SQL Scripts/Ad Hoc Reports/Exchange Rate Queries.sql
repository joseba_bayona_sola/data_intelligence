﻿--
---------------------------------------------------
-- Set reporting dates
---------------------------------------------------
drop table if exists tmp_rundate;
create temp table tmp_rundate
as
select (TIMESTAMP '2016-12-01 00:00:00.000') as monthend, (TIMESTAMP '2016-09-01 00:00:00.000') as monthstart;
select * from tmp_rundate;
--
-- ***************************************************************************************************************** --
-- drop table if exists tmp_exchrate;
-- create temp table tmp_exchrate
-- as
select
distinct
	sccy.currencycode as ccycode,
--	surate.effectivedate,
	date_trunc('minute',surate.effectivedate) effdate,
	date_part('dow',surate.effectivedate) as day,
	date_trunc('minute',CASE
				WHEN date_part('dow',surate.effectivedate) = 5
				THEN surate.EffectiveDate + interval '3 day'
				ELSE surate.EffectiveDate + interval '1 day' 
	END) as effectiveend,
	surate.value as spotrate
from
	countrymanagement$currency sccy
left outer join
	countrymanagement$HistoricalSpotRate_Currency hsr_succy
on
	sccy.id = hsr_succy.countrymanagement$currencyid
left outer join
	countrymanagement$SpotRate surate
on
	hsr_succy.countrymanagement$spotrateid = surate.id
-- and
-- 	date_trunc('day',coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate)) = date_trunc('day',surate.EffectiveDate) + interval '1 day'
where
	surate.EffectiveDate <= (select max(monthend) from tmp_rundate) 
order by
	sccy.currencycode
--	surate.effectivedate
--

-- select * from tmp_exchrate order by 1, 2