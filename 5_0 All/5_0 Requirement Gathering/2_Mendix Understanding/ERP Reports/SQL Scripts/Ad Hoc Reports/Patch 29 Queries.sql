﻿
select
	batch_id,
	arriveddate,
	confirmeddate,
	stockregistereddate
from
	inventory$warehousestockitembatch whsib
where
--	stockregistereddate is null
--and
	arriveddate is null
or
	confirmeddate is null
-- ((	confirmeddate >= '2016-02-10'
-- and
-- 	confirmeddate < '2016-02-12')
-- or
-- (	confirmeddate >= '2016-07-29'
-- and
-- 	confirmeddate < '2016-07-30'))
order by
	stockregistereddate desc,
	confirmeddate desc,
	arriveddate desc


"2016-07-29 14:14:35"
"2016-02-11 15:18:36.912"