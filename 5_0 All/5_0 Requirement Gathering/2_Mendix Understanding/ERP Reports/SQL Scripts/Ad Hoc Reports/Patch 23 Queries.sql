﻿--
select
	whsib.batch_id,
	wh.name,
	si.sku,
	whsib.stockregistereddate,
	whsib.receivedquantity,
	whsib.currency,
	whsib.productunitcost,
	whsib.currency,
	sh.receiptid,
	sh.ordernumber,
	sh.arriveddate,
	su.currencycode,
	shol.unitcost,
--	count(shol.stockitem)
	shol.stockitem,
	shol.Quantityreceived,
	shol.Quantityrejected,
	shol.Quantityaccepted
from
	inventory$warehousestockitembatch whsib
inner join
	inventory$warehousestockitembatch_shipment whsib_sh
on
	whsib.id = whsib_sh.inventory$warehousestockitembatchid
left outer join
	warehouseshipments$Shipment sh
on
	whsib_sh.warehouseshipments$Shipmentid = sh.id
left outer join
	warehouseshipments$shipmentorderline_shipment as shol_sh
on
	sh.id = shol_sh.warehouseshipments$shipmentid
left outer join
	warehouseshipments$shipmentorderline as shol
on
	shol_sh.warehouseshipments$shipmentorderlineid = shol.id
LEFT OUTER JOIN
	inventory$warehousestockitembatch_warehousestockitem whsib_whsi
on
	whsib.id = whsib_whsi.inventory$warehousestockitembatchid
left outer join
	inventory$warehousestockitem whsi
on
	whsib_whsi.inventory$warehousestockitemid = whsi.id
left outer join
	inventory$warehousestockitem_stockitem whsi_si
on
	whsi.id = whsi_si.inventory$warehousestockitemid
left outer join
	product$stockitem si
on
	whsi_si.product$stockitemid = si.id
left outer join
	warehouseshipments$shipment_supplier sh_su
on
	sh.id = sh_su.warehouseshipments$shipmentid
left outer join
	suppliers$supplier su
on
	sh_su.suppliers$supplierid = su.id
left outer join
	inventory$located_at whsi_wh
on
	whsi.id = whsi_wh.inventory$warehousestockitemid
left outer join
	inventory$warehouse wh
on
	whsi_wh.inventory$warehouseid = wh.id
where
	si.sku = shol.stockitem
and
	whsib.receivedquantity = shol.quantityaccepted
-- and
-- 	whsib.productunitcost = shol.unitcost
and
	whsib.currency != su.currencycode
-- and
-- 	sh.receiptid = 'P00013225-1'
-- and
-- 	si.sku in ('01113B3D4BNC10000B200301','01113B3D4BQC50000AJ00301','02365B1D5BX0000060000301')
-- group by
-- 	whsib.batch_id,
-- 	wh.name,
-- 	si.sku,
-- 	whsib.stockregistereddate,
-- 	whsib.receivedquantity,
-- 	whsib.currency,
-- 	whsib.productunitcost,
-- 	whsib.currency,
-- 	sh.receiptid,
-- 	sh.ordernumber,
-- 	sh.arriveddate,
-- 	shol.unitcost,
-- 	su.currencycode
-- having
-- 	count(shol.stockitem) = 2
order by
	sh.receiptid,
	si.sku,
	whsib.batch_id
--limit 1000




where
whsib_shol.inventory$warehousestockitembatchid is null




where
whsib.batch_id in (412475,412497)


select count (*)
from inventory$warehousestockitembatch whsib
inner join
inventory$warehousestockitembatch_shipmentorderline whsip_shol
on
whsib.id = whsip_shol.inventory$warehousestockitembatchid



select count(*)
from inventory$warehousestockitembatch_shipmentorderline


select count(*)
from inventory$warehousestockitembatch_shipment


select * from
databasepatching$runtimelog
where PATCHNAME = 'Patch023'


--
select
	whsib.batch_id,
	wh.name,
	sh.shipmentid,
	sh.receiptid,
	sh2.shipmentid,
	sh2.receiptid
from
	inventory$warehousestockitembatch whsib
left outer join
	inventory$warehousestockitembatch_shipment whsib_sh
on
	whsib.id = whsib_sh.inventory$warehousestockitembatchid
left outer join
	warehouseshipments$Shipment sh
on
	whsib_sh.warehouseshipments$Shipmentid = sh.id
LEFT OUTER JOIN
	inventory$warehousestockitembatch_shipmentorderline whsib_shol
on
	whsib.id = whsib_shol.inventory$warehousestockitembatchid
left outer join
	warehouseshipments$shipmentorderline shol
on
	whsib_shol.warehouseshipments$shipmentorderlineid = shol.id
left outer join
	warehouseshipments$shipmentorderline_shipment as shol_sh2
on
	shol.id = shol_sh2.warehouseshipments$shipmentorderlineid
left outer join
	warehouseshipments$shipment as sh2
on
	shol_sh2.warehouseshipments$shipmentid = sh2.id
LEFT OUTER JOIN
	inventory$warehousestockitembatch_warehousestockitem whsib_whsi
on
	whsib.id = whsib_whsi.inventory$warehousestockitembatchid
left outer join
	inventory$warehousestockitem whsi
on
	whsib_whsi.inventory$warehousestockitemid = whsi.id
left outer join
	inventory$located_at whsi_wh
on
	whsi.id = whsi_wh.inventory$warehousestockitemid
left outer join
	inventory$warehouse wh
on
	whsi_wh.inventory$warehouseid = wh.id
where not
(
	sh.shipmentid is null
and
	sh2.shipmentid is null
)
and
	sh.shipmentid != sh2.shipmentid