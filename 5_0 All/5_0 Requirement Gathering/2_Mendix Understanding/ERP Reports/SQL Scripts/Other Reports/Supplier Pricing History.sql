﻿--
---------------------------------------------------
-- Supplier Pricing History
---------------------------------------------------
SELECT
	su.suppliername,
	su.currencycode,
	prf.magentoproductid,
	prf.name productfamily,
--	ps.description,
	ps.size packsize,
	supr.leadtime,
	supr.unitprice,
	stpr.effectivedate,
	stpr.expirydate,
	stpr.moq
FROM
	suppliers$supplierprice supr
full outer join
	suppliers$standardprice stpr
on
	supr.id = stpr.id
left outer join
	suppliers$supplierprices_supplier supr_su
on
	supr.id = supr_su.suppliers$supplierpriceid
left outer join
	suppliers$supplier su
on
	supr_su.suppliers$supplierid = su.id
----------------------------------------
-- JOIN to Pack size and Product Family
----------------------------------------
left outer join
	suppliers$SupplierPrices_PackSize supr_ps
on
	supr.id = supr_ps.suppliers$supplierpriceid
left outer join
	product$packsize ps
on
	supr_ps.product$packsizeid = ps.id
left outer join
	product$packsize_productfamily ps_prf
on
	ps.id = ps_prf.product$packsizeid
left outer join
	product$productfamily prf
on
	ps_prf.product$productfamilyid = prf.id
---------------
-- End of JOINs
---------------
order by
	su.suppliername,
	su.currencycode,
	prf.name,
	ps.size,
	stpr.effectivedate	

