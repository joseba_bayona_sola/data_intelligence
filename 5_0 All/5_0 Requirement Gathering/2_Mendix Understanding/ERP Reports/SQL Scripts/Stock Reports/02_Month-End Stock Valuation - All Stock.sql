﻿-- *************************************************************** Stock Valuation - All Stock *************************************************************** --
--
-- SQL Script 'Month-End Stock Reporting - All Stock Extract' must be run first to create the extract table 'a$reporting$monthlystockdetail' used by this report
-- 
-- *************************************************************** Stock Valuation - All Stock *************************************************************** --
--
SELECT
-- distinct
	Warehouse,
	InventoryStatus,
	BatchId,
	OriginalShipmentType,
	ShipmentType,
	DateDelivered,
	DateReceived,
	DateRegistered,
	PONumber,
	SupplierName,
	SupplierOrderRef,
	SupplierCurrency,
	SupplierUnitCost,
	Transfer_PONumber,
	Transfer_SupplierName,
	Transfer_SupplierOrderRef,
	ReceiptId,
	DeliveryNote,
	sku,
	StockItem,
	ProductFamily,
	PackSize,
	Status,
	QuantityOrdered,
	QuantityReceived,
	QuantityAccepted,
	TotalQuantityAdded as QuantityAdded,
	TotalQuantityIssued as QuantityIssued,
   	TotalQuantityDisposed as QuantityDisposed,
	0 as ERPManualAdjustment,
--	StartERPAdded + CurrentERPAdded - StartERPDisposed - CurrentERPDisposed as ERPManualAdjustment,
	StockInTransit,
	CurrentStock,
	Currency,
	UnitCost,
	TotalValue
---------------------------------------------------
-- Additional columns not included in stock report
---------------------------------------------------
--	CheckTotal,
--  	StartSNAPAdded,
--  	StartERPAdded,
--  	StartIssued,
--    	StartSNAPDisposed,
--    	StartERPDisposed,
--  	CurrentSNAPAdded,
--  	CurrentERPAdded,
--  	CurrentIssued,
--    	CurrentSNAPDisposed,
--    	CurrentERPDisposed,
--
   FROM
	a$reporting$monthlystockdetail as stock
where
	InventoryStatus = 'Stock in Warehouse'
and
	Transfer_PONumber is not null
and
	SupplierName = Transfer_SupplierName
-- and
-- (	CurrentStock != CurrentStock2
-- or
-- 	CurrentStock != CheckTotal
-- or
-- 	CurrentStock2 != CheckTotal)
--	CheckTotal != 0
--	ShipmentType not in ('Stock')
--
--order by 4, 3, 1, 2
order by 2, 3, 1, 4, 5
