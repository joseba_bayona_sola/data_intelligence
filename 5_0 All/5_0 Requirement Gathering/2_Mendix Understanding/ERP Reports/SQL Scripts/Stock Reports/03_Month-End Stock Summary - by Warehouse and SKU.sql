﻿-- *************************************************************** Stock Valuation - All Stock *************************************************************** --
--
-- SQL Script 'Month-End Stock Reporting - All Stock Extract' must be run first to create the extract table 'a$reporting$monthlystockdetail' used by this report
-- 
-- *************************************************************** Stock Valuation - All Stock *************************************************************** --
select
	'1' as Sequence,
	'All Warehouses' as warehouse,
	'All Stock' as sku,
	'' as StockItem,
	'' as ProductFamily,
	'' as PackSize,
	sum(CurrentStock)
FROM
	a$reporting$monthlystockdetail as stock
where
	InventoryStatus in ('Stock Received','Stock in Warehouse')
--
UNION ALL
select
	'2' as Sequence,
	warehouse,
	'All Stock' as sku,
	'' as StockItem,
	'' as ProductFamily,
	'' as PackSize,
	sum(CurrentStock)
FROM
	a$reporting$monthlystockdetail as stock
where
	InventoryStatus in ('Stock Received','Stock in Warehouse')
group by
	warehouse
--
UNION ALL
select
	'3' as Sequence,
	warehouse,
	sku,
	StockItem,
	ProductFamily,
	to_char(PackSize,'999') as PackSize,
	sum(CurrentStock)
FROM
	a$reporting$monthlystockdetail as stock
where
	InventoryStatus in ('Stock Received','Stock in Warehouse')
group by
	warehouse,
	sku,
	StockItem,
	ProductFamily,
	PackSize
order by
	1, 2, 3
--
