
/*
tmp_rundate
	(monthend, monthstart)
*/


/*
tmp_exchrate
	(ccycode, effectivedate, effectiveend, spotrate)

FROM
	countrymanagement$currency sccy
		countrymanagement$HistoricalSpotRate_Currency hsr_succy
	countrymanagement$SpotRate surate

WHERE
	surate.EffectiveDate <= (select max(monthend) from tmp_rundate) 
*/


/*
	(Warehouse, 
	ShipmentType, 
	ReceiptId, 
	DateDelivered, DateReceived, DateRegistered, status, 
	PO_Number, Transfer_PO_Number,
	SupplierName, SupplierOrderRef, DeliveryNote, 
	Originating_SupplierName, Originating_SupplierOrderRef, 
	sku, StockItem, ProductFamily, PackSize, 
	POStatus, 
	SupplierCurrencyCode, SupplierUnitCost, SupplierRate, 
	EffDate, 
	WarehouseCurrency, WarehouseUnitCost, 
	QuantityOrdered, SupplierOrderValue, QuantityReceived, QuantityAccepted, 
	TotalReceived, SupplierReceivedValue, WarehouseReceivedValue)

FROM

	warehouseshipments$shipmentorderline as shol
		warehouseshipments$shipmentorderline_shipment as shol_sh
	warehouseshipments$shipment as sh
		warehouseshipments$shipment_warehouse sh_wh
	inventory$warehouse wh

		warehouseshipments$shipmentorderline_purchaseorderline shol_pol
	purchaseorders$purchaseorderline pol
		purchaseorders$purchaseorderline_purchaseorderlineheader as pol_polh
	purchaseorders$purchaseorderlineheader as polh
		purchaseorders$purchaseorderlineheader_purchaseorder as polh_po
	purchaseorders$purchaseorder as po

		purchaseorders$SupplyPO_TransferHeader spo_th
	intersitetransfer$TransferHeader suth
		purchaseorders$TransferPO_TransferHeader txpo_th
	purchaseorders$purchaseorder as txpo

		purchaseorders$TransferPO_TransferHeader tpo_th
	intersitetransfer$TransferHeader txth
		purchaseorders$SupplyPO_TransferHeader supo_th
	purchaseorders$purchaseorder as supo

		purchaseorders$PurchaseOrder_Supplier po_su
	suppliers$supplier txsu

		purchaseorders$PurchaseOrder_Supplier po_su2
	suppliers$supplier susu

		PurchaseOrders$Shortage_PurchaseOrderLine shr_pol
	PurchaseOrders$Shortage shr
		PurchaseOrders$OrderLineShortage_Shortage olsh_shr
	PurchaseOrders$OrderLineShortage olsh
		PurchaseOrders$OrderLineShortage_OrderLine olsh_ol
	OrderProcessing$OrderLine ol
		OrderProcessing$OrderLine_Order ol_or
	OrderProcessing$Order ord

		purchaseorders$PurchaseOrderLine_StockItem pol_si
	product$stockitem si
		product$stockitem_product si_pr
	product$product pr
		product$product_productfamily pr_prf
	product$productfamily prf

		warehouseshipments$shipment_supplier sh_su
	suppliers$supplier su

	tmp_exchrate srates

		inventory$Warehouse_Country wh_ctry
	CountryManagement$Country ctry
		CountryManagement$Country_Currency ctry_ccy
	countrymanagement$currency wccy
 	tmp_exchrate wrates

WHERE 
	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) < (select max(monthend) from tmp_rundate)

	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) >= (select max(monthstart) from tmp_rundate)
*/

