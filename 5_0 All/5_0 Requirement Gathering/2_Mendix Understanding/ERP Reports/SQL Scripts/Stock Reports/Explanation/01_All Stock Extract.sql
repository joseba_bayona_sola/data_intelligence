
/* create table
tmp$rundate
	(monthstart, monthend, cutoff)
*/

/* create table
a$reporting$monthlystockdetail
	(Warehouse, InventoryStatus, BatchId, id, 
	originalshipmenttype, ShipmentType, 
	DateDelivered, DateReceived, DateRegistered, PONumber, 
	SupplierName, SupplierOrderRef, SupplierCurrency, SupplierUnitCost,
	tpo_header, spo_header, 
	ordertype, ordernum, wholesaleordernum, 
	ReceiptId, DeliveryNote, 
	sku, StockItem, ProductFamily, PackSize, 
	Status, 
	QuantityOrdered, QuantityReceived, QuantityAccepted, 
	TotalQuantityAdded, TotalQuantityIssued, TotalQuantityDisposed, 
	StartSNAPAdded, StartERPAdded, StartIssued, StartSNAPDisposed, StartERPDisposed, 
	CurrentSNAPAdded, CurrentERPAdded, CurrentIssued, CurrentSNAPDisposed, CurrentERPDisposed, 
	ERPManualAdjustment,
	StockInTransit, 
	CurrentStock, CheckTotal, 
	Currency, UnitCost, TotalValue)

	InventoryStatus: 
		Stock in Warehouse (22) - (Stock Delivered / Stock Received) (830) - Stock in Transit (1097)
*/


/*
InventoryStatus: Stock in Warehouse (22)

	Warehouse Stock Item Batch (Warehouse Stock Item - Stock Item - Product - Product Family) 
		+ Batch Stock Issue + Batch Stock Movement (Stock Movement) (x8)
		+ Warehouse Shipment (x3) (Supplier) + Purchase Order (Supplier) + Intersite Transfer (Supplier)
		+ Shortage + Order 

FROM
	inventory$warehousestockitembatch
		inventory$warehousestockitembatch_warehousestockitem
	inventory$warehousestockitem
		inventory$located_at
	inventory$warehouse

		inventory$warehousestockitem_stockitem
	product$stockitem

		inventory$warehousestockitembatch_shipment
	warehouseshipments$shipment sh

		inventory$warehousestockitembatch_shipmentorderline
	warehouseshipments$shipmentorderline
		warehouseshipments$shipmentorderline_shipment
	warehouseshipments$shipment sh2

		inventory$warehousestockitembatch_shipmenthistory
	warehouseshipments$shipment shist
		warehouseshipments$shipment_supplier shist_su
	suppliers$supplier shistsu

		warehouseshipments$ShipmentOrderLine_PurchaseOrderLine
	purchaseorders$PurchaseOrderLine
		purchaseorders$PurchaseOrderLine_PurchaseOrderLineHeader
	purchaseorders$PurchaseOrderLineHeader
		purchaseorders$PurchaseOrderLineHeader_PurchaseOrder
	purchaseorders$PurchaseOrder

		purchaseorders$SupplyPO_TransferHeader spo_th
	intersitetransfer$TransferHeader suth 
		purchaseorders$TransferPO_TransferHeader txpo_th
	purchaseorders$purchaseorder as txpo

		purchaseorders$TransferPO_TransferHeader tpo_th
	intersitetransfer$TransferHeader txth
		purchaseorders$SupplyPO_TransferHeader supo_th
	purchaseorders$purchaseorder as supo

		purchaseorders$PurchaseOrder_Supplier po_su
	suppliers$supplier txsu

		purchaseorders$PurchaseOrder_Supplier po_su2
	suppliers$supplier susu

		PurchaseOrders$Shortage_PurchaseOrderLine shr_pol
	PurchaseOrders$Shortage shr
		PurchaseOrders$OrderLineShortage_Shortage olsh_shr
	PurchaseOrders$OrderLineShortage olsh
		PurchaseOrders$OrderLineShortage_OrderLine olsh_ol
	OrderProcessing$OrderLine ol
		OrderProcessing$OrderLine_Order ol_or
	OrderProcessing$Order ord

		warehouseshipments$shipment_supplier sh_su
	suppliers$supplier su

		product$stockitem_product si_pr
	product$product pr
		product$product_productfamily pr_prf
	product$productfamily prf

	--> qty issued 
	--> qty issued in current month
		inventory$batchstockissue_warehousestockitembatch bsi_whsib
	inventory$batchstockissue bsi

	--> SNAP disposed, ERP disposed, SNAP added, ERP added
	--> SNAP disposed, ERP disposed, SNAP added, ERP added (CURRENT MONTH)
		inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	inventory$batchstockmovement bsm
		inventory$batchstockmovement_stockmovement bsm_sm
	inventory$stockmovement sm

	--> qty disposed,  qty added
		inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	inventory$batchstockmovement bsm
		inventory$batchstockmovement_stockmovement bsm_sm
	inventory$stockmovement sm

	a$whsib_UnitCost

WHERE
	whsib.ArrivedDate <= (select max(monthend) from tmp$rundate)
	whsib.StockRegisteredDate < (select max(cutoff) from tmp$rundate)
	wh.name || si.sku not in XXXX (total stock is zero or negative)
	si.sku not in (SKUs for glasses package products) 
	coalesce(whsib.ReceivedQuantity,0) + coalesce(added.QtyAdded,0) - coalesce(disposed.QtyDisposed,0) - coalesce(issued.QtyIssued,0) != 0
*/


/*
InventoryStatus: (Stock Delivered / Stock Received) (830)

FROM
	warehouseshipments$shipmentorderline as shol
		warehouseshipments$shipmentorderline_shipment as shol_sh
	warehouseshipments$shipment as sh
		warehouseshipments$shipment_warehouse sh_wh
	inventory$warehouse wh

		warehouseshipments$shipmentorderline_purchaseorderline shol_pol
	purchaseorders$purchaseorderline pol
		purchaseorders$purchaseorderline_purchaseorderlineheader as pol_polh
	purchaseorders$purchaseorderlineheader as polh
		purchaseorders$purchaseorderlineheader_purchaseorder as polh_po
	purchaseorders$purchaseorder as po

		purchaseorders$SupplyPO_TransferHeader spo_th
	intersitetransfer$TransferHeader suth
		purchaseorders$TransferPO_TransferHeader tpo_th
	intersitetransfer$TransferHeader txth

		PurchaseOrders$Shortage_PurchaseOrderLine shr_pol
	PurchaseOrders$Shortage shr
		PurchaseOrders$OrderLineShortage_Shortage olsh_shr
	PurchaseOrders$OrderLineShortage olsh
		PurchaseOrders$OrderLineShortage_OrderLine olsh_ol
	OrderProcessing$OrderLine ol
		OrderProcessing$OrderLine_Order ol_or
	OrderProcessing$Order ord

		purchaseorders$PurchaseOrderLine_StockItem pol_si
	product$stockitem si
		product$stockitem_product si_pr
	product$product pr
		product$product_productfamily pr_prf
	product$productfamily prf

		warehouseshipments$shipment_supplier sh_su
	suppliers$supplier su

	countrymanagement$currency ccy

	countrymanagement$currency wccy

WHERE
	sh.stockregistereddate is null or sh.stockregistereddate >= (select max(cutoff) from tmp$rundate)
	sh.status != 'Cancelled'
*/


/*
InventoryStatus: Stock in Transit (1097)

	intersitetransfer$intransitstockitembatch itsib
		intersitetransfer$intransitstockitembatch_transferheader itsib_th
	intersitetransfer$transferheader th

		purchaseorders$SupplyPO_TransferHeader spo_th
	purchaseorders$purchaseorder spo
		purchaseorders$PurchaseOrder_Supplier spo_su
	Suppliers$Supplier ssu

		purchaseorders$TransferPO_TransferHeader tpo_th
	purchaseorders$purchaseorder tpo
		purchaseorders$PurchaseOrder_Supplier tpo_su
	Suppliers$Supplier tsu

		purchaseorders$PurchaseOrder_Warehouse tpo_wh
	inventory$warehouse wh

		intersitetransfer$intransitstockitembatch_intransitbatchheader itsib_itbh
	intersitetransfer$intransitbatchheader itbh

		intersitetransfer$intransitstockitembatch_stockitem itsib_si
	product$stockitem si

		product$stockitem_product si_pr
	product$product pr
		product$product_productfamily pr_prf
	product$productfamily prf

		intersitetransfer$intransitbatchheader_customershipment itbh_csh
	customershipments$customershipment csh

		customershipments$customershipment_progress csh_lu
	customershipments$lifecycle_update lu


	--> include batches with remaining qty > 0 and which have not yet been received
		purchaseorders$TransferPO_TransferHeader tpo_th
	purchaseorders$purchaseorder tpo
		purchaseorders$purchaseorderLineheader_purchaseorder polh_po
	purchaseorders$purchaseorderlineheader polh
		purchaseorders$purchaseorderLine_purchaseorderlineheader pol_polh
	purchaseorders$purchaseorderline pol
		warehouseshipments$shipmentorderline_purchaseorderline shol_pol
	warehouseshipments$shipmentorderline as shol

WHERE
	csh.intersite = true
	csh.status = 'Complete'
	csh_complete.timestamp < (select max(monthend) from tmp$rundate)
	itsib.remainingquantity != 0
*/
