
/*
tmp_rundate
	(monthend, monthstart)
*/


/*
tmp_invoice_product_price
	(invoicenumber, name, packsize, productunitcost, qtyissued, totalcost)

FROM
	customershipments$customershipment csh

	customerinvoicing$customershipmentinvoice csi

		customerinvoicing$customershipmentinvoice_customerinvoice csi_ci
	customerinvoicing$customerinvoice ci

		customershipments$picklist_customershipment pl_csh
	customershipments$pickline pl

		customershipments$PickLine_OrderLineStockAllocationTransaction pl_olsat
	stockallocation$OrderLineStockAllocationTransaction olsat

		inventory$BatchTransaction_OrderLineStockAllocationTransaction bt_olsat
	inventory$BatchStockAllocation bsa

		inventory$BatchTransaction_WarehouseStockItemBatch bt_whsib
	inventory$WarehouseStockItemBatch whsib

		inventory$warehousestockitembatch_warehousestockitem whsib_whsi
	inventory$warehousestockitem whsi
		inventory$warehousestockitem_stockitem whsi_si
	product$stockitem si

		product$stockitem_product si_pr
	product$product pr
		product$product_productfamily pr_prf
	product$productfamily prf

WHERE
	csh.shipmentnumber like 'WS%'
	bsa.cancelled = false
*/

/*
tmp_invoice_product
	(invoicenumber, name, packsize, avecost)

FROM
	tmp_invoice_product_price
*/


/*
tmp_invoice_product
	(invoicenumber, invoicedate, status, 
	customer, warehouse, 
	product, packsize, Quantity, Lines,
	AveCost, Currency, NetPrice, VATRate, VATAmount, GrossAmount)

FROM
	customerinvoicing$customerinvoiceline cil

		customerinvoicing$customerinvoiceline_customershipmentinvoice cil_csi
	customerinvoicing$customershipmentinvoice csi

		customerinvoicing$customershipmentinvoice_customerinvoice csi_ci
	customerinvoicing$customerinvoice ci

		customerinvoicing$customershipmentinvoice_customerentity csi_ce
	customer$customerentity ce
	customer$wholesalecustomer wsc

	customershipments$customershipment csh

	product$stockitem si

	product$stockitem_product si_pr
	product$product pr
	product$product_productfamily pr_prf
	product$productfamily prf

	tmp_invoice_product tmp_pr
WHERE
	ci.invoicenumber is not null

	ci.invoicedate >= (select max(startdate) from tmp_rundate) / ci.invoicedate < (select max(enddate) from tmp_rundate)


FROM
	customerinvoicing$customerinvoice ci

		customerinvoicing$customerinvoice_customerinvoicecarriage ci_cic
	customerinvoicing$customerinvoiceline cil

		customerinvoicing$customerinvoice_customerentity ci_ce
	customer$customerentity ce
	customer$wholesalecustomer wsc

	customerinvoicing$customerinvoice ci
		customerinvoicing$CustomerShipmentInvoice_CustomerInvoice csi_ci
	customerinvoicing$customershipmentinvoice csi

WHERE
	ci.invoicenumber is not null

	ci.invoicedate >= (select max(startdate) from tmp_rundate) / ci.invoicedate < (select max(enddate) from tmp_rundate)

*/
