﻿--
---------------------------------------------------
-- Set reporting dates
---------------------------------------------------
drop table if exists tmp_rundate;
create temp table tmp_rundate
as
select (TIMESTAMP '2016-12-01 00:00:00.000') as monthend, (TIMESTAMP '2016-09-01 00:00:00.000') as monthstart;
select * from tmp_rundate;
--
---------------------------------------------------
-- Create temp table for exchange rates
---------------------------------------------------
drop table if exists tmp_exchrate;
create temp table tmp_exchrate
as
select
distinct
	sccy.currencycode as ccycode,
--	surate.effectivedate,
	date_trunc('minute',surate.effectivedate) effectivedate,
	date_trunc('minute',
		CASE
			WHEN date_part('dow',surate.effectivedate) = 5
			THEN surate.EffectiveDate + interval '3 day'
			ELSE surate.EffectiveDate + interval '1 day' 
		END) as effectiveend,
	surate.value as spotrate
from
	countrymanagement$currency sccy
left outer join
	countrymanagement$HistoricalSpotRate_Currency hsr_succy
on
	sccy.id = hsr_succy.countrymanagement$currencyid
left outer join
	countrymanagement$SpotRate surate
on
	hsr_succy.countrymanagement$spotrateid = surate.id
-- and
-- 	date_trunc('day',coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate)) = date_trunc('day',surate.EffectiveDate) + interval '1 day'
where
	surate.EffectiveDate <= (select max(monthend) from tmp_rundate) 
order by
	sccy.currencycode,
	date_trunc('minute',surate.effectivedate);
select * from tmp_exchrate;
--
-- ***************************************************************************************************************** --
-- *************************** Start 'Stock Delivered' and 'Stock Received' **************************************** --
-- ***************************************************************************************************************** --
-- select
-- 	shol_id,
-- 	count(receiptid)
-- from
-- (
SELECT
	wh.name Warehouse,
--	shol.id sholid,
-- 	sh.ShipmentType as OriginalShipmentType,
	CASE
		WHEN	supo.ordernumber is not null			-- It's an intersite transfer and this is the receipt for the Transfer PO.
		THEN	'Intersite'
		WHEN	shortages.ShortageType = 'Wholesale_order'	-- It's an intersite transfer and this is the receipt for the Supply PO. It's also linked to a wholesale shortage.
		AND 	txpo.ordernumber is not null
		THEN	'Wholesale (for Intersite)'		
		WHEN	shortages.ShortageType = 'Wholesale_order'	-- It's linked to a wholesale shortage. It's not an intersite transfer.
		AND 	(supo.ordernumber is null and txpo.ordernumber is null)
		THEN	'Wholesale'
		WHEN	shortages.ShortageType is not null		-- It's an intersite transfer and this is the receipt for the Supply PO. It's also linked to a retail shortage.
		AND 	txpo.ordernumber is not null
		THEN	'Back_To_Back (for Intersite)'
		WHEN	shortages.ShortageType is not null		-- It's linked to a retail shortage. It's not an intersite transfer.
		AND 	(supo.ordernumber is null and txpo.ordernumber is null)
		THEN	'Back_To_Back'
		WHEN	txpo.ordernumber is not null			-- It's an intersite transfer and this is the receipt for the Supply PO.
		THEN	'Stock (for Intersite)'	
 		WHEN	sh.ShipmentType	= 'Back_To_Back'		-- The original shipment type is 'Back_To_Back' but the shortage records no longer exist.
 		THEN 	sh.ShipmentType
		ELSE	'Stock'						-- If none of the above applies then default to 'Stock'.
	END as ShipmentType,
-- 	shortages.ShortageType,
	sh.receiptid ReceiptId,
	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) as DateDelivered,
	coalesce(sh.confirmeddate,sh.stockregistereddate) as DateReceived,
	sh.stockregistereddate DateRegistered,
	sh.status,
	po.ordernumber PO_Number,
	su.suppliername SupplierName,
	coalesce(po.supplierreference,txpo.supplierreference) SupplierOrderRef,
	sh.supplieradviceref DeliveryNote,
	coalesce(supo.ordernumber,txpo.ordernumber) Transfer_PO_Number, 
	susu.suppliername Originating_SupplierName,
	supo.supplierreference Originating_SupplierOrderRef,	
	si.sku,
	si.StockItemDescription as StockItem,
	prf.name as ProductFamily,
	si.packsize PackSize,
	shol.status POStatus,
	su.currencycode as SupplierCurrencyCode,
	shol.unitcost as SupplierUnitCost,
	srates.spotrate as SupplierRate,
	srates.effectivedate as EffDate,
	wccy.currencycode as WarehouseCurrency,
	shol.unitcost * (wrates.spotrate/srates.spotrate) as WarehouseUnitCost,
-- 	wrates.spotrate as WarehouseRate,
-- 	wrates.effectivedate as EffDate,
	shol.Quantityordered QuantityOrdered,
	shol.Quantityordered * shol.unitcost as SupplierOrderValue,
	shol.Quantityreceived QuantityReceived,
	shol.Quantityaccepted QuantityAccepted,
	CASE
		WHEN (shol.Quantityaccepted is not null) and (shol.Quantityaccepted !=0)
		THEN shol.Quantityaccepted
		WHEN (shol.Quantityreceived is not null) and (shol.Quantityreceived !=0)
		THEN shol.Quantityreceived
		ELSE shol.Quantityordered
	END as TotalReceived,
	CASE
		WHEN (shol.Quantityaccepted is not null) and (shol.Quantityaccepted !=0)
		THEN shol.Quantityaccepted
		WHEN (shol.Quantityreceived is not null) and (shol.Quantityreceived !=0)
		THEN shol.Quantityreceived
		ELSE shol.Quantityordered
	END * shol.unitcost as SupplierReceivedValue,
	CASE
		WHEN (shol.Quantityaccepted is not null) and (shol.Quantityaccepted !=0)
		THEN shol.Quantityaccepted
		WHEN (shol.Quantityreceived is not null) and (shol.Quantityreceived !=0)
		THEN shol.Quantityreceived
		ELSE shol.Quantityordered
	END * (shol.unitcost * (wrates.spotrate/srates.spotrate)) as WarehouseReceivedValue
FROM
	warehouseshipments$shipmentorderline as shol
left outer join
	warehouseshipments$shipmentorderline_shipment as shol_sh
on
	shol.id = shol_sh.warehouseshipments$shipmentorderlineid
left outer join
	warehouseshipments$shipment as sh
on
	shol_sh.warehouseshipments$shipmentid = sh.id
left outer join
	warehouseshipments$shipment_warehouse sh_wh
on
	sh.id = sh_wh.warehouseshipments$shipmentid
left outer join
	inventory$warehouse wh
on
	sh_wh.inventory$warehouseid = wh.id
-------------------------
-- JOIN to Purchase Order
-------------------------
left outer join
	warehouseshipments$shipmentorderline_purchaseorderline shol_pol
on
	shol.id = shol_pol.warehouseshipments$shipmentorderlineid
left outer join
	purchaseorders$purchaseorderline pol
on
	shol_pol.purchaseorders$purchaseorderlineid = pol.id
left outer join
	purchaseorders$purchaseorderline_purchaseorderlineheader as pol_polh
on
	pol.id = pol_polh.purchaseorders$purchaseorderlineid
left outer join
	purchaseorders$purchaseorderlineheader as polh
on
	pol_polh.purchaseorders$purchaseorderlineheaderid = polh.id
left outer join
	purchaseorders$purchaseorderlineheader_purchaseorder as polh_po
on
	polh.id = polh_po.purchaseorders$purchaseorderlineheaderid
left outer join
	purchaseorders$purchaseorder as po
on
	polh_po.purchaseorders$purchaseorderid = po.id
---------------------------------------
-- JOIN to Transfer Header for Supplier
---------------------------------------
left outer join
	purchaseorders$SupplyPO_TransferHeader spo_th
on
	po.id = spo_th.purchaseorders$PurchaseOrderid
left outer join
	intersitetransfer$TransferHeader suth
on
	spo_th.intersitetransfer$transferheaderid = suth.id
left outer join
	purchaseorders$TransferPO_TransferHeader txpo_th
on
	suth.id = txpo_th.intersitetransfer$transferheaderid
left outer join
	purchaseorders$purchaseorder as txpo
on
	txpo_th.purchaseorders$PurchaseOrderid = txpo.id
----------------------------------------
-- JOIN to Transfer Header for Warehouse
----------------------------------------
left outer join
	purchaseorders$TransferPO_TransferHeader tpo_th
on
	po.id = tpo_th.purchaseorders$PurchaseOrderid
left outer join
	intersitetransfer$TransferHeader txth
on
	tpo_th.intersitetransfer$transferheaderid = txth.id
left outer join
	purchaseorders$SupplyPO_TransferHeader supo_th
on
	txth.id = supo_th.intersitetransfer$transferheaderid
left outer join
	purchaseorders$purchaseorder as supo
on
	supo_th.purchaseorders$PurchaseOrderid = supo.id
----------------------------
-- JOIN to Transfer Supplier
----------------------------
left outer join
	purchaseorders$PurchaseOrder_Supplier po_su
on
	txpo.id = po_su.purchaseorders$PurchaseOrderid
left outer join
	suppliers$supplier txsu
on
	po_su.suppliers$supplierid = txsu.id
----------------------------
-- JOIN to Supply Supplier
----------------------------
left outer join
	purchaseorders$PurchaseOrder_Supplier po_su2
on
	supo.id = po_su2.purchaseorders$PurchaseOrderid
left outer join
	suppliers$supplier susu
on
	po_su2.suppliers$supplierid = susu.id
----------------------------------------
-- JOIN to Customer Order for Shortages
----------------------------------------
-- left outer join
-- 	PurchaseOrders$Shortage_PurchaseOrderLine shr_pol
-- on
-- 	pol.id = shr_pol.PurchaseOrders$PurchaseOrderLineid
-- left outer join
-- 	PurchaseOrders$Shortage shr
-- on
-- 	shr_pol.PurchaseOrders$Shortageid = shr.id
-- left outer join
-- 	PurchaseOrders$OrderLineShortage_Shortage olsh_shr
-- on
-- 	shr.id = olsh_shr.PurchaseOrders$Shortageid
-- left outer join
-- 	PurchaseOrders$OrderLineShortage olsh
-- on
-- 	olsh_shr.PurchaseOrders$OrderLineShortageid = olsh.id
-- left outer join
-- 	PurchaseOrders$OrderLineShortage_OrderLine olsh_ol
-- on
-- 	olsh.id = olsh_ol.PurchaseOrders$OrderLineShortageid
-- left outer join
-- 	OrderProcessing$OrderLine ol
-- on
-- 	olsh_ol.OrderProcessing$OrderLineid = ol.id
-- left outer join
-- 	OrderProcessing$OrderLine_Order ol_or
-- on
-- 	ol.id = ol_or.OrderProcessing$OrderLineid
-- left outer join
-- 	OrderProcessing$Order ord
-- on
-- 	ol_or.OrderProcessing$Orderid = ord.id
left outer join
(
	select
		shr_pol.PurchaseOrders$PurchaseOrderLineid polid,
		max(ord._type) ShortageType,
		max(ord.MagentoOrderID) MagentoOrderID
	from
		PurchaseOrders$Shortage_PurchaseOrderLine shr_pol
	left outer join
		PurchaseOrders$Shortage shr
	on
		shr_pol.PurchaseOrders$Shortageid = shr.id
	left outer join
		PurchaseOrders$OrderLineShortage_Shortage olsh_shr
	on
		shr.id = olsh_shr.PurchaseOrders$Shortageid
	left outer join
		PurchaseOrders$OrderLineShortage olsh
	on
		olsh_shr.PurchaseOrders$OrderLineShortageid = olsh.id
	left outer join
		PurchaseOrders$OrderLineShortage_OrderLine olsh_ol
	on
		olsh.id = olsh_ol.PurchaseOrders$OrderLineShortageid
	left outer join
		OrderProcessing$OrderLine ol
	on
		olsh_ol.OrderProcessing$OrderLineid = ol.id
	left outer join
		OrderProcessing$OrderLine_Order ol_or
	on
		ol.id = ol_or.OrderProcessing$OrderLineid
	left outer join
		OrderProcessing$Order ord
	on
		ol_or.OrderProcessing$Orderid = ord.id
	group by
		shr_pol.PurchaseOrders$PurchaseOrderLineid
) shortages
on
	pol.id = shortages.polid
-------------------------
-- JOIN to Product Family
-------------------------
left outer join
	purchaseorders$PurchaseOrderLine_StockItem pol_si
on
	pol.id = pol_si.purchaseorders$PurchaseOrderLineid
left outer join
	product$stockitem si
on
	pol_si.product$stockitemid = si.id
left outer join
	product$stockitem_product si_pr
on
	si.id = si_pr.product$stockitemid
left outer join
	product$product pr
on
	si_pr.product$productid = pr.id
left outer join
	product$product_productfamily pr_prf
on
	pr.id = pr_prf.product$productid
left outer join
	product$productfamily prf
on
	pr_prf.product$productfamilyid = prf.id
---------------------
-- JOIN to Supplier
-------------------
left outer join
	warehouseshipments$shipment_supplier sh_su
on
	sh.id = sh_su.warehouseshipments$shipmentid
left outer join
	suppliers$supplier su
on
	sh_su.suppliers$supplierid = su.id
----------------------------
-- JOIN to supplier Currency
----------------------------
left outer join
 	tmp_exchrate srates
on
	su.currencycode = srates.ccycode
and
	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) >= srates.EffectiveDate
and
	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) < srates.EffectiveEnd
-----------------------------
-- JOIN to warehouse Currency
-----------------------------
left outer join
	inventory$Warehouse_Country wh_ctry
on
	wh.id = wh_ctry.inventory$Warehouseid
left outer join
	CountryManagement$Country ctry
on
	wh_ctry.CountryManagement$Countryid = ctry.id
left outer join
	CountryManagement$Country_Currency ctry_ccy
on
	ctry.id = ctry_ccy.CountryManagement$Countryid
left outer join
	countrymanagement$currency wccy
on
	ctry_ccy.countrymanagement$currencyid = wccy.id
left outer join
 	tmp_exchrate wrates
on
	wccy.currencycode = wrates.ccycode
and
	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) >= wrates.EffectiveDate
and
	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) < wrates.EffectiveEnd
---------------
-- End of JOINs
---------------
where
	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) < (select max(monthend) from tmp_rundate)
and
	coalesce(sh.arriveddate,sh.confirmeddate,sh.stockregistereddate) >= (select max(monthstart) from tmp_rundate)
and
	sh.status != 'Cancelled'
-- and
-- 	po.ordernumber in ('P00013838','P00013836')
-- 	po.ordernumber in ('P00016757')
-- and
-- 	ord._type is null
-- and 
-- 	(supo.ordernumber is null and txpo.ordernumber is null)
-- and
-- 	coalesce(txth.transfernum,suth.transfernum) is null
-- and
-- 	sh.status = 'Stock_Registered'
-- and
-- 	shol.id = 74590868828755034
-- and
-- 	wccy.currencycode != su.currencycode
order by 1,2,4,5,7,20
-- 	coalesce(txth.transfernum,suth.transfernum),
-- 	sh.receiptid,
-- 	po.ordernumber,
-- 	sh.stockregistereddate desc,
-- 	sh.confirmeddate desc, 
-- 	sh.arriveddate desc
-- limit 100
-- ) receipts
-- group by
-- 	shol_id
-- having
-- 	count(receiptid) > 1
