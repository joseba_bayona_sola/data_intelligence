﻿--
---------------------------------------------------
-- Set reporting dates
---------------------------------------------------
drop table if exists tmp$rundate;
create temp table tmp$rundate
as
select
(TIMESTAMP '2016-11-01 00:00:00.000') as monthstart,
(TIMESTAMP '2016-12-01 00:00:00.000') as monthend,
(TIMESTAMP '2016-12-01 00:00:00.000') as cutoff;
select * from tmp$rundate;
--
drop table if exists a$reporting$monthlystockdetail;
create table a$reporting$monthlystockdetail
as
-- ***************************************************************************************************************** --
-- *************************** Start 'Stock in Warehouse' query **************************************************** --
-- ***************************************************************************************************************** --
SELECT
	wh.Name as Warehouse,
	'Stock in Warehouse' as InventoryStatus,
	whsib.Batch_ID as BatchId,
	whsib.id as id,
	coalesce(sh2.ShipmentType,sh.ShipmentType) as originalshipmenttype,
	CASE
		WHEN	supo.ordernumber is not null
		THEN	'Intersite'
		WHEN	ord._type = 'Wholesale_order'
		AND 	txpo.ordernumber is not null
		THEN	'Wholesale (for Intersite)'		
		WHEN	ord._type = 'Wholesale_order'
		AND 	(supo.ordernumber is null and txpo.ordernumber is null)
		THEN	'Wholesale'
		WHEN	ord._type is not null
		AND 	txpo.ordernumber is not null
		THEN	'Back_To_Back (for Intersite)'
		WHEN	ord._type is not null
		AND 	(txpo.ordernumber is null and txpo.ordernumber is null)
		THEN	'Back_To_Back'
		WHEN	txpo.ordernumber is not null
		THEN	'Stock (for Intersite)'	
		ELSE	'Stock'
-- 		ELSE	sh.ShipmentType
	END as ShipmentType,
	whsib.ArrivedDate as DateDelivered,
	whsib.ConfirmedDate as DateReceived,
	whsib.StockRegisteredDate as DateRegistered,
	CASE
		WHEN	sh2.ShipmentType = 'Intersite'
		THEN	shist.ordernumber
		ELSE	coalesce(po.OrderNumber, sh.OrderNumber)
	END as PONumber,
	CASE
		WHEN	sh2.ShipmentType = 'Intersite'
		THEN	shist.suppliername
		ELSE	su.suppliername 
	END as SupplierName,
	CASE
		WHEN	sh2.ShipmentType = 'Intersite'
		THEN	null
		ELSE	po.supplierreference 
	END as SupplierOrderRef,
	su.currencycode SupplierCurrency,
	shol.UnitCost SupplierUnitCost,
	CASE
		WHEN sh2.ShipmentType = 'Intersite'
		THEN coalesce(po.OrderNumber, sh.OrderNumber)
		ELSE null
	END as Transfer_PONumber,
	CASE
		WHEN sh2.ShipmentType = 'Intersite'
		THEN su.suppliername
		ELSE null
	END as Transfer_SupplierName,
	CASE
		WHEN sh2.ShipmentType = 'Intersite'
		THEN po.supplierreference
		ELSE null
	END as Transfer_SupplierOrderRef,
	txth.transfernum tpo_header,
	suth.transfernum spo_header,
	ord._type ordertype,
	ord.magentoorderid ordernum,
	ord.incrementid wholesaleordernum,
	coalesce(sh2.receiptid,sh.receiptid) ReceiptId,
	coalesce(sh2.supplieradviceref,sh.supplieradviceref) DeliveryNote,
	si.sku,
	si.StockItemDescription as StockItem,
	prf.name as ProductFamily,
	si.packsize PackSize,
	null as Status,
	null as QuantityOrdered,
	coalesce(whsib.ReceivedQuantity,0) as QuantityReceived,
	null as QuantityAccepted,
	coalesce(added.QtyAdded,0) TotalQuantityAdded,
   	coalesce(issued.QtyIssued,0) TotalQuantityIssued,
 	coalesce(disposed.QtyDisposed,0) TotalQuantityDisposed,
  	coalesce(start_added.QtyAdded,0) StartSNAPAdded,
  	coalesce(start_erpadded.QtyAdded,0) StartERPAdded,
   	coalesce(start_issued.QtyIssued,0) StartIssued,
 	coalesce(start_disposed.QtyDisposed,0) StartSNAPDisposed,
 	coalesce(start_erpdisposed.QtyDisposed,0) StartERPDisposed,
   	coalesce(current_added.QtyAdded,0) CurrentSNAPAdded,
   	coalesce(current_erpadded.QtyAdded,0) CurrentERPAdded,
   	coalesce(current_issued.QtyIssued,0) CurrentIssued,
 	coalesce(current_disposed.QtyDisposed,0) CurrentSNAPDisposed,
 	coalesce(current_erpdisposed.QtyDisposed,0) CurrentERPDisposed,
	0 as ERPManualAdjustment,
	0 as StockInTransit,
-- 	coalesce(whsib.ReceivedQuantity,0)
-- 			+ coalesce(start_added.QtyAdded,0) + coalesce(start_erpadded.QtyAdded,0) + coalesce(current_added.QtyAdded,0) + coalesce(current_erpadded.QtyAdded,0)
-- 			- coalesce(start_disposed.QtyDisposed,0) - coalesce(start_erpdisposed.QtyDisposed,0) - coalesce(current_disposed.QtyDisposed,0) - coalesce(current_erpdisposed.QtyDisposed,0)
-- 			- coalesce(start_issued.QtyIssued,0) - coalesce(current_issued.QtyIssued,0) as CurrentStock,
	coalesce(whsib.ReceivedQuantity,0) + coalesce(added.QtyAdded,0) - coalesce(disposed.QtyDisposed,0) - coalesce(issued.QtyIssued,0) as CurrentStock,
	coalesce(whsib.ReceivedQuantity,0) - coalesce(whsib.DisposedQuantity,0) -coalesce(whsib.IssuedQuantity,0) as CheckTotal,
	CASE
		WHEN wh.name like 'Horizon%' and whsib.currency is null
		THEN 'GBP'
		WHEN wh.name = 'York' and whsib.currency is null
		THEN 'GBP'
		WHEN wh.name = 'Amsterdam' and whsib.currency is null
		THEN 'EUR'
		ELSE whsib.currency
	END as Currency,
	coalesce(uc.TotalUnitCost,whsib.ProductUnitCost,0) as UnitCost,
	coalesce(uc.TotalUnitCost,whsib.ProductUnitCost,0)  *
		(coalesce(whsib.ReceivedQuantity,0) + coalesce(added.QtyAdded,0) - coalesce(disposed.QtyDisposed,0) - coalesce(issued.QtyIssued,0)) as TotalValue
FROM
	inventory$warehousestockitembatch as whsib
LEFT OUTER JOIN
	inventory$warehousestockitembatch_warehousestockitem whsib_whsi
on
	whsib.id = whsib_whsi.inventory$warehousestockitembatchid
left outer join
	inventory$warehousestockitem whsi
on
	whsib_whsi.inventory$warehousestockitemid = whsi.id
left outer join
	inventory$located_at whsi_wh
on
	whsi.id = whsi_wh.inventory$warehousestockitemid
left outer join
	inventory$warehouse wh
on
	whsi_wh.inventory$warehouseid = wh.id
---------------------
-- JOIN to Stock Item
---------------------
left outer join
	inventory$warehousestockitem_stockitem whsi_si
on
	whsi.id = whsi_si.inventory$warehousestockitemid
left outer join
	product$stockitem si
on
	whsi_si.product$stockitemid = si.id
---------------------
-- JOIN to Shipment
---------------------
LEFT OUTER JOIN
	inventory$warehousestockitembatch_shipment whsib_sh
on
	whsib.id = whsib_sh.inventory$warehousestockitembatchid
left outer join
	warehouseshipments$shipment sh
on
	whsib_sh.warehouseshipments$shipmentid = sh.id
-----------------------------------------
-- JOIN to ShipmentOrderLine and Shipment
-----------------------------------------
LEFT OUTER JOIN
	inventory$warehousestockitembatch_shipmentorderline whsib_shol
on
	whsib.id = whsib_shol.inventory$warehousestockitembatchid
left outer join
	warehouseshipments$shipmentorderline shol
on
	whsib_shol.warehouseshipments$shipmentorderlineid = shol.id
left outer join
	warehouseshipments$shipmentorderline_shipment as shol_sh2
on
	shol.id = shol_sh2.warehouseshipments$shipmentorderlineid
left outer join
	warehouseshipments$shipment as sh2
on
	shol_sh2.warehouseshipments$shipmentid = sh2.id
----------------------------
-- JOIN to Shipment History
----------------------------
LEFT OUTER JOIN
(
	select  whsib_shist.inventory$warehousestockitembatchid, whsib_shist.warehouseshipments$shipmentid, shist.id, shist.receiptid, shist.ordernumber, shist.ShipmentType,
	shistsu.suppliername
	from inventory$warehousestockitembatch_shipmenthistory whsib_shist
	left outer join warehouseshipments$shipment shist
	on whsib_shist.warehouseshipments$shipmentid = shist.id
	left outer join warehouseshipments$shipment_supplier shist_su
	on shist.id = shist_su.warehouseshipments$shipmentid
	left outer join suppliers$supplier shistsu
	on shist_su.suppliers$supplierid = shistsu.id
	where shist.ShipmentType = 'Back_To_Back' 
) shist
on
	whsib.id = shist.inventory$warehousestockitembatchid
-------------------------------------------
-- JOIN to Purchase Order
-------------------------------------------
left outer join
	warehouseshipments$ShipmentOrderLine_PurchaseOrderLine shol_pol
on
	shol.id = shol_pol.warehouseshipments$ShipmentOrderLineid
left outer join
	purchaseorders$PurchaseOrderLine pol
on
	shol_pol.purchaseorders$PurchaseOrderLineid = pol.id
left outer join
	purchaseorders$PurchaseOrderLine_PurchaseOrderLineHeader pol_polh
on
	pol.id = pol_polh.purchaseorders$PurchaseOrderLineid
left outer join
	purchaseorders$PurchaseOrderLineHeader polh
on
	pol_polh.purchaseorders$PurchaseOrderLineHeaderid = polh.id 
left outer join
	purchaseorders$PurchaseOrderLineHeader_PurchaseOrder polh_po
on
	polh.id = polh_po.purchaseorders$PurchaseOrderLineHeaderid
left outer join
	purchaseorders$PurchaseOrder po
on
	polh_po.purchaseorders$PurchaseOrderid = po.id
-- ---------------------------
-- -- JOIN to Transfer Header
-- ---------------------------
-- left outer join
-- 	purchaseorders$SupplyPO_TransferHeader spo_th
-- on
-- 	po.id = spo_th.purchaseorders$PurchaseOrderid
-- left outer join
-- 	intersitetransfer$TransferHeader suth
-- on
-- 	spo_th.intersitetransfer$transferheaderid = suth.id
-- left outer join
-- 	purchaseorders$TransferPO_TransferHeader tpo_th
-- on
-- 	po.id = tpo_th.purchaseorders$PurchaseOrderid
-- left outer join
-- 	intersitetransfer$TransferHeader txth
-- on
-- 	tpo_th.intersitetransfer$transferheaderid = txth.id
---------------------------------------
-- JOIN to Transfer Header for Supplier
---------------------------------------
left outer join
	purchaseorders$SupplyPO_TransferHeader spo_th
on
	po.id = spo_th.purchaseorders$PurchaseOrderid
left outer join
	intersitetransfer$TransferHeader suth
on
	spo_th.intersitetransfer$transferheaderid = suth.id
left outer join
	purchaseorders$TransferPO_TransferHeader txpo_th
on
	suth.id = txpo_th.intersitetransfer$transferheaderid
left outer join
	purchaseorders$purchaseorder as txpo
on
	txpo_th.purchaseorders$PurchaseOrderid = txpo.id
----------------------------------------
-- JOIN to Transfer Header for Warehouse
----------------------------------------
left outer join
	purchaseorders$TransferPO_TransferHeader tpo_th
on
	po.id = tpo_th.purchaseorders$PurchaseOrderid
left outer join
	intersitetransfer$TransferHeader txth
on
	tpo_th.intersitetransfer$transferheaderid = txth.id
left outer join
	purchaseorders$SupplyPO_TransferHeader supo_th
on
	txth.id = supo_th.intersitetransfer$transferheaderid
left outer join
	purchaseorders$purchaseorder as supo
on
	supo_th.purchaseorders$PurchaseOrderid = supo.id
----------------------------
-- JOIN to Transfer Supplier
----------------------------
left outer join
	purchaseorders$PurchaseOrder_Supplier po_su
on
	txpo.id = po_su.purchaseorders$PurchaseOrderid
left outer join
	suppliers$supplier txsu
on
	po_su.suppliers$supplierid = txsu.id
----------------------------
-- JOIN to Supply Supplier
----------------------------
left outer join
	purchaseorders$PurchaseOrder_Supplier po_su2
on
	supo.id = po_su2.purchaseorders$PurchaseOrderid
left outer join
	suppliers$supplier susu
on
	po_su2.suppliers$supplierid = susu.id
----------------------------------------
-- JOIN to Customer Order for Shortages
----------------------------------------
left outer join
	PurchaseOrders$Shortage_PurchaseOrderLine shr_pol
on
	pol.id = shr_pol.PurchaseOrders$PurchaseOrderLineid
left outer join
	PurchaseOrders$Shortage shr
on
	shr_pol.PurchaseOrders$Shortageid = shr.id
left outer join
	PurchaseOrders$OrderLineShortage_Shortage olsh_shr
on
	shr.id = olsh_shr.PurchaseOrders$Shortageid
left outer join
	PurchaseOrders$OrderLineShortage olsh
on
	olsh_shr.PurchaseOrders$OrderLineShortageid = olsh.id
left outer join
	PurchaseOrders$OrderLineShortage_OrderLine olsh_ol
on
	olsh.id = olsh_ol.PurchaseOrders$OrderLineShortageid
left outer join
	OrderProcessing$OrderLine ol
on
	olsh_ol.OrderProcessing$OrderLineid = ol.id
left outer join
	OrderProcessing$OrderLine_Order ol_or
on
	ol.id = ol_or.OrderProcessing$OrderLineid
left outer join
	OrderProcessing$Order ord
on
	ol_or.OrderProcessing$Orderid = ord.id
-------------------
-- JOIN to Supplier
-------------------
left outer join
	warehouseshipments$shipment_supplier sh_su
on
	sh2.id = sh_su.warehouseshipments$shipmentid
left outer join
	suppliers$supplier su
on
	sh_su.suppliers$supplierid = su.id
-------------------------
-- JOIN to Product Family
-------------------------
left outer join
	product$stockitem_product si_pr
on
	si.id = si_pr.product$stockitemid
left outer join
	product$product pr
on
	si_pr.product$productid = pr.id
left outer join
	product$product_productfamily pr_prf
on
	pr.id = pr_prf.product$productid
left outer join
	product$productfamily prf
on
	pr_prf.product$productfamilyid = prf.id
-----------------------------------------------------------------------------------------------------
-- Sub-queries to calculate stock movements prior to start of current month, and during current month
-----------------------------------------------------------------------------------------------------
-- JOIN to subquery to get qty issued
-------------------------------------
LEFT OUTER JOIN
(
	select bsi_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsi.IssuedQuantity,0)) as QtyIssued
	from inventory$batchstockissue_warehousestockitembatch bsi_whsib
	LEFT OUTER JOIN inventory$batchstockissue bsi
	on bsi_whsib.inventory$batchstockissueid = bsi.id
	where bsi.createddate < (select max(monthstart) from tmp$rundate)
	and bsi.cancelled = false
	group by bsi_whsib.inventory$warehousestockitembatchid
) start_issued
 on
	whsib.id = start_issued.whsib_id
--------------------------------------------
-- JOIN to subquery to get qty SNAP disposed
--------------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyDisposed
	from inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Negative'
	and not (coalesce(sm.movementtype,'null') = 'ERPADJ'
	and coalesce(sm.reasonid,'null') = 'Manual ERP stock movement created by Patch028')
	and sm.DateClosed < (select max(monthstart) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) start_disposed
on
	whsib.id = start_disposed.whsib_id
-------------------------------------------
-- JOIN to subquery to get qty ERP disposed
-------------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyDisposed
	from inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Negative'
	and (coalesce(sm.movementtype,'null') = 'ERPADJ'
	and coalesce(sm.reasonid,'null') = 'Manual ERP stock movement created by Patch028')
	and sm.DateClosed < (select max(monthstart) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) start_erpdisposed
on
	whsib.id = start_erpdisposed.whsib_id
-----------------------------------------
-- JOIN to subquery to get qty SNAP added
-----------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyAdded
	from inventory$warehousestockitembatch whsib
	left outer join inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	on whsib.id = bsm_whsib.inventory$warehousestockitembatchid
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Positive'
	and not (coalesce(sm.movementtype,'null') = 'ERPADJ'
	and coalesce(sm.reasonid,'null') = 'Manual ERP stock movement created by Patch028')
	and not (sm.movetype = 'SREG' and bsm.Quantity = whsib.ReceivedQuantity)
	and sm.DateClosed  < (select max(monthstart) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) start_added
on
	whsib.id = start_added.whsib_id
-----------------------------------------
-- JOIN to subquery to get qty ERP added
-----------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyAdded
	from inventory$warehousestockitembatch whsib
	left outer join inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	on whsib.id = bsm_whsib.inventory$warehousestockitembatchid
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Positive'
	and (coalesce(sm.movementtype,'null') = 'ERPADJ'
	and coalesce(sm.reasonid,'null') = 'Manual ERP stock movement created by Patch028')
	and not (sm.movetype = 'SREG' and bsm.Quantity = whsib.ReceivedQuantity)
	and sm.DateClosed  < (select max(monthstart) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) start_erpadded
on
	whsib.id = start_erpadded.whsib_id
------------------------------------------------------
-- JOIN to subquery to get qty issued in current month
------------------------------------------------------
LEFT OUTER JOIN
(
	select bsi_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsi.IssuedQuantity,0)) as QtyIssued
	from inventory$batchstockissue_warehousestockitembatch bsi_whsib
	LEFT OUTER JOIN inventory$batchstockissue bsi
	on bsi_whsib.inventory$batchstockissueid = bsi.id
	where bsi.createddate >= (select max(monthstart) from tmp$rundate)
	and   bsi.createddate < (select max(monthend) from tmp$rundate)
	and   bsi.cancelled = false
	group by bsi_whsib.inventory$warehousestockitembatchid
) current_issued
 on
	whsib.id = current_issued.whsib_id
-------------------------------------------------------------
-- JOIN to subquery to get qty SNAP disposed in current month
-------------------------------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyDisposed
	from inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Negative'
	and not (coalesce(sm.movementtype,'null') = 'ERPADJ'
	and coalesce(sm.reasonid,'null') = 'Manual ERP stock movement created by Patch028')
	and sm.DateClosed >= (select max(monthstart) from tmp$rundate)
	and sm.DateClosed < (select max(monthend) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) current_disposed
on
	whsib.id = current_disposed.whsib_id
-------------------------------------------------------------
-- JOIN to subquery to get qty ERP disposed in current month
-------------------------------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyDisposed
	from inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Negative'
	and (coalesce(sm.movementtype,'null') = 'ERPADJ'
	and coalesce(sm.reasonid,'null') = 'Manual ERP stock movement created by Patch028')
	and sm.DateClosed >= (select max(monthstart) from tmp$rundate)
	and sm.DateClosed < (select max(monthend) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) current_erpdisposed
on
	whsib.id = current_erpdisposed.whsib_id
----------------------------------------------------------------
-- JOIN to subquery to get total qty SNAP added in current month
----------------------------------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyAdded
	from inventory$warehousestockitembatch whsib
	left outer join inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	on whsib.id = bsm_whsib.inventory$warehousestockitembatchid
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Positive'
	and not (coalesce(sm.movementtype,'null') = 'ERPADJ'
	and coalesce(sm.reasonid,'null') = 'Manual ERP stock movement created by Patch028')
	and not (sm.movetype = 'SREG' and bsm.Quantity = whsib.ReceivedQuantity)
	and sm.DateClosed  >= (select max(monthstart) from tmp$rundate)
	and sm.DateClosed  < (select max(monthend) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) current_added
on
	whsib.id = current_added.whsib_id
---------------------------------------------------------------
-- JOIN to subquery to get total qty ERP added in current month
---------------------------------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyAdded
	from inventory$warehousestockitembatch whsib
	left outer join inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	on whsib.id = bsm_whsib.inventory$warehousestockitembatchid
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Positive'
	and (coalesce(sm.movementtype,'null') = 'ERPADJ'
	and coalesce(sm.reasonid,'null') = 'Manual ERP stock movement created by Patch028')
	and not (sm.movetype = 'SREG' and bsm.Quantity = whsib.ReceivedQuantity)
	and sm.DateClosed  >= (select max(monthstart) from tmp$rundate)
	and sm.DateClosed  < (select max(monthend) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) current_erpadded
on
	whsib.id = current_erpadded.whsib_id
-------------------------------------
-- JOIN to subquery to get qty issued
-------------------------------------
LEFT OUTER JOIN
(
	select bsi_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsi.IssuedQuantity,0)) as QtyIssued
	from inventory$batchstockissue_warehousestockitembatch bsi_whsib
	LEFT OUTER JOIN inventory$batchstockissue bsi
	on bsi_whsib.inventory$batchstockissueid = bsi.id
	where bsi.createddate < (select max(monthend) from tmp$rundate)
	and   bsi.cancelled = false
	group by bsi_whsib.inventory$warehousestockitembatchid
) issued
 on
	whsib.id = issued.whsib_id
---------------------------------------
-- JOIN to subquery to get qty disposed
---------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyDisposed
	from inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Negative'
	and sm.DateClosed < (select max(monthend) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) disposed
on
	whsib.id = disposed.whsib_id
-- 	
-- ------------------------------------
-- -- JOIN to subquery to get qty added
-- ------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyAdded
	from inventory$warehousestockitembatch whsib
	left outer join inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	on whsib.id = bsm_whsib.inventory$warehousestockitembatchid
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Positive'
	and not (sm.movetype = 'SREG' and bsm.Quantity = whsib.ReceivedQuantity)
	and sm.DateClosed  < (select max(monthend) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) added
on
	whsib.id = added.whsib_id
--------------------------------------------
-- JOIN to temp table for UnitCost patching
--------------------------------------------
left outer join
	a$whsib_UnitCost uc
on
	whsib.batch_id = uc.batch_id
---------------
-- End of JOINs
---------------
WHERE
--	coalesce(whsib.ArrivedDate,whsib.ConfirmedDate,whsib.StockRegisteredDate) < (select max(monthend) from tmp$rundate)
	whsib.ArrivedDate <= (select max(monthend) from tmp$rundate)
and
-- -- Some batch records loaded in the initial data load (February 2016) have missing (null) stock registered dates - these need to be included as stock in warehouse
-- -- 	CASE
-- -- 		WHEN	whsib.StockRegisteredDate is null
-- -- 		AND	whsib.ArrivedDate < '2016-03-01 00:00:00.000'
-- -- 		THEN	'2016-02-10 00:00:00.000'
-- -- 		ELSE	whsib.StockRegisteredDate
-- -- 	END	< (select max(cutoff) from tmp$rundate)
	whsib.StockRegisteredDate < (select max(cutoff) from tmp$rundate)
------------------------------------------------------------
-- exclude any records where total stock is zero or negative
------------------------------------------------------------
and
	wh.name || si.sku not in
(select distinct warehouse || sku from
(
SELECT
	wh.Name as Warehouse,
	si.sku,
	sum(coalesce(whsib.ReceivedQuantity,0) + coalesce(added.QtyAdded,0) - coalesce(disposed.QtyDisposed,0) - coalesce(issued.QtyIssued,0)) as CurrentStock
FROM
	inventory$warehousestockitembatch as whsib
LEFT OUTER JOIN
	inventory$warehousestockitembatch_warehousestockitem whsib_whsi
on
	whsib.id = whsib_whsi.inventory$warehousestockitembatchid
	
left outer join
	inventory$warehousestockitem whsi
on
	whsib_whsi.inventory$warehousestockitemid = whsi.id
	
left outer join
	inventory$located_at whsi_wh
on
	whsi.id = whsi_wh.inventory$warehousestockitemid
	
left outer join
	inventory$warehouse wh
on
	whsi_wh.inventory$warehouseid = wh.id
---------------------
-- JOIN to Stock Item
---------------------
left outer join
	inventory$warehousestockitem_stockitem whsi_si
on
	whsi.id = whsi_si.inventory$warehousestockitemid
left outer join
	product$stockitem si
on
	whsi_si.product$stockitemid = si.id
-------------------------------------
-- JOIN to subquery to get qty issued
-------------------------------------
LEFT OUTER JOIN
(
	select bsi_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsi.IssuedQuantity,0)) as QtyIssued
	from inventory$batchstockissue_warehousestockitembatch bsi_whsib
	LEFT OUTER JOIN inventory$batchstockissue bsi
	on bsi_whsib.inventory$batchstockissueid = bsi.id
	where bsi.createddate < (select max(monthend) from tmp$rundate)
	and bsi.cancelled = false
	group by bsi_whsib.inventory$warehousestockitembatchid
) issued
 on
	whsib.id = issued.whsib_id
---------------------------------------
-- JOIN to subquery to get qty disposed
---------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- NOTE: This approach to calculating additions & disposals is not currently accurate as manual updates can be made directly against the warehouse stock item batch record
-- To be reviewed post-July month end
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyDisposed
	from inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Negative'
	and sm.DateClosed < (select max(monthend) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) disposed
on
	whsib.id = disposed.whsib_id
-- 	
-- ------------------------------------
-- -- JOIN to subquery to get qty added
-- ------------------------------------
LEFT OUTER JOIN
(
	select bsm_whsib.inventory$warehousestockitembatchid as whsib_id, sum(coalesce(bsm.Quantity,0)) as QtyAdded
	from inventory$warehousestockitembatch whsib
	left outer join inventory$batchstockmovement_warehousestockitembatch bsm_whsib
	on whsib.id = bsm_whsib.inventory$warehousestockitembatchid
	LEFT OUTER JOIN inventory$batchstockmovement bsm
	on bsm_whsib.inventory$batchstockmovementid = bsm.id
	LEFT OUTER JOIN inventory$batchstockmovement_stockmovement bsm_sm
	on bsm.id = bsm_sm.inventory$batchstockmovementid
	LEFT OUTER JOIN inventory$stockmovement sm
	on bsm_sm.inventory$stockmovementid = sm.id
	where sm.stockadjustment = 'Positive'
	and not (sm.movetype = 'SREG' and bsm.Quantity = whsib.ReceivedQuantity)
	and sm.DateClosed  < (select max(monthend) from tmp$rundate)
	group by bsm_whsib.inventory$warehousestockitembatchid
) added
on
	whsib.id = added.whsib_id
---------------
-- End of JOINs
---------------
WHERE
--	coalesce(whsib.ArrivedDate,whsib.ConfirmedDate,whsib.StockRegisteredDate) < (select max(monthend) from tmp$rundate)
	whsib.ArrivedDate < (select max(monthend) from tmp$rundate)
and
-- Some batch records loaded in the initial data load (February 2016) have missing (null) stock registered dates - these need to be included as stock in warehouse
-- 	CASE
-- 		WHEN	whsib.StockRegisteredDate is null
-- 		AND	whsib.ArrivedDate < '2016-03-01 00:00:00.000'
-- 		THEN	'2016-02-10 00:00:00.000'
-- 		ELSE	whsib.StockRegisteredDate
-- 	END	< (select max(cutoff) from tmp$rundate)
	whsib.StockRegisteredDate < (select max(cutoff) from tmp$rundate)
group by
	wh.Name,
	si.sku
having
	sum(coalesce(whsib.ReceivedQuantity,0) + coalesce(added.QtyAdded,0) - coalesce(disposed.QtyDisposed,0) - coalesce(issued.QtyIssued,0)) = 0
order by
	wh.Name,
	si.sku
) ZeroSKU
)
--and wh.name like 'Horizon%'
-----------------------------------------------------------------------------------------
-- Exclude the SKUs for glasses package products which are not held as physical stock
-----------------------------------------------------------------------------------------
and
	si.sku not in 
(
'029680000000000000000011',
'029720000000000000000011',
'029760000000000000000011',
'029780000000000000000011',
'029700000000000000000011',
'029740000000000000000011',
'029730000000000000000011',
'029690000000000000000011',
'029750000000000000000011',
'029770000000000000000011',
'029710000000000000000011'
)
and
-- 	coalesce(whsib.ReceivedQuantity,0) + coalesce(start_added.QtyAdded,0) + coalesce(current_added.QtyAdded,0) -
--	coalesce(start_disposed.QtyDisposed,0) - coalesce(current_disposed.QtyDisposed,0) - coalesce(start_issued.QtyIssued,0) - coalesce(current_issued.QtyIssued,0) != 0
	coalesce(whsib.ReceivedQuantity,0) + coalesce(added.QtyAdded,0) - coalesce(disposed.QtyDisposed,0) - coalesce(issued.QtyIssued,0) != 0
--
-- ***************************************************************************************************************** --
-- *************************** Start 'Stock Delivered' and 'Stock Received' **************************************** --
-- ***************************************************************************************************************** --
UNION ALL
SELECT
	wh.name Warehouse,
	CASE
		WHEN sh.arriveddate < (select max(monthend) from tmp$rundate)
		and (sh.confirmeddate is null
		or  sh.confirmeddate >= (select max(monthend) from tmp$rundate))
		and (sh.stockregistereddate is null
		or  sh.stockregistereddate >= (select max(cutoff) from tmp$rundate))
		THEN 'Stock Delivered'
		ELSE 'Stock Received'
	END as InventoryStatus,
	null as BatchId,
	null as id,
	sh.ShipmentType as originalshipmenttype,
	CASE
		WHEN	ord._type = 'Wholesale_order'
		AND NOT	(tpo_th.purchaseorders$PurchaseOrderid is null and spo_th.purchaseorders$PurchaseOrderid is null)
		THEN	'Wholesale (for Intersite)'
		WHEN	ord._type = 'Wholesale_order'
		THEN	'Wholesale'
		WHEN	sh.ShipmentType = 'Back_To_Back'
		AND	spo_th.purchaseorders$PurchaseOrderid is not null
		THEN	'Back_To_Back (for Intersite)'
		WHEN	sh.ShipmentType = 'Stock'
		AND NOT	(tpo_th.purchaseorders$PurchaseOrderid is null and spo_th.purchaseorders$PurchaseOrderid is null)
		THEN	'Stock (for Intersite)'
		ELSE	coalesce(sh.ShipmentType,'Stock')
	END as ShipmentType,
	coalesce(sh.arriveddate,sh.confirmeddate) DateDelivered, 
	sh.confirmeddate DateReceived, 
	sh.stockregistereddate DateRegistered,
	po.ordernumber PO_Number,
	su.suppliername SupplierName,
	po.supplierreference SupplierOrderRef,
	su.currencycode SupplierCurrency,
	shol.unitcost as SupplierUnitCost,
	null Transfer_PONumber,
	null Transfer_SupplierName,
	null Transfer_SupplierOrderRef,
	txth.transfernum tpo_header,
	suth.transfernum spo_header,
	ord._type ordertype,
	ord.magentoorderid ordernum,
	ord.incrementid wholesaleordernum,
	sh.receiptid ReceiptId,
	sh.supplieradviceref DeliveryNote,
	si.sku,
	si.StockItemDescription as StockItem,
	prf.name as ProductFamily,
	si.packsize PackSize,
	shol.status Status,
	shol.Quantityordered QuantityOrdered,
	shol.Quantityreceived QuantityReceived,
	shol.Quantityaccepted QuantityAccepted,
	null as TotalQuantityAdded,
   	null as TotalQuantityIssued,
 	null as TotalQuantityDisposed,
  	null as StartSNAPAdded,
  	null as StartERPAdded,
   	null as StartIssued,
 	null as StartSNAPDisposed,
 	null as StartERPDisposed,
   	null as CurrentSNAPAdded,
   	null as CurrentERPAdded,
   	null as CurrentIssued,
 	null as CurrentSNAPDisposed,
 	null as CurrentERPDisposed,
	null as ERPManualAdjustment,
	null as StockInTransit,
	CASE
		WHEN (shol.Quantityaccepted is not null) and (shol.Quantityaccepted !=0)
		THEN shol.Quantityaccepted
		WHEN (shol.Quantityreceived is not null) and (shol.Quantityreceived !=0)
		THEN shol.Quantityreceived
		ELSE shol.Quantityordered
	END as CurrentStock,
	0 as CheckTotal,
	CASE
		WHEN wh.name = 'York'
		THEN 'GBP'
		ELSE 'EUR'
	END as Currency,
	shol.unitcost * (wccy.spotrate/ccy.spotrate) as UnitCost,
	CASE
		WHEN (shol.Quantityaccepted is not null) and (shol.Quantityaccepted !=0)
		THEN (shol.unitcost * (wccy.spotrate/ccy.spotrate)) * shol.Quantityaccepted
		WHEN (shol.Quantityreceived is not null) and (shol.Quantityreceived !=0)
		THEN (shol.unitcost * (wccy.spotrate/ccy.spotrate)) * shol.Quantityreceived
		ELSE (shol.unitcost * (wccy.spotrate/ccy.spotrate)) * shol.Quantityordered
	END as TotalValue
FROM
	warehouseshipments$shipmentorderline as shol
left outer join
	warehouseshipments$shipmentorderline_shipment as shol_sh
on
	shol.id = shol_sh.warehouseshipments$shipmentorderlineid
left outer join
	warehouseshipments$shipment as sh
on
	shol_sh.warehouseshipments$shipmentid = sh.id
left outer join
	warehouseshipments$shipment_warehouse sh_wh
on
	sh.id = sh_wh.warehouseshipments$shipmentid
left outer join
	inventory$warehouse wh
on
	sh_wh.inventory$warehouseid = wh.id
-------------------------
-- JOIN to Purchase Order
-------------------------
left outer join
	warehouseshipments$shipmentorderline_purchaseorderline shol_pol
on
	shol.id = shol_pol.warehouseshipments$shipmentorderlineid
left outer join
	purchaseorders$purchaseorderline pol
on
	shol_pol.purchaseorders$purchaseorderlineid = pol.id
left outer join
	purchaseorders$purchaseorderline_purchaseorderlineheader as pol_polh
on
	pol.id = pol_polh.purchaseorders$purchaseorderlineid
left outer join
	purchaseorders$purchaseorderlineheader as polh
on
	pol_polh.purchaseorders$purchaseorderlineheaderid = polh.id
left outer join
	purchaseorders$purchaseorderlineheader_purchaseorder as polh_po
on
	polh.id = polh_po.purchaseorders$purchaseorderlineheaderid
left outer join
	purchaseorders$purchaseorder as po
on
	polh_po.purchaseorders$purchaseorderid = po.id
---------------------------
-- JOIN to Transfer Header
---------------------------
left outer join
	purchaseorders$SupplyPO_TransferHeader spo_th
on
	po.id = spo_th.purchaseorders$PurchaseOrderid
left outer join
	intersitetransfer$TransferHeader suth
on
	spo_th.intersitetransfer$transferheaderid = suth.id
left outer join
	purchaseorders$TransferPO_TransferHeader tpo_th
on
	po.id = tpo_th.purchaseorders$PurchaseOrderid
left outer join
	intersitetransfer$TransferHeader txth
on
	tpo_th.intersitetransfer$transferheaderid = txth.id
----------------------------------------
-- JOIN to Customer Order for Shortages
----------------------------------------
left outer join
	PurchaseOrders$Shortage_PurchaseOrderLine shr_pol
on
	pol.id = shr_pol.PurchaseOrders$PurchaseOrderLineid
left outer join
	PurchaseOrders$Shortage shr
on
	shr_pol.PurchaseOrders$Shortageid = shr.id
left outer join
	PurchaseOrders$OrderLineShortage_Shortage olsh_shr
on
	shr.id = olsh_shr.PurchaseOrders$Shortageid
left outer join
	PurchaseOrders$OrderLineShortage olsh
on
	olsh_shr.PurchaseOrders$OrderLineShortageid = olsh.id
left outer join
	PurchaseOrders$OrderLineShortage_OrderLine olsh_ol
on
	olsh.id = olsh_ol.PurchaseOrders$OrderLineShortageid
left outer join
	OrderProcessing$OrderLine ol
on
	olsh_ol.OrderProcessing$OrderLineid = ol.id
left outer join
	OrderProcessing$OrderLine_Order ol_or
on
	ol.id = ol_or.OrderProcessing$OrderLineid
left outer join
	OrderProcessing$Order ord
on
	ol_or.OrderProcessing$Orderid = ord.id
-------------------------
-- JOIN to Product Family
-------------------------
left outer join
	purchaseorders$PurchaseOrderLine_StockItem pol_si
on
	pol.id = pol_si.purchaseorders$PurchaseOrderLineid
left outer join
	product$stockitem si
on
	pol_si.product$stockitemid = si.id
left outer join
	product$stockitem_product si_pr
on
	si.id = si_pr.product$stockitemid
left outer join
	product$product pr
on
	si_pr.product$productid = pr.id
left outer join
	product$product_productfamily pr_prf
on
	pr.id = pr_prf.product$productid
left outer join
	product$productfamily prf
on
	pr_prf.product$productfamilyid = prf.id
---------------------
-- JOIN to Supplier
-------------------
left outer join
	warehouseshipments$shipment_supplier sh_su
on
	sh.id = sh_su.warehouseshipments$shipmentid
left outer join
	suppliers$supplier su
on
	sh_su.suppliers$supplierid = su.id
--
-------------------
-- JOIN to Currency
-------------------
left outer join
	countrymanagement$currency ccy
on
	su.currencycode = ccy.currencycode
-----------------------------
-- JOIN to warehouse Currency
-----------------------------
left outer join
	countrymanagement$currency wccy
on
	CASE
		WHEN wh.name = 'York'
		THEN 'GBP'
		ELSE 'EUR'
	END = wccy.currencycode
---------------
-- End of JOINs
---------------
where
	coalesce(sh.arriveddate,sh.confirmeddate) < (select max(monthend) from tmp$rundate)
and
(
 	sh.stockregistereddate is null
or
	sh.stockregistereddate >= (select max(cutoff) from tmp$rundate)
)
and
	sh.status != 'Cancelled'
--
-- ***************************************************************************************************************** --
-- ************************************** Start 'Stock in Transit' query ******************************************* --
-- ***************************************************************************************************************** --
----------------------------------
-- Start 'Stock in Transit' report
----------------------------------
UNION ALL
SELECT 
 	wh.name Warehouse,
 	'Stock in Transit' as InventoryStatus,
	null BatchId,
	null as id,
	'Intersite' as originalshipmenttype,
	'Intersite' as ShipmentType,
	itsib.ArrivedDate as DateDelivered,
	itsib.ConfirmedDate as DateReceived,
	itsib.StockRegisteredDate as DateRegistered,
	spo.ordernumber,
	ssu.suppliername SupplierName,
	spo.supplierreference SupplierOrderRef,
	ssu.currencycode SupplierCurrency,
	null SupplierUnitCost,
	tpo.ordernumber Transfer_PONumber,
	tsu.suppliername Transfer_SupplierName,
	tpo.supplierreference Transfer_SupplierOrderRef,
	null tpo_header,
	null spo_header,
	null ordertype,
	null ordernum,
	null wholesaleordernum,
	null ReceiptId,
	null DeliveryNote,
	si.sku,
	si.StockItemDescription as StockItem,
	prf.name as ProductFamily,
	si.packsize PackSize,
	null as Status,
	null as QuantityOrdered,
	null as QuantityReceived,
	null as QuantityAccepted,
	null as TotalQuantityAdded,
   	null as TotalQuantityIssued,
 	null as TotalQuantityDisposed,
  	null as StartSNAPAdded,
  	null as StartERPAdded,
   	null as StartIssued,
 	null as StartSNAPDisposed,
 	null as StartERPDisposed,
   	null as CurrentSNAPAdded,
   	null as CurrentERPAdded,
   	null as CurrentIssued,
 	null as CurrentSNAPDisposed,
 	null as CurrentERPDisposed,
	null as ERPManualAdjustment,
 	itsib.remainingquantity as StockInTransit,
 	itsib.remainingquantity as CurrentStock,
	0 as CheckTotal,
 	itsib.currency as Currency,
	itsib.totalunitcost as UnitCost,
	itsib.remainingquantity * itsib.totalunitcost as TotalValue
FROM
	intersitetransfer$intransitstockitembatch itsib
--------------------------
-- JOIN to transfer header
--------------------------
left outer join
	intersitetransfer$intransitstockitembatch_transferheader itsib_th
on
	itsib.id = itsib_th.IntersiteTransfer$InTransitStockItemBatchid
left outer join
	intersitetransfer$transferheader th
on
	itsib_th.intersitetransfer$transferheaderid = th.id
-----------------------
-- JOIN to supplier PO
-----------------------
left outer join
	purchaseorders$SupplyPO_TransferHeader spo_th
on
	th.id = spo_th.intersitetransfer$transferheaderid
left outer join
	purchaseorders$purchaseorder spo
on
	spo_th.purchaseorders$purchaseorderid = spo.id
left outer join
	purchaseorders$PurchaseOrder_Supplier spo_su
on
	spo.id = spo_su.purchaseorders$purchaseorderid
left outer join
	Suppliers$Supplier ssu
on
	spo_su.Suppliers$Supplierid = ssu.id
-----------------------
-- JOIN to transfer PO
-----------------------
left outer join
	purchaseorders$TransferPO_TransferHeader tpo_th
on
	th.id = tpo_th.intersitetransfer$transferheaderid
left outer join
	purchaseorders$purchaseorder tpo
on
	tpo_th.purchaseorders$purchaseorderid = tpo.id
left outer join
	purchaseorders$PurchaseOrder_Supplier tpo_su
on
	tpo.id = tpo_su.purchaseorders$purchaseorderid
left outer join
	Suppliers$Supplier tsu
on
	tpo_su.Suppliers$Supplierid = tsu.id
------------------------------
-- JOIN to receiving warehouse
------------------------------
left outer join
	purchaseorders$PurchaseOrder_Warehouse tpo_wh
on
	tpo.id = tpo_wh.purchaseorders$purchaseorderid
left outer join
	inventory$warehouse wh
on
	tpo_wh.inventory$warehouseid = wh.id
-----------------------
-- JOIN to batch header
-----------------------
left outer join
	intersitetransfer$intransitstockitembatch_intransitbatchheader itsib_itbh
on
	itsib.id = itsib_itbh.IntersiteTransfer$InTransitStockItemBatchid
left outer join
	intersitetransfer$intransitbatchheader itbh
on
	itsib_itbh.intersitetransfer$intransitbatchheaderid = itbh.id
---------------------
-- JOIN to Stock Item
---------------------
left outer join
	intersitetransfer$intransitstockitembatch_stockitem itsib_si
on
	itsib.id = itsib_si.intersitetransfer$intransitstockitembatchid
left outer join
	product$stockitem si
on
	itsib_si.product$stockitemid = si.id
-------------------------
-- JOIN to Product Family
-------------------------
left outer join
	product$stockitem_product si_pr
on
	si.id = si_pr.product$stockitemid
left outer join
	product$product pr
on
	si_pr.product$productid = pr.id
left outer join
	product$product_productfamily pr_prf
on
	pr.id = pr_prf.product$productid
left outer join
	product$productfamily prf
on
	pr_prf.product$productfamilyid = prf.id
--
----------------------------
-- JOIN to customer shipment
----------------------------
left outer join
	intersitetransfer$intransitbatchheader_customershipment itbh_csh
on
	itbh.id = itbh_csh.intersitetransfer$intransitbatchheaderid
left outer join
	customershipments$customershipment csh
on
	itbh_csh.customershipments$customershipmentid = csh.id
-------------------------------------
-- JOIN to customer shipment progress
-------------------------------------
left outer join
(
select distinct
	csh_lu.customershipments$customershipmentid cshid,
	lu.status,
	lu.timestamp
from
	customershipments$customershipment_progress csh_lu
left outer join
	customershipments$lifecycle_update lu
on
	csh_lu.customershipments$lifecycle_updateid = lu.id
where
	lu.status = 'Complete'
) as csh_complete
on
	csh.id = csh_complete.cshid
---------------------------------------------------------------------------------------------------
-- JOIN to subquery to include batches with remaining qty > 0 and which have not yet been received 
---------------------------------------------------------------------------------------------------
inner join
(
SELECT
	th.transfernum,
	tpo.ordernumber PO_Number,
	sum(pol.quantity) pol_qty,
	count(distinct pol.stockitemid) pol_count,
	sum(pol.quantity) / count(distinct pol.stockitemid) pol_qty2,
	sum(shol.quantityreceived) received,
	sum(shol.quantityaccepted) accepted,
	sum(shol.quantityrejected) rejected,
	sum(shol.quantityreturned) returned
FROM
	intersitetransfer$transferheader th
-----------------------
-- JOIN to transfer PO
-----------------------
left outer join
	purchaseorders$TransferPO_TransferHeader tpo_th
on
	th.id = tpo_th.intersitetransfer$transferheaderid
left outer join
	purchaseorders$purchaseorder tpo
on
	tpo_th.purchaseorders$purchaseorderid = tpo.id
----------------------------------
-- JOIN to transfer PO line header
----------------------------------
left outer join
	purchaseorders$purchaseorderLineheader_purchaseorder polh_po
on
	tpo.id = polh_po.purchaseorders$purchaseorderid
left outer join
	purchaseorders$purchaseorderlineheader polh
on
	polh_po.purchaseorders$purchaseorderlineheaderid = polh.id
---------------------------
-- JOIN to transfer PO line 
---------------------------
left outer join
	purchaseorders$purchaseorderLine_purchaseorderlineheader pol_polh
on
	polh.id = pol_polh.purchaseorders$purchaseorderlineheaderid
left outer join
	purchaseorders$purchaseorderline pol
on
	pol_polh.purchaseorders$purchaseorderlineid = pol.id
-------------------------------
-- JOIN to transfer receipt line 
--------------------------------
left outer join
	warehouseshipments$shipmentorderline_purchaseorderline shol_pol
on
	pol.id = shol_pol.purchaseorders$purchaseorderlineid
left outer join
	warehouseshipments$shipmentorderline as shol
on
	shol_pol.warehouseshipments$shipmentorderlineid = shol.id
where
	th.transfernum in
(
SELECT 
distinct
 	th.transfernum transfernum
FROM
	intersitetransfer$intransitstockitembatch itsib
--------------------------
-- JOIN to transfer header
--------------------------
left outer join
	intersitetransfer$intransitstockitembatch_transferheader itsib_th
on
	itsib.id = itsib_th.IntersiteTransfer$InTransitStockItemBatchid
left outer join
	intersitetransfer$transferheader th
on
	itsib_th.intersitetransfer$transferheaderid = th.id
-----------------------
-- JOIN to batch header
-----------------------
left outer join
	intersitetransfer$intransitstockitembatch_intransitbatchheader itsib_itbh
on
	itsib.id = itsib_itbh.IntersiteTransfer$InTransitStockItemBatchid
left outer join
	intersitetransfer$intransitbatchheader itbh
on
	itsib_itbh.intersitetransfer$intransitbatchheaderid = itbh.id
----------------------------
-- JOIN to customer shipment
----------------------------
left outer join
	intersitetransfer$intransitbatchheader_customershipment itbh_csh
on
	itbh.id = itbh_csh.intersitetransfer$intransitbatchheaderid
left outer join
	customershipments$customershipment csh
on
	itbh_csh.customershipments$customershipmentid = csh.id
-------------------------------------
-- JOIN to customer shipment progress
-------------------------------------
left outer join
(
select distinct
	csh_lu.customershipments$customershipmentid cshid,
	lu.status,
	lu.timestamp
from
	customershipments$customershipment_progress csh_lu
left outer join
	customershipments$lifecycle_update lu
on
	csh_lu.customershipments$lifecycle_updateid = lu.id
where
	lu.status = 'Complete'
) as csh_complete
on
	csh.id = csh_complete.cshid
where
	csh.intersite = true
and
	csh.status = 'Complete'
and
	csh_complete.timestamp < (select max(monthend) from tmp$rundate)
and
	itsib.remainingquantity != 0
order by
	th.transfernum
)
group by
	th.transfernum,
	tpo.ordernumber
having 
(	sum(shol.quantityreceived) = 0
and
	sum(shol.quantityaccepted) = 0
and
	sum(shol.quantityrejected) = 0
and
	sum(shol.quantityreturned) = 0)
order by
	th.transfernum,
	tpo.ordernumber
) as NotDelivered
on 
	th.transfernum = NotDelivered.transfernum
-------------------------------------------------------------------------------------------------
-- End of JOIN to subquery to include only the batches which have not yet been received 
-------------------------------------------------------------------------------------------------
where
	csh.intersite = true
and
	csh.status = 'Complete'
and
	csh_complete.timestamp < (select max(monthend) from tmp$rundate)
and
	itsib.remainingquantity != 0
order by 2, 1, 3, 4, 5

