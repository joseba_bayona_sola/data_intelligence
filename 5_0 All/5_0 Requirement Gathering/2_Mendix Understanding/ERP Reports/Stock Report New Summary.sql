
-- STOCK + ISSUES + RECEIVED + ADJUST 


-- STOCK 
	-- inventory$warehousestockitembatch
	
	-- inventory$warehousestockitembatch_shipment

	-- inventory$warehousestockitembatch_warehousestockitem
	-- inventory$warehousestockitem_stockitem
	-- product$stockitem

	-- inventory$located_at
	-- inventory$warehouse


-- ISSUES 
	-- inventory$batchstockissue

	-- inventory$batchstockissue_warehousestockitembatch
	-- inventory$warehousestockitembatch_warehousestockitem
	-- inventory$warehousestockitem_stockitem
	-- product$stockitem

	-- inventory$located_at
	-- inventory$warehouse

	-- inventory$batchstockissue_batchstockallocation
	-- inventory$batchstockallocation

	-- inventory$batchtransaction_orderlinestockallocationtransaction

	-- stockallocation$at_order
	-- orderprocessing$order
	-- orderprocessing$order_transferheader

-- RECEIVED 
	-- warehouseshipments$shipment

	-- warehouseshipments$shipment_warehouse
	-- inventory$warehouse

	-- warehouseshipments$shipment_supplier
	-- suppliers$supplier

	-- suppliers$supplier_currency
	-- countrymanagement$currency

	-- countrymanagement$spotrate
	-- countrymanagement$historicalspotrate_currency
	-- countrymanagement$currency

	-- warehouseshipments$shipmentorderline_shipment
	-- warehouseshipments$shipmentorderline

	-- warehouseshipments$shipmentorderline_purchaseorderline
	-- purchaseorders$purchaseorderline

	-- purchaseorders$purchaseorderline_stockitem
	-- product$stockitem

-- ADJUST 
	-- inventory$batchstockmovement

	-- inventory$batchstockmovement_warehousestockitembatch
	-- inventory$warehousestockitembatch

	-- inventory$warehousestockitembatch_warehousestockitem

	-- inventory$located_at
	-- inventory$warehouse

	-- inventory$batchstockmovement_stockmovement
	-- inventory$stockmovement

	-- inventory$warehousestockitem_stockitem
	-- product$stockitem

	-- inventory$warehousestockitembatch_shipment