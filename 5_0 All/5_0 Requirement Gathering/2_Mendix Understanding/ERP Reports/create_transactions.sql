DROP TABLE IF EXISTS AllTransactions;

SELECT *
INTO TABLE AllTransactions
FROM
  (
    -- STOCK
    SELECT
      'stock'          AS transaction,
      createddate      AS datetime,
      shortname        AS warehouse,
      sku              AS sku,

      -- adjustment can't be tracked (positive adjustment added to receivedquantity and some of the adjustments have no timestamp)
      receivedquantity - inventory$warehousestockitembatch.disposedquantity AS units,
      productunitcost  AS unitcost,
      NULL :: VARCHAR  AS currency,
      NULL :: FLOAT    AS day_rate,
      NULL :: FLOAT    AS eurate,
      NULL :: VARCHAR  AS supplier,
      NULL :: VARCHAR  AS suppliertype,
      NULL :: VARCHAR  AS document_number,
      NULL :: VARCHAR  AS order_status

    FROM inventory$warehousestockitembatch
      LEFT OUTER JOIN inventory$warehousestockitembatch_shipment
        ON inventory$warehousestockitembatch_shipment.inventory$warehousestockitembatchid =
           inventory$warehousestockitembatch.id
      JOIN inventory$warehousestockitembatch_warehousestockitem
        ON inventory$warehousestockitembatch_warehousestockitem.inventory$warehousestockitembatchid =
           inventory$warehousestockitembatch.id
      JOIN inventory$located_at ON inventory$located_at.inventory$warehousestockitemid =
                                   inventory$warehousestockitembatch_warehousestockitem.inventory$warehousestockitemid
      JOIN inventory$warehouse ON inventory$located_at.inventory$warehouseid = inventory$warehouse.id
      JOIN inventory$warehousestockitem_stockitem
        ON inventory$warehousestockitem_stockitem.inventory$warehousestockitemid =
           inventory$warehousestockitembatch_warehousestockitem.inventory$warehousestockitemid
      JOIN product$stockitem
        ON inventory$warehousestockitem_stockitem.product$stockitemid = product$stockitem.id

    -- only those that don't have receipts( shipment) attached
    WHERE inventory$warehousestockitembatch_shipment.warehouseshipments$shipmentid IS NULL

    UNION ALL

    -- ISSUES

    SELECT
      'issue'                                  AS transaction,
      CASE WHEN orderprocessing$order_transferheader.intersitetransfer$transferheaderid IS NOT NULL THEN
          inventory$batchstockissue.createddate + interval '1 hour'
        ELSE
          inventory$batchstockissue.createddate
      END
                                              AS datetime,
      shortname                                AS warehouse,
      sku                                      AS sku,
      inventory$batchstockissue.issuedquantity AS units,
      NULL :: FLOAT                            AS unitcost,
      NULL :: VARCHAR                          AS currency,
      NULL :: FLOAT                            AS day_rate,
      NULL :: FLOAT                            AS eurate,
      NULL :: VARCHAR                          AS supplier,
      CASE WHEN
        orderprocessing$order_transferheader.intersitetransfer$transferheaderid IS NOT NULL
        THEN 'Company'
      ELSE
        NULL :: VARCHAR END
                                               AS suppliertype,
      incrementid
                                               AS document_number,

      orderstatus                              AS order_status


    FROM inventory$batchstockissue
      JOIN inventory$batchstockissue_warehousestockitembatch
        ON inventory$batchstockissue_warehousestockitembatch.inventory$batchstockissueid = inventory$batchstockissue.id
      JOIN inventory$warehousestockitembatch_warehousestockitem
        ON inventory$warehousestockitembatch_warehousestockitem.inventory$warehousestockitembatchid =
           inventory$batchstockissue_warehousestockitembatch.inventory$warehousestockitembatchid
      JOIN inventory$warehousestockitem_stockitem
        ON inventory$warehousestockitem_stockitem.inventory$warehousestockitemid =
           inventory$warehousestockitembatch_warehousestockitem.inventory$warehousestockitemid
      JOIN product$stockitem
        ON inventory$warehousestockitem_stockitem.product$stockitemid = product$stockitem.id
      JOIN inventory$located_at
        ON inventory$located_at.inventory$warehousestockitemid =
           inventory$warehousestockitem_stockitem.inventory$warehousestockitemid
      JOIN inventory$warehouse
        ON inventory$warehouse.id = inventory$located_at.inventory$warehouseid

      -- inventory$batchstockallocation
      LEFT OUTER JOIN inventory$batchstockissue_batchstockallocation
        ON inventory$batchstockissue_batchstockallocation.inventory$batchstockissueid = inventory$batchstockissue.id
      LEFT OUTER JOIN inventory$batchstockallocation
        ON inventory$batchstockallocation.id =
           inventory$batchstockissue_batchstockallocation.inventory$batchstockallocationid

      -- stockallocation$orderlinestockallocationtransaction
      LEFT OUTER JOIN inventory$batchtransaction_orderlinestockallocationtransaction
        ON inventory$batchtransaction_orderlinestockallocationtransaction.inventory$batchstockallocationid =
           inventory$batchstockissue_batchstockallocation.inventory$batchstockallocationid

      LEFT OUTER JOIN stockallocation$at_order
        ON stockallocation$at_order.stockallocation$orderlinestockallocationtransactionid =
           inventory$batchtransaction_orderlinestockallocationtransaction.stockallocation$orderlinestockallocationtransactionid

      -- orderprocessing$order
      LEFT OUTER JOIN orderprocessing$order
        ON orderprocessing$order.id = stockallocation$at_order.orderprocessing$orderid

      -- orderprocessing$order_transferheader
      LEFT OUTER JOIN orderprocessing$order_transferheader
        ON orderprocessing$order_transferheader.orderprocessing$orderid =
           orderprocessing$order.id

    WHERE inventory$batchstockissue.cancelled = FALSE

    UNION ALL

    -- RECEIVED
    SELECT

      'receipt'  AS transaction,
      -- intersite receipt can't be before transfer order issue
      CASE WHEN suppliertype = 'Company' AND stockregistereddate - warehouseshipments$shipment.arriveddate > interval '6 day' THEN
        coalesce(coalesce(stockregistereddate, confirmeddate), warehouseshipments$shipment.arriveddate)
      ELSE
        coalesce(coalesce(arriveddate, confirmeddate), warehouseshipments$shipment.createddate)
      END
                                                                                              AS datetime,
      shortname                                                                               AS warehouse,
      sku                                                                                     AS sku,
      warehouseshipments$shipmentorderline.quantityreceived                                   AS units,
      unitcost                                                                                AS unitcost,
      cm.currencycode                                                                         AS currency,
      cur.spotrate                                                                            AS day_rate,
      eur.spotrate                                                                            AS eurate,
      suppliername                                                                            AS supplier,
      suppliertype                                                                            AS suppliertype,
      warehouseshipments$shipment.receiptid                                                   AS document_number,
      NULL :: VARCHAR                                                                         AS order_status

    FROM warehouseshipments$shipment
      JOIN warehouseshipments$shipment_warehouse
        ON warehouseshipments$shipment_warehouse.warehouseshipments$shipmentid = warehouseshipments$shipment.id
      JOIN inventory$warehouse ON warehouseshipments$shipment_warehouse.inventory$warehouseid = inventory$warehouse.id
      LEFT OUTER JOIN warehouseshipments$shipment_supplier
        ON warehouseshipments$shipment_supplier.warehouseshipments$shipmentid = warehouseshipments$shipment.id
      LEFT OUTER JOIN suppliers$supplier
        ON suppliers$supplier.id = warehouseshipments$shipment_supplier.suppliers$supplierid
      LEFT OUTER JOIN suppliers$supplier_currency
        ON warehouseshipments$shipment_supplier.suppliers$supplierid = suppliers$supplier_currency.suppliers$supplierid
      LEFT OUTER JOIN countrymanagement$currency AS cm
        ON cm.id = suppliers$supplier_currency.countrymanagement$currencyid

      LEFT OUTER JOIN
      (SELECT
         date_trunc('day', effectivedate) AS ed,
         currencycode,
         value                            AS spotrate
       FROM countrymanagement$spotrate
         JOIN countrymanagement$historicalspotrate_currency
           ON countrymanagement$spotrate.id = countrymanagement$historicalspotrate_currency.countrymanagement$spotrateid
         JOIN countrymanagement$currency
           ON countrymanagement$currency.id = countrymanagement$historicalspotrate_currency.countrymanagement$currencyid
       GROUP BY 1, 2, 3) AS cur ON ed = date_trunc('day', coalesce(coalesce(arriveddate, confirmeddate),
                                                                   warehouseshipments$shipment.createddate)) AND
                                   cur.currencycode = cm.currencycode
      LEFT OUTER JOIN
      (SELECT
         date_trunc('day', effectivedate) AS ed2,
         currencycode,
         value                            AS spotrate
       FROM countrymanagement$spotrate
         JOIN countrymanagement$historicalspotrate_currency
           ON countrymanagement$spotrate.id = countrymanagement$historicalspotrate_currency.countrymanagement$spotrateid
         JOIN countrymanagement$currency
           ON countrymanagement$currency.id = countrymanagement$historicalspotrate_currency.countrymanagement$currencyid
       GROUP BY 1, 2, 3) AS eur ON ed2 = date_trunc('day', coalesce(coalesce(arriveddate, confirmeddate),
                                                                    warehouseshipments$shipment.createddate)) AND
                                   eur.currencycode = 'EUR'

      JOIN warehouseshipments$shipmentorderline_shipment
        ON warehouseshipments$shipmentorderline_shipment.warehouseshipments$shipmentid = warehouseshipments$shipment.id
      JOIN warehouseshipments$shipmentorderline
        ON warehouseshipments$shipmentorderline_shipment.warehouseshipments$shipmentorderlineid =
           warehouseshipments$shipmentorderline.id
      JOIN warehouseshipments$shipmentorderline_purchaseorderline
        ON warehouseshipments$shipmentorderline_purchaseorderline.warehouseshipments$shipmentorderlineid =
           warehouseshipments$shipmentorderline.id
      JOIN purchaseorders$purchaseorderline
        ON warehouseshipments$shipmentorderline_purchaseorderline.purchaseorders$purchaseorderlineid =
           purchaseorders$purchaseorderline.id
      JOIN purchaseorders$purchaseorderline_stockitem
        ON purchaseorders$purchaseorderline_stockitem.purchaseorders$purchaseorderlineid =
           purchaseorders$purchaseorderline.id
      JOIN product$stockitem
        ON product$stockitem.id = purchaseorders$purchaseorderline_stockitem.product$stockitemid
    WHERE warehouseshipments$shipment.status = 'Stock_Registered' AND
          warehouseshipments$shipmentorderline.quantityreceived > 0

    UNION ALL

    -- ADJUST
    SELECT
      'adjustment'                                            AS transaction,
      coalesce(inventory$batchstockmovement.createddate,
               inventory$warehousestockitembatch.createddate) + interval '1 hour' AS datetime,
      shortname                                               AS warehouse,
      sku                                                     AS sku,

      CASE
      WHEN batchstockmovementtype = 'Manual_Negative' THEN -1
      WHEN batchstockmovementtype = 'Manual_Positive' THEN 1

      --- movetype	movementtype	fromstate	tostate	stockadjustment

      WHEN movetype = 'SREG' AND movementtype =	'SREG' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1

      WHEN movetype = 'SADJ' AND movementtype = 'ERPADJ'  AND fromstate = 'Z' and tostate =	'W'	THEN  1
      WHEN movetype = 'SADJ' AND movementtype =	'ERPADJ'  AND fromstate = 'W' and tostate =	'Z'	THEN -1
      WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'Z' and tostate =	'W'	THEN  1
      WHEN movetype = 'SADJ' AND movementtype = 'SADJ' 	  AND fromstate = 'W' and tostate =	'Z'	THEN -1

      WHEN movetype = 'SDISS' AND movementtype = 'SDISS' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
      WHEN movetype = 'SDISS' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

      WHEN movetype = 'SDISI' AND movementtype = 'SDISI' 	AND fromstate = 'W' and tostate =	'Z'	THEN -1
      WHEN movetype = 'SDISI' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'Z'	THEN -1

      WHEN movetype = 'SREL' AND movementtype = 'SREL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
      WHEN movetype = 'SHOL' AND movementtype = 'ERPADJ'  AND fromstate = 'W' and tostate =	'W'	THEN  0
      WHEN movetype = 'SHOL' AND movementtype = 'SHOL' 	  AND fromstate = 'W' and tostate =	'W'	THEN  0
      WHEN movetype = 'ERP Batch' AND movementtype = 'ERPADJ' AND fromstate = 'W' and tostate =	'W'	THEN 0

      ELSE 0
      END *
      quantity
                                                              AS units,
      NULL                                                    AS unitcost,
      NULL                                                    AS currency,
      NULL                                                    AS day_rate,
      NULL                                                    AS eurate,
      NULL                                                    AS supplier,
      NULL                                                    AS suppliertype,
      NULL :: VARCHAR                                         AS document_number,
      NULL :: VARCHAR                                         AS order_status

    FROM inventory$batchstockmovement
      JOIN inventory$batchstockmovement_warehousestockitembatch
        ON inventory$batchstockmovement_warehousestockitembatch.inventory$batchstockmovementid =
           inventory$batchstockmovement.id
      JOIN inventory$warehousestockitembatch
        ON inventory$batchstockmovement_warehousestockitembatch.inventory$warehousestockitembatchid =
           inventory$warehousestockitembatch.id
      JOIN inventory$warehousestockitembatch_warehousestockitem
        ON inventory$warehousestockitembatch_warehousestockitem.inventory$warehousestockitembatchid =
           inventory$warehousestockitembatch.id
      JOIN inventory$located_at ON inventory$located_at.inventory$warehousestockitemid =
                                   inventory$warehousestockitembatch_warehousestockitem.inventory$warehousestockitemid
      JOIN inventory$warehouse ON inventory$located_at.inventory$warehouseid = inventory$warehouse.id
      LEFT OUTER JOIN inventory$batchstockmovement_stockmovement
        ON inventory$batchstockmovement_stockmovement.inventory$batchstockmovementid = inventory$batchstockmovement.id
      LEFT OUTER JOIN inventory$stockmovement
        ON inventory$batchstockmovement_stockmovement.inventory$stockmovementid = inventory$stockmovement.id
      JOIN inventory$warehousestockitem_stockitem
        ON inventory$warehousestockitem_stockitem.inventory$warehousestockitemid =
           inventory$warehousestockitembatch_warehousestockitem.inventory$warehousestockitemid
      JOIN product$stockitem
        ON inventory$warehousestockitem_stockitem.product$stockitemid = product$stockitem.id
      LEFT OUTER JOIN inventory$warehousestockitembatch_shipment
        ON inventory$warehousestockitembatch_shipment.inventory$warehousestockitembatchid = inventory$warehousestockitembatch.id

    WHERE movementtype NOT IN ('SHOL', 'SHREL')
          -- ignore movements to stock batches (unfortunately it can't be re-traced)
          AND inventory$warehousestockitembatch_shipment.warehouseshipments$shipmentid IS NOT NULL

    ORDER BY datetime ASC, transaction DESC
  ) AS t;

CREATE INDEX alltransactions_idx_datetime
  ON alltransactions (datetime);
CREATE INDEX alltransactions_idx_sku
  ON alltransactions (sku);
CREATE INDEX alltransactions_idx_transaction
  ON alltransactions (transaction);
CREATE INDEX alltransactions_idx_document_number
  ON alltransactions (document_number);
CREATE INDEX alltransactions_idx_orderstatus
  ON alltransactions (order_status);

ALTER TABLE alltransactions ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE alltransactions ADD COLUMN productid VARCHAR(5);

update AllTransactions set productid = substring(sku from 2 for 4);

CREATE INDEX alltransactions_idx_productid
  ON alltransactions (productid);

UPDATE alltransactions as t2
      SET
      day_rate =
      (SELECT t.day_rate FROM alltransactions AS t WHERE
        t.datetime < t2.datetime AND t.currency = t2.currency AND day_rate IS NOT NULL
        ORDER BY t.datetime DESC
      LIMIT 1)
WHERE t2.transaction = 'receipt' AND t2.day_rate IS NULL;

UPDATE alltransactions as t2
      SET
      eurate =
      (SELECT t.eurate FROM alltransactions AS t WHERE
        t.datetime < t2.datetime AND t.eurate IS NOT NULL
        ORDER BY t.datetime DESC
      LIMIT 1)
WHERE t2.transaction = 'receipt' AND t2.eurate IS NULL;

drop table if exists suppliermap;

create table suppliermap (
  invoice_supplier varchar,
  receipt_supplier varchar,
  cost_name varchar
);

INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Abatro001','Abatron', 'Abatron');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Advanc001','AdvanceEyeCare', 'AdvanceEyeCare');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Alcon 002','Alcon UK', 'Alcon UK');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Alcon 003','Alcon NL', 'Alcon NL');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Alensa001','ALENSA s.r.o. ', 'ALENSA');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Alloga001','Alloga/UniDrug', 'Alloga');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('AMEX BV INTERCO AP','', 'company');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Amex O001','', 'company');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Amex O002','', 'company');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Amex O003','', 'company');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('AMO (U001','AMO Abott', 'Amo');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Bankop001','Bankoptica', 'Bankoptica');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Bausch001','B&L NL', 'B&L NL');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Bausch002','B&L UK', 'B&L UK');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Brande001','BrandedItemsLtd', 'BrandedItemsLtd');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Brande002','Branded Items LTD USD', 'Branded Items LTD USD');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Cantor001','', 'Cantor');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Cooper001','CooperVision NL', 'CooperVision NL');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Cooper003','CooperVision ES', 'CooperVision ES');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Cooper007','CooperVision UK', 'CooperVision UK');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Cooper008','CooperVision FR', 'CooperVision FR');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Coopervision Ltd   (122373001 )- do not use','', 'Coopervision Ltd   (122373001 )- do not use');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('David 001','David Thomas', 'David Thomas');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('East M001','EMP Pharma', 'EMP Pharma');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Empori001','', 'Empori');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Ercon 002','Ercon', 'Ercon');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Ercon 003','Ercon', 'Ercon');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Errevi001','', 'Errevi');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Essilor001','Essilor International ', 'Essilor');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Eye-Co001','Eye-Con', 'Eye-Con');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Fabris001','Fabris Lane', 'Fabris Lane');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('FineVi001','Fine Vision', 'Fine Vision');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Genera001','GTS', 'GTS');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Genera002','GTS', 'GTS');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Genesi001','Genesis', 'Genesis');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Genesi002','Genesis', 'Genesis');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Haicha001','Haichang CLs', 'Haichang CLs');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Horizo 001','', 'Horizon');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('IBS S.001','IBS', 'IBS');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('I-Care001','iCare Vision', 'iCare Vision');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Impor 001','', 'Impor');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Infar 001','Infar EUR', 'Infar EUR');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Infar International- USD- Amex-do not use','Infar USD', 'Infar USD');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('InfarN003','Infar USD', 'Infar USD');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('I See 001','I See Vision', 'I See Vision');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Johnso001','J&J UK', 'J&J UK');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Johnso002','J&J NL', 'J&J NL');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Johnso003','J&J FR', 'J&J FR');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Jorinis001','Jorinis', 'Jorinis');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Karawa001','Karawane Handel GmbH', 'Karawane Handel GmbH');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Kelabi001','Kelabit', 'Kelabit');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('LensL001','LensLogistics', 'LensLogistics');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Mawdsl001','Mawdsleys PreW', 'Mawdsleys PreW');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('MediaLen001','Medialens', 'Medialens');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Midopt001','Mid Optic', 'Mid Optic');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('MyGrup001','MyGrupLtd', 'MyGrupLtd');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Olympus001','', 'Olympus');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Ophtal001','', 'Opthal');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Optica001','Optical Finance', 'Optical Finance');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Optika001','Optikal', 'Optikal');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Oxbrid001','Oxbridge', 'Oxbridge');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Oxbrid002','Oxbridge', 'Oxbridge');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Pie Op002','Pogrande', 'Pogrande');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Royal 002','', 'Royal');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Shotley001','Shotley Limited', 'Shotley Limited');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Szkla.001','Szkla', 'Szkla');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Ta-To Contacts Co. Ltd','Ta-To Contacts Co. Ltd', 'Ta-To Contacts Co. Ltd');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('TeamJ001','RectoLennox', 'RectoLennox');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('TradeT003','', 'TradeT');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Visco 002','Visco Vision Inc.', 'Visco Vision Inc.');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Vision002','', 'company');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Vision003','', 'company');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Vision Direct Europe Ltd - INTERCO AP','', 'company');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('VisionW001','Vision World', 'Vision World');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Winsto001','Winston', 'Winston');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('Winston Optical-USD-Amex','Winston USD', 'Winston USD');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('LensL001','J&J SV', 'LensLogistics');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('','CooperVision UK (Stock)', 'CooperVision UK (Stock)');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('','VD York', 'company');
INSERT INTO suppliermap (invoice_supplier,receipt_supplier, cost_name) VALUES ('','VD Amsterdam', 'company');

create index suppliermap_idx_invoice on suppliermap (invoice_supplier);
create index suppliermap_idx_receipt on suppliermap (receipt_supplier);
create index suppliermap_idx_name on suppliermap (cost_name);

--ALTER TABLE alltransactions ADD COLUMN in_id integer;
--ALTER TABLE alltransactions ADD COLUMN in_units integer;
--ALTER TABLE alltransactions ADD COLUMN out_id integer;
--ALTER TABLE alltransactions ADD COLUMN out_units integer;
--ALTER TABLE alltransactions ADD COLUMN out_err_units integer;