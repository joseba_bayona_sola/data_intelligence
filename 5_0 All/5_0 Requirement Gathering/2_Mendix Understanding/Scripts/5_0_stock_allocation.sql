﻿
-- Order Lines Stock Allocation
select id, 
	transactionID, timestamp,
	allocationType, recordType, 
	allocatedUnits, allocatedStockQuantity, issuedQuantity, packSize, 
	stockAllocated, cancelled, 
	issuedDateTime
from stockallocation$orderlinestockallocationtransaction
limit 1000;

-- Magento Allocation
select id, 
	allocationFull, shipped, cancelled, 
	quantity
from stockallocation$magentoallocation
limit 1000;

-- Magento BOM Allocation
select id, 
	allocationFull, shipped, cancelled
from stockallocation$magentobomallocation;

-- Manual Allocation
select id, packQuantity
from stockallocation$manualallocation;

-- Missing Product
select id, 
	count, OK, 
	createdDate, changedDate
from stockallocation$missingproduct;

---------------------------------------------------------------------------------

	select *
	from stockallocatio$magentoallocatio_orderlinestockallocationtransac
	limit 1000;
	
	select *
	from stockallocation$parentallocation_childallocation
	limit 1000;
	
	select *
	from stockallocatio$orderlinestockallocationtransa_magentobomallocat;

---------------------------------------------------------------------------------

	select stockallocation$orderLineStockAllocationTransactionID, inventory$warehouseID
	from stockallocation$at_warehouse
	limit 1000;
	
	select stockallocation$orderLineStockAllocationTransactionID, inventory$warehouseStockItemID
	from stockallocation$at_warehousestockitem
	limit 1000;

	select stockallocation$orderLineStockAllocationTransactionID, inventory$warehouseStockItemBatchID
	from stockallocation$at_batch
	limit 1000;

	select stockallocation$orderLineStockAllocationTransactionID, product$packSizeID
	from stockallocation$at_packsize
	limit 1000;

	select stockallocation$orderLineStockAllocationTransactionID, orderprocessing$orderID
	from stockallocation$at_order
	limit 1000;

	select stockallocation$orderLineStockAllocationTransactionID, orderprocessing$orderLineID
	from stockallocation$at_orderline
	limit 1000;

	select stockallocation$orderLineStockAllocationTransactionID, customershipments$customerShipmentLineID
	from stockallocatio$orderlinestockallocationtransa_customershipmentl
	limit 1000;




	select stockallocation$magentoAllocationID, inventory$warehouseID
	from stockallocation$magentoallocation_warehouse
	limit 1000;

	select stockallocation$magentoAllocationID, orderprocessing$orderLineMagentoID
	from stockallocation$magentoallocation_orderlinemagento
	limit 1000;

	select stockallocation$magentoAllocationID, orderprocessing$shippingGroupID
	from stockallocation$magentoallocation_shippinggroup
	limit 1000;



	select stockallocation$magentoBOMAllocationID, inventory$warehouseID
	from stockallocation$magentobomallocation_warehouse;

	select stockallocation$magentoBOMAllocationID, orderprocessing$orderLineMagentoID
	from stockallocation$magentobomallocation_orderlinemagento;

	select stockallocation$magentoBOMAllocationID, orderprocessing$shippingGroupID
	from stockallocation$magentobomallocation_shippinggroup;

	select stockallocation$magentoBOMAllocationID, orderprocessing$bomID
	from stockallocation$magentobomallocation_bom;



	select *
	from stockallocation$manualallocation_orderlineshortage;

	select *
	from stockallocation$manualallocation_warehousestockitem;

	select *
	from stockallocation$manualallocation_order;



	select stockAllocation$missingProductID, inventory$warehouseID
	from stockallocation$missingproduct_warehouse;

	select stockAllocation$missingProductID, product$productID
	from stockallocation$missingproduct_product;

	select stockAllocation$missingProductID, product$packSizeID
	from stockallocation$missingproduct_packsize;


