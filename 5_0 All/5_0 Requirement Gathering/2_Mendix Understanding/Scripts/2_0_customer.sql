﻿
-- Customer Entity
select id, 
	customerid, subMetaObjectName
from customer$customerentity
limit 1000;

-- Customer - Retail
select id, 
	email, phoneNumber, 
	prefix, suffix, firstName, lastName, middleName, 
	taxvat
from customer$retailcustomer
limit 1000;

-- Address
select id, 
	companyName, email, telephone, 
	customerTitle, customerFirstName, customerSurname, customerMiddleName, customerSuffix, 
	addressID, addressType, 
	street1, street2, street3, 
	postcode, city, regionID, region, countryID, 
	isnew, defaultDeliveryAddress,
	createdDate, changedDate, 
	system$owner, system$changedby
from customer$address;

-- Customer - Wholesale
select id, name, _type
from customer$wholesalecustomer
order by name;

-- Customer Contact
select id, 
	prefix, firstName, middleName, lastName, suffix, 
	role, --department, 
	email, telephone, phoneNumber, mobile, 
	salutation, comment
from customer$contact;

-- Customer Wholesale Order Header
select id, customerRef, dueDate
from customer$wholesaleorderheader
order by dueDate;

-- Customer Wholesale Prices
select id, 
	netPrice, formattedPrice, moq,
	active, activePrice,
	created, validFromDate, validToDate, 
	createdDate, changedDate, 
	system$owner, system$changedBy
from customer$wholesaleprice
order by created, netprice;

-------------------------------------------------------------------------------------

	select *
	from customer$address_customerentity;
	
	select *
	from customer$orderaddress_customer;
	
	select *
	from customer$contact_wholesalecustomer;
	
	select *
	from customer$wholesaleorderheader_order;
	
	select *
	from customer$wholesaleorderdocuments_customer;

	select *
	from customer$wholesalepricelist;
	
	select *
	from customer$wholesaleproductfamilies;
	
	select *
	from customer$wholesalepacksizes


-------------------------------------------------------------------------------------

	select *
	from customer$customer_legacycompany;
	
	select *
	from customer$address_country;
	
	select *
	from customer$wholesalecustomer_warehousecustomer;
	
	select *
	from customer$wholesalecustomer_currency;
	
	select *
	from customer$wholesalecustomer_supplyingwarehouse;
	
	select *
	from customer$wholesaleaddresses_deprecated;
	
	select *
	from customer$wholesaleorderdocument_template;
