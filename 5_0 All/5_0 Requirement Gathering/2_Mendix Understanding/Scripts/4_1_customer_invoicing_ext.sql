﻿
-- Customer Invoice
select status, count(*)
from customerinvoicing$customerinvoice
group by status
order by status

select ci.id, 
	cu.*,
	ci.invoiceNumber, ci.status, ci.invoiceDate, 
	ci.netTotalGoods, ci.VAT, ci.grossTotal, ci.netCarriage,
	ci.numPacks, 
	ci.countryISO2, ci.currency,
	ci.customerVATReg,
	ci.createdDate
from 
		customerinvoicing$customerinvoice ci
	inner join
		(select cu.*, cicu.customerinvoicing$customerInvoiceID
		from
				(select customerinvoicing$customerInvoiceID, customer$customerEntityId
				from customerinvoicing$customerinvoice_customerentity) cicu
			left join
				(select ce.id, ce.customerid, ce.subMetaObjectName,
					wc.name, wc._type
				from 
						(select id, customerid, subMetaObjectName
						from customer$customerentity) ce
					inner join
						(select id, name, _type
						from customer$wholesalecustomer) wc on ce.id = wc.id) cu on cicu.customer$customerEntityId = cu.id) cu 
							on ci.id = cu.customerinvoicing$customerInvoiceID
order by invoiceDate desc;

select cu.*, cicu.customerinvoicing$customerInvoiceID
from
		(select customerinvoicing$customerInvoiceID, customer$customerEntityId
		from customerinvoicing$customerinvoice_customerentity) cicu
	left join
		(select ce.id, ce.customerid, ce.subMetaObjectName,
			rc.email, rc.phoneNumber, rc.prefix, rc.suffix, rc.firstName, rc.lastName, rc.middleName, rc.taxvat
		from 
				(select id, customerid, subMetaObjectName
				from customer$customerentity) ce
			inner join
				(select id, 
					email, phoneNumber, 
					prefix, suffix, firstName, lastName, middleName, 
					taxvat
				from customer$retailcustomer) rc on ce.id = rc.id) cu on cicu.customer$customerEntityId = cu.id

select cu.*, cicu.customerinvoicing$customerInvoiceID
from
		(select customerinvoicing$customerInvoiceID, customer$customerEntityId
		from customerinvoicing$customerinvoice_customerentity) cicu
	left join
		(select ce.id, ce.customerid, ce.subMetaObjectName,
			wc.name, wc._type
		from 
				(select id, customerid, subMetaObjectName
				from customer$customerentity) ce
			inner join
				(select id, name, _type
				from customer$wholesalecustomer) wc on ce.id = wc.id) cu on cicu.customer$customerEntityId = cu.id


-- Customer Invoice Line - Carriage
select description, count(*)
from customerinvoicing$customerinvoiceline
where sku is null 
group by description
order by description

select ci.id, 
	cu.*,
	ci.invoiceNumber, ci.status, ci.invoiceDate, 
	ci.netTotalGoods, ci.VAT, ci.grossTotal, ci.netCarriage,
	ci.numPacks, 
	ci.countryISO2, ci.currency,
	ci.customerVATReg,
	ci.createdDate, 
	cil.*
from 
		customerinvoicing$customerinvoice ci
	inner join
		(select cu.*, cicu.customerinvoicing$customerInvoiceID
		from
				(select customerinvoicing$customerInvoiceID, customer$customerEntityId
				from customerinvoicing$customerinvoice_customerentity) cicu
			left join
				(select ce.id, ce.customerid, ce.subMetaObjectName,
					wc.name, wc._type
				from 
						(select id, customerid, subMetaObjectName
						from customer$customerentity) ce
					inner join
						(select id, name, _type
						from customer$wholesalecustomer) wc on ce.id = wc.id) cu on cicu.customer$customerEntityId = cu.id) cu 
							on ci.id = cu.customerinvoicing$customerInvoiceID
	inner join
		(select cil.*, cicil.customerInvoicing$customerInvoiceID
		from
				(select customerinvoicing$customerInvoiceLineID, customerInvoicing$customerInvoiceID
				from customerinvoicing$customerinvoice_customerinvoicecarriage) cicil
			inner join
				(select id, 
					SKU, productSKU, description, reference,
					packQuantity, formattedPackQty, packPrice, VATRate, formattedVATRate, 
					netPrice, VATAmount, totalPrice, formattedTotalPrice, 
					createdDate 
				from customerinvoicing$customerinvoiceline) cil on cicil.customerinvoicing$customerInvoiceLineID = cil.id) cil on ci.id = cil.customerInvoicing$customerInvoiceID

where ci.id in (97671816918601739, 97671816918601740)
order by invoiceDate desc;

-- Customer Shipment Invoice
select ci.id, 
	cu.*,
	ci.invoiceNumber, ci.status, ci.invoiceDate, 
	ci.netTotalGoods, ci.VAT, ci.grossTotal, ci.netCarriage,
	ci.numPacks, 
	ci.countryISO2, ci.currency,
	ci.customerVATReg,
	ci.createdDate, 
	cis.*
from 
		customerinvoicing$customerinvoice ci
	inner join
		(select cu.*, cicu.customerinvoicing$customerInvoiceID
		from
				(select customerinvoicing$customerInvoiceID, customer$customerEntityId
				from customerinvoicing$customerinvoice_customerentity) cicu
			left join
				(select ce.id, ce.customerid, ce.subMetaObjectName,
					wc.name, wc._type
				from 
						(select id, customerid, subMetaObjectName
						from customer$customerentity) ce
					inner join
						(select id, name, _type
						from customer$wholesalecustomer) wc on ce.id = wc.id) cu on cicu.customer$customerEntityId = cu.id) cu 
							on ci.id = cu.customerinvoicing$customerInvoiceID
	inner join
		(select cis.*, cisci.customerinvoicing$customerInvoiceID
		from
				(select customerinvoicing$customerShipmentInvoiceID, customerinvoicing$customerInvoiceID
				from customerinvoicing$customershipmentinvoice_customerinvoice) cisci
			inner join
				(select id, 
					shipmentNumber, orderNumber, 
					dispatchDate, 
					warehouse, address,
					countryISO2, currency, 
					numPacks, netTotalGoods, netCarriage, netTotal, VAT, grossTotal, excluded
				from customerinvoicing$customershipmentinvoice) cis on cisci.customerinvoicing$customerShipmentInvoiceID = cis.id) cis 
					on ci.id = cis.customerinvoicing$customerInvoiceID
where ci.id in (97671816918601739, 97671816918601740)
order by invoiceDate desc;

select cis.*, cisci.customerinvoicing$customerInvoiceID
from
		(select customerinvoicing$customerShipmentInvoiceID, customerinvoicing$customerInvoiceID
		from customerinvoicing$customershipmentinvoice_customerinvoice) cisci
	right join
		(select id, 
			shipmentNumber, orderNumber, 
			dispatchDate, 
			warehouse, address,
			countryISO2, currency, 
			numPacks, netTotalGoods, netCarriage, netTotal, VAT, grossTotal, excluded
		from customerinvoicing$customershipmentinvoice) cis on cisci.customerinvoicing$customerShipmentInvoiceID = cis.id

-- Customer Invoice Line 
select ci.id, 
	cu.*,
	ci.invoiceNumber, ci.status, ci.invoiceDate, 
	ci.netTotalGoods, ci.VAT, ci.grossTotal, ci.netCarriage,
	ci.numPacks, 
	ci.countryISO2, ci.currency,
	ci.customerVATReg,
	ci.createdDate, 
	cis.*, cil.*
from 
		customerinvoicing$customerinvoice ci
	inner join
		(select cu.*, cicu.customerinvoicing$customerInvoiceID
		from
				(select customerinvoicing$customerInvoiceID, customer$customerEntityId
				from customerinvoicing$customerinvoice_customerentity) cicu
			left join
				(select ce.id, ce.customerid, ce.subMetaObjectName,
					wc.name, wc._type
				from 
						(select id, customerid, subMetaObjectName
						from customer$customerentity) ce
					inner join
						(select id, name, _type
						from customer$wholesalecustomer) wc on ce.id = wc.id) cu on cicu.customer$customerEntityId = cu.id) cu 
							on ci.id = cu.customerinvoicing$customerInvoiceID
	inner join
		(select cis.*, cisci.customerinvoicing$customerInvoiceID
		from
				(select customerinvoicing$customerShipmentInvoiceID, customerinvoicing$customerInvoiceID
				from customerinvoicing$customershipmentinvoice_customerinvoice) cisci
			inner join
				(select id, 
					shipmentNumber, orderNumber, 
					dispatchDate, 
					warehouse, address,
					countryISO2, currency, 
					numPacks, netTotalGoods, netCarriage, netTotal, VAT, grossTotal, excluded
				from customerinvoicing$customershipmentinvoice) cis on cisci.customerinvoicing$customerShipmentInvoiceID = cis.id) cis 
					on ci.id = cis.customerinvoicing$customerInvoiceID
	inner join
		(select cil.*, customerinvoicing$customerShipmentInvoiceID
		from 
				(select customerinvoicing$customerInvoiceLineID, customerinvoicing$customerShipmentInvoiceID
				from customerinvoicing$customerinvoiceline_customershipmentinvoice) cilcis
			inner join
				(select id, 
					SKU, productSKU, description, reference,
					packQuantity, formattedPackQty, packPrice, VATRate, formattedVATRate, 
					netPrice, VATAmount, totalPrice, formattedTotalPrice, 
					createdDate 
				from customerinvoicing$customerinvoiceline) cil on cilcis.customerinvoicing$customerInvoiceLineID = cil.id) cil 
					on cis.id = cil.customerinvoicing$customerShipmentInvoiceID	
where ci.id in (97671816918601739, 97671816918601740)
order by invoiceDate desc, cis.shipmentNumber, cil.sku;


