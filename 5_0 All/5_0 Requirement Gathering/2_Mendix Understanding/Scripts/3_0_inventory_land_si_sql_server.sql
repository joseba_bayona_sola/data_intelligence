
select id, 
	id_string, 
	availableqty, actualstockqty, allocatedstockqty, forwarddemand, dueinqty, stocked, 
	outstandingallocation, onhold,
	stockingmethod, 
	changed
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$warehousestockitem" where id_string like ''01083B2D4CP%'' limit 1000')
order by id_string

	select inventory$warehousestockitemid, inventory$warehouseid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$located_at" limit 1000')

	select inventory$warehousestockitemid, product$stockitemid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$warehousestockitem_stockitem" limit 1000')

select id, batch_id,
	fullyallocated, fullyIssued, status, arriveddate,  
	receivedquantity, allocatedquantity, issuedquantity, onHoldQuantity, disposedQuantity, forwardDemand, negativeFreeToSell, registeredQuantity,
	groupFIFODate, confirmedDate, stockRegisteredDate, receiptCreatedDate, 
	productUnitCost, carriageUnitCost, inteCoCarriageUnitCost, dutyUnitCost, totalUnitCost, totalUnitCostIncInterCo, interCoProfitUnitCost, 
	exchangeRate, currency,

	createdDate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$warehousestockitembatch" where batch_id = 828544 limit 1000')

	select inventory$warehousestockitembatchid, inventory$warehousestockitemid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$warehousestockitembatch_warehousestockitem" limit 1000')

select id, 
	issueid, createddate, issuedquantity, 
	cancelled, shippeddate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$batchstockissue" where issueid = 2274913 limit 1000')

	select inventory$batchstockissueid, inventory$warehousestockitembatchid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$batchstockissue_warehousestockitembatch" limit 1000')

select id, 
	fullyissued, cancelled, allocatedquantity, allocatedunits, issuedquantity, 
	isnew,
	createddate, changeddate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$batchstockallocation" limit 1000')

	select inventory$batchstockissueid, inventory$batchstockallocationid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$batchstockissue_batchstockallocation" limit 1000')

	select inventory$batchstockallocationid, stockallocation$orderlinestockallocationtransactionid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$batchtransaction_orderlinestockallocationtransaction" limit 1000')

------------------------------------------------------------------------

select id, 
	batchtransaction_id, reference, date,
	oldallocation, newallocation, oldissue, newissue, processed
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$repairstockbatchs" limit 1000')

	select inventory$repairstockbatchsid, inventory$warehousestockitembatchid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$repairstockbatchs_warehousestockitembatch" limit 1000')








------------------------------------------------------------------------


select id, 
	moveid, movelineid, 
	stockadjustment, movementtype, movetype, _class, reasonid, status, 
	skuid, qtyactioned, 
	fromstate, tostate, 
	fromstatus, tostatus, 
	operator, supervisor,
	dateCreated, dateClosed
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$stockmovement" where moveid = ''SRG0015883'' limit 1000')


	select inventory$stockmovementid, inventory$warehousestockitemid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$stockmovement_warehousestockitem" limit 1000')

------------------------------------------------------------------------

select id, batchstockmovement_id, 
	quantity, comment
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$batchstockmovement" limit 1000')

	select inventory$batchstockmovementid, inventory$stockmovementid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$batchstockmovement_stockmovement" limit 1000')

	select inventory$batchstockmovementid, inventory$warehousestockitembatchid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$batchstockmovement_warehousestockitembatch" limit 1000')
