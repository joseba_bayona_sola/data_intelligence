﻿
-- Sequence 
select id, 
	sequenceType, sequenceName, latestAssignedNumber, identifier,
	createdDate, changedDate, 
	system$owner, system$changedby
from orderprocessing$sequence
order by sequenceName;

-- Order Status Update 
select id, status, timestamp 
from orderprocessing$order_statusupdate
limit 1000;

	select status, count(*)
	from orderprocessing$order_statusupdate
	group by status
	order by status;

-- Order Processing Transaction 
select id, 
	orderReference, status, detail, messageBody,
	timestamp 
from orderprocessing$orderprocessingtransaction
limit 1000;

	select status, detail, count(*)
	from orderprocessing$orderprocessingtransaction
	group by status, detail
	order by status, detail;
	
-- Order 
select id, 
	magentoOrderID, incrementID, orderLinesMagento, orderLinesDeduped,
	_type, status, orderStatus, shipmentStatus, statusUpdated, 
	customerOrderReference, addressCanBeUpdated,
	couponCode,
	shippingMethod, shippingDescription, paymentMethod, 
	automatic_reorder,
	allocationStrategy, allocationRunTimeStatus, allocationStatus, notAllocated, partialAllocated, fullAllocated,
	shipmentGroups ,notShipped, partialShipped, fullShipped, 
	labelRenderer,
	totalNetPrice, totalPacksOrdered,
	orderCurrencyCode, globalCurrencyCode,
	transferOrder, onHold, processingFailed, intersite, 
	promiseDate,  promisedDeliveryDate, promisedShippingDate, 
	allocatedDate, allocationDueDate, daysBeforeDueDateForAllocation, dueDate, 
	updatedAt,
	createdDate, --changedDate, 
	system$owner, system$changedby
from orderprocessing$order
limit 1000;

-- Base Values 
select id, 
	subtotal, subtotalCanceled, subtotalInvoiced, subtotalRefunded, 
	discountAmount, discountCanceled, discountInvoiced, discountRefunded, 
	shippingAmount, shippingCanceled, shippingInvoiced, shippingRefunded, 
	totalCanceled, totalInvoiced, totalRefunded, 
	adjustmentNegative, adjustmentPositive, 
	subtotalInclTax,
	custBalanceAmount, customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
	grandTotal, 
	toOrderRate, toGlobalRate, currencyCode
from orderprocessing$basevalues
limit 1000;

-- Order Line Magento 
select id, 
	lineAdded, deduplicate, fulfilled
from orderprocessing$orderlinemagento
limit 1000;

-- Order Line
select id, 
	status, 
	packsOrdered, packsAllocated, packsShipped
from orderprocessing$orderline
limit 1000;

-- BOM
select id, specified, instantiated 
from orderprocessing$bom;

-- Wholesales Order Line Import
select id, 
	barcode, qty, 
	reference, 
	matchFound, matching, 
	lineNumber
from orderprocessing$wholesaleorderlineimport;

-- Order Address
select id, 
	addressID, addressType, 
	customerTitle, customerFirstName, customerMiddleName, customerSurname, customerSuffix, fullName,
	companyName, 
	street1, street2, street3, 
	postCode, city, regionID, region, countryID, 
	email, Telephone, 
	deliveryInstructions, 
	createdDate, changedDate, 
	system$owner, system$changedBy
from orderprocessing$orderaddress
limit 1000;

-- Shipping Group 
select id, 
	dispensingFee, 
	allocationStatus, shipmentStatus, 
	wholesale, 
	letterBoxAble, onHold, adHoc
from orderprocessing$shippinggroup
limit 1000;

-- Order Line Abstract
select id, 
	magentoItemID, orderLineType, subMetaObjectName,
	sku, eye, lensGroupID, 
	basePrice, baseRowPrice, 
	quantityOrdered, quantityInvoiced, quantityRefunded, quantityShipped, quantityAllocated, quantityIssued,  
	netPrice, netRowPrice,
	allocated, 
	createdDate, changedDate, 
	system$owner, system$changedBy
from orderprocessing$orderlineabstract
limit 1000;

-- Option Set
select id, optionSetName 
from orderprocessing$optionset;

-- Option
select id, 
	sortOrder, attributeName, attributeType,
	valueBoolean, valueDateTime, valueDecimal, valueInteger, valueString,
	mandatory, displayOnPickList, displayOnPackingSlip 
from orderprocessing$option;

----------------------------------------------------------------------------------------------

	select *
	from orderprocessing$statusupdate_order
	limit 1000;
	
	select *
	from orderprocessing$orderprocessingtransaction_order
	limit 1000;
	
	select *
	from orderprocessing$basevalues_order
	limit 1000;
	
	select *
	from orderprocessing$orderlinemagento_order
	limit 1000;
	
	select *
	from orderprocessing$orderline_order
	limit 1000;
	
	select *
	from orderprocessing$orderlinemagento_orderline
	limit 1000;
	
	select *
	from orderprocessing$bom_parent;
	
	select *
	from orderprocessing$bom_child;
	
	select *
	from orderprocessing$wholesaleorderlines;
	
	select *
	from orderprocessing$address_order
	limit 1000;
	
	select *
	from orderprocessing$shippinggroup_order
	limit 1000;
	
	select *
	from orderprocessing$orderlineabstract_shippinggroup
	limit 1000;
	
	select *
	from orderprocessing$orderlineabstract_optionset;
	
	select *
	from orderprocessing$option_optionset;

----------------------------------------------------------------------------------------------

	select *
	from orderprocessing$orderprocessingtransaction_queuemessage
	limit 1000;
	
	select *
	from orderprocessing$order_customer
	limit 1000;
	
	select *
	from orderprocessing$order_magwebstore
	limit 1000;
	
	select *
	from orderprocessing$order_consignmenttransaction;
	
	select *
	from orderprocessing$wholesalecustomerorders;
	
	select *
	from orderprocessing$order_transferheader;
	
	select *
	from orderprocessing$order_forcedwarehouse;
	
	select *
	from orderprocessing$order_createdby;

----------------------------------------------------------------------------------------------
		
	select *
	from orderprocessing$wholesaleorderheader_lines;
		
	select *
	from orderprocessing$wholesaleorderlineprices;
		
	select *
	from orderprocessing$wholesaleorderlineimport_stockitem;
		
	select *
	from orderprocessing$wholesaleorderlineimport_wholesalecustomer;
		
	select *
	from orderprocessing$orderaddress_country
	limit 1000;
		
	select *
	from orderprocessing$shippinggroup_warehouse
	limit 1000;
		
	select *
	from orderprocessing$orderline_product
	limit 1000;
		
	select *
	from orderprocessing$orderline_requiredstockitem
	limit 1000;
		
	select *
	from orderprocessing$optionset_optionset;
