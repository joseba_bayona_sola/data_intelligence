﻿
-- Customer Shipment
select id, 
	shipmentID, shipmentNumber, orderIncrementID, partitionID0_120,
	status, statusChange, 

	shippingMethod, shippingDescription, paymentMethod, 
	warehouseMethod, 
	fullyShipped, 
	labelRenderer, 
	shipmentValue, shippingTotal, wholeSaleCarriage, toFollowValue, 
	notify, syncedToMagento, preHoldStage, preHoldStatus, hold, processingFailed, intersite, 
	year, month, 
	dispatchDate, expectedDeliveryDate, expectedShippingDate, confirmedShippingDate, 
	createdDate
from customershipments$customershipment
limit 1000;

-- Shipment Transaction
select id, 
	shipmentTransactionID, sent, timestamp
from customershipments$shipmenttransaction
limit 1000;

-- Consignment
select id, 
	identifier, order_number, 
	warehouse_id, 
	consignment_number, 
	shipping_method, service_id, carrier, service, 
	delivery_instructions, tracking_url, 
	order_value, currency,
	create_date
from customershipments$consignment
limit 1000;

-- Package Detail
select id, 
	tracking_number, description, 
	width, length, height
from customershipments$packagedetail
limit 1000;

-- Lifecycle Update
select id, status, timestamp
from customershipments$lifecycle_update
limit 1000;

-- API Updates
select id, 
	service, status, operation, 
	timestamp, 
	detail, body
from customershipments$api_updates
limit 1000;

-- Shipment Billing Address
select id, 
	addressID,
	fullName, companyName, 
	street1, street2, street3, 
	postCode, city, region, countryID
	telephone
from customershipments$shipmentbillingaddress
limit 1000;

-- Shipment Delivery Address
select id, 
	addressID,
	fullName, companyName, 
	street1, street2, street3, 
	postCode, city, region, countryID
	telephone
from customershipments$shipmentdeliveryaddress
limit 1000;

-- Customer Shipment Line
select id, 
	shipmentLineID, lineNumber, status, variableBreakdown, 
	stockItemID, productName, productDescription, eye, 
	quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
	packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
	price, priceFormatted, 
	subTotal, subTotal_formatted, 
	VATRate, VAT, VAT_formatted,
	fullyShipped,  
	BOMLine, 
	system$changedBy
from customershipments$customershipmentline
limit 1000;

-- Pickline
select id, pickLineID
from customershipments$pickline
limit 1000;

-- Pickline Update
select id, 
	status, timestamp,
	qtyOrdered, qtyAllocated, qtyTasked, qtyPicked, qtyShipped, qtyDelivered, qtyDueOut
from customershipments$picklineupdate
limit 1000;

-- Dispatch Note
select *
from customershipments$dispatchnote
order by deliveryAddress_Country, shipmentDate;

-- Product Family Summary
select id, 
	identifier, productFamily, 
	totalPackQuantity, totalOrderQuantity
from customershipments$productfamilysummary
order by identifier
limit 1000;

-- Wholesale Dispatch Note
select id, 
	total_orderQuantity, total_packQuantity
from customershipments$wholesaledispatchnote;

-------------------------------------------------------------------------------------

	select *
	from customershipments$shipmenttransaction_customershipment
	limit 1000;
	
	select *
	from customershipments$scurriconsignment_shipment
	limit 1000;
	
	select *
	from customershipments$consignmentpackages
	limit 1000;
	
	select *
	from customershipments$customershipment_progress
	limit 1000;
	
	select *
	from customershipments$api_updates_customershipment
	limit 1000;
	
	select *
	from customershipments$shipmentbillingaddress_customershipment
	limit 1000;
	
	select *
	from customershipments$shipmentdeliveryaddress_customershipment
	limit 1000;
	
	select *
	from customershipments$customershipmentline_customershipment
	limit 1000;
	
	select *
	from customershipments$picklist_customershipment
	limit 1000;
	
	select *
	from customershipments$pickline_shipmentlineupdate
	limit 1000;
	
	select *
	from customershipments$customershipmentline_picklineupdate
	limit 1000;
	
	select *
	from customershipments$dispatchnote_customershipment
	limit 1000;
	
	select *
	from customershipments$customershipmentline_dispatchnote
	limit 1000;
	
	select *
	from customershipments$productfamilysummary_wholesaledispatchnote
	limit 1000;

-------------------------------------------------------------------------------------

	select *
	from customershipments$customershipment_warehouse
	limit 1000;
	
	select *
	from customershipments$customershipment_order
	limit 1000;
	
	select *
	from customershipments$customershipment_label
	limit 1000;
	
	select *
	from customershipments$customershipment_deliveryaddressdonotuse
	limit 1000;
	
	select *
	from customershipments$customershipment_shippinggroup
	limit 1000;
	
	select *
	from customershipments$api_updates_warehouseshipment
	limit 1000;
	
	select *
	from customershipments$api_updates_purchaseorder
	limit 1000;
	
	select *
	from customershipments$shipmentbillingaddress_country
	limit 1000;
	
	select *
	from customershipments$shipmentdeliveryaddress_country
	limit 1000;
	
	select *
	from customershipments$customershipmentline_orderlinemagento
	limit 1000;
	
	select *
	from customershipments$customershipmentline_product
	limit 1000;
	
	select *
	from customershipments$customershipmentline_stockitemforwholesale
	limit 1000;
	
	select *
	from customershipments$pickline_orderlinestockallocationtransaction
	limit 1000;
	
	select *
	from customershipments$genericdownloadable
	limit 1000;
