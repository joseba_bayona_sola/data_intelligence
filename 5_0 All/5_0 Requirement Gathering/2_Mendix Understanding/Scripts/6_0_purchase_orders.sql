﻿
-- Purchase Order
select id, 
	orderNumber, status, source, 

	interCompanyCarriage, inboundCarriage, 
	itemCost, totalPrice, formattedPrice, duty,
	leadTime, 
	dueDate, receivedDate,
	approvedBy, approvedDate, submittedBy, submittedDate, confirmedBy, confirmedDate, completedBy, completedDate, 
	invoicePaymentDate, depositPaidDate, paymentDueDate, exWorksDate, 
	auditComment, supplierReference, EDIStatus, EDIFileButtonVisible, EDIFTPButtonVisible,
	createdDate
from purchaseorders$purchaseorder
order by orderNumber
limit 1000;

select id, 
	orderNumber, status, source, 
	potype, 
	transferleadtime, transitleadtime, 
	spoleadtime, spoduedate,
	tpoleadtime, tpotransferleadtime, tpotransferdate, tpoduedate,	
	requiredshipdate, requiredtransferdate, plannedshipdate, plannedtransferdate,
	invoiceterms, suppliernote, deposit, depositamount,
	estimatedpaymentdate, estimateddepositdate, 	
	createdDate, changedDate, "system$changedby"
from purchaseorders$purchaseorder
order by orderNumber desc
limit 1000;


-- Purchase Order Line Header
select id, 
	headerID, status, problems, 
	lines, items, totalPrice, 
	auditComment,
	createdDate, changedDate, 
	system$owner, system$changedBy
from purchaseorders$purchaseorderlineheader
order by createddate desc
limit 1000;

select id, 
	planneddeliverydate, plannedtransferdate, plannedshipdate, 
	listprice, landedprice, 
	stockregisteredqty, 
	totallistvalue, totalimportdutyvalue, totalunitvalue, totalfreightvalue, totalvatvalue, totalothervalue, totalorderlinevalue	  
from purchaseorders$purchaseorderlineheader
-- where id = 46443371157352327
order by id desc
limit 1000;

 
  

-- Purchase Order Line
select id, 
	SNAPlineID, po_lineID, stockItemID, 
	quantity, quantityUnallocated, quantityOpen, quantityExWorks, quantityPlanned, quantityInTransit, quantityDelivered, quantityReceived, quantityRejected, quantityCancelled, 
	fulfilled, 
	auditComment, progressComment, 
	backTobackDueDate, 
	createdDate, changedDate, 
	system$owner, system$changedBy	
from purchaseorders$purchaseorderline
limit 1000;

select id, 
	listprice, landedprice,
	quantityregistered, quantityinvoiced, quantityarrived, quantitypending, quantityunassigned, quantityshortage, totalcancelledqty,   
	listpricelinevalue, importdutylinevalue, productlinevalue, freightlinevalue, vatlinevalue, otherlinevalue, invoicelinevalue
from purchaseorders$purchaseorderline
order by id desc
limit 1000;


   
   
   
   
   

-- Shipping Terms
select id, description, term
from purchaseorders$shippingterms;

-- Purchase Order Import Run
select id
from purchaseorders$purchaseorderimportrun;

-- Purchase Order Document
select id
from purchaseorders$purchaseorderdocument;

-- Purchase Order Line Import
select id, 
	productCode, itemCode, 
	packSize, quantity
from purchaseorders$purchaseorderlineimport;

-- Purchase Order Line Export
select id, 
	productCode, itemCode, 
	packSize, quantity, 
	orderDate, orderNumber, po_lineRef,
	supplierProductCode, barCode, 
	itemName, 
	bc, di, po, cy, ax, ad, _do, co,
	unitCost 
from purchaseorders$purchaseorderlineexport;



-- Shortage
select id, 
	shortageID, purchaseOrderNumber,
	status, 
	stockItemDescription, 
	unitQuantity, packQuantity, 
	requiredDate, dueInDate,
	wholesale, 
	createdDate, changedDate, 
	system$changedBy
from purchaseorders$shortage
order by purchaseOrderNumber;

-- Shortage Header
select id, wholesale
from purchaseorders$shortageheader;

-- Order Line Shortage
select id, 
	orderLineShortageID, 
	orderLineShortageType, shortageProgress, 
	unitQuantity, packQuantity, 
	advisedDate, dueInDate,
	createdDate, changedDate
from purchaseorders$orderlineshortage;

-- BOM Shortage
select id, 
	BOMShortageID, parentQuantity, 
	createdDate
from purchaseorders$bomshortage
limit 1000;

-- Bulk Progress Update
select id, 
	advisdedDate, progressComment
from purchaseorders$bulkprogressupdate;

------------------------------------------------------------------------------------------------

	select * 
	from purchaseorders$purchaseorderlineheader_purchaseorder
	limit 1000;
	
	select * 
	from purchaseorders$purchaseorderline_purchaseorderlineheader
	limit 1000;

	select * 
	from purchaseorders$purchaseorder_shippingterms;

	select * 
	from purchaseorders$importrun_purchaseorder;

	select * 
	from purchaseorders$purchaseorderlinedocument_purchaseorder;

	select * 
	from purchaseorders$purchaseorderlineimport_purchaseorderlinedocumen;

	select * 
	from purchaseorders$purchaseorderlineexport_purchaseorderdocument;

	select * 
	from purchaseorders$shortage_purchaseorderline;

	select * 
	from purchaseorders$shortage_shortageheader;

	select * 
	from purchaseorders$orderlineshortage_shortage;

	select * 
	from purchaseorders$orderlineshortage_bomshortage;

	select * 
	from purchaseorders$orderlineshortage_bulkprogressupdate;


------------------------------------------------------------------------------------------------

	select *
	from purchaseorders$purchaseorder_warehouse;
	
	select *
	from purchaseorders$purchaseorder_assignedto;
	
	select *
	from purchaseorders$purchaseorder_createdby;
	
	select *
	from purchaseorders$purchaseorder_supplier;
	
	select *
	from purchaseorders$transferpo_transferheader;
	
	select *
	from purchaseorders$supplypo_transferheader;
	
	select *
	from purchaseorders$shortage_warehouseforpurchasing;
	
	select *
	from purchaseorders$purchaseorderlineheader_productfamily;
	
	select *
	from purchaseorders$purchaseorderlineheader_packsize;
	
	select *
	from purchaseorders$purchaseorderlineheader_supplierprices;
	
	select *
	from purchaseorders$purchaseorderlineheader_supplierpricehistory;
	
	select *
	from purchaseorders$purchaseorderline_stockitem
	limit 1000;
	
	select *
	from purchaseorders$shortage_stockitem;
	
	select *
	from purchaseorders$shortage_warehouse;
	
	select *
	from purchaseorders$shortage_supplier;
	
	select *
	from purchaseorders$shortage_standardprice;
	
	select *
	from purchaseorders$shortage_product;
	
	select *
	from purchaseorders$shortage_warehousestockitem;
	
	select *
	from purchaseorders$purchaseorder_warehouseforpurchasing;
	
	select *
	from purchaseorders$shortageheader_supplier;
	
	select *
	from purchaseorders$shortageheader_sourcewarehouse;
	
	select *
	from purchaseorders$shortageheader_destinationwarehouse;
	
	select *
	from purchaseorders$shortageheader_packsize;
	
	select *
	from purchaseorders$shortageheader_wholesalecustomer;
	
	select *
	from purchaseorders$orderlineshortage_orderline;
	
	select *
	from purchaseorders$bomshortage_orderline;
	
	select *
	from purchaseorders$bomshortage_product;
