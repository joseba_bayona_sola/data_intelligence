
-- Product Category
select id, 
	code, name
from openquery(MENDIX_DB_LOCAL, 'select * from "product$productcategory"')
order by code

-- Product Family
select id, 
	magentoProductID, magentoSKU, name, manufacturerProductFamily, status, 
	productFamilyType, 
	promotionalProduct, updateSNAPDescription, createToOrder, 
	createdDate, changedDate, 
	system$owner, system$changedBy 
from openquery(MENDIX_DB_LOCAL, 'select * from "product$productfamily"')
order by magentoProductID

	select product$productfamilyid, product$productcategoryid 
	from openquery(MENDIX_DB_LOCAL, 'select * from "product$productfamily_productcategory"')
		
-- Product 
select id, 
	productType, valid, 
	SKU, oldSKU, description, 
	isBOM, displaySubProductOnPackingSlip,
	createdDate, changedDate, 
	system$owner, system$changedBy
from openquery(MENDIX_DB_LOCAL, 'select * from "product$product" where oldsku like ''1DAM90%'' limit 1000')
order by sku

	select product$productid, product$productfamilyid 
	from openquery(MENDIX_DB_LOCAL, 'select * from "product$product_productfamily" limit 1000')

-- Product Stock Item
select id, 
	SKU, oldSKU, SNAPDescription, packSize, 
	manufacturerArticleID, manufacturerCodeNumber,
	SNAPUploadStatus, 
	changed
from openquery(MENDIX_DB_LOCAL, 'select * from "product$stockitem" where sku like ''01083B2D4CT%'' limit 1000')

	select product$stockitemid, product$productid 
	from openquery(MENDIX_DB_LOCAL, 'select * from "product$stockitem_product" limit 1000')
	
		
-- Product Contact Lens
select id, 
	bc, di, po, 
	cy, ax,  
	ad, _do, co, co_EDI
from openquery(MENDIX_DB_LOCAL, 'select * from "product$contactlens" limit 1000')

-- Product Pack Size
select id, 
	description, size, 
	packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
	height, width, weight, depth, 
	productGroup1, productGroup2, productGroup2Type, 
	breakable 
from openquery(MENDIX_DB_LOCAL, 'select * from "product$packsize"')
order by description, size

	select product$packsizeid, product$productfamilyid 
	from openquery(MENDIX_DB_LOCAL, 'select * from "product$packsize_productfamily"')
		
	select product$packsizeid, product$stockitemid 
	from openquery(MENDIX_DB_LOCAL, 'select * from "product$stockitem_packsize" limit 1000')
