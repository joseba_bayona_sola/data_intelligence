﻿
-- Warehouse Shipment 
select shipmentType, count(*)
from warehouseshipments$shipment
group by shipmentType
order by shipmentType;

select status, count(*)
from warehouseshipments$shipment
group by status
order by status;

select s.id, 
	s.shipmentID, s.orderNumber, po.orderNumber, s.receiptID, s.receiptNumber, s.invoiceRef,
	w.id, w.code, w.name, w.shortName, w.warehouseType, su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName,
	s.status, s.shipmentType, 
	s.stockRegisteredDate, s.dueDate, s.confirmedDate, s.arrivedDate,
	s.totalItemPrice, s.interCompanyCarriagePrice, s.inboundCarriagePrice, s.interCompanyProfit, s.duty,
	s.lock,
	s.auditComment,
	s.createdDate, 
	po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate
from 
		warehouseshipments$shipment s
	inner join
		(select w.id, w.code, w.name, w.shortName, w.warehouseType, 
			sw.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, inventory$warehouseID
				from warehouseshipments$shipment_warehouse) sw 
			inner join
				(select id, code, name, shortName, warehouseType 
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.warehouseshipments$shipmentID
	left join
		(select su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName, 
			ssu.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, suppliers$supplierID
				from warehouseshipments$shipment_supplier) ssu 
			inner join	
				(select id, supplierType, supplierID, vendorCode, supplierName
				from suppliers$supplier) su on ssu.suppliers$supplierID = su.id) su on s.id = su.warehouseshipments$shipmentID
	left join
		(select po.id, po.orderNumber, po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
			spo.warehouseshipments$shipmentID
		from				
				(select warehouseshipments$shipmentID, purchaseorders$purchaseOrderID
				from warehouseshipments$backtobackshipment_purchaseorder) spo
			inner join
				(select id, 
					orderNumber, status, source, 

					itemCost, totalPrice, leadTime, 
					approvedBy, approvedDate
				from purchaseorders$purchaseorder) po on spo.purchaseorders$purchaseOrderID = po.id) po on s.id = po.warehouseshipments$shipmentID

order by s.orderNumber desc
limit 1000;

-- Warehouse Shipment Order Line
select status, count(*)
from warehouseshipments$shipmentorderline
group by status
order by status;

select syncStatus, count(*)
from warehouseshipments$shipmentorderline
group by syncStatus
order by syncStatus;

select s.id, 
	s.shipmentID, s.orderNumber, po.orderNumber, s.receiptID, s.receiptNumber, s.invoiceRef,
	w.id, w.code, w.name, w.shortName, w.warehouseType, su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName,
	s.status, s.shipmentType, 
	s.stockRegisteredDate, s.dueDate, s.confirmedDate, s.arrivedDate,
	s.totalItemPrice, s.interCompanyCarriagePrice, s.inboundCarriagePrice, s.interCompanyProfit, s.duty,
	s.lock,
	s.auditComment,
	s.createdDate, 
	po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
	sol.status, sol.syncStatus, sol.processed, sol.quantityOrdered, sol.quantityReceived, sol.quantityRejected, sol.quantityAccepted, sol.quantityReturned, sol.unitCost
from 
		warehouseshipments$shipment s
	inner join
		(select w.id, w.code, w.name, w.shortName, w.warehouseType, 
			sw.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, inventory$warehouseID
				from warehouseshipments$shipment_warehouse) sw 
			inner join
				(select id, code, name, shortName, warehouseType 
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.warehouseshipments$shipmentID
	left join
		(select su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName, 
			ssu.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, suppliers$supplierID
				from warehouseshipments$shipment_supplier) ssu 
			inner join	
				(select id, supplierType, supplierID, vendorCode, supplierName
				from suppliers$supplier) su on ssu.suppliers$supplierID = su.id) su on s.id = su.warehouseshipments$shipmentID
	left join
		(select po.id, po.orderNumber, po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
			spo.warehouseshipments$shipmentID
		from				
				(select warehouseshipments$shipmentID, purchaseorders$purchaseOrderID
				from warehouseshipments$backtobackshipment_purchaseorder) spo
			inner join
				(select id, 
					orderNumber, status, source, 

					itemCost, totalPrice, leadTime, 
					approvedBy, approvedDate
				from purchaseorders$purchaseorder) po on spo.purchaseorders$purchaseOrderID = po.id) po on s.id = po.warehouseshipments$shipmentID
	inner join
		(select sol.id, sol.status, sol.syncStatus, sol.processed, sol.quantityOrdered, sol.quantityReceived, sol.quantityRejected, sol.quantityAccepted, sol.quantityReturned, sol.unitCost, 
			sols.warehouseshipments$shipmentID
		from
			(select warehouseshipments$shipmentOrderLineID, warehouseshipments$shipmentID
			from warehouseshipments$shipmentorderline_shipment) sols	
		inner join	
			(select id, 
				line, stockItem,
				status, syncStatus, syncResolution, created, processed,
				quantityOrdered, quantityReceived, quantityRejected, quantityAccepted, quantityReturned, 
				unitCost, 
				auditComment
			from warehouseshipments$shipmentorderline) sol on sols.warehouseshipments$shipmentOrderLineID = sol.id) sol on s.id = sol.warehouseshipments$shipmentID


where s.orderNumber = 'P00017237'
order by s.orderNumber desc
limit 1000;

	select warehouseshipments$shipmentOrderLineID, purchaseorders$purchaseOrderLineID
	from warehouseshipments$shipmentorderline_purchaseorderline
	limit 1000
	
	select warehouseshipments$shipmentOrderLineID, snapreceipts$snapReceiptLineID
	from warehouseshipments$shipmentorderline_snapreceiptline
	limit 1000;


-- Repair Stock Dates
select s.id, 
	s.shipmentID, s.orderNumber, po.orderNumber, s.receiptID, s.receiptNumber, s.invoiceRef,
	w.id, w.code, w.name, w.shortName, w.warehouseType, su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName,
	s.status, s.shipmentType, 
	s.stockRegisteredDate, s.dueDate, s.confirmedDate, s.arrivedDate,
	s.totalItemPrice, s.interCompanyCarriagePrice, s.inboundCarriagePrice, s.interCompanyProfit, s.duty,
	s.lock,
	s.auditComment,
	s.createdDate, 
	po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
	rsd.oldArrivedDate, rsd.oldConfirmedDate, rsd.newArrivedDate, rsd.newConfirmedDate, rsd.error, rsd.errorDetail
from 
		warehouseshipments$shipment s
	inner join
		(select w.id, w.code, w.name, w.shortName, w.warehouseType, 
			sw.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, inventory$warehouseID
				from warehouseshipments$shipment_warehouse) sw 
			inner join
				(select id, code, name, shortName, warehouseType 
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.warehouseshipments$shipmentID
	left join
		(select su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName, 
			ssu.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, suppliers$supplierID
				from warehouseshipments$shipment_supplier) ssu 
			inner join	
				(select id, supplierType, supplierID, vendorCode, supplierName
				from suppliers$supplier) su on ssu.suppliers$supplierID = su.id) su on s.id = su.warehouseshipments$shipmentID
	left join
		(select po.id, po.orderNumber, po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
			spo.warehouseshipments$shipmentID
		from				
				(select warehouseshipments$shipmentID, purchaseorders$purchaseOrderID
				from warehouseshipments$backtobackshipment_purchaseorder) spo
			inner join
				(select id, 
					orderNumber, status, source, 

					itemCost, totalPrice, leadTime, 
					approvedBy, approvedDate
				from purchaseorders$purchaseorder) po on spo.purchaseorders$purchaseOrderID = po.id) po on s.id = po.warehouseshipments$shipmentID
	left join
		(select rsd.id, rsd.oldArrivedDate, rsd.oldConfirmedDate, rsd.newArrivedDate, rsd.newConfirmedDate, rsd.error, rsd.errorDetail, 
			rsds.warehouseShipments$shipmentID
		from
				(select warehouseshipments$repairStockDatesID, warehouseShipments$shipmentID
				from warehouseshipments$repairstockdates_shipment) rsds
			inner join
				(select id, 
					oldArrivedDate, oldConfirmedDate, 
					newArrivedDate, newConfirmedDate, 
					error, errorDetail, 
					createdDate
				from warehouseshipments$repairstockdates) rsd on rsds.warehouseshipments$repairStockDatesID = rsd.id) rsd on s.id = rsd.warehouseshipments$shipmentID
--where rsd.id is not null
order by s.orderNumber desc
limit 1000;



-- Warehouse Shipment Processor
select s.id, 
	s.shipmentID, s.orderNumber, po.orderNumber, s.receiptID, s.receiptNumber, s.invoiceRef,
	w.id, w.code, w.name, w.shortName, w.warehouseType, su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName,
	s.status, s.shipmentType, 
	s.stockRegisteredDate, s.dueDate, s.confirmedDate, s.arrivedDate,
	s.totalItemPrice, s.interCompanyCarriagePrice, s.inboundCarriagePrice, s.interCompanyProfit, s.duty,
	s.lock,
	s.auditComment,
	s.createdDate, 
	po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
	sp.status, sp.batchSize, sp.dateStarted, sp.dateRequested, sp.timeTaken, sp.linesProcessed, sp.totalLines, sp.remaining, sp.autoStart
from 
		warehouseshipments$shipment s
	inner join
		(select w.id, w.code, w.name, w.shortName, w.warehouseType, 
			sw.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, inventory$warehouseID
				from warehouseshipments$shipment_warehouse) sw 
			inner join
				(select id, code, name, shortName, warehouseType 
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.warehouseshipments$shipmentID
	left join
		(select su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName, 
			ssu.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, suppliers$supplierID
				from warehouseshipments$shipment_supplier) ssu 
			inner join	
				(select id, supplierType, supplierID, vendorCode, supplierName
				from suppliers$supplier) su on ssu.suppliers$supplierID = su.id) su on s.id = su.warehouseshipments$shipmentID
	left join
		(select po.id, po.orderNumber, po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
			spo.warehouseshipments$shipmentID
		from				
				(select warehouseshipments$shipmentID, purchaseorders$purchaseOrderID
				from warehouseshipments$backtobackshipment_purchaseorder) spo
			inner join
				(select id, 
					orderNumber, status, source, 

					itemCost, totalPrice, leadTime, 
					approvedBy, approvedDate
				from purchaseorders$purchaseorder) po on spo.purchaseorders$purchaseOrderID = po.id) po on s.id = po.warehouseshipments$shipmentID
	left join
		(select sp.id, sp.status, sp.batchSize, 
			sp.dateStarted, sp.dateRequested, sp.timeTaken, 
			sp.linesProcessed, sp.totalLines, sp.remaining, sp.autoStart, 
			sps.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentProcessorID, warehouseshipments$shipmentID
				from warehouseshipments$shipmentprocessor_shipment) sps
			inner join
				(select id, 
					status, 
					batchSize, 
					dateStarted, dateRequested, timeTaken, 
					linesProcessed, totalLines, remaining, autoStart, --offset, 
					errorMessage
				from warehouseshipments$shipmentprocessor) sp on sps.warehouseshipments$shipmentProcessorID = sp.id) sp on s.id = sp.warehouseshipments$shipmentID

order by s.orderNumber desc
limit 1000;


-- Over Delivery
select s.id, 
	s.shipmentID, s.orderNumber, po.orderNumber, s.receiptID, s.receiptNumber, s.invoiceRef,
	w.id, w.code, w.name, w.shortName, w.warehouseType, su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName,
	s.status, s.shipmentType, 
	s.stockRegisteredDate, s.dueDate, s.confirmedDate, s.arrivedDate,
	s.totalItemPrice, s.interCompanyCarriagePrice, s.inboundCarriagePrice, s.interCompanyProfit, s.duty,
	s.lock,
	s.auditComment,
	s.createdDate, 
	po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
	od.status, od.quantity, od.createdDate
from 
		warehouseshipments$shipment s
	inner join
		(select w.id, w.code, w.name, w.shortName, w.warehouseType, 
			sw.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, inventory$warehouseID
				from warehouseshipments$shipment_warehouse) sw 
			inner join
				(select id, code, name, shortName, warehouseType 
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.warehouseshipments$shipmentID
	left join
		(select su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName, 
			ssu.warehouseshipments$shipmentID
		from
				(select warehouseshipments$shipmentID, suppliers$supplierID
				from warehouseshipments$shipment_supplier) ssu 
			inner join	
				(select id, supplierType, supplierID, vendorCode, supplierName
				from suppliers$supplier) su on ssu.suppliers$supplierID = su.id) su on s.id = su.warehouseshipments$shipmentID
	left join
		(select po.id, po.orderNumber, po.status, po.source, po.itemCost, po.totalPrice, po.leadTime, po.approvedBy, po.approvedDate, 
			spo.warehouseshipments$shipmentID
		from				
				(select warehouseshipments$shipmentID, purchaseorders$purchaseOrderID
				from warehouseshipments$backtobackshipment_purchaseorder) spo
			inner join
				(select id, 
					orderNumber, status, source, 

					itemCost, totalPrice, leadTime, 
					approvedBy, approvedDate
				from purchaseorders$purchaseorder) po on spo.purchaseorders$purchaseOrderID = po.id) po on s.id = po.warehouseshipments$shipmentID
	left join
		(select od.id, od.status, od.quantity, od.createdDate, 
			ods.warehouseshipments$shipmentID
		from 
				(select warehouseshipments$overDeliveryID, warehouseshipments$shipmentID
				from warehouseshipments$overdelivery_shipment) ods
			inner join
				(select id, 
					status, 
					quantity, createdDate
				from warehouseshipments$overdelivery) od on ods.warehouseshipments$overDeliveryID = od.id) od on s.id = od.warehouseshipments$shipmentID	
--where od.id is not null
order by s.orderNumber desc
limit 1000;



	