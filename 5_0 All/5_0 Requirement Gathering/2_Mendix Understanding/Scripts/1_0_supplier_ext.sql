﻿
-- Supplier
select id, 
	supplierType, supplierID, vendorCode, supplierName, vendorShortName, 
	email, telephone, fax,
	countryID, regionID, region, city, postcode, 
	street1, street2, street3, 
	vatCode, currencyCode, 
	b2bBoxesLimit, defaultLeadTime, isNullSupplier, flatFileConfigurationToUse, isEDIEnabled, vendorStoreNumber
from suppliers$supplier
where supplierType = 'Manufacturer' -- Company - Manufacturer - Wholesale
order by supplierName;

	select supplierType, count(*)
	from suppliers$supplier
	group by supplierType
	order by supplierType

select s.id, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
	c.id, c.companyregnum, c.companyname, c.code, 
	co.id, co.continent, co.countryName, co.iso_2, co.iso_3, 
	cu.id, cu.currencyCode, cu.currencyName, cu.standardRate, cu.spotRate
from 
		suppliers$supplier s
	inner join 
		(select suppliers$supplierID, companyorganisation$companyID
		from suppliers$supplier_company) sc on s.id = sc.suppliers$supplierID
	inner join 
		(select id, companyregnum, companyname, code
		from companyorganisation$company) c on 	sc.companyorganisation$companyID = c.id
	inner join 
		(select suppliers$supplierID, countryManagement$countryID
		from suppliers$supplier_country) sco on s.id = sco.suppliers$supplierID 
	inner join 
		(select id, 
			continent, countryName,
			iso_2, iso_3, numCode, 
			selected, 
			currencyFormat, 
			taxationArea, standardVATRate, reducedVATRate
		from countrymanagement$country) co on sco.countryManagement$countryID = co.id	
	inner join 	
		(select suppliers$supplierID, countryManagement$currencyID
		from suppliers$supplier_currency) scu on s.id = scu.suppliers$supplierID
	inner join
		(select id, 
			currencyCode, currencyName, 
			standardRate, spotRate, 
			lastStandardUpdate, lastSpotUpdate,
			createdDate, changedDate, 
			system$owner, system$changedby
		from countrymanagement$currency) cu on scu.countryManagement$currencyID = cu.id	
order by c.companyname, s.supplierName, co.countryName, cu.currencyName;

-- Supplier - Contacts
select s.id, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
	c.id, c.name, c.department, c.role, c.email, c.telephone, c.mobile
from 
		suppliers$supplier s
	inner join
		(select suppliers$contactsID, suppliers$supplierID
		from suppliers$contacts_supplier) cs on s.id = cs.suppliers$supplierID
	inner join
		(select id, 
			name, department, role,
			email, telephone, mobile
		from suppliers$contacts) c on cs.suppliers$contactsID = c.id
order by s.supplierName, c.name;

-- Supplier - Supplier Price
select s.id, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
	sp.id, sp.subMetaObjectName, sp.unitPrice, sp.totalWarehouseUnitPrice, sp.orderMultiple, sp.leadTime, sp.directPrice, sp.expiredPrice, sp.comments
from 
		suppliers$supplier s
	inner join
		(select suppliers$supplierPriceID, suppliers$supplierID
		from suppliers$supplierprices_supplier) ssp on s.id = ssp.suppliers$supplierID
	inner join
		(select id, 
			unitPrice, totalWarehouseUnitPrice, orderMultiple, leadTime, 
			subMetaObjectName,
			directPrice, expiredPrice,
			comments, 
			lastUpdated, 
			createdDate, changedDate, 
			system$owner, system$changedBy
		from suppliers$supplierprice) sp on ssp.suppliers$supplierPriceID = sp.id
order by s.supplierName, sp.subMetaObjectName, sp.unitPrice;


select s.id,
	pf.id, pf.magentoProductID, pf.magentoSKU, pf.name,
	ps.id, ps.description, ps.size, 
	s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
	sp.id, sp.subMetaObjectName, sp.unitPrice, sp.totalWarehouseUnitPrice, sp.orderMultiple, sp.leadTime, sp.directPrice, sp.expiredPrice, sp.comments, 
	ps.productGroup1, ps.productGroup2Type, ps.productGroup2, 
	ps.height, ps.width, ps.weight, ps.depth, ps.breakable, ps.allocationPreferenceOrder, ps.purchasingPreferenceOrder, ps.packagingNo
from 
		suppliers$supplier s
	inner join
		(select suppliers$supplierPriceID, suppliers$supplierID
		from suppliers$supplierprices_supplier) ssp on s.id = ssp.suppliers$supplierID
	inner join
		(select id, 
			unitPrice, totalWarehouseUnitPrice, orderMultiple, leadTime, 
			subMetaObjectName,
			directPrice, expiredPrice,
			comments, 
			lastUpdated, 
			createdDate, changedDate, 
			system$owner, system$changedBy
		from suppliers$supplierprice) sp on ssp.suppliers$supplierPriceID = sp.id
	inner join 
		(select suppliers$supplierPriceID, product$packSizeID
		from suppliers$supplierprices_packsize) spp on sp.id = spp.suppliers$supplierPriceID 
	inner join
		(select id, 
			description, size, 
			productGroup1, productGroup2Type, productGroup2, 
			height, width, weight, depth, breakable, allocationPreferenceOrder,
			packagingNo, purchasingPreferenceOrder 
		from product$packsize) ps on spp.product$packSizeID = ps.id
	inner join
		(select product$packsizeID, product$productFamilyID 
		from product$packsize_productfamily) pspf on ps.id = pspf.product$packsizeID 
	inner join	
		product$productfamily pf on pspf.product$productFamilyID = pf.id
where pf.magentoProductID in ('1083', '1100')		
order by pf.magentoProductID, ps.size, s.supplierName, sp.unitPrice;
	


-- Supplier - Supplier Price - Standard

select s.id, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
	sp.id, sp.subMetaObjectName, sp.unitPrice, sp.totalWarehouseUnitPrice, sp.orderMultiple, sp.leadTime, sp.directPrice, sp.expiredPrice, sp.comments, 
	stp.moq, stp.effectiveDate, stp.expiryDate
from 
		suppliers$supplier s
	inner join
		(select suppliers$supplierPriceID, suppliers$supplierID
		from suppliers$supplierprices_supplier) ssp on s.id = ssp.suppliers$supplierID
	inner join
		(select id, 
			unitPrice, totalWarehouseUnitPrice, orderMultiple, leadTime, 
			subMetaObjectName,
			directPrice, expiredPrice,
			comments, 
			lastUpdated, 
			createdDate, changedDate, 
			system$owner, system$changedBy
		from suppliers$supplierprice) sp on ssp.suppliers$supplierPriceID = sp.id
	left join
		(select id, moq, effectiveDate, expiryDate
		from suppliers$standardprice) stp on sp.id = stp.id		
order by s.supplierName, sp.subMetaObjectName, sp.unitPrice;

select s.id, 
	pf.id, pf.magentoProductID, pf.magentoSKU, pf.name,
	ps.id, ps.description, ps.size, 
	s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
	sp.id, sp.subMetaObjectName, sp.unitPrice, sp.totalWarehouseUnitPrice, sp.orderMultiple, sp.leadTime, sp.directPrice, sp.expiredPrice, sp.comments, stp.moq, stp.effectiveDate, stp.expiryDate,
	ps.productGroup1, ps.productGroup2Type, ps.productGroup2, 
	ps.height, ps.width, ps.weight, ps.depth, ps.breakable, ps.allocationPreferenceOrder, ps.purchasingPreferenceOrder, ps.packagingNo
from 
		suppliers$supplier s
	inner join
		(select suppliers$supplierPriceID, suppliers$supplierID
		from suppliers$supplierprices_supplier) ssp on s.id = ssp.suppliers$supplierID
	inner join
		(select id, 
			unitPrice, totalWarehouseUnitPrice, orderMultiple, leadTime, 
			subMetaObjectName,
			directPrice, expiredPrice,
			comments, 
			lastUpdated, 
			createdDate, changedDate, 
			system$owner, system$changedBy
		from suppliers$supplierprice) sp on ssp.suppliers$supplierPriceID = sp.id
	inner join 
		(select suppliers$supplierPriceID, product$packSizeID
		from suppliers$supplierprices_packsize) spp on sp.id = spp.suppliers$supplierPriceID 
	inner join
		(select id, 
			description, size, 
			productGroup1, productGroup2Type, productGroup2, 
			height, width, weight, depth, breakable, allocationPreferenceOrder,
			packagingNo, purchasingPreferenceOrder 
		from product$packsize) ps on spp.product$packSizeID = ps.id
	inner join
		(select product$packsizeID, product$productFamilyID 
		from product$packsize_productfamily) pspf on ps.id = pspf.product$packsizeID 
	inner join	
		product$productfamily pf on pspf.product$productFamilyID = pf.id
	left join
		(select id, moq, effectiveDate, expiryDate
		from suppliers$standardprice) stp on sp.id = stp.id		
where pf.magentoProductID in ('1083', '1100')		
order by pf.magentoProductID, ps.size, s.supplierName, sp.unitPrice;
	

