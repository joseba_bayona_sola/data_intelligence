﻿
-- Company Organisation
select id, companyregnum, companyname, code
from companyorganisation$company
order by companyname;

-- Company Location
select id, 
	address_id, 
	addressType, 
	country, region, city, postcode, 
	addressline1, addressline2, addressline3, 
	telephone, fax, 
	locationname
from companyorganisation$companylocation;

-- Company Bank Account
select id, 
	iban, bic, accountname
from companyorganisation$bankAccount;

-- Company Label Address
select id, 
	line1, line2, line3, 
	contactdetails, 
	vatnumber
from companyorganisation$labeladdress;

-- Company Mag Web Store
select id, 
	storeid, storename, storecurrencycode, storeToOrderRate, storeToBaseRate,
	language, 
	country, 
	snapcode, 
	dispensingstatement, 
	system$owner, system$changedby
	createddate, changeddate
from companyorganisation$magwebstore
order by storeid;

-- Company Returns Information
select id, title, instructions, contact
from companyorganisation$returnsInformation;

-- Company Logo
select id
from companyorganisation$logo;

--------------------------------------------------------------------------------------------------

	select companyorganisation$companyID1, companyorganisation$companyID2
	from companyorganisation$parentcompany_company;
	
	select companyorganisation$companyLocationID, companyorganisation$companyID
	from companyorganisation$companyaddresses_company;
	
	select companyorganisation$BankAccountID, companyorganisation$companyLocationID
	from companyorganisation$bankaccount_companylocation;
	
	select companyorganisation$companyLocationID, companyorganisation$labelAddressID
	from companyorganisation$companylocation_labeladdress;
	
	select companyorganisation$magWebStoreID, companyorganisation$labelAddressID
	from companyorganisation$magwebstore_labeladdress;
	
	select companyorganisation$returnsInformationID, companyorganisation$magWebStoreID
	from companyorganisation$returnsinformation_magwebstore;
	
	select companyorganisation$logoID, companyorganisation$magWebStoreID
	from companyorganisation$logo_magwebstore;
	
--------------------------------------------------------------------------------------------------

	select companyorganisation$companyLocationID, countryManagement$countryID
	from companyorganisation$companyaddresses_country;

	select companyorganisation$magWebStoreID, countryManagement$countryID
	from companyorganisation$magwebstore_defaultcountry;

	select companyorganisation$companyLocationID, countryManagement$currencyID
	from companyorganisation$companylocation_currency;
