
select *
from openquery(MENDIX_DB_LOCAL, 'select * from "suppliers$supplier"')

select id, 
	supplierID, supplierName, currencyCode, supplierType, isEDIEnabled, -- countryID, 
	vatCode, defaultLeadTime, 
	email, telephone, fax, 
	flatFileConfigurationToUse, 
	street1, street2, street3, city, region, postcode, -- regionID, 
	vendorCode, vendorShortName, vendorStoreNumber
from openquery(MENDIX_DB_LOCAL, 'select * from "suppliers$supplier"')
order by supplierType, supplierName

select id, 
	supplierID, supplierName, currencyCode, supplierType, isEDIEnabled, -- countryID, 
	defaultLeadTime, 
	email, telephone, fax, 
	flatFileConfigurationToUse, 
	street1, street2, street3, city, region, postcode, -- regionID, 
	vendorCode, vendorShortName, vendorStoreNumber
from openquery(MENDIX_DB_LOCAL, 
	'select id, 
		supplierID, supplierName, currencyCode, supplierType, isEDIEnabled, 
		vatCode, defaultLeadTime, 
		email, telephone, fax, 
		flatFileConfigurationToUse, 
		street1, street2, street3, city, region, postcode, 
		vendorCode, vendorShortName, vendorStoreNumber 
	from "suppliers$supplier"')
order by supplierType, supplierName

	select suppliers$supplierid, countrymanagement$countryid
	from openquery(MENDIX_DB_LOCAL, 'select "suppliers$supplierid", "countrymanagement$countryid" from "suppliers$supplier_country"')

	select suppliers$supplierid, companyorganisation$companyid
	from openquery(MENDIX_DB_LOCAL, 'select "suppliers$supplierid", "companyorganisation$companyid" from "suppliers$supplier_company"')

select *
from openquery(MENDIX_DB_LOCAL, 'select * from "suppliers$supplierprice"')

select id, 
	subMetaObjectName,
	unitPrice, leadTime, totalWarehouseUnitPrice, orderMultiple, 
	directPrice, expiredPrice,
	comments, 
	lastUpdated, 
	createdDate, changedDate, 
	system$owner, system$changedBy
from openquery(MENDIX_DB_LOCAL, 'select * from "suppliers$supplierprice"')

select *
from openquery(MENDIX_DB_LOCAL, 'select * from "suppliers$standardprice"')

select id, 
	moq, 
	effectiveDate, expiryDate
from openquery(MENDIX_DB_LOCAL, 'select id, moq, effectiveDate, expiryDate from "suppliers$standardprice"')

	select suppliers$supplierpriceid, suppliers$supplierid
	from openquery(MENDIX_DB_LOCAL, 'select "suppliers$supplierpriceid", "suppliers$supplierid" from "suppliers$supplierprices_supplier"')

	select suppliers$supplierpriceid, product$packsizeid
	from openquery(MENDIX_DB_LOCAL, 'select "suppliers$supplierpriceid", "product$packsizeid" from "suppliers$supplierprices_packsize"')
