
select id, 
	shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
	status, shipmentType, 
	dueDate, arrivedDate, confirmedDate, stockRegisteredDate, 
	totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
	lock,
	auditComment,
	createdDate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."warehouseshipments$shipment" limit 1000')
order by createddate desc

	select warehouseshipments$shipmentid, inventory$warehouseid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."warehouseshipments$shipment_warehouse" limit 1000')

	select warehouseshipments$shipmentid, suppliers$supplierid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."warehouseshipments$shipment_supplier" limit 1000')


	select inventory$warehousestockitembatchid, warehouseshipments$shipmentid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$warehousestockitembatch_shipment" limit 1000')


select id, 
	line, stockItem,
	status, syncStatus, syncResolution, created, processed,
	quantityOrdered, quantityReceived, quantityAccepted, quantityRejected,  quantityReturned, 
	unitCost, 
	auditComment
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."warehouseshipments$shipmentorderline" limit 1000')

	select warehouseshipments$shipmentorderlineid, warehouseshipments$shipmentid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."warehouseshipments$shipmentorderline_shipment" limit 1000')

	select warehouseshipments$shipmentorderlineid, suppliers$supplierid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."warehouseshipments$shipmentorderline_supplier" limit 1000')
	
	select warehouseshipments$shipmentorderlineid, purchaseorders$purchaseorderlineid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."warehouseshipments$shipmentorderline_purchaseorderline" limit 1000')

	select warehouseshipments$shipmentorderlineid, snapreceipts$snapreceiptlineid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."warehouseshipments$shipmentorderline_snapreceiptline" limit 1000')


	select inventory$warehousestockitembatchid, warehouseshipments$shipmentorderlineid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$warehousestockitembatch_shipmentorderline" limit 1000')
	