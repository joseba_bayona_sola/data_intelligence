﻿
-- Product Family
select id, 
	magentoProductID, magentoSKU, name, manufacturerProductFamily,
	modality, productType, ProductFeature, 
	productFamilyType, productGroup, Category, productSpecialisation,
	status, 
	promotionalProduct, updateSNAPDescription, createToOrder, 
	createdDate, changedDate, 
	system$owner, system$changedBy
from product$productfamily
order by magentoProductID;

	select productFamilyType, count(*)
	from product$productfamily
	group by productFamilyType
	order by productFamilyType;

	select status, count(*)
	from product$productfamily
	group by status
	order by status;

	select modality, productType, ProductFeature, count(*)
	from product$productfamily
	group by modality, productType, ProductFeature
	order by modality, productType, ProductFeature;

	select productGroup, Category, count(*)
	from product$productfamily
	group by productGroup, Category
	order by productGroup, Category;

	select productSpecialisation, count(*)
	from product$productfamily
	group by productSpecialisation
	order by productSpecialisation;
	
select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	pc.id, pc.code, pc.name, 
	pfn.id, pfn.name
from 
		product$productfamily pf
	inner join
		(select product$productFamilyID, product$productCategoryID 
		from product$productfamily_productcategory) pfpc on pf.id = pfpc.product$productFamilyID
	inner join	
		(select id, code, name
		from product$productcategory) pc on pfpc.product$productCategoryID = pc.id
	left join	
		(select product$productFamilyID, product$productFamilyNameID 
		from product$productfamilyname_productfamily) pfpfn on pf.id = pfpfn.product$productFamilyID
	left join	
		(select id, name
		from product$productfamilyname) pfn on pfpfn.product$productFamilyNameID = pfn.id	
order by pf.magentoProductID;	


-- Product Family - Product 
select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	p.id, p.productType, p.SKU, p.oldSKU, p.description, 
	p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip
from 
		product$productfamily pf
	inner join
		(select product$productID, product$productFamilyID 
		from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
	inner join
		(select id, 
			productType, 
			SKU, oldSKU, description, 
			subMetaObjectName, price,
			isBOM, valid, displaySubProductOnPackingSlip,
			createdDate, changedDate, 
			system$owner, system$changedBy	
		from product$product) p on pfp.product$productID = p.id
where pf.magentoProductID in ('1083', '1100', '2851')
order by p.description;

	select productType, subMetaObjectName, count(*)
	from product$product
	group by productType, subMetaObjectName
	order by productType, subMetaObjectName

	select price, count(*)
	from product$product
	group by price
	order by price
		
-- BOM
select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	p.id, p.productType, p.SKU, p.oldSKU, p.description, 
	p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
	bq.id, bq.quantityPer
from 
		product$productfamily pf
	inner join
		(select product$productID, product$productFamilyID 
		from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
	inner join
		(select id, 
			productType, 
			SKU, oldSKU, description, 
			subMetaObjectName, price,
			isBOM, valid, displaySubProductOnPackingSlip,
			createdDate, changedDate, 
			system$owner, system$changedBy	
		from product$product) p on pfp.product$productID = p.id
	inner join
		(select product$bomQuantityID, product$productID 
		from product$parent_product) pp on p.id = pp.product$productID 
	inner join
		(select id, quantityPer
		from product$bomquantity) bq on pp.product$bomQuantityID = bq.id
order by p.description;

-- Stock Item
select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	p.productType, p.SKU, p.oldSKU, p.description, 
	-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
	-- si.id, 
	si.SKU, si.oldSKU, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
from 
		product$productfamily pf
	inner join
		(select product$productID, product$productFamilyID 
		from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
	inner join
		(select id, 
			productType, 
			SKU, oldSKU, description, 
			subMetaObjectName, price,
			isBOM, valid, displaySubProductOnPackingSlip,
			createdDate, changedDate, 
			system$owner, system$changedBy	
		from product$product) p on pfp.product$productID = p.id		
	inner join
		(select product$stockItemID, product$productID 
		from product$stockitem_product) sip on p.id = sip.product$productID 
	inner join
		(select id, 
			SKU, oldSKU, packSize, 
			manufacturerArticleID, manufacturerCodeNumber,
			SNAPDescription, SNAPUploadStatus, 
			changed
		from product$stockitem) si on sip.product$stockItemID = si.id
where pf.magentoProductID in ('1083', '1100')
order by p.description, si.packSize;

	-- BOM Stock Item
	select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
		p.productType, p.SKU, p.oldSKU, p.description, bq.quantityPer,
		-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
		-- si.id, 
		si.SKU, si.oldSKU, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
	from 
			product$productfamily pf
		inner join
			(select product$productID, product$productFamilyID 
			from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
		inner join
			(select id, 
				productType, 
				SKU, oldSKU, description, 
				subMetaObjectName, price,
				isBOM, valid, displaySubProductOnPackingSlip,
				createdDate, changedDate, 
				system$owner, system$changedBy	
			from product$product) p on pfp.product$productID = p.id		
		inner join
			(select product$bomQuantityID, product$productID 
			from product$parent_product) pp on p.id = pp.product$productID 
		inner join
			(select id, quantityPer
			from product$bomquantity) bq on pp.product$bomQuantityID = bq.id			
		inner join
			(select product$stockItemID, product$productID 
			from product$stockitem_product) sip on p.id = sip.product$productID 
		inner join
			(select id, 
				SKU, oldSKU, packSize, 
				manufacturerArticleID, manufacturerCodeNumber,
				SNAPDescription, SNAPUploadStatus, 
				changed
			from product$stockitem) si on sip.product$stockItemID = si.id
	order by p.description, si.packSize;

-- BARCODE
select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	p.productType, p.SKU, p.oldSKU, p.description, 
	-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
	-- si.id, 
	si.SKU, si.oldSKU, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus, 
	b.upCCode, b.lineID, b.lineIDInteger, b.codeType, b.alt, b.u, b.isCurrent
from 
		product$productfamily pf
	inner join
		(select product$productID, product$productFamilyID 
		from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
	inner join
		(select id, 
			productType, 
			SKU, oldSKU, description, 
			subMetaObjectName, price,
			isBOM, valid, displaySubProductOnPackingSlip,
			createdDate, changedDate, 
			system$owner, system$changedBy	
		from product$product) p on pfp.product$productID = p.id		
	inner join
		(select product$stockItemID, product$productID 
		from product$stockitem_product) sip on p.id = sip.product$productID 
	inner join
		(select id, 
			SKU, oldSKU, packSize, 
			manufacturerArticleID, manufacturerCodeNumber,
			SNAPDescription, SNAPUploadStatus, 
			changed
		from product$stockitem) si on sip.product$stockItemID = si.id
	inner join
		(select product$barcodeID, product$stockItemID 
		from product$barcode_stockitem) bsi on si.id = bsi.product$stockItemID 
	inner join
		(select id, 
			upCCode, 
			lineID, lineIDInteger, codeType, alt, u, isCurrent
		from product$barcode) b on bsi.product$barcodeID = b.id
where pf.magentoProductID in ('1083', '1100')
order by p.description, si.packSize;

----------------------------------------------------------

-- Product Family - Packsize
select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	ps.description, ps.size, ps.productGroup1, ps.productGroup2Type, ps.productGroup2, 
	ps.height, ps.width, ps.weight, ps.depth, ps.breakable, ps.allocationPreferenceOrder, ps.purchasingPreferenceOrder, ps.packagingNo 
from 
		product$productfamily pf
	inner join
		(select product$packsizeID, product$productFamilyID 
		from product$packsize_productfamily) pspf on pf.id = pspf.product$productFamilyID 
	inner join	
		(select id, 
			description, size, 
			productGroup1, productGroup2Type, productGroup2, 
			height, width, weight, depth, breakable, allocationPreferenceOrder,
			packagingNo, purchasingPreferenceOrder 
		from product$packsize) ps on pspf.product$packsizeID = ps.id
-- where pf.magentoProductID in ('1083', '1100')
order by pf.magentoProductID, ps.description, ps.size;

	select size, count(*)
	from product$packsize
	group by size
	order by size;
	
	select productGroup1, count(*)
	from product$packsize
	group by productGroup1
	order by productGroup1;

	select productGroup2, count(*)
	from product$packsize
	group by productGroup2
	order by productGroup2;	

select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	ps.description, ps.size, ps.productGroup1, ps.productGroup2Type, ps.productGroup2, 
	ps.height, ps.width, ps.weight, ps.depth, ps.breakable, ps.allocationPreferenceOrder, ps.purchasingPreferenceOrder, ps.packagingNo, 
	wps.identifierID, wps.defaultCostPrice, wps.productGroup1, wps.productGroup2Type, wps.productGroup2, 
	wps.allocationPreference, wps.variableSelected, wps.newRecord, wps.changed, wps.updateSNAP, wps.active
from 
		product$productfamily pf
	inner join
		(select product$packsizeID, product$productFamilyID 
		from product$packsize_productfamily) pspf on pf.id = pspf.product$productFamilyID 
	inner join	
		(select id, 
			description, size, 
			productGroup1, productGroup2Type, productGroup2, 
			height, width, weight, depth, breakable, allocationPreferenceOrder,
			packagingNo, purchasingPreferenceOrder 
		from product$packsize) ps on pspf.product$packsizeID = ps.id
	inner join
		(select product$warehousePackSizeID, product$packSizeID 
		from product$warehousepacksize_packsize) wpsps on ps.id = wpsps.product$packSizeID
	inner join
		(select id, 
			identifierID, defaultCostPrice,
			productGroup1, productGroup2Type, productGroup2, 
			allocationPreference, 
			variableSelected, newRecord, changed, updateSNAP, active
		from product$warehousepacksize) wps on wpsps.product$warehousePackSizeID = wps.id
where pf.magentoProductID in ('1083', '1100') or ps.id = 40532396646337493
order by pf.magentoProductID, ps.description, ps.size;	

		
	select w.id, w.code, w.shortName, 
		wps.identifierID, wps.defaultCostPrice,
		wps.productGroup1, wps.productGroup2Type, wps.productGroup2, 
		wps.allocationPreference, 
		wps.variableSelected, wps.newRecord, wps.changed, wps.updateSNAP, wps.active
	from 
			(select id, code, shortName 
			from inventory$warehouse) w
		inner join
			(select product$warehousePackSizeID, inventory$warehouseID
			from product$warehousepacksize_warehouse) psw on w.id = psw.inventory$warehouseID
		inner join
			(select id, 
				identifierID, defaultCostPrice,
				productGroup1, productGroup2Type, productGroup2, 
				allocationPreference, 
				variableSelected, newRecord, changed, updateSNAP, active
			from product$warehousepacksize) wps on psw.product$warehousePackSizeID = wps.id
	order by wps.productGroup1, w.shortname;
	
-- Product Contact Lens
select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	p.productType, p.SKU, p.oldSKU, p.description, 
	cl.bc, cl.bc_value, cl.di, cl.di_value, cl.po, cl.po_value, cl.cy, cl.cy_value, cl.ax, cl.ax_value, cl.ad, cl.ad_value, cl._do, cl._do_value, cl.co, cl.co_value, cl.co_EDI
from 
		product$productfamily pf
	inner join
		(select product$productID, product$productFamilyID 
		from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
	inner join
		(select id, 
			productType, 
			SKU, oldSKU, description, 
			subMetaObjectName, price,
			isBOM, valid, displaySubProductOnPackingSlip,
			createdDate, changedDate, 
			system$owner, system$changedBy	
		from product$product) p on pfp.product$productID = p.id		
	inner join
		(select id, 
			bc, bc_value, di, di_value, po, po_value, 
			cy, cy_value, ax, ax_value, 
			ad, ad_value, _do, _do_value, co, co_value, co_EDI
		from product$contactlens) cl on p.id = cl.id
where pf.magentoProductID in ('1083', '1100', '2385')
	and cl.po = '-3.00'
order by p.description

"OP55T-AX170-BC8.7-CY-1.75-DI14.4-PO-3.00"

	select id, 
		bc, bc_value, di, di_value, po, po_value, 
		cy, cy_value, ax, ax_value, 
		ad, ad_value, _do, _do_value, co, co_value, co_EDI
	from product$contactlens
	where bc_value is not null or di_value is not null or po_value is not null or 
		cy_value is not null or ax_value is not null or 
		ad_value is not null or _do_value is not null or co_value is not null
	limit 100
	
	select 
		bc, bc_value, count(*)
		-- di, di_value, count(*)
		-- po, po_value, count(*)
		-- cy, cy_value, count(*)
		-- ax, ax_value, count(*)
		-- ad, ad_value, count(*)
		-- _do, _do_value, count(*)
		-- co, co_value, count(*)
	
	from product$contactlens
	group by 
		bc, bc_value
		-- di, di_value
		-- po, po_value
		-- cy, cy_value
		-- ax, ax_value
		-- ad, ad_value
		-- _do, _do_value
		-- co, co_value
	order by 
		bc, bc_value
		-- di, di_value
		-- po, po_value
		-- cy, cy_value
		-- ax, ax_value
		-- ad, ad_value
		-- _do, _do_value
		-- co, co_value

	select *
	from product$contactlens
	where co = 'YELLOW'
	
-- Shipping Group
select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	pc.id, pc.code, pc.name, 
	sg.shippingGroupID, sg.name, sg.letterBoxAble, sg.dispensingFee
from 
		product$productfamily pf
	inner join
		(select product$productFamilyID, product$productCategoryID 
		from product$productfamily_productcategory) pfpc on pf.id = pfpc.product$productFamilyID
	inner join	
		(select id, code, name
		from product$productcategory) pc on pfpc.product$productCategoryID = pc.id
	inner join
		(select product$productCategoryID, product$shippingGroupID
		from product$productcategory_shipmentgroup) pcsg on pc.id = pcsg.product$productCategoryID
	inner join
		(select id, shippingGroupID, name, letterBoxAble, dispensingFee
		from product$shippinggroup) sg on pcsg.product$shippingGroupID = sg.id
order by pf.magentoProductID;
