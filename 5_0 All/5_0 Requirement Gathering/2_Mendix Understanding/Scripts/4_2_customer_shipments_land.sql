﻿
-- Customer Shipment
select id, 
	shipmentID, partitionID0_120,
	shipmentNumber, orderIncrementID, 
	status, statusChange, 

	processingFailed, warehouseMethod, labelRenderer, 
	shippingMethod, shippingDescription, paymentMethod, 
	
	notify, fullyShipped, syncedToMagento, 
	
	shipmentValue, shippingTotal, toFollowValue, 
	hold, intersite, 
	 

	dispatchDate, expectedShippingDate, expectedDeliveryDate,  
	createdDate

	-- wholeSaleCarriage
	-- preHoldStage, preHoldStatus, 
	-- confirmedShippingDate,	
	-- year, month,
from customershipments$customershipment
where orderincrementid = '24000046508'
order by id desc
limit 1000;

-- Shipment Transaction
select s.id, 
	tr.*
from 
	customershipments$customershipment s
inner join
	(select tr.shipmentTransactionID, tr.sent, tr.timestamp, trs.customershipments$customerShipmentID
	from
		(select customershipments$shipmentTransactionID, customershipments$customerShipmentID
		from customershipments$shipmenttransaction_customershipment) trs
	inner join
		(select id, 
			shipmentTransactionID, sent, timestamp
		from customershipments$shipmenttransaction) tr on trs.customershipments$shipmentTransactionID = tr.id) tr on s.id = tr.customershipments$customerShipmentID
where orderIncrementID = '24000046508' -- in ('24000046456', '5001282816', '8002050800', '21000268977')
order by s.shipmentNumber
limit 1000;

-- Lifecycle Update
select s.id, 
	li.* 
from 
	customershipments$customershipment s
inner join
	(select li.id, li.status, li.timestamp, sli.customershipments$customerShipmentID
	from
			(select customershipments$lifecycle_updateID, customershipments$customerShipmentID
			from customershipments$customershipment_progress) sli
		inner join
			(select id, status, timestamp
			from customershipments$lifecycle_update) li on sli.customershipments$lifecycle_updateID = li.id) li on s.id = li.customershipments$customerShipmentID
	
where orderIncrementID = '24000046508' -- in ('24000046456', '5001282816', '8002050800', '21000268977')
order by s.shipmentNumber, li.timestamp
limit 1000;

-- API Updates
select s.id, 
	api.*
from 
	customershipments$customershipment s
inner join
	(select api.*, sapi.customershipments$customerShipmentID
	from
			(select customershipments$api_updatesID, customershipments$customerShipmentID
			from customershipments$api_updates_customershipment) sapi
		inner join
			(select id, 
				service, status, operation, 
				timestamp, 
				detail, body
			from customershipments$api_updates) api on sapi.customershipments$api_updatesID = api.id) api on s.id = api.customershipments$customerShipmentID
where orderIncrementID = '24000046508' -- in ('24000046456', '5001282816', '8002050800', '21000268977')
order by s.shipmentNumber, api.timestamp, api.service
limit 1000;


-- Consignment - Package Detail
select s.id, 
	c.*
from 
	customershipments$customershipment s
left join
	(select c.*, sc.customershipments$customerShipmentID
	from
		(select customershipments$consignmentID, customershipments$customerShipmentID
		from customershipments$scurriconsignment_shipment) sc 
	inner join
		(select c.*, 
			pk.tracking_number, pk.description, pk.width, pk.length, pk.height
		from
				(select id, 
					order_number, identifier, 
					warehouse_id, 
					consignment_number, 
					shipping_method, service_id, carrier, service, 
					delivery_instructions, tracking_url, 
					order_value, currency,
					create_date
				from customershipments$consignment) c
			inner join
				(select customershipments$packageDetailID, customershipments$consignmentID
				from customershipments$consignmentpackages) cpk on c.id = cpk.customershipments$consignmentID
			inner join
				(select id, 
					tracking_number, description, 
					width, length, height
				from customershipments$packagedetail) pk on cpk.customershipments$packageDetailID = pk.id) c on sc.customershipments$consignmentID = c.id) c
						on s.id = c.customershipments$customerShipmentID
where orderIncrementID = '24000046508' -- in ('24000046456', '5001282816', '8002050800', '21000268977')
order by s.shipmentNumber
limit 1000;

-- Customer Shipment Line
select s.id, 
	sl.*
from 
	customershipments$customershipment s
inner join
	(select sl.*, sls.customershipments$customerShipmentID
	from
		(select customershipments$customerShipmentLineID, customershipments$customerShipmentID
		from customershipments$customershipmentline_customershipment) sls
	inner join
		(select id, 
			shipmentLineID, lineNumber, status, variableBreakdown, 
			stockItemID, productName, productDescription, eye, 
			quantityDescription, itemQuantityOrdered, itemQuantityIssued, itemQuantityShipped, itemQuantityPreviouslyShipped, 
			packQuantityOrdered, packQuantityIssued, packQuantityShipped, 
			price, priceFormatted, 
			subTotal, subTotal_formatted, 
			VATRate, VAT, VAT_formatted,
			fullyShipped,  
			BOMLine, 
			system$changedBy
		from customershipments$customershipmentline) sl on sls.customershipments$customerShipmentLineID = sl.id) sl on s.id = sl.customershipments$customerShipmentID	
where orderIncrementID = '24000046508' -- in ('24000046456', '5001282816', '8002050800', '21000268977')
order by s.shipmentNumber, sl.shipmentLineID
limit 1000;