﻿
-- Language
select id, languageCode, languageName
from translation$language
order by languageCode;

-- Trans. Country
select id, countryCode, countryName 
from translation$country
order by countryCode;

-- Content
select id, 
	module, section, label, 
	sourceText, 
	notranslations
from translation$content
order by module, section, label;

	select module, section, count(*)
	from translation$content
	group by module, section
	order by module, section;

-- Translation
select id, 
	destinationText
from translation$translation;

---------------------------------------------------------------------------------

	select *
	from translation$translation_country;
	
	select *
	from translation$translation_language;
	
	select *
	from translation$translation_content;
