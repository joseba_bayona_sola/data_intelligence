﻿
-- Customer - Retail
select ce.id, ce.customerid, ce.subMetaObjectName,
	rc.email, rc.phoneNumber, rc.prefix, rc.suffix, rc.firstName, rc.lastName, rc.middleName, rc.taxvat
from 
		(select id, customerid, subMetaObjectName
		from customer$customerentity) ce
	inner join
		(select id, 
			email, phoneNumber, 
			prefix, suffix, firstName, lastName, middleName, 
			taxvat
		from customer$retailcustomer) rc on ce.id = rc.id
order by ce.customerID
limit 1000;

-- Customer - WholeSale
select ce.id, ce.customerid, ce.subMetaObjectName,
	wsc.name, wsc._type
from 
		(select id, customerid, subMetaObjectName
		from customer$customerentity) ce
	inner join
		(select id, name, _type
		from customer$wholesalecustomer) wsc on ce.id = wsc.id
order by wsc.name
limit 1000;

	select ce.id, ce.customerid, ce.subMetaObjectName,
		wsc.name, wsc._type, 
		c.prefix, c.firstName, c.middleName, c.lastName, c.suffix, c.role, c.email, c.telephone, c.phoneNumber, c.mobile,
		oh.customerRef, oh.dueDate--, 
		-- pl.netPrice, pl.formattedPrice, pl.moq, pl.active, pl.activePrice, pl.created, pl.validFromDate, pl.validToDate
	from 
			(select id, customerid, subMetaObjectName
			from customer$customerentity) ce
		inner join
			(select id, name, _type
			from customer$wholesalecustomer) wsc on ce.id = wsc.id
		left join
			(select customer$contactID, customer$wholesaleCustomerID
			from customer$contact_wholesalecustomer) cwsc on wsc.id = cwsc.customer$wholesaleCustomerID
		left join			
			(select id, 
				prefix, firstName, middleName, lastName, suffix, 
				role, --department, 
				email, telephone, phoneNumber, mobile, 
				salutation, comment
			from customer$contact) c on cwsc.customer$contactID = c.id
		left join
			(select customer$wholesaleOrderHeaderID, customer$wholesaleCustomerID
			from customer$wholesaleorderdocuments_customer) ohwsc on wsc.id = ohwsc.customer$wholesaleCustomerID
		left join			
			(select id, customerRef, dueDate
			from customer$wholesaleorderheader) oh on ohwsc.customer$wholesaleOrderHeaderID = oh.id		
	order by wsc.name, oh.dueDate
	limit 1000;

	select ce.id, ce.customerid, ce.subMetaObjectName,
		wsc.name, wsc._type, 
		c.prefix, c.firstName, c.middleName, c.lastName, c.suffix, c.role, c.email, c.telephone, c.phoneNumber, c.mobile,
		--oh.customerRef, oh.dueDate--, 
		pl.netPrice, pl.formattedPrice, pl.moq, pl.active, pl.activePrice, pl.created, pl.validFromDate, pl.validToDate
	from 
			(select id, customerid, subMetaObjectName
			from customer$customerentity) ce
		inner join
			(select id, name, _type
			from customer$wholesalecustomer) wsc on ce.id = wsc.id
		left join
			(select customer$contactID, customer$wholesaleCustomerID
			from customer$contact_wholesalecustomer) cwsc on wsc.id = cwsc.customer$wholesaleCustomerID
		left join			
			(select id, 
				prefix, firstName, middleName, lastName, suffix, 
				role, --department, 
				email, telephone, phoneNumber, mobile, 
				salutation, comment
			from customer$contact) c on cwsc.customer$contactID = c.id
		left join
			(select customer$wholesalePriceID, customer$wholesaleCustomerID
			from customer$wholesalepricelist) plwsc on wsc.id = plwsc.customer$wholesaleCustomerID 
		left join			
			(select id, 
				netPrice, formattedPrice, moq,
				active, activePrice,
				created, validFromDate, validToDate, 
				createdDate, changedDate, 
				system$owner, system$changedBy
			from customer$wholesaleprice) pl on plwsc.customer$wholesalePriceID = pl.id	
	order by wsc.name, pl.created
	limit 1000;