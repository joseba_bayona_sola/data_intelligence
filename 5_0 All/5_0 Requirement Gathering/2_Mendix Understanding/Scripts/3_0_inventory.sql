﻿
-- Warehouse 
select id, 
	code, name, shortName, warehouseType, 
	snapCode, scurriCode, 
	active, snapWarehouse, wms_active, useHoldingLabels
from inventory$warehouse
order by code;

-- Preference
select id, "order"
from inventory$preference;

-- Warehouse Supplier
select id
from inventory$warehousesupplier;

-- Supplier Er Routine
select id, supplierName,
	mondayDefaultCanSupply, tuesdayDefaultCanSupply, wednesdayDefaultCanSupply, thursdayDefaultCanSupply, fridayDefaultCanSupply, saturdayDefaultCanSupply, sundayDefaultCanSupply, 
	mondayDefaultCanOrder, tuesdayDefaultCanOrder, wednesdayDefaultCanOrder, thursdayDefaultCanOrder, fridayDefaultCanOrder, saturdayDefaultCanOrder, sundayDefaultCanOrder, 
	mondayOrderCutOffTime, tuesdayOrderCutOffTime, wednesdayOrderCutOffTime, thursdayOrderCutOffTime, fridayOrderCutOffTime, saturdayOrderCutOffTime, sundayOrderCutOffTime
from inventory$supplierroutine
order by supplierName;

-- Temp Stock Movement Err Correct
select *
from inventory$temp_stockmovement_errcorrect;



-- Warehouse Stock Item
select id, 
	id_string, stockingMethod, 
	actualStockQty, availableQty, allocatedStockQty, dueInQty, 
	forwardDemand, outStandingAllocation,
	stocked, changed
from inventory$warehousestockitem
limit 1000;

-- SNAP Status
select id, 
	recordIdentifier, productGroup1, productGroup2, 
	snapStatus, snapStatusError,
	SKUText, 
	snapClassification, snapExpiry, snapFunction, snapRotation, snapTransaction, snapProduction, snapLife, snapUnit, snapSKUStatus, snapBatch, 
	changed
from inventory$snapstatus
limit 1000;

-- Stock Movement
select id, 
	siteID, 
	moveID, moveLineID, 
	moveType, movementType, _class, _class_jsonkey,
	status, 
	reference, SKUId, 
	qtyTasked, qtyActioned, unit, boxQty, 
	fromState, fromSite, fromFacility, fromZone, fromBay, fromSection, fromSlot, fromStU, fromStT, fromOwner, fromStatus, fromAssignType, fromAssignRef, fromLoad, 
	toState, toSite, toFacility, toZone, toBay, toSection, toSlot, toStU, toStT, toOwner, toStatus, toAssignType, toAssignRef, toLoad, 
	sequence, jobID, jobSequence, 
	operator, supervisor, reasonID, 
	dateCreated, dateClosed, 
	stage, stockAdjustment
from inventory$stockmovement
limit 1000;

-- Batch Stock Movement
select id, 
	batchStockMovement_id, 
	quantity, comment
from inventory$batchstockmovement
limit 1000;

-- Warehouse Stock Item Batch
select id, 
	batch_id, 
	status, 
	arrivedDate, confirmedDate, stockRegisteredDate, createdDate, receiptCreatedDate, groupFIFODate,

	receivedQuantity, allocatedQuantity, issuedQuantity, onHoldQuantity, disposedQuantity, registeredQuantity, --remainingQuantity, 
	productUnitCost, dutyUnitCost, carriageUnitCost, totalUnitCost, interCoProfitUnitCost, inteCoCarriageUnitCost, totalUnitCostIncInterCo,
	exchangeRate, currency,
	fullyAllocated, fullyIssued, 
	forwardDemand, negativeFreeToSell
from inventory$warehousestockitembatch
limit 1000;

-- Repairt Stock Batch
select id, 
	batchTransaction_id, reference,
	oldAllocation, oldIssue, newAllocation, newIssue, 
	processed, 
	date 
from inventory$repairstockbatchs
limit 1000;

-- Batch Stock Allocation
select id, 
	allocatedQuantity, allocatedUnits, issuedQuantity,
	fullyIssued, isnew, cancelled,
	createdDate, changedDate
from inventory$batchstockallocation
limit 1000;

-- Batch Stock Issue
select id, 
	issueID, issuedQuantity, cancelled,
	createdDate, shippedDate
from inventory$batchstockissue
limit 1000;

--------------------------------------------------------------------------------

	select *
	from inventory$warehouse_preference;

	select *
	from inventory$warehousesupplier_warehouse;

	select *
	from inventory$supplierroutine_warehousesupplier;

	select *
	from inventory$warehousestockitem_snapstatus
	limit 1000;

	select *
	from inventory$stockmovement_warehousestockitem
	limit 1000;

	select *
	from inventory$batchstockmovement_stockmovement
	limit 1000;

	select *
	from inventory$batchstockmovement_warehousestockitembatch
	limit 1000;

	select *
	from inventory$warehousestockitembatch_warehousestockitem
	limit 1000;

	select *
	from inventory$repairstockbatchs_warehousestockitembatch
	limit 1000;

	select *
	from inventory$batchtransaction_warehousestockitembatch
	limit 1000;

	select *
	from inventory$batchstockissue_batchstockallocation
	limit 1000;

	select *
	from inventory$batchstockissue_warehousestockitembatch
	limit 1000;

--------------------------------------------------------------------------------

	select *
	from inventory$warehouse_companylocation;
	
	select *
	from inventory$warehouse_country;
	
	select *
	from inventory$warehouse_timezone;
	
	select *
	from inventory$warehouse_asasupplier;
	
	select *
	from inventory$country_preference;
	
	select *
	from inventory$warehousesupplier_supplier;
	
	select *
	from inventory$warehousestockitem_stockitem
	limit 1000;
	
	select *
	from inventory$warehousestockitembatch_shipment
	limit 1000;
	
	select *
	from inventory$warehousestockitembatch_shipmenthistory
	limit 1000;
	
	select *
	from inventory$warehousestockitembatch_shipmentorderline
	limit 1000;
	
	select *
	from inventory$batchtransaction_orderlinestockallocationtransaction
	limit 1000;
	
	select *
	from inventory$located_at
	limit 1000;

