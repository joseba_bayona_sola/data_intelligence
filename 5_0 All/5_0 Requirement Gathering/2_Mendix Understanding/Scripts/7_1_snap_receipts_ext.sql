﻿
-- SNAP Receipt
select receiptStatus, count(*)
from snapreceipts$snapreceipt
group by receiptStatus
order by receiptStatus;

select receiptprocessStatus, processDetail, count(*)
from snapreceipts$snapreceipt
group by receiptprocessStatus, processDetail 
order by receiptprocessStatus, processDetail;

select id, receiptID, 
	lines, lineQty,
	receiptStatus, receiptprocessStatus, processDetail,  
	status, stockStatus, orderType, orderClass, 
	supplierID, supplierName, 
	consignmentID, 
	purchaseOrder, 
	priority, weight, actualWeight, volume, 
	stuQty, 
	dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
	datesfixed
from snapreceipts$snapreceipt
order by receiptID
limit 1000;


-- SNAP Receipt Line

	select snapreceipts$snapreceiptLineID, snapreceipts$snapreceiptID
	from snapreceipts$snapreceiptline_snapreceipt
	limit 1000;

select id, 
	line, 
	status, level, unitOfMeasure,
	skuID,
	qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected	
from snapreceipts$snapreceiptline
limit 1000;

