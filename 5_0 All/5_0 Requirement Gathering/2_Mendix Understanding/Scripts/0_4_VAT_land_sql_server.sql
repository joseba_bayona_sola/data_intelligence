
select id, commoditycode, commodityname
from openquery(MENDIX_DB_LOCAL, 'select * from "vat$commodity"')	
	
	select product$productfamilyid, vat$commodityid
	from openquery(MENDIX_DB_LOCAL, 'select * from "product$productfamily_commodity"')

select id, 
	dispensingservicesrate, rateeffectivedate, rateenddate
from openquery(MENDIX_DB_LOCAL, 'select * from "vat$dispensingservicesrate"')
	
	select vat$dispensingservicesrateid, vat$commodityid
	from openquery(MENDIX_DB_LOCAL, 'select * from "vat$dispensingservices_commodity"')
	
	select vat$dispensingservicesrateid, countrymanagement$countryid
	from openquery(MENDIX_DB_LOCAL, 'select * from "vat$dispensingservicesrate_country"')



select id, 
	vatratingcode, vatratingdescription
from openquery(MENDIX_DB_LOCAL, 'select * from "vat$vatrating"')

	select vat$vatratingid, countrymanagement$countryid
	from openquery(MENDIX_DB_LOCAL, 'select * from "vat$vatrating_country"')


select id, 
	vatrate, vatrateeffectivedate, vatrateenddate, 
	createddate, changeddate, 
	system$owner, system$changedby
from openquery(MENDIX_DB_LOCAL, 'select * from "vat$vatrate"')
	
	select vat$vatrateid, vat$vatratingid
	from openquery(MENDIX_DB_LOCAL, 'select * from "vat$vatrate_vatrating"')
	



select id, 
	vatsaletype, taxationarea, scheduleeffectivedate, scheduleenddate
from openquery(MENDIX_DB_LOCAL, 'select * from "vat$vatschedule"')
	
	select vat$vatscheduleid, vat$vatratingid
	from openquery(MENDIX_DB_LOCAL, 'select * from "vat$vatschedule_vatrating"')
	
	select vat$vatscheduleid, vat$commodityid
	from openquery(MENDIX_DB_LOCAL, 'select * from "vat$vatschedule_commodity"')


