﻿
-- Warehouse
select id, 
	code, name, shortName, warehouseType, 
	snapCode, scurriCode, 
	active, snapWarehouse, wms_active, useHoldingLabels
from inventory$warehouse
order by code;

select w.id, w.code, w.name, w.shortName, w.warehouseType, w.snapCode, w.scurriCode, w.active, w.snapWarehouse, w.wms_active, w.useHoldingLabels, 
	c.addressType, c.country, c.region, c.city, c.postcode, c.addressline1, c.addressline2, c.addressline3, c.telephone, c.fax, c.locationname,
	co.continent, co.countryName, co.iso_2
from 
		inventory$warehouse w
	inner join
		(select inventory$warehouseID, companyOrganisation$companyLocationID
		from inventory$warehouse_companylocation) wc on w.id = wc.inventory$warehouseID
	inner join
		(select id, 
			address_id, 
			addressType, 
			country, region, city, postcode, 
			addressline1, addressline2, addressline3, 
			telephone, fax, 
			locationname
		from companyorganisation$companylocation) c on wc.companyOrganisation$companyLocationID = c.id
	inner join	
		(select inventory$warehouseID, countryManagement$countryID
		from inventory$warehouse_country) wco on w.id = wco.inventory$warehouseID
	inner join
		(select id, continent, countryName, iso_2
		from countrymanagement$country)	co on wco.countryManagement$countryID = co.id
order by w.code;

	
-- Warehouse - Country - Preference
select w.id, w.code, w.name, w.shortName, w.warehouseType, 
	c.continent, c.countryName, c.iso_2, p."order"
from 
		inventory$warehouse w
	inner join
		(select inventory$preferenceID, inventory$warehouseID
		from inventory$warehouse_preference) wp on w.id = wp.inventory$warehouseID
	inner join
		(select id, "order"
		from inventory$preference) p on wp.inventory$preferenceID = p.id	
	inner join
		(select inventory$preferenceID, countryManagement$countryID
		from inventory$country_preference) cp on p.id = cp.inventory$preferenceID
	inner join
		(select id, continent, countryName, iso_2
		from countrymanagement$country) c on cp.countryManagement$countryID = c.id		
order by c.continent, c.countryName, p."order", w.code;


-- Warehouse - Supplier
select w.id, w.code, w.name, w.shortName, w.warehouseType, 
	ws.id, sr.id, sr.supplierName
from
		inventory$warehouse w
	inner join 
		(select inventory$warehouseSupplierID, inventory$warehouseID
		from inventory$warehousesupplier_warehouse) wsw	on w.id = wsw.inventory$warehouseID	
	inner join
		(select id
		from inventory$warehousesupplier) ws on wsw.inventory$warehouseSupplierID = ws.id
	inner join
		(select inventory$supplierRoutineID, inventory$warehouseSupplierID
		from inventory$supplierroutine_warehousesupplier) srws on ws.id = srws.inventory$warehouseSupplierID
	inner join
		(select id, supplierName,
			mondayDefaultCanSupply, tuesdayDefaultCanSupply, wednesdayDefaultCanSupply, thursdayDefaultCanSupply, fridayDefaultCanSupply, saturdayDefaultCanSupply, sundayDefaultCanSupply, 
			mondayDefaultCanOrder, tuesdayDefaultCanOrder, wednesdayDefaultCanOrder, thursdayDefaultCanOrder, fridayDefaultCanOrder, saturdayDefaultCanOrder, sundayDefaultCanOrder, 
			mondayOrderCutOffTime, tuesdayOrderCutOffTime, wednesdayOrderCutOffTime, thursdayOrderCutOffTime, fridayOrderCutOffTime, saturdayOrderCutOffTime, sundayOrderCutOffTime
		from inventory$supplierroutine) sr on srws.inventory$supplierRoutineID = sr.id
order by sr.supplierName, w.code		


-- Warehouse Stock Item
select stockingMethod, count(*)
from inventory$warehousestockitem
group by stockingMethod
order by stockingMethod

select si.id, si.magentoProductID, si.magentoSKU, si.name, 
	si.productType, si.SKU, si.oldSKU, si.description, si.SKU_si, si.oldSKU_si, si.packSize, si.manufacturerArticleID, 
	w.code, w.name, w.shortName,
	ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
	ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed
from
		(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.productType, p.SKU, p.oldSKU, p.description, si.SKU SKU_si, si.oldSKU oldSKU_si, si.packSize, si.manufacturerArticleID
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id
		where pf.magentoProductID in ('1083', '1100')
		) si 
	inner join
		(select inventory$warehouseStockItemID, product$stockItemID
		from inventory$warehousestockitem_stockitem) wssi on si.id = wssi.product$stockItemID
	inner join	
		(select id, 
			id_string, stockingMethod, 
			actualStockQty, availableQty, allocatedStockQty, dueInQty, 
			forwardDemand, outStandingAllocation,
			stocked, changed
		from inventory$warehousestockitem) ws on wssi.inventory$warehouseStockItemID = ws.id
	inner join
		(select inventory$warehouseStockItemID, inventory$warehouseID
		from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
	inner join	
		inventory$warehouse w on l.inventory$warehouseID = w.id
order by si.description, si.packSize, w.code;	

	-- With SNAP Status
	select si.id, si.magentoProductID, si.magentoSKU, si.name, 
		si.productType, si.SKU, si.oldSKU, si.description, si.SKU_si, si.oldSKU_si, si.packSize, si.manufacturerArticleID, 
		w.code, w.name, w.shortName,
		ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
		ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed, 

		ss.recordIdentifier, ss.productGroup1, ss.productGroup2, ss.snapStatus, ss.snapStatusError, ss.SKUText, 
		ss.snapClassification, ss.snapExpiry, ss.snapFunction, ss.snapRotation, ss.snapTransaction, ss.snapProduction, ss.snapLife, ss.snapUnit, ss.snapSKUStatus, ss.snapBatch
	from
			(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
				p.productType, p.SKU, p.oldSKU, p.description, si.SKU SKU_si, si.oldSKU oldSKU_si, si.packSize, si.manufacturerArticleID
			from 
					product$productfamily pf
				inner join
					(select product$productID, product$productFamilyID 
					from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
				inner join
					(select id, 
						productType, 
						SKU, oldSKU, description, 
						subMetaObjectName, price,
						isBOM, valid, displaySubProductOnPackingSlip,
						createdDate, changedDate, 
						system$owner, system$changedBy	
					from product$product) p on pfp.product$productID = p.id		
				inner join
					(select product$stockItemID, product$productID 
					from product$stockitem_product) sip on p.id = sip.product$productID 
				inner join
					(select id, 
						SKU, oldSKU, packSize, 
						manufacturerArticleID, manufacturerCodeNumber,
						SNAPDescription, SNAPUploadStatus, 
						changed
					from product$stockitem) si on sip.product$stockItemID = si.id
			where pf.magentoProductID in ('1083') --, '1100')
			) si 
		inner join
			(select inventory$warehouseStockItemID, product$stockItemID
			from inventory$warehousestockitem_stockitem) wssi on si.id = wssi.product$stockItemID
		inner join	
			(select id, 
				id_string, stockingMethod, 
				actualStockQty, availableQty, allocatedStockQty, dueInQty, 
				forwardDemand, outStandingAllocation,
				stocked, changed
			from inventory$warehousestockitem) ws on wssi.inventory$warehouseStockItemID = ws.id
		inner join
			(select inventory$warehouseStockItemID, inventory$warehouseID
			from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
		inner join	
			inventory$warehouse w on l.inventory$warehouseID = w.id
		inner join
			(select inventory$warehouseStockItemID, inventory$snapStatusID
			from inventory$warehousestockitem_snapstatus) wsss on ws.id = wsss.inventory$warehouseStockItemID
		inner join
			(select id, 
				recordIdentifier, productGroup1, productGroup2, 
				snapStatus, snapStatusError,
				SKUText, 
				snapClassification, snapExpiry, snapFunction, snapRotation, snapTransaction, snapProduction, snapLife, snapUnit, snapSKUStatus, snapBatch, 
				changed
			from inventory$snapstatus) ss on wsss.inventory$snapStatusID = ss.id
	order by si.description, si.packSize, w.code;


-- Warehouse Stock Item Batch
select status, count(*)
from inventory$warehousestockitembatch
group by status
order by status;

select si.id, si.magentoProductID, si.magentoSKU, si.name, 
	si.productType, si.SKU, si.oldSKU, si.description, si.SKU_si, si.oldSKU_si, si.packSize, si.manufacturerArticleID, 
	w.code, w.name, w.shortName,
	ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
	ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed, 
	wsib.*
from
		(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.productType, p.SKU, p.oldSKU, p.description, si.SKU SKU_si, si.oldSKU oldSKU_si, si.packSize, si.manufacturerArticleID
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id
		where pf.magentoProductID in ('1083') --, '1100')
		) si 
	inner join
		(select inventory$warehouseStockItemID, product$stockItemID
		from inventory$warehousestockitem_stockitem) wssi on si.id = wssi.product$stockItemID
	inner join	
		(select id, 
			id_string, stockingMethod, 
			actualStockQty, availableQty, allocatedStockQty, dueInQty, 
			forwardDemand, outStandingAllocation,
			stocked, changed
		from inventory$warehousestockitem) ws on wssi.inventory$warehouseStockItemID = ws.id
	inner join
		(select inventory$warehouseStockItemID, inventory$warehouseID
		from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
	inner join	
		inventory$warehouse w on l.inventory$warehouseID = w.id
	inner join
		(select inventory$warehouseStockItemBatchID, inventory$warehouseStockItemID
		from inventory$warehousestockitembatch_warehousestockitem) wswsib on ws.id = wswsib.inventory$warehouseStockItemID
	inner join	
		(select id, 
			batch_id, 
			status, 
			arrivedDate, confirmedDate, stockRegisteredDate, createdDate, receiptCreatedDate, groupFIFODate,

			receivedQuantity, allocatedQuantity, issuedQuantity, onHoldQuantity, disposedQuantity, registeredQuantity, --remainingQuantity, 
			productUnitCost, dutyUnitCost, carriageUnitCost, totalUnitCost, interCoProfitUnitCost, inteCoCarriageUnitCost, totalUnitCostIncInterCo,
			exchangeRate, currency,
			fullyAllocated, fullyIssued, 
			forwardDemand, negativeFreeToSell
		from inventory$warehousestockitembatch) wsib on wswsib.inventory$warehouseStockItemBatchID = wsib.id
order by si.description, si.packSize, w.code, wsib.arrivedDate;



-- Stock Movement
select moveType, movementType, _class, _class_jsonkey, count(*)
from inventory$stockmovement
group by moveType, movementType, _class, _class_jsonkey
order by moveType, movementType, _class, _class_jsonkey

select moveType, _class, count(*)
from inventory$stockmovement
group by moveType, _class
order by moveType, _class

select status, count(*)
from inventory$stockmovement
group by status
order by status

select stage, count(*)
from inventory$stockmovement
group by stage
order by stage

select stockAdjustment, count(*)
from inventory$stockmovement
group by stockAdjustment
order by stockAdjustment


select si.id, si.magentoProductID, si.magentoSKU, si.name, 
	si.productType, si.SKU, si.oldSKU, si.description, si.SKU_si, si.oldSKU_si, si.packSize, si.manufacturerArticleID, 
	w.code, w.name, w.shortName,
	ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
	ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed, 
	smo.*
from
		(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.productType, p.SKU, p.oldSKU, p.description, si.SKU SKU_si, si.oldSKU oldSKU_si, si.packSize, si.manufacturerArticleID
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id
		where pf.magentoProductID in ('1083') --, '1100')
		) si 
	inner join
		(select inventory$warehouseStockItemID, product$stockItemID
		from inventory$warehousestockitem_stockitem) wssi on si.id = wssi.product$stockItemID
	inner join	
		(select id, 
			id_string, stockingMethod, 
			actualStockQty, availableQty, allocatedStockQty, dueInQty, 
			forwardDemand, outStandingAllocation,
			stocked, changed
		from inventory$warehousestockitem) ws on wssi.inventory$warehouseStockItemID = ws.id
	inner join
		(select inventory$warehouseStockItemID, inventory$warehouseID
		from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
	inner join	
		inventory$warehouse w on l.inventory$warehouseID = w.id
	inner join
		(select inventory$stockMovementID, inventory$warehouseStockItemID
		from inventory$stockmovement_warehousestockitem) smws on ws.id = smws.inventory$warehouseStockItemID
	inner join
		(select id, 
			siteID, 
			moveID, moveLineID, 
			moveType, movementType, _class, _class_jsonkey,
			status, 
			reference, SKUId, 
			qtyTasked, qtyActioned, unit, boxQty, 
			fromState, fromSite, fromFacility, fromZone, fromBay, fromSection, fromSlot, fromStU, fromStT, fromOwner, fromStatus, fromAssignType, fromAssignRef, fromLoad, 
			toState, toSite, toFacility, toZone, toBay, toSection, toSlot, toStU, toStT, toOwner, toStatus, toAssignType, toAssignRef, toLoad, 
			sequence, jobID, jobSequence, 
			operator, supervisor, reasonID, 
			dateCreated, dateClosed, 
			stage, stockAdjustment
		from inventory$stockmovement) smo on smws.inventory$stockMovementID = smo.id

order by si.description, si.packSize, w.code, smo.dateCreated;

-- Batch Stock Movement
select si.id, si.magentoProductID, si.magentoSKU, si.name, 
	si.productType, si.SKU, si.oldSKU, si.description, si.SKU_si, si.oldSKU_si, si.packSize, si.manufacturerArticleID, 
	w.code, w.name, w.shortName,
	ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
	ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed, 
	wsib.*, 
	bsm.*
from
		(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.productType, p.SKU, p.oldSKU, p.description, si.SKU SKU_si, si.oldSKU oldSKU_si, si.packSize, si.manufacturerArticleID
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id
		where pf.magentoProductID in ('1083') --, '1100')
		) si 
	inner join
		(select inventory$warehouseStockItemID, product$stockItemID
		from inventory$warehousestockitem_stockitem) wssi on si.id = wssi.product$stockItemID
	inner join	
		(select id, 
			id_string, stockingMethod, 
			actualStockQty, availableQty, allocatedStockQty, dueInQty, 
			forwardDemand, outStandingAllocation,
			stocked, changed
		from inventory$warehousestockitem) ws on wssi.inventory$warehouseStockItemID = ws.id
	inner join
		(select inventory$warehouseStockItemID, inventory$warehouseID
		from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
	inner join	
		inventory$warehouse w on l.inventory$warehouseID = w.id
	inner join
		(select inventory$warehouseStockItemBatchID, inventory$warehouseStockItemID
		from inventory$warehousestockitembatch_warehousestockitem) wswsib on ws.id = wswsib.inventory$warehouseStockItemID
	inner join	
		(select id, 
			batch_id, 
			status, 
			arrivedDate, confirmedDate, stockRegisteredDate, createdDate, receiptCreatedDate, groupFIFODate,

			receivedQuantity, allocatedQuantity, issuedQuantity, onHoldQuantity, disposedQuantity, registeredQuantity, --remainingQuantity, 
			productUnitCost, dutyUnitCost, carriageUnitCost, totalUnitCost, interCoProfitUnitCost, inteCoCarriageUnitCost, totalUnitCostIncInterCo,
			exchangeRate, currency,
			fullyAllocated, fullyIssued, 
			forwardDemand, negativeFreeToSell
		from inventory$warehousestockitembatch) wsib on wswsib.inventory$warehouseStockItemBatchID = wsib.id
	left join
		(select inventory$batchStockMovementID, inventory$warehouseStockItemBatchID
		from inventory$batchstockmovement_warehousestockitembatch) bsmwsib on wsib.id = bsmwsib.inventory$warehouseStockItemBatchID
	left join	
		(select id, 
			batchStockMovement_id, 
			quantity, comment
		from inventory$batchstockmovement) bsm on bsmwsib.inventory$batchStockMovementID = bsm.id
order by si.description, si.packSize, w.code, wsib.arrivedDate;



-- Repair Stock Batchs
select si.id, si.magentoProductID, si.magentoSKU, si.name, 
	si.productType, si.SKU, si.oldSKU, si.description, si.SKU_si, si.oldSKU_si, si.packSize, si.manufacturerArticleID, 
	w.code, w.name, w.shortName,
	ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
	ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed, 
	wsib.*, 
	rsb.*
from
		(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.productType, p.SKU, p.oldSKU, p.description, si.SKU SKU_si, si.oldSKU oldSKU_si, si.packSize, si.manufacturerArticleID
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id
		where pf.magentoProductID in ('1083') --, '1100')
		) si 
	inner join
		(select inventory$warehouseStockItemID, product$stockItemID
		from inventory$warehousestockitem_stockitem) wssi on si.id = wssi.product$stockItemID
	inner join	
		(select id, 
			id_string, stockingMethod, 
			actualStockQty, availableQty, allocatedStockQty, dueInQty, 
			forwardDemand, outStandingAllocation,
			stocked, changed
		from inventory$warehousestockitem) ws on wssi.inventory$warehouseStockItemID = ws.id
	inner join
		(select inventory$warehouseStockItemID, inventory$warehouseID
		from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
	inner join	
		inventory$warehouse w on l.inventory$warehouseID = w.id
	inner join
		(select inventory$warehouseStockItemBatchID, inventory$warehouseStockItemID
		from inventory$warehousestockitembatch_warehousestockitem) wswsib on ws.id = wswsib.inventory$warehouseStockItemID
	inner join	
		(select id, 
			batch_id, 
			status, 
			arrivedDate, confirmedDate, stockRegisteredDate, createdDate, receiptCreatedDate, groupFIFODate,

			receivedQuantity, allocatedQuantity, issuedQuantity, onHoldQuantity, disposedQuantity, registeredQuantity, --remainingQuantity, 
			productUnitCost, dutyUnitCost, carriageUnitCost, totalUnitCost, interCoProfitUnitCost, inteCoCarriageUnitCost, totalUnitCostIncInterCo,
			exchangeRate, currency,
			fullyAllocated, fullyIssued, 
			forwardDemand, negativeFreeToSell
		from inventory$warehousestockitembatch) wsib on wswsib.inventory$warehouseStockItemBatchID = wsib.id
	left join
		(select inventory$repairStockBatchsID, inventory$WarehouseStockItemBatchID
		from inventory$repairstockbatchs_warehousestockitembatch) rsbwsib on wsib.id = rsbwsib.inventory$WarehouseStockItemBatchID
	left join
		(select id, 
			batchTransaction_id, reference,
			oldAllocation, oldIssue, newAllocation, newIssue, 
			processed, 
			date 
		from inventory$repairstockbatchs) rsb on rsbwsib.inventory$repairStockBatchsID = rsb.id		
order by si.description, si.packSize, w.code, wsib.arrivedDate;


-- Batch Stock Allocation
select si.id, si.magentoProductID, si.magentoSKU, si.name, 
	si.productType, si.SKU, si.oldSKU, si.description, si.SKU_si, si.oldSKU_si, si.packSize, si.manufacturerArticleID, 
	w.code, w.name, w.shortName,
	ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
	ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed, 
	wsib.*, 
	bsa.*
from
		(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.productType, p.SKU, p.oldSKU, p.description, si.SKU SKU_si, si.oldSKU oldSKU_si, si.packSize, si.manufacturerArticleID
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id
		where pf.magentoProductID in ('1083') and p.sku in ('01083B2D4CH0000000000') --, '1100')
		) si 
	inner join
		(select inventory$warehouseStockItemID, product$stockItemID
		from inventory$warehousestockitem_stockitem) wssi on si.id = wssi.product$stockItemID
	inner join	
		(select id, 
			id_string, stockingMethod, 
			actualStockQty, availableQty, allocatedStockQty, dueInQty, 
			forwardDemand, outStandingAllocation,
			stocked, changed
		from inventory$warehousestockitem) ws on wssi.inventory$warehouseStockItemID = ws.id
	inner join
		(select inventory$warehouseStockItemID, inventory$warehouseID
		from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
	inner join	
		inventory$warehouse w on l.inventory$warehouseID = w.id
	inner join
		(select inventory$warehouseStockItemBatchID, inventory$warehouseStockItemID
		from inventory$warehousestockitembatch_warehousestockitem) wswsib on ws.id = wswsib.inventory$warehouseStockItemID
	inner join	
		(select id, 
			batch_id, 
			status, 
			arrivedDate, confirmedDate, stockRegisteredDate, createdDate, receiptCreatedDate, groupFIFODate,

			receivedQuantity, allocatedQuantity, issuedQuantity, onHoldQuantity, disposedQuantity, registeredQuantity, --remainingQuantity, 
			productUnitCost, dutyUnitCost, carriageUnitCost, totalUnitCost, interCoProfitUnitCost, inteCoCarriageUnitCost, totalUnitCostIncInterCo,
			exchangeRate, currency,
			fullyAllocated, fullyIssued, 
			forwardDemand, negativeFreeToSell
		from inventory$warehousestockitembatch) wsib on wswsib.inventory$warehouseStockItemBatchID = wsib.id
	left join
		(select inventory$batchStockAllocationID, inventory$warehouseStockItemBatchID
		from inventory$batchtransaction_warehousestockitembatch) bsawsib on wsib.id = bsawsib.inventory$warehouseStockItemBatchID
	left join	
		(select id, 
			allocatedQuantity, allocatedUnits, issuedQuantity,
			fullyIssued, isnew, cancelled,
			createdDate, changedDate
		from inventory$batchstockallocation) bsa on bsawsib.inventory$batchStockAllocationID = bsa.id

order by si.description, si.packSize, w.code, wsib.arrivedDate, bsa.createdDate;

	select inventory$batchStockAllocationID, stockallocation$orderLineStockAllocationTransactionID
	from inventory$batchtransaction_orderlinestockallocationtransaction
	limit 1000;

-- Batch Stock Issue
select si.id, si.magentoProductID, si.magentoSKU, si.name, 
	si.productType, si.SKU, si.oldSKU, si.description, si.SKU_si, si.oldSKU_si, si.packSize, si.manufacturerArticleID, 
	w.code, w.name, w.shortName,
	ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
	ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed, 
	wsib.*, 
	bsa.*, bsi.*
from
		(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.productType, p.SKU, p.oldSKU, p.description, si.SKU SKU_si, si.oldSKU oldSKU_si, si.packSize, si.manufacturerArticleID
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id
		where pf.magentoProductID in ('1083') and p.sku in ('01083B2D4CH0000000000') --, '1100')
		) si 
	inner join
		(select inventory$warehouseStockItemID, product$stockItemID
		from inventory$warehousestockitem_stockitem) wssi on si.id = wssi.product$stockItemID
	inner join	
		(select id, 
			id_string, stockingMethod, 
			actualStockQty, availableQty, allocatedStockQty, dueInQty, 
			forwardDemand, outStandingAllocation,
			stocked, changed
		from inventory$warehousestockitem) ws on wssi.inventory$warehouseStockItemID = ws.id
	inner join
		(select inventory$warehouseStockItemID, inventory$warehouseID
		from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
	inner join	
		inventory$warehouse w on l.inventory$warehouseID = w.id
	inner join
		(select inventory$warehouseStockItemBatchID, inventory$warehouseStockItemID
		from inventory$warehousestockitembatch_warehousestockitem) wswsib on ws.id = wswsib.inventory$warehouseStockItemID
	inner join	
		(select id, 
			batch_id, 
			status, 
			arrivedDate, confirmedDate, stockRegisteredDate, createdDate, receiptCreatedDate, groupFIFODate,

			receivedQuantity, allocatedQuantity, issuedQuantity, onHoldQuantity, disposedQuantity, registeredQuantity, --remainingQuantity, 
			productUnitCost, dutyUnitCost, carriageUnitCost, totalUnitCost, interCoProfitUnitCost, inteCoCarriageUnitCost, totalUnitCostIncInterCo,
			exchangeRate, currency,
			fullyAllocated, fullyIssued, 
			forwardDemand, negativeFreeToSell
		from inventory$warehousestockitembatch) wsib on wswsib.inventory$warehouseStockItemBatchID = wsib.id
	left join
		(select inventory$batchStockAllocationID, inventory$warehouseStockItemBatchID
		from inventory$batchtransaction_warehousestockitembatch) bsawsib on wsib.id = bsawsib.inventory$warehouseStockItemBatchID
	left join	
		(select id, 
			allocatedQuantity, allocatedUnits, issuedQuantity,
			fullyIssued, isnew, cancelled,
			createdDate, changedDate
		from inventory$batchstockallocation) bsa on bsawsib.inventory$batchStockAllocationID = bsa.id
	left join
		(select inventory$batchStockIssueID, inventory$batchStockAllocationID
		from inventory$batchstockissue_batchstockallocation) bsibsa on bsa.id = bsibsa.inventory$batchStockAllocationID
	left join
		(select id, 
			issueID, issuedQuantity, cancelled,
			createdDate, shippedDate
		from inventory$batchstockissue) bsi on bsibsa.inventory$batchStockIssueID = bsi.id
order by si.description, si.packSize, w.code, wsib.arrivedDate, bsa.createdDate;


	
	