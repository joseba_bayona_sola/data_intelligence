
select id, 
	incrementID, magentoOrderID, orderStatus, createdat, updatedat,
	orderLinesMagento, orderLinesDeduped,

	_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
	allocatedDate, promisedShippingDate, promisedDeliveryDate,
	labelRenderer,  
	couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder, 
	
	createdDate

from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$order" where incrementID in (''8002436082'', ''24000046508'', ''17000281863'', ''24000046456'') limit 1000')
order by createdDate

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$order_customer" where "orderprocessing$orderid" = 48695170983643660 limit 1000')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$order_magwebstore" where "orderprocessing$orderid" = 48695170983643660 limit 1000')



select id, status, timestamp 
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$order_statusupdate" where id in (74027918878300603, 74027918878300604, 74027918878308681, 74027918878308685) limit 1000')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$statusupdate_order" where "orderprocessing$orderid" = 48695170983643660 limit 1000')

select id, orderReference, status, detail, timestamp  -- messageBody, 
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderprocessingtransaction" where id in (5910974511928759) limit 1000')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderprocessingtransaction_order" where "orderprocessing$orderid" = 48695170983643660 limit 1000')


select id, 
	subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
	discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
	shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
	adjustmentNegative, adjustmentPositive, 
					
	custBalanceAmount, 
	customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
	grandTotal, totalInvoiced, totalCanceled, totalRefunded,
	toOrderRate, toGlobalRate, currencyCode
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$basevalues" where id = 4785074605068559 limit 1000')


	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$basevalues_order" where "orderprocessing$orderid" = 48695170983643660 limit 1000')


select id, 
	magentoItemID, 
	quantityOrdered, quantityInvoiced, quantityRefunded, quantityIssued, quantityAllocated, quantityShipped, 
	basePrice, baseRowPrice, 
	allocated, 
	orderLineType, subMetaObjectName,
	sku, eye, lensGroupID, 

	netPrice, netRowPrice,
	
	createdDate, changedDate, 
	system$owner, system$changedBy
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderlineabstract" where id in (1970324861768020, 1970324861768019, 1970324861768018, 
	54324670526327104, 54324670526327105, 54324670526327106) limit 1000')


select id, lineAdded, deduplicate, fulfilled
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderlinemagento" where id in (1970324861768020, 1970324861768019, 1970324861768018) limit 1000')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderlinemagento_order" where "orderprocessing$orderid" = 48695170983643660 limit 1000')

select id, status, packsOrdered, packsAllocated, packsShipped
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderline" where id in (54324670526327104, 54324670526327105, 54324670526327106) limit 1000')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderline_order" where "orderprocessing$orderid" = 48695170983643660 limit 1000')

select id, 
	dispensingFee, 
	allocationStatus, shipmentStatus, 
	wholesale, 
	letterBoxAble, onHold, adHoc
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$shippinggroup" where id = 109775240925907680 limit 1000')	

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$shippinggroup_order" where "orderprocessing$orderid" = 48695170983643660 limit 1000')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderlineabstract_shippinggroup" where "orderprocessing$shippinggroupid" = 109775240925907680 limit 1000')


	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderline_product" where "orderprocessing$orderlineabstractid" in (1970324861768020, 1970324861768019, 1970324861768018, 
		54324670526327104, 54324670526327105, 54324670526327106)  limit 1000')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."orderprocessing$orderline_requiredstockitem" where "orderprocessing$orderlineabstractid" in (1970324861768020, 1970324861768019, 1970324861768018, 
		54324670526327104, 54324670526327105, 54324670526327106)  limit 1000')
