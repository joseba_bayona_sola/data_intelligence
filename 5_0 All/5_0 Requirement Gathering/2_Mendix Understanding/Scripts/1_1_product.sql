﻿
-- Product Category
select id, 
	code, name
from product$productcategory
order by code;

-- Product Family Name
select id, name
from product$productfamilyname
order by name;

-- Product Family
select id, 
	magentoProductID, magentoSKU, name, manufacturerProductFamily,
	modality, productType, ProductFeature, 
	productFamilyType, productGroup, Category, productSpecialisation,
	status, 
	promotionalProduct, updateSNAPDescription, createToOrder, 
	createdDate, changedDate, 
	system$owner, system$changedBy
from product$productfamily
order by magentoProductID;

-- Product Other
select id
from product$productother;

-- Product 
select id, 
	productType, 
	SKU, oldSKU, description, 
	subMetaObjectName, price,
	isBOM, valid, displaySubProductOnPackingSlip,
	createdDate, changedDate, 
	system$owner, system$changedBy	
from product$product
limit 1000;

-- Product BOM Quantity
select id, quantityPer
from product$bomquantity
order by quantityPer;

-- Product Stock Item
select id, 
	SKU, oldSKU, packSize, 
	manufacturerArticleID, manufacturerCodeNumber,
	SNAPDescription, SNAPUploadStatus, 
	changed
from product$stockitem
limit 1000;

-- Product BAR Code
select id, 
	upCCode, 
	lineID, lineIDInteger, codeType, alt, u, isCurrent
from product$barcode
limit 1000;

-- Product Pack Size
select id, 
	description, size, 
	productGroup1, productGroup2Type, productGroup2, 
	height, width, weight, depth, breakable, allocationPreferenceOrder,
	packagingNo, purchasingPreferenceOrder 
from product$packsize
order by description, size;

-- Product Warehouse Pack Size
select id, 
	identifierID, defaultCostPrice,
	productGroup1, productGroup2Type, productGroup2, 
	allocationPreference, 
	variableSelected, newRecord, changed, updateSNAP, active
from product$warehousepacksize

-- Product Contact Lens
select id, 
	bc, bc_value, di, di_value, po, po_value, 
	cy, cy_value, ax, ax_value, 
	ad, ad_value, _do, _do_value, co, co_value, co_EDI
from product$contactlens
limit 1000;

-------------------------------------------------------------------------

select id, 
	keyID, code, value
	theType
from product$keycodes
order by code;

select id, anyOptions
from product$productconfigurable;

select id, 
	sortOrder, attributeName, 
	attributeType, 
	mandatory, displayOnPackingSlip, displayOnPickList
from product$option
order by sortOrder;

select id, optionSetName
from product$optionset;

select id
from product$productgrouped;

select id, shippingGroupID, name,
	letterBoxAble, dispensingFee
from product$shippinggroup;

select id, 
	description_suffix, 
	pk_line, descsuffix_line, 
	bc_line, di_line, po_line, 
	cy_line, ax_line, 
	ad_line, do_line, co_line, 
	complete -- offset
from product$snapdescriptdef
order by description_suffix;

select id, SKU, name
from product$dataimport;

select id, quantityPer
from product$breakingrules;

select *
from product$productcatalog;

select id
from product$productuploaddocument;

select id, 
	date, description, comment, reference
from product$productuploadregister;

select id
from product$productimportrun;

select *
from product$snapproductsynch;

select id
from product$snapproductsynchdoc;

select id
from product$snapalternatesynchdoc;

select id, 
	SKU, identifier, update, 
	altenateType, alternate
from product$snapalternateupload;

select *
from product$newpacksize;

select *
from product$newproductfamily;

-------------------------------------------------------------------------

	select * 
	from product$productfamilyname_productfamily;
	
	select * 
	from product$productfamily_productcategory;
	
	select * 
	from product$product_productfamily
	limit 1000;
	
	select * 
	from product$parent_product;
	
	select * 
	from product$stockitem_product
	limit 1000;
	
	select * 
	from product$bom_stockitem;
	
	select * 
	from product$barcode_stockitem
	limit 1000;
	
	select * 
	from product$breakingrules_child_stockitem;
	select * 
	from product$breakingrules_parent_stockitem;
	
	select * 
	from product$packsize_productfamily;
	
	select * 
	from product$stockitem_packsize
	limit 1000;
	
	select * 
	from product$warehousepacksize_packsize;

-------------------------------------------------------------------------

	select *
	from product$productfamilyname_magwebstore;

	select *
	from product$productfamily_commodity;

	select *
	from product$warehousepacksize_warehouse;

	select *
	from product$shippinggroup_forcedwarehouse;
