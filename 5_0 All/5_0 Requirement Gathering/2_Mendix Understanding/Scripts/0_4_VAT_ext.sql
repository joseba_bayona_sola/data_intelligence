﻿
-- Commmodity - Dispensing Rate

	--> Commodity similar to Product Type but for Disp. Services
		-- Need to take care about Colour Plano Contact Lenses
		-- Prescription Glasses in Glasses Frames?
	--> Dispensing Service Rate currently depending on Commodity
		-- Add Country at that relationship level? 
		-- Why many repeated values (0?)? 
	--> Commodity finally related with Product Family
	
select *
from vat$commodity c;

select *
from vat$dispensingservicesrate;

select c.id, c.commodityCode, c.commodityName,
	ds.id, ds.dispensingServicesRate, ds.rateEffectiveDate, ds.rateEndDate
from 
		vat$commodity c
	left join
		(select vat$dispensingServicesRateID, vat$commodityID 
		from vat$dispensingservices_commodity) dsc on c.id = dsc.vat$commodityID 
	left join
		(select id, 
			dispensingServicesRate, 
			rateEffectiveDate, rateEndDate
		from vat$dispensingservicesrate) ds on dsc.vat$dispensingServicesRateID = ds.id
order by c.commodityCode;

	select co.id, co.continent, co.countryName, co.iso_2, co.iso_3, co.taxationArea, co.standardVATRate, co.reducedVATRate,
		c.id, c.commodityCode, c.commodityName,
		ds.id, ds.dispensingServicesRate, ds.rateEffectiveDate, ds.rateEndDate
	from 
			vat$commodity c
		left join
			(select vat$dispensingServicesRateID, vat$commodityID 
			from vat$dispensingservices_commodity) dsc on c.id = dsc.vat$commodityID 
		left join
			(select id, 
				dispensingServicesRate, 
				rateEffectiveDate, rateEndDate
			from vat$dispensingservicesrate) ds on dsc.vat$dispensingServicesRateID = ds.id
		left join
			(select vat$dispensingServicesRateID, countryManagement$countryID 
			from vat$dispensingservicesrate_country) dsco on ds.id = dsco.vat$dispensingServicesRateID
		left join
			(select id, continent, countryName, iso_2, iso_3, taxationArea, standardVATRate, reducedVATRate
			from countrymanagement$country) co on dsco.countryManagement$countryID = co.id		
	order by c.commodityCode;


select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	c.id, c.commodityCode, c.commodityName
from 
		vat$commodity c
	inner join
		(select product$productFamilyID, vat$commodityID
		from product$productfamily_commodity) pfc on c.id = pfc.vat$commodityID
	inner join
		(select id, magentoProductID, magentoSKU, name
		from product$productfamily) pf on pfc.product$productFamilyID = pf.id
order by pf.magentoProductID;

select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
	c.id, c.commodityCode, c.commodityName,
	ds.id, ds.dispensingServicesRate, ds.rateEffectiveDate, ds.rateEndDate
from 
		vat$commodity c
	inner join
		(select product$productFamilyID, vat$commodityID
		from product$productfamily_commodity) pfc on c.id = pfc.vat$commodityID
	inner join
		(select id, magentoProductID, magentoSKU, name
		from product$productfamily) pf on pfc.product$productFamilyID = pf.id
	inner join
		(select vat$dispensingServicesRateID, vat$commodityID 
		from vat$dispensingservices_commodity) dsc on c.id = dsc.vat$commodityID 
	inner join
		(select id, 
			dispensingServicesRate, 
			rateEffectiveDate, rateEndDate
		from vat$dispensingservicesrate) ds on dsc.vat$dispensingServicesRateID = ds.id
order by pf.magentoProductID;





-- VAT Rate, Rating
	--> VAT Rating: Different rate types
		-- Who establish these values (Standard Rate, Reduced Rate, Reduced Rate Other, New Reduced Rate, Zero Rate, Exempt, Sales another EC Country, Sales outside EC)
		-- Should not be more focused on Commodity
	--> VAT Rate: Rate values
		-- Related to VAT Rating Rate Types
		-- Effective Date - End Date Values - Used??
	--> Country Relationship
		-- Vat Rating Rate Types + Related Vat Rates are related to Countries
		-- What is the Country meaning there? Website Country - Shipping Country? 

select *
from vat$vatrating;

	select vatRatingDescription, count(*)
	from vat$vatrating
	group by vatRatingDescription
	order by vatRatingDescription
	
select *
from vat$vatrate;

select vr1.vatRatingCode, vr1.vatRatingDescription, -- vr1.id, 
	vr2.vatRate, vr2.vatRateEffectiveDate, vr2.vatRateEndDate -- vr2.id, 
from 
		vat$vatrating vr1
	inner join
		(select vat$vatRateID, vat$vatRatingID 
		from vat$vatrate_vatrating) vr1vr2 on vr1.id = vr1vr2.vat$vatRatingID 	
	inner join
		vat$vatrate vr2 on vr1vr2.vat$vatRateID = vr2.id
order by vr1.vatRatingDescription, vr1.vatRatingCode
		
select c.id, c.continent, c.countryName, c.iso_2, c.iso_3, c.taxationArea, c.standardVATRate, c.reducedVATRate,
	vr1.vatRatingCode, vr1.vatRatingDescription, -- vr1.id, 
	vr2.vatRate, vr2.vatRateEffectiveDate, vr2.vatRateEndDate -- vr2.id, 
from 
		vat$vatrating vr1
	inner join
		(select vat$vatRateID, vat$vatRatingID 
		from vat$vatrate_vatrating) vr1vr2 on vr1.id = vr1vr2.vat$vatRatingID 	
	inner join
		vat$vatrate vr2 on vr1vr2.vat$vatRateID = vr2.id
	inner join
		(select vat$vatRatingID, countryManagement$countryID 
		from vat$vatrating_country) vrc on vr1.id = vrc.vat$vatRatingID
	inner join
		(select id, continent, countryName, iso_2, iso_3, taxationArea, standardVATRate, reducedVATRate
		from countrymanagement$country) c on vrc.countryManagement$countryID = c.id			
order by c.countryName, vr1.vatRatingCode, vr1.vatRatingDescription;

	

-- VAT Schedule
	--> VAT Schedule
		-- Meaning: ???
		-- VAT Rating Relationship: Related to Vat Rating Rate Types
select *
from vat$vatschedule

	select vatsaletype, count(*)
	from vat$vatschedule
	group by vatsaletype

	select taxationarea, count(*)
	from vat$vatschedule
	group by taxationarea

select c.id, c.continent, c.countryName, c.iso_2, c.iso_3, c.taxationArea, c.standardVATRate, c.reducedVATRate,
	vr1.vatRatingCode, vr1.vatRatingDescription, -- vr1.id, 
	vr2.vatRate, vr2.vatRateEffectiveDate, vr2.vatRateEndDate, -- vr2.id, 
	vs.taxationArea, vs.vatSaleType, vs.scheduleEffectiveDate, vs.scheduleEndDate
from 
		vat$vatrating vr1
	inner join
		(select vat$vatRateID, vat$vatRatingID 
		from vat$vatrate_vatrating) vr1vr2 on vr1.id = vr1vr2.vat$vatRatingID 	
	inner join
		vat$vatrate vr2 on vr1vr2.vat$vatRateID = vr2.id
	inner join
		(select vat$vatRatingID, countryManagement$countryID 
		from vat$vatrating_country) vrc on vr1.id = vrc.vat$vatRatingID
	inner join
		(select id, continent, countryName, iso_2, iso_3, taxationArea, standardVATRate, reducedVATRate
		from countrymanagement$country) c on vrc.countryManagement$countryID = c.id			
	inner join
		(select vat$vatScheduleID, vat$vatRatingID 
		from vat$vatschedule_vatrating) vsvr on vr1.id = vsvr.vat$vatRatingID 
	inner join	
		(select id, 
			taxationArea, vatSaleType, 
			scheduleEffectiveDate, scheduleEndDate
		from vat$vatschedule) vs on vsvr.vat$vatScheduleID = vs.id		
order by c.countryName, vr1.vatRatingCode, vr1.vatRatingDescription;


select co.continent, co.countryName, co.iso_2, --co.id, co.iso_3, co.taxationArea, co.standardVATRate, co.reducedVATRate,
	vr1.vatRatingCode, vr1.vatRatingDescription, -- vr1.id, 
	vr2.vatRate, vr2.vatRateEffectiveDate, vr2.vatRateEndDate, -- vr2.id, 
	vs.taxationArea, vs.vatSaleType, vs.scheduleEffectiveDate, vs.scheduleEndDate, 
	c.id_com, c.commodityCode, c.commodityName, c.id, c.dispensingServicesRate, c.rateEffectiveDate, c.rateEndDate
from 
		vat$vatrating vr1
	inner join
		(select vat$vatRateID, vat$vatRatingID 
		from vat$vatrate_vatrating) vr1vr2 on vr1.id = vr1vr2.vat$vatRatingID 	
	inner join
		vat$vatrate vr2 on vr1vr2.vat$vatRateID = vr2.id
	inner join
		(select vat$vatRatingID, countryManagement$countryID 
		from vat$vatrating_country) vrc on vr1.id = vrc.vat$vatRatingID
	inner join
		(select id, continent, countryName, iso_2, iso_3, taxationArea, standardVATRate, reducedVATRate
		from countrymanagement$country) co on vrc.countryManagement$countryID = co.id			
	inner join
		(select vat$vatScheduleID, vat$vatRatingID 
		from vat$vatschedule_vatrating) vsvr on vr1.id = vsvr.vat$vatRatingID 
	inner join	
		(select id, 
			taxationArea, vatSaleType, 
			scheduleEffectiveDate, scheduleEndDate
		from vat$vatschedule) vs on vsvr.vat$vatScheduleID = vs.id		
	inner join
		(select vat$vatScheduleID, vat$commodityID 
		from vat$vatschedule_commodity) vsc on vs.id = vsc.vat$vatScheduleID
	inner join
		(select c.id id_com, c.commodityCode, c.commodityName,
			ds.id, ds.dispensingServicesRate, ds.rateEffectiveDate, ds.rateEndDate
		from 
				vat$commodity c
			left join
				(select vat$dispensingServicesRateID, vat$commodityID 
				from vat$dispensingservices_commodity) dsc on c.id = dsc.vat$commodityID 
			left join
				(select id, 
					dispensingServicesRate, 
					rateEffectiveDate, rateEndDate
				from vat$dispensingservicesrate) ds on dsc.vat$dispensingServicesRateID = ds.id) c on vsc.vat$commodityID = c.id_com
order by co.countryName, vr1.vatRatingCode, vr1.vatRatingDescription;




