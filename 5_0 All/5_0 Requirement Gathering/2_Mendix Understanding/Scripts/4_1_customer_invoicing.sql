﻿
-- Customer Invoice
select id, 
	invoiceNumber, status, invoiceDate, 
	netTotalGoods, VAT, grossTotal, netCarriage,
	numPacks, 
	countryISO2, currency,
	customerVATReg,
	createdDate
from customerinvoicing$customerinvoice
order by invoiceDate;

-- Customer Invoice Line
select id, 
	SKU, productSKU, description, reference,
	packQuantity, formattedPackQty, packPrice, VATRate, formattedVATRate, 
	netPrice, VATAmount, totalPrice, formattedTotalPrice, 
	createdDate 
from customerinvoicing$customerinvoiceline
limit 1000;

-- Customer Shipment Invoice
select id, 
	shipmentNumber, orderNumber, 
	dispatchDate, 
	warehouse, address,
	countryISO2, currency, 
	numPacks, netTotalGoods, netCarriage, netTotal, VAT, grossTotal, excluded
from customerinvoicing$customershipmentinvoice
order by warehouse, ordernumber;

-- Customer Invoice Doc
select id
from customerinvoicing$customerinvoicedoc;

------------------------------------------------------------------------------------------

	select *
	from customerinvoicing$customerinvoice_customerinvoicecarriage;
	
	select *
	from customerinvoicing$customerinvoiceline_customershipmentinvoice
	limit 1000;
	
	select *
	from customerinvoicing$customershipmentinvoice_customerinvoice;
	
	select *
	from customerinvoicing$customerinvoicedocument_customerinvoice;

------------------------------------------------------------------------------------------

	select *
	from customerinvoicing$customerinvoice_customerentity;
	
	select *
	from customerinvoicing$customershipmentinvoice_customerentity;
