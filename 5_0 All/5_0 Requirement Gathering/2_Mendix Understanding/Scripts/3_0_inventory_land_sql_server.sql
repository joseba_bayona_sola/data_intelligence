
select id, 
	code, name, shortName, warehouseType, snapWarehouse, 
	snapCode, scurriCode, 
	active, wms_active, useHoldingLabels
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$warehouse"')
order by code;




-- Suplier Routine
select id, supplierName,
	mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
	tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
	wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
	thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
	fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
	saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
	sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$supplierroutine"')
order by suppliername

	
	select inventory$supplierRoutineID, inventory$warehouseSupplierID
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."inventory$supplierroutine_warehousesupplier"')
	order by inventory$warehouseSupplierID