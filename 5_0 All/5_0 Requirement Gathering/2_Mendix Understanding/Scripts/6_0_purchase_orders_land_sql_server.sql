

select id, 
	ordernumber, source, status, 
	leadtime, 
	itemcost, totalprice, formattedprice, 
	duedate, receiveddate, createddate, 
	approveddate, approvedby, confirmeddate, confirmedby, submitteddate, submittedby, completeddate, completedby
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorder" limit 1000')

	select purchaseorders$purchaseorderid, suppliers$supplierid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorder_supplier" limit 1000')

	select purchaseorders$purchaseorderid, inventory$warehouseid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorder_warehouse" limit 1000')



select id, 
	status, problems, 
	lines, items, totalprice, 
	createddate, changeddate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorderlineheader" limit 1000')

	select purchaseorders$purchaseorderlineheaderid, purchaseorders$purchaseorderid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorderlineheader_purchaseorder" limit 1000')

	select purchaseorders$purchaseorderlineheaderid, product$productfamilyid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorderlineheader_productfamily" limit 1000')

	select purchaseorders$purchaseorderlineheaderid, product$packsizeid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorderlineheader_packsize" limit 1000')

	select purchaseorders$purchaseorderlineheaderid, suppliers$supplierpriceid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorderlineheader_supplierprices" limit 1000')


select id, 
	po_lineID, stockItemID,
	quantity, quantityReceived, quantityDelivered, quantityInTransit, quantityPlanned, quantityOpen, quantityUnallocated, quantityCancelled, quantityExWorks, quantityRejected
	lineprice, 
	backtobackduedate,
	createdDate, changedDate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorderline" limit 1000')

	select purchaseorders$purchaseorderlineid, purchaseorders$purchaseorderlineheaderid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorderline_purchaseorderlineheader" limit 1000')

	select purchaseorders$purchaseorderlineid, product$stockitemid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$purchaseorderline_stockitem" limit 1000')
