﻿
-- Warehouse Shipment 
select id, 
	shipmentID, orderNumber, receiptID, receiptNumber, invoiceRef,
	status, shipmentType, 
	stockRegisteredDate, dueDate, confirmedDate, arrivedDate,
	totalItemPrice, interCompanyCarriagePrice, inboundCarriagePrice, interCompanyProfit, duty,
	lock,
	auditComment,
	createdDate
from warehouseshipments$shipment
order by orderNumber
limit 1000;

-- Shipment Order Line
select id, 
	line, stockItem,
	status, syncStatus, syncResolution, created, processed,
	quantityOrdered, quantityReceived, quantityRejected, quantityAccepted, quantityReturned, 
	unitCost, 
	auditComment
from warehouseshipments$shipmentorderline
limit 1000;

-- Repair Stock Dates
select id, 
	oldArrivedDate, oldConfirmedDate, 
	newArrivedDate, newConfirmedDate, 
	error, errorDetail, 
	createdDate
from warehouseshipments$repairstockdates
order by createdDate;

-- Shipment Import Run
select id
from warehouseshipments$shipmentimportrun;

-- Shipment Processor
select id, 
	status, 
	batchSize, 
	dateStarted, dateRequested, timeTaken, 
	linesProcessed, totalLines, remaining, autoStart, --offset, 
	errorMessage
from warehouseshipments$shipmentprocessor
limit 1000;

-- Shipment Document
select id
from warehouseshipments$shipmentdocument;

-- Shipment Order Export Line
select id, 
	purchaseOrder, receiptID, 
	productCode, quantity, packsize
from warehouseshipments$shipmentorderexportline;

-- Shipment Order Import Line
select id, 
	purchaseOrder, 
	productCode, quantity, packsize
from warehouseshipments$shipmentorderimportline;

-- Over Delivery
select id, 
	status, 
	quantity, createdDate
from warehouseshipments$overdelivery;

--------------------------------------------------------------------------

	select *
	from warehouseshipments$shipmentorderline_shipment
	limit 1000;
	
	select *
	from warehouseshipments$repairstockdates_shipment
	limit 1000;
	
	select *
	from warehouseshipments$shipmentimportrun_shipment;
	
	select *
	from warehouseshipments$shipmentprocessor_shipment
	limit 1000;
	
	select *
	from warehouseshipments$shipmentdocument_shipment;
	
	select *
	from warehouseshipments$shipmentorderexportline_shipmentdocument;
	
	select *
	from warehouseshipments$shipmentorderimportline_shipmentdocument;
	
	select *
	from warehouseshipments$overdelivery_shipment

--------------------------------------------------------------------------

	select *
	from warehouseshipments$shipment_warehouse
	limit 1000;
	
	select *
	from warehouseshipments$shipment_supplier
	limit 1000;
	
	select *
	from warehouseshipments$shipment_supplierinvoice;
	
	select *
	from warehouseshipments$backtobackshipment_purchaseorder
	limit 1000;
	
	select *
	from warehouseshipments$shipmentorderline_purchaseorderline
	limit 1000
	
	select *
	from warehouseshipments$shipmentorderline_snapreceiptline
	limit 1000;
	
	select *
	from warehouseshipments$shipmentorderline_supplier
	limit 1000;
	
	select *
	from warehouseshipments$overdelivery_purchaseorderline;
	
	select *
	from warehouseshipments$overdelivery_snapreceiptline;
	
	select *
	from warehouseshipments$overdelivery_assignedto;
	
