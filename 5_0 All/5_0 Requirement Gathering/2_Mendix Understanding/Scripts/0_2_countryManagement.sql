﻿
-- Country
select id, 
	continent, countryName,
	iso_2, iso_3, numCode, 
	selected, 
	currencyFormat, 
	taxationArea, standardVATRate, reducedVATRate
from countrymanagement$country
order by continent, countryName;

-- Currency
select id, 
	currencyCode, currencyName, 
	standardRate, spotRate, 
	lastStandardUpdate, lastSpotUpdate,
	createdDate, changedDate, 
	system$owner, system$changedby
from countrymanagement$currency
order by currencyCode;

-- Spot Rate
select id, 
	dataSource, effectiveDate, value, 
	createdDate
from countrymanagement$spotrate
order by dataSource, effectiveDate, value;

------------------------------------------------------------------------------

	select *
	from countrymanagement$country_currency;
	
	select *
	from countrymanagement$currentspotrate_currency;
	
	select *
	from countrymanagement$historicalspotrate_currency;

------------------------------------------------------------------------------

-- Currency Format 
select id, currencyCode, currencyFormat
from currencyformat$currency
