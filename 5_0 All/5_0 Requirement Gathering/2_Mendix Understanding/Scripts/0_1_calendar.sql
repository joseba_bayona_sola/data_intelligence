﻿

-- Shipping Method (Y)
select id, 
	magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
	leadtime, letterRate1, letterRate2, tracked, invoicedocs,
	mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking
	mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime	
from calendar$shippingmethod
order by magentoShippingDescription;

	select *
	from calendar$shippingmethods_warehouse;

	select *
	from calendar$shippingmethods_destinationcountry;

	
-- Default Calendar (Y)
select id, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby
from calendar$defaultcalendar;

	select *
	from calendar$defaultcalendar_warehouse;

-- Warehouse Calendar
select id, 
	working, day, cutofftime, date
from calendar$warehousecalendar
order by date;

	select *
	from calendar$calendar_warehouse;


-- Shipping Cut Offs
select id, 
	working, deliveryWorking, cutofftime, collectiontime
from calendar$shippingcutoffs
order by working, cutofftime, collectiontime, deliveryWorking;

	select *
	from calendar$shippingcutoffs_warehousecalendar;
	
	select *
	from calendar$shippingcutoffs_shippingmethods;
	









-- Default Warehouse Supplier Calendar (Y)
select id, 
	mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
	mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
	mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby 
from calendar$defaultwarehousesuppliercalender;

	select *
	from calendar$defaultwarehousesuppliercalender_warehouse;

-- Supplier Calendar
select id, 
	day, date
from calendar$warehousesuppliercalendar
order by date;

	select *
	from calendar$warehousesuppliercalendar_warehouse;
	
-- Supplier Cut Offs
select id, 
	orderOn, suppliesOn, cutofftime
from calendar$suppliercutoffs
order by orderOn, suppliesOn, cutofftime;

	select *
	from calendar$suppliercutoffs_warehousesuppliercalendar;

	select *
	from calendar$suppliercutoffs_warehousesupplier;

	select *
	from calendar$suppliercutoffs_supplierroutine;


-- Supllier Exception Dates
select id, 
	acceptOrders, deliverOrders, orderCutOffTime, date
from calendar$supplierexceptiondates;

-----------------------------------------------------------------------------
	


-----------------------------------------------------------------------------

	

	
	

	
	
	

	


