
-- Company
select id, companyregnum, companyname, code
from openquery(MENDIX_DB_LOCAL, 'select * from "companyorganisation$company"')
order by companyname;

-- Company Mag Web Store
select id, 
	storeid, storename, language, country, 
	storecurrencycode, storeToOrderRate, storeToBaseRate,
	snapcode, 
	dispensingstatement, 
	system$owner, system$changedby
	createddate, changeddate
from openquery(MENDIX_DB_LOCAL, 'select * from "companyorganisation$magwebstore"')
order by storeid;

-- Country
select id,
	countryname, iso_2, iso_3, numcode, 
	currencyformat, 
	taxationarea, standardvatrate, reducedvatrate, 
	selected
from openquery(MENDIX_DB_LOCAL, 'select * from "countrymanagement$country"')
order by countryname