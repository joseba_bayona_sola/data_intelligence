﻿
-- Order 
select id, 
	magentoOrderID, incrementID, orderLinesMagento, orderLinesDeduped,
	_type, orderStatus, shipmentStatus, statusUpdated, 
	couponCode,
	shippingMethod, shippingDescription, paymentMethod, 
	automatic_reorder,
	allocationStatus, 
	labelRenderer,
	totalNetPrice, totalPacksOrdered,
	orderCurrencyCode, globalCurrencyCode,
	createdDate, allocatedDate, promisedShippingDate, promisedDeliveryDate  
from orderprocessing$order
--where shipmentStatus = 'Full'
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
order by createdDate desc
limit 1000;

	-- Customer - Store
	select o.id, 
		st.storeid, st.storename, cu.customerid, cu.email, cu.firstName, cu.lastName,
		o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
		o._type, o.orderStatus, o.shipmentStatus, o.statusUpdated, 
		o.couponCode,
		o.shippingMethod, o.shippingDescription, o.paymentMethod, 
		o.automatic_reorder,
		o.allocationStatus, 
		o.labelRenderer,
		o.totalNetPrice, o.totalPacksOrdered,
		o.orderCurrencyCode, o.globalCurrencyCode,
		o.createdDate, o.allocatedDate, o.promisedShippingDate, o.promisedDeliveryDate
	from 
			orderprocessing$order o
		inner join
			(select cu.customerid, cu.email, cu.firstName, cu.lastName, ocu.orderprocessing$orderID
			from
					(select orderprocessing$orderID, customer$retailCustomerID
					from orderprocessing$order_customer) ocu
				inner join
					(select ce.id, ce.customerid, ce.subMetaObjectName,
						rc.email, rc.phoneNumber, rc.prefix, rc.suffix, rc.firstName, rc.lastName, rc.middleName, rc.taxvat
					from 
							(select id, customerid, subMetaObjectName
							from customer$customerentity) ce
						inner join
							(select id, 
								email, phoneNumber, 
								prefix, suffix, firstName, lastName, middleName, 
								taxvat
							from customer$retailcustomer) rc on ce.id = rc.id) cu on ocu.customer$retailCustomerID = cu.id) cu on o.id = cu.orderprocessing$orderID
		inner join
			(select st.id, st.storeid, st.storename, ost.orderprocessing$orderID
			from	
					(select orderprocessing$orderID, companyorganisation$magWebStoreID
					from orderprocessing$order_magwebstore) ost
				inner join
					(select id, storeid, storename
					from companyorganisation$magwebstore) st on ost.companyorganisation$magWebStoreID = st.id) st on o.id = st.orderprocessing$orderID
	where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
	order by createdDate desc
	limit 1000;

-- Base Values
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, o.statusUpdated, 
	o.couponCode,
	o.shippingMethod, o.shippingDescription, o.paymentMethod, 
	o.automatic_reorder,
	o.allocationStatus, 
	o.labelRenderer,
	o.totalNetPrice, o.totalPacksOrdered,
	o.orderCurrencyCode, o.globalCurrencyCode,
	o.createdDate, o.allocatedDate, o.promisedShippingDate, o.promisedDeliveryDate, 
	bv.*
from 
		orderprocessing$order o
	inner join
		(select bv.*, bvo.orderProcessing$orderID
		from 
				(select orderprocessing$baseValuesID, orderProcessing$orderID
				from orderprocessing$basevalues_order) bvo
			inner join	
				(select id, 
					subtotal, subtotalCanceled, subtotalInvoiced, subtotalRefunded, 
					discountAmount, discountCanceled, discountInvoiced, discountRefunded, 
					shippingAmount, shippingCanceled, shippingInvoiced, shippingRefunded, 
					totalCanceled, totalInvoiced, totalRefunded, 
					adjustmentNegative, adjustmentPositive, 
					subtotalInclTax,
					custBalanceAmount, customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
					grandTotal, 
					toOrderRate, toGlobalRate, currencyCode
				from orderprocessing$basevalues) bv on bvo.orderprocessing$baseValuesID = bv.id) bv on o.id = bv.orderProcessing$orderID
		
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
order by createdDate desc
limit 1000;



---

-- Order Line Magento
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, o.statusUpdated, 
	o.couponCode,
	o.shippingMethod, o.shippingDescription, o.paymentMethod, 
	o.automatic_reorder,
	o.allocationStatus, 
	o.labelRenderer,
	o.totalNetPrice, o.totalPacksOrdered,
	o.orderCurrencyCode, o.globalCurrencyCode,
	o.createdDate, o.allocatedDate, o.promisedShippingDate, o.promisedDeliveryDate, 
	olm.*
from 
		orderprocessing$order o
	inner join
		(select olm.*, olmo.orderprocessing$orderID
		from
			(select orderprocessing$orderLineMagentoID, orderprocessing$orderID
			from orderprocessing$orderlinemagento_order) olmo
		inner join	
			(select olm.id, olm.lineAdded, olm.deduplicate, olm.fulfilled, 
				ola.magentoItemID, ola.orderLineType, ola.subMetaObjectName,
				ola.sku, ola.eye, ola.lensGroupID, 
				ola.basePrice, ola.baseRowPrice, 
				ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityShipped, ola.quantityAllocated, ola.quantityIssued,  
				ola.netPrice, ola.netRowPrice,
				ola.allocated
			from
					(select id, lineAdded, deduplicate, fulfilled
					from orderprocessing$orderlinemagento) olm
				inner join
					(select id, 
						magentoItemID, orderLineType, subMetaObjectName,
						sku, eye, lensGroupID, 
						basePrice, baseRowPrice, 
						quantityOrdered, quantityInvoiced, quantityRefunded, quantityShipped, quantityAllocated, quantityIssued,  
						netPrice, netRowPrice,
						allocated, 
						createdDate, changedDate, 
						system$owner, system$changedBy
					from orderprocessing$orderlineabstract) ola on olm.id = ola.id) olm on olmo.orderprocessing$orderLineMagentoID = olm.id) olm 
						on o.id = olm.orderProcessing$orderID
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
order by createdDate desc, olm.sku
limit 1000;

-- Order Line
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, o.statusUpdated, 
	o.couponCode,
	o.shippingMethod, o.shippingDescription, o.paymentMethod, 
	o.automatic_reorder,
	o.allocationStatus, 
	o.labelRenderer,
	o.totalNetPrice, o.totalPacksOrdered,
	o.orderCurrencyCode, o.globalCurrencyCode,
	o.createdDate, o.allocatedDate, o.promisedShippingDate, o.promisedDeliveryDate, 
	ol.*
from 
		orderprocessing$order o
	inner join
		(select ol.*, olo.orderprocessing$orderID
		from 
			(select orderprocessing$orderLineID, orderprocessing$orderID
			from orderprocessing$orderline_order) olo
		inner join
			(select ol.id, ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped, 
				ola.magentoItemID, ola.orderLineType, ola.subMetaObjectName,
				ola.sku, ola.eye, ola.lensGroupID, 
				ola.basePrice, ola.baseRowPrice, 
				ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityShipped, ola.quantityAllocated, ola.quantityIssued,  
				ola.netPrice, ola.netRowPrice,
				ola.allocated
			from
					(select id, status, packsOrdered, packsAllocated, packsShipped
					from orderprocessing$orderline) ol
				inner join
					(select id, 
						magentoItemID, orderLineType, subMetaObjectName,
						sku, eye, lensGroupID, 
						basePrice, baseRowPrice, 
						quantityOrdered, quantityInvoiced, quantityRefunded, quantityShipped, quantityAllocated, quantityIssued,  
						netPrice, netRowPrice,
						allocated, 
						createdDate, changedDate, 
						system$owner, system$changedBy
					from orderprocessing$orderlineabstract) ola on ol.id = ola.id) ol on olo.orderprocessing$orderLineID = ol.id) ol 
						on o.id = ol.orderProcessing$orderID		
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
order by createdDate desc, ol.sku
limit 1000;


------------------------------

-- Shipping Group
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, o.statusUpdated, 
	o.couponCode,
	o.shippingMethod, o.shippingDescription, o.paymentMethod, 
	o.automatic_reorder,
	o.allocationStatus, 
	o.labelRenderer,
	o.totalNetPrice, o.totalPacksOrdered,
	o.orderCurrencyCode, o.globalCurrencyCode,
	o.createdDate, o.allocatedDate, o.promisedShippingDate, o.promisedDeliveryDate, 
	sg.*
from 
		orderprocessing$order o
	inner join
		(select sg.id, sg.dispensingFee, sg.allocationStatus, sg.shipmentStatus, 
			sg.wholesale, sg.letterBoxAble, sg.onHold, sg.adHoc, 
			sgo.orderprocessing$orderID
		from
				(select orderprocessing$shippingGroupID, orderprocessing$orderID
				from orderprocessing$shippinggroup_order) sgo
			inner join	
				(select id, 
					dispensingFee, 
					allocationStatus, shipmentStatus, 
					wholesale, 
					letterBoxAble, onHold, adHoc
				from orderprocessing$shippinggroup) sg on sgo.orderprocessing$shippingGroupID = sg.id) sg on o.id = sg.orderProcessing$orderID
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
order by createdDate desc
limit 1000;

-- Order Address
select o.id, 
	o.magentoOrderID, o.incrementID, o.orderLinesMagento, o.orderLinesDeduped,
	o._type, o.orderStatus, o.shipmentStatus, o.statusUpdated, 
	o.couponCode,
	o.shippingMethod, o.shippingDescription, o.paymentMethod, 
	o.automatic_reorder,
	o.allocationStatus, 
	o.labelRenderer,
	o.totalNetPrice, o.totalPacksOrdered,
	o.orderCurrencyCode, o.globalCurrencyCode,
	o.createdDate, o.allocatedDate, o.promisedShippingDate, o.promisedDeliveryDate, 
	a.*
from 
		orderprocessing$order o
	inner join
		(select a.*, ao.orderprocessing$orderID
		from
				(select orderprocessing$orderAddressID, orderprocessing$orderID
				from orderprocessing$address_order) ao
			inner join
				(select id, 
					addressID, addressType, 
					customerTitle, customerFirstName, customerMiddleName, customerSurname, customerSuffix, fullName,
					companyName, 
					street1, street2, street3, 
					postCode, city, regionID, region, countryID, 
					email, Telephone, 
					deliveryInstructions--, 
					--createdDate, changedDate, 
					--system$owner, system$changedBy
				from orderprocessing$orderaddress) a on ao.orderprocessing$orderAddressID = a.id) a on o.id = a.orderProcessing$orderID
where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')
order by createdDate desc
limit 1000;

------------------
	-- '8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825'
select ola.*, olp.product$productID, olsi.product$stockItemID, psi.*
from 
		(select id, 
			magentoItemID, orderLineType, subMetaObjectName,
			sku, eye, lensGroupID, 
			basePrice, baseRowPrice, 
			quantityOrdered, quantityInvoiced, quantityRefunded, quantityShipped, quantityAllocated, quantityIssued,  
			netPrice, netRowPrice,
			allocated -- , 
			-- createdDate, changedDate, 
			-- system$owner, system$changedBy
		from orderprocessing$orderlineabstract
		where id in (54324670526327603, 1970324861768598, 54324670526287676, 1970324861722408, 1970324861722407)) ola
	inner join
		(select orderprocessing$orderLineAbstractID, product$productID
		from orderprocessing$orderline_product) olp on ola.id = olp.orderprocessing$orderLineAbstractID
	left join		
		(select orderprocessing$orderLineAbstractID, product$stockItemID
		from orderprocessing$orderline_requiredstockitem) olsi on ola.id = olsi.orderprocessing$orderLineAbstractID
	inner join
		(select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
			p.id productID, p.productType, p.SKU, p.oldSKU, p.description, 
			-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
			-- si.id, 
			si.id stockItemID, si.SKU SKU_SI, si.oldSKU, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
		from 
				product$productfamily pf
			inner join
				(select product$productID, product$productFamilyID 
				from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
			inner join
				(select id, 
					productType, 
					SKU, oldSKU, description, 
					subMetaObjectName, price,
					isBOM, valid, displaySubProductOnPackingSlip,
					createdDate, changedDate, 
					system$owner, system$changedBy	
				from product$product) p on pfp.product$productID = p.id		
			inner join
				(select product$stockItemID, product$productID 
				from product$stockitem_product) sip on p.id = sip.product$productID 
			inner join
				(select id, 
					SKU, oldSKU, packSize, 
					manufacturerArticleID, manufacturerCodeNumber,
					SNAPDescription, SNAPUploadStatus, 
					changed
				from product$stockitem) si on sip.product$stockItemID = si.id) psi 
					on olp.product$productID = psi.productID -- and olsi.product$stockItemID = psi.stockItemID
order by psi.sku, ola.id				