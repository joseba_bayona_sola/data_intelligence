﻿select sa.id, 
	w.id, w.code, w.shortName,
	o.*, 
	sa.transactionID, sa.timestamp,
	sa.allocationType, sa.recordType, 
	sa.allocatedUnits, sa.allocatedStockQuantity, sa.issuedQuantity, sa.packSize, 
	sa.stockAllocated, sa.cancelled, 
	sa.issuedDateTime
from 
		stockallocation$orderlinestockallocationtransaction sa
	inner join
		(select w.id, w.code, w.shortName, saw.stockallocation$orderLineStockAllocationTransactionID
		from 
				(select stockallocation$orderLineStockAllocationTransactionID, inventory$warehouseID
				from stockallocation$at_warehouse) saw		
			inner join
				(select id, code, shortName
				from inventory$warehouse) w on saw.inventory$warehouseID = w.id) w on sa.id = w.stockallocation$orderLineStockAllocationTransactionID	
	inner join
		(select o.*, sao.stockallocation$orderLineStockAllocationTransactionID
		from
			(select stockallocation$orderLineStockAllocationTransactionID, orderprocessing$orderID
			from stockallocation$at_order) sao
		inner join
			(select id, 
				magentoOrderID, incrementID, orderLinesMagento, orderLinesDeduped,
				_type, orderStatus, shipmentStatus, 
				allocationStatus, 
				createdDate, allocatedDate, promisedShippingDate, promisedDeliveryDate
			from orderprocessing$order
			where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')) o on sao.orderprocessing$orderID = o.id) o
				on sa.id = o.stockallocation$orderLineStockAllocationTransactionID
order by o.incrementID
limit 1000;

	select sa.id, 
		w.id, w.code, w.shortName,
		o.*, ol.*,
		sa.transactionID, sa.timestamp,
		sa.allocationType, sa.recordType, 
		sa.allocatedUnits, sa.allocatedStockQuantity, sa.issuedQuantity, sa.packSize, 
		sa.stockAllocated, sa.cancelled, 
		sa.issuedDateTime
	from 
			stockallocation$orderlinestockallocationtransaction sa
		inner join
			(select w.id, w.code, w.shortName, saw.stockallocation$orderLineStockAllocationTransactionID
			from 
					(select stockallocation$orderLineStockAllocationTransactionID, inventory$warehouseID
					from stockallocation$at_warehouse) saw		
				inner join
					(select id, code, shortName
					from inventory$warehouse) w on saw.inventory$warehouseID = w.id) w on sa.id = w.stockallocation$orderLineStockAllocationTransactionID	
		inner join
			(select o.*, sao.stockallocation$orderLineStockAllocationTransactionID
			from
				(select stockallocation$orderLineStockAllocationTransactionID, orderprocessing$orderID
				from stockallocation$at_order) sao
			inner join
				(select id, 
					magentoOrderID, incrementID, orderLinesMagento, orderLinesDeduped,
					_type, orderStatus, shipmentStatus, 
					allocationStatus, 
					createdDate, allocatedDate, promisedShippingDate, promisedDeliveryDate
				from orderprocessing$order
				where incrementID in ('8002327128', '8002395321', '8002290953', '8002314962', '8002380227', '8002323825')) o on sao.orderprocessing$orderID = o.id) o
					on sa.id = o.stockallocation$orderLineStockAllocationTransactionID
		inner join
			(select ol.*, saol.stockallocation$orderLineStockAllocationTransactionID
			from
				(select stockallocation$orderLineStockAllocationTransactionID, orderprocessing$orderLineID
				from stockallocation$at_orderline) saol
			inner join
				(select ol.id, ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped, 
					ola.magentoItemID, ola.orderLineType, ola.subMetaObjectName,
					ola.sku, ola.eye, ola.lensGroupID, 
					ola.basePrice, ola.baseRowPrice, 
					ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityShipped, ola.quantityAllocated, ola.quantityIssued,  
					ola.netPrice, ola.netRowPrice,
					ola.allocated
				from
						(select id, status, packsOrdered, packsAllocated, packsShipped
						from orderprocessing$orderline) ol
					inner join
						(select *
						from orderprocessing$orderlineabstract) ola on ol.id = ola.id) ol on saol.orderprocessing$orderLineID = ol.id) ol 
							on sa.id = ol.stockallocation$orderLineStockAllocationTransactionID	
	order by o.incrementID
	limit 1000;
