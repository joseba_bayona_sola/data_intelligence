﻿
-- Warehouse
select id, 
	code, name, shortName, warehouseType, snapWarehouse, 
	snapCode, scurriCode, 
	active, wms_active, useHoldingLabels
from inventory$warehouse
order by code;

-- Suplier Routine
select id, supplierName,
	mondayDefaultCanSupply, mondayOrderCutOffTime, mondayDefaultCanOrder, 
	tuesdayDefaultCanSupply, tuesdayOrderCutOffTime, tuesdayDefaultCanOrder, 
	wednesdayDefaultCanSupply, wednesdayOrderCutOffTime, wednesdayDefaultCanOrder, 
	thursdayDefaultCanSupply, thursdayOrderCutOffTime, thursdayDefaultCanOrder, 
	fridayDefaultCanSupply, fridayOrderCutOffTime, fridayDefaultCanOrder,  
	saturdayDefaultCanSupply, saturdayOrderCutOffTime, saturdayDefaultCanOrder,
	sundayDefaultCanSupply, sundayOrderCutOffTime, sundayDefaultCanOrder
from inventory$supplierroutine
order by suppliername

	--select inventory$warehouseSupplierID, inventory$warehouseID
	--from inventory$warehousesupplier_warehouse
		
	select inventory$supplierRoutineID, inventory$warehouseSupplierID
	from inventory$supplierroutine_warehousesupplier
	order by inventory$warehouseSupplierID