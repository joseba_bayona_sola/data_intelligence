
select id, 
	transfernum, allocatedstrategy, 
	totalremaining,
	createddate, changeddate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$transferheader"')
order by createddate

	select intersitetransfer$transferheaderid, purchaseorders$purchaseorderid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$supplypo_transferheader"')

	select intersitetransfer$transferheaderid, purchaseorders$purchaseorderid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$transferpo_transferheader"')


select id, createddate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$intransitbatchheader"')

	select intersitetransfer$intransitbatchheaderid, intersitetransfer$transferheaderid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$intransitbatchheader_transferheader"')

	select intersitetransfer$intransitbatchheaderid, customershipments$customershipmentid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$intransitbatchheader_customershipment"')

	
select id, 
	issuedquantity, remainingquantity, 
	used,
	productunitcost, carriageUnitCost, totalunitcost, totalUnitCostIncInterCo,
	currency,
	groupFIFOdate, arriveddate, confirmeddate, stockregistereddate, 
	createddate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$intransitstockitembatch"')

	select intersitetransfer$intransitstockitembatchid, intersitetransfer$transferheaderid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$intransitstockitembatch_transferheader"')

	select intersitetransfer$intransitstockitembatchid, intersitetransfer$intransitbatchheaderid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$intransitstockitembatch_intransitbatchheader"')


	select intersitetransfer$intransitstockitembatchid, product$stockitemid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$intransitstockitembatch_stockitem"')
	
	select intersitetransfer$intransitstockitembatchid, warehouseshipments$shipmentid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."intersitetransfer$intransitstockitembatch_shipment"')



	