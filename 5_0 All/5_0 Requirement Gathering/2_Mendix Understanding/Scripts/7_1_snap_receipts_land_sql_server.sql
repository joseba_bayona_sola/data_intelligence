
select id, receiptid, 
	lines, lineqty, 	
	receiptStatus, receiptprocessStatus, processDetail,  priority, 
	status, stockstatus, 
	ordertype, orderclass, 
	suppliername, consignmentID, 
	weight, actualWeight, volume, 
	dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
	datesfixed
-- select *
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."snapreceipts$snapreceipt"')

	select snapreceipts$snapreceiptid, warehouseshipments$shipmentid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."snapreceipts$snapreceipt_shipment"')


select id, 
	line, skuid, 
	status, level, unitofmeasure, 
	qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected	
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."snapreceipts$snapreceiptline" limit 1000')

	select snapreceipts$snapreceiptlineid, snapreceipts$snapreceiptid	
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."snapreceipts$snapreceiptline_snapreceipt" limit 1000')

	select snapreceipts$snapreceiptlineid, product$stockitemid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."snapreceipts$snapreceiptline_stockitem" limit 1000')


