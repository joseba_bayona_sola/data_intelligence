﻿
-- Default Calendar
select w.code, w.name, w.shortName, w.warehouseType, w.snapCode, w.scurriCode, 
	c.mondayWorking, c.tuesdayWorking, c.wednesdayWorking, c.thursdayWorking, c.fridayWorking, c.saturdayWorking, c.sundayWorking, 
	c.mondayCutOff, c.tuesdayCutOff, c.wednesdayCutOff, c.thursdayCutOff, c.fridayCutOff, c.saturdayCutOff, c.sundayCutOff, 
	c.timeHorizonWeeks
from 
		calendar$defaultcalendar c
	inner join
		(select calendar$defaultCalendarID, inventory$warehouseID
		from calendar$defaultcalendar_warehouse) cw on c.id = cw.calendar$defaultCalendarID
	inner join
		(select id, code, name, shortName, warehouseType, snapCode, scurriCode 
		from inventory$warehouse) w on cw.inventory$warehouseID = w.id
order by w.code

-- Warehouse Calendar
select w.code, w.name, w.shortName, w.warehouseType, w.snapCode, w.scurriCode, 
	wc.id, wc.working, wc.date, wc.day, wc.cutofftime
from 
		calendar$warehousecalendar wc
	inner join
		(select calendar$warehouseCalendarID, inventory$warehouseID
		from calendar$calendar_warehouse) cw on wc.id = cw.calendar$warehouseCalendarID
	inner join
		(select id, code, name, shortName, warehouseType, snapCode, scurriCode 
		from inventory$warehouse) w on cw.inventory$warehouseID = w.id
order by w.code, date;

	-- Warehouse Calendar - Shipping Method - Cut Off
	select wc.id, w.code, w.name, w.shortName, w.warehouseType, w.snapCode, w.scurriCode, 
		wc.id, wc.working, wc.day, wc.date, wc.cutofftime, 
		sm.magentoShippingDescription, sm.magentoShippingMethod, sm.scurriShippingMethod, sm.snapShippingMethod, sm.leadtime,
		sco.working, sco.cutofftime, sco.collectiontime, sco.deliveryWorking
	from 
			calendar$warehousecalendar wc
		inner join
			(select calendar$warehouseCalendarID, inventory$warehouseID
			from calendar$calendar_warehouse) cw on wc.id = cw.calendar$warehouseCalendarID
		inner join
			(select id, code, name, shortName, warehouseType, snapCode, scurriCode 
			from inventory$warehouse) w on cw.inventory$warehouseID = w.id
		inner join
			(select calendar$shippingCutOffsID, calendar$warehouseCalendarID
			from calendar$shippingcutoffs_warehousecalendar) scow on wc.id = scow.calendar$warehouseCalendarID
		inner join
			(select id, working, cutofftime, collectiontime, deliveryWorking
			from calendar$shippingcutoffs) sco on scow.calendar$shippingCutOffsID = sco.id			
		inner join
			(select calendar$shippingCutOffsID, calendar$shippingMethodID
			from calendar$shippingcutoffs_shippingmethods) scosm on sco.id = scosm.calendar$shippingCutOffsID
		inner join	
			(select id, magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, leadtime
			from calendar$shippingmethod) sm on scosm.calendar$shippingMethodID = sm.id
	order by w.code, date, magentoShippingDescription;



-- Supplier Warehouse Calendar
select w.code, w.name, w.shortName, w.warehouseType, w.snapCode, w.scurriCode,
	wc.mondayCanOrder, wc.tuesdayCanOrder, wc.wednesdayCanOrder, wc.thursdayCanOrder, wc.fridayCanOrder, wc.saturdayCanOrder, wc.sundayCanOrder, 
	wc.mondayCanSupply, wc.tuesdayCanSupply, wc.wednesdayCanSupply, wc.thursdayCanSupply, wc.fridayCanSupply, wc.saturdayCanSupply, wc.sundayCanSupply, 
	wc.mondayCutOffTime, wc.tuesdayCutOffTime, wc.wednesdayCutOffTime, wc.thursdayCutOffTime, wc.fridayCutOffTime, wc.saturdayCutOffTime, wc.sundayCutOffTime, 
	wc.timeHorizonWeeks
from 
		calendar$defaultwarehousesuppliercalender wc
	inner join
		(select calendar$defaultWarehouseSupplierCalenderID, inventory$warehouseID
		from calendar$defaultwarehousesuppliercalender_warehouse) cw on wc.id = cw.calendar$defaultWarehouseSupplierCalenderID
	inner join
		(select id, code, name, shortName, warehouseType, snapCode, scurriCode 
		from inventory$warehouse) w on cw.inventory$warehouseID = w.id

select w.id, w.code, w.name, w.shortName, w.warehouseType, w.snapCode, w.scurriCode,
	wc.id, wc.date, wc.day 
from 
		calendar$warehousesuppliercalendar wc
	inner join
		(select calendar$WarehouseSupplierCalendarID, inventory$warehouseID
		from calendar$warehousesuppliercalendar_warehouse) cw on wc.id = cw.calendar$WarehouseSupplierCalendarID
	inner join
		(select id, code, name, shortName, warehouseType, snapCode, scurriCode 
		from inventory$warehouse) w on cw.inventory$warehouseID = w.id
order by w.code, date;

	select w.id, w.code, w.name, w.shortName, w.warehouseType, w.snapCode, w.scurriCode,
		wc.id, wc.date, wc.day,  
		s.supplierName,
		sc.orderOn, sc.suppliesOn, sc.cutofftime
	from 
			calendar$warehousesuppliercalendar wc
		inner join
			(select calendar$WarehouseSupplierCalendarID, inventory$warehouseID
			from calendar$warehousesuppliercalendar_warehouse) cw on wc.id = cw.calendar$WarehouseSupplierCalendarID
		inner join
			(select id, code, name, shortName, warehouseType, snapCode, scurriCode 
			from inventory$warehouse) w on cw.inventory$warehouseID = w.id
		inner join	
			(select calendar$supplierCutOffsID, calendar$warehouseSupplierCalendarID
			from calendar$suppliercutoffs_warehousesuppliercalendar) wsc on wc.id = wsc.calendar$warehouseSupplierCalendarID
		inner join		
			(select id, orderOn, suppliesOn, cutofftime
			from calendar$suppliercutoffs) sc on wsc.calendar$supplierCutOffsID = sc.id
		inner join
			(select calendar$supplierCutOffsID, inventory$warehouseSupplierID
			from calendar$suppliercutoffs_warehousesupplier) ws1 on sc.id = ws1.calendar$supplierCutOffsID
		inner join
			(select calendar$supplierCutOffsID, inventory$supplierRoutineID
			from calendar$suppliercutoffs_supplierroutine) sr1 on sc.id = sr1.calendar$supplierCutOffsID
		inner join
			(select ws.id, 
				sr.id id_sr, sr.supplierName
			from
					(select id
					from inventory$warehousesupplier) ws
				inner join
					(select inventory$supplierRoutineID, inventory$warehouseSupplierID
					from inventory$supplierroutine_warehousesupplier) srws on ws.id = srws.inventory$warehouseSupplierID
				inner join
					(select id, supplierName,
						mondayDefaultCanSupply, tuesdayDefaultCanSupply, wednesdayDefaultCanSupply, thursdayDefaultCanSupply, fridayDefaultCanSupply, saturdayDefaultCanSupply, sundayDefaultCanSupply, 
						mondayDefaultCanOrder, tuesdayDefaultCanOrder, wednesdayDefaultCanOrder, thursdayDefaultCanOrder, fridayDefaultCanOrder, saturdayDefaultCanOrder, sundayDefaultCanOrder, 
						mondayOrderCutOffTime, tuesdayOrderCutOffTime, wednesdayOrderCutOffTime, thursdayOrderCutOffTime, fridayOrderCutOffTime, saturdayOrderCutOffTime, sundayOrderCutOffTime
					from inventory$supplierroutine) sr on srws.inventory$supplierRoutineID = sr.id) s 
						on ws1.inventory$warehouseSupplierID = s.id and sr1.inventory$supplierRoutineID	= s.id_sr	
	order by w.code, date, s.supplierName;


	

