﻿-- Purchase Order
select status, count(*)
from purchaseorders$purchaseorder
group by status
order by status;

select source, count(*)
from purchaseorders$purchaseorder
group by source
order by source;

select po.id, 
	po.orderNumber, po.status, po.source, 
	w.code, w.name, w.shortName, w.warehouseType, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
	po.interCompanyCarriage, po.inboundCarriage, 
	po.itemCost, po.totalPrice, po.formattedPrice, po.duty,
	po.leadTime, 
	po.approvedBy, po.approvedDate, po.submittedBy, po.submittedDate, po.confirmedBy, po.confirmedDate, po.completedBy, po.completedDate, 
	po.dueDate, po.receivedDate, po.invoicePaymentDate, po.depositPaidDate, po.paymentDueDate, po.exWorksDate, 
	po.auditComment, po.supplierReference, po.EDIStatus, po.EDIFileButtonVisible, po.EDIFTPButtonVisible,
	po.createdDate
from 
		purchaseorders$purchaseorder po
	inner join
		(select purchaseorders$purchaseOrderID, inventory$warehouseID
		from purchaseorders$purchaseorder_warehouse) pow on po.id = pow.purchaseorders$purchaseOrderID
	inner join
		(select id, code, name, shortName, warehouseType 
		from inventory$warehouse) w on pow.inventory$warehouseID = w.id
	inner join	
		(select purchaseorders$purchaseOrderID, suppliers$supplierID
		from purchaseorders$purchaseorder_supplier) pos on po.id = pos.purchaseorders$purchaseOrderID
	inner join	
		(select id, supplierType, supplierID, vendorCode, supplierName
		from suppliers$supplier) s on pos.suppliers$supplierID = s.id
order by po.orderNumber desc
limit 1000;


-- Purchase Order Line Header
select po.id, 
	po.orderNumber, po.status, po.source, 
	w.code, w.name, w.shortName, w.warehouseType, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
	po.itemCost, po.totalPrice, 
	po.leadTime, 
	po.approvedBy, po.approvedDate, 
	polh.id, polh.headerID, polh.status, polh.problems, polh.lines, polh.items, polh.totalPrice, polh.auditComment, polh.createdDate
from 
		purchaseorders$purchaseorder po
	inner join
		(select purchaseorders$purchaseOrderID, inventory$warehouseID
		from purchaseorders$purchaseorder_warehouse) pow on po.id = pow.purchaseorders$purchaseOrderID
	inner join
		(select id, code, name, shortName, warehouseType 
		from inventory$warehouse) w on pow.inventory$warehouseID = w.id
	inner join	
		(select purchaseorders$purchaseOrderID, suppliers$supplierID
		from purchaseorders$purchaseorder_supplier) pos on po.id = pos.purchaseorders$purchaseOrderID
	inner join	
		(select id, supplierType, supplierID, vendorCode, supplierName
		from suppliers$supplier) s on pos.suppliers$supplierID = s.id
	inner join
		(select purchaseorders$purchaseOrderLineHeaderID, purchaseorders$purchaseOrderID 
		from purchaseorders$purchaseorderlineheader_purchaseorder) polhpo on po.id = polhpo.purchaseorders$purchaseOrderID 
	inner join
		(select id, 
			headerID, status, problems, 
			lines, items, totalPrice, 
			auditComment,
			createdDate, changedDate, 
			system$owner, system$changedBy
		from purchaseorders$purchaseorderlineheader) polh on polhpo.purchaseorders$purchaseOrderLineHeaderID = polh.id
where po.orderNumber > 'P00017215' 		
order by po.orderNumber desc, polh.createdDate
limit 1000;

	select po.id, 
		po.orderNumber, po.status, po.source, 
		w.code, w.name, w.shortName, w.warehouseType, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
		po.itemCost, po.totalPrice, 
		po.leadTime, 
		po.approvedBy, po.approvedDate, 
		polh.id, polh.headerID, pf.magentoProductID, pf.magentoSKU, pf.name, ps.size, s2.unitPrice, 
		polh.status, polh.problems, polh.lines, polh.items, polh.totalPrice, polh.auditComment, polh.createdDate
	from 
			purchaseorders$purchaseorder po
		inner join
			(select purchaseorders$purchaseOrderID, inventory$warehouseID
			from purchaseorders$purchaseorder_warehouse) pow on po.id = pow.purchaseorders$purchaseOrderID
		inner join
			(select id, code, name, shortName, warehouseType 
			from inventory$warehouse) w on pow.inventory$warehouseID = w.id
		inner join	
			(select purchaseorders$purchaseOrderID, suppliers$supplierID
			from purchaseorders$purchaseorder_supplier) pos on po.id = pos.purchaseorders$purchaseOrderID
		inner join	
			(select id, supplierType, supplierID, vendorCode, supplierName
			from suppliers$supplier) s on pos.suppliers$supplierID = s.id
		inner join
			(select purchaseorders$purchaseOrderLineHeaderID, purchaseorders$purchaseOrderID 
			from purchaseorders$purchaseorderlineheader_purchaseorder) polhpo on po.id = polhpo.purchaseorders$purchaseOrderID 
		inner join
			(select id, 
				headerID, status, problems, 
				lines, items, totalPrice, 
				auditComment,
				createdDate, changedDate, 
				system$owner, system$changedBy
			from purchaseorders$purchaseorderlineheader) polh on polhpo.purchaseorders$purchaseOrderLineHeaderID = polh.id
		inner join
			(select purchaseorders$purchaseOrderLineHeaderID, product$productFamilyID
			from purchaseorders$purchaseorderlineheader_productfamily) polhpf on polh.id = polhpf.purchaseorders$purchaseOrderLineHeaderID
		inner join
			(select id, magentoProductID, magentoSKU, name
			from product$productfamily) pf on polhpf.product$productFamilyID = pf.id
		inner join			
			(select purchaseorders$purchaseOrderLineHeaderID, product$packsizeID
			from purchaseorders$purchaseorderlineheader_packsize) polhps on polh.id = polhps.purchaseorders$purchaseOrderLineHeaderID
		inner join
			(select id, 
				description, size, 
				productGroup1, productGroup2Type, productGroup2, 
				height, width, weight, depth, breakable, allocationPreferenceOrder,
				packagingNo, purchasingPreferenceOrder 
			from product$packsize) ps on polhps.product$packsizeID = ps.id
		inner join			
			(select purchaseorders$purchaseOrderLineHeaderID, suppliers$supplierPriceID
			from purchaseorders$purchaseorderlineheader_supplierprices) polhs2 on polh.id = polhs2.purchaseorders$purchaseOrderLineHeaderID
		inner join	
			(select id, 
				unitPrice, totalWarehouseUnitPrice, orderMultiple, leadTime, 
				subMetaObjectName,
				directPrice, expiredPrice,
				comments, 
				lastUpdated, 
				createdDate, changedDate, 
				system$owner, system$changedBy
			from suppliers$supplierprice) s2 on polhs2.suppliers$supplierPriceID = s2.id		
	where po.orderNumber = 'P00017237' -- > 'P00017215' 		
	order by po.orderNumber desc, polh.createdDate
	limit 1000;



-- Purchase Order Line
	select po.id, 
		po.orderNumber, po.status, po.source, 
		w.code, w.name, w.shortName, w.warehouseType, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
		po.itemCost, po.totalPrice, 
		po.leadTime, 
		po.approvedBy, po.approvedDate, 
		polh.id, polh.headerID, pf.magentoProductID, pf.magentoSKU, pf.name, ps.size, s2.unitPrice, 
		polh.status, polh.problems, polh.lines, polh.items, polh.totalPrice, polh.auditComment, polh.createdDate, 
		pol.po_lineID, pol.stockItemID, pol.quantity, pol.quantityUnallocated, pol.quantityOpen
	from 
			purchaseorders$purchaseorder po
		inner join
			(select purchaseorders$purchaseOrderID, inventory$warehouseID
			from purchaseorders$purchaseorder_warehouse) pow on po.id = pow.purchaseorders$purchaseOrderID
		inner join
			(select id, code, name, shortName, warehouseType 
			from inventory$warehouse) w on pow.inventory$warehouseID = w.id
		inner join	
			(select purchaseorders$purchaseOrderID, suppliers$supplierID
			from purchaseorders$purchaseorder_supplier) pos on po.id = pos.purchaseorders$purchaseOrderID
		inner join	
			(select id, supplierType, supplierID, vendorCode, supplierName
			from suppliers$supplier) s on pos.suppliers$supplierID = s.id
		inner join
			(select purchaseorders$purchaseOrderLineHeaderID, purchaseorders$purchaseOrderID 
			from purchaseorders$purchaseorderlineheader_purchaseorder) polhpo on po.id = polhpo.purchaseorders$purchaseOrderID 
		inner join
			(select id, 
				headerID, status, problems, 
				lines, items, totalPrice, 
				auditComment,
				createdDate, changedDate, 
				system$owner, system$changedBy
			from purchaseorders$purchaseorderlineheader) polh on polhpo.purchaseorders$purchaseOrderLineHeaderID = polh.id
		inner join
			(select purchaseorders$purchaseOrderLineHeaderID, product$productFamilyID
			from purchaseorders$purchaseorderlineheader_productfamily) polhpf on polh.id = polhpf.purchaseorders$purchaseOrderLineHeaderID
		inner join
			(select id, magentoProductID, magentoSKU, name
			from product$productfamily) pf on polhpf.product$productFamilyID = pf.id
		inner join			
			(select purchaseorders$purchaseOrderLineHeaderID, product$packsizeID
			from purchaseorders$purchaseorderlineheader_packsize) polhps on polh.id = polhps.purchaseorders$purchaseOrderLineHeaderID
		inner join
			(select id, 
				description, size, 
				productGroup1, productGroup2Type, productGroup2, 
				height, width, weight, depth, breakable, allocationPreferenceOrder,
				packagingNo, purchasingPreferenceOrder 
			from product$packsize) ps on polhps.product$packsizeID = ps.id
		inner join			
			(select purchaseorders$purchaseOrderLineHeaderID, suppliers$supplierPriceID
			from purchaseorders$purchaseorderlineheader_supplierprices) polhs2 on polh.id = polhs2.purchaseorders$purchaseOrderLineHeaderID
		inner join	
			(select id, 
				unitPrice, totalWarehouseUnitPrice, orderMultiple, leadTime, 
				subMetaObjectName,
				directPrice, expiredPrice,
				comments, 
				lastUpdated, 
				createdDate, changedDate, 
				system$owner, system$changedBy
			from suppliers$supplierprice) s2 on polhs2.suppliers$supplierPriceID = s2.id	
		inner join
			(select purchaseorders$purchaseOrderLineID, purchaseorders$purchaseOrderLineHeaderID 
			from purchaseorders$purchaseorderline_purchaseorderlineheader) polpolh on polh.id = polpolh.purchaseorders$purchaseOrderLineHeaderID
		inner join
			(select id, 
				SNAPlineID, po_lineID, stockItemID, 
				quantity, quantityUnallocated, quantityOpen, quantityExWorks, quantityPlanned, quantityInTransit, quantityDelivered, quantityReceived, quantityRejected, quantityCancelled, 
				fulfilled, 
				auditComment, progressComment, 
				backTobackDueDate, 
				createdDate, changedDate, 
				system$owner, system$changedBy	
			from purchaseorders$purchaseorderline) pol on polpolh.purchaseorders$purchaseOrderLineID = pol.id
	where po.orderNumber = 'P00017237' 		
	order by po.orderNumber desc, polh.createdDate, pol.po_lineID
	limit 1000;

	select po.id, 
		po.orderNumber, po.status, po.source, 
		w.code, w.name, w.shortName, w.warehouseType, s.supplierType, s.supplierID, s.vendorCode, s.supplierName, 
		po.itemCost, po.totalPrice, 
		po.leadTime, 
		po.approvedBy, po.approvedDate, 
		polh.id, polh.headerID, pf.magentoProductID, pf.magentoSKU, pf.name, ps.size, s2.unitPrice, 
		polh.status, polh.problems, polh.lines, polh.items, polh.totalPrice, polh.auditComment, polh.createdDate, 
		pol.po_lineID, pol.stockItemID, pol.quantity, pol.quantityUnallocated, pol.quantityOpen, 
		si.*
	from 
			purchaseorders$purchaseorder po
		inner join
			(select purchaseorders$purchaseOrderID, inventory$warehouseID
			from purchaseorders$purchaseorder_warehouse) pow on po.id = pow.purchaseorders$purchaseOrderID
		inner join
			(select id, code, name, shortName, warehouseType 
			from inventory$warehouse) w on pow.inventory$warehouseID = w.id
		inner join	
			(select purchaseorders$purchaseOrderID, suppliers$supplierID
			from purchaseorders$purchaseorder_supplier) pos on po.id = pos.purchaseorders$purchaseOrderID
		inner join	
			(select id, supplierType, supplierID, vendorCode, supplierName
			from suppliers$supplier) s on pos.suppliers$supplierID = s.id
		inner join
			(select purchaseorders$purchaseOrderLineHeaderID, purchaseorders$purchaseOrderID 
			from purchaseorders$purchaseorderlineheader_purchaseorder) polhpo on po.id = polhpo.purchaseorders$purchaseOrderID 
		inner join
			(select id, 
				headerID, status, problems, 
				lines, items, totalPrice, 
				auditComment,
				createdDate, changedDate, 
				system$owner, system$changedBy
			from purchaseorders$purchaseorderlineheader) polh on polhpo.purchaseorders$purchaseOrderLineHeaderID = polh.id
		inner join
			(select purchaseorders$purchaseOrderLineHeaderID, product$productFamilyID
			from purchaseorders$purchaseorderlineheader_productfamily) polhpf on polh.id = polhpf.purchaseorders$purchaseOrderLineHeaderID
		inner join
			(select id, magentoProductID, magentoSKU, name
			from product$productfamily) pf on polhpf.product$productFamilyID = pf.id
		inner join			
			(select purchaseorders$purchaseOrderLineHeaderID, product$packsizeID
			from purchaseorders$purchaseorderlineheader_packsize) polhps on polh.id = polhps.purchaseorders$purchaseOrderLineHeaderID
		inner join
			(select id, 
				description, size, 
				productGroup1, productGroup2Type, productGroup2, 
				height, width, weight, depth, breakable, allocationPreferenceOrder,
				packagingNo, purchasingPreferenceOrder 
			from product$packsize) ps on polhps.product$packsizeID = ps.id
		inner join			
			(select purchaseorders$purchaseOrderLineHeaderID, suppliers$supplierPriceID
			from purchaseorders$purchaseorderlineheader_supplierprices) polhs2 on polh.id = polhs2.purchaseorders$purchaseOrderLineHeaderID
		inner join	
			(select id, 
				unitPrice, totalWarehouseUnitPrice, orderMultiple, leadTime, 
				subMetaObjectName,
				directPrice, expiredPrice,
				comments, 
				lastUpdated, 
				createdDate, changedDate, 
				system$owner, system$changedBy
			from suppliers$supplierprice) s2 on polhs2.suppliers$supplierPriceID = s2.id	
		inner join
			(select purchaseorders$purchaseOrderLineID, purchaseorders$purchaseOrderLineHeaderID 
			from purchaseorders$purchaseorderline_purchaseorderlineheader) polpolh on polh.id = polpolh.purchaseorders$purchaseOrderLineHeaderID
		inner join
			(select id, 
				SNAPlineID, po_lineID, stockItemID, 
				quantity, quantityUnallocated, quantityOpen, quantityExWorks, quantityPlanned, quantityInTransit, quantityDelivered, quantityReceived, quantityRejected, quantityCancelled, 
				fulfilled, 
				auditComment, progressComment, 
				backTobackDueDate, 
				createdDate, changedDate, 
				system$owner, system$changedBy	
			from purchaseorders$purchaseorderline) pol on polpolh.purchaseorders$purchaseOrderLineID = pol.id
		inner join
			(select purchaseorders$purchaseOrderLineID, product$stockItemID
			from purchaseorders$purchaseorderline_stockitem) polsi on pol.id = polsi.purchaseorders$purchaseOrderLineID
		inner join
			(select si.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
				p.productType, p.SKU, p.oldSKU, p.description, 
				-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
				-- si.id, 
				si.SKU, si.oldSKU, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
			from 
					product$productfamily pf
				inner join
					(select product$productID, product$productFamilyID 
					from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
				inner join
					(select id, 
						productType, 
						SKU, oldSKU, description, 
						subMetaObjectName, price,
						isBOM, valid, displaySubProductOnPackingSlip,
						createdDate, changedDate, 
						system$owner, system$changedBy	
					from product$product) p on pfp.product$productID = p.id		
				inner join
					(select product$stockItemID, product$productID 
					from product$stockitem_product) sip on p.id = sip.product$productID 
				inner join
					(select id, 
						SKU, oldSKU, packSize, 
						manufacturerArticleID, manufacturerCodeNumber,
						SNAPDescription, SNAPUploadStatus, 
						changed
					from product$stockitem) si on sip.product$stockItemID = si.id) si on polsi.product$stockItemID = si.id				
	where po.orderNumber = 'P00017237' 		
	order by po.orderNumber desc, polh.createdDate, pol.po_lineID
	limit 1000;





-- Shortage
select s.id, 
	w.id, w.code, w.name, w.shortName, w.warehouseType, su.supplierType, su.supplierID, su.vendorCode, su.supplierName,
	s.shortageID, s.purchaseOrderNumber,
	s.status, 
	s.stockItemDescription, 
	si.magentoProductID, si.magentoSKU, si.name, si.productType, si.SKU, si.oldSKU, si.description, si.SKU_SI, si.oldSKU_SI, si.packSize,
	wsi.id_string, wsi.stockingMethod, wsi.actualStockQty, wsi.availableQty, wsi.allocatedStockQty, wsi.dueInQty, wsi.forwardDemand, wsi.outStandingAllocation, wsi.stocked, wsi.changed,
	s.unitQuantity, s.packQuantity, 
	s.requiredDate, s.dueInDate,
	s.wholesale
from 
		purchaseorders$shortage s
	inner join
		(select w.id, w.code, w.name, w.shortName, w.warehouseType, 
			sw.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, inventory$warehouseID
				from purchaseorders$shortage_warehouse) sw 
			inner join
				(select id, code, name, shortName, warehouseType 
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.purchaseorders$shortageID
	inner join
		(select su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName, 
			ssu.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, suppliers$supplierID
				from purchaseorders$shortage_supplier) ssu 
			inner join	
				(select id, supplierType, supplierID, vendorCode, supplierName
				from suppliers$supplier) su on ssu.suppliers$supplierID = su.id) su on s.id = su.purchaseorders$shortageID
	inner join
		(select p.magentoProductID, p.magentoSKU, p.name, p.productType, p.SKU, p.oldSKU, p.description, p.SKU_SI, p.oldSKU_SI, p.packSize, 
			sp.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, product$productID
				from purchaseorders$shortage_product) sp
			inner join
				(select purchaseorders$shortageID, product$stockItemID
				from purchaseorders$shortage_stockitem) ssi on sp.purchaseorders$shortageID = ssi.purchaseorders$shortageID
			inner join
				(select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
					p.id productID, p.productType, p.SKU, p.oldSKU, p.description, 
					-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
					-- si.id, 
					si.id stockItemID, si.SKU SKU_SI, si.oldSKU oldSKU_SI, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
				from 
						product$productfamily pf
					inner join
						(select product$productID, product$productFamilyID 
						from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
					inner join
						(select id, 
							productType, 
							SKU, oldSKU, description, 
							subMetaObjectName, price,
							isBOM, valid, displaySubProductOnPackingSlip,
							createdDate, changedDate, 
							system$owner, system$changedBy	
						from product$product) p on pfp.product$productID = p.id		
					inner join
						(select product$stockItemID, product$productID 
						from product$stockitem_product) sip on p.id = sip.product$productID 
					inner join
						(select id, 
							SKU, oldSKU, packSize, 
							manufacturerArticleID, manufacturerCodeNumber,
							SNAPDescription, SNAPUploadStatus, 
							changed
						from product$stockitem) si on sip.product$stockItemID = si.id) p on sp.product$productID = p.productID and ssi.product$stockItemID = p.stockItemID) si on s.id = si.purchaseorders$shortageID				
	inner join
		(select wsi.id_string, wsi.stockingMethod, wsi.actualStockQty, wsi.availableQty, wsi.allocatedStockQty, wsi.dueInQty, 
			wsi.forwardDemand, wsi.outStandingAllocation, wsi.stocked, wsi.changed, 
			swsi.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, inventory$warehouseStockItemID
				from purchaseorders$shortage_warehousestockitem) swsi
			inner join
				(select ws.id,
					w.code, w.name, w.shortName,
					ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
					ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed
				from
						(select id, 
							id_string, stockingMethod, 
							actualStockQty, availableQty, allocatedStockQty, dueInQty, 
							forwardDemand, outStandingAllocation,
							stocked, changed
						from inventory$warehousestockitem) ws 
					inner join
						(select inventory$warehouseStockItemID, inventory$warehouseID
						from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
					inner join	
						inventory$warehouse w on l.inventory$warehouseID = w.id) wsi on swsi.inventory$warehouseStockItemID = wsi.id) wsi on s.id = wsi.purchaseorders$shortageID
	
where purchaseOrderNumber = 'P00017237'
order by purchaseOrderNumber, si.sku;

-- Shortage Header 
select s.id, 
	w.id, w.code, w.name, w.shortName, w.warehouseType, su.supplierType, su.supplierID, su.vendorCode, su.supplierName,
	s.shortageID, s.purchaseOrderNumber,
	s.status, 
	s.stockItemDescription, 
	si.magentoProductID, si.magentoSKU, si.name, si.productType, si.SKU, si.oldSKU, si.description, si.SKU_SI, si.oldSKU_SI, si.packSize,
	wsi.id_string, wsi.stockingMethod, wsi.actualStockQty, wsi.availableQty, wsi.allocatedStockQty, wsi.dueInQty, wsi.forwardDemand, wsi.outStandingAllocation, wsi.stocked, wsi.changed,
	s.unitQuantity, s.packQuantity, 
	s.requiredDate, s.dueInDate,
	s.wholesale, sh.wholesale
from 
		purchaseorders$shortage s
	inner join
		(select w.id, w.code, w.name, w.shortName, w.warehouseType, 
			sw.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, inventory$warehouseID
				from purchaseorders$shortage_warehouse) sw 
			inner join
				(select id, code, name, shortName, warehouseType 
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.purchaseorders$shortageID
	inner join
		(select su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName, 
			ssu.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, suppliers$supplierID
				from purchaseorders$shortage_supplier) ssu 
			inner join	
				(select id, supplierType, supplierID, vendorCode, supplierName
				from suppliers$supplier) su on ssu.suppliers$supplierID = su.id) su on s.id = su.purchaseorders$shortageID
	inner join
		(select p.magentoProductID, p.magentoSKU, p.name, p.productType, p.SKU, p.oldSKU, p.description, p.SKU_SI, p.oldSKU_SI, p.packSize, 
			sp.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, product$productID
				from purchaseorders$shortage_product) sp
			inner join
				(select purchaseorders$shortageID, product$stockItemID
				from purchaseorders$shortage_stockitem) ssi on sp.purchaseorders$shortageID = ssi.purchaseorders$shortageID
			inner join
				(select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
					p.id productID, p.productType, p.SKU, p.oldSKU, p.description, 
					-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
					-- si.id, 
					si.id stockItemID, si.SKU SKU_SI, si.oldSKU oldSKU_SI, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
				from 
						product$productfamily pf
					inner join
						(select product$productID, product$productFamilyID 
						from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
					inner join
						(select id, 
							productType, 
							SKU, oldSKU, description, 
							subMetaObjectName, price,
							isBOM, valid, displaySubProductOnPackingSlip,
							createdDate, changedDate, 
							system$owner, system$changedBy	
						from product$product) p on pfp.product$productID = p.id		
					inner join
						(select product$stockItemID, product$productID 
						from product$stockitem_product) sip on p.id = sip.product$productID 
					inner join
						(select id, 
							SKU, oldSKU, packSize, 
							manufacturerArticleID, manufacturerCodeNumber,
							SNAPDescription, SNAPUploadStatus, 
							changed
						from product$stockitem) si on sip.product$stockItemID = si.id) p on sp.product$productID = p.productID and ssi.product$stockItemID = p.stockItemID) si on s.id = si.purchaseorders$shortageID				
	inner join
		(select wsi.id_string, wsi.stockingMethod, wsi.actualStockQty, wsi.availableQty, wsi.allocatedStockQty, wsi.dueInQty, 
			wsi.forwardDemand, wsi.outStandingAllocation, wsi.stocked, wsi.changed, 
			swsi.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, inventory$warehouseStockItemID
				from purchaseorders$shortage_warehousestockitem) swsi
			inner join
				(select ws.id,
					w.code, w.name, w.shortName,
					ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
					ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed
				from
						(select id, 
							id_string, stockingMethod, 
							actualStockQty, availableQty, allocatedStockQty, dueInQty, 
							forwardDemand, outStandingAllocation,
							stocked, changed
						from inventory$warehousestockitem) ws 
					inner join
						(select inventory$warehouseStockItemID, inventory$warehouseID
						from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
					inner join	
						inventory$warehouse w on l.inventory$warehouseID = w.id) wsi on swsi.inventory$warehouseStockItemID = wsi.id) wsi on s.id = wsi.purchaseorders$shortageID
	
	inner join
		(select sh.wholesale, 
			ssh.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, purchaseorders$shortageHeaderID 
				from purchaseorders$shortage_shortageheader) ssh
			inner join
				(select id, wholesale
				from purchaseorders$shortageheader) sh on ssh.purchaseorders$shortageHeaderID = sh.id) sh on s.id = sh.purchaseorders$shortageID

where purchaseOrderNumber = 'P00017237'
order by purchaseOrderNumber, si.sku;

	
-- Order Line Shortage 
select s.id, 
	w.id, w.code, w.name, w.shortName, w.warehouseType, su.supplierType, su.supplierID, su.vendorCode, su.supplierName,
	s.shortageID, s.purchaseOrderNumber,
	s.status, 
	s.stockItemDescription, 
	si.magentoProductID, si.magentoSKU, si.name, si.productType, si.SKU, si.oldSKU, si.description, si.SKU_SI, si.oldSKU_SI, si.packSize,
	wsi.id_string, wsi.stockingMethod, wsi.actualStockQty, wsi.availableQty, wsi.allocatedStockQty, wsi.dueInQty, wsi.forwardDemand, wsi.outStandingAllocation, wsi.stocked, wsi.changed,
	s.unitQuantity, s.packQuantity, 
	s.requiredDate, s.dueInDate,
	s.wholesale, 
	ols.orderLineShortageID, ols.orderLineShortageType, ols.shortageProgress, ols.unitQuantity, ols.packQuantity, ols.advisedDate, ols.dueInDate
from 
		purchaseorders$shortage s
	inner join
		(select w.id, w.code, w.name, w.shortName, w.warehouseType, 
			sw.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, inventory$warehouseID
				from purchaseorders$shortage_warehouse) sw 
			inner join
				(select id, code, name, shortName, warehouseType 
				from inventory$warehouse) w on sw.inventory$warehouseID = w.id) w on s.id = w.purchaseorders$shortageID
	inner join
		(select su.id, su.supplierType, su.supplierID, su.vendorCode, su.supplierName, 
			ssu.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, suppliers$supplierID
				from purchaseorders$shortage_supplier) ssu 
			inner join	
				(select id, supplierType, supplierID, vendorCode, supplierName
				from suppliers$supplier) su on ssu.suppliers$supplierID = su.id) su on s.id = su.purchaseorders$shortageID
	inner join
		(select p.magentoProductID, p.magentoSKU, p.name, p.productType, p.SKU, p.oldSKU, p.description, p.SKU_SI, p.oldSKU_SI, p.packSize, 
			sp.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, product$productID
				from purchaseorders$shortage_product) sp
			inner join
				(select purchaseorders$shortageID, product$stockItemID
				from purchaseorders$shortage_stockitem) ssi on sp.purchaseorders$shortageID = ssi.purchaseorders$shortageID
			inner join
				(select pf.id, pf.magentoProductID, pf.magentoSKU, pf.name, 
					p.id productID, p.productType, p.SKU, p.oldSKU, p.description, 
					-- p.id, p.subMetaObjectName, p.price, p.isBOM, p.valid, p.displaySubProductOnPackingSlip, 
					-- si.id, 
					si.id stockItemID, si.SKU SKU_SI, si.oldSKU oldSKU_SI, si.packSize, si.manufacturerArticleID, si.manufacturerCodeNumber, si.SNAPDescription, si.SNAPUploadStatus
				from 
						product$productfamily pf
					inner join
						(select product$productID, product$productFamilyID 
						from product$product_productfamily) pfp on pf.id = pfp.product$productFamilyID
					inner join
						(select id, 
							productType, 
							SKU, oldSKU, description, 
							subMetaObjectName, price,
							isBOM, valid, displaySubProductOnPackingSlip,
							createdDate, changedDate, 
							system$owner, system$changedBy	
						from product$product) p on pfp.product$productID = p.id		
					inner join
						(select product$stockItemID, product$productID 
						from product$stockitem_product) sip on p.id = sip.product$productID 
					inner join
						(select id, 
							SKU, oldSKU, packSize, 
							manufacturerArticleID, manufacturerCodeNumber,
							SNAPDescription, SNAPUploadStatus, 
							changed
						from product$stockitem) si on sip.product$stockItemID = si.id) p on sp.product$productID = p.productID and ssi.product$stockItemID = p.stockItemID) si on s.id = si.purchaseorders$shortageID				
	inner join
		(select wsi.id_string, wsi.stockingMethod, wsi.actualStockQty, wsi.availableQty, wsi.allocatedStockQty, wsi.dueInQty, 
			wsi.forwardDemand, wsi.outStandingAllocation, wsi.stocked, wsi.changed, 
			swsi.purchaseorders$shortageID
		from
				(select purchaseorders$shortageID, inventory$warehouseStockItemID
				from purchaseorders$shortage_warehousestockitem) swsi
			inner join
				(select ws.id,
					w.code, w.name, w.shortName,
					ws.id_string, ws.stockingMethod, ws.actualStockQty, ws.availableQty, ws.allocatedStockQty, ws.dueInQty, 
					ws.forwardDemand, ws.outStandingAllocation, ws.stocked, ws.changed
				from
						(select id, 
							id_string, stockingMethod, 
							actualStockQty, availableQty, allocatedStockQty, dueInQty, 
							forwardDemand, outStandingAllocation,
							stocked, changed
						from inventory$warehousestockitem) ws 
					inner join
						(select inventory$warehouseStockItemID, inventory$warehouseID
						from inventory$located_at) l on ws.id = l.inventory$warehouseStockItemID
					inner join	
						inventory$warehouse w on l.inventory$warehouseID = w.id) wsi on swsi.inventory$warehouseStockItemID = wsi.id) wsi on s.id = wsi.purchaseorders$shortageID
	left join
		(select ols.orderLineShortageID, ols.orderLineShortageType, ols.shortageProgress, 
			ols.unitQuantity, ols.packQuantity, ols.advisedDate, ols.dueInDate, 
			olss.purchaseorders$shortageID 
		from
			(select purchaseorders$orderLineShortageID, purchaseorders$shortageID 
			from purchaseorders$orderlineshortage_shortage) olss
		inner join	
			(select id, 
				orderLineShortageID, 
				orderLineShortageType, shortageProgress, 
				unitQuantity, packQuantity, 
				advisedDate, dueInDate,
				createdDate, changedDate
			from purchaseorders$orderlineshortage) ols on olss.purchaseorders$orderLineShortageID = ols.id) ols on s.id = ols.purchaseorders$shortageID 	-- 1 to N
where purchaseOrderNumber = 'P00017237'
order by purchaseOrderNumber, si.sku;


			



