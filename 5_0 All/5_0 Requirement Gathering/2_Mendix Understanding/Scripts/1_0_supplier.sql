﻿
-- Supplier 
select id, 
	supplierType, supplierID, vendorCode, supplierName, vendorShortName, 
	email, telephone, fax,
	countryID, regionID, region, city, postcode, 
	street1, street2, street3, 
	vatCode, currencyCode, 
	b2bBoxesLimit, defaultLeadTime, isNullSupplier, flatFileConfigurationToUse, isEDIEnabled, vendorStoreNumber
from suppliers$supplier
order by supplierType, supplierName;

-- Contacts 
select id, 
	name, department, role
	email, telephone, mobile
from suppliers$contacts
order by name;

-- Supplier Price 
select id, 
	unitPrice, totalWarehouseUnitPrice, orderMultiple, leadTime, 
	subMetaObjectName,
	directPrice, expiredPrice,
	comments, 
	lastUpdated, 
	createdDate, changedDate, 
	system$owner, system$changedBy
from suppliers$supplierprice;

-- Supplier Standard Price
select id, 
	moq, 
	effectiveDate, expiryDate
from suppliers$standardprice;

-- Supplier Spot Price
select id
from suppliers$spotprice;

-------------------------------------------------------------

	select *
	from suppliers$contacts_supplier;
	
	select *
	from suppliers$supplierprices_supplier;

-------------------------------------------------------------

	select *
	from suppliers$supplier_company;
	
	select *
	from suppliers$supplier_country;
	
	select *
	from suppliers$supplier_currency;
	
	select *
	from suppliers$supplier_ediftpconfigurations;
	
	select *
	from suppliers$supplierprices_packsize;
