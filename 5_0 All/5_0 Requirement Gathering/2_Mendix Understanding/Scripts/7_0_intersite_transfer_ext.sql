﻿
-- Transfer Header
select id, 
	transferNum, 
	allocatedStrategy, 
	totalRemaining, interCompanyCarriage, inboundCarriage, 
	dateToShip, receivedDate, dueDate,
	createdDate, changedDate, 
	system$changedBy
from intersitetransfer$transferheader
order by transferNum;

-- In Transit Batch Header
select id, createdDate
from intersitetransfer$intransitbatchheader

-- In Transit Stock Item Batch
select id, 
	totalUnitCost, totalUnitCostIncInterCo, 
	ProductUnitCost, dutyUnitCost, carriageUnitCost, inteCoCarriageUnitCost, interCoProfitUnitCost, 
	currency, 
	issuedQuantity, remainingQuantity,
	confirmedDate, arrivedDate, groupFIFOdate, stockRegisteredDate, 
	used, 
	createddate
from intersitetransfer$intransitstockitembatch
limit 1000;

------------------------------------------------------------------

	select intersitetransfer$inTransitBatchHeaderID, intersitetransfer$transferHeaderID
	from intersitetransfer$intransitbatchheader_transferheader;
	
	select intersitetransfer$inTransitStockItemBatchID, intersitetransfer$transferHeaderID
	from intersitetransfer$intransitstockitembatch_transferheader
	limit 1000;
	
	select intersitetransfer$inTransitStockItemBatchID, intersitetransfer$inTransitBatchHeaderID
	from intersitetransfer$intransitstockitembatch_intransitbatchheader
	limit 1000;

------------------------------------------------------------------

	select intersitetransfer$inTransitBatchHeaderID, customershipments$customerShipmentID
	from intersitetransfer$intransitbatchheader_customershipment;
	
	select intersitetransfer$inTransitStockItemBatchID, product$stockItemID
	from intersitetransfer$intransitstockitembatch_stockitem
	limit 1000;
	
	select intersitetransfer$inTransitStockItemBatchID, warehouseshipments$shipmentID
	from intersitetransfer$intransitstockitembatch_shipment
	limit 1000;

