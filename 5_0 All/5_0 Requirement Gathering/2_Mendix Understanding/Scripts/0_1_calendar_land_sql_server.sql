
select id, 
	magentoShippingDescription, magentoShippingMethod, scurriShippingMethod, snapShippingMethod, 
	leadtime, letterRate1, letterRate2, tracked, invoicedocs,
	mondayDefaultCutOff, tuesdayDefaultCutOff, wednesdayDefaultCutOff, thursdayDefaultCutOff, fridayDefaultCutOff, saturdayDefaultCutOff, sundayDefaultCutOff, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayDeliveryWorking, tuesdayDeliveryWorking, wednesdayDeliveryWorking, thursdayDeliveryWorking, fridayDeliveryWorking, saturdayDeliveryWorking, sundayDeliveryWorking
	mondayCollectionTime, tuesdayCollectionTime, wednesdayCollectionTime, thursdayCollectionTime, fridayCollectionTime, saturdayCollectionTime, sundayCollectionTime	
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$shippingmethod"')
order by magentoShippingDescription

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$shippingmethods_warehouse"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$shippingmethods_destinationcountry"')


select id, 
	mondayWorking, tuesdayWorking, wednesdayWorking, thursdayWorking, fridayWorking, saturdayWorking, sundayWorking, 
	mondayCutOff, tuesdayCutOff, wednesdayCutOff, thursdayCutOff, fridayCutOff, saturdayCutOff, sundayCutOff, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$defaultcalendar"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$defaultcalendar_warehouse"')


select id, 
	working, day, cutofftime, date
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$warehousecalendar"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$calendar_warehouse"')

select id, 
	working, deliveryWorking, cutofftime, collectiontime
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$shippingcutoffs"')
order by working, cutofftime, collectiontime, deliveryWorking;

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$shippingcutoffs_warehousecalendar"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$shippingcutoffs_shippingmethods"')




select id, 
	mondayCanSupply, tuesdayCanSupply, wednesdayCanSupply, thursdayCanSupply, fridayCanSupply, saturdayCanSupply, sundayCanSupply, 
	mondayCutOffTime, tuesdayCutOffTime, wednesdayCutOffTime, thursdayCutOffTime, fridayCutOffTime, saturdayCutOffTime, sundayCutOffTime, 
	mondayCanOrder, tuesdayCanOrder, wednesdayCanOrder, thursdayCanOrder, fridayCanOrder, saturdayCanOrder, sundayCanOrder, 
	timeHorizonWeeks,
	createdDate, changedDate, 
	system$changedby 
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$defaultwarehousesuppliercalender"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$defaultwarehousesuppliercalender_warehouse"')

select id, 
	day, date
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$warehousesuppliercalendar"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$warehousesuppliercalendar_warehouse"')

select id, 
	orderOn, suppliesOn, cutofftime
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$suppliercutoffs"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$suppliercutoffs_warehousesuppliercalendar"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$suppliercutoffs_warehousesupplier"')

	select *
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$suppliercutoffs_supplierroutine"')




select id, 
	acceptOrders, deliverOrders, orderCutOffTime, date
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."calendar$supplierexceptiondates"')
