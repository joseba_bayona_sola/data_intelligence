﻿
-- Order 
select id, 
	incrementID, magentoOrderID, createdDate, orderStatus, 
	orderLinesMagento, orderLinesDeduped,

	_type, shipmentStatus, statusUpdated, allocationStatus, allocationStrategy,
	allocatedDate, promisedShippingDate, promisedDeliveryDate,
	labelRenderer,  
	couponCode, shippingMethod, shippingDescription, paymentMethod, automatic_reorder
		
	--totalNetPrice, totalPacksOrdered, orderCurrencyCode, globalCurrencyCode,
	
from orderprocessing$order
--where shipmentStatus = 'Full'
where incrementID in ('8002436082', '24000046508', '17000281863', '24000046456')
order by createdDate desc
limit 1000;

-- Order Status Update
select o.id, 
	su.id, su.status, su.timestamp
from 
		orderprocessing$order o
	inner join
		(select su.id, su.status, su.timestamp, suo.orderprocessing$orderID
		from
				(select orderprocessing$order_statusUpdateID, orderprocessing$orderID
				from orderprocessing$statusupdate_order) suo
			inner join
				(select id, status, timestamp 
				from orderprocessing$order_statusupdate) su on suo.orderprocessing$order_statusUpdateID = su.id) su on o.id = su.orderprocessing$orderID
where incrementID = '24000046508' -- in ('8002436082', '24000046508', '17000281863', '24000046456')
order by o.incrementID, status
limit 1000;


-- Order Processing Transaction
select o.id, 
	opt.id, opt.orderReference, opt.status, opt.detail, opt.messageBody, opt.timestamp
from 
		orderprocessing$order o
	inner join
		(select opt.id, opt.orderReference, opt.status, opt.detail, opt.messageBody, opt.timestamp, orderprocessing$orderID
		from
				(select orderprocessing$orderprocessingTransactionID, orderprocessing$orderID
				from orderprocessing$orderprocessingtransaction_order) opto
			inner join
				(select id, orderReference, status, detail, messageBody, timestamp 
				from orderprocessing$orderprocessingtransaction) opt on opto.orderprocessing$orderprocessingTransactionID = opt.id) opt on o.id = opt.orderprocessing$orderID
where incrementID = '24000046508' -- in ('8002436082', '24000046508', '17000281863', '24000046456', '26000143796')
order by createdDate desc
limit 1000;

-- Base Values
select o.id, 
	bv.*
from 
		orderprocessing$order o
	inner join
		(select bv.*, bvo.orderProcessing$orderID
		from 
				(select orderprocessing$baseValuesID, orderProcessing$orderID
				from orderprocessing$basevalues_order) bvo
			inner join	
				(select id, 
					subtotal, subtotalInclTax, subtotalInvoiced, subtotalCanceled, subtotalRefunded, 
					discountAmount, discountInvoiced, discountCanceled, discountRefunded, 
					shippingAmount, shippingInvoiced, shippingCanceled, shippingRefunded, 					 
					adjustmentNegative, adjustmentPositive, 
					
					custBalanceAmount, 
					customerBalanceAmount, customerBalanceInvoiced, customerBalanceRefunded, customerBalanceTotRefunded, customerBalanceTotalRefunded, 
					grandTotal, totalInvoiced, totalCanceled, totalRefunded,
					toOrderRate, toGlobalRate, currencyCode
				from orderprocessing$basevalues) bv on bvo.orderprocessing$baseValuesID = bv.id) bv on o.id = bv.orderProcessing$orderID
		
where incrementID = '24000046508' -- in ('8002436082', '24000046508', '17000281863', '24000046456')
order by createdDate desc
limit 1000;

-- Order Line Magento
select o.id, 
	olm.*
from 
		orderprocessing$order o
	inner join
		(select olm.*, olmo.orderprocessing$orderID
		from
			(select orderprocessing$orderLineMagentoID, orderprocessing$orderID
			from orderprocessing$orderlinemagento_order) olmo
		inner join	
			(select olm.id, ola.id id_ola,
				ola.magentoItemID, 
				ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityIssued, ola.quantityAllocated, ola.quantityShipped, 
				ola.basePrice, ola.baseRowPrice, 
				
				olm.lineAdded, olm.deduplicate, olm.fulfilled, 
				ola.allocated,
				ola.orderLineType, ola.subMetaObjectName,
				ola.sku, ola.eye, ola.lensGroupID, 
				
				  
				ola.netPrice, ola.netRowPrice
				
			from
					(select id, lineAdded, deduplicate, fulfilled
					from orderprocessing$orderlinemagento) olm
				inner join
					(select id, 
						magentoItemID, orderLineType, subMetaObjectName,
						sku, eye, lensGroupID, 
						basePrice, baseRowPrice, 
						quantityOrdered, quantityInvoiced, quantityRefunded, quantityShipped, quantityAllocated, quantityIssued,  
						netPrice, netRowPrice,
						allocated, 
						createdDate, changedDate, 
						system$owner, system$changedBy
					from orderprocessing$orderlineabstract) ola on olm.id = ola.id) olm on olmo.orderprocessing$orderLineMagentoID = olm.id) olm 
						on o.id = olm.orderProcessing$orderID
where incrementID = '24000046508' -- in ('8002436082', '24000046508', '17000281863', '24000046456')
order by createdDate desc, olm.sku
limit 1000;


-- Order Line
select o.id, 
	ol.*
from 
		orderprocessing$order o
	inner join
		(select ol.*, olo.orderprocessing$orderID
		from 
			(select orderprocessing$orderLineID, orderprocessing$orderID
			from orderprocessing$orderline_order) olo
		inner join
			(select ol.id, ol.status, ol.packsOrdered, ol.packsAllocated, ol.packsShipped 
				--ola.magentoItemID, ola.orderLineType, ola.subMetaObjectName,
				--ola.sku, ola.eye, ola.lensGroupID, 
				--ola.basePrice, ola.baseRowPrice, 
				--ola.quantityOrdered, ola.quantityInvoiced, ola.quantityRefunded, ola.quantityShipped, ola.quantityAllocated, ola.quantityIssued,  
				--ola.netPrice, ola.netRowPrice,
				--ola.allocated
			from
					(select id, status, packsOrdered, packsAllocated, packsShipped
					from orderprocessing$orderline) ol
				inner join
					(select id, 
						magentoItemID, orderLineType, subMetaObjectName,
						sku, eye, lensGroupID, 
						basePrice, baseRowPrice, 
						quantityOrdered, quantityInvoiced, quantityRefunded, quantityShipped, quantityAllocated, quantityIssued,  
						netPrice, netRowPrice,
						allocated, 
						createdDate, changedDate, 
						system$owner, system$changedBy
					from orderprocessing$orderlineabstract) ola on ol.id = ola.id) ol on olo.orderprocessing$orderLineID = ol.id) ol 
						on o.id = ol.orderProcessing$orderID		
where incrementID = '24000046508' -- in ('8002436082', '24000046508', '17000281863', '24000046456')
order by createdDate desc
limit 1000;

-- Shipping Group
select o.id, 
	sg.*
from 
		orderprocessing$order o
	inner join
		(select sg.id, sg.dispensingFee, sg.allocationStatus, sg.shipmentStatus, 
			sg.wholesale, sg.letterBoxAble, sg.onHold, sg.adHoc, 
			sgo.orderprocessing$orderID
		from
				(select orderprocessing$shippingGroupID, orderprocessing$orderID
				from orderprocessing$shippinggroup_order) sgo
			inner join	
				(select id, 
					dispensingFee, 
					allocationStatus, shipmentStatus, 
					wholesale, 
					letterBoxAble, onHold, adHoc
				from orderprocessing$shippinggroup) sg on sgo.orderprocessing$shippingGroupID = sg.id) sg on o.id = sg.orderProcessing$orderID
where incrementID = '24000046508' -- in ('8002436082', '24000046508', '17000281863', '24000046456')
order by createdDate desc
limit 1000;

select orderprocessing$orderLineAbstractID, product$productID
from orderprocessing$orderline_product
limit 1000 

select orderprocessing$orderLineAbstractID, product$stockItemID
from orderprocessing$orderline_requiredstockitem		
limit 1000 