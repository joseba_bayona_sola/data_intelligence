﻿
-- Product Category
select id, 
	code, name
from product$productcategory
order by code;

-- Product Family
select id, 
	magentoProductID, magentoSKU, name, manufacturerProductFamily, status, 
	productFamilyType, 
	promotionalProduct, updateSNAPDescription, createToOrder, 
	createdDate, changedDate, 
	system$owner, system$changedBy 
	-- modality, productType, ProductFeature, 
	-- productGroup, Category, productSpecialisation -- 
from product$productfamily
order by magentoProductID;

	select product$productfamilyid, product$productcategoryid 
	from product$productfamily_productcategory;

-- Product 
select id, 
	productType, valid, 
	SKU, oldSKU, description, 
	isBOM, displaySubProductOnPackingSlip,
	createdDate, changedDate, 
	system$owner, system$changedBy	
	-- subMetaObjectName, price,
from product$product
where oldsku like '1DAM90%'
order by sku
limit 1000;

	select product$productid, product$productfamilyid  
	from product$product_productfamily
	limit 1000;



-- Product Stock Item
select id, 
	SKU, oldSKU, SNAPDescription, packSize, 
	manufacturerArticleID, manufacturerCodeNumber,
	SNAPUploadStatus, 
	changed
from product$stockitem
where sku like '01083B2D4CT%'
limit 1000;	

	select product$stockitemid, product$productid 
	from product$stockitem_product
	limit 1000;
	
-- Product Contact Lens
select id, 
	bc, di, po, 
	cy, ax,  
	ad, _do, co, co_EDI
	-- bc_value, di_value, po_value, cy_value, ax_value, ad_value, _do_value, co_value, 
from product$contactlens
limit 1000;

-- Product Pack Size
select id, 
	description, size, 
	packagingNo, allocationPreferenceOrder, purchasingPreferenceOrder, 
	height, width, weight, depth, 
	productGroup1, productGroup2, productGroup2Type, 
	breakable 
from product$packsize
order by description, size;

	select product$packsizeid, product$productfamilyid 
	from product$packsize_productfamily;

	select product$packsizeid, product$stockitemid 
	from product$stockitem_packsize
	limit 1000;

-- Product Warehouse Pack Size ???
select id, 
	identifierID, defaultCostPrice,
	productGroup1, productGroup2Type, productGroup2, 
	allocationPreference, 
	variableSelected, newRecord, changed, updateSNAP, active
from product$warehousepacksize

	
