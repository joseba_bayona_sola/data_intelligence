
select id, wholesale
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortageheader"')

	select purchaseorders$shortageheaderid, suppliers$supplierid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortageheader_supplier"')

	select purchaseorders$shortageheaderid, inventory$warehouseid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortageheader_sourcewarehouse"')
	
	select purchaseorders$shortageheaderid, inventory$warehouseid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortageheader_destinationwarehouse"')
	
	select purchaseorders$shortageheaderid, product$packsizeid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortageheader_packsize"')

select id, 
	shortageid, purchaseordernumber, ordernumbers,
	wholesale, 
	status, 
	unitquantity, packquantity, 
	requireddate, 
	createdDate, changedDate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortage"')
order by shortageid
	
	select purchaseorders$shortageid, purchaseorders$shortageheaderid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortage_shortageheader"')

	select purchaseorders$shortageid, purchaseorders$purchaseorderlineid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortage_purchaseorderline"')

	select purchaseorders$shortageid, suppliers$supplierid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortage_supplier"')

	select purchaseorders$shortageid, product$productid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortage_product"')

	select purchaseorders$shortageid, suppliers$standardpriceid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortage_standardprice"')

	select purchaseorders$shortageid, product$stockitemid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortage_stockitem"')

	select purchaseorders$shortageid, inventory$warehousestockitemid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$shortage_warehousestockitem"')


select id, 
	orderlineshortageid,
	orderlineshortagetype,
	unitquantity, packquantity,
	shortageprogress, adviseddate, 
	createdDate, changedDate
from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$orderlineshortage"')

	select purchaseorders$orderlineshortageid, purchaseorders$shortageid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$orderlineshortage_shortage"')

	select purchaseorders$orderlineshortageid, orderprocessing$orderlineid
	from openquery(MENDIX_DB_LOCAL, 'select * from mendix_db.public."purchaseorders$orderlineshortage_orderline"')
