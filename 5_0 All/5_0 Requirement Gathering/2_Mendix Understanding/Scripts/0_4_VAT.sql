﻿
-- Commmodity
select id, 
	commodityCode, commodityName
from vat$commodity
order by commodityCode;

-- Dispensing Service Rate
select id, 
	dispensingServicesRate, 
	rateEffectiveDate, rateEndDate
from vat$dispensingservicesrate
order by dispensingServicesRate, rateEffectiveDate;


-- VAT Rate
select id, 
	vatRate, vatRateEffectiveDate, vatRateEndDate,
	createdDate, changedDate, 
	system$owner, system$changedBy
from vat$vatrate
order by vatRate, vatRateEffectiveDate;

-- VAT Rating
select id, 
	vatRatingCode, vatRatingDescription
from vat$vatrating
order by vatRatingCode, vatRatingDescription;

-- VAT Schedule
select id, 
	taxationArea, vatSaleType, 
	scheduleEffectiveDate, scheduleEndDate
from vat$vatschedule
order by taxationArea, vatSaleType;


-- VAT Registration
select id, 
	vatRegistrationNumber, defaultVATRegistration, 
	vatRegistrationDate, vatRegistrationEndDate
from vat$vatregistration
order by vatRegistrationNumber;

----------------------------------------------------------------------------------

	select * 
	from vat$dispensingservices_commodity;
	
	select * 
	from vat$vatrate_vatrating;
	
	select * 
	from vat$vatschedule_commodity;
	
	select * 
	from vat$vatschedule_vatrating;

----------------------------------------------------------------------------------

	select * 
	from vat$dispensingservicesrate_country;
	
	select * 
	from vat$vatrating_country;
	
	select * 
	from vat$vatregistration_country;
	
	select * 
	from vat$vatregistration_company;
	
	select * 
	from vat$vatregistration_customerentity;
