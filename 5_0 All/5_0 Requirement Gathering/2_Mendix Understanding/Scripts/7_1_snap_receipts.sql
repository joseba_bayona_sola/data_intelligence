﻿
-- SNAP Receipt
select id, receiptID, 
	lines, lineQty,
	receiptStatus, receiptprocessStatus, processDetail,  
	status, stockStatus, orderType, orderClass, 
	supplierID, supplierName, 
	consignmentID, 
	purchaseOrder, 
	priority, weight, actualWeight, volume, 
	stuQty, 
	dateDueIn, dateReceipt, dateCreated, dateArrival, dateClosed,  
	datesfixed
from snapreceipts$snapreceipt
order by receiptID
limit 1000;

-- SNAP Receipt Line
select id, 
	line, 
	status, level, unitOfMeasure,
	skuID,
	qtyOrdered, qtyAdvised, qtyReceived, qtyDueIn, qtyRejected	
from snapreceipts$snapreceiptline
limit 1000;

------------------------------------------------------------------

	select snapreceipts$snapreceiptLineID, snapreceipts$snapreceiptID
	from snapreceipts$snapreceiptline_snapreceipt
	limit 1000;
	
------------------------------------------------------------------

	select snapreceipts$snapreceiptID, suppliers$supplierID
	from snapreceipts$snapreceipt_supplier;

	select snapreceipts$snapreceiptID, warehouseshipments$shipmentID
	from snapreceipts$snapreceipt_shipment
	limit 1000;

	select snapreceipts$snapreceiptID, warehouseshipments$shipmentID
	from snapreceipts$snapreceipt_matchedshipment
	limit 1000;

	select snapreceipts$snapreceiptLineID, product$stockItemID
	from snapreceipts$snapreceiptline_stockitem
	limit 1000;
