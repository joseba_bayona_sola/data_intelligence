﻿
select t1.schemaname, t1.tablename, 
	cast(t3.reltuples as bigint) num_rows
	--pg_size_pretty(pg_total_relation_size('"' || t2.table_schema || '"."' || t2.table_name || '"')) AS size
from 
		pg_tables t1
	inner join 	
		information_schema.tables t2 on t1.schemaname = t2.table_schema and t1.tablename = t2.table_name
	inner join 
		pg_class t3 on t1.tablename = t3.relname
where t1.schemaname = 'public'
--order by t1.schemaname, t1.tablename
order by num_rows desc


