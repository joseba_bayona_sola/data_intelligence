
select top 1000 count(*)
from Warehouse.act.fact_customer_signature_v
where create_date < '2017-04-01'
	and first_order_date is not null

drop table DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def

select rank() over (order by create_date, customer_id) num_row, customer_id, create_date, null selected
into DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
from Warehouse.act.fact_customer_signature_v
where create_date < '2017-04-01'
	and first_order_date is not null
order by create_date

select top 250000 (num_row * 6) + 1 num_row_sel
from DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
order by num_row

merge DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def trg
using
	(select t1.customer_id
	from 
			DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def t1
		inner join
			(select top 250000 (num_row * 6) + 1 num_row_sel
			from DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
			order by num_row) t2 on t1.num_row = t2.num_row_sel) src 
on trg.customer_id = src.customer_id
when matched then
	update set 
		trg.selected = 1;

select *
from DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
where selected = 1
order by num_row

select year(create_date), month(create_date), count(*)
from DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
where selected = 1
group by year(create_date), month(create_date)
order by year(create_date), month(create_date)

----------------------------------------------------------

drop table DW_GetLenses_jbs.dbo.next_order_date_sample_acs_next_def

create table DW_GetLenses_jbs.dbo.next_order_date_sample_acs_next_def(
	order_id_bk					bigint, 
	activity_date				datetime,
	rank_next_order_freq_time	char(2),
	next_order_freq_time		integer)

insert into DW_GetLenses_jbs.dbo.next_order_date_sample_acs_next_def(order_id_bk, activity_date, rank_next_order_freq_time, next_order_freq_time)
	select acs.order_id_bk, acs.activity_date, acs.rank_next_order_freq_time, acs.next_order_freq_time
	from 
			Warehouse.act.fact_activity_sales_v acs
		inner join
			(select customer_id
			from DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
			where selected = 1) sc on acs.customer_id = sc.customer_id

select top 1000 *
from Warehouse.act.fact_activity_sales_v
