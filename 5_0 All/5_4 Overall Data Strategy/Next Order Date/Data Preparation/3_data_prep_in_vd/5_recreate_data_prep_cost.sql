
use VisionDirect
go

-- Extract all the line item transactions for our customers up to their observation date
drop table [dbo].[ol_model_base];

select c.[customer_id], 
	max(c.[target_order_date]) as [target_order_date], c.[cat_name_short], 
	count(*) as num_product, min(c.[cost_per_month]) as cost_per_month_min, max(c.[cost_per_month]) as cost_per_month_max, 
	avg(c.[cost_per_month]) as cost_per_month_avg, sum(c.[cost_per_month]) as cost_per_month_sum
into [dbo].[ol_model_base]
from 
	(select a.*, b.[target_order_date], 
		case 
			when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
			when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
			else NULL 
		end as cost_per_month, -- HOW CALCULATED COST_PER_MONTH
		
		case 
			when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies') then a.[category_name] 
			when a.[category_name] = 'CL - Yearly' then 'CL - Monthlies'
			when left(a.[category_name],2) = 'EC' then 'EC'
			when left(a.[category_name],2) = 'SL' then 'SL'
			else 'Other' end as cat_name_short
	from 
			[dbo].[ol] as a
		inner join 
			[dbo].[cust_obs_date_targetset] as b on CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
		where a.[order_status_name]='OK') as c
group by c.[customer_id], c.[cat_name_short]
order by c.[customer_id], c.[cat_name_short];


	-- transpose the data so we have a column of data for each major category
	drop table [dbo].[ol_model_base_trans];

	SELECT *
	into [dbo].[ol_model_base_trans]
	FROM
			(SELECT [customer_id], [target_order_date], [cat_name_short], [cost_per_month_avg]
			FROM [dbo].[ol_model_base]) AS SourceTable 
		PIVOT
			(AVG(cost_per_month_avg) FOR [cat_name_short] IN
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];


-- Determine the rank position of this cost per item data;
	drop table [dbo].[cust_avg_spend];

	select x.[customer_id], x.[target_order_date],
		cast(x.[EC] as decimal(10,2)) as [avg_unit_price_paid_EC], cast(x.[CL - Monthlies] as decimal(10,2)) as [avg_unit_price_paid_CLM], 
		cast(x.[CL - Daily] as decimal(10,2)) as [avg_unit_price_paid_CLD], cast(x.[CL - Two Weeklies] as decimal(10,2)) as [avg_unit_price_paid_CL2W], 
		cast(x.[SL] as decimal(10,2)) as [avg_unit_price_paid_SL], cast(x.[Other] as decimal(10,2)) as [avg_unit_price_paid_Oth],
		
		cast(rank_EC / max_EC as decimal(10,2)) as [rank_pct_aupp_EC], cast(rank_CLM / max_CLM as decimal(10,2)) as [rank_pct_aupp_CLM], 
		cast(rank_CLD / max_CLD as decimal(10,2)) as [rank_pct_aupp_CLD], cast(rank_CL2W / max_CL2W as decimal(10,2)) as [rank_pct_aupp_C2W],
		cast(rank_SL / max_SL as decimal(10,2)) as [rank_pct_aupp_SL], cast(rank_Oth / max_Oth as decimal(10,2)) as [rank_pct_aupp_Oth]
	into [dbo].[cust_avg_spend]
	from
	(select *,
		case when [CL - Monthlies] is not null then  cast(dense_rank() over (order by [CL - Monthlies]) as real) else null end as rank_CLM,
		case when [CL - Daily] is not null then cast(dense_rank() over (order by [CL - Daily]) as real) else null end as rank_CLD,
		case when [CL - Two Weeklies] is not null then cast(dense_rank() over (order by [CL - Two Weeklies]) as real) else null end as rank_CL2W,
		case when [SL] is not null then cast(dense_rank() over (order by [SL]) as real) else null end as rank_SL,
		case when [EC] is not null then cast(dense_rank() over (order by [EC]) as real) else null end as rank_EC,
		case when [Other] is not null then cast(dense_rank() over (order by [Other]) as real) else null end as rank_Oth
	from [dbo].[ol_model_base_trans]) as x,

	(select max(a.rank_CLM) as max_CLM ,max(a.rank_CLD) as max_CLD, max(a.rank_CL2W) as max_CL2W, 
		max(a.rank_SL) as max_SL, max(a.rank_EC) as max_EC, max(a.rank_Oth) as max_Oth
	from 
		(select *, 
			case when [CL - Monthlies] is not null then  cast(dense_rank() over (order by [CL - Monthlies]) as real) else null end as rank_CLM,
			case when [CL - Daily] is not null then cast(dense_rank() over (order by [CL - Daily]) as real) else null end as rank_CLD,
			case when [CL - Two Weeklies] is not null then cast(dense_rank() over (order by [CL - Two Weeklies]) as real) else null end as rank_CL2W,
			case when [SL] is not null then cast(dense_rank() over (order by [SL]) as real) else null end as rank_SL,
			case when [EC] is not null then cast(dense_rank() over (order by [EC]) as real) else null end as rank_EC,
			case when [Other] is not null then cast(dense_rank() over (order by [Other]) as real) else null end as rank_Oth
		from [dbo].[ol_model_base_trans]) as a) as y


	drop table [dbo].[single_cust_view_interim2];

	select a.*,
		case when b.[avg_unit_price_paid_CLM] is null then null else b.[avg_unit_price_paid_CLM] end as [avg_unit_price_paid_CLM],
		case when b.[avg_unit_price_paid_CLD] is null then null else b.[avg_unit_price_paid_CLD] end as [avg_unit_price_paid_CLD],
		case when b.[avg_unit_price_paid_CL2W] is null then null else b.[avg_unit_price_paid_CL2W] end as [avg_unit_price_paid_CL2W],
		case when b.[avg_unit_price_paid_SL] is null then null else b.[avg_unit_price_paid_SL] end as [avg_unit_price_paid_SL],
		case when b.[avg_unit_price_paid_EC] is null then null else b.[avg_unit_price_paid_EC] end as [avg_unit_price_paid_EC],
		case when b.[avg_unit_price_paid_Oth] is null then null else b.[avg_unit_price_paid_Oth] end as [avg_unit_price_paid_Oth],
		
		b.[rank_pct_aupp_CLM], b.[rank_pct_aupp_CLD], b.[rank_pct_aupp_C2W], 
		b.[rank_pct_aupp_SL], b.[rank_pct_aupp_EC], b.[rank_pct_aupp_Oth]
	into [dbo].[single_cust_view_interim2]
	from 
			[dbo].[single_cust_view_interim1] as a 
		inner join
			[dbo].[cust_avg_spend] as b on CONVERT (DATE, a.[last_order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]