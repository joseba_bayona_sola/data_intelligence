
use VisionDirect
go

--
-- Code to prepare the estimated time for next order input data set.
--
--
--
-- Note to self.
-- I need to do this process twice.  The first time we look back to allow for the development of the index
-- The second time to estimate the next consumption date but with the index based on real behaviour to get the 
-- forecast to a more realistic level.  At the moment I've not included any adjustments for situations like
-- the customer just ordering solution, which can happen and is liable to mess up this calculation.
-- 
-- This table is a transaction table of orders with the best estimate of how long the order will last.
-- If contact lenses have been purchased then we use this as the driver of the estimation for the next purchase date
drop table [dbo].[cust_order_history];

select e.[customer_id], e.[order_date], 
	case when e.[qty_time_cl] is not null then e.[qty_time_cl] else e.[qty_time_ncl] end as [order_est_consumption_time],
	e.[qty_time_cl], e.[qty_time_ncl],
	rank() over(partition by e.[customer_id] order by e.[order_date] asc) as [cust_order_num], 
	case 
		when e.[qty_time_ncl] is not null then 
			case when e.[qty_time_cl] is null then 1 else 2 end 
			else 0 
		end as [curr_order_type]
into [dbo].[cust_order_history]
from 
	(select d.[customer_id], max(d.[order_date]) as [order_date], max(d.[qty_time_cl]) as [qty_time_cl] ,max(d.[qty_time_ncl]) as [qty_time_ncl]
	from 
		(-- At this level we get the customer id the order date and the months consumption figure for contact lenses or other items.  
		select c.[customer_id], c.[order_date], c.[cat_name_vshort],
			case when c.[cat_name_vshort] = 'CL' then max(c.[qty_time]) else null end as [qty_time_cl],
			case when c.[cat_name_vshort] <> 'CL' then max(c.[qty_time]) else null end as [qty_time_ncl]
		from 
			(select 
				cast(a.[customer_id] as varchar) + cast(a.[base_curve] as varchar) + cast(a.[diameter] as varchar) + cast(a.[power] as varchar) as cust_id_prescription,
				a.*, b.[target_order_date],
				case 
					when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
					when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
					else NULL 
				end as cost_per_month,
				case 
					when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies', 'CL - Yearly') then 'CL' 
					when left(a.[category_name],2) = 'EC' then 'EC'
					when left(a.[category_name],2) = 'SL' then 'SL'
					else 'OT' 
				end as cat_name_vshort
			from 
					[dbo].[ol] as a
				inner join 
					[dbo].[cust_obs_date_targetset] as b on 
						CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
			where a.[order_status_name]='OK') as c
		group by c.[customer_id], c.[order_date], c.[cat_name_vshort]) as d
		group by d.[customer_id], CONVERT(DATE, d.[order_date])) as e
order by e.customer_id, e.order_date;




-- First we must look backwards to calcuate the historical time between orders and from this calculate the index between product 
-- ordered and time taken to consume said product.
-- Join the just created table to itself to get the previous order date, from which we can look at the time taken between orders
-- To cope with two orders in the same month set the lower limit of months_to_next_order to 1 when calculating the index
drop table [dbo].[cust_order_history_bckwrd];

select c.*,
	case 
		when c.[order_est_consumption_time]=0 then cast(c.[order_est_consumption_time] as decimal(10,2)) else cast(c.[months_to_next_order] as decimal(10,2)) / c.[order_est_consumption_time] 
	end as [cust_consumption_index],

	case 
		when c.[qty_time_ncl] is not null then case when c.[qty_time_cl] is null then 1 else 2 end 
		else 0 
	end as [order_type]
into [dbo].[cust_order_history_bckwrd]
from 
	(select a.*, b.[order_date] as next_order_date,
		DATEDIFF(MONTH, CONVERT(DATE, a.[order_date]), CONVERT(DATE, b.[order_date])) as [months_to_next_order]
	from 
			[dbo].[cust_order_history] as a
		inner join 
			[dbo].[cust_order_history] as b on a.[customer_id]=b.[customer_id]
	where a.[cust_order_num]+1=b.[cust_order_num]) as c
order by c.[customer_id], c.[order_date];



-- Calculate for all of history the index between prescription duration and consumption duration;
drop table [dbo].[order_index_all];
select a.[customer_id],
	AVG(a.[cust_consumption_index]) as [consumption_index_all_orders], STDEV(a.[cust_consumption_index]) as [consumption_index_all_orders_stddev],
	COUNT(*) as [consumption_index_all_orders_N]
into [dbo].[order_index_all]
from [dbo].[cust_order_history_bckwrd] as a
group by a.[customer_id]
order by a.[customer_id];

drop table [dbo].[order_index_no_lens];
select a.[customer_id],
	AVG(a.[cust_consumption_index]) as [consumption_index_no_lenses], STDEV(a.[cust_consumption_index]) as [consumption_index_no_lenses_stddev],
	COUNT(*) as [consumption_index_no_lenses_N]
into [dbo].[order_index_no_lens]
from [dbo].[cust_order_history_bckwrd] as a
where a.[order_type] = 1
group by a.[customer_id]
order by a.[customer_id];

drop table [dbo].[order_index_lens_only];
select a.[customer_id],
	AVG(a.[cust_consumption_index]) as [consumption_index_lenses_only], STDEV(a.[cust_consumption_index]) as [consumption_index_lenses_only_stddev], 
	COUNT(*) as [consumption_index_lenses_only_N]
into [dbo].[order_index_lens_only]
from [dbo].[cust_order_history_bckwrd] as a
where a.[order_type] = 0
group by a.[customer_id]
order by a.[customer_id];


-- Merge these three indicies into one table for use later
drop table [dbo].[order_indicies];

select a.[customer_id],
	a.[consumption_index_all_orders], b.[consumption_index_no_lenses], c.[consumption_index_lenses_only],
	a.[consumption_index_all_orders_stddev], b.[consumption_index_no_lenses_stddev], c.[consumption_index_lenses_only_stddev],
	a.[consumption_index_all_orders_N], b.[consumption_index_no_lenses_N], c.[consumption_index_lenses_only_N],
	case 
		when a.[consumption_index_all_orders_stddev] <> 0 then a.[consumption_index_all_orders]/ a.[consumption_index_all_orders_stddev] 
		else null 
	end as [consumption_index_all_orders_CV],
	case 
		when b.[consumption_index_no_lenses_stddev] <> 0 then b.[consumption_index_no_lenses] / b.[consumption_index_no_lenses_stddev] 
		else null 
	end as [consumption_index_no_lenses_CV],
	case 
		when c.[consumption_index_lenses_only_stddev] <> 0 then c.[consumption_index_lenses_only] / c.[consumption_index_lenses_only_stddev] 
		else null 
	end as [consumption_index_lenses_only_CV]
into [dbo].[order_indicies]
from 
		[dbo].[order_index_all] as a
	left join 
		[dbo].[order_index_no_lens] as b on a.[customer_id]=b.[customer_id]
	left join 
		[dbo].[order_index_lens_only] as c on a.[customer_id]=c.[customer_id];


drop table [dbo].[single_cust_view_fin]
select a.*,
	b.[order_est_consumption_time], 
	c.[consumption_index_all_orders], c.[consumption_index_no_lenses], c.[consumption_index_lenses_only],
	c.[consumption_index_all_orders_stddev], c.[consumption_index_no_lenses_stddev], c.[consumption_index_lenses_only_stddev],
	c.[consumption_index_all_orders_N], c.[consumption_index_no_lenses_N], c.[consumption_index_lenses_only_N],
	c.[consumption_index_all_orders_CV], c.[consumption_index_no_lenses_CV], c.[consumption_index_lenses_only_CV],
	b.[order_est_consumption_time]*c.[consumption_index_all_orders] as [est_nmtno_all_orders],
	b.[order_est_consumption_time]*c.[consumption_index_no_lenses] as [est_nmtno_all_no_lenses],
	b.[order_est_consumption_time]*c.[consumption_index_lenses_only] as [est_nmtno_lenses_only],
	
	cast(a.[target_ndtno] as decimal) / (365/12) as [target_nmtno], b.[curr_order_type]
into [dbo].[single_cust_view_fin]
from 
		[dbo].[single_cust_view_interim2] as a 
	left join 
		[dbo].[cust_order_history] as b on a.[customer_id]=b.[customer_id] and CONVERT(DATE, a.[last_order_date]) = CONVERT(DATE, b.[order_date])
	left join 
		[dbo].[order_indicies] as c on a.[customer_id]=c.[customer_id]
order by a.[customer_id], a.[last_order_date];