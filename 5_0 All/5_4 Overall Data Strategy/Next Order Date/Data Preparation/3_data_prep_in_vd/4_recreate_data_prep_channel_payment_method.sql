
use VisionDirect
go

-- Commence data preparation for marketing channel, payment method, discount and reorder flags and shipping days
-- Create an intermediate table with the additional bits of data in the order line table that haven't made into the signature tables
drop table [dbo].[oh_addnl_cols];

select b.[customer_id], b.[last_order_date],
	case	
		when left(b.marketing_channel_name,5)='Email' then 'Email' 
		when b.marketing_channel_name in ('Refer a Friend', 'Affiliates', 'Referral', 'Redirect', 'Super Affiliates') then 'Referral' 
		when left(b.marketing_channel_name,11)='Paid Search' or b.marketing_channel_name='Display' then 'PaidSearch' 
		when b.marketing_channel_name in ('SMS Url', 'Unknown', 'Mutuelles Auto', 'Social Media') then 'Other'
		else b.marketing_channel_name 
	end as  marketing_channel_name,
	b.[discount_F] ,b.[reorder_F],
	-- case when (b.[shipping_days] <> 'NULL' and b.[shipping_days] is not null) then cast(b.[shipping_days] as real) else NULL end as [shipping_days],
	b.[shipping_days], 
	case	
		when b.[payment_method_name]='N/A' then 'Unknown'
		when b.[payment_method_name]='Card - New' then 'Card-New'
		when b.[payment_method_name]='Card - Stored' then 'Card-Stored'
		when b.[payment_method_name]='PayPal' then 'PayPal'
		else 'Other' end as [payment_method_name]
into [dbo].[oh_addnl_cols]
from 
	(select a.[customer_id], a.[order_date],
		a.[marketing_channel_name],
		case when upper(a.[discount_f])='Y' then 1 else 0 end as [discount_F], case when upper(a.[reorder_f])='Y' then 1 else 0 end as [reorder_F],
		a.[shipping_days], a.[payment_method_name], 
		c.[last_order_date]
	from 
			[dbo].[oh] as a
		inner join 
			[dbo].[single_cust_view] as c on a.[customer_id]=c.[customer_id] 
	where  CONVERT (DATE, a.[order_date])<= CONVERT (DATE, c.[last_order_date])) as b; -- PREVIOUS ORDERS to OBSERVATION DATE


-- Create an intermediate table for the summary fields on shipping - avg, discount - count and %pct and finally re-order flags.
drop table [dbo].[oh_small_summary];

select [customer_id], max([last_order_date]) as last_order_date, 
	sum([discount_F]) as [num_discounted_orders], sum([reorder_F]) as [num_reorder_orders], 
	avg([shipping_days]) as [avg_days_to_shipping], count(*) as [num_instances]
into [dbo].[oh_small_summary]
from [dbo].[oh_addnl_cols]
group by [customer_id]
order by [customer_id];

-------------------------------------




-- Do the percentage counts and transposition for the payment_method.

	-- Create a table of records of counts of sales channel per customer for the obs date
	drop table [dbo].[payment_method_name];

	select a.customer_id, max(a.last_order_date) as last_order_date ,a.[payment_method_name], count(a.[payment_method_name]) as numinst
	into [dbo].[payment_method_name]
	from [dbo].[oh_addnl_cols] as a
	group by a.customer_id, a.[payment_method_name]
	order by a.customer_id;

	-- Combine the total data onto the marketing channel data
	drop table [dbo].[payment_method_name_1];
	
	select a.* ,b.total_instances, cast(a.numinst as real) /cast(b.total_instances as real) as pct_instance
	into [dbo].[payment_method_name_1]
	from 
			[dbo].[payment_method_name] as a 
		inner join
			(select customer_id, [last_order_date], sum(numinst) as total_instances
			from [dbo].[payment_method_name]
			group by customer_id, last_order_date) as b on a.customer_id=b.customer_id and a.last_order_date=b.last_order_date
	order by customer_id, last_order_date;

	-- Next transpose this data and then link it to the single customer view.
	-- transpose the data for percentage spend by category
	drop table [dbo].[payment_method_name_trans];

	SELECT *
	into [dbo].[payment_method_name_trans]
	FROM
		(SELECT [customer_id], [last_order_date], [payment_method_name], [pct_instance]
		FROM [dbo].[payment_method_name_1]) AS SourceTable 
	PIVOT
		(AVG([pct_instance]) FOR [payment_method_name] IN
			([Card-New], [Card-Stored], [Unknown], [PayPal], [Other])) AS PivotTable
	order by [customer_id];



	--
	-- Marketing Channel Analysis
	-- Create a table of records of counts of sales channel per customer for the obs date
	drop table [dbo].[marketing_channel];

	select a.customer_id, max(a.last_order_date) as last_order_date, a.marketing_channel_name, count(a.marketing_channel_name) as numinst
	into [dbo].[marketing_channel]
	from [dbo].[oh_addnl_cols] as a
	group by a.customer_id, a.marketing_channel_name
	order by a.customer_id;

	-- Combine the total data onto the marketing channel data
	drop table [dbo].[marketing_channel_1];

	select a.*, b.total_instances, cast(a.numinst as real) /cast(b.total_instances as real) as pct_instance
	into [dbo].[marketing_channel_1]
	from 
			[dbo].[marketing_channel] as a 
		inner join
			(select customer_id, [last_order_date], sum(numinst) as total_instances
			from [dbo].[marketing_channel]
			group by customer_id, last_order_date) as b on a.customer_id=b.customer_id and a.last_order_date=b.last_order_date
	order by customer_id, last_order_date;


	-- transpose the data for percentage spend by category
	drop table [dbo].[marketing_channel_trans];

	SELECT *
	into [dbo].[marketing_channel_trans]
	FROM
			(SELECT [customer_id], [last_order_date], [marketing_channel_name], [pct_instance]
			FROM [dbo].[marketing_channel_1]) AS SourceTable 
		PIVOT
			(AVG([pct_instance]) FOR [marketing_channel_name] IN
				([Non-GA], [Organic Search], [Referral], [Email], [PaidSearch], [Customer Service], [Migration CH], [Direct Entry], [Other],[Hist CH])) AS PivotTable
	order by [customer_id];

	-- do the same for transpose the data for counts
	drop table [dbo].[marketing_channel_trans_1];

	SELECT *
	into [dbo].[marketing_channel_trans_1]
	FROM
			(SELECT [customer_id], [last_order_date], [marketing_channel_name],[numinst]
			FROM [dbo].[marketing_channel_1]) AS SourceTable 
		PIVOT
			(AVG([numinst]) FOR [marketing_channel_name] IN
				([Non-GA], [Organic Search], [Referral], [Email], [PaidSearch], [Customer Service], [Migration CH], [Direct Entry], [Other], [Hist CH])) AS PivotTable
	order by [customer_id];


-- Merge all of this data together to create a product view for the single customer view
drop table [dbo].[marketing_channel_summary];
select a.[customer_id], a.[last_order_date], 
	case when a.[Non-GA] is NULL then 0 else a.[Non-GA] end as [ct_NonGA]
	,case when a.[Organic Search] is NULL then 0 else a.[Organic Search] end as [ct_OrganicSearch]
	,case when a.[Referral] is NULL then 0 else a.[Referral] end as [ct_Referral]
	,case when a.[Email] is NULL then 0 else a.[Email] end as [ct_Email]
	,case when a.[PaidSearch] is NULL then 0 else a.[PaidSearch] end as [ct_PaidSearch]
	,case when a.[Other] is NULL then 0 else a.[Other] end as [ct_spend_Other]
	,case when a.[Migration CH] is NULL then 0 else a.[Migration CH] end as [ct_Migration CH]
	,case when a.[Direct Entry] is NULL then 0 else a.[Direct Entry] end as [ct_DirectEntry]
	,case when a.[Hist CH] is NULL then 0 else a.[Hist CH] end as [ct_HistCH]

	,case when a.[Non-GA] is NULL then 0 else a.[Non-GA] end as [pct_NonGA]
	,case when a.[Organic Search] is NULL then 0 else a.[Organic Search] end as [pct_OrganicSearch]
	,case when a.[Referral] is NULL then 0 else a.[Referral] end as [pct_Referral]
	,case when a.[Email] is NULL then 0 else a.[Email] end as [pct_Email]
	,case when a.[PaidSearch] is NULL then 0 else a.[PaidSearch] end as [pct_PaidSearch]
	,case when a.[Other] is NULL then 0 else a.[Other] end as [pct_spend_Other]
	,case when a.[Migration CH] is NULL then 0 else a.[Migration CH] end as [pct_MigrationCH]
	,case when a.[Direct Entry] is NULL then 0 else a.[Direct Entry] end as [pct_DirectEntry]
	,case when a.[Hist CH] is NULL then 0 else a.[Hist CH] end as [pct_HistCH]

	,c.[avg_days_to_shipping]
	,c.[num_discounted_orders]
	,c.[num_reorder_orders]

	,case when d.[Card-New] is NULL then 0 else d.[Card-New] end as [pm_pct_card_new]
	,case when d.[Card-Stored] is NULL then 0 else d.[Card-Stored] end as [pm_pct_card_stored]
	,case when d.[PayPal] is NULL then 0 else d.[PayPal] end as [pm_pct_paypal]
	,case when d.[Other] is NULL then 0 else d.[Other] end as [pm_pct_other]
	,case when d.[Unknown] is NULL then 0 else d.[Unknown] end as [pm_pct_unknown]

	into [dbo].[marketing_channel_summary]
	from 
			[dbo].[marketing_channel_trans] as a
		inner join 
			[dbo].[marketing_channel_trans_1] as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date]
		left join 
			[dbo].[oh_small_summary] as c on a.[customer_id]=c.[customer_id] and a.[last_order_date]=c.[last_order_date]
		left join 
			[dbo].[payment_method_name_trans] as d on a.[customer_id]=d.[customer_id] and a.[last_order_date]=d.[last_order_date]
	order by a.[customer_id];


-- Create the final final output table
drop table [dbo].[single_cust_view_interim1];
select a.*, 
	b.[ct_NonGA], b.[ct_OrganicSearch], b.[ct_Referral], b.[ct_Email], b.[ct_PaidSearch], b.[ct_spend_Other], b.[ct_Migration CH], b.[ct_DirectEntry], b.[ct_HistCH],
	b.[pct_NonGA], b.[pct_OrganicSearch], b.[pct_Referral], b.[pct_Email], b.[pct_PaidSearch], b.[pct_spend_Other], b.[pct_MigrationCH], b.[pct_DirectEntry] ,b.[pct_HistCH], 
	b.[avg_days_to_shipping], b.[num_discounted_orders], b.[num_reorder_orders], 
	b.[pm_pct_card_new], b.[pm_pct_card_stored], b.[pm_pct_paypal], b.[pm_pct_other], b.[pm_pct_unknown]
into [dbo].[single_cust_view_interim1]
from 
		[dbo].[single_cust_view] as a 
	inner join
		[dbo].[marketing_channel_summary] as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date];