use VisionDirect
go

----------------------------------------- Notes

	-- Per Customer - Order takes the qty_time per Contact Lens - No Contact Lens
		-- Why diff between Contact Lens - No Contact Lens
		-- If different qty_time per Order then MAX --- WHY
	-- order_est_consumption_time based on qty_time (if possible the qty_time_cl)
	-- curr_order_type: Flag identifying the order type: 0 - 1 - 2

			select 
				cast(a.[customer_id] as varchar) + cast(a.[base_curve] as varchar) + cast(a.[diameter] as varchar) + cast(a.[power] as varchar) as cust_id_prescription,
				a.customer_id, a.order_line_id_bk, a.order_id_bk, a.order_date, 
				a.category_name, a.product_id_magento, a.product_family_name, 
				a.qty_unit, a.qty_pack, a.qty_time, 
				a.local_subtotal, a.local_total_inc_vat,
				b.[target_order_date], 
				case 
					when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
					when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
					else NULL 
				end as cost_per_month,
				case 
					when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies', 'CL - Yearly') then 'CL' 
					when left(a.[category_name],2) = 'EC' then 'EC'
					when left(a.[category_name],2) = 'SL' then 'SL'
					else 'OT' 
				end as cat_name_vshort
			from 
					[dbo].[ol] as a
				inner join 
					[dbo].[cust_obs_date_targetset] as b on 
						CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
			where a.[order_status_name]='OK'
				and a.customer_id in (1433563, 1480267)

		select c.[customer_id], c.[order_date], c.[cat_name_vshort],
			case when c.[cat_name_vshort] = 'CL' then max(c.[qty_time]) else null end as [qty_time_cl],
			case when c.[cat_name_vshort] <> 'CL' then max(c.[qty_time]) else null end as [qty_time_ncl]
		from
			(select 
				cast(a.[customer_id] as varchar) + cast(a.[base_curve] as varchar) + cast(a.[diameter] as varchar) + cast(a.[power] as varchar) as cust_id_prescription,
				a.customer_id, a.order_line_id_bk, a.order_id_bk, a.order_date, 
				a.category_name, a.product_id_magento, a.product_family_name, 
				a.qty_unit, a.qty_pack, a.qty_time, 
				a.local_subtotal, a.local_total_inc_vat,
				b.[target_order_date], 
				case 
					when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
					when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
					else NULL 
				end as cost_per_month,
				case 
					when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies', 'CL - Yearly') then 'CL' 
					when left(a.[category_name],2) = 'EC' then 'EC'
					when left(a.[category_name],2) = 'SL' then 'SL'
					else 'OT' 
				end as cat_name_vshort
			from 
					[dbo].[ol] as a
				inner join 
					[dbo].[cust_obs_date_targetset] as b on 
						CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
			where a.[order_status_name]='OK'
				and a.customer_id in (1433563, 1480267)) c
		group by c.[customer_id], c.[order_date], c.[cat_name_vshort]
		order by c.[customer_id], c.[order_date]

	select d.[customer_id], max(d.[order_date]) as [order_date], max(d.[qty_time_cl]) as [qty_time_cl] ,max(d.[qty_time_ncl]) as [qty_time_ncl]
	from
		(select c.[customer_id], c.[order_date], c.[cat_name_vshort],
			case when c.[cat_name_vshort] = 'CL' then max(c.[qty_time]) else null end as [qty_time_cl],
			case when c.[cat_name_vshort] <> 'CL' then max(c.[qty_time]) else null end as [qty_time_ncl]
		from
			(select 
				cast(a.[customer_id] as varchar) + cast(a.[base_curve] as varchar) + cast(a.[diameter] as varchar) + cast(a.[power] as varchar) as cust_id_prescription,
				a.customer_id, a.order_line_id_bk, a.order_id_bk, a.order_date, 
				a.category_name, a.product_id_magento, a.product_family_name, 
				a.qty_unit, a.qty_pack, a.qty_time, 
				a.local_subtotal, a.local_total_inc_vat,
				b.[target_order_date], 
				case 
					when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
					when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
					else NULL 
				end as cost_per_month,
				case 
					when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies', 'CL - Yearly') then 'CL' 
					when left(a.[category_name],2) = 'EC' then 'EC'
					when left(a.[category_name],2) = 'SL' then 'SL'
					else 'OT' 
				end as cat_name_vshort
			from 
					[dbo].[ol] as a
				inner join 
					[dbo].[cust_obs_date_targetset] as b on 
						CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
			where a.[order_status_name]='OK'
				and a.customer_id in (1433563, 1480267)) c
		group by c.[customer_id], c.[order_date], c.[cat_name_vshort]) d
	group by d.[customer_id], CONVERT(DATE, d.[order_date])


select e.[customer_id], e.[order_date], 
	e.[qty_time_cl], e.[qty_time_ncl],

	case when e.[qty_time_cl] is not null then e.[qty_time_cl] else e.[qty_time_ncl] end as [order_est_consumption_time],
	
	rank() over(partition by e.[customer_id] order by e.[order_date] asc) as [cust_order_num], 
	case 
		when e.[qty_time_ncl] is not null then 
			case when e.[qty_time_cl] is null then 1 else 2 end 
			else 0 
		end as [curr_order_type]
from
	(select d.[customer_id], max(d.[order_date]) as [order_date], max(d.[qty_time_cl]) as [qty_time_cl] ,max(d.[qty_time_ncl]) as [qty_time_ncl]
	from
		(select c.[customer_id], c.[order_date], c.[cat_name_vshort],
			case when c.[cat_name_vshort] = 'CL' then max(c.[qty_time]) else null end as [qty_time_cl],
			case when c.[cat_name_vshort] <> 'CL' then max(c.[qty_time]) else null end as [qty_time_ncl]
		from
			(select 
				cast(a.[customer_id] as varchar) + cast(a.[base_curve] as varchar) + cast(a.[diameter] as varchar) + cast(a.[power] as varchar) as cust_id_prescription,
				a.customer_id, a.order_line_id_bk, a.order_id_bk, a.order_date, 
				a.category_name, a.product_id_magento, a.product_family_name, 
				a.qty_unit, a.qty_pack, a.qty_time, 
				a.local_subtotal, a.local_total_inc_vat,
				b.[target_order_date], 
				case 
					when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
					when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
					else NULL 
				end as cost_per_month,
				case 
					when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies', 'CL - Yearly') then 'CL' 
					when left(a.[category_name],2) = 'EC' then 'EC'
					when left(a.[category_name],2) = 'SL' then 'SL'
					else 'OT' 
				end as cat_name_vshort
			from 
					[dbo].[ol] as a
				inner join 
					[dbo].[cust_obs_date_targetset] as b on 
						CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
			where a.[order_status_name]='OK'
				and a.customer_id in (1433563, 1480267)) c
		group by c.[customer_id], c.[order_date], c.[cat_name_vshort]) d
	group by d.[customer_id], CONVERT(DATE, d.[order_date])) e
order by e.[customer_id], e.[order_date]

----------------------------------------------------------------

	-- Per Customer - Order take the next order made and calculate the diff months
		-- How months calculated from num_days
	-- Calculate
		-- Per Order cust_consumption_index: months_to_next_order / order_est_consumption_time
			

	select a.*, 
		b.[order_date] as next_order_date,
		DATEDIFF(MONTH, CONVERT(DATE, a.[order_date]), CONVERT(DATE, b.[order_date])) as [months_to_next_order]
	from 
			[dbo].[cust_order_history] as a
		inner join 
			[dbo].[cust_order_history] as b on a.[customer_id]=b.[customer_id]
	where a.[cust_order_num]+1=b.[cust_order_num]
		and a.customer_id in (1433563, 1480267)

select c.*,
	case 
		when c.[order_est_consumption_time]=0 then cast(c.[order_est_consumption_time] as decimal(10,2)) else cast(c.[months_to_next_order] as decimal(10,2)) / c.[order_est_consumption_time] 
	end as [cust_consumption_index],

	case 
		when c.[qty_time_ncl] is not null then case when c.[qty_time_cl] is null then 1 else 2 end 
		else 0 
	end as [order_type]
from 
	(select a.*, 
		b.[order_date] as next_order_date,
		DATEDIFF(MONTH, CONVERT(DATE, a.[order_date]), CONVERT(DATE, b.[order_date])) as [months_to_next_order]
	from 
			[dbo].[cust_order_history] as a
		inner join 
			[dbo].[cust_order_history] as b on a.[customer_id]=b.[customer_id]
	where a.[cust_order_num]+1=b.[cust_order_num]
		and a.customer_id in (1433563, 1480267)) as c
order by c.[customer_id], c.[order_date];

----------------------------------------------------------------

	-- Calculate
		-- Per Customer cust_consumption_index: AVG - STDEV - NUM_ORDERS - COEFFICENT VARIATION
			-- Why cust_consumption_index order to order
		-- Depending on Order Type: ALL - ONLY_LENS - ONLY_NO_LENS

----------------------------------------------------------------

	-- Only take on b the order_date = last_order_date
		
		-- est_nmtno_all_orders: Estimate next order time depending on order_est_consumption_time * consumption_index_all_orders

select b.customer_id, b.order_date, 
	b.order_est_consumption_time, 
	b.cust_order_num, b.curr_order_type, 
	c.consumption_index_all_orders, c.consumption_index_all_orders_stddev, c.consumption_index_all_orders_n, c.consumption_index_all_orders_cv 
from
		[dbo].[cust_order_history] as b 
	left join 
		[dbo].[order_indicies] as c on b.[customer_id]=c.[customer_id]
where b.customer_id in (1433563, 1480267)

	
	select consumption_index_all_orders, count(*)
	from [dbo].[order_indicies]
	group by consumption_index_all_orders
	order by consumption_index_all_orders