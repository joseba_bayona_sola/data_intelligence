
use VisionDirect
go

DROP table [dbo].[cust_obs_date_targetset];

select z.*,
	DATEDIFF(day, z.target_order_date, z.order_date) as target_ndtno_orig,
	
	-- If there is no subsequent purchase then set the days to next order to be 15 months
	-- if the days to next order is greater than 15 months then clip it to 15 months.
		case 
			when DATEDIFF(day, z.target_order_date, z.order_date) = 0 then 460
			when DATEDIFF(day, z.target_order_date, z.order_date) > 460 then 460
			else DATEDIFF(day, z.target_order_date, z.order_date) 
		end as target_ndtno, 

		case 
			when DATEDIFF(day, z.target_order_date, z.order_date) = 0 then 0 
			when DATEDIFF(month, z.target_order_date, z.order_date) <= 15 then 1 
			else 0 
		end as [target_renewal]

into [dbo].[cust_obs_date_targetset]
from
	(select c.*,
		ROW_NUMBER() over (partition by c.[customer_id] order by c.[order_date] desc) as last_rec
	from
		(SELECT a.[order_id_bk], a.[order_status_name], a.[customer_id], a.[order_date],
			a.next_order_freq_time, 
			a.[reminder_date],
			
			b.[order_date] as [target_order_date], b.[order_id] as [target_order_id], 
			ROW_NUMBER() over (partition by a.[customer_id] order by a.[order_date]) as gb_rownum
		FROM 
				[dbo].[oh] as a
			left join 
				[dbo].[cust_obs_date] as b on a.[customer_id] = b.[customer_id] 
		where a.[order_date] >= b.[order_date]) as c
	where c.gb_rownum <= 2) as z
where z.[last_rec] = 1
order by z.[customer_id], z.[order_date];


------------------------------------------------------------

drop table [dbo].[cust_signature_obs_wt];

SELECT a.[customer_id],
	a.[website_create], a.[create_date], a.[create_time_mm], a.[website_register], 
	a.[market_name], a.[country_code_ship],
	a.[customer_unsubscribe_name],
	a.[first_order_date], a.[last_order_date], 
	a.[customer_status_name], a.[rank_num_tot_orders], a.[num_tot_orders],
	a.[rank_num_tot_cancel], a.[num_tot_cancel], a.[rank_num_tot_refund], a.[num_tot_refund], a.[num_tot_discount_orders],
	a.[main_type_oh_name], a.[num_diff_product_type_oh],
	a.[num_dist_products],
	a.[main_order_qty_time], a.[rank_avg_order_qty_time], a.[avg_order_qty_time], a.[rank_stdev_order_qty_time], a.[stdev_order_qty_time],
	a.[rank_avg_order_freq_time], a.[avg_order_freq_time], a.[rank_stdev_order_freq_time], a.[stdev_order_freq_time],
	
	cast(datediff(month,a.[create_date], b.[target_order_date]) as real) / 12 as length_of_relationship, b.[target_order_date], b.[target_order_id],
	-- b.[gb_rownum], b.[last_rec]
    b.[target_ndtno_orig], b.[target_ndtno], b.[target_renewal]

into [dbo].[cust_signature_obs_wt]
FROM 
		[dbo].[cust_signature_obs] as a 
	inner join 
		[dbo].[cust_obs_date_targetset] as b on a.[customer_id]=b.[customer_id] and CONVERT (DATE, a.[last_order_date]) = CONVERT (DATE, b.[target_order_date]) 
order by a.[customer_id]