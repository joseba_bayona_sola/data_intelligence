
-- cust_obs_date_targetset

	-- target_order_date, targer_order_id: the order taken at Observation
	-- order_date, order_id_bk: NEXT order date from Observation (SAME if there is not NEXT)
	-- target_ntdno_orig: Diff days between orders
	-- target_ntdno_orig: Diff days between orders (460 if bigger than 15 months)
	-- target_renewal: Flag 0 - 1 reflecting CHURN

select top 1000 *
from VisionDirect.dbo.cust_obs_date_targetset
-- where customer_id in (1433563, 1480267)
where order_date <> target_order_date
order by customer_id, order_date

	select count(*), count(distinct customer_id)
	from VisionDirect.dbo.cust_obs_date_targetset	
	where order_date = target_order_date

	select target_renewal, count(*), count(distinct customer_id)
	from VisionDirect.dbo.cust_obs_date_targetset	
	group by target_renewal
	order by target_renewal


-- cust_signature_obs_wt
	-- length_of_relationship: In Years 

select top 1000 
	customer_id, website_create, create_date, create_time_mm,
	website_register, market_name, country_code_ship, customer_unsubscribe_name, 
	first_order_date, last_order_date, 
	customer_status_name, rank_num_tot_orders, num_tot_orders, rank_num_tot_cancel, num_tot_cancel, rank_num_tot_refund, num_tot_refund, num_tot_discount_orders, 
	main_type_oh_name, num_diff_product_type_oh, num_dist_products,
	main_order_qty_time, rank_avg_order_qty_time, avg_order_qty_time, rank_stdev_order_qty_time, stdev_order_qty_time,
	rank_avg_order_freq_time, avg_order_freq_time, rank_stdev_order_freq_time, stdev_order_freq_time,

	length_of_relationship, target_order_date, target_order_id,
    target_ndtno_orig, target_ndtno, target_renewal
from VisionDirect.dbo.cust_signature_obs_wt
where customer_id in (1433563, 1480267)
order by customer_id

	select rank_num_tot_orders, count(*), count(distinct customer_id)
	from VisionDirect.dbo.cust_signature_obs_wt	
	group by rank_num_tot_orders
	order by rank_num_tot_orders

	select target_renewal, count(*), count(distinct customer_id)
	from VisionDirect.dbo.cust_signature_obs_wt	
	group by target_renewal
	order by target_renewal

	select rank_num_tot_orders, target_renewal, count(*), count(distinct customer_id)
	from VisionDirect.dbo.cust_signature_obs_wt	
	group by rank_num_tot_orders, target_renewal
	order by rank_num_tot_orders, target_renewal

------------------------------------------------------------------
------------------------------------------------------------------

	select top 1000 *
	from VisionDirect.dbo.product_spend_history
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.product_spend_history	

	select top 1000 *
	from VisionDirect.dbo.product_spend
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.product_spend

			select top 1000 *
			from VisionDirect.dbo.product_orders_trans
			where customer_id in (1433563, 1480267)
			order by customer_id

			select top 1000 *
			from VisionDirect.dbo.product_orders_trans_pct
			where customer_id in (1433563, 1480267)
			order by customer_id

			select top 1000 *
			from VisionDirect.dbo.product_spend_trans
			where customer_id in (1433563, 1480267)
			order by customer_id

			select top 1000 *
			from VisionDirect.dbo.product_spend_trans_pct
			where customer_id in (1433563, 1480267)
			order by customer_id

	select top 1000 *
	from VisionDirect.dbo.customer_prod_summary_1
	where customer_id in (1433563, 1480267)

	select top 1000 *
	from VisionDirect.dbo.customer_prod_summary
	where customer_id in (1433563, 1480267)

	-- single_cust_view
	select top 1000 *
	from VisionDirect.dbo.single_cust_view
	where customer_id in (1433563, 1480267)

	select top 1000 	
		customer_id, website_create, create_date, create_time_mm,
		website_register, market_name, country_code_ship, customer_unsubscribe_name, 
		first_order_date, last_order_date, 
		customer_status_name, rank_num_tot_orders, num_tot_orders, rank_num_tot_cancel, num_tot_cancel, rank_num_tot_refund, num_tot_refund, num_tot_discount_orders, 
		main_type_oh_name, num_diff_product_type_oh, num_dist_products,
		main_order_qty_time, rank_avg_order_qty_time, avg_order_qty_time, rank_stdev_order_qty_time, stdev_order_qty_time,
		rank_avg_order_freq_time, avg_order_freq_time, rank_stdev_order_freq_time, stdev_order_freq_time,

		length_of_relationship, target_order_date, target_order_id,
		target_ndtno_orig, target_ndtno, target_renewal, 

		all_history_spend, all_history_product_count, 

		spend_SL, spend_CLDaily, spend_CLTwoWeeklies, spend_CLMonthlies, spend_EC, spend_Other, 
		spend_pct_SL, spend_pct_CLDaily, spend_pct_CLTwoWeeklies, spend_pct_CLMonthlies, spend_pct_EC, spend_pct_Other, 
		orders_SL, orders_CLDaily, orders_CLTwoWeeklies, orders_CLMonthlies, orders_EC, orders_Other, 
		orders_pct_SL, orders_pct_CLDaily, orders_pct_CLTwoWeeklies, orders_pct_CLMonthlies, orders_pct_EC, orders_pct_Other

	from VisionDirect.dbo.single_cust_view
	where customer_id in (1433563, 1480267)

	
------------------------------------------------------------------
------------------------------------------------------------------

	select top 1000 *
	from VisionDirect.dbo.oh_addnl_cols
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.oh_addnl_cols

	select top 1000 *
	from VisionDirect.dbo.oh_small_summary
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.oh_small_summary



	select top 1000 *
	from VisionDirect.dbo.payment_method_name
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.payment_method_name

	select top 1000 *
	from VisionDirect.dbo.payment_method_name_1
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.payment_method_name_1

	select top 1000 *
	from VisionDirect.dbo.payment_method_name_trans
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.payment_method_name_trans
		
		

	select top 1000 *
	from VisionDirect.dbo.marketing_channel
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.marketing_channel

	select top 1000 *
	from VisionDirect.dbo.marketing_channel_1
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.marketing_channel_1

	select top 1000 *
	from VisionDirect.dbo.marketing_channel_trans
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.marketing_channel_trans

	select top 1000 *
	from VisionDirect.dbo.marketing_channel_trans_1
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.marketing_channel_trans_1

		
	select top 1000 *
	from VisionDirect.dbo.marketing_channel_summary
	where customer_id in (1433563, 1480267)
	order by customer_id, last_order_date

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.marketing_channel_summary


	-- single_cust_view_interim1
	select top 1000 *
	from VisionDirect.dbo.single_cust_view_interim1
	where customer_id in (1433563, 1480267)
	order by customer_id

	select top 1000 
		customer_id, website_create, create_date, create_time_mm,
		website_register, market_name, country_code_ship, customer_unsubscribe_name, 
		first_order_date, last_order_date, 
		customer_status_name, rank_num_tot_orders, num_tot_orders, rank_num_tot_cancel, num_tot_cancel, rank_num_tot_refund, num_tot_refund, num_tot_discount_orders, 
		main_type_oh_name, num_diff_product_type_oh, num_dist_products,
		main_order_qty_time, rank_avg_order_qty_time, avg_order_qty_time, rank_stdev_order_qty_time, stdev_order_qty_time,
		rank_avg_order_freq_time, avg_order_freq_time, rank_stdev_order_freq_time, stdev_order_freq_time,

		length_of_relationship, target_order_date, target_order_id,
		target_ndtno_orig, target_ndtno, target_renewal, 

		all_history_spend, all_history_product_count, 

		spend_SL, spend_CLDaily, spend_CLTwoWeeklies, spend_CLMonthlies, spend_EC, spend_Other, 
		spend_pct_SL, spend_pct_CLDaily, spend_pct_CLTwoWeeklies, spend_pct_CLMonthlies, spend_pct_EC, spend_pct_Other, 
		orders_SL, orders_CLDaily, orders_CLTwoWeeklies, orders_CLMonthlies, orders_EC, orders_Other, 
		orders_pct_SL, orders_pct_CLDaily, orders_pct_CLTwoWeeklies, orders_pct_CLMonthlies, orders_pct_EC, orders_pct_Other, 

		ct_NonGA, ct_OrganicSearch, ct_Referral, ct_Email, ct_PaidSearch, ct_spend_Other, [ct_Migration CH], ct_DirectEntry, ct_HistCH,
		pct_NonGA, pct_OrganicSearch, pct_Referral, pct_Email, pct_PaidSearch, pct_spend_Other, pct_MigrationCH, pct_DirectEntry, pct_HistCH, 
		avg_days_to_shipping, num_discounted_orders, num_reorder_orders, 
		pm_pct_card_new, pm_pct_card_stored, pm_pct_paypal, pm_pct_other, pm_pct_unknown

	from VisionDirect.dbo.single_cust_view_interim1
	where customer_id in (1433563, 1480267)
	order by customer_id

	
------------------------------------------------------------------
------------------------------------------------------------------

	select top 1000 *
	from VisionDirect.dbo.ol_model_base
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.ol_model_base

	select top 1000 *
	from VisionDirect.dbo.ol_model_base_trans
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.ol_model_base_trans	

	select top 1000 *
	from VisionDirect.dbo.cust_avg_spend
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.cust_avg_spend
		

	-- single_cust_view_interim2
	select top 1000 *
	from VisionDirect.dbo.single_cust_view_interim2
	where customer_id in (1433563, 1480267)
	order by customer_id

	select top 1000 
		customer_id, website_create, create_date, create_time_mm,
		website_register, market_name, country_code_ship, customer_unsubscribe_name, 
		first_order_date, last_order_date, 
		customer_status_name, rank_num_tot_orders, num_tot_orders, rank_num_tot_cancel, num_tot_cancel, rank_num_tot_refund, num_tot_refund, num_tot_discount_orders, 
		main_type_oh_name, num_diff_product_type_oh, num_dist_products,
		main_order_qty_time, rank_avg_order_qty_time, avg_order_qty_time, rank_stdev_order_qty_time, stdev_order_qty_time,
		rank_avg_order_freq_time, avg_order_freq_time, rank_stdev_order_freq_time, stdev_order_freq_time,

		length_of_relationship, target_order_date, target_order_id,
		target_ndtno_orig, target_ndtno, target_renewal, 

		all_history_spend, all_history_product_count, 

		spend_SL, spend_CLDaily, spend_CLTwoWeeklies, spend_CLMonthlies, spend_EC, spend_Other, 
		spend_pct_SL, spend_pct_CLDaily, spend_pct_CLTwoWeeklies, spend_pct_CLMonthlies, spend_pct_EC, spend_pct_Other, 
		orders_SL, orders_CLDaily, orders_CLTwoWeeklies, orders_CLMonthlies, orders_EC, orders_Other, 
		orders_pct_SL, orders_pct_CLDaily, orders_pct_CLTwoWeeklies, orders_pct_CLMonthlies, orders_pct_EC, orders_pct_Other, 

		ct_NonGA, ct_OrganicSearch, ct_Referral, ct_Email, ct_PaidSearch, ct_spend_Other, [ct_Migration CH], ct_DirectEntry, ct_HistCH,
		pct_NonGA, pct_OrganicSearch, pct_Referral, pct_Email, pct_PaidSearch, pct_spend_Other, pct_MigrationCH, pct_DirectEntry, pct_HistCH, 
		avg_days_to_shipping, num_discounted_orders, num_reorder_orders, 
		pm_pct_card_new, pm_pct_card_stored, pm_pct_paypal, pm_pct_other, pm_pct_unknown, 

		avg_unit_price_paid_CLM, avg_unit_price_paid_CLD, avg_unit_price_paid_CL2W, avg_unit_price_paid_SL, avg_unit_price_paid_EC, avg_unit_price_paid_Oth, 
		rank_pct_aupp_CLM, rank_pct_aupp_CLD, rank_pct_aupp_C2W, rank_pct_aupp_SL, rank_pct_aupp_EC, rank_pct_aupp_Oth
	from VisionDirect.dbo.single_cust_view_interim2
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.single_cust_view_interim2


------------------------------------------------------------------
------------------------------------------------------------------

	select top 1000 *
	from VisionDirect.dbo.cust_order_history
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id), min(order_date), max(order_date)
		from VisionDirect.dbo.cust_order_history

	select top 1000 *
	from VisionDirect.dbo.cust_order_history_bckwrd
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id), min(order_date), max(order_date)
		from VisionDirect.dbo.cust_order_history_bckwrd
		

			select top 1000 *
			from VisionDirect.dbo.order_index_all
			where customer_id in (1433563, 1480267)
			order by customer_id

				select count(*), count(distinct customer_id), min(order_date), max(order_date)
				from VisionDirect.dbo.order_index_all

			select top 1000 *
			from VisionDirect.dbo.order_index_no_lens
			where customer_id in (1433563, 1480267)
			order by customer_id

				select count(*), count(distinct customer_id), min(order_date), max(order_date)
				from VisionDirect.dbo.order_index_no_lens

			select top 1000 *
			from VisionDirect.dbo.order_index_lens_only
			where customer_id in (1433563, 1480267)
			order by customer_id

				select count(*), count(distinct customer_id), min(order_date), max(order_date)
				from VisionDirect.dbo.order_index_lens_only

	select top 1000 *
	from VisionDirect.dbo.order_indicies
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.order_indicies

	
--------------------------------------------------------------

	-- single_cust_view_fin
	select top 1000 *
	from VisionDirect.dbo.single_cust_view_fin
	where customer_id in (1433563, 1480267)
	order by customer_id

	select top 1000 
		customer_id, website_create, create_date, create_time_mm,
		website_register, market_name, country_code_ship, customer_unsubscribe_name, 
		first_order_date, last_order_date, 
		customer_status_name, rank_num_tot_orders, num_tot_orders, rank_num_tot_cancel, num_tot_cancel, rank_num_tot_refund, num_tot_refund, num_tot_discount_orders, 
		main_type_oh_name, num_diff_product_type_oh, num_dist_products,
		main_order_qty_time, rank_avg_order_qty_time, avg_order_qty_time, rank_stdev_order_qty_time, stdev_order_qty_time,
		rank_avg_order_freq_time, avg_order_freq_time, rank_stdev_order_freq_time, stdev_order_freq_time,

		length_of_relationship, target_order_date, target_order_id,
		target_ndtno_orig, target_ndtno, target_renewal, 

		all_history_spend, all_history_product_count, 

		spend_SL, spend_CLDaily, spend_CLTwoWeeklies, spend_CLMonthlies, spend_EC, spend_Other, 
		spend_pct_SL, spend_pct_CLDaily, spend_pct_CLTwoWeeklies, spend_pct_CLMonthlies, spend_pct_EC, spend_pct_Other, 
		orders_SL, orders_CLDaily, orders_CLTwoWeeklies, orders_CLMonthlies, orders_EC, orders_Other, 
		orders_pct_SL, orders_pct_CLDaily, orders_pct_CLTwoWeeklies, orders_pct_CLMonthlies, orders_pct_EC, orders_pct_Other, 

		ct_NonGA, ct_OrganicSearch, ct_Referral, ct_Email, ct_PaidSearch, ct_spend_Other, [ct_Migration CH], ct_DirectEntry, ct_HistCH,
		pct_NonGA, pct_OrganicSearch, pct_Referral, pct_Email, pct_PaidSearch, pct_spend_Other, pct_MigrationCH, pct_DirectEntry, pct_HistCH, 
		avg_days_to_shipping, num_discounted_orders, num_reorder_orders, 
		pm_pct_card_new, pm_pct_card_stored, pm_pct_paypal, pm_pct_other, pm_pct_unknown, 

		avg_unit_price_paid_CLM, avg_unit_price_paid_CLD, avg_unit_price_paid_CL2W, avg_unit_price_paid_SL, avg_unit_price_paid_EC, avg_unit_price_paid_Oth, 
		rank_pct_aupp_CLM, rank_pct_aupp_CLD, rank_pct_aupp_C2W, rank_pct_aupp_SL, rank_pct_aupp_EC, rank_pct_aupp_Oth, 

		order_est_consumption_time, 
		consumption_index_all_orders, consumption_index_no_lenses, consumption_index_lenses_only,
		consumption_index_all_orders_stddev, consumption_index_no_lenses_stddev, consumption_index_lenses_only_stddev,
		consumption_index_all_orders_N, consumption_index_no_lenses_N, consumption_index_lenses_only_N,
		consumption_index_all_orders_CV, consumption_index_no_lenses_CV, consumption_index_lenses_only_CV,
		est_nmtno_all_orders, est_nmtno_all_no_lenses, est_nmtno_lenses_only,
		target_nmtno, curr_order_type
	from VisionDirect.dbo.single_cust_view_fin
	where customer_id in (1433563, 1480267)
	order by customer_id

		select count(*), count(distinct customer_id)
		from VisionDirect.dbo.single_cust_view_fin

	select top 1000 
		customer_id, 
		customer_status_name, num_tot_orders, 
		avg_order_qty_time, stdev_order_qty_time,
		avg_order_freq_time, stdev_order_freq_time,

		order_est_consumption_time, 
		consumption_index_all_orders, 
		consumption_index_all_orders_stddev, 
		consumption_index_all_orders_N, 
		consumption_index_all_orders_CV, 
		est_nmtno_all_orders, 
		target_nmtno, curr_order_type
	from VisionDirect.dbo.single_cust_view_fin
	-- where customer_id in (1433563, 1480267)
	order by customer_id