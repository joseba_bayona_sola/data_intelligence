
-- Notes
	-- Better to use Global instead of Local 
	-- Better to use Subtotal instead of Local Total Inc VAT
	-- Is cost_per_month calculated correctly - What about OL where qty_time = 0

	-- On Trans we take the average value of customer spent per category

	select a.customer_id, a.order_line_id_bk, a.order_id_bk, a.order_date, 
		a.category_name, a.product_id_magento, a.product_family_name, 
		a.qty_unit, a.qty_pack, a.qty_time, 
		a.local_subtotal, a.local_total_inc_vat,
		b.[target_order_date], 
		case 
			when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
			when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
			else NULL 
		end as cost_per_month, -- HOW CALCULATED COST_PER_MONTH
		
		case 
			when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies') then a.[category_name] 
			when a.[category_name] = 'CL - Yearly' then 'CL - Monthlies'
			when left(a.[category_name],2) = 'EC' then 'EC'
			when left(a.[category_name],2) = 'SL' then 'SL'
			else 'Other' end as cat_name_short
	from 
			[dbo].[ol] as a
		inner join 
			[dbo].[cust_obs_date_targetset] as b on CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
		where a.[order_status_name]='OK'
			and a.customer_id in (1433563, 1480267)
	order by a.customer_id, a.order_date, a.order_line_id_bk


select c.[customer_id], 
	max(c.[target_order_date]) as [target_order_date], c.[cat_name_short], 
	count(*) as num_product, 
	min(c.[cost_per_month]) as cost_per_month_min, max(c.[cost_per_month]) as cost_per_month_max, 
	avg(c.[cost_per_month]) as cost_per_month_avg, sum(c.[cost_per_month]) as cost_per_month_sum
from 
	(select a.customer_id, a.order_line_id_bk, a.order_id_bk, a.order_date, 
		a.category_name, a.product_id_magento, a.product_family_name, 
		a.qty_unit, a.qty_pack, a.qty_time, 
		a.local_subtotal, a.local_total_inc_vat,
		b.[target_order_date], 
		case 
			when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
			when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
			else NULL 
		end as cost_per_month, -- HOW CALCULATED COST_PER_MONTH
		
		case 
			when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies') then a.[category_name] 
			when a.[category_name] = 'CL - Yearly' then 'CL - Monthlies'
			when left(a.[category_name],2) = 'EC' then 'EC'
			when left(a.[category_name],2) = 'SL' then 'SL'
			else 'Other' end as cat_name_short
	from 
			[dbo].[ol] as a
		inner join 
			[dbo].[cust_obs_date_targetset] as b on CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
		where a.[order_status_name]='OK'
			and a.customer_id in (1433563, 1480267)) as c
group by c.[customer_id], c.[cat_name_short]
order by c.[customer_id], c.[cat_name_short];

select *
from
	(select cat_name_short, category_name, product_id_magento, product_family_name, cost_per_month, count(*) num, 
		sum(count(*)) over (partition by product_id_magento) num_tot_product,
		-- count(*) over () num_rows, 
		rank() over (partition by product_id_magento order by count(*) desc) rank_product
	from
		(select 
			case 
				when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies') then a.[category_name] 
				when a.[category_name] = 'CL - Yearly' then 'CL - Monthlies'
				when left(a.[category_name],2) = 'EC' then 'EC'
				when left(a.[category_name],2) = 'SL' then 'SL'
				else 'Other' 
			end as cat_name_short,		
			a.category_name, a.product_id_magento, a.product_family_name, 
			case 
				when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
				when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
				else NULL 
			end as cost_per_month

		from 
				[dbo].[ol] as a
			inner join 
				[dbo].[cust_obs_date_targetset] as b on CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
		where a.[order_status_name]='OK') t
	group by cat_name_short, category_name, product_id_magento, product_family_name, cost_per_month) t
where rank_product = 1
order by cat_name_short, category_name, product_id_magento, product_family_name, num desc, cost_per_month


-----------------------------------------------------------
	-- Notes
		-- Per each Customer ranks their spend per category giving a rank number - X
		-- Calculates total number instances per category - y

		-- Calculates final rank pct depending on rank_per_customer and total_num_instances

	select count(*) over (),
		x.[customer_id], x.[target_order_date],
		cast(x.[EC] as decimal(10,2)) as [avg_unit_price_paid_EC], cast(x.[CL - Monthlies] as decimal(10,2)) as [avg_unit_price_paid_CLM], 
		cast(x.[CL - Daily] as decimal(10,2)) as [avg_unit_price_paid_CLD], cast(x.[CL - Two Weeklies] as decimal(10,2)) as [avg_unit_price_paid_CL2W], 
		cast(x.[SL] as decimal(10,2)) as [avg_unit_price_paid_SL], cast(x.[Other] as decimal(10,2)) as [avg_unit_price_paid_Oth],
		
		cast(rank_EC / max_EC as decimal(10,2)) as [rank_pct_aupp_EC], cast(rank_CLM / max_CLM as decimal(10,2)) as [rank_pct_aupp_CLM], 
		cast(rank_CLD / max_CLD as decimal(10,2)) as [rank_pct_aupp_CLD], cast(rank_CL2W / max_CL2W as decimal(10,2)) as [rank_pct_aupp_C2W],
		cast(rank_SL / max_SL as decimal(10,2)) as [rank_pct_aupp_SL], cast(rank_Oth / max_Oth as decimal(10,2)) as [rank_pct_aupp_Oth]

	from
	(select *,
		case when [CL - Monthlies] is not null then  cast(dense_rank() over (order by [CL - Monthlies]) as real) else null end as rank_CLM,
		case when [CL - Daily] is not null then cast(dense_rank() over (order by [CL - Daily]) as real) else null end as rank_CLD,
		case when [CL - Two Weeklies] is not null then cast(dense_rank() over (order by [CL - Two Weeklies]) as real) else null end as rank_CL2W,
		case when [SL] is not null then cast(dense_rank() over (order by [SL]) as real) else null end as rank_SL,
		case when [EC] is not null then cast(dense_rank() over (order by [EC]) as real) else null end as rank_EC,
		case when [Other] is not null then cast(dense_rank() over (order by [Other]) as real) else null end as rank_Oth
	from [dbo].[ol_model_base_trans]) as x,

	(select max(a.rank_CLM) as max_CLM ,max(a.rank_CLD) as max_CLD, max(a.rank_CL2W) as max_CL2W, 
		max(a.rank_SL) as max_SL, max(a.rank_EC) as max_EC, max(a.rank_Oth) as max_Oth
	from 
		(select *, 
			case when [CL - Monthlies] is not null then  cast(dense_rank() over (order by [CL - Monthlies]) as real) else null end as rank_CLM,
			case when [CL - Daily] is not null then cast(dense_rank() over (order by [CL - Daily]) as real) else null end as rank_CLD,
			case when [CL - Two Weeklies] is not null then cast(dense_rank() over (order by [CL - Two Weeklies]) as real) else null end as rank_CL2W,
			case when [SL] is not null then cast(dense_rank() over (order by [SL]) as real) else null end as rank_SL,
			case when [EC] is not null then cast(dense_rank() over (order by [EC]) as real) else null end as rank_EC,
			case when [Other] is not null then cast(dense_rank() over (order by [Other]) as real) else null end as rank_Oth
		from [dbo].[ol_model_base_trans]) as a) as y
	-- where customer_id in (1433563, 1480267)
	where x.[CL - Monthlies] is not null
	order by x.[CL - Monthlies]

	select count(distinct [CL - Monthlies])
	from [dbo].[ol_model_base_trans]