use VisionDirect
go

-- Recode the product categories into fewer more populated and meaningful values
	drop table [dbo].[product_spend_history];

	select a.[customer_id], max(a.last_order) as [last_order], a.cat_name_short,
		sum(cast(a.num_tot_orders1 as real)) as product_orders, sum(cast(a.subtotal_tot_orders as decimal(10,2))) as subtotal_orders,
		max(cast(a.num_tot_orders as real)) as num_tot_orders, max(cast(a.num_dist_products as real)) as num_dist_products
    into [dbo].[product_spend_history]
	from
		(SELECT [customer_id], 
			num_tot_orders_customer [num_tot_orders], [num_dist_products],
			[category_name], [product_id_magento], [product_family_name],
			first_order_date [first_order], last_order_date [last_order],
			num_tot_orders_product [num_tot_orders1], [subtotal_tot_orders],
			case 
				when category_name in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies') then category_name 
				when category_name = 'CL - Yearly' then 'CL - Monthlies'
				when left(category_name,2) = 'EC' then 'EC'
				when left(category_name,2) = 'SL' then 'SL'
				else 'Other' 
			end as cat_name_short
		FROM [VisionDirect].[dbo].[cust_prod_signature_obs]) as a
	group by a.customer_id, a.cat_name_short
	order by a.customer_id, a.cat_name_short;


	-- Tag on the last order date to the product data as this is used for joining back to the customer view and also to 
	-- calculate the time since a product category was purchased.
	drop table [dbo].[product_spend];
	
	select a.*,
		b.[last_order_date], b.[sum_product_orders], b.[sum_subtotal_orders],
		case when b.[sum_subtotal_orders] = 0 then NULL else a.[subtotal_orders]/b.[sum_subtotal_orders] end as [pct_subtotal_orders],
		case when b.[sum_product_orders] = 0 then NULL else a.[product_orders]/b.[sum_product_orders] end as [pct_product_orders],
		DATEDIFF(month, last_order, last_order_date) as months_since_order
    into [dbo].[product_spend]
	from 
			[dbo].[product_spend_history] as a
		inner join
			(-- Extract the maximum date for each customer_id this is the observation date that we need to join onto the customer table
			select [customer_id], max([last_order]) as [last_order_date], 
				sum([product_orders]) as [sum_product_orders], sum([subtotal_orders]) as [sum_subtotal_orders]
			from [dbo].[product_spend_history]
			group by [customer_id]) as b on a.[customer_id]=b.[customer_id]
	order by a.[customer_id], a.[cat_name_short];


	-- transpose the data for spend by category
	drop table [dbo].[product_orders_trans];

	SELECT *
	into [dbo].[product_orders_trans]
	FROM
			(SELECT [customer_id], [last_order_date], [cat_name_short], [product_orders]
			FROM [dbo].[product_spend]) AS SourceTable 
		PIVOT 
			(AVG([product_orders]) FOR [cat_name_short] IN 
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];

	-- transpose the data for percentage spend by category
	drop table [dbo].[product_orders_trans_pct];

	SELECT *
	into [dbo].[product_orders_trans_pct]
	FROM
			(SELECT [customer_id], [last_order_date], [cat_name_short], [pct_product_orders]
			FROM [dbo].[product_spend])  AS SourceTable 
		PIVOT
			(AVG([pct_product_orders]) FOR [cat_name_short] IN
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];

	-- transpose the data for spend by category
	drop table [dbo].[product_spend_trans];

	SELECT *
	into [dbo].[product_spend_trans]
	FROM
			(SELECT [customer_id], [last_order_date], [cat_name_short], [subtotal_orders]
			FROM [dbo].[product_spend]) AS SourceTable 
		PIVOT
			(AVG([subtotal_orders]) FOR [cat_name_short] IN
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];


	-- transpose the data for percentage spend by category
	drop table [dbo].[product_spend_trans_pct];

	SELECT *
	into [dbo].[product_spend_trans_pct]
	FROM
			(SELECT [customer_id], [last_order_date], [cat_name_short], [pct_subtotal_orders]
			FROM [dbo].[product_spend]) AS SourceTable 
		PIVOT
			(AVG([pct_subtotal_orders]) FOR [cat_name_short] IN
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];


-- Merge all of this data together to create a product view for the single customer view
drop table [dbo].[customer_prod_summary_1];

select a.[customer_id], a.[last_order_date],
	-- e.[sum_subtotal_orders] as [all_orders_value], e.[sum_product_orders] as [all_orders_product_count]
	case when a.[EC] is NULL then 0 else a.[EC] end as [spend_EC],
	case when a.[CL - Monthlies] is NULL then 0 else a.[CL - Monthlies] end as [spend_CL-Monthlies],
	case when a.[CL - Daily] is NULL then 0 else a.[CL - Daily] end as [spend_CL-Daily],
	case when a.[CL - Two Weeklies] is NULL then 0 else a.[CL - Two Weeklies] end as [spend_CL-Two Weeklies],
	case when a.[SL] is NULL then 0 else a.[SL] end as [spend_SL],
	case when a.[Other] is NULL then 0 else a.[Other] end as [spend_Other],

	case when b.[EC] is NULL then 0 else b.[EC] end as [spend_pct_EC],
	case when b.[CL - Monthlies] is NULL then 0 else b.[CL - Monthlies] end as [spend_pct_CL-Monthlies],
	case when b.[CL - Daily] is NULL then 0 else b.[CL - Daily] end as [spend_pct_CL-Daily],
	case when b.[CL - Two Weeklies] is NULL then 0 else b.[CL - Two Weeklies] end as [spend_pct_CL-Two Weeklies],
	case when b.[SL] is NULL then 0 else b.[SL] end as [spend_pct_SL],
	case when b.[Other] is NULL then 0 else b.[Other] end as [spend_pct_Other],

	case when c.[EC] is NULL then 0 else c.[EC] end as [orders_EC],
	case when c.[CL - Monthlies] is NULL then 0 else c.[CL - Monthlies] end as [orders_CL-Monthlies],
	case when c.[CL - Daily] is NULL then 0 else c.[CL - Daily] end as [orders_CL-Daily],
	case when c.[CL - Two Weeklies] is NULL then 0 else c.[CL - Two Weeklies] end as [orders_CL-Two Weeklies],
	case when c.[SL] is NULL then 0 else c.[SL] end as [orders_SL],
	case when c.[Other] is NULL then 0 else c.[Other] end as [orders_Other],

	case when d.[EC] is NULL then 0 else d.[EC] end as [orders_pct_EC],
	case when d.[CL - Monthlies] is NULL then 0 else d.[CL - Monthlies] end as [orders_pct_CL-Monthlies],
	case when d.[CL - Daily] is NULL then 0 else d.[CL - Daily] end as [orders_pct_CL-Daily],
	case when d.[CL - Two Weeklies] is NULL then 0 else d.[CL - Two Weeklies] end as [orders_pct_CL-Two Weeklies],
	case when d.[SL] is NULL then 0 else d.[SL] end as [orders_pct_SL],
	case when d.[Other] is NULL then 0 else d.[Other] end as [orders_pct_Other]
	
into [dbo].[customer_prod_summary_1]
from 
		[dbo].[product_spend_trans] as a
	inner join 
		[dbo].[product_spend_trans_pct] as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date]
	inner join 
		[dbo].[product_orders_trans] as c on a.[customer_id]=c.[customer_id] and a.[last_order_date]=c.[last_order_date]
	inner join 
		[dbo].[product_orders_trans_pct] as d on a.[customer_id]=d.[customer_id] and a.[last_order_date]=d.[last_order_date]
	--	inner join 
		-- (select * 
		-- from [dbo].[product_spend] 
		-- where [last_order_date]=[last_order]) as e on a.[customer_id]=e.[customer_id] and a.[last_order_date]=e.[last_order_date]
order by a.[customer_id];


-- Merge in the subtotals just so we have them (not that they couldn't be manually recalculated).
drop table [dbo].[customer_prod_summary];

select  a.*, 
	b.[sum_subtotal_orders] as [all_orders_value], b.[sum_product_orders] as [all_orders_product_count]
into [dbo].[customer_prod_summary]
from 
		[dbo].[customer_prod_summary_1] as a
	inner join 
		(select distinct [customer_id], [last_order_date], [sum_subtotal_orders], [sum_product_orders] 
		from [dbo].[product_spend]) as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date]
order by a.[customer_id];


--  Merge this onto the target data table.
drop table [dbo].[single_cust_view];
select  a.*,
	b.[all_orders_value] as [all_history_spend] ,b.[all_orders_product_count] as [all_history_product_count],
	
	b.[spend_SL] as [spend_SL], b.[spend_CL-Daily] as [spend_CLDaily], b.[spend_CL-Two Weeklies] as [spend_CLTwoWeeklies], b.[spend_CL-Monthlies] as [spend_CLMonthlies], 
	b.[spend_EC] as [spend_EC], b.[spend_Other] as [spend_Other], 
	
	b.[spend_pct_SL] as [spend_pct_SL], b.[spend_pct_CL-Daily] as [spend_pct_CLDaily], b.[spend_pct_CL-Two Weeklies] as [spend_pct_CLTwoWeeklies], b.[spend_pct_CL-Monthlies] as [spend_pct_CLMonthlies], 
	b.[spend_pct_EC] as [spend_pct_EC], b.[spend_pct_Other] as [spend_pct_Other], 
	
	b.[orders_SL] as [orders_SL], b.[orders_CL-Daily] as [orders_CLDaily], b.[orders_CL-Two Weeklies] as [orders_CLTwoWeeklies], b.[orders_CL-Monthlies] as [orders_CLMonthlies], 
	b.[orders_EC] as [orders_EC], b.[orders_Other] as [orders_Other], 
	
	b.[orders_pct_SL] as [orders_pct_SL], b.[orders_pct_CL-Daily] as [orders_pct_CLDaily], b.[orders_pct_CL-Two Weeklies] as [orders_pct_CLTwoWeeklies], b.[orders_pct_CL-Monthlies] as [orders_pct_CLMonthlies], 
	b.[orders_pct_EC] as [orders_pct_EC], b.[orders_pct_Other] as [orders_pct_Other]

into [dbo].[single_cust_view]
from 
		[dbo].[cust_signature_obs_wt] as a 
	inner join 
		[dbo].[customer_prod_summary] as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date]
order by a.[customer_id];