
select top 1000 customer_id, order_id, order_date, substring(order_date, 1, 10), convert(date, substring(order_date, 1, 10), 103),
	order_status_name, acquired_website_f, website, country_code_ship, 
	order_number, num_orders, sample_trans
from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date
-- where customer_id = 615
order by customer_id

	select count(*), count(distinct customer_id)
	from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date

	select order_status_name, count(*)
	from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date
	group by order_status_name
	order by order_status_name

	select acquired_website_f, website, count(*)
	from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date
	group by acquired_website_f, website
	order by acquired_website_f, website

	select order_number, count(*)
	from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date
	group by order_number
	order by order_number

	select num_orders, count(*)
	from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date
	group by num_orders
	order by num_orders

-----------------------------------------------------

select *
from Warehouse.sales.dim_order_header_v
where customer_id = 615 
order by order_date

-----------------------------------------------------

	drop table DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_c

	select c.idCustomer_sk idCustomer_sk_fk, t.customer_id, dateadd(day, 1, convert(date, substring(order_date, 1, 10), 103)) order_date
	into DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_c
	from 
			DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date t
		inner join
			Warehouse.gen.dim_customer_v c on t.customer_id = c.customer_id_bk
	-- where t.customer_id in (615, 1777)

	select *
	from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_c
	order by customer_id