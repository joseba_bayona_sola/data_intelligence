
		drop table #fact_customer_signature_aux_orders

		drop table #fact_customer_signature_aux_other

		drop table #fact_customer_signature_aux_sales

		drop table #fact_customer_signature_aux_sales_main_product
		drop table #fact_customer_signature_aux_sales_main_freq
		drop table #fact_customer_signature_aux_sales_main_ch
		drop table #fact_customer_signature_aux_sales_main_mch
		drop table #fact_customer_signature_aux_sales_main_pr
		drop table #fact_customer_signature_aux_sales_main

----------------------------------------------------

		select order_id_bk, idCustomer_sk_fk, 
			idStore_sk_fk, idCalendarActivityDate_sk_fk, activity_date,
			dense_rank() over (partition by idCustomer_sk_fk order by customer_order_seq_no) num_order, 
			count(*) over (partition by idCustomer_sk_fk, s.website_group) num_tot_orders_web, 
			count(*) over (partition by idCustomer_sk_fk) num_tot_orders, sum(global_subtotal) over (partition by idCustomer_sk_fk) subtotal_tot_orders, 
			count (case when (global_discount = 0) then null else 1 end) over (partition by idCustomer_sk_fk) num_tot_discount_orders,
			sum(global_discount) over (partition by idCustomer_sk_fk) discount_tot_value, 
			count (case when (global_store_credit_used = 0) then null else 1 end) over (partition by idCustomer_sk_fk) num_tot_store_credit_orders,
			sum(global_store_credit_used) over (partition by idCustomer_sk_fk) store_credit_tot_value, 

			idProductTypeOH_sk_fk, product_type_oh_name, order_qty_time, 
			idChannel_sk_fk, channel_name, idMarketingChannel_sk_fk, marketing_channel_name, idPriceType_sk_fk, price_type_name, 
			customer_status_name, customer_order_seq_no,
			global_discount, global_subtotal, 
			1 local_to_global_rate, 'GBP' order_currency_code
		into #fact_customer_signature_aux_orders
		from 
				Dw_GetLenses_jbs.dbo.fact_activity_sales_full_v acs
			inner join
				Warehouse.gen.dim_store_v s on acs.idStore_sk_fk = s.idStore_sk
		where activity_type_name = 'ORDER'	
			and activity_date < order_date

		
-------------------------------------------

		select aof.idCustomer_sk_fk, 
			crreg.idStoreCreate_sk_fk, crreg.idCalendarCreateDate_sk_fk, 
			crreg.idStoreRegister_sk_fk, crreg.idCalendarRegisterDate_sk_fk, al.idCalendarLastLogin_sk_fk idCalendarLastLoginDate_sk_fk, 
			crreg.idCustomerOrigin_sk_fk,
			null num_tot_lapsed, null num_tot_reactivate,
			null num_tot_emails, null num_tot_text_messages, null num_tot_reviews, isnull(al.num_tot_logins, 0) num_tot_logins,
			1 idCustomerStatusLifecycle_sk_fk, 1 idCalendarStatusUpdateDate_sk_fk 
		into #fact_customer_signature_aux_other
		from 
				(select distinct idCustomer_sk_fk, customer_id
				from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_c) aof
			inner join
				Warehouse.act.dim_customer_cr_reg crreg on aof.customer_id = crreg.customer_id_bk
			left join
				Warehouse.act.fact_activity_login al on aof.idCustomer_sk_fk = al.idCustomer_sk_fk

		merge into #fact_customer_signature_aux_other trg
		using 
			(select t.idCustomer_sk_fk, cs.idCustomerStatus_sk idCustomerStatusLifecycle_sk_fk, 
				CONVERT(INT, (CONVERT(VARCHAR(8), t.activity_date, 112))) idCalendarStatusUpdateDate_sk_fk
			from
					(select idCustomer_sk_fk, 
						case 
							when (activity_type_name = 'REGISTER') then 'RNB'
							when (activity_type_name = 'REACTIVATE') then 'REACTIVATED'
							else activity_type_name
						end activity_type_name, activity_date
					from
						(select idCustomer_sk_fk, t.activity_type_name, a.r_imp, activity_date, 
							count(*) over (partition by idCustomer_sk_fk) num_rep_activities,
							dense_rank() over (partition by idCustomer_sk_fk order by activity_date, a.r_imp) ord_activities, 
							count(*) over (partition by idCustomer_sk_fk, activity_date) num_rep_activities_2
						from 
								(select idCustomer_sk_fk, activity_type_name, 
									case when activity_type_name = 'REGISTER' then min(activity_date) over (partition by idCustomer_sk_fk) else activity_date end activity_date
								from 
										(select idCustomer_sk_fk, activity_type_name, activity_date
										from DW_GetLenses_jbs.dbo.fact_activity_other_full_v
										where activity_type_name in ('REGISTER', 'LAPSED')
											and activity_date < order_date
										union
										select idCustomer_sk_fk, customer_status_name activity_type_name, activity_date
										from #fact_customer_signature_aux_orders) t) t
							inner join
								(select 'REGISTER' activity_type_name, 1 r_imp
								union
								select 'NEW' activity_type_name, 2 r_imp
								union
								select 'LAPSED' activity_type_name, 3 r_imp
								union
								select 'REACTIVATED' activity_type_name, 4 r_imp
								union
								select 'REGULAR' activity_type_name, 5 r_imp) a on t.activity_type_name = a.activity_type_name
							) t
					where num_rep_activities = ord_activities) t
				inner join
					Warehouse.gen.dim_customer_status cs on t.activity_type_name = cs.customer_status_name_bk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
		when matched then
			update set 
				trg.idCustomerStatusLifecycle_sk_fk = src.idCustomerStatusLifecycle_sk_fk, 
				trg.idCalendarStatusUpdateDate_sk_fk = src.idCalendarStatusUpdateDate_sk_fk;


		-- Update num_tot_lapsed, num_tot_reactivate
		merge into #fact_customer_signature_aux_other trg
		using 
			(select idCustomer_sk_fk, count(*) num_tot_lapsed
			from DW_GetLenses_jbs.dbo.fact_activity_other_full_v
			where activity_type_name = 'LAPSED'
				and activity_date < order_date
			group by idCustomer_sk_fk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
			when matched then
				update set trg.num_tot_lapsed = src.num_tot_lapsed;

		merge into #fact_customer_signature_aux_other trg
		using 
			(select idCustomer_sk_fk, count(*) num_tot_reactivate
			from DW_GetLenses_jbs.dbo.fact_activity_other_full_v
			where activity_type_name = 'REACTIVATE'
				and activity_date < order_date
			group by idCustomer_sk_fk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
			when matched then
				update set trg.num_tot_reactivate = src.num_tot_reactivate;

----------------------------------------------------


		select
			case when (r.idCustomer_sk_fk is not null) then r.idCustomer_sk_fk else o_c.idCustomer_sk_fk end idCustomer_sk_fk,
			o_c.idStoreFirstOrder_sk_fk, o_c.idStoreLastOrder_sk_fk, 
			o_c.idCalendarFirstOrderDate_sk_fk, o_c.idCalendarLastOrderDate_sk_fk, 
			isnull(o_c.num_tot_orders_web, 0) num_tot_orders_web, isnull(o_c.num_tot_orders, 0) + isnull(r.num_tot_refund, 0) num_tot_orders_gen,
			isnull(o_c.num_tot_orders, 0) num_tot_orders, isnull(o_c.subtotal_tot_orders, 0) subtotal_tot_orders, 
			isnull(o_c.num_tot_cancel, 0) num_tot_cancel, isnull(o_c.subtotal_tot_cancel, 0) subtotal_tot_cancel, 
			isnull(r.num_tot_refund, 0) num_tot_refund, isnull(r.subtotal_tot_refund, 0) subtotal_tot_refund, 
			isnull(o_c.num_tot_discount_orders, 0) num_tot_discount_orders, isnull(o_c.discount_tot_value, 0) discount_tot_value, 
			isnull(o_c.num_tot_store_credit_orders, 0) num_tot_store_credit_orders, isnull(o_c.store_credit_tot_value, 0) store_credit_tot_value, 
			o_c.idChannelFirst_sk_fk, o_c.idMarketingChannelFirst_sk_fk, o_c.idPriceTypeFirst_sk_fk, 
			o_c.order_id_bk_first, o_c.order_id_bk_last,
			o_c.first_discount_amount, o_c.first_subtotal_amount, o_c.last_subtotal_amount, 
			o_c.local_to_global_rate, o_c.order_currency_code 
		into #fact_customer_signature_aux_sales
		from 
				(select 
					case when (c.idCustomer_sk_fk is not null) then c.idCustomer_sk_fk else o1.idCustomer_sk_fk end idCustomer_sk_fk,
					o1.idStoreFirstOrder_sk_fk, o2.idStoreLastOrder_sk_fk, 
					o1.idCalendarFirstOrderDate_sk_fk, o2.idCalendarLastOrderDate_sk_fk, 
					isnull(o1.num_tot_orders_web, 0) num_tot_orders_web,
					isnull(o1.num_tot_orders, 0) num_tot_orders, isnull(o1.subtotal_tot_orders, 0) subtotal_tot_orders, 
					isnull(c.num_tot_cancel, 0) num_tot_cancel, isnull(c.subtotal_tot_cancel, 0) subtotal_tot_cancel, 
					-- isnull(r.num_tot_refund, 0) num_tot_refund, isnull(r.subtotal_tot_refund, 0) subtotal_tot_refund, 
					isnull(o1.num_tot_discount_orders, 0) num_tot_discount_orders, isnull(o1.discount_tot_value, 0) discount_tot_value, 
					isnull(o1.num_tot_store_credit_orders, 0) num_tot_store_credit_orders, isnull(o1.store_credit_tot_value, 0) store_credit_tot_value, 
					o1.idChannelFirst_sk_fk, o1.idMarketingChannelFirst_sk_fk, o1.idPriceTypeFirst_sk_fk,
					o1.order_id_bk_first, o2.order_id_bk_last, 
					o1.first_discount_amount, o1.first_subtotal_amount, o2.last_subtotal_amount,
					o1.local_to_global_rate, o1.order_currency_code 
				from 
						(select idCustomer_sk_fk, 
							idStore_sk_fk idStoreFirstOrder_sk_fk, idCalendarActivityDate_sk_fk idCalendarFirstOrderDate_sk_fk, 
							global_discount first_discount_amount, global_subtotal first_subtotal_amount, 
							num_tot_orders_web, -- ?? Check correctly for the Customer store_id 
							num_tot_orders, subtotal_tot_orders, 
							num_tot_discount_orders, discount_tot_value, num_tot_store_credit_orders, store_credit_tot_value, 
							idChannel_sk_fk idChannelFirst_sk_fk, idMarketingChannel_sk_fk idMarketingChannelFirst_sk_fk, idPriceType_sk_fk idPriceTypeFirst_sk_fk, 
							order_id_bk order_id_bk_first, 
							local_to_global_rate, order_currency_code
						from #fact_customer_signature_aux_orders
						where num_order = 1) o1
					inner join
						(select idCustomer_sk_fk, 
							idStore_sk_fk idStoreLastOrder_sk_fk, idCalendarActivityDate_sk_fk idCalendarLastOrderDate_sk_fk, 
							global_subtotal last_subtotal_amount, 
							order_id_bk order_id_bk_last
						from #fact_customer_signature_aux_orders
						where num_order = num_tot_orders) o2 on o1.idCustomer_sk_fk = o2.idCustomer_sk_fk
					full join
						(select idCustomer_sk_fk, count(*) num_tot_cancel, sum(global_subtotal) subtotal_tot_cancel
						from DW_GetLenses_jbs.dbo.fact_activity_sales_full_v
						where activity_type_name = 'CANCEL'
							and activity_date < order_date
						group by idCustomer_sk_fk) c on o1.idCustomer_sk_fk = c.idCustomer_sk_fk) o_c
			full join
				(select idCustomer_sk_fk, count(*) num_tot_refund, sum(global_subtotal) subtotal_tot_refund
				from DW_GetLenses_jbs.dbo.fact_activity_sales_full_v
				where activity_type_name = 'REFUND'
					and activity_date < order_date
				group by idCustomer_sk_fk) r on o_c.idCustomer_sk_fk = r.idCustomer_sk_fk

	merge into #fact_customer_signature_aux_sales trg
	using
		(select acs.idCustomer_sk_fk, acs.idChannel_sk_fk, acs.idMarketingChannel_sk_fk, acs.idPriceType_sk_fk
		from 
				(select idCustomer_sk_fk
				from #fact_customer_signature_aux_sales
				where num_tot_orders = 0
					and num_tot_refund <> 0) cs
			inner join
				Warehouse.act.fact_activity_sales acs on cs.idCustomer_sk_fk = acs.idCustomer_sk_fk and acs.customer_order_seq_no_gen = 1 and acs.idActivityType_sk_fk = 9) src 
	on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
	when matched then 
		update set
			trg.idChannelFirst_sk_fk = src.idChannel_sk_fk, trg.idMarketingChannelFirst_sk_fk = src.idMarketingChannel_sk_fk, 
			trg.idPriceTypeFirst_sk_fk = src.idPriceType_sk_fk; 

----------------------------------------------------


		select so.idCustomer_sk_fk, so.idProductTypeOH_sk_fk idProductTypeOHMain_sk_fk, t.num_dist_product_type_oh num_diff_product_type_oh, 
			0 main_order_qty_time, 0 min_order_qty_time, 0 avg_order_qty_time, 0 max_order_qty_time, convert(decimal(12, 4), 0.1) stdev_order_qty_time
		into #fact_customer_signature_aux_sales_main_product
		from 
				#fact_customer_signature_aux_orders so
			inner join
				(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
				from #fact_customer_signature_aux_orders
				group by idCustomer_sk_fk, num_tot_orders
				having count(distinct idProductTypeOH_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
		group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh

	insert into #fact_customer_signature_aux_sales_main_product(idCustomer_sk_fk, idProductTypeOHMain_sk_fk, num_diff_product_type_oh, 
		main_order_qty_time, min_order_qty_time, avg_order_qty_time, max_order_qty_time, stdev_order_qty_time)

		select idCustomer_sk_fk, idProductTypeOH_sk_fk, num_dist_product_type_oh, 
			0 main_order_qty_time, 0 min_order_qty_time, 0 avg_order_qty_time, 0 max_order_qty_time, convert(decimal(12, 4), 0.1) stdev_order_qty_time
		from
			(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
				num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
				dense_rank() over (partition by idCustomer_sk_fk order by product_type_oh_name) num_row
			from
				(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, num_dist_product_type_oh,
					num_rep_product_type_oh, sum_rep_product_type_oh, max_rep_product_type_oh, 
					max(sum_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_sum_rep_product_type_oh
				from
					(select idCustomer_sk_fk, num_tot_orders, idProductTypeOH_sk_fk, product_type_oh_name, 
						num_dist_product_type_oh,
						num_rep_product_type_oh, sum_rep_product_type_oh, 
						max(num_rep_product_type_oh) over (partition by idCustomer_sk_fk) max_rep_product_type_oh
					from
						(select so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, 
							t.num_dist_product_type_oh,
							count(*) num_rep_product_type_oh, sum(so.global_subtotal) sum_rep_product_type_oh
						from 
								#fact_customer_signature_aux_orders so
							inner join
								(select idCustomer_sk_fk, num_tot_orders, count(distinct idProductTypeOH_sk_fk) num_dist_product_type_oh
								from #fact_customer_signature_aux_orders
								group by idCustomer_sk_fk, num_tot_orders
								having count(distinct idProductTypeOH_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
						group by so.idCustomer_sk_fk, so.num_tot_orders, so.idProductTypeOH_sk_fk, so.product_type_oh_name, t.num_dist_product_type_oh) t) t 
				where num_rep_product_type_oh = max_rep_product_type_oh) t
			where sum_rep_product_type_oh = max_sum_rep_product_type_oh) t
		where num_row = 1

		merge into #fact_customer_signature_aux_sales_main_product trg
		using
			(select t1.idCustomer_sk_fk, t2.order_qty_time main_order_qty_time, 
				t1.min_order_qty_time, t1.avg_order_qty_time, t1.max_order_qty_time, t1.stdev_order_qty_time
			from
					(select t.idCustomer_sk_fk, 
						min(o.order_qty_time) min_order_qty_time, avg(o.order_qty_time) avg_order_qty_time, max(o.order_qty_time) max_order_qty_time, 
						stdevp(o.order_qty_time) stdev_order_qty_time
					from 
							#fact_customer_signature_aux_sales_main_product t
						inner join
							#fact_customer_signature_aux_orders o on t.idCustomer_sk_fk = o.idCustomer_sk_fk and t.idProductTypeOHMain_sk_fk = o.idProductTypeOH_sk_fk
					group by t.idCustomer_sk_fk) t1
				inner join
					(select idCustomer_sk_fk, order_qty_time
					from
						(select idCustomer_sk_fk, order_qty_time, num_rep_order_qty_time, 
							max(order_qty_time) over (partition by idCustomer_sk_fk) max_order_qty_time
						from
							(select idCustomer_sk_fk, order_qty_time, num_rep_order_qty_time, 
								max(num_rep_order_qty_time) over (partition by idCustomer_sk_fk) max_num_rep_order_qty_time
							from
								(select t.idCustomer_sk_fk, o.order_qty_time, count(*) num_rep_order_qty_time
								from 
										#fact_customer_signature_aux_sales_main_product t
									inner join
										#fact_customer_signature_aux_orders o on t.idCustomer_sk_fk = o.idCustomer_sk_fk and t.idProductTypeOHMain_sk_fk = o.idProductTypeOH_sk_fk
								group by t.idCustomer_sk_fk, o.order_qty_time) t) t
						where num_rep_order_qty_time = max_num_rep_order_qty_time) t
					where order_qty_time = max_order_qty_time) t2 on t1.idCustomer_sk_fk = t2.idCustomer_sk_fk) src
		on trg.idCustomer_sk_fk = src.idCustomer_sk_fk
		when matched then 
			update set 
				trg.main_order_qty_time = src.main_order_qty_time, 
				trg.min_order_qty_time = src.min_order_qty_time, trg.avg_order_qty_time = src.avg_order_qty_time, trg.max_order_qty_time = src.max_order_qty_time, 
				trg.stdev_order_qty_time = src.stdev_order_qty_time; 



		select idCustomer_sk_fk, null main_order_freq_time, 
			min(diff_days) min_order_freq_time, avg(diff_days) avg_order_freq_time, max(diff_days) max_order_freq_time, stdevp(diff_days) stdev_order_freq_time 
		into #fact_customer_signature_aux_sales_main_freq
		from 
			(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
				idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
				diff_days, num_order 
			from
				(select order_id_bk, idCustomer_sk_fk, num_tot_orders, 
					idCalendarActivityDate_sk_fk, activity_date, next_activity_date, 
					datediff(day, activity_date, next_activity_date) diff_days,
					num_order
				from
					(select order_id_bk, idCustomer_sk_fk,
						idCalendarActivityDate_sk_fk, activity_date, 
						lead(activity_date) over (partition by idCustomer_sk_fk order by num_order) next_activity_date,
						num_tot_orders, num_order, 
						customer_order_seq_no
					from #fact_customer_signature_aux_orders) t) t
			where next_activity_date is not null) t
		group by idCustomer_sk_fk, num_tot_orders



		select idCustomer_sk_fk, idChannel_sk_fk
		into #fact_customer_signature_aux_sales_main_ch
		from
			(select idCustomer_sk_fk, idChannel_sk_fk, channel_name, 
				num_rep_channel, sum_rep_channel, max_rep_channel, 
				dense_rank() over (partition by idCustomer_sk_fk order by channel_name) num_row
			from
				(select idCustomer_sk_fk, idChannel_sk_fk, channel_name, 
					num_rep_channel, sum_rep_channel, max_rep_channel, 
					max(sum_rep_channel) over (partition by idCustomer_sk_fk) max_sum_rep_channel
				from
					(select idCustomer_sk_fk, idChannel_sk_fk, channel_name, 
						num_rep_channel, sum_rep_channel, 
						max(num_rep_channel) over (partition by idCustomer_sk_fk) max_rep_channel
					from
						(select so.idCustomer_sk_fk, so.idChannel_sk_fk, so.channel_name, 
							count(*) num_rep_channel, sum(so.global_subtotal) sum_rep_channel
						from 
								#fact_customer_signature_aux_orders so
							inner join
								(select idCustomer_sk_fk, num_tot_orders, count(distinct idChannel_sk_fk) num_dist_price_type
								from #fact_customer_signature_aux_orders
								group by idCustomer_sk_fk, num_tot_orders
								having count(distinct idChannel_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
						group by so.idCustomer_sk_fk, so.idChannel_sk_fk, so.channel_name) t) t 
				where num_rep_channel = max_rep_channel) t
			where sum_rep_channel = max_sum_rep_channel) t
		where num_row = 1	

		insert into #fact_customer_signature_aux_sales_main_ch
			select distinct so.idCustomer_sk_fk, so.idChannel_sk_fk
			from 
					#fact_customer_signature_aux_orders so
				inner join
					(select idCustomer_sk_fk, num_tot_orders, count(distinct idChannel_sk_fk) num_dist_channel
					from #fact_customer_signature_aux_orders
					group by idCustomer_sk_fk, num_tot_orders
					having count(distinct idChannel_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk


		select idCustomer_sk_fk, idMarketingChannel_sk_fk
		into #fact_customer_signature_aux_sales_main_mch
		from
			(select idCustomer_sk_fk, idMarketingChannel_sk_fk, marketing_channel_name, 
				num_rep_channel, sum_rep_channel, max_rep_channel, 
				dense_rank() over (partition by idCustomer_sk_fk order by marketing_channel_name) num_row
			from
				(select idCustomer_sk_fk, idMarketingChannel_sk_fk, marketing_channel_name, 
					num_rep_channel, sum_rep_channel, max_rep_channel, 
					max(sum_rep_channel) over (partition by idCustomer_sk_fk) max_sum_rep_channel
				from
					(select idCustomer_sk_fk, idMarketingChannel_sk_fk, marketing_channel_name, 
						num_rep_channel, sum_rep_channel, 
						max(num_rep_channel) over (partition by idCustomer_sk_fk) max_rep_channel
					from
						(select so.idCustomer_sk_fk, so.idMarketingChannel_sk_fk, so.marketing_channel_name, 
							count(*) num_rep_channel, sum(so.global_subtotal) sum_rep_channel
						from 
								#fact_customer_signature_aux_orders so
							inner join
								(select idCustomer_sk_fk, num_tot_orders, count(distinct idMarketingChannel_sk_fk) num_dist_price_type
								from #fact_customer_signature_aux_orders
								group by idCustomer_sk_fk, num_tot_orders
								having count(distinct idMarketingChannel_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
						group by so.idCustomer_sk_fk, so.idMarketingChannel_sk_fk, so.marketing_channel_name) t) t 
				where num_rep_channel = max_rep_channel) t
			where sum_rep_channel = max_sum_rep_channel) t
		where num_row = 1

		insert into #fact_customer_signature_aux_sales_main_mch
			select distinct so.idCustomer_sk_fk, so.idMarketingChannel_sk_fk
			from 
					#fact_customer_signature_aux_orders so
				inner join
					(select idCustomer_sk_fk, num_tot_orders, count(distinct idMarketingChannel_sk_fk) num_dist_marketing_channel
					from #fact_customer_signature_aux_orders
					group by idCustomer_sk_fk, num_tot_orders
					having count(distinct idMarketingChannel_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk



		select idCustomer_sk_fk, idPriceType_sk_fk
		into #fact_customer_signature_aux_sales_main_pr
		from
			(select idCustomer_sk_fk, idPriceType_sk_fk, price_type_name, 
				num_rep_price_type, sum_rep_price_type, max_rep_price_type, 
				dense_rank() over (partition by idCustomer_sk_fk order by price_type_name) num_row
			from
				(select idCustomer_sk_fk, idPriceType_sk_fk, price_type_name, 
					num_rep_price_type, sum_rep_price_type, max_rep_price_type, 
					max(sum_rep_price_type) over (partition by idCustomer_sk_fk) max_sum_rep_price_type
				from
					(select idCustomer_sk_fk, idPriceType_sk_fk, price_type_name, 
						num_rep_price_type, sum_rep_price_type, 
						max(num_rep_price_type) over (partition by idCustomer_sk_fk) max_rep_price_type
					from
						(select so.idCustomer_sk_fk, so.idPriceType_sk_fk, so.price_type_name, 
							count(*) num_rep_price_type, sum(so.global_subtotal) sum_rep_price_type
						from 
								#fact_customer_signature_aux_orders so
							inner join
								(select idCustomer_sk_fk, num_tot_orders, count(distinct idPriceType_sk_fk) num_dist_price_type
								from #fact_customer_signature_aux_orders
								group by idCustomer_sk_fk, num_tot_orders
								having count(distinct idPriceType_sk_fk) <> 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk
						group by so.idCustomer_sk_fk, so.idPriceType_sk_fk, so.price_type_name) t) t 
				where num_rep_price_type = max_rep_price_type) t
			where sum_rep_price_type = max_sum_rep_price_type) t
		where num_row = 1		

		insert into #fact_customer_signature_aux_sales_main_pr
			select distinct so.idCustomer_sk_fk, so.idPriceType_sk_fk
			from 
					#fact_customer_signature_aux_orders so
				inner join
					(select idCustomer_sk_fk, num_tot_orders, count(distinct idPriceType_sk_fk) num_dist_price_type
					from #fact_customer_signature_aux_orders
					group by idCustomer_sk_fk, num_tot_orders
					having count(distinct idPriceType_sk_fk) = 1) t on so.idCustomer_sk_fk = t.idCustomer_sk_fk


		select pro.idCustomer_sk_fk, 
			pro.idProductTypeOHMain_sk_fk, pro.num_diff_product_type_oh,
			pro.main_order_qty_time, pro.min_order_qty_time, pro.avg_order_qty_time, pro.max_order_qty_time, pro.stdev_order_qty_time, 
			freq.main_order_freq_time, 
			isnull(freq.min_order_freq_time, 0) min_order_freq_time, isnull(freq.avg_order_freq_time, 0) avg_order_freq_time, isnull(freq.max_order_freq_time, 0) max_order_freq_time, 
			isnull(freq.stdev_order_freq_time, 0) stdev_order_freq_time, 
			ch.idChannel_sk_fk idChannelMain_sk_fk, mch.idMarketingChannel_sk_fk idMarketingChannelMain_sk_fk, pr.idPriceType_sk_fk idPriceTypeMain_sk_fk
		into #fact_customer_signature_aux_sales_main
		from 
				#fact_customer_signature_aux_sales_main_product pro
			left join
				#fact_customer_signature_aux_sales_main_freq freq on pro.idCustomer_sk_fk = freq.idCustomer_sk_fk
			inner join
				#fact_customer_signature_aux_sales_main_ch ch on pro.idCustomer_sk_fk = ch.idCustomer_sk_fk
			inner join
				#fact_customer_signature_aux_sales_main_mch mch on pro.idCustomer_sk_fk = mch.idCustomer_sk_fk
			inner join
				#fact_customer_signature_aux_sales_main_pr pr on pro.idCustomer_sk_fk = pr.idCustomer_sk_fk

---------------------------------------------------------------------------------

	drop table DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_cust_sign

	select o.idCustomer_sk_fk, 
		o.idStoreCreate_sk_fk, o.idCalendarCreateDate_sk_fk, 
		o.idStoreRegister_sk_fk, o.idCalendarRegisterDate_sk_fk, 
		s.idStoreFirstOrder_sk_fk, s.idStoreLastOrder_sk_fk, 
		s.idCalendarFirstOrderDate_sk_fk, s.idCalendarLastOrderDate_sk_fk, o.idCalendarLastLoginDate_sk_fk, 
		
		o.idCustomerOrigin_sk_fk,

		isnull(s.num_tot_orders_web, 0) num_tot_orders_web, isnull(s.num_tot_orders_gen, 0) num_tot_orders_gen,
		isnull(s.num_tot_orders, 0) num_tot_orders, isnull(s.subtotal_tot_orders, 0) subtotal_tot_orders, 
		isnull(s.num_tot_cancel, 0) num_tot_cancel, isnull(s.subtotal_tot_cancel, 0) subtotal_tot_cancel, 
		isnull(s.num_tot_refund, 0) num_tot_refund, isnull(s.subtotal_tot_refund, 0) subtotal_tot_refund, 
		isnull(o.num_tot_lapsed, 0) num_tot_lapsed, isnull(o.num_tot_reactivate, 0) num_tot_reactivate,
		isnull(s.num_tot_discount_orders, 0) num_tot_discount_orders, isnull(s.discount_tot_value, 0) discount_tot_value, 
		isnull(s.num_tot_store_credit_orders, 0) num_tot_store_credit_orders, isnull(s.store_credit_tot_value, 0) store_credit_tot_value, 
		o.num_tot_emails, o.num_tot_text_messages, o.num_tot_reviews, o.num_tot_logins,

		o.idCustomerStatusLifecycle_sk_fk, o.idCalendarStatusUpdateDate_sk_fk, 

		sm.idProductTypeOHMain_sk_fk, sm.num_diff_product_type_oh,
		sm.main_order_qty_time, sm.min_order_qty_time, sm.avg_order_qty_time, sm.max_order_qty_time, sm.stdev_order_qty_time, 
		sm.main_order_freq_time, sm.min_order_freq_time, sm.avg_order_freq_time, sm.max_order_freq_time, sm.stdev_order_freq_time,
		sm.idChannelMain_sk_fk, sm.idMarketingChannelMain_sk_fk, sm.idPriceTypeMain_sk_fk, 
		s.idChannelFirst_sk_fk, s.idMarketingChannelFirst_sk_fk, s.idPriceTypeFirst_sk_fk, 

		s.order_id_bk_first, s.order_id_bk_last,
		s.first_discount_amount, s.first_subtotal_amount, s.last_subtotal_amount, 
		s.local_to_global_rate, s.order_currency_code
	into DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_cust_sign
	from 
			#fact_customer_signature_aux_other o
		left join
			#fact_customer_signature_aux_sales s on o.idCustomer_sk_fk = s.idCustomer_sk_fk
		left join
			#fact_customer_signature_aux_sales_main sm on o.idCustomer_sk_fk = sm.idCustomer_sk_fk