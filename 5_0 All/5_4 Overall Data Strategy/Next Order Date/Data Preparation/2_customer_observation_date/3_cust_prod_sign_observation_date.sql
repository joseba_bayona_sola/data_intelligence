
drop table #oh

drop table #ol

	select oh.order_id_bk, oh.order_date, oh.idCalendarOrderDate, oh.customer_id_bk
	into #oh
	from 
			(select distinct customer_id customer_id_bk, order_date
			from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_c) c
		inner join 
			Landing.aux.sales_dim_order_header_aud oh on c.customer_id_bk = oh.customer_id_bk and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
				and oh.order_date < c.order_date

	select ol.order_line_id_bk, ol.order_id_bk, oh.order_date, oh.customer_id_bk, 
		ol.product_id_bk, ol.local_subtotal, ol.local_to_global_rate, 
		count(*) over (partition by customer_id_bk, product_id_bk) num_lines,
		rank() over (partition by customer_id_bk, product_id_bk order by order_date, order_line_id_bk) ord_lines
	into #ol
	from 
			#oh oh
		inner join 
			Landing.aux.sales_fact_order_line_aud ol on oh.order_id_bk = ol.order_id_bk


	drop table DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_cust_prod_sign

	select c.idCustomer_sk idCustomer_sk_fk, p.idProductFamily_sk idProductFamily_sk_fk,
		t1.idCalendarFirstOrderDate idCalendarFirstOrderDate_sk_fk, t1.idCalendarLastOrderDate idCalendarLastOrderDate_sk_fk, 
		t2.order_id_bk_first, t3.order_id_bk_last, 
		t1.num_tot_orders, t1.subtotal_tot_orders, 
		0 num_dist_products, convert(decimal(12, 4), 0.1) product_percentage, convert(decimal(12, 4), 0.1) category_percentage
	into DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_cust_prod_sign
	from
			(select c.customer_id_bk, ol.product_id_bk, 
				min(oh.idCalendarOrderDate) idCalendarFirstOrderDate, max(oh.idCalendarOrderDate) idCalendarLastOrderDate, 
				count(distinct oh.order_id_bk) num_tot_orders, sum(ol.local_subtotal * ol.local_to_global_rate) subtotal_tot_orders
			from
					(select distinct customer_id customer_id_bk
					from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_c) c
				inner join
					#oh oh on c.customer_id_bk = oh.customer_id_bk -- and oh.order_status_name_bk in ('OK', 'PARTIAL REFUND')
				inner join
					#ol ol on oh.order_id_bk = ol.order_id_bk
			group by c.customer_id_bk, ol.product_id_bk) t1
		inner join
			(select customer_id_bk, product_id_bk, order_id_bk order_id_bk_first
			from #ol
			where ord_lines = 1) t2 on t1.customer_id_bk = t2.customer_id_bk and t1.product_id_bk = t2.product_id_bk
		inner join	
			(select customer_id_bk, product_id_bk, order_id_bk order_id_bk_last
			from #ol
			where ord_lines = num_lines) t3 on t1.customer_id_bk = t3.customer_id_bk and t1.product_id_bk = t3.product_id_bk
		inner join
			Warehouse.gen.dim_customer_v c on t1.customer_id_bk = c.customer_id_bk
		inner join
			Warehouse.prod.dim_product_family p on t1.product_id_bk = p.product_id_bk


	merge into DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_cust_prod_sign with (tablock) as trg
	using 
		(select cps.idCustomer_sk_fk, cps.idProductFamily_sk_fk,
			count(*) over (partition by cps.idCustomer_sk_fk) num_dist_products,
			case when (sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk) = 0) then 0 
				else convert(decimal(12, 4), cps.subtotal_tot_orders * 100 / sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk)) end product_percentage, 
			case when (sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk) = 0) then 0 
				else convert(decimal(12, 4), sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk, pf.category_name) * 100 / sum(cps.subtotal_tot_orders) over (partition by cps.idCustomer_sk_fk)) end category_percentage
		from 
				DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_cust_prod_sign cps 
			inner join
				Warehouse.prod.dim_product_family_v pf on cps.idProductFamily_sk_fk = pf.idProductFamily_sk) src
		on (trg.idCustomer_sk_fk = src.idCustomer_sk_fk and trg.idProductFamily_sk_fk = src.idProductFamily_sk_fk)
	when matched and not exists 
		(select 			
			isnull(trg.num_dist_products, 0), isnull(trg.product_percentage, 0), isnull(trg.category_percentage, 0) 
		intersect
		select 
			isnull(src.num_dist_products, 0), isnull(src.product_percentage, 0), isnull(src.category_percentage, 0))
		then 
			update set
				trg.num_dist_products = src.num_dist_products, trg.product_percentage = src.product_percentage, trg.category_percentage = src.category_percentage;

select *
from DW_GetLenses_jbs.dbo.next_order_date_customer_observation_date_cust_prod_sign