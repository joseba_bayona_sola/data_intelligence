
select cs.customer_id, cs.website_create, cs.create_date, cs.create_time_mm,
	cs.website_register, cs.market_name, cs2.country_code_ship, cs2.customer_unsubscribe_name, 
	cs.first_order_date, cs.last_order_date, 
	cs.customer_status_name, cs.rank_num_tot_orders, cs.num_tot_orders, cs.rank_num_tot_cancel, cs.num_tot_cancel, cs.rank_num_tot_refund, cs.num_tot_refund, cs.num_tot_discount_orders, 
	cs.main_type_oh_name, cs.num_diff_product_type_oh, cs3.num_dist_products,
	cs.main_order_qty_time, cs.rank_avg_order_qty_time, cs.avg_order_qty_time, cs.rank_stdev_order_qty_time, cs.stdev_order_qty_time,
	cs.rank_avg_order_freq_time, cs.avg_order_freq_time, cs.rank_stdev_order_freq_time, cs.stdev_order_freq_time
from 
		DW_GetLenses_jbs.dbo.fact_customer_signature_v cs
	inner join
		(select customer_id
		from DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
		where selected = 1) sc on cs.customer_id = sc.customer_id
	inner join
		Warehouse.act.dim_customer_signature_v cs2 on  cs.customer_id = cs2.customer_id
	inner join
		(select distinct customer_id, num_dist_products
		from DW_GetLenses_jbs.dbo.fact_customer_product_signature_v) cs3 on  cs.customer_id = cs3.customer_id
order by cs.customer_id

select cps.customer_id, cps.num_tot_orders_customer, cps.num_dist_products,
	cps.category_name, cps.product_id_magento, cps.product_family_name, 
	cps.first_order_date, cps.last_order_date, cps.num_tot_orders_product, cps.subtotal_tot_orders
from 
		DW_GetLenses_jbs.dbo.fact_customer_product_signature_v cps
	inner join
		(select customer_id
		from DW_GetLenses_jbs.dbo.next_order_date_sample_customers_def
		where selected = 1) sc on cps.customer_id = sc.customer_id
order by cps.customer_id, cps.category_name, cps.product_id_magento

