
create table DW_GetLenses_jbs.dbo.next_order_date_sample_customers(
	idCustomer_sk_fk		int, 
	customer_id				int)

	insert into DW_GetLenses_jbs.dbo.next_order_date_sample_customers(idCustomer_sk_fk, customer_id)
		select top 40 idCustomer_sk_fk, customer_id
			-- , website_register, last_order_date, customer_status_name, num_tot_orders, stdev_order_qty_time, avg_order_freq_time, num_diff_product_type_oh
		from Warehouse.act.fact_customer_signature_v
		where website_register = 'visiondirect.co.uk'
			and last_order_date > '2016-05-10'
			and customer_status_name = 'REGULAR'

	insert into DW_GetLenses_jbs.dbo.next_order_date_sample_customers(idCustomer_sk_fk, customer_id)
		select top 20 idCustomer_sk_fk, customer_id
			-- , website_register, last_order_date, customer_status_name, num_tot_orders, stdev_order_qty_time, avg_order_freq_time, num_diff_product_type_oh
		from Warehouse.act.fact_customer_signature_v
		where website_register = 'visiondirect.co.uk'
			and last_order_date > '2016-05-10'
			and customer_status_name = 'NEW'

	insert into DW_GetLenses_jbs.dbo.next_order_date_sample_customers(idCustomer_sk_fk, customer_id)
		select top 20 idCustomer_sk_fk, customer_id
			-- , website_register, last_order_date, customer_status_name, num_tot_orders, stdev_order_qty_time, avg_order_freq_time, num_diff_product_type_oh
		from Warehouse.act.fact_customer_signature_v
		where website_register = 'visiondirect.co.uk'
			and last_order_date > '2016-05-10'
			and customer_status_name = 'LAPSED'

	-----------------------------------------

	insert into DW_GetLenses_jbs.dbo.next_order_date_sample_customers(idCustomer_sk_fk, customer_id)
		select top 10 idCustomer_sk_fk, customer_id
			-- , website_register, last_order_date, customer_status_name, num_tot_orders, stdev_order_qty_time, avg_order_freq_time, num_diff_product_type_oh
		from Warehouse.act.fact_customer_signature_v
		where website_register <> 'visiondirect.co.uk'
			and last_order_date > '2016-05-10'
			and customer_status_name = 'REGULAR'

	insert into DW_GetLenses_jbs.dbo.next_order_date_sample_customers(idCustomer_sk_fk, customer_id)
		select top 5 idCustomer_sk_fk, customer_id
			-- , website_register, last_order_date, customer_status_name, num_tot_orders, stdev_order_qty_time, avg_order_freq_time, num_diff_product_type_oh
		from Warehouse.act.fact_customer_signature_v
		where website_register <> 'visiondirect.co.uk'
			and last_order_date > '2016-05-10'
			and customer_status_name = 'LAPSED'

	insert into DW_GetLenses_jbs.dbo.next_order_date_sample_customers(idCustomer_sk_fk, customer_id)
		select top 5 idCustomer_sk_fk, customer_id
			-- , website_register, last_order_date, customer_status_name, num_tot_orders, stdev_order_qty_time, avg_order_freq_time, num_diff_product_type_oh
		from Warehouse.act.fact_customer_signature_v
		where website_register <> 'visiondirect.co.uk'
			and last_order_date > '2016-05-10'
			and customer_status_name = 'NEW'

----------------------------------------

create table DW_GetLenses_jbs.dbo.next_order_date_sample_acs_next(
	order_id_bk				bigint, 
	activity_date			datetime,
	next_order_freq_time	integer)

insert into DW_GetLenses_jbs.dbo.next_order_date_sample_acs_next(order_id_bk, activity_date, next_order_freq_time)
	select acs.order_id_bk, acs.activity_date, acs.next_order_freq_time
	from 
			Warehouse.act.fact_activity_sales_v acs
		inner join
			DW_GetLenses_jbs.dbo.next_order_date_sample_customers sc on acs.customer_id = sc.customer_id
