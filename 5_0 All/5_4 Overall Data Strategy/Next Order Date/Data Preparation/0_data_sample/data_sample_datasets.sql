
select *
from DW_GetLenses_jbs.dbo.next_order_date_sample_customers

select top 1000 *
from Warehouse.sales.dim_order_header_v

select top 1000 *
from Warehouse.sales.fact_order_line_v

select top 1000 *
from Warehouse.sales.fact_customer_signature_v

select top 1000 *
from Warehouse.sales.fact_customer_product_signature_v


---------------------------------------------


select oh.order_id_bk, oh.website, oh.order_status_name, oh.order_date, ac.next_order_freq_time, oh.order_qty_time, 
	oh.customer_id, oh.country_code_ship, oh.customer_status_name order_lifecycle_name, oh.customer_order_seq_no, 
	oh.shipping_days, oh.marketing_channel_name, oh.customer_unsubscribe_name, oh.price_type_name, oh.discount_f, 
	oh.payment_method_name, oh.shipping_method_name, oh.reorder_f, oh.reminder_type_name, oh.reminder_period_name, oh.reminder_date,
	oh.local_subtotal, oh.local_shipping, oh.local_discount, oh.local_store_credit_used, oh.local_total_inc_vat, oh.num_lines
from 
		Warehouse.sales.dim_order_header_v oh
	inner join
		DW_GetLenses_jbs.dbo.next_order_date_sample_customers sc on oh.customer_id = sc.customer_id
	inner join
		DW_GetLenses_jbs.dbo.next_order_date_sample_acs_next ac on oh.order_id_bk = ac.order_id_bk
order by oh.customer_id, oh.order_status_name, oh.order_date

select ol.order_line_id_bk, ol.order_id_bk, ol.order_status_name, ol.order_date, ol.customer_id,
	ol.product_type_name, ol.category_name, ol.cl_type_name, ol.cl_feature_name, ol.product_id_magento, ol.product_family_name, 
	ol.base_curve, ol.diameter, ol.power, ol.cylinder, ol.axis, ol.addition, ol.dominance, ol.colour, ol.eye, 
	ol.qty_unit, ol.qty_pack, ol.qty_time, ol.local_price_unit, ol.local_price_pack, ol.local_price_pack_discount, 
	ol.local_subtotal, ol.local_shipping, ol.local_discount, ol.local_store_credit_used, ol.local_total_inc_vat
from 
		Warehouse.sales.fact_order_line_v ol
	inner join
		DW_GetLenses_jbs.dbo.next_order_date_sample_customers sc on ol.customer_id = sc.customer_id
order by ol.customer_id, ol.order_status_name, ol.order_date

select cs.customer_id, cs.website_create, cs.create_date, cs.create_time_mm,
	cs.website_register, cs.market_name, cs2.country_code_ship, cs2.customer_unsubscribe_name, 
	cs.first_order_date, cs.last_order_date, 
	cs.customer_status_name, cs.num_tot_orders, cs.num_tot_cancel, cs.num_tot_refund, cs.num_tot_discount_orders, 
	cs.main_type_oh_name, cs.num_diff_product_type_oh, cs2.num_dist_products,
	cs.main_order_qty_time, cs.avg_order_qty_time, cs.stdev_order_qty_time,
	cs.avg_order_freq_time, cs.stdev_order_freq_time
from 
		Warehouse.act.fact_customer_signature_v cs
	inner join
		DW_GetLenses_jbs.dbo.next_order_date_sample_customers sc on cs.customer_id = sc.customer_id
	inner join
		Warehouse.act.dim_customer_signature_v cs2 on  cs.customer_id = cs2.customer_id
order by cs.customer_id

select cps.customer_id, cps.num_tot_orders_customer, cps.num_dist_products,
	cps.category_name, cps.product_id_magento, cps.product_family_name, 
	cps.idCalendarFirstOrderDate_sk_fk first_order, cps.idCalendarLastOrderDate_sk_fk last_order, cps.num_tot_orders, cps.subtotal_tot_orders
from 
		Warehouse.act.fact_customer_product_signature_v cps
	inner join
		DW_GetLenses_jbs.dbo.next_order_date_sample_customers sc on cps.customer_id = sc.customer_id
order by cps.customer_id, cps.category_name, cps.product_id_magento

