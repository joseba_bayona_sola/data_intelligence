
select variable
from VisionDirect.dbo.variables
order by variable

select variable, charindex('__', variable, 1), 
	case when (charindex('__', variable, 1) <> 0) then substring(variable, 0, charindex('__', variable, 1)) else variable end variable_mod,
	correlation_coefficient
from VisionDirect.dbo.variables_logistic
order by variable

select variable, correlation_coefficient
from VisionDirect.dbo.variables_linear
order by variable

select vlo.variable, case when (charindex('__', vlo.variable, 1) <> 0) then substring(vlo.variable, 0, charindex('__', vlo.variable, 1)) else vlo.variable end variable_mod,
	vlo.correlation_coefficient correlation_coefficient_lo, vli.correlation_coefficient correlation_coefficient_li
from 
		VisionDirect.dbo.variables_logistic vlo
	inner join
		VisionDirect.dbo.variables_linear vli on vlo.variable = vli.variable

---------------------------------------------------

select *
from 
		VisionDirect.dbo.variables v1
	left join
		VisionDirect.dbo.variables_logistic v2 on v2.variable like v1.variable + '%'

select v1.variable, v2.variable, v2.correlation_coefficient, 
	count(*) over (partition by v2.variable) num_rep
from 
		VisionDirect.dbo.variables v1
	full join
		(select variable, 
			case when (charindex('__', variable, 1) <> 0) then substring(variable, 0, charindex('__', variable, 1)) else variable end variable_mod,
			correlation_coefficient
		from VisionDirect.dbo.variables_logistic) v2 on v1.variable = v2.variable_mod
order by v1.variable, v2.variable

select v1.variable, v2.variable, isnull(v2.variable, v1.variable) variable_def,
	v2.correlation_coefficient_lo, v2.correlation_coefficient_li
from 
		VisionDirect.dbo.variables v1
	full join
		(select vlo.variable, case when (charindex('__', vlo.variable, 1) <> 0) then substring(vlo.variable, 0, charindex('__', vlo.variable, 1)) else vlo.variable end variable_mod,
			vlo.correlation_coefficient correlation_coefficient_lo, vli.correlation_coefficient correlation_coefficient_li
		from 
				VisionDirect.dbo.variables_logistic vlo
			inner join
				VisionDirect.dbo.variables_linear vli on vlo.variable = vli.variable) v2 on v1.variable = v2.variable_mod
-- where v2.variable is null
order by variable_def, v1.variable, v2.variable


