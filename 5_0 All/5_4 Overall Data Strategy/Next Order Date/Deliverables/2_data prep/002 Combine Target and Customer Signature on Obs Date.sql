/****** Script for SelectTopNRows command from SSMS  ******/
drop table [dbo].[cust_signature_obs_wt];

SELECT a.[customer_id],
	a.[website_create], a.[create_date], a.[create_time_mm], a.[website_register], 
	a.[market_name], a.[country_code_ship],
	a.[customer_unsubscribe_name],
	a.[first_order_date], a.[last_order_date], 
	a.[customer_status_name], a.[rank_num_tot_orders], a.[num_tot_orders],
	a.[rank_num_tot_cancel], a.[num_tot_cancel], a.[rank_num_tot_refund], a.[num_tot_refund], a.[num_tot_discount_orders],
	a.[main_type_oh_name], a.[num_diff_product_type_oh],
	a.[num_dist_products],
	a.[main_order_qty_time], a.[rank_avg_order_qty_time], a.[avg_order_qty_time], a.[rank_stdev_order_qty_time], a.[stdev_order_qty_time],
	a.[rank_avg_order_freq_time], a.[avg_order_freq_time], a.[rank_stdev_order_freq_time], a.[stdev_order_freq_time],
	
	cast(datediff(month,a.[create_date], b.[target_order_date]) as real) / 12 as length_of_relationship, b.[target_order_date], b.[target_order_id],
	-- b.[gb_rownum], b.[last_rec]
    b.[target_ndtno_orig], b.[target_ndtno], b.[target_renewal]

into [dbo].[cust_signature_obs_wt]
FROM 
		[dbo].[cust_signature_obs] as a 
	inner join 
		[dbo].[cust_obs_date_targetset] as b on a.[customer_id]=b.[customer_id] and CONVERT (DATE, a.[last_order_date]) = CONVERT (DATE, b.[target_order_date]) 
order by a.[customer_id]