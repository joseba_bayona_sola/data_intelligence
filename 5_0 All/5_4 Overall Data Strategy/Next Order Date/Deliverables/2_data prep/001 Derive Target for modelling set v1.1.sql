/****** Select the target record, i.e. the next row value for each customer record  ******/
-- This is likely to have customers with multiple orders on the same day.  Need to develop a strategy to remove
DROP table [dbo].[cust_obs_date_targetset];
select z.*
		,DATEDIFF(day, z.target_order_date, z.order_date) as target_ndtno_orig
		-- If there is no subsequent purchase then set the days to next order to be 15 months
		-- if the days to next order is greater than 15 months then clip it to 15 months.
		,case when DATEDIFF(day, z.target_order_date, z.order_date) = 0 then 460
			  when DATEDIFF(day, z.target_order_date, z.order_date) > 460 then 460
			  else DATEDIFF(day, z.target_order_date, z.order_date) end as target_ndtno
		,case when DATEDIFF(day, z.target_order_date, z.order_date) = 0 then 0 
			  when DATEDIFF(month, z.target_order_date, z.order_date) <= 15 then 1 
			  else 0 end as [target_renewal]
into [dbo].[cust_obs_date_targetset]
from
(
	select c.*
			,ROW_NUMBER() over (partition by c.[customer_id] order by c.[order_date] desc) as last_rec
	from
	(
		SELECT a.[order_id]
				,a.[order_status_name]
				,a.[customer_id]
				,a.[order_date]
				,a.next_order_freq_time
				,a.[reminder_date]
				,b.[order_date] as [target_order_date]
				,b.[order_id] as [target_order_id]
				,ROW_NUMBER() over (partition by a.[customer_id] order by a.[order_date]) as gb_rownum
		  FROM [dbo].[oh] as a
		  left join [dbo].[cust_obs_date] as b
		  on a.[customer_id]=b.[customer_id]
		  where a.[order_date] >= b.[order_date] 
	) as c
	where c.gb_rownum <= 2
) as z
where z.[last_rec] = 1
order by z.[customer_id], z.[order_date]
;



/****** Select the target record, i.e. the next row value for each customer record  ******/
-- This is likely to have customers with multiple orders on the same day.  Need to develop a strategy to remove
DROP table [dbo].[cust_obs_date_targetset];

select z.*,
	DATEDIFF(day, z.target_order_date, z.order_date) as target_ndtno_orig,
	
	-- If there is no subsequent purchase then set the days to next order to be 15 months
	-- if the days to next order is greater than 15 months then clip it to 15 months.
		case 
			when DATEDIFF(day, z.target_order_date, z.order_date) = 0 then 460
			when DATEDIFF(day, z.target_order_date, z.order_date) > 460 then 460
			else DATEDIFF(day, z.target_order_date, z.order_date) 
		end as target_ndtno, 

		case 
			when DATEDIFF(day, z.target_order_date, z.order_date) = 0 then 0 
			when DATEDIFF(month, z.target_order_date, z.order_date) <= 15 then 1 
			else 0 
		end as [target_renewal]

into [dbo].[cust_obs_date_targetset]
from
(
	select c.*,
		ROW_NUMBER() over (partition by c.[customer_id] order by c.[order_date] desc) as last_rec
	from
		(SELECT a.[order_id], a.[order_status_name], a.[customer_id], a.[order_date],
			a.next_order_freq_time, 
			a.[reminder_date],
			
			b.[order_date] as [target_order_date], b.[order_id] as [target_order_id], 
			ROW_NUMBER() over (partition by a.[customer_id] order by a.[order_date]) as gb_rownum
		FROM 
				[dbo].[oh] as a
			left join 
				[dbo].[cust_obs_date] as b on a.[customer_id] = b.[customer_id] where a.[order_date] >= b.[order_date]) as c
	where c.gb_rownum <= 2) as z
where z.[last_rec] = 1
order by z.[customer_id], z.[order_date];


