
-- Recode the product categories into fewer more populated and meaningful values
	drop table [dbo].[product_spend_history];

	select a.[customer_id], max(a.last_order) as [last_order], a.cat_name_short,
		sum(cast(a.num_tot_orders1 as real)) as product_orders, sum(cast(a.subtotal_tot_orders as decimal(10,2))) as subtotal_orders,
		max(cast(a.num_tot_orders as real)) as num_tot_orders, max(cast(a.num_dist_products as real)) as num_dist_products
    into [dbo].[product_spend_history]
	from
		(SELECT [customer_id], 
			[num_tot_orders], [num_dist_products],
			[category_name], [product_id_magento], [product_family_name],
			[first_order], [last_order],
			[num_tot_orders1], [subtotal_tot_orders],
			case 
				when category_name in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies') then category_name 
				when category_name = 'CL - Yearly' then 'CL - Monthlies'
				when left(category_name,2) = 'EC' then 'EC'
				when left(category_name,2) = 'SL' then 'SL'
				else 'Other' 
			end as cat_name_short
		FROM [VisionDirect].[dbo].[cust_prod_signature_obs]) as a
	group by a.customer_id, a.cat_name_short
	order by a.customer_id, a.cat_name_short;


	-- Tag on the last order date to the product data as this is used for joining back to the customer view and also to 
	-- calculate the time since a product category was purchased.
	drop table [dbo].[product_spend];
	
	select a.*,
		b.[last_order_date], b.[sum_product_orders], b.[sum_subtotal_orders],
		case when b.[sum_subtotal_orders] = 0 then NULL else a.[subtotal_orders]/b.[sum_subtotal_orders] end as [pct_subtotal_orders],
		case when b.[sum_product_orders] = 0 then NULL else a.[product_orders]/b.[sum_product_orders] end as [pct_product_orders],
		DATEDIFF(month, last_order, last_order_date) as months_since_order
    into [dbo].[product_spend]
	from 
			[dbo].[product_spend_history] as a
		inner join
			(-- Extract the maximum date for each customer_id this is the observation date that we need to join onto the customer table
			select [customer_id], max([last_order]) as [last_order_date], 
				sum([product_orders]) as [sum_product_orders], sum([subtotal_orders]) as [sum_subtotal_orders]
			from [dbo].[product_spend_history]
			group by [customer_id]) as b on a.[customer_id]=b.[customer_id]
	order by a.[customer_id], a.[cat_name_short];


	-- transpose the data for spend by category
	drop table [dbo].[product_orders_trans];

	SELECT *
	into [dbo].[product_orders_trans]
	FROM
			(SELECT [customer_id], [last_order_date], [cat_name_short], [product_orders]
			FROM [dbo].[product_spend]) AS SourceTable 
		PIVOT 
			(AVG([product_orders]) FOR [cat_name_short] IN 
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];

	-- transpose the data for percentage spend by category
	drop table [dbo].[product_orders_trans_pct];

	SELECT *
	into [dbo].[product_orders_trans_pct]
	FROM
			(SELECT [customer_id], [last_order_date], [cat_name_short], [pct_product_orders]
			FROM [dbo].[product_spend])  AS SourceTable 
		PIVOT
			(AVG([pct_product_orders]) FOR [cat_name_short] IN
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];

	-- transpose the data for spend by category
	drop table [dbo].[product_spend_trans];

	SELECT *
	into [dbo].[product_spend_trans]
	FROM
			(SELECT [customer_id], [last_order_date], [cat_name_short], [subtotal_orders]
			FROM [dbo].[product_spend]) AS SourceTable 
		PIVOT
			(AVG([subtotal_orders]) FOR [cat_name_short] IN
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];


	-- transpose the data for percentage spend by category
	drop table [dbo].[product_spend_trans_pct];

	SELECT *
	into [dbo].[product_spend_trans_pct]
	FROM
			(SELECT [customer_id], [last_order_date], [cat_name_short], [pct_subtotal_orders]
			FROM [dbo].[product_spend]) AS SourceTable 
		PIVOT
			(AVG([pct_subtotal_orders]) FOR [cat_name_short] IN
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];


-- Merge all of this data together to create a product view for the single customer view
drop table [dbo].[customer_prod_summary_1];

select a.[customer_id], a.[last_order_date],
	-- e.[sum_subtotal_orders] as [all_orders_value], e.[sum_product_orders] as [all_orders_product_count]
	case when a.[EC] is NULL then 0 else a.[EC] end as [spend_EC],
	case when a.[CL - Monthlies] is NULL then 0 else a.[CL - Monthlies] end as [spend_CL-Monthlies],
	case when a.[CL - Daily] is NULL then 0 else a.[CL - Daily] end as [spend_CL-Daily],
	case when a.[CL - Two Weeklies] is NULL then 0 else a.[CL - Two Weeklies] end as [spend_CL-Two Weeklies],
	case when a.[SL] is NULL then 0 else a.[SL] end as [spend_SL],
	case when a.[Other] is NULL then 0 else a.[Other] end as [spend_Other],

	case when b.[EC] is NULL then 0 else b.[EC] end as [spend_pct_EC],
	case when b.[CL - Monthlies] is NULL then 0 else b.[CL - Monthlies] end as [spend_pct_CL-Monthlies],
	case when b.[CL - Daily] is NULL then 0 else b.[CL - Daily] end as [spend_pct_CL-Daily],
	case when b.[CL - Two Weeklies] is NULL then 0 else b.[CL - Two Weeklies] end as [spend_pct_CL-Two Weeklies],
	case when b.[SL] is NULL then 0 else b.[SL] end as [spend_pct_SL],
	case when b.[Other] is NULL then 0 else b.[Other] end as [spend_pct_Other],

	case when c.[EC] is NULL then 0 else c.[EC] end as [orders_EC],
	case when c.[CL - Monthlies] is NULL then 0 else c.[CL - Monthlies] end as [orders_CL-Monthlies],
	case when c.[CL - Daily] is NULL then 0 else c.[CL - Daily] end as [orders_CL-Daily],
	case when c.[CL - Two Weeklies] is NULL then 0 else c.[CL - Two Weeklies] end as [orders_CL-Two Weeklies],
	case when c.[SL] is NULL then 0 else c.[SL] end as [orders_SL],
	case when c.[Other] is NULL then 0 else c.[Other] end as [orders_Other],

	case when d.[EC] is NULL then 0 else d.[EC] end as [orders_pct_EC],
	case when d.[CL - Monthlies] is NULL then 0 else d.[CL - Monthlies] end as [orders_pct_CL-Monthlies],
	case when d.[CL - Daily] is NULL then 0 else d.[CL - Daily] end as [orders_pct_CL-Daily],
	case when d.[CL - Two Weeklies] is NULL then 0 else d.[CL - Two Weeklies] end as [orders_pct_CL-Two Weeklies],
	case when d.[SL] is NULL then 0 else d.[SL] end as [orders_pct_SL],
	case when d.[Other] is NULL then 0 else d.[Other] end as [orders_pct_Other],
	
into [dbo].[customer_prod_summary_1]
from 
		[dbo].[product_spend_trans] as a
	inner join 
		[dbo].[product_spend_trans_pct] as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date]
	inner join 
		[dbo].[product_orders_trans] as c on a.[customer_id]=c.[customer_id] and a.[last_order_date]=c.[last_order_date]
	inner join 
		[dbo].[product_orders_trans_pct] as d on a.[customer_id]=d.[customer_id] and a.[last_order_date]=d.[last_order_date]
	--	inner join 
		-- (select * 
		-- from [dbo].[product_spend] 
		-- where [last_order_date]=[last_order]) as e on a.[customer_id]=e.[customer_id] and a.[last_order_date]=e.[last_order_date]
order by a.[customer_id];


-- Merge in the subtotals just so we have them (not that they couldn't be manually recalculated).
drop table [dbo].[customer_prod_summary];

select  a.*, 
	b.[sum_subtotal_orders] as [all_orders_value], b.[sum_product_orders] as [all_orders_product_count]
into [dbo].[customer_prod_summary]
from 
		[dbo].[customer_prod_summary_1] as a
	inner join 
		(select distinct [customer_id], [last_order_date], [sum_subtotal_orders], [sum_product_orders] 
		from [dbo].[product_spend]) as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date]
order by a.[customer_id];


--  Merge this onto the target data table.
drop table [dbo].[single_cust_view];
select  a.*,
	b.[all_orders_value] as [all_history_spend] ,b.[all_orders_product_count] as [all_history_product_count],
	
	b.[spend_SL] as [spend_SL], b.[spend_CL-Daily] as [spend_CLDaily], b.[spend_CL-Two Weeklies] as [spend_CLTwoWeeklies], b.[spend_CL-Monthlies] as [spend_CLMonthlies], 
	b.[spend_EC] as [spend_EC], b.[spend_Other] as [spend_Other], 
	
	b.[spend_pct_SL] as [spend_pct_SL], b.[spend_pct_CL-Daily] as [spend_pct_CLDaily], b.[spend_pct_CL-Two Weeklies] as [spend_pct_CLTwoWeeklies], b.[spend_pct_CL-Monthlies] as [spend_pct_CLMonthlies], 
	b.[spend_pct_EC] as [spend_pct_EC], b.[spend_pct_Other] as [spend_pct_Other], 
	
	b.[orders_SL] as [orders_SL], b.[orders_CL-Daily] as [orders_CLDaily], b.[orders_CL-Two Weeklies] as [orders_CLTwoWeeklies], b.[orders_CL-Monthlies] as [orders_CLMonthlies], 
	b.[orders_EC] as [orders_EC], b.[orders_Other] as [orders_Other], 
	
	b.[orders_pct_SL] as [orders_pct_SL], b.[orders_pct_CL-Daily] as [orders_pct_CLDaily], b.[orders_pct_CL-Two Weeklies] as [orders_pct_CLTwoWeeklies], b.[orders_pct_CL-Monthlies] as [orders_pct_CLMonthlies], 
	b.[orders_pct_EC] as [orders_pct_EC], b.[orders_pct_Other] as [orders_pct_Other]

into [dbo].[single_cust_view]
from 
		[dbo].[cust_signature_obs_wt] as a 
	inner join 
		[dbo].[customer_prod_summary] as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date]
order by a.[customer_id];


---------------------------------------------------------------------------------------------

-- Commence data preparation for marketing channel, payment method, discount and reorder flags and shipping days
-- Create an intermediate table with the additional bits of data in the order line table that haven't made into the signature tables
drop table [dbo].[oh_addnl_cols];

select b.[customer_id], b.[last_order_date],
	case	
		when left(b.marketing_channel_name,5)='Email' then 'Email' 
		when b.marketing_channel_name in ('Refer a Friend', 'Affiliates', 'Referral', 'Redirect', 'Super Affiliates') then 'Referral' 
		when left(b.marketing_channel_name,11)='Paid Search' or b.marketing_channel_name='Display' then 'PaidSearch' 
		when b.marketing_channel_name in ('SMS Url', 'Unknown', 'Mutuelles Auto', 'Social Media') then 'Other'
		else b.marketing_channel_name 
	end as  marketing_channel_name,
	b.[discount_F] ,b.[reorder_F],
	case when b.[shipping_days] <> 'NULL' then cast(b.[shipping_days] as real) else NULL end as [shipping_days],
	case	
		when b.[payment_method_name]='N/A' then 'Unknown'
		when b.[payment_method_name]='Card - New' then 'Card-New'
		when b.[payment_method_name]='Card - Stored' then 'Card-Stored'
		when b.[payment_method_name]='PayPal' then 'PayPal'
		else 'Other' end as [payment_method_name]
into [dbo].[oh_addnl_cols]
from 
	(select a.[customer_id], a.[order_date],
		a.[marketing_channel_name],
		case when upper(a.[discount_f])='Y' then 1 else 0 end as [discount_F], case when upper(a.[reorder_f])='Y' then 1 else 0 end as [reorder_F],
		a.[shipping_days], a.[payment_method_name], 
		c.[last_order_date]
	from 
			[dbo].[oh] as a
		inner join 
			[dbo].[single_cust_view] as c on a.[customer_id]=c.[customer_id] 
	where  CONVERT (DATE, a.[order_date])<= CONVERT (DATE, c.[last_order_date])) as b;


-- Create an intermediate table for the summary fields on shipping - avg, discount - count and %pct and finally re-order flags.
drop table [dbo].[oh_small_summary];

select [customer_id], max([last_order_date]) as last_order_date, 
	sum([discount_F]) as [num_discounted_orders], sum([reorder_F]) as [num_reorder_orders], 
	avg([shipping_days]) as [avg_days_to_shipping], count(*) as [num_instances]
into [dbo].[oh_small_summary]
from [dbo].[oh_addnl_cols]
group by [customer_id]
order by [customer_id];

-------------------------------------




-- Do the percentage counts and transposition for the payment_method.

	-- Create a table of records of counts of sales channel per customer for the obs date
	drop table [dbo].[payment_method_name];

	select a.customer_id, max(a.last_order_date) as last_order_date ,a.[payment_method_name], count(a.[payment_method_name]) as numinst
	into [dbo].[payment_method_name]
	from [dbo].[oh_addnl_cols] as a
	group by a.customer_id, a.[payment_method_name]
	order by a.customer_id;

	-- Combine the total data onto the marketing channel data
	drop table [dbo].[payment_method_name_1];
	
	select a.* ,b.total_instances, cast(a.numinst as real) /cast(b.total_instances as real) as pct_instance
	into [dbo].[payment_method_name_1]
	from 
			[dbo].[payment_method_name] as a 
		inner join
			(select customer_id, [last_order_date], sum(numinst) as total_instances
			from [dbo].[payment_method_name]
			group by customer_id, last_order_date) as b on a.customer_id=b.customer_id and a.last_order_date=b.last_order_date
	order by customer_id, last_order_date;

	-- Next transpose this data and then link it to the single customer view.
	-- transpose the data for percentage spend by category
	drop table [dbo].[payment_method_name_trans];

	SELECT *
	into [dbo].[payment_method_name_trans]
	FROM
		(SELECT [customer_id], [last_order_date], [payment_method_name], [pct_instance]
		FROM [dbo].[payment_method_name_1]) AS SourceTable 
	PIVOT
		(AVG([pct_instance]) FOR [payment_method_name] IN
			([Card-New], [Card-Stored], [Unknown], [PayPal], [Other])) AS PivotTable
	order by [customer_id];



	--
	-- Marketing Channel Analysis
	-- Create a table of records of counts of sales channel per customer for the obs date
	drop table [dbo].[marketing_channel];

	select a.customer_id, max(a.last_order_date) as last_order_date, a.marketing_channel_name, count(a.marketing_channel_name) as numinst
	into [dbo].[marketing_channel]
	from [dbo].[oh_addnl_cols] as a
	group by a.customer_id, a.marketing_channel_name
	order by a.customer_id;

	-- Combine the total data onto the marketing channel data
	drop table [dbo].[marketing_channel_1];

	select a.*, b.total_instances, cast(a.numinst as real) /cast(b.total_instances as real) as pct_instance
	into [dbo].[marketing_channel_1]
	from 
			[dbo].[marketing_channel] as a 
		inner join
			(select customer_id, [last_order_date], sum(numinst) as total_instances
			from [dbo].[marketing_channel]
			group by customer_id, last_order_date) as b on a.customer_id=b.customer_id and a.last_order_date=b.last_order_date
	order by customer_id, last_order_date;


	-- transpose the data for percentage spend by category
	drop table [dbo].[marketing_channel_trans];

	SELECT *
	into [dbo].[marketing_channel_trans]
	FROM
			(SELECT [customer_id], [last_order_date], [marketing_channel_name], [pct_instance]
			FROM [dbo].[marketing_channel_1]) AS SourceTable 
		PIVOT
			(AVG([pct_instance]) FOR [marketing_channel_name] IN
				([Non-GA], [Organic Search], [Referral], [Email], [PaidSearch], [Customer Service], [Migration CH], [Direct Entry], [Other],[Hist CH])) AS PivotTable
	order by [customer_id];

	-- do the same for transpose the data for counts
	drop table [dbo].[marketing_channel_trans_1];

	SELECT *
	into [dbo].[marketing_channel_trans_1]
	FROM
			(SELECT [customer_id], [last_order_date], [marketing_channel_name],[numinst]
			FROM [dbo].[marketing_channel_1]) AS SourceTable 
		PIVOT
			(AVG([numinst]) FOR [marketing_channel_name] IN
				([Non-GA], [Organic Search], [Referral], [Email], [PaidSearch], [Customer Service], [Migration CH], [Direct Entry], [Other], [Hist CH])) AS PivotTable
	order by [customer_id];


-- Merge all of this data together to create a product view for the single customer view
drop table [dbo].[marketing_channel_summary];
select a.[customer_id], a.[last_order_date], 
	case when a.[Non-GA] is NULL then 0 else a.[Non-GA] end as [ct_NonGA]
	,case when a.[Organic Search] is NULL then 0 else a.[Organic Search] end as [ct_OrganicSearch]
	,case when a.[Referral] is NULL then 0 else a.[Referral] end as [ct_Referral]
	,case when a.[Email] is NULL then 0 else a.[Email] end as [ct_Email]
	,case when a.[PaidSearch] is NULL then 0 else a.[PaidSearch] end as [ct_PaidSearch]
	,case when a.[Other] is NULL then 0 else a.[Other] end as [ct_spend_Other]
	,case when a.[Migration CH] is NULL then 0 else a.[Migration CH] end as [ct_Migration CH]
	,case when a.[Direct Entry] is NULL then 0 else a.[Direct Entry] end as [ct_DirectEntry]
	,case when a.[Hist CH] is NULL then 0 else a.[Hist CH] end as [ct_HistCH]

	,case when a.[Non-GA] is NULL then 0 else a.[Non-GA] end as [pct_NonGA]
	,case when a.[Organic Search] is NULL then 0 else a.[Organic Search] end as [pct_OrganicSearch]
	,case when a.[Referral] is NULL then 0 else a.[Referral] end as [pct_Referral]
	,case when a.[Email] is NULL then 0 else a.[Email] end as [pct_Email]
	,case when a.[PaidSearch] is NULL then 0 else a.[PaidSearch] end as [pct_PaidSearch]
	,case when a.[Other] is NULL then 0 else a.[Other] end as [pct_spend_Other]
	,case when a.[Migration CH] is NULL then 0 else a.[Migration CH] end as [pct_MigrationCH]
	,case when a.[Direct Entry] is NULL then 0 else a.[Direct Entry] end as [pct_DirectEntry]
	,case when a.[Hist CH] is NULL then 0 else a.[Hist CH] end as [pct_HistCH]

	,c.[avg_days_to_shipping]
	,c.[num_discounted_orders]
	,c.[num_reorder_orders]

	,case when d.[Card-New] is NULL then 0 else d.[Card-New] end as [pm_pct_card_new]
	,case when d.[Card-Stored] is NULL then 0 else d.[Card-Stored] end as [pm_pct_card_stored]
	,case when d.[PayPal] is NULL then 0 else d.[PayPal] end as [pm_pct_paypal]
	,case when d.[Other] is NULL then 0 else d.[Other] end as [pm_pct_other]
	,case when d.[Unknown] is NULL then 0 else d.[Unknown] end as [pm_pct_unknown]

	into [dbo].[marketing_channel_summary]
	from 
			[dbo].[marketing_channel_trans] as a
		inner join 
			[dbo].[marketing_channel_trans_1] as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date]
		left join 
			[dbo].[oh_small_summary] as c on a.[customer_id]=c.[customer_id] and a.[last_order_date]=c.[last_order_date]
		left join 
			[dbo].[payment_method_name_trans] as d on a.[customer_id]=d.[customer_id] and a.[last_order_date]=d.[last_order_date]
	order by a.[customer_id];


---------------------------------------------------------------------

-- Create the final final output table
drop table [dbo].[single_cust_view_interim1];
select a.*, 
	b.[ct_NonGA], b.[ct_OrganicSearch], b.[ct_Referral], b.[ct_Email], b.[ct_PaidSearch], b.[ct_spend_Other], b.[ct_Migration CH], b.[ct_DirectEntry], b.[ct_HistCH],
	b.[pct_NonGA], b.[pct_OrganicSearch], b.[pct_Referral], b.[pct_Email], b.[pct_PaidSearch], b.[pct_spend_Other], b.[pct_MigrationCH], b.[pct_DirectEntry] ,b.[pct_HistCH], 
	b.[avg_days_to_shipping], b.[num_discounted_orders], b.[num_reorder_orders], 
	b.[pm_pct_card_new], b.[pm_pct_card_stored], b.[pm_pct_paypal], b.[pm_pct_other], b.[pm_pct_unknown]
into [dbo].[single_cust_view_interim1]
from 
		[dbo].[single_cust_view] as a 
	inner join
		[dbo].[marketing_channel_summary] as b on a.[customer_id]=b.[customer_id] and a.[last_order_date]=b.[last_order_date];


---------------------------------------------------------------------

-- Extract all the line item transactions for our customers up to their observation date
drop table [dbo].[ol_model_base];

select c.[customer_id], 
	max(c.[target_order_date]) as [target_order_date], c.[cat_name_short], 
	count(*) as num_product, min(c.[cost_per_month]) as cost_per_month_min, max(c.[cost_per_month]) as cost_per_month_max, 
	avg(c.[cost_per_month]) as cost_per_month_avg, sum(c.[cost_per_month]) as cost_per_month_sum
into [dbo].[ol_model_base]
from 
	(select a.*, b.[target_order_date], 
		case 
			when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
			when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
			else NULL 
		end as cost_per_month,
		
		case 
			when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies') then a.[category_name] 
			when a.[category_name] = 'CL - Yearly' then 'CL - Monthlies'
			when left(a.[category_name],2) = 'EC' then 'EC'
			when left(a.[category_name],2) = 'SL' then 'SL'
			else 'Other' end as cat_name_short
	from 
			[dbo].[ol] as a
		inner join 
			[dbo].[cust_obs_date_targetset] as b on CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
		where a.[order_status_name]='OK') as c
group by c.[customer_id], c.[cat_name_short]
order by c.[customer_id], c.[cat_name_short];


	-- transpose the data so we have a column of data for each major category
	drop table [dbo].[ol_model_base_trans];

	SELECT *
	into [dbo].[ol_model_base_trans]
	FROM
			(SELECT [customer_id], [target_order_date], [cat_name_short], [cost_per_month_avg]
			FROM [dbo].[ol_model_base]) AS SourceTable 
		PIVOT
			(AVG(cost_per_month_avg) FOR [cat_name_short] IN
				([EC], [CL - Monthlies], [CL - Daily], [CL - Two Weeklies], [SL], [Other])) AS PivotTable
	order by [customer_id];


-- Determine the rank position of this cost per item data;
	drop table [dbo].[cust_avg_spend];

	select x.[customer_id], x.[target_order_date],
		cast(x.[EC] as decimal(10,2)) as [avg_unit_price_paid_EC], cast(x.[CL - Monthlies] as decimal(10,2)) as [avg_unit_price_paid_CLM], 
		cast(x.[CL - Daily] as decimal(10,2)) as [avg_unit_price_paid_CLD], cast(x.[CL - Two Weeklies] as decimal(10,2)) as [avg_unit_price_paid_CL2W], 
		cast(x.[SL] as decimal(10,2)) as [avg_unit_price_paid_SL], cast(x.[Other] as decimal(10,2)) as [avg_unit_price_paid_Oth],
		
		cast(rank_EC / max_EC as decimal(10,2)) as [rank_pct_aupp_EC], cast(rank_CLM / max_CLM as decimal(10,2)) as [rank_pct_aupp_CLM], 
		cast(rank_CLD / max_CLD as decimal(10,2)) as [rank_pct_aupp_CLD], cast(rank_CL2W / max_CL2W as decimal(10,2)) as [rank_pct_aupp_C2W],
		cast(rank_SL / max_SL as decimal(10,2)) as [rank_pct_aupp_SL], cast(rank_Oth / max_Oth as decimal(10,2)) as [rank_pct_aupp_Oth]
	into [dbo].[cust_avg_spend]
	from
	(select *,
		case when [CL - Monthlies] is not null then  cast(dense_rank() over (order by [CL - Monthlies]) as real) else null end as rank_CLM,
		case when [CL - Daily] is not null then cast(dense_rank() over (order by [CL - Daily]) as real) else null end as rank_CLD,
		case when [CL - Two Weeklies] is not null then cast(dense_rank() over (order by [CL - Two Weeklies]) as real) else null end as rank_CL2W,
		case when [SL] is not null then cast(dense_rank() over (order by [SL]) as real) else null end as rank_SL,
		case when [EC] is not null then cast(dense_rank() over (order by [EC]) as real) else null end as rank_EC,
		case when [Other] is not null then cast(dense_rank() over (order by [Other]) as real) else null end as rank_Oth
	from [dbo].[ol_model_base_trans]) as x,

	(select max(a.rank_CLM) as max_CLM ,max(a.rank_CLD) as max_CLD, max(a.rank_CL2W) as max_CL2W, 
		max(a.rank_SL) as max_SL, max(a.rank_EC) as max_EC, max(a.rank_Oth) as max_Oth
	from 
		(select *, 
			case when [CL - Monthlies] is not null then  cast(dense_rank() over (order by [CL - Monthlies]) as real) else null end as rank_CLM,
			case when [CL - Daily] is not null then cast(dense_rank() over (order by [CL - Daily]) as real) else null end as rank_CLD,
			case when [CL - Two Weeklies] is not null then cast(dense_rank() over (order by [CL - Two Weeklies]) as real) else null end as rank_CL2W,
			case when [SL] is not null then cast(dense_rank() over (order by [SL]) as real) else null end as rank_SL,
			case when [EC] is not null then cast(dense_rank() over (order by [EC]) as real) else null end as rank_EC,
			case when [Other] is not null then cast(dense_rank() over (order by [Other]) as real) else null end as rank_Oth
		from [dbo].[ol_model_base_trans]) as a) as y


	-- Combine this data onto the modelling set to create the output single customer view;
	-- Create the final final output table
	drop table [dbo].[single_cust_view_interim2];

	select a.*,
		case when b.[avg_unit_price_paid_CLM] is null then null else b.[avg_unit_price_paid_CLM] end as [avg_unit_price_paid_CLM],
		case when b.[avg_unit_price_paid_CLD] is null then null else b.[avg_unit_price_paid_CLD] end as [avg_unit_price_paid_CLD],
		case when b.[avg_unit_price_paid_CL2W] is null then null else b.[avg_unit_price_paid_CL2W] end as [avg_unit_price_paid_CL2W],
		case when b.[avg_unit_price_paid_SL] is null then null else b.[avg_unit_price_paid_SL] end as [avg_unit_price_paid_SL],
		case when b.[avg_unit_price_paid_EC] is null then null else b.[avg_unit_price_paid_EC] end as [avg_unit_price_paid_EC],
		case when b.[avg_unit_price_paid_Oth] is null then null else b.[avg_unit_price_paid_Oth] end as [avg_unit_price_paid_Oth],
		
		b.[rank_pct_aupp_CLM], b.[rank_pct_aupp_CLD], b.[rank_pct_aupp_C2W], 
		b.[rank_pct_aupp_SL], b.[rank_pct_aupp_EC], b.[rank_pct_aupp_Oth]
	into [dbo].[single_cust_view_interim2]
	from 
			[dbo].[single_cust_view_interim1] as a 
		inner join
			[dbo].[cust_avg_spend] as b on CONVERT (DATE, a.[last_order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]

--------------------------------------------------------------------

--
-- Code to prepare the estimated time for next order input data set.
--
--
--
-- Note to self.
-- I need to do this process twice.  The first time we look back to allow for the development of the index
-- The second time to estimate the next consumption date but with the index based on real behaviour to get the 
-- forecast to a more realistic level.  At the moment I've not included any adjustments for situations like
-- the customer just ordering solution, which can happen and is liable to mess up this calculation.
-- 
-- This table is a transaction table of orders with the best estimate of how long the order will last.
-- If contact lenses have been purchased then we use this as the driver of the estimation for the next purchase date
drop table [dbo].[cust_order_history];

select e.[customer_id], e.[order_date], 
	case when e.[qty_time_cl] is not null then e.[qty_time_cl] else e.[qty_time_ncl] end as [order_est_consumption_time],
	e.[qty_time_cl], e.[qty_time_ncl],
	rank() over(partition by e.[customer_id] order by e.[order_date] asc) as [cust_order_num], 
	case 
		when e.[qty_time_ncl] is not null then 
			case when e.[qty_time_cl] is null then 1 else 2 end 
			else 0 
		end as [curr_order_type]
into [dbo].[cust_order_history]
from 
	(select d.[customer_id], max(d.[order_date]) as [order_date], max(d.[qty_time_cl]) as [qty_time_cl] ,max(d.[qty_time_ncl]) as [qty_time_ncl]
	from 
		(-- At this level we get the customer id the order date and the months consumption figure for contact lenses or other items.  
		select c.[customer_id], c.[order_date], c.[cat_name_vshort],
			case when c.[cat_name_vshort] = 'CL' then max(c.[qty_time]) else null end as [qty_time_cl],
			case when c.[cat_name_vshort] <> 'CL' then max(c.[qty_time]) else null end as [qty_time_ncl]
		from 
			(select 
				cast(a.[customer_id] as varchar) + cast(a.[base_curve] as varchar) + cast(a.[diameter] as varchar) + cast(a.[power] as varchar) as cust_id_prescription,
				a.*, b.[target_order_date],
				case 
					when a.[qty_time] <> 0 then cast(a.[local_total_inc_vat] as real)/a.[qty_time]
					when charindex('DAY',UPPER(a.[product_family_name])) > 0 then (a.[qty_unit]*a.[qty_pack]) / 30.25
					else NULL 
				end as cost_per_month,
				case 
					when a.[category_name] in ('CL - Daily', 'CL - Monthlies', 'CL - Two Weeklies', 'CL - Yearly') then 'CL' 
					when left(a.[category_name],2) = 'EC' then 'EC'
					when left(a.[category_name],2) = 'SL' then 'SL'
					else 'OT' 
				end as cat_name_vshort
			from 
					[dbo].[ol] as a
				inner join 
					[dbo].[cust_obs_date_targetset] as b on 
						CONVERT (DATE, a.[order_date]) <= CONVERT (DATE, b.[target_order_date]) and a.[customer_id]=b.[customer_id]
			where a.[order_status_name]='OK') as c
		group by c.[customer_id], c.[order_date], c.[cat_name_vshort]) as d
		group by d.[customer_id], CONVERT(DATE, d.[order_date])) as e
order by e.customer_id, e.order_date;


-- First we must look backwards to calcuate the historical time between orders and from this calculate the index between product 
-- ordered and time taken to consume said product.
-- Join the just created table to itself to get the previous order date, from which we can look at the time taken between orders
-- To cope with two orders in the same month set the lower limit of months_to_next_order to 1 when calculating the index
drop table [dbo].[cust_order_history_bckwrd];

select c.*,
	case 
		when c.[order_est_consumption_time]=0 then cast(c.[order_est_consumption_time] as decimal(10,2)) else cast(c.[months_to_next_order] as decimal(10,2)) / c.[order_est_consumption_time] 
	end as [cust_consumption_index],

	case 
		when c.[qty_time_ncl] is not null then case when c.[qty_time_cl] is null then 1 else 2 end 
		else 0 
	end as [order_type]
into [dbo].[cust_order_history_bckwrd]
from 
	(select a.*, b.[order_date] as next_order_date,
		DATEDIFF(MONTH, CONVERT(DATE, a.[order_date]), CONVERT(DATE, b.[order_date])) as [months_to_next_order]
	from 
			[dbo].[cust_order_history] as a
		inner join 
			[dbo].[cust_order_history] as b on a.[customer_id]=b.[customer_id]
	where a.[cust_order_num]+1=b.[cust_order_num]) as c
order by c.[customer_id], c.[order_date];



-- Calculate for all of history the index between prescription duration and consumption duration;
drop table [dbo].[order_index_all];
select a.[customer_id],
	AVG(a.[cust_consumption_index]) as [consumption_index_all_orders], STDEV(a.[cust_consumption_index]) as [consumption_index_all_orders_stddev],
	COUNT(*) as [consumption_index_all_orders_N]
into [dbo].[order_index_all]
from [dbo].[cust_order_history_bckwrd] as a
group by a.[customer_id]
order by a.[customer_id];

drop table [dbo].[order_index_no_lens];
select a.[customer_id],
	AVG(a.[cust_consumption_index]) as [consumption_index_no_lenses], STDEV(a.[cust_consumption_index]) as [consumption_index_no_lenses_stddev],
	COUNT(*) as [consumption_index_no_lenses_N]
into [dbo].[order_index_no_lens]
from [dbo].[cust_order_history_bckwrd] as a
where a.[order_type] = 1
group by a.[customer_id]
order by a.[customer_id];

drop table [dbo].[order_index_lens_only];
select a.[customer_id],
	AVG(a.[cust_consumption_index]) as [consumption_index_lenses_only], STDEV(a.[cust_consumption_index]) as [consumption_index_lenses_only_stddev], 
	COUNT(*) as [consumption_index_lenses_only_N]
into [dbo].[order_index_lens_only]
from [dbo].[cust_order_history_bckwrd] as a
where a.[order_type] = 0
group by a.[customer_id]
order by a.[customer_id];


-- Merge these three indicies into one table for use later
drop table [dbo].[order_indicies];

select a.[customer_id],
	a.[consumption_index_all_orders], b.[consumption_index_no_lenses], c.[consumption_index_lenses_only],
	a.[consumption_index_all_orders_stddev], b.[consumption_index_no_lenses_stddev], c.[consumption_index_lenses_only_stddev],
	a.[consumption_index_all_orders_N], b.[consumption_index_no_lenses_N], c.[consumption_index_lenses_only_N],
	case 
		when a.[consumption_index_all_orders_stddev] <> 0 then a.[consumption_index_all_orders]/ a.[consumption_index_all_orders_stddev] 
		else null 
	end as [consumption_index_all_orders_CV],
	case 
		when b.[consumption_index_no_lenses_stddev] <> 0 then b.[consumption_index_no_lenses] / b.[consumption_index_no_lenses_stddev] 
		else null 
	end as [consumption_index_no_lenses_CV],
	case 
		when c.[consumption_index_lenses_only_stddev] <> 0 then c.[consumption_index_lenses_only] / c.[consumption_index_lenses_only_stddev] 
		else null 
	end as [consumption_index_lenses_only_CV]
into [dbo].[order_indicies]
from 
		[dbo].[order_index_all] as a
	left join 
		[dbo].[order_index_no_lens] as b on a.[customer_id]=b.[customer_id]
	left join 
		[dbo].[order_index_lens_only] as c on a.[customer_id]=c.[customer_id];


--------------------------------------------------------------------


-- Next merge this index data onto the single customer view.
drop table [dbo].[single_cust_view_fin]
select a.*,
	b.[order_est_consumption_time], 
	c.[consumption_index_all_orders], c.[consumption_index_no_lenses], c.[consumption_index_lenses_only],
	c.[consumption_index_all_orders_stddev], c.[consumption_index_no_lenses_stddev], c.[consumption_index_lenses_only_stddev],
	c.[consumption_index_all_orders_N], c.[consumption_index_no_lenses_N], c.[consumption_index_lenses_only_N],
	c.[consumption_index_all_orders_CV], c.[consumption_index_no_lenses_CV], c.[consumption_index_lenses_only_CV],
	b.[order_est_consumption_time]*c.[consumption_index_all_orders] as [est_nmtno_all_orders],
	b.[order_est_consumption_time]*c.[consumption_index_no_lenses] as [est_nmtno_all_no_lenses],
	b.[order_est_consumption_time]*c.[consumption_index_lenses_only] as [est_nmtno_lenses_only],
	
	cast(a.[target_ndtno] as decimal) / (365/12) as [target_nmtno], b.[curr_order_type]
into [dbo].[single_cust_view_fin]
from 
		[dbo].[single_cust_view_interim2] as a 
	left join 
		[dbo].[cust_order_history] as b on a.[customer_id]=b.[customer_id] and CONVERT(DATE, a.[last_order_date]) = CONVERT(DATE, b.[order_date])
	left join 
		[dbo].[order_indicies] as c on a.[customer_id]=c.[customer_id]
order by a.[customer_id], a.[last_orde_date];