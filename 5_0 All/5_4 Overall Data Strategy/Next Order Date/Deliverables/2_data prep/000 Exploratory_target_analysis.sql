/****** Script for SelectTopNRows command from SSMS  ******/
-- what does the raw order data look like?
SELECT [customer_id]
	  ,ROW_NUMBER() OVER(PARTITION BY [customer_id] ORDER BY [customer_id]) as order_number
	  ,[order_id]
      ,[order_date]
      ,[order_status_name]
      ,[acquired_website_f]
      ,[website]
      ,[rank_next_order_freq_time]
      ,[next_order_freq_time]
      ,[rank_order_qty_time]
      ,[order_qty_time]
      ,[country_code_ship]
      ,[order_lifecycle_name]
      ,[rank_seq_no]
      ,[customer_order_seq_no]
      ,[rank_shipping_days]
      ,[shipping_days]
      ,[marketing_channel_name]
      ,[customer_unsubscribe_name]
      ,[price_type_name]
      ,[discount_f]
      ,[payment_method_name]
      ,[shipping_method_name]
      ,[reorder_f]
      ,[reminder_type_name]
      ,[reminder_period_name]
      ,[reminder_date]
      ,[local_subtotal]
      ,[local_shipping]
      ,[local_discount]
      ,[local_store_credit_used]
      ,[local_total_inc_vat]
      ,[num_line ]
	  ,ABS(CHECKSUM(NewId())) % 100 as random_number -- generate a random number between 0 and 99
  FROM [VisionDirect].[dbo].[oh]
  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
  and order_status_name = 'OK'
  --and [customer_id] in (269046)--469652, 269046, 338311, 868262, 791047)
  order by [customer_id],[order_date]
  ;

-- how many different order status values are there?
SELECT [order_status_name]
		,count(*) as [num_instances]
  FROM [VisionDirect].[dbo].[oh]
  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
	--	and order_status_name = 'OK'
  group by [order_status_name]
  ;

-- what does the distribution of num orders look like over the sample time window?
select  a.[num_orders]
		,count(*) as [num_customers]
from
(
SELECT [customer_id]
		,count(*) as [num_orders]
  FROM [VisionDirect].[dbo].[oh]
  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
		and order_status_name = 'OK'
  group by [customer_id]
) as a
group by a.[num_orders]
order by [num_orders]
  ;

  -- who are the extreme ordering customers?
  -- what does the distribution of num orders look like over the sample time window?
 select a.* 
		,ceiling(cast(a.num_orders as float) * (cast(random_number as float) / 100.0)) as sample_trans
  from
  (
	SELECT [customer_id]
			,count(*) as [num_orders]
			,(ABS(CHECKSUM(NewId())) % 100) as random_number -- generate a random number between 0.00 and 0.99
	  FROM [VisionDirect].[dbo].[oh]
	  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
			and order_status_name = 'OK'
	  group by [customer_id]
  ) as a
  --where a.[num_orders]>=67
;


-- what does the distribution of the random number generation look like over the sample time window?
select  b.[random_number]
		,count(*) as [num_customers]
from
(
 select a.* 
		,ceiling(cast(a.num_orders as float) * (cast(random_number as float) / 100.0)) as sample_trans -- Calculate the transaction number to be 
  from
  (
	SELECT [customer_id]
			,count(*) as [num_orders]
			,(ABS(CHECKSUM(NewId())) % 100) as random_number -- generate a random number between 0.00 and 0.99
	  FROM [VisionDirect].[dbo].[oh]
	  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
			and order_status_name = 'OK'
	  group by [customer_id]
  ) as a
) as b
group by b.[random_number]
order by b.[random_number]
  ;
  -- as of 26th June the distribution looked quite normal.

  -- Identify the transaction from which we wish to recreate the customer record status;


  select a.* 
	 	 ,ceiling(cast(a.num_orders as float) * (cast(random_number as float) / 100.0)) as sample_trans -- Calculate the transaction number to be matched
  from
  (
    -- Generate a random number between 0 and 99 for each record
	SELECT [customer_id]
			,count(*) as [num_orders]
			,(ABS(CHECKSUM(NewId())) % 100) as random_number -- generate a random number between 0 and 99
	  FROM [VisionDirect].[dbo].[oh]
	  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
			and order_status_name = 'OK'
	  group by [customer_id]
  ) as a


  -- randomly sample the observation date;
  DROP table [dbo].[cust_obs_date];
  SELECT a.*
		,b.[num_orders]
		,b.[sample_trans]
  into [dbo].[cust_obs_date]
  from 
  (
	  SELECT [customer_id]
		  ,[order_id]
		  ,[order_date]
		  ,[order_status_name]
		  ,[acquired_website_f]
		  ,[website]
		  ,[country_code_ship]
		  ,ROW_NUMBER() OVER(PARTITION BY [customer_id] ORDER BY [customer_id]) as order_number
	  FROM [VisionDirect].[dbo].[oh]
	  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
	  and order_status_name = 'OK'
	) as a
	inner join
	(
	  select aa.* 
	 		 ,ceiling(cast(aa.num_orders as float) * (cast(aa.random_number as float) / 100.0)) as sample_trans -- Calculate the transaction number to be matched
	  from
	  (
		-- Generate a random number between 0 and 99 for each record
		SELECT [customer_id]
				,count(*) as [num_orders]
				,(ABS(CHECKSUM(NewId())) % 99)+1 as random_number -- generate a random number between 0 and 99
		  FROM [VisionDirect].[dbo].[oh]
		  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
				and order_status_name = 'OK'
		  group by [customer_id]
	  ) as aa 
	) as b
	on a.[customer_id]=b.[customer_id] and a.[order_number]=b.[sample_trans]
    order by [customer_id]
  ;




-- how many valid customer records in the raw transaction file?
select count(*)
from
(
SELECT distinct([customer_id])
  FROM [VisionDirect].[dbo].[oh]
  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
  and order_status_name = 'OK'
  ) as a
  ;

  -- BUG hunt;
  -- zero sample trans values;
  select b.*
  from(
  	  select aa.* 
	 		 ,ceiling(cast(aa.num_orders as float) * (cast(aa.random_number as float) / 100.0)) as sample_trans -- Calculate the transaction number to be matched
	  from
	  (
		-- Generate a random number between 0 and 99 for each record
		SELECT [customer_id]
				,count(*) as [num_orders]
				,(ABS(CHECKSUM(NewId())) % 99)+1 as random_number -- generate a random number between 0 and 99
		  FROM [VisionDirect].[dbo].[oh]
		  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
				and order_status_name = 'OK'
		  group by [customer_id]
	  ) as aa 
	) as b
	  where b.sample_trans=0



-- Distribution of order date;
select a.year_month_of_order
		,count(*) as num_recs
from
	(
		SELECT [customer_id]
			  ,[order_id]
			  ,[order_date]
			  ,[order_status_name]
			  ,[acquired_website_f]
			  ,[website]
			  ,[country_code_ship]
			  ,[order_number]
			  ,[num_orders]
			  ,[sample_trans]
			  ,year([order_date]) * 100 + month([order_date]) as year_month_of_order
		  FROM [VisionDirect].[dbo].[cust_obs_date]
	) as a
group by a.year_month_of_order
order by a.year_month_of_order
;


-- min max checks on random number;
select max(aa.random_number)
	  from
	  (
		-- Generate a random number between 0 and 99 for each record
		SELECT [customer_id]
				,count(*) as [num_orders]
				,(ABS(CHECKSUM(NewId())) % 100)+1 as random_number -- generate a random number between 0 and 99
		  FROM [VisionDirect].[dbo].[oh]
		  where [order_date]>='01Apr2014 00:00.00' and [order_date] < '01Apr2017 00:00.00'
				and order_status_name = 'OK'
		  group by [customer_id]
	  ) as aa 
	  ;
